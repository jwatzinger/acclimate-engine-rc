#include "AddEntityController.h"
#include "ComponentCombineController.h"
#include "Entity\EntityManager.h"
#include "Gui\BaseWindow.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace editor
	{

		enum class Methods
		{
			EMPTY,
			COMBINE,
			TEMPLATE
		};

		AddEntityController::AddEntityController(gui::Module& module, ecs::EntityManager& entities) : 
			BaseController(module, L"../Editor/Menu/Entity/AddEntity.axm"), m_bContinue(false), m_pEntities(&entities)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");
			m_pBox = GetWidgetByName<MethodBox>(L"Methods");

			GetWidgetByName(L"Continue")->SigReleased.Connect(this, &AddEntityController::OnContinue);
			GetWidgetByName(L"Cancel")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);

			m_pComponents = &AddController<ComponentCombineController>(entities);

			m_pName = GetWidgetByName<gui::Textbox<>>(L"Name");
			m_pName->SigConfirm.Connect(this, &AddEntityController::OnContinue);
		}

		ComponentCombineController& AddEntityController::GetCombineController(void)
		{
			return *m_pComponents;
		}

		bool AddEntityController::Execute(void)
		{
			m_bContinue = false;
			m_pName->OnFocus(true);
			m_pWindow->Execute();

			if(m_bContinue)
			{
				const std::wstring& stName = m_pName->GetContent();
				switch((Methods)m_pBox->GetContent())
				{
				case Methods::EMPTY:
					{
						OnCreateEmpty(stName);
						break;
					}
				case Methods::COMBINE:
					{
						bool bReturned = false;
						if(m_pComponents->Execute(stName, &bReturned))
							SigEntityAdded();
						if(bReturned)
							return Execute();
						break;
					}
				}
				
				return true;
			}
			else
				return false;
		}

		void AddEntityController::Update(void)
		{
			if(m_pMainWidget->IsVisible())
				Execute();
		}

		void AddEntityController::OnCreateEmpty(void)
		{
			OnCreateEmpty(L"Unnamed");
		}

		void AddEntityController::OnCreateCombined(void)
		{
			if(m_pComponents->Execute(L"Unnamend", nullptr))
				SigEntityAdded();
		}

		void AddEntityController::OnContinue(void)
		{
			m_pWindow->OnClose();
			m_bContinue = true;
		}

		void AddEntityController::OnContinue(std::wstring stName)
		{
			OnContinue();
		}

		void AddEntityController::OnCreateEmpty(const std::wstring& stName)
		{
			auto& entity = m_pEntities->CreateEntity(stName);
			SigEntityAdded();
		}

	}
}

