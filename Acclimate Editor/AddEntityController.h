#pragma once
#include "Gui\BaseController.h"
#include "Gui\RadioBox.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class ComponentCombineController;

		class AddEntityController :
			public gui::BaseController
		{
			typedef gui::GuiRadioBox<int> MethodBox;
		public:
			AddEntityController(gui::Module& module, ecs::EntityManager& entities);

			ComponentCombineController& GetCombineController(void);

			bool Execute(void);

			void Update(void);

			void OnCreateEmpty(void);
			void OnCreateCombined(void);

			core::Signal<> SigEntityAdded;

		private:

			void OnContinue(void);
			void OnContinue(std::wstring stName);
			void OnCreateEmpty(const std::wstring& stName);

			bool m_bContinue;

			ecs::EntityManager* m_pEntities;

			MethodBox* m_pBox;
			gui::BaseWindow* m_pWindow;
			gui::Textbox<>* m_pName;

			ComponentCombineController* m_pComponents;
		};

	}
}


