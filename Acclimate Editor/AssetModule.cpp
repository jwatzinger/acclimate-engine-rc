#include "AssetModule.h"
#include "AssetViewer.h"
#include "Utility.h"
#include "File\Dir.h"
#include "Gfx\Context.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\ResourceBlock.h"
#include "Gui\MenuBar.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		const gfx::PathData paths(L"../Resources/", L"Textures/", L"Effects/", L"Meshes/");

		AssetModule::AssetModule(gui::BaseController& controller, gui::MenuBar& bar, const gfx::Context& gfx, const render::IRenderer& renderer):
			m_pLoader(gfx.load.pLoader), m_saver(gfx.resources), m_resources(gfx.resources, paths)
		{
			auto& assets = bar.AddItem(L"ASSETS");

			m_pViewer = &controller.AddController<AssetViewer>(assets, gfx, m_resources, renderer);
		}

		Project::ModuleType AssetModule::GetModuleType(void) const
		{
			return Project::ModuleType::RESOURCES;
		}

		void AssetModule::AddResourceBlock(const std::wstring& stName, gfx::ResourceBlock& block, bool bLocked)
		{
			m_pViewer->AddResourceBlock(stName, block, bLocked);
			m_mBlocks[stName] = &block;
		}

		void AssetModule::Update(void)
		{
		}

		void AssetModule::OnNew(const std::wstring& stFile)
		{
		}

		void AssetModule::OnLoad(const std::wstring& stFile)
		{
			m_resources.SetPath(getProjectDirectory() + L"Resources/");
			m_resources.Clear();
			m_pViewer->Clear();

			m_resources.Begin();
			m_pViewer->BeginLoad();

			m_pLoader->Load(stFile);

			m_pViewer->EndLoad();
			m_resources.End();
		}

		void AssetModule::OnSave(const std::wstring& stFile)
		{
			m_saver.Save(stFile, m_resources);
		}

		void AssetModule::OnNewScene(const std::wstring& stScene, const std::wstring& stFile)
		{
			file::DirCreate(L"Resources");

			xml::Doc doc;
			doc.InsertNode(L"Resources");
			doc.SaveFile(stFile);
		}

		void AssetModule::OnSceneChanged(void)
		{
			m_pViewer->OnRefresh();
		}

		void AssetModule::OnSaveScene(const std::wstring& stFile)
		{
			if(m_mBlocks.count(L"Scene"))
			{
				m_saver.Save(stFile, *m_mBlocks[L"Scene"]);
			}
		}

		void AssetModule::OnClose(void)
		{
			m_pViewer->Clear();
		}

		void AssetModule::OnBeginTest(void)
		{

		}

		void AssetModule::OnEndTest(void)
		{

		}

	}
}
