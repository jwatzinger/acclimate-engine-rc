#pragma once
#include "IModule.h"
#include "Gfx\ResourceSaver.h"
#include "Gfx\ResourceBlock.h"

namespace acl
{
	namespace gfx
	{
		struct Context;
		class IResourceLoader;
	}

	namespace gui
	{
		class BaseController;
		class MenuBar;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class AssetViewer;

		class AssetModule :
			public IModule
		{
			typedef std::map<std::wstring, const gfx::ResourceBlock*> BlockMap;
		public:
			AssetModule(gui::BaseController& controller, gui::MenuBar& bar, const gfx::Context& gfx, const render::IRenderer& renderer);

			void AddResourceBlock(const std::wstring& stName, gfx::ResourceBlock& block, bool bLocked);

			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override;
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			BlockMap m_mBlocks;
			AssetViewer* m_pViewer;

			gfx::ResourceSaver m_saver;
			// TODO: this is storing dublicated information, think about integrating it somehow into the seperate controller
			gfx::ResourceBlock m_resources;
			gfx::IResourceLoader* m_pLoader;
		};

	}
}


