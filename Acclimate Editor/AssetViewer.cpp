#include "AssetViewer.h"
#include "TextureController.h"
#include "MeshController.h"
#include "MaterialController.h"
#include "ModelController.h"
#include "EffectController.h"
#include "Gfx\Context.h"
#include "Gfx\ResourceBlock.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Gui\TabBar.h"
#include "Gui\TabBarItem.h"

namespace acl
{
	namespace editor
	{

		AssetViewer::AssetViewer(gui::Module& module, gui::MenuItem& assets, const gfx::Context& gfx, gfx::ResourceBlock& resources, const render::IRenderer& renderer) :
			BaseController(module, L"../Editor/Menu/Asset/AssetViewer.axm"), m_pResources(&resources)
		{
			auto& viewer = assets.AddOption(L"Assets viewer");
			viewer.SigReleased.Connect(this, &AssetViewer::OnToggle);
			viewer.SetShortcut(gui::ComboKeys::CTRL_ALT, 'A', gui::ShortcutState::ALWAYS);
			viewer.SigShortcut.Connect(this, &AssetViewer::OnToggle);

			// tab bar
			m_pBar = GetWidgetByName<gui::TabBar>(L"Blocks");
			m_pBar->SigItemSelected.Connect(this, &AssetViewer::OnSelectTab);

			auto& resourceArea = *GetWidgetByName(L"Resources");

			// textures
			m_pTextures = &AddController<TextureController>(resources.Textures(), gfx.load.textures);
			m_pTextures->SetParent(resourceArea);
			StoreController(L"Textures", *m_pTextures);

			auto& textures = assets.AddOption(L"Textures");
			textures.SigReleased.Connect(m_pTextures, &TextureController::OnToggle);
			textures.SetShortcut(gui::ComboKeys::CTRL_ALT, 'T', gui::ShortcutState::ALWAYS);
			textures.SigShortcut.Connect(m_pTextures, &TextureController::OnToggle);

			// effects
			m_pEffects = &AddController<EffectController>(gfx.resources.effects, gfx.load.effects);
			m_pEffects->SetParent(resourceArea);
			StoreController(L"Effects", *m_pEffects);

			auto& effects = assets.AddOption(L"Effects");
			effects.SigReleased.Connect(m_pEffects, &TextureController::OnToggle);
			//effects.SetShortcut(gui::ComboKeys::CTRL_ALT, 'T', gui::ShortcutState::ALWAYS);
			effects.SigShortcut.Connect(m_pTextures, &TextureController::OnToggle);

			// materials
			m_pMaterials = &AddController<MaterialController>(gfx.resources.materials, gfx.load.materials, gfx.resources.effects, gfx.resources.textures);
			m_pMaterials->SetParent(resourceArea);
			StoreController(L"Materials", *m_pMaterials);
			m_pEffects->SigEffectsChanged.Connect(m_pMaterials, &MaterialController::OnEffectsChanged);

			auto& materials = assets.AddOption(L"Materials");
			materials.SigReleased.Connect(m_pMaterials, &MaterialController::OnToggle);
			materials.SetShortcut(gui::ComboKeys::CTRL_ALT, 'M', gui::ShortcutState::ALWAYS);
			materials.SigShortcut.Connect(m_pMaterials, &MaterialController::OnToggle);

			// meshes
			m_pMeshes = &AddController<MeshController>(gfx.resources.meshes, gfx.load.meshes, gfx.resources.models, gfx.resources.effects, renderer);
			m_pMeshes->SetParent(resourceArea);
			StoreController(L"Meshes", *m_pMeshes);

			auto& meshes = assets.AddOption(L"Meshes");
			meshes.SigReleased.Connect(m_pMeshes, &MeshController::OnToggle);
			meshes.SetShortcut(gui::ComboKeys::CTRL_ALT, 'E', gui::ShortcutState::ALWAYS);
			meshes.SigShortcut.Connect(m_pMeshes, &MeshController::OnToggle);

			// models
			m_pModels = &AddController<ModelController>(gfx.resources.models, gfx.load.models, gfx.resources.meshes, gfx.resources.materials, gfx.resources.effects, renderer);
			m_pModels->SetParent(resourceArea);
			StoreController(L"Models", *m_pModels);
			m_pMeshes->SigMeshesChanged.Connect(m_pModels, &ModelController::OnMeshesChanged);
			m_pMaterials->SigMaterialsChanged.Connect(m_pModels, &ModelController::OnMaterialsChanged);

			auto& models = assets.AddOption(L"Models");
			models.SigReleased.Connect(m_pModels, &ModelController::OnToggle);
			models.SetShortcut(gui::ComboKeys::CTRL_ALT, 'D', gui::ShortcutState::ALWAYS);
			models.SigShortcut.Connect(m_pModels, &ModelController::OnToggle);
		}

		void AssetViewer::AddResourceBlock(const std::wstring& stName, gfx::ResourceBlock& block, bool bLocked)
		{
			m_pTextures->AddBlock(stName, block.Textures(), bLocked);
			m_pMaterials->AddBlock(stName, block.Materials(), bLocked);
			m_pMeshes->AddBlock(stName, block.Meshes(), bLocked);
			m_pModels->AddBlock(stName, block.Models(), bLocked);
			m_pEffects->AddBlock(stName, block.Effects(), bLocked);
		}

		void AssetViewer::Clear(void)
		{
			m_pResources->Clear();

			m_pMaterials->Clear();
			m_pMeshes->Clear();
			m_pModels->Clear();
			m_pEffects->Clear();

			OnRefresh();
		}

		void AssetViewer::BeginLoad(void)
		{
			m_pResources->Begin();

			m_pMaterials->Begin();
			m_pMeshes->Begin();
			m_pModels->Begin();
			m_pEffects->Begin();
		}

		void AssetViewer::EndLoad(void)
		{
			m_pResources->End();

			m_pMaterials->End();
			m_pMeshes->End();
			m_pModels->End();
			m_pEffects->End();

			OnRefresh();
		}

		void AssetViewer::OnRefresh(void)
		{
			m_pTextures->Refresh();
		}

		void AssetViewer::StoreController(const std::wstring& stName, gui::BaseController& controller)
		{
			m_pBar->AddItem(stName);

			m_mController[stName] = &controller;
		}

		void AssetViewer::OnSelectTab(const gui::TabBarItem& item)
		{
			for(auto& controller : m_mController)
			{
				if(item.GetName() == controller.first)
					controller.second->OnToggle(true);
				else
					controller.second->OnToggle(false);
			}
		}

	}
}

