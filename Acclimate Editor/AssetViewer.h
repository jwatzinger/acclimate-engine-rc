#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace gfx
	{
		struct Context;
		class ResourceBlock;
	}

	namespace gui
	{
		class MenuItem;
		class TabBar;
		class TabBarItem;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class ModelController;
		class TextureController;
		class MeshController;
		class MaterialController;
		class EffectController;
		
		class AssetViewer :
			public gui::BaseController
		{
			typedef std::map<std::wstring, gui::BaseController*> ControllerMap;
		public:
			AssetViewer(gui::Module& module, gui::MenuItem& assets, const gfx::Context& gfx, gfx::ResourceBlock& resources, const render::IRenderer& renderer);

			void AddResourceBlock(const std::wstring& stName, gfx::ResourceBlock& block, bool bLocked);
			void Clear(void);
			void BeginLoad(void);
			void EndLoad(void);

			void OnRefresh(void);

		private:

			void StoreController(const std::wstring& stName, gui::BaseController& controller);

			void OnSelectTab(const gui::TabBarItem& item);

			ModelController* m_pModels;
			TextureController* m_pTextures;
			MeshController* m_pMeshes;
			MaterialController* m_pMaterials;
			EffectController* m_pEffects;
			ControllerMap m_mController;

			gfx::ResourceBlock* m_pResources;

			gui::TabBar* m_pBar;
		};

	}
}


