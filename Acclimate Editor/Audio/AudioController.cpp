#include "AudioController.h"
#include "AudioImportController.h"
#include "Audio\ILoader.h"
#include "File\File.h"
#include "Gui\ContextMenu.h"
#include "Gui\ContextMenuItem.h"
#include "..\Utility.h"

namespace acl
{
	namespace editor
	{

		const math::Rect rIcon(0, 0, 21, 21);

		AudioController::AudioController(gui::Module& module, audio::Files& files, const audio::ILoader& loader) : BaseController(module, L"../Editor/Menu/Audio/Audio.axm"),
			m_pFiles(&files), m_pLoader(&loader)
		{
			m_pList = &AddWidget<AudioList>(0.0f, 0.0f, 0.33f, 1.0f, 0.02f);

			// context menu
			auto& context = AddWidget<gui::ContextMenu>(0.02f);
			auto& import = context.AddItem(L"Import audio-file");
			import.SigReleased.Connect(this, &AudioController::OnImport);

			m_pList->SetContextMenu(&context);
		}

		AudioController::~AudioController(void)
		{
		}

		void AudioController::Refresh(void)
		{
			m_pList->Clear();

			for(auto& file : m_pFiles->Map())
			{
				auto& item = m_pList->AddItem(file.first, file.second);
				item.SetIcon(L"AudioIcon", rIcon);
			}
		}

		void AudioController::OnImport(void)
		{
			AudioImportController import(*m_pModule);

			std::wstring stFile, stName, stTarget;
			bool copy;
			if(import.Execute(stFile, stName, copy, stTarget))
			{
				// handle copy
				if(copy)
				{
					const auto& stTargetPath = getProjectDirectory() + L"Sounds/" + stTarget;
					file::Copy(stFile, stTargetPath);
					stFile = stTargetPath;
				}

				m_pLoader->LoadFile(stFile, stName);

				Refresh();
			}
		}

	}
}
