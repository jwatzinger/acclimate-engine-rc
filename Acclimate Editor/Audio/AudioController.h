#pragma once
#include "Audio\Files.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace audio
	{
		class ILoader;
	}

	namespace editor
	{

		class AudioController :
			public gui::BaseController
		{
			typedef gui::List<audio::File*> AudioList;
		public:
			AudioController(gui::Module& module, audio::Files& files, const audio::ILoader& loader);
			~AudioController(void);

			void Refresh(void);

		private:

			void OnImport(void);
			
			audio::Files* m_pFiles;
			const audio::ILoader* m_pLoader;

			AudioList* m_pList;
		};

	}
}

