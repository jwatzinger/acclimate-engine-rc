#include "AudioImportController.h"
#include "File\File.h"
#include "Gui\Checkbox.h"
#include "Gui\FileDialog.h"
#include "Gui\Window.h"

namespace acl
{
	namespace editor
	{

		AudioImportController::AudioImportController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Audio/Import.axm"),
			m_import(false)
		{
			m_pName = GetWidgetByName<gui::Textbox<>>(L"TargetName");
			m_pName->SigConfirm.Connect(this, &AudioImportController::OnImport);
			m_pName->OnFocus(true);

			m_pFile = GetWidgetByName<gui::Textbox<>>(L"FileName");
			m_pFile->SigConfirm.Connect(this, &AudioImportController::OnImport);

			m_pBox = GetWidgetByName<gui::CheckBox>(L"Copy");

			m_pWindow = GetWidgetByName<gui::Window>(L"ImportWindow");

			GetWidgetByName(L"Import")->SigReleased.Connect(this, &AudioImportController::OnImport);
			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AudioImportController::OnCancel);
		}

		AudioImportController::~AudioImportController()
		{
		}

		bool AudioImportController::Execute(std::wstring& stFile, std::wstring& stName, bool& copy, std::wstring& stTarget)
		{
			m_import = false;

			gui::FileDialog dialog(gui::DialogType::OPEN, L"Choose sound file");
			
			if(dialog.Execute())
			{
				const auto& stFilename = dialog.GetFileName();
				stFile = dialog.GetFullPath();

				m_pFile->SetText(file::SubExtention(stFilename));

				m_pWindow->Execute();

				if(m_import)
				{
					stName = m_pName->GetContent();
					copy = m_pBox->IsChecked();

					if(copy)
						stTarget = m_pFile->GetContent() + L"." + file::Extention(stFilename);
				}
			}

			return m_import;
		}

		void AudioImportController::OnImport(void)
		{
			m_import = true;
			m_pWindow->OnClose();
		}

		void AudioImportController::OnImport(std::wstring stName)
		{
			OnImport();
		}

		void AudioImportController::OnCancel(void)
		{
			m_import = false;
			m_pWindow->OnClose();
		}

	}
}

