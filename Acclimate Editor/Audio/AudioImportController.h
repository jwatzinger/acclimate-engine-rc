#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gui
	{
		class Window;
		class CheckBox;
	}

	namespace editor
	{

		class AudioImportController :
			public gui::BaseController
		{
		public:
			AudioImportController(gui::Module& module);
			~AudioImportController();

			bool Execute(std::wstring& stFile, std::wstring& stName, bool& copy, std::wstring& stTarget);

		private:

			void OnImport(void);
			void OnImport(std::wstring stName);
			void OnCancel(void);

			bool m_import;

			gui::Window* m_pWindow;
			gui::Textbox<>* m_pName, *m_pFile;
			gui::CheckBox* m_pBox;
		};

	}
}


