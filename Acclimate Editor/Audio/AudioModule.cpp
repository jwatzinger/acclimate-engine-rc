#include "AudioModule.h"
#include "AudioController.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Audio\ILoader.h"
#include "Audio\Context.h"
#include "Audio\Saver.h"

namespace acl
{
	namespace editor
	{

		AudioModule::AudioModule(gui::BaseController& controller, gui::MenuBar& bar, const audio::Context& audio) : m_pAudio(&audio)
		{
			m_pController = new AudioController(controller.GetModule(), audio.files, audio.loader);

			auto* pItem = bar.GetItem(L"ASSETS");
			pItem->InsertSeperator();
			auto& audioOption = pItem->AddOption(L"View audio files");
			audioOption.SigReleased.Connect(m_pController, &AudioController::OnToggle);
		}

		AudioModule::~AudioModule(void)
		{
			delete m_pController;
		}

		Project::ModuleType AudioModule::GetModuleType(void) const
		{
			return Project::ModuleType::AUDIO;
		}

		void AudioModule::OnNew(const std::wstring& stFile)
		{
			m_pController->Refresh();
		}

		void AudioModule::OnSceneChanged(void)
		{
		}

		void AudioModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void AudioModule::Update(void)
		{
			m_pController->Update();
		}

		void AudioModule::OnLoad(const std::wstring& stFile)
		{
			m_pAudio->loader.Load(stFile);
			m_pController->Refresh();
		}

		void AudioModule::OnSave(const std::wstring& stFile)
		{
			audio::Saver saver(m_pAudio->files);
			saver.Save(stFile);
		}

		void AudioModule::OnClose(void)
		{
		}

		void AudioModule::OnBeginTest(void)
		{
		}

		void AudioModule::OnEndTest(void)
		{
		}

	}
}
