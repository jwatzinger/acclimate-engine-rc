#pragma once
#include "..\IModule.h"

namespace acl
{
	namespace audio
	{
		struct Context;
	}

	namespace gui
	{
		class MenuBar;
		class BaseController;
	}

	namespace editor
	{

		class AudioController;

		class AudioModule :
			public IModule
		{
		public:
			AudioModule(gui::BaseController& controller, gui::MenuBar& bar, const audio::Context& audio);
			~AudioModule(void);

			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			AudioController* m_pController;

			const audio::Context* m_pAudio;
		};

	}
}


