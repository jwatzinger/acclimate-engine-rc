#include "ComponentAttachController.h"
#include "ComponentBoxController.h"
#include "Gui\ScrollArea.h"
#include "Gui\BaseWindow.h"
#include "Gui\CheckBox.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		ComponentAttachController::ComponentAttachController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Entity/ComponentAttach.axm"),
			m_pEntity(nullptr), m_bAttach(false)
		{
			m_pArea = GetWidgetByName<gui::ScrollArea>(L"Components");

			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);
			GetWidgetByName(L"Attach")->SigReleased.Connect(this, &ComponentAttachController::OnAttach);

			// checkbox
			GetWidgetByName<gui::CheckBox>(L"Select")->SigChecked.Connect(this, &ComponentAttachController::OnSelectAll);
		}

		void ComponentAttachController::AddComponentController(const std::wstring& stName, IComponentController& controller)
		{
			auto& boxController = AddController<ComponentBoxController>(*m_pArea, controller, stName, m_vBoxController.size()*0.11f);
			boxController.SigToggle.Connect(this, &ComponentAttachController::OnToggledArea);

			m_vBoxController.push_back(&boxController);
		}

		void ComponentAttachController::RemoveComponentController(const std::wstring& stName)
		{
			unsigned int pos = 0;
			for(auto pController : m_vBoxController)
			{
				if(pController->GetName() == stName)
				{
					RemoveController(*pController);
					m_vBoxController.erase(m_vBoxController.begin() + pos);
					return;
				}

				pos++;
			}
		}

		bool ComponentAttachController::Execute(ecs::Entity& entity)
		{
			m_bAttach = false;
			unsigned int i = 0;
			for(auto pController : m_vBoxController)
			{
				i++;

				const bool visible = !pController->HasComponent(entity);
				if(visible != pController->IsVisible())
				{
					pController->OnToggle(visible);
					for(unsigned int j = i; j < m_vBoxController.size(); j++)
					{
						const int direction = -1 + 2 * (visible);
						m_vBoxController[j]->Move(direction*2);
					}
				}
			}

			m_pEntity = &entity;
			m_pWindow->Execute();
			m_pEntity = nullptr;

			if(m_bAttach)
				SigAttached();
			return m_bAttach;
		}

		void ComponentAttachController::OnAttach(void)
		{
			for(auto pController : m_vBoxController)
			{
				pController->AttachEntity(*m_pEntity);
			}
			m_bAttach = true;
			m_pWindow->OnClose();
		}

		void ComponentAttachController::OnSelectAll(bool bSelect)
		{
			for(auto pController : m_vBoxController)
			{
				pController->SetSelected(bSelect);
			}
		}

		void ComponentAttachController::OnToggledArea(bool bVisible, ComponentBoxController& controller)
		{
			auto itr = std::find(m_vBoxController.begin(), m_vBoxController.end(), &controller);

			ACL_ASSERT(itr != m_vBoxController.end());

			size_t id = itr - m_vBoxController.begin();

			const int direction = -1 + 2 * (bVisible);
			for(unsigned int i = id + 1; i < m_vBoxController.size(); i++)
			{
				m_vBoxController[i]->Move(direction);
			}
		}
	}
}

