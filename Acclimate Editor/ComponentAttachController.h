#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace ecs
	{
		class Entity;
	}

	namespace gui
	{
		class ScrollArea;
		class BaseWindow;
	}

	namespace editor
	{

		class IComponentController;
		class ComponentBoxController;

		class ComponentAttachController :
			public gui::BaseController
		{
			typedef std::vector<ComponentBoxController*> ControllerVector;
		public:
			ComponentAttachController(gui::Module& module);

			void AddComponentController(const std::wstring& stName, IComponentController& controller);
			void RemoveComponentController(const std::wstring& stName);

			bool Execute(ecs::Entity& entity);

			core::Signal<> SigAttached;

		private:

			void OnAttach(void);
			void OnSelectAll(bool bSelect);
			void OnToggledArea(bool bVisible, ComponentBoxController& controller);

			bool m_bAttach;
			ecs::Entity* m_pEntity;

			ControllerVector m_vBoxController;

			gui::ScrollArea* m_pArea;
			gui::BaseWindow* m_pWindow;
		};

	}
}


