#include "ComponentBoxController.h"
#include "IComponentController.h"
#include "Utility.h"
#include "Entity\Entity.h"
#include "Gui\Label.h"
#include "Gui\Button.h"

namespace acl
{
	namespace editor
	{

		ComponentBoxController::ComponentBoxController(gui::Module& module, gui::Widget& parent, IComponentController& controller, const std::wstring& stName, float y) :
			BaseController(module, parent, GetEditorDirectory() + L"Menu/Entity/ComponentBox.axm"), m_pController(&controller), m_bAreaVisible(true), m_y(y),
			m_stName(stName)
		{
			m_pMainWidget->SetY(y);

			// child controller area
			m_pArea = GetWidgetByName(L"Controller");
			controller.GetController().SetParent(*m_pArea);

			// button
			auto pButton = GetWidgetByName<gui::Button>(L"Name");
			pButton->SetLabel(stName + L":");
			pButton->SigReleased.Connect(this, &ComponentBoxController::OnToggleArea);

			// checkbox
			m_pSelected = GetWidgetByName<gui::CheckBox>(L"Selected");
		}

		void ComponentBoxController::AttachEntity(ecs::Entity& entity)
		{
			if(m_pSelected->IsChecked() && !HasComponent(entity))
				m_pController->Attach(entity);
		}

		void ComponentBoxController::Move(int direction)
		{
			m_y += direction*0.11f*0.5f;
			m_pMainWidget->SetY(m_y);
		}

		void ComponentBoxController::SetSelected(bool bSelect)
		{
			m_pSelected->OnCheck(bSelect);
		}

		const std::wstring& ComponentBoxController::GetName(void) const
		{
			return m_stName;
		}

		bool ComponentBoxController::HasComponent(ecs::Entity& entity) const
		{
			return entity.GetComponent(m_pController->GetComponentId()) != nullptr;
		}

		void ComponentBoxController::OnToggleArea(void)
		{
			m_bAreaVisible = !m_bAreaVisible;
			m_pArea->SetVisible(m_bAreaVisible);
			SigToggle(m_bAreaVisible, *this);
		}

	}
}

