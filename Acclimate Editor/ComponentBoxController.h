#pragma once
#include "Gui\BaseController.h"
#include "Gui\CheckBox.h"

namespace acl
{
	namespace ecs
	{
		class Entity;
	}

	namespace editor
	{

		class IComponentController;

		class ComponentBoxController :
			public gui::BaseController
		{
		public:
			ComponentBoxController(gui::Module& module, gui::Widget& parent, IComponentController& controller, const std::wstring& stName, float y);

			void AttachEntity(ecs::Entity& entity);
			void Move(int direction);

			void SetSelected(bool bSelect);

			const std::wstring& GetName(void) const;
			bool HasComponent(ecs::Entity& entity) const;

			core::Signal<bool, ComponentBoxController&> SigToggle;

		private:

			void OnToggleArea(void);

			float m_y;
			bool m_bAreaVisible;
			std::wstring m_stName;

			IComponentController* m_pController;

			gui::Widget* m_pArea;
			gui::CheckBox* m_pSelected;
		};

	}
}


