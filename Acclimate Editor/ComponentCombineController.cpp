#include "ComponentCombineController.h"
#include "ComponentBoxController.h"
#include "Entity\EntityManager.h"
#include "Gui\BaseWindow.h"
#include "Gui\ScrollArea.h"
#include "Gui\CheckBox.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		ComponentCombineController::ComponentCombineController(gui::Module& module, ecs::EntityManager& entities) : 
			BaseController(module, L"../Editor/Menu/Entity/ComponentCombine.axm"), m_pEntities(&entities), m_bReturn(false),
			m_bCreated(false)
		{
			m_pArea = GetWidgetByName<gui::ScrollArea>(L"Components");

			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");

			// buttons
			GetWidgetByName(L"Return")->SigReleased.Connect(this, &ComponentCombineController::OnReturn);
			GetWidgetByName(L"Create")->SigReleased.Connect(this, &ComponentCombineController::OnCreate);

			// checkbox
			GetWidgetByName<gui::CheckBox>(L"Select")->SigChecked.Connect(this, &ComponentCombineController::OnSelectAll);
		}

		void ComponentCombineController::AddComponentController(const std::wstring& stName, IComponentController& controller)
		{
			auto& boxController = AddController<ComponentBoxController>(*m_pArea, controller, stName, m_vController.size()*0.11f);
			boxController.SigToggle.Connect(this, &ComponentCombineController::OnToggledArea);

			m_vController.push_back(&boxController);
		}

		void ComponentCombineController::RemoveComponentController(const std::wstring& stName)
		{
			unsigned int pos = 0;
			for(auto pController : m_vController)
			{
				if(pController->GetName() == stName)
				{
					RemoveController(*pController);
					m_vController.erase(m_vController.begin() + pos);
					return;
				}
				pos++;
			}
		}

		bool ComponentCombineController::Execute(const std::wstring& stName, bool* pReturn)
		{
			m_bReturn = false;
			m_bCreated = false;
			m_stName = stName;

			m_pWindow->Execute();

			if(pReturn)
				*pReturn = m_bReturn;

			return m_bCreated;
		}

		void ComponentCombineController::OnCreate(void)
		{
			auto& entity = m_pEntities->CreateEntity(m_stName);

			for (auto pController : m_vController)
			{
				pController->AttachEntity(entity);
			}

			m_bReturn = false;
			m_bCreated = true;

			m_pWindow->OnClose();
		}

		void ComponentCombineController::OnReturn(void)
		{
			m_pWindow->OnClose();
			m_bReturn = true;
		}

		void ComponentCombineController::OnSelectAll(bool bSelect)
		{
			for (auto pController : m_vController)
			{
				pController->SetSelected(bSelect);
			}
		}

		void ComponentCombineController::OnToggledArea(bool bVisible, ComponentBoxController& controller)
		{
			auto itr = std::find(m_vController.begin(), m_vController.end(), &controller);

			ACL_ASSERT(itr != m_vController.end());

			size_t id = itr - m_vController.begin();

			int direction = -1 + 2 * (bVisible);
			for(unsigned int i = id+1; i < m_vController.size(); i++)
			{
				m_vController[i]->Move(direction);
			}
		}

	}
}

