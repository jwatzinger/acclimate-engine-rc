#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gui
	{
		class BaseWindow;
		class ScrollArea;
	}

	namespace editor
	{

		class IComponentController;
		class ComponentBoxController;

		class ComponentCombineController :
			public gui::BaseController
		{
			typedef std::vector<ComponentBoxController*> ControllerVector;
		public:
			ComponentCombineController(gui::Module& module, ecs::EntityManager& entities);

			void AddComponentController(const std::wstring& stName, IComponentController& controller);
			void RemoveComponentController(const std::wstring& stName);

			bool Execute(const std::wstring& stName, bool* pReturn);

		private:

			void OnCreate(void);
			void OnReturn(void);
			void OnSelectAll(bool bSelect);
			void OnToggledArea(bool bVisible, ComponentBoxController& controller);

			bool m_bReturn, m_bCreated;
			std::wstring m_stName;

			ControllerVector m_vController;
			ecs::EntityManager* m_pEntities;

			gui::ScrollArea* m_pArea;
			gui::BaseWindow* m_pWindow;
		};

	}
}

