#include "ComponentRegistry.h"
#include "IComponentController.h"
#include "IComponentAttribute.h"
#include "EntityAttributeController.h"
#include "ComponentCombineController.h"
#include "ComponentAttachController.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace editor
	{

		ComponentRegistry::ComponentRegistry(ComponentCombineController& components, ComponentAttachController& attach, EntityAttributeController& attributes) :
			m_pComponents(&components), m_pAttach(&attach), m_pAttributes(&attributes)
		{
		}

		ComponentRegistry::~ComponentRegistry()
		{
			for (auto& controller : m_mController)
			{
				delete controller.second;
			}
		}

		void ComponentRegistry::AddComponentAttachController(const std::wstring& stName, IComponentController& controller)
		{
			m_pComponents->AddComponentController(stName, controller);
			m_pAttach->AddComponentController(stName, controller.Clone());
			m_mController[stName] = &controller;
		}

		void ComponentRegistry::AddComponentAtttributeController(const std::wstring& stName, IComponentAttribute& controller)
		{
			m_pAttributes->AddComponent(stName, controller);
		}

		void ComponentRegistry::RemoveComponentAttachController(const std::wstring& stName)
		{
			auto itr = m_mController.find(stName);
			if(itr != m_mController.end())
			{
				m_pComponents->RemoveComponentController(stName);
				m_pAttach->RemoveComponentController(stName);
				delete m_mController[stName];
				m_mController.erase(itr);
			}
		}

		void ComponentRegistry::RemoveComponentAtttributeController(const std::wstring& stName)
		{
			m_pAttributes->RemoveComponent(stName);
		}

		void ComponentRegistry::OnEntityChanged(void)
		{
			m_pAttributes->OnUpdateEntity();
		}

	}
}
