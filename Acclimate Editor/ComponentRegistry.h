#pragma once
#include <map>
#include "IComponentRegistry.h"

namespace acl
{
	namespace editor
	{

		class ComponentCombineController;
		class ComponentAttachController;
		class EntityAttributeController;

		class ComponentRegistry :
			public IComponentRegistry
		{
			typedef std::map<std::wstring, IComponentController*> ControllerMap;
		public:
			ComponentRegistry(ComponentCombineController& components, ComponentAttachController& attach, EntityAttributeController& attributes);
			~ComponentRegistry();

			void AddComponentAttachController(const std::wstring& stName, IComponentController& controller);
			void AddComponentAtttributeController(const std::wstring& stName, IComponentAttribute& controller);

			void RemoveComponentAttachController(const std::wstring& stName);
			void RemoveComponentAtttributeController(const std::wstring& stName);

			void OnEntityChanged(void);

		private:

			ControllerMap m_mController;
			ComponentCombineController* m_pComponents;
			ComponentAttachController* m_pAttach;
			EntityAttributeController* m_pAttributes;
		};

	}
}

