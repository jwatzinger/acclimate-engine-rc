#include "ComponentTreeCallback.h"
#include "Gui\ContextMenu.h"

namespace acl
{
	namespace editor
	{

		ComponentTreeCallback::ComponentTreeCallback(const ecs::EntityHandle& entity, const ecs::BaseComponent& component) :
			m_entity(entity), m_pComponent(&component)
		{
		}

		const std::wstring* ComponentTreeCallback::GetIconName(void) const
		{
			static const std::wstring stIcon = L"ComponentIcon";
			return &stIcon;
		}

		bool ComponentTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool ComponentTreeCallback::IsDeletable(void) const
		{
			return true;
		}

		bool ComponentTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void ComponentTreeCallback::OnSelect(void)
		{
			SigSelect(m_entity, *m_pComponent);
		}

		void ComponentTreeCallback::OnDelete(void)
		{
			m_entity->DetachComponent(*m_pComponent);
			SigDelete();
		}

		gui::ContextMenu* ComponentTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pComponentMenu = new gui::ContextMenu(0.02f);
			
			auto& copy = pComponentMenu->AddItem(L"Copy");
			copy.SigReleased.Connect(this, &ComponentTreeCallback::OnCopy);

			return pComponentMenu;
		}

		void ComponentTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void ComponentTreeCallback::OnCopy(void)
		{
			SigCopy(*m_pComponent);
		}

	}
}

