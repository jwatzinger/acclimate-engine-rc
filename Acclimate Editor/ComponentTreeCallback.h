#pragma once
#include "Core\Dll.h"
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace ecs
	{
		struct BaseComponent;
	}

	namespace editor
	{

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;

		class ComponentTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			ComponentTreeCallback(const ecs::EntityHandle& entity, const ecs::BaseComponent& component);
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			const std::wstring* GetIconName(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring&) override {};

			core::Signal<ecs::EntityHandle&, const ecs::BaseComponent&> SigSelect;
			core::Signal<> SigDelete;
			core::Signal<const ecs::BaseComponent&> SigCopy;

		private:

			void OnCopy(void);
			void OnPaste(void);

			ecs::EntityHandle m_entity;
			const ecs::BaseComponent* m_pComponent;
		};
	}
}


