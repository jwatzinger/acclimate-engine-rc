#include "Editor.h"
#include "ModelPickController.h"
#include "EntityPickController.h"
#include "TexturePickController.h"
#include "ScriptClassPickController.h"
#include "IComponentRegistry.h"
#include "IGameView.h"
#include "Core\BaseContext.h"
#include "EntityExplorer.h"
#include "Entity\EntityManager.h"
#include "Gui\DockArea.h"
#include "System\Log.h"
#include "Render\IRenderer.h"

namespace acl
{
	namespace editor
	{

		Editor::Editor(IComponentRegistry& registry, const core::GameStateContext& context, gui::DockArea& main, EntityExplorer& explorer, gui::MenuItem& sceneItem) :
			m_pRegistry(&registry), m_pContext(&context), m_pMain(&main), m_pGameView(nullptr), m_pEntities(&explorer), m_sceneItem(sceneItem)
		{
			m_pModelPick = new ModelPickController(*main.GetModule(), context.gfx.resources.models, *context.render.renderer.GetStage(L"modelpreview"));

			m_pTexturePick = new TexturePickController(*main.GetModule(), context.gfx.resources.textures);

			m_pEntityPick = new EntityPickController(*main.GetModule(), context.ecs.entities);

			m_pScriptClassPick = new ScriptClassPickController(*main.GetModule(), context.script.core);
		}

		Editor::~Editor(void)
		{
			delete m_pModelPick;
			delete m_pTexturePick;
			delete m_pEntityPick;
			delete m_pScriptClassPick;

			for(auto& extention : m_mExtentions)
			{
				delete extention.second;
			}
		}

		void Editor::SetGameView(IGameView* pGameView)
		{
			if(m_pGameView)
			{
				m_pEntities->SigEntitySelected.Disconnect(m_pGameView, &IGameView::OnSelectEntity);
				m_pEntities->SigGotoEntity.Disconnect(m_pGameView, &IGameView::OnGotoEntity);
			}

			if(pGameView)
			{
				pGameView->OnInit(m_pContext->gfx, m_pContext->render.renderer);
				pGameView->GetClickedSignal().Connect(this, &Editor::OnClickedGameView);
				m_pMain->AddDock(pGameView->GetDock(), gui::DockType::MIDDLE, true);
				m_pEntities->SigEntitySelected.Connect(pGameView, &IGameView::OnSelectEntity);
				m_pEntities->SigGotoEntity.Connect(pGameView, &IGameView::OnGotoEntity);
			}	

			m_pGameView = pGameView;
		}

		void Editor::AddSceneExtention(ISceneExtention& extention)
		{
			auto& stExtention = extention.OnGetName();
			if(!m_mExtentions.count(stExtention))
			{
				auto& section = m_sceneItem.AddSection();
				extention.OnInit(*m_pMain->GetModule());
				extention.OnAddMenuItems(section);
			}
			else
			{
				sys::log->Out(sys::LogModule::EDITOR, sys::LogType::WARNING, "Failed to add extention", stExtention, ". Extention already exists.");
				delete &extention;
			}
		}

		gui::Module& Editor::GetModule(void) const
		{
			return *m_pMain->GetModule();
		}

		IComponentRegistry& Editor::GetComponentRegistry(void) const
		{
			return *m_pRegistry;
		}

		const ecs::MessageManager& Editor::GetMessageManager(void) const
		{
			return m_pContext->ecs.messages;
		}

		bool Editor::PickTexture(const gfx::ITexture** pTexture, std::wstring* pName) const
		{
			m_pTexturePick->OnRefresh();
			return m_pTexturePick->Execute(pTexture, pName);
		}

		bool Editor::PickTexture(const std::wstring& stName, const gfx::ITexture** pTexture, std::wstring* pName) const
		{
			m_pTexturePick->OnRefresh();
			return m_pTexturePick->Execute(stName, pTexture, pName);
		}

		bool Editor::PickModel(const gfx::IModel** pModel, std::wstring* pName) const
		{
			m_pModelPick->OnRefresh(); // todo: optimize, we only want to refresh if model list actually changed
			return m_pModelPick->Execute(pModel, pName);
		}

		bool Editor::PickModel(const std::wstring& stDefault, const gfx::IModel** pModel, std::wstring* pName) const
		{
			m_pModelPick->OnRefresh(); // todo: optimize, we only want to refresh if model list actually changed
			return m_pModelPick->Execute(stDefault, pModel, pName);
		}

		bool Editor::PickEntity(ecs::EntityHandle* pEntity, std::wstring* pName) const
		{
			m_pEntityPick->OnRefresh();
			return m_pEntityPick->Execute(pEntity, pName);
		}

		bool Editor::PickEntity(const std::wstring& stDefault, ecs::EntityHandle* pEntity, std::wstring* pName) const
		{
			m_pEntityPick->OnRefresh();
			return m_pEntityPick->Execute(stDefault, pEntity, pName);
		}

		void Editor::OnEntityChanged(void)
		{
			m_pRegistry->OnEntityChanged();
		}

		void Editor::DeleteEntity(const ecs::EntityHandle& entity)
		{
			m_pEntities->DeleteEntity(entity);
		}

		void Editor::CopyEntity(const ecs::Entity& entity)
		{
			m_pEntities->CopyEntity(entity);
		}

		void Editor::PasteEntity(void)
		{
			m_pEntities->PasteEntity();
		}

		bool Editor::PickScriptClass(const std::string& stInterface, std::string* pName) const
		{
			m_pScriptClassPick->OnRefresh(stInterface);
			return m_pScriptClassPick->Execute(pName);
		}

		bool Editor::PickScriptClass(const std::string& stDefault, const std::string& stInterface, std::string* pName) const
		{
			m_pScriptClassPick->OnRefresh(stInterface);
			return m_pScriptClassPick->Execute(stDefault, pName);
		}

		void Editor::Render(void) const
		{
			if(m_pGameView)
				m_pGameView->OnRender();
		}

		void Editor::OnClickedGameView(void)
		{
			ACL_ASSERT(m_pGameView);

			const auto entity = m_pGameView->OnPickEntity(m_pContext->ecs.entities, m_pContext->gui.module.m_cursor.GetPosition());
			m_pGameView->OnSelectEntity(&entity);

			m_pEntities->PickEntity(entity);
		}

	}
}
