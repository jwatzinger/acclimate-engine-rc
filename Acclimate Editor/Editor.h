#pragma once
#include <map>
#include "IEditor.h"
#include "ISceneExtention.h"

namespace acl
{
	namespace core
	{
		struct GameStateContext;
	}

	namespace gui
	{
		class Widget;
		class DockArea;
		class MenuItem;
	}

	namespace editor
	{

		class ModelPickController;
		class EntityPickController;
		class TexturePickController;
		class ScriptClassPickController;
		class EntityExplorer;

		class Editor :
			public IEditor
		{
			typedef std::map<std::wstring, ISceneExtention*> ExtentionMap;
		public:

			Editor(IComponentRegistry& registry, const core::GameStateContext& context, gui::DockArea& main, EntityExplorer& entities, gui::MenuItem& sceneItem);
			~Editor(void);

			void SetGameView(IGameView* pGameView) override;
			void AddSceneExtention(ISceneExtention& extention) override;

			gui::Module& GetModule(void) const override;
			IComponentRegistry& GetComponentRegistry(void) const override;
			const ecs::MessageManager& GetMessageManager(void) const override;

			bool PickTexture(const gfx::ITexture** pTexture, std::wstring* pName) const override;
			bool PickTexture(const std::wstring& stDefault, const gfx::ITexture** pTexture, std::wstring* pName) const override;
			bool PickModel(const gfx::IModel** pModel, std::wstring* pName) const override;
			bool PickModel(const std::wstring& stDefault, const gfx::IModel** pModel, std::wstring* pName) const override;
			bool PickEntity(ecs::EntityHandle* pEntity, std::wstring* pName) const override;
			bool PickEntity(const std::wstring& stDefault, ecs::EntityHandle* pEntity, std::wstring* pName) const override;
			bool PickScriptClass(const std::string& stInterface, std::string* pName) const override;
			bool PickScriptClass(const std::string& stDefault, const std::string& stInterface, std::string* pName) const override;

			void OnEntityChanged(void) override;
			void DeleteEntity(const ecs::EntityHandle& entity) override;
			void CopyEntity(const ecs::Entity& entity) override;
			void PasteEntity(void) override;

			void Render(void) const override;

		private:

			void OnClickedGameView(void);

			IGameView* m_pGameView;
			IComponentRegistry* m_pRegistry;
			TexturePickController* m_pTexturePick;
			ModelPickController* m_pModelPick;
			EntityPickController* m_pEntityPick;
			EntityExplorer* m_pEntities;
			ScriptClassPickController* m_pScriptClassPick;
			ExtentionMap m_mExtentions;

			const core::GameStateContext* m_pContext;

			gui::DockArea* m_pMain;
			MenuItem m_sceneItem;
		};

	}
}


