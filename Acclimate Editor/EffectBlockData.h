#pragma once
#include "Gfx\Effects.h"

namespace acl
{
	namespace editor
	{

		struct EffectBlockData
		{
			EffectBlockData(void) : pBlock(nullptr), bLocked(false)
			{
			}

			EffectBlockData(const gfx::Effects::Block& block, bool bLocked) : pBlock(&block),
			bLocked(bLocked)
			{
			}

			const gfx::Effects::Block* pBlock;
			bool bLocked;
		};

	}
}

