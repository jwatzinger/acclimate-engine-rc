#include "EffectController.h"
#include "Gui\FileDialog.h"
#include "Gui\MsgBox.h"
#include "Gui\TreeView.h"
#include "Gui\TextArea.h"
#include "Gfx\IEffectLoader.h"
#include "Gfx\EffectFile.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		EffectController::EffectController(gui::Module& module, gfx::Effects& effects, const gfx::IEffectLoader& loader) : 
			BaseController(module, L"../Editor/Menu/Asset/Effects.axm"), m_pLoader(&loader), m_effects(effects),
			m_model(m_mEffects)
		{
			// effect list
			m_pTree = GetWidgetByName<gui::TreeView>(L"Effects");
			m_pTree->SetModel(&m_model);

			// name box
			m_pNameBox = GetWidgetByName<gui::Textbox<>>(L"Name");

			// texture button
			GetWidgetByName<gui::Widget>(L"NewEffect")->SigReleased.Connect(this, &EffectController::OnNewEffect);

			// delete button
			m_pDelete = GetWidgetByName<gui::Widget>(L"Delete");
			m_pDelete->SigReleased.Connect(this, &EffectController::OnDeleteSelected);

			// import controller
			//m_pTextureImport = &AddController<TextureImportController>();

			// text area
			m_pArea = GetWidgetByName<gui::TextArea>(L"Text");

			// model
			m_model.SigEffectSelected.Connect(this, &EffectController::OnPickEffect);
		}

		EffectController::~EffectController(void)
		{
			m_pTree->SetModel(nullptr);
			m_effects.Clear();
		}

		void EffectController::Update(void)
		{
			BaseController::Update();

			m_model.Update();
		}

		void EffectController::AddBlock(const std::wstring& stName, const gfx::Effects::Block& effects, bool bLocked)
		{
			m_mEffects[stName] = EffectBlockData(effects, bLocked);
		}

		void EffectController::Begin(void)
		{
			m_effects.Begin();
		}

		void EffectController::End(void)
		{
			m_effects.End();
			OnRefreshList();
		}

		void EffectController::Clear(void)
		{
			m_effects.Clear();
			OnRefreshList();
		}

		void EffectController::OnNewEffect(void)
		{
			//gui::FileDialog dialog(gui::DialogType::OPEN);
			//if(dialog.Execute())
			//{
			//	while (true)
			//	{
			//		gfx::TextureLoadInfo info;
			//		info.stPath = dialog.GetFullPath();

			//		if(m_pTextureImport->Execute(info))
			//		{
			//			if(m_textures.Has(info.stName))
			//			{
			//				gui::MsgBox messageBox(L"Error", (L"Texture with name \"" + info.stName + L"\" already exists.").c_str(), gui::IconType::WARNING);
			//				m_pModule->RegisterWidget(messageBox);
			//				messageBox.Execute();
			//			}
			//			else
			//			{
			//				if (!m_pLoader->Load(info.stName, info.stPath, info.bRead, info.format))
			//				{
			//					gui::MsgBox messageBox(L"Error", (L"Failed to load texture from file " + info.stPath + L".").c_str(), gui::IconType::ALERT);
			//					m_pModule->RegisterWidget(messageBox);
			//					messageBox.Execute();
			//				}
			//				else
			//				{
			//					m_pTextureImport->OnLoadedTexture(info.stName);
			//					m_pList->SelectByName(info.stName);
			//				}

			//				break;
			//			}
			//		}
			//		else
			//			break;
			//	}
			//}

			//m_pList->OnFocus(true);
		}

		void EffectController::OnRefreshList(void)
		{
			m_model.OnEffectsChanged();
			SigEffectsChanged();
		}

		void EffectController::OnPickEffect(const gfx::IEffect* pEffect)
		{
			if(pEffect)
			{
				//m_pNameBox->SetText(m_pList->GetSelectedName());
				m_pArea->SetText(conv::ToW(pEffect->GetFile().GetSource()));
			}
			else
			{
				m_pNameBox->SetText(L"");
				m_pArea->SetText(L"");
			}

			//GetWidgetByName<gui::Texture>(L"Texture")->SetTexture(pTexture);
		}

		void EffectController::OnDeleteSelected(void)
		{
			
		}

	}
}

	