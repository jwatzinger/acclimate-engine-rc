#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"
#include "EffectBlockData.h"
#include "EffectTreeModel.h"

namespace acl
{
	namespace gfx
	{
		class IEffectLoader;
	}
	
	namespace gui
	{
		class TreeView;
		class TextArea;
	}

	namespace editor
	{

		//class TextureImportController;

		class EffectController :
			public gui::BaseController
		{
			typedef std::map<std::wstring, EffectBlockData> EffectMap;
		public:

			EffectController(gui::Module& module, gfx::Effects& effects, const gfx::IEffectLoader& loader);
			~EffectController(void);

			void Update(void) override;

			void AddBlock(const std::wstring& stName, const gfx::Effects::Block& textures, bool bLocked);

			void Begin(void);
			void End(void);
			void Clear(void);

			core::Signal<> SigEffectsChanged;

		private:

			void OnNewEffect(void);
			void OnRefreshList(void);
			void OnPickEffect(const gfx::IEffect* pEffect);
			void OnDeleteSelected(void);

			EffectMap m_mEffects;
			gfx::Effects::Block m_effects;
			const gfx::IEffectLoader* m_pLoader;

			gui::TreeView* m_pTree;
			gui::Textbox<>* m_pNameBox;
			gui::Widget* m_pDelete;
			gui::TextArea* m_pArea;
			EffectTreeModel m_model;

			//TextureImportController* m_pTextureImport;
		};

	}
}


