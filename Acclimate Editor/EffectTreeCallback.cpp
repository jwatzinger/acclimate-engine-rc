#include "EffectTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		EffectTreeCallback::EffectTreeCallback(const gfx::IEffect& effect, bool bLocked) :
			m_pEffect(&effect), m_bLocked(bLocked)
		{
		}

		const std::wstring* EffectTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"EffectLockedIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"EffectIcon";
				return &stIcon;
			}
		}

		bool EffectTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool EffectTreeCallback::IsDeletable(void) const
		{
			return !m_bLocked;
		}

		bool EffectTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void EffectTreeCallback::OnSelect(void)
		{
			SigSelect(*m_pEffect);
		}

		void EffectTreeCallback::OnDelete(void)
		{
			SigDelete(*m_pEffect);
		}

		gui::ContextMenu* EffectTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);
			return pEntityMenu;
		}

		void EffectTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void EffectTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(*m_pEffect, stName);
		}

	}
}