#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class IEffect;
	}

	namespace editor
	{

		class EffectTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			EffectTreeCallback(const gfx::IEffect& effect, bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const gfx::IEffect&> SigSelect;
			core::Signal<const gfx::IEffect&> SigDelete;
			core::Signal<const gfx::IEffect&, const std::wstring&> SigRename;

		private:

			bool m_bLocked;
			const gfx::IEffect* m_pEffect;
		};

	}
}


