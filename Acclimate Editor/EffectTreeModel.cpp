#include "EffectTreeModel.h"
#include "FolderTreeCallback.h"
#include "EffectTreeCallback.h"

namespace acl
{
	namespace editor
	{

		EffectTreeModel::EffectTreeModel(const EffectMap& effects) : m_root(L"Effects"),
			m_bDirty(false), m_pEffects(&effects)
		{
		}

		const gui::Node& EffectTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& EffectTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void EffectTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& effects : *m_pEffects)
				{
					gui::Node effectBlockNode(effects.first);
					auto pCallback = new FolderTreeCallback(effects.second.bLocked);
					effectBlockNode.pCallback = pCallback;

					const size_t size = effects.second.pBlock->Size();
					if(size)
					{
						for(unsigned int i = 0; i < size; i++)
						{
							auto& effect = effects.second.pBlock->GetPair(i);
							gui::Node component(effect.first);
							auto pEffectCallback = new EffectTreeCallback(*effect.second, effects.second.bLocked);
							pEffectCallback->SigDelete.Connect(this, &EffectTreeModel::OnDeleteEffect);
							pEffectCallback->SigSelect.Connect(this, &EffectTreeModel::OnSelectEffect);
							component.pCallback = pEffectCallback;
							effectBlockNode.vChilds.push_back(component);
						}

						m_root.vChilds.push_back(effectBlockNode);
					}
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void EffectTreeModel::OnEffectsChanged(void)
		{
			m_bDirty = true;
		}

		void EffectTreeModel::OnDeleteEffect(const gfx::IEffect& effect)
		{

		}

		void EffectTreeModel::OnSelectEffect(const gfx::IEffect& effect)
		{
			SigEffectSelected(&effect);
		}

		//void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		//{
		//	SigGotoEntity(entity);
		//}

		//void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		//{
		//	SigEntitySelected(&entity);
		//}

		//void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		//{
		//	m_pEntities->RemoveEntity(entity);
		//	SigEntitySelected(nullptr);
		//	m_bDirty = true;
		//}

		//void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		//{
		//	if(m_pAttach->Execute(entity))
		//		m_bDirty = true;
		//}

		//void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		//{
		//	if(entity.GetName() != stName)
		//	{
		//		entity.SetName(stName);
		//		m_bDirty = true;
		//	}
		//}


	}
}

