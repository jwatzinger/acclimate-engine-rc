#pragma once
#include "Gui\ITreeModel.h"
#include <map>
#include "EffectBlockData.h"

namespace acl
{
	namespace editor
	{

		class EffectTreeModel :
			public gui::ITreeModel
		{
			typedef std::map<std::wstring, EffectBlockData> EffectMap;
		public:
			EffectTreeModel(const EffectMap& effects);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnEffectsChanged(void);

			core::Signal<const gfx::IEffect*> SigEffectSelected;

		private:

			void OnDeleteEffect(const gfx::IEffect& effect);
			void OnSelectEffect(const gfx::IEffect& effect);

			bool m_bDirty;
			const EffectMap* m_pEffects;

			gui::Node m_root;
			core::Signal<> SigUpdated;
		};

	}
}


