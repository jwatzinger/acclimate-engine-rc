#include "EntityClipboardData.h"
#include "Entity\Component.h"

namespace acl
{
	namespace editor
	{

		EntityClipboardData::EntityClipboardData(const ecs::Entity& entity) : m_entity(entity)
		{
		}

		const ecs::Entity& EntityClipboardData::GetEntity(void) const
		{
			return m_entity;
		}

		ComponentClipboardData::ComponentClipboardData(const ecs::BaseComponent& component) : m_pComponent(&component.Clone())
		{
		}

		ComponentClipboardData::~ComponentClipboardData(void)
		{
			delete m_pComponent;
		}

		const ecs::BaseComponent& ComponentClipboardData::GetComponent(void) const
		{
			return *m_pComponent;
		}

	}
}