#pragma once
#include "Entity\Entity.h"
#include "System\ClipboardData.h"

namespace acl
{
	namespace ecs
	{
		struct BaseComponent;
	}

	namespace editor
	{

		class EntityClipboardData :
			public sys::ClipboardData<EntityClipboardData>
		{
		public:
			EntityClipboardData(const ecs::Entity& entity);

			const ecs::Entity& GetEntity(void) const;

		private:

			const ecs::Entity m_entity;
		};

		class ComponentClipboardData :
			public sys::ClipboardData<ComponentClipboardData>
		{
		public:
			ComponentClipboardData(const ecs::BaseComponent& component);
			~ComponentClipboardData(void);

			const ecs::BaseComponent& GetComponent(void) const;

		private:

			const ecs::BaseComponent* m_pComponent;
		};

	}
}


