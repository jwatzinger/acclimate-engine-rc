#include "EntityAttributeController.h"
#include "IComponentAttribute.h"
#include "Gui\MultiTable.h"
#include "Gui\Table.h"
#include "Gui\TableItem.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"

namespace acl
{
	namespace editor
	{

		EntityAttributeController::EntityAttributeController(gui::Module& module, gui::DockArea& dock) :
			BaseController(module, L"../Editor/Menu/Entity/Attribute.axm")
		{
			dock.AddDock(*GetWidgetByName<gui::Dock>(L"Dock"), gui::DockType::RIGHT, false);

			m_pTable = GetWidgetByName<gui::MultiTable>(L"Attributes");
		}

		void EntityAttributeController::AddComponent(const std::wstring& stName, IComponentAttribute& attributes)
		{
			m_vAttributes.push_back(std::make_pair(stName, &attributes));
		}

		void EntityAttributeController::RemoveComponent(const std::wstring& stName)
		{
			unsigned int pos = 0;
			for(auto pController : m_vAttributes)
			{
				if(pController.first == stName)
				{
					delete pController.second;
					m_vAttributes.erase(m_vAttributes.begin() + pos);

					OnSelectEntity(nullptr);

					return;
				}
				pos++;
			}
		}

		void EntityAttributeController::OnEntityChanged(void)
		{
			OnSelectEntity(&m_entity);
		}

		void EntityAttributeController::OnSelectEntity(const ecs::EntityHandle* pEntity)
		{
			if(pEntity)
				m_entity = *pEntity;
			else
				m_entity.Invalidate();

			m_pTable->Clear();
			if(pEntity && pEntity->IsValid())
			{
				for(auto controller : m_vAttributes)
				{
					if(controller.second->OnHasComponent(*pEntity))
					{
						IComponentAttribute::TableMap mTables;
						controller.second->OnFillTable(mTables);

						auto& table = m_pTable->AddTable(controller.first);
						for(auto& row : mTables)
						{
							table.AddRow(row.first).AddChild(*row.second);
						}

						controller.second->OnSelectEntity(*pEntity);
					}
				}
			}
		}

		void EntityAttributeController::OnUpdateEntity(void)
		{
			for(auto& controller : m_vAttributes)
			{
				controller.second->OnUpdate();
			}
		}

	}
}
