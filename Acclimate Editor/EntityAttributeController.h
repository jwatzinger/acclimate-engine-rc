#pragma once
#include "Entity\Entity.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace gui
	{
		class MultiTable;
		class DockArea;
	}

	namespace editor
	{

		class IComponentAttribute;

		class EntityAttributeController :
			public gui::BaseController
		{
			typedef std::vector<std::pair<std::wstring, IComponentAttribute*>> AttributeVector;
		public:
			EntityAttributeController(gui::Module& module, gui::DockArea& dock);

			void AddComponent(const std::wstring& stName, IComponentAttribute& attributes);
			void RemoveComponent(const std::wstring& stName);

			void OnEntityChanged(void);
			void OnUpdateEntity(void);
			void OnSelectEntity(const ecs::EntityHandle* pEntity);
			
		private:
			
			ecs::EntityHandle m_entity;

			gui::MultiTable* m_pTable;

			AttributeVector m_vAttributes;
		};

	}
}


