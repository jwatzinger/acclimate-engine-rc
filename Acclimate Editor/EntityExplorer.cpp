#include "EntityExplorer.h"
#include "EntityTreeModel.h"
#include "ComponentAttachController.h"
#include "Entity\EntityClipboardData.h"
#include "Entity\EntityManager.h"
#include "Gui\TreeView.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\ContextMenu.h"

namespace acl
{
	namespace editor
	{

		EntityExplorer::EntityExplorer(gui::Module& module, gui::DockArea& dock, ecs::EntityManager& entityManager) :
			BaseController(module, L"../Editor/Menu/Entity/Explorer.axm"), m_pEntities(&entityManager)
		{
			dock.AddDock(*GetWidgetByName<gui::Dock>(L"Dock"), gui::DockType::RIGHT, false);

			m_pAttach = &AddController<ComponentAttachController>();

			m_pModel = new EntityTreeModel(entityManager, *m_pAttach);
			m_pModel->SigEntitySelected.Connect(this, &EntityExplorer::OnEntitySelected);
			m_pModel->SigGotoEntity.Connect(this, &EntityExplorer::OnGotoEntity);
			m_pModel->SigCopyEntity.Connect(this, &EntityExplorer::OnCopyEntity);
			m_pModel->SigCopyComponent.Connect(this, &EntityExplorer::CopyComponent);
			m_pModel->SigPaste.Connect(this, &EntityExplorer::OnPaste);
			m_pTree = GetWidgetByName<gui::TreeView>(L"Entities");
			m_pTree->SetModel(m_pModel);
			m_pTree->SigKey.Connect(this, &EntityExplorer::OnKey);

			auto& context = AddWidget<gui::ContextMenu>(0.02f);
			context.AddItem(L"Create entity").SigReleased.Connect(&SigCreateEntity, &core::Signal<>::operator());
			m_pTree->SetContextMenu(&context);
		}

		EntityExplorer::~EntityExplorer(void)
		{
			m_pTree->SetModel(nullptr);
			delete m_pModel;
		}

		ComponentAttachController& EntityExplorer::GetAttachController(void)
		{
			return *m_pAttach;
		}

		void EntityExplorer::PickEntity(const ecs::EntityHandle& entity)
		{
			SigEntitySelected(&entity);
		}

		void EntityExplorer::OnEntitiesChanged(void)
		{
			m_pModel->OnEntitiesChanged();
		}

		void EntityExplorer::DeleteEntity(const ecs::EntityHandle& entity)
		{
			m_pEntities->RemoveEntity(entity);
			m_pModel->OnEntitiesChanged();
			PickEntity(ecs::EntityHandle());
		}

		void EntityExplorer::CopyEntity(const ecs::Entity& entity)
		{
			m_pModule->m_clipboard.AddData<EntityClipboardData>(entity);
		}

		void EntityExplorer::CopyComponent(const ecs::BaseComponent& component)
		{
			m_pModule->m_clipboard.AddData<ComponentClipboardData>(component);
		}

		void EntityExplorer::PasteEntity(void)
		{
			if(m_pModule->ComboKeysActive(gui::ComboKeys::CTRL))
			{
				if(auto pData = m_pModule->m_clipboard.GetCurrentData<EntityClipboardData>())
					OnPasteEntity(pData->GetEntity());
				else if(auto pComponent = m_pModule->m_clipboard.GetCurrentData<ComponentClipboardData>())
					OnPasteComponent(pComponent->GetComponent());
			}
		}

		void EntityExplorer::Update(void)
		{
			m_pModel->Update();
		}

		void EntityExplorer::OnEntitySelected(const ecs::EntityHandle* pEntity)
		{
			m_entity = *pEntity;

			SigEntitySelected(pEntity);
		}

		void EntityExplorer::OnGotoEntity(const ecs::Entity& entity)
		{
			SigGotoEntity(entity);
		}

		void EntityExplorer::OnCopyEntity(const ecs::EntityHandle& entity)
		{
			ACL_ASSERT(entity);

			CopyEntity(entity);
		}

		void EntityExplorer::OnKey(gui::Keys key)
		{
			switch(key)
			{
			case gui::Keys::V:
				PasteEntity();
				break;
			}
		}

		void EntityExplorer::OnPasteEntity(const ecs::Entity& entity)
		{
			auto& e = m_pEntities->CreateEntity(L"");
			*e = entity;

			OnEntitiesChanged();
			SigEntitySelected(&e);
		}

		void EntityExplorer::OnPasteComponent(const ecs::BaseComponent& component)
		{
			if(m_entity)
				auto& comp = m_entity->AttachComponent(component);
		}

		void EntityExplorer::OnPaste(ecs::EntityHandle& entity)
		{
			if(auto pComponent = m_pModule->m_clipboard.GetCurrentData<ComponentClipboardData>())
			{
				ACL_ASSERT(entity);

				auto& component = pComponent->GetComponent();
				auto& comp = entity->AttachComponent(component);

				if(&component != &comp)
					OnEntitiesChanged();
			}
		}

	}
}

