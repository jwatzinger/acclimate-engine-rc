#pragma once
#include "Entity\Entity.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gui
	{
		class TreeView;
		class DockArea;
	}

	namespace editor
	{

		class EntityTreeModel;
		class ComponentAttachController;

		class EntityExplorer :
			public gui::BaseController
		{
		public:
			EntityExplorer(gui::Module& module, gui::DockArea& dock, ecs::EntityManager& entityManager);
			~EntityExplorer(void);

			ComponentAttachController& GetAttachController(void);

			void PickEntity(const ecs::EntityHandle& entity);
			void OnEntitiesChanged(void);
			void DeleteEntity(const ecs::EntityHandle& entity);
			void CopyEntity(const ecs::Entity& entity);
			void CopyComponent(const ecs::BaseComponent& component);
			void PasteEntity(void);

			void Update(void) override;

			core::Signal<const ecs::EntityHandle*> SigEntitySelected;
			core::Signal<const ecs::Entity&> SigGotoEntity;
			core::Signal<> SigCreateEntity;

		private:

			void OnEntitySelected(const ecs::EntityHandle* pEntity);
			void OnGotoEntity(const ecs::Entity& entity);
			void OnCopyEntity(const ecs::EntityHandle& entity);
			void OnPasteEntity(const ecs::Entity& entity);
			void OnCopyComponent(const ecs::BaseComponent& component);
			void OnPasteComponent(const ecs::BaseComponent& component);
			void OnPaste(ecs::EntityHandle& entity);

			void OnKey(gui::Keys key);

			ecs::EntityManager* m_pEntities;
			ecs::EntityHandle m_entity;

			EntityTreeModel* m_pModel;
			gui::TreeView* m_pTree;

			ComponentAttachController* m_pAttach;
		};

	}
}


