#include "EntityModule.h"
#include "AddEntityController.h"
#include "EntityAttributeController.h"
#include "EntityExplorer.h"
#include "ComponentRegistry.h"
#include "ComponentAttachController.h"
#include "Entity\Loader.h"
#include "Entity\Saver.h"
#include "Entity\EntityManager.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Gui\DockArea.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		EntityModule::EntityModule(gui::BaseController& controller, gui::MenuBar& bar, gui::DockArea& dock, ecs::EntityManager& entities):
			m_pEntities(&entities), m_pState(nullptr)
		{
			/****************************************************
			* ENTITY
			****************************************************/

			auto& entityItem = bar.AddItem(L"ENTITIES");

			// add entity
			auto& addEntity = controller.AddController<AddEntityController>(entities);

			auto& addEntityOption = entityItem.AddOption(L"Add entity");
			addEntityOption.SigReleased.Connect(&addEntity, &AddEntityController::OnToggle);

			auto& emptyEntity = entityItem.AddOption(L"Empty entity");
			emptyEntity.SigReleased.Connect(&addEntity, &AddEntityController::OnCreateEmpty);
			emptyEntity.SetShortcut(gui::ComboKeys::CTRL_ALT, 'X', gui::ShortcutState::ALWAYS);
			emptyEntity.SigShortcut.Connect(&addEntity, &AddEntityController::OnCreateEmpty);
			
			auto& combinedEntity = entityItem.AddOption(L"Combined entity");
			combinedEntity.SigReleased.Connect(&addEntity, &AddEntityController::OnCreateCombined);
			combinedEntity.SetShortcut(gui::ComboKeys::CTRL_ALT, 'C', gui::ShortcutState::ALWAYS);
			combinedEntity.SigShortcut.Connect(&addEntity, &AddEntityController::OnCreateCombined);

			// entity attributes
			auto& attributes = controller.AddController<EntityAttributeController>(dock);

			// entity explorer
			m_pEntityExplorer = &controller.AddController<EntityExplorer>(dock, entities);
			m_pEntityExplorer->SigEntitySelected.Connect(&attributes, &EntityAttributeController::OnSelectEntity);
			addEntity.SigEntityAdded.Connect(m_pEntityExplorer, &EntityExplorer::OnEntitiesChanged);
			m_pEntityExplorer->GetAttachController().SigAttached.Connect(&attributes, &EntityAttributeController::OnEntityChanged);
			m_pEntityExplorer->SigCreateEntity.Connect(&addEntity, &AddEntityController::OnToggle);

			auto& explorer = entityItem.AddOption(L"Entity-explorer");
			explorer.SigReleased.Connect(m_pEntityExplorer, &EntityExplorer::OnToggle);
			explorer.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'E', gui::ShortcutState::ALWAYS);
			explorer.SigShortcut.Connect(m_pEntityExplorer, &EntityExplorer::OnToggle);

			m_pComponentRegistry = new ComponentRegistry(addEntity.GetCombineController(), m_pEntityExplorer->GetAttachController(), attributes);
		}

		EntityModule::~EntityModule(void)
		{
			delete m_pComponentRegistry;
		}

		EntityExplorer& EntityModule::GetEntityExplorer(void)
		{
			return *m_pEntityExplorer;
		}

		IComponentRegistry& EntityModule::GetComponentRegistry(void)
		{
			return *m_pComponentRegistry;
		}

		Project::ModuleType EntityModule::GetModuleType(void) const
		{
			return Project::ModuleType::ENTITIES;
		}

		void EntityModule::Update(void)
		{
		}

		void EntityModule::OnNew(const std::wstring& stFile)
		{
		}

		void EntityModule::OnLoad(const std::wstring& stFile)
		{
			m_pEntities->Clear();

			ecs::Loader loader(*m_pEntities);
			loader.Load(stFile);

			m_pEntityExplorer->OnEntitiesChanged();
		}

		void EntityModule::OnSave(const std::wstring& stFile)
		{
		}

		void EntityModule::OnNewScene(const std::wstring& stScene, const std::wstring& stFile)
		{
			xml::Doc doc;

			doc.InsertNode(L"World");

			doc.SaveFile(stFile);
		}

		void EntityModule::OnSceneChanged(void)
		{
			m_pEntityExplorer->OnEntitiesChanged();
		}

		void EntityModule::OnSaveScene(const std::wstring& stFile)
		{
			ecs::Saver saver(*m_pEntities);
			saver.Save(stFile);
		}

		void EntityModule::OnClose(void)
		{
			m_pEntities->Clear();

			m_pEntityExplorer->OnEntitiesChanged();
		}

		void EntityModule::OnBeginTest(void)
		{
			ACL_ASSERT(!m_pState);
			m_pState = &m_pEntities->ChangeState();
		}

		void EntityModule::OnEndTest(void)
		{
			ACL_ASSERT(m_pState);
			m_pEntities->RevertState(*m_pState);
			m_pState = nullptr;
		}


	}
}

