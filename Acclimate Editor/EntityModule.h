#pragma once
#include <string>
#include "IModule.h"

namespace acl
{
	namespace gui
	{
		class MenuBar;
		class BaseController;
		class DockArea;
	}

	namespace ecs
	{
		class EntityManager;
		class EntityState;
	}

	namespace editor
	{

		class ComponentCombineController;
		class ComponentAttachController;
		class EntityExplorer;
		class IComponentRegistry;

		class EntityModule :
			public IModule
		{
		public:
			EntityModule(gui::BaseController& controller, gui::MenuBar& bar, gui::DockArea& dock, ecs::EntityManager& entities);
			~EntityModule(void);

			EntityExplorer& GetEntityExplorer(void);
			IComponentRegistry& GetComponentRegistry(void);

			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override;
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			ecs::EntityManager* m_pEntities;
			ecs::EntityState* m_pState;

			EntityExplorer* m_pEntityExplorer;

			IComponentRegistry* m_pComponentRegistry;
		};

	}
}


