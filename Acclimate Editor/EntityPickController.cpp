#include "EntityPickController.h"
#include "Entity\EntityManager.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		EntityPickController::EntityPickController(gui::Module& module, const ecs::EntityManager& entities) : 
			BaseController(module, L"../Editor/Menu/Entity/Pick.axm"), m_pEntities(&entities), m_bPick(false)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"EntityWindow");
			m_pList = &AddWidget<EntityList>(0.0f, 0.0f, 0.4f, 0.9f, 0.02f);
			m_pList->SigConfirm.Connect(this, &EntityPickController::OnPick);

			GetWidgetByName(L"Pick")->SigReleased.Connect(this, &EntityPickController::OnPick);

			OnRefresh();
		}

		bool EntityPickController::Execute(ecs::EntityHandle* ppEntity, std::wstring* pName)
		{
			return Execute(L"", ppEntity, pName);
		}

		bool EntityPickController::Execute(const std::wstring& stDefault, ecs::EntityHandle* ppEntity, std::wstring* pName)
		{
			m_bPick = false;
			m_pList->OnFocus(true);

			if(!stDefault.empty())
				m_pList->SelectByName(stDefault);

			m_pWindow->Execute();
			if(m_bPick)
			{
				if(ppEntity)
					*ppEntity = m_pList->GetContent();
				if(pName)
					*pName = m_pList->GetSelectedName();
			}
			return m_bPick;
		}

		void EntityPickController::OnRefresh(void)
		{
			m_pList->Clear();

			for(auto& entity : m_pEntities->AllEntities())
			{
				m_pList->AddItem(entity->GetName(), entity);
			}

			if(m_pList->GetSize())
				m_pList->SelectById(0);
		}

		void EntityPickController::OnPick(void)
		{
			m_bPick = true;
			m_pWindow->OnClose();
		}

	}
}

