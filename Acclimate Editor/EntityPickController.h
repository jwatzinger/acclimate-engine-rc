#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class EntityPickController :
			public gui::BaseController
		{
			typedef gui::List<ecs::EntityHandle> EntityList;
		public:
			EntityPickController(gui::Module& module, const ecs::EntityManager& entities);

			bool Execute(ecs::EntityHandle* ppEntity, std::wstring* pName);
			bool Execute(const std::wstring& stDefault, ecs::EntityHandle* ppEntity, std::wstring* pName);

			void OnRefresh(void);

		private:

			void OnPick(void);

			bool m_bPick;

			EntityList* m_pList;
			gui::BaseWindow* m_pWindow;

			const ecs::EntityManager* m_pEntities;
		};

	}
}


