#include "EntityTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		EntityTreeCallback::EntityTreeCallback(const ecs::EntityHandle& entity) : m_entity(entity)
		{
		}

		const std::wstring* EntityTreeCallback::GetIconName(void) const
		{
			static const std::wstring stIcon = L"EntityIcon";
			return &stIcon;
		}

		bool EntityTreeCallback::IsRenameable(void) const
		{
			return true;
		}

		bool EntityTreeCallback::IsDeletable(void) const
		{
			return true;
		}

		bool EntityTreeCallback::IsDefaultExpanded(void) const
		{
			return true;
		}

		void EntityTreeCallback::OnSelect(void)
		{
			SigSelect(m_entity);
		}

		void EntityTreeCallback::OnGoto(void)
		{
			SigGoto(m_entity);
		}

		void EntityTreeCallback::OnDelete(void)
		{
			SigDelete(m_entity);
		}

		gui::ContextMenu* EntityTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);

			// attach component
			auto& attach = pEntityMenu->AddItem(L"Attach component");
			attach.SigReleased.Connect(this, &EntityTreeCallback::OnAttach);
			attach.SigShortcut.Connect(this, &EntityTreeCallback::OnAttach);
			attach.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'A', gui::ShortcutState::ALWAYS);
			pEntityMenu->InsertSeperator();

			// goto
			auto& gotoItem = pEntityMenu->AddItem(L"Goto");
			gotoItem.SigReleased.Connect(this, &EntityTreeCallback::OnGoto);
			gotoItem.SigShortcut.Connect(this, &EntityTreeCallback::OnGoto);
			gotoItem.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'G', gui::ShortcutState::ALWAYS);
			pEntityMenu->InsertSeperator();

			// properties 
			auto& copy = pEntityMenu->AddItem(L"Copy");
			copy.SigReleased.Connect(this, &EntityTreeCallback::OnCopy);

			auto& paste = pEntityMenu->AddItem(L"Paste");
			paste.SigReleased.Connect(this, &EntityTreeCallback::OnPaste);

			pEntityMenu->AddItem(L"Properties");
			return pEntityMenu;
		}

		void EntityTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void EntityTreeCallback::OnAttach(void)
		{
			if(m_entity.IsValid())
				SigAttach(m_entity);
		}

		void EntityTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(m_entity, stName);
		}
		
		void EntityTreeCallback::OnCopy(void)
		{
			SigCopy(m_entity);
		}

		void EntityTreeCallback::OnPaste(void)
		{
			SigPaste(m_entity);
		}

	}
}