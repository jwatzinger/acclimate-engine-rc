#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace editor
	{

		class EntityTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			EntityTreeCallback(const ecs::EntityHandle& entity);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<ecs::EntityHandle&> SigSelect;
			core::Signal<const ecs::EntityHandle&> SigCopy;
			core::Signal<ecs::Entity&> SigGoto;
			core::Signal<ecs::Entity&> SigAttach;
			core::Signal<ecs::EntityHandle&> SigDelete;
			core::Signal<ecs::Entity&, const std::wstring&> SigRename;
			core::Signal<ecs::EntityHandle&> SigPaste;

		private:

			void OnAttach(void);
			void OnGoto(void);
			void OnCopy(void);
			void OnPaste(void);

			ecs::EntityHandle m_entity;
		};

	}
}


