#include "EntityTreeModel.h"
#include "EntityTreeCallback.h"
#include "ComponentTreeCallback.h"
#include "ComponentAttachController.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\EntityManager.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace editor
	{

		EntityTreeModel::EntityTreeModel(ecs::EntityManager& entities, ComponentAttachController& attach) : m_root(L"World"),
			m_pEntities(&entities), m_bDirty(false), m_pAttach(&attach)
		{
		}

		const gui::Node& EntityTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& EntityTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void EntityTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& entity : m_pEntities->AllEntities())
				{
					gui::Node entityNode(entity->GetName());
					auto pCallback = new EntityTreeCallback(entity);
					pCallback->SigDelete.Connect(this, &EntityTreeModel::OnDeleteEntity);
					pCallback->SigAttach.Connect(this, &EntityTreeModel::OnAttachComponents);
					pCallback->SigRename.Connect(this, &EntityTreeModel::OnRenameEntity);
					pCallback->SigSelect.Connect(this, &EntityTreeModel::OnSelectEntity);
					pCallback->SigGoto.Connect(this, &EntityTreeModel::OnGotoEntity);
					pCallback->SigCopy.Connect(&SigCopyEntity, &core::Signal<const ecs::EntityHandle&>::operator());
					pCallback->SigPaste.Connect(&SigPaste, &core::Signal<ecs::EntityHandle&>::operator());
					entityNode.pCallback = pCallback;

					for(auto& ids : ecs::ComponentRegistry::GetIdMap())
					{
						if(auto pComponent = entity->GetComponent(ids.second))
						{
							gui::Node component(ids.first);
							auto pComponentCallback = new ComponentTreeCallback(entity, *pComponent);
							pComponentCallback->SigDelete.Connect(this, &EntityTreeModel::OnEntitiesChanged);
							pComponentCallback->SigSelect.Connect(this, &EntityTreeModel::OnSelectComponent);
							pComponentCallback->SigCopy.Connect(&SigCopyComponent, &core::Signal<const ecs::BaseComponent&>::operator());
							component.pCallback = pComponentCallback;
							entityNode.vChilds.push_back(component);
						}		
					}

					m_root.vChilds.push_back(entityNode);
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void EntityTreeModel::OnEntitiesChanged(void)
		{
			m_bDirty = true;
		}

		void EntityTreeModel::OnSelectEntity(ecs::EntityHandle& entity)
		{
			SigEntitySelected(&entity);
		}

		void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		{
			SigGotoEntity(entity);
		}

		void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		{
			SigEntitySelected(&entity);
		}

		void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		{
			m_pEntities->RemoveEntity(entity);
			SigEntitySelected(nullptr);
			m_bDirty = true;
		}

		void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		{
			if(m_pAttach->Execute(entity))
				m_bDirty = true;
		}

		void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		{
			if(entity.GetName() != stName)
			{
				entity.SetName(stName);
				m_bDirty = true;
			}
		}


	}
}

