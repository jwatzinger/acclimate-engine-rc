#pragma once
#include "Entity\Entity.h"
#include "Gui\ITreeModel.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
		struct BaseComponent;
	}

	namespace editor
	{
		
		class ComponentAttachController;

		class EntityTreeModel :
			public gui::ITreeModel
		{
		public:
			EntityTreeModel(ecs::EntityManager& entities, ComponentAttachController& attach);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnEntitiesChanged(void);

			core::Signal<const ecs::EntityHandle*> SigEntitySelected;
			core::Signal<const ecs::Entity&> SigGotoEntity;
			core::Signal<const ecs::EntityHandle&> SigCopyEntity;
			core::Signal<const ecs::BaseComponent&> SigCopyComponent;
			core::Signal<ecs::EntityHandle&> SigPaste;

		private:

			void OnSelectEntity(ecs::EntityHandle& entity);
			void OnGotoEntity(ecs::Entity& entity);
			void OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component);
			void OnDeleteEntity(ecs::EntityHandle& entity);
			void OnAttachComponents(ecs::Entity& entity);
			void OnRenameEntity(ecs::Entity& entity, const std::wstring& stName);

			bool m_bDirty;

			gui::Node m_root;

			ecs::EntityManager* m_pEntities;

			core::Signal<> SigUpdated;

			ComponentAttachController* m_pAttach;
		};

	}
}


