#include "AddVariableController.h"
#include "Core\EventAttribute.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddVariableController::AddVariableController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Events/AddVariable.axm"),
			m_create(false)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"New Variable");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddVariableController::OnCreate);

			// type
			m_pType = &AddWidget<TypeBox>(0.0f, 58.0f, 1.0f, 21.0f, 10.0f);
			m_pType->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pType->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pType->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pType->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			fillTypeBox(*m_pType);

			// array
			m_pArray = GetWidgetByName<gui::CheckBox>(L"Array");

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddVariableController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddVariableController::OnCreate);
		}

		AddVariableController::AddVariableController(gui::Module& module, core::AttributeType type, bool isArray) : AddVariableController(module)
		{
			m_pType->SelectByItem(std::make_pair(type, -1));
			m_pType->OnDisable();

			m_pArray->OnCheck(isArray);
			m_pArray->OnDisable();
		}

		AddVariableController::AddVariableController(gui::Module& module, unsigned int objectType, bool isArray) : AddVariableController(module)
		{
			m_pType->SelectByItem(std::make_pair(core::AttributeType::OBJECT, -1));
			m_pType->OnDisable();

			m_pArray->OnCheck(isArray);
			m_pArray->OnDisable();
		}

		AddVariableController::~AddVariableController()
		{
		}

		bool AddVariableController::Execute(std::wstring* pName, core::AttributeType* pType, unsigned int* pObjectType, bool* pArray, bool* pConst)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();
				auto& type = m_pType->GetContent();
				if(pType)
					*pType = type.first;
				if(pObjectType)
					*pObjectType = type.second;
				if(pArray)
					*pArray = m_pArray->IsChecked();
				if(pConst)
					*pConst = false;
				return true;
			}
			else
				return false;
		}

		void AddVariableController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddVariableController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
