#pragma once
#include "TypeBox.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"
#include "Gui\ComboBox.h"

namespace acl
{
	namespace core
	{
		enum class AttributeType;
	}

	namespace gui
	{
		class Window;
		class CheckBox;
	}

	namespace editor
	{
		
		class AddVariableController :
			public gui::BaseController
		{
		public:
			AddVariableController(gui::Module& module);
			AddVariableController(gui::Module& module, core::AttributeType type, bool isArray);
			AddVariableController(gui::Module& module, unsigned int objectType, bool isArray);
			~AddVariableController();

			bool Execute(std::wstring* pName, core::AttributeType* pType, unsigned int* pObjectType, bool* pArray, bool* pConst);

		private:

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;

			gui::Textbox<>* m_pBox;
			TypeBox* m_pType;
			gui::CheckBox* m_pArray;
			gui::Window* m_pWindow;
		};

	}
}

