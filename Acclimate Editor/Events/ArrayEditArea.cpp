#include "ArrayEditArea.h"
#include "Core\EventAttribute.h"
#include "Gui\Table.h"
#include "Gui\TableItem.h"
#include "Gui\Textbox.h"
#include "Gui\ScrollArea.h"
#include "Gui\Checkbox.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		ArrayEditArea::ArrayEditArea(gui::Module& module, gui::Widget& parent, core::EventAttribute& attribute) : BaseController(module, parent, L"../Editor/Menu/Events/ArrayEdit.axm"),
			m_pAttribute(&attribute), m_focusId(-1)
		{
			ACL_ASSERT(attribute.IsArray());

			// table
			m_pTable = GetWidgetByName<gui::Table>(L"Table");
			m_pTable->SigResize.Connect(this, &ArrayEditArea::OnTableResize);

			// scrollarea
			m_pArea = GetWidgetByName<gui::ScrollArea>(L"Area");
			m_pArea->SetAreaHeight(m_pTable->GetItemAreaSize());

			switch(attribute.GetType())
			{
			case core::AttributeType::BOOL:
				for(auto value : m_pAttribute->GetValueArray<bool>())
				{
					AddTableRow(value);
				}
				break;
			case core::AttributeType::INT:
				for(auto value : m_pAttribute->GetValueArray<int>())
				{
					AddTableRow(conv::ToString(value));
				}
				break;
			case core::AttributeType::FLOAT:
				for(auto value : m_pAttribute->GetValueArray<float>())
				{
					AddTableRow(conv::ToString(value));
				}
				break;
			case core::AttributeType::STRING:
				for(auto& stValue : m_pAttribute->GetValueArray<std::wstring>())
				{
					AddTableRow(stValue);
				}
				break;
			case core::AttributeType::OBJECT:
				for(unsigned int i = 0; i < m_pAttribute->GetValueArray().Size(); i++)
				{
					AddTableRow();
				}
				break;
			default:
				ACL_ASSERT(false);
			}

			// add, remove, clear
			auto pPlus = GetWidgetByName(L"+");
			pPlus->SigReleased.Connect(this, &ArrayEditArea::OnAddRow);

			m_pMinus = GetWidgetByName(L"-");
			m_pMinus->SigReleased.Connect(this, &ArrayEditArea::OnRemoveRow);
			if(m_pTable->GetNumItems() <= 1)
				m_pMinus->OnDisable();

			auto pClear = GetWidgetByName(L"X");
			pClear->SigReleased.Connect(this, &ArrayEditArea::OnClearTable);
		}

		ArrayEditArea::~ArrayEditArea(void)
		{
			for(auto pData : m_vData)
			{
				delete pData;
			}
		}

		core::EventAttribute& ArrayEditArea::GetAttribute(void)
		{
			return *m_pAttribute;
		}

		void ArrayEditArea::ApplySelected(void)
		{
			if(m_focusId != -1 && m_pAttribute->GetType() != core::AttributeType::OBJECT)
			{
				auto pWidget = (gui::Textbox<>*)m_vData[m_focusId]->GetWidget();
				OnChangeValue(pWidget->GetContent());
			}
		}

		void ArrayEditArea::AddTableRow(bool value)
		{
			ACL_ASSERT(m_pAttribute->GetType() == core::AttributeType::BOOL);

			const auto id = m_pTable->GetNumItems();
			auto& item = m_pTable->AddRow(conv::ToString(m_pTable->GetNumItems()));

			auto& checkbox = AddWidget<gui::CheckBox>(0.0f, 0.0f, 1.0f);
			checkbox.OnCheck(value);

			// data container - aquire focus data
			auto* pData = new RowData(checkbox, checkbox.SigChecked, id);
			pData->SigAccess.Connect(this, &ArrayEditArea::OnChangeValue);
			m_vData.push_back(pData);

			item.AddChild(checkbox);
			checkbox.OnFocus(true);
		}

		void ArrayEditArea::AddTableRow(const std::wstring& stValue)
		{
			ACL_ASSERT(m_pAttribute->GetType() != core::AttributeType::BOOL);

			const auto id = m_pTable->GetNumItems();
			auto& item = m_pTable->AddRow(conv::ToString(m_pTable->GetNumItems()));

			auto& textbox = AddWidget<gui::Textbox<>>(0.0f, 0.0f, 1.0f, 1.0f, stValue);
			textbox.SigContentSet.Connect(this, &ArrayEditArea::OnChangeValue);

				// data container - aquire focus data
			auto* pData = new RowData(textbox, textbox.SigFocus, id);
			pData->SigAccess.Connect(this, &ArrayEditArea::OnRowFocus);
			m_vData.push_back(pData);

			item.AddChild(textbox);
			textbox.OnFocus(true);
		}

		void ArrayEditArea::AddTableRow(void)
		{
			ACL_ASSERT(m_pAttribute->GetType() == core::AttributeType::OBJECT);

			const auto id = m_pTable->GetNumItems();
			auto& item = m_pTable->AddRow(conv::ToString(m_pTable->GetNumItems()));
		}

		template<typename Type>
		void setTextFromAttribute(gui::Textbox<>& box, core::EventAttribute& attribute, unsigned int row)
		{
			box.SetText(conv::ToString(attribute.GetValueArray<Type>()[row]));
		}

		void ArrayEditArea::SetRowValue(unsigned int row)
		{
			ACL_ASSERT(row < m_vData.size());

			auto* pData = m_vData.at(row);
			auto* pTextbox = (gui::Textbox<>*)pData->GetWidget();

			switch(m_pAttribute->GetType())
			{
			case core::AttributeType::BOOL:
				setTextFromAttribute<bool>(*pTextbox, *m_pAttribute, row);
				break;
			case core::AttributeType::FLOAT:
				setTextFromAttribute<float>(*pTextbox, *m_pAttribute, row);
				break;
			case core::AttributeType::INT:
				setTextFromAttribute<int>(*pTextbox, *m_pAttribute, row);
				break;
			case core::AttributeType::STRING:
				pTextbox->SetText(m_pAttribute->GetValueArray<std::wstring>()[row]);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void ArrayEditArea::OnAddRow(void)
		{
			switch(m_pAttribute->GetType())
			{
			case core::AttributeType::BOOL:
				m_pAttribute->GetValueArray<bool>().push_back(false);
				AddTableRow(false);
				break;
			case core::AttributeType::FLOAT:
				m_pAttribute->GetValueArray<float>().push_back(0.0f);
				AddTableRow(std::wstring(L"0.0f"));
				break;
			case core::AttributeType::INT:
				m_pAttribute->GetValueArray<int>().push_back(0);
				AddTableRow(std::wstring(L"0"));
				break;
			case core::AttributeType::STRING:
				m_pAttribute->GetValueArray<std::wstring>().push_back(L"");
				AddTableRow(std::wstring(L""));
				break;
			case core::AttributeType::OBJECT:
				m_pAttribute->GetValueArray().PushBack();
				AddTableRow();
				break;
			default:
				ACL_ASSERT(false);
			}

			m_pMinus->OnEnable();
		}

		void ArrayEditArea::OnRemoveRow(void)
		{
			const auto numItems = m_pTable->GetNumItems();
			ACL_ASSERT(numItems != 0);
			m_pTable->RemoveRow(numItems - 1);

			if(m_focusId == numItems - 1)
				m_focusId = -1;

			switch(m_pAttribute->GetType())
			{
			case core::AttributeType::BOOL:
				m_pAttribute->GetValueArray<bool>().pop_back();
				break;
			case core::AttributeType::FLOAT:
				m_pAttribute->GetValueArray<float>().pop_back();
				break;
			case core::AttributeType::INT:
				m_pAttribute->GetValueArray<int>().pop_back();
				break;
			case core::AttributeType::STRING:
				m_pAttribute->GetValueArray<std::wstring>().pop_back();
				break;
			case core::AttributeType::OBJECT:
				m_pAttribute->GetValueArray().PopBack();
				break;
			default:
				ACL_ASSERT(false);
			}

			if(numItems == 1)
				m_pMinus->OnDisable();
		}

		void ArrayEditArea::OnClearTable(void)
		{
			m_pTable->Clear();

			switch(m_pAttribute->GetType())
			{
			case core::AttributeType::BOOL:
				m_pAttribute->GetValueArray<bool>().clear();
				break;
			case core::AttributeType::INT:
				m_pAttribute->GetValueArray<int>().clear();
				break;
			case core::AttributeType::FLOAT:
				m_pAttribute->GetValueArray<float>().clear();
				break;
			case core::AttributeType::STRING:
				m_pAttribute->GetValueArray<std::wstring>().clear();
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void ArrayEditArea::OnRowFocus(unsigned int id, bool hasFocus)
		{
			ACL_ASSERT(m_pAttribute->GetType() != core::AttributeType::BOOL);

			if(hasFocus)
				m_focusId = id;
			else if(id == m_focusId)
			{
				if((unsigned int)m_focusId < m_pTable->GetNumItems())
				{
					SetRowValue(id);
					m_focusId = -1;
				}
			}
		}

		template<typename Type>
		void changeArrayValue(core::EventAttribute& attribute, unsigned int id, const std::wstring& stValue)
		{
			ACL_ASSERT(id < attribute.GetValueArray<Type>().size());

			attribute.GetValueArray<Type>()[id] = conv::FromString<Type>(stValue);
		}

		void ArrayEditArea::OnChangeValue(std::wstring stValue)
		{
			ACL_ASSERT(m_focusId != -1);

			switch(m_pAttribute->GetType())
			{
			case core::AttributeType::BOOL:
				ACL_ASSERT(false);
			case core::AttributeType::INT:
				changeArrayValue<int>(*m_pAttribute, m_focusId, stValue);
				break;
			case core::AttributeType::FLOAT:
				changeArrayValue<float>(*m_pAttribute, m_focusId, stValue);
				break;
			case core::AttributeType::STRING:
				m_pAttribute->GetValueArray<std::wstring>()[m_focusId] = stValue;
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void ArrayEditArea::OnChangeValue(unsigned int id, bool value)
		{
			ACL_ASSERT(m_pAttribute->GetType() == core::AttributeType::BOOL);

			auto& val = m_pAttribute->GetValueArray<bool>().at(id);

			ACL_ASSERT(val != value);
			val = value;
		}

		void ArrayEditArea::OnTableResize(size_t size)
		{
			m_pArea->SetAreaHeight(m_pTable->GetItemAreaSize());
		}

	}
}

