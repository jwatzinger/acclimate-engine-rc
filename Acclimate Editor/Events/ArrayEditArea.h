#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"

namespace acl
{
	namespace core
	{
		class EventAttribute;
	}

	namespace gui
	{
		class Table;
		class ScrollArea;
	}

	namespace editor
	{

		class ArrayEditArea :
			public gui::BaseController
		{
			typedef gui::DataContainer<unsigned int, gui::Widget, bool> RowData;
			typedef std::vector<RowData*> DataVector;
		public:
			ArrayEditArea(gui::Module& module, gui::Widget& parent, core::EventAttribute& attribute);
			~ArrayEditArea(void);

			core::EventAttribute& GetAttribute(void);

			void ApplySelected(void);

		private:

			void AddTableRow(bool value);
			void AddTableRow(const std::wstring& stValue);
			void AddTableRow(void);
			void SetRowValue(unsigned int row);

			void OnAddRow(void);
			void OnRemoveRow(void);
			void OnClearTable(void);
			void OnRowFocus(unsigned int id, bool hasFocus);
			void OnChangeValue(std::wstring stValue);
			void OnChangeValue(unsigned int, bool value);
			void OnTableResize(size_t size);

			gui::Table* m_pTable;
			gui::Widget* m_pMinus;
			gui::ScrollArea* m_pArea;
			DataVector m_vData;

			int m_focusId;
			core::EventAttribute* m_pAttribute;
		};

	}
}


