#include "ArrayViewController.h"
#include "ArrayEditArea.h"
#include "Core\EventAttribute.h"
#include "Gui\Window.h"

namespace acl
{
	namespace editor
	{

		ArrayViewController::ArrayViewController(gui::Module& module, core::EventAttribute& attribute) : BaseController(module, L"../Editor/Menu/Events/ArrayView.axm"),
			m_isConfirmed(false), m_pRealAttribute(&attribute)
		{
			auto& newAttribute = *new core::EventAttribute(attribute);
			m_pArray = &AddController<ArrayEditArea>(*GetMainWidget(), newAttribute);

			// window
			m_pWindow = GetWidgetByName<gui::Window>(L"Window");
			GetWidgetByName(L"OK")->SigReleased.Connect(this, &ArrayViewController::OnOK);
			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &ArrayViewController::OnCancel);
		}

		ArrayViewController::~ArrayViewController(void)
		{
			delete &m_pArray->GetAttribute();
		}

		void ArrayViewController::Execute(void)
		{
			m_isConfirmed = false;

			m_pWindow->Execute();

			if(m_isConfirmed)
				*m_pRealAttribute = m_pArray->GetAttribute();
		}

		void ArrayViewController::OnOK(void)
		{
			m_isConfirmed = true;
			m_pArray->ApplySelected();
			m_pWindow->OnClose();
		}

		void ArrayViewController::OnCancel(void)
		{
			m_isConfirmed = false;
			m_pWindow->OnClose();
		}

	}
}

