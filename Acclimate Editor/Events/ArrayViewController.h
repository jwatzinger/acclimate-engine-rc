#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"

namespace acl
{
	namespace core
	{
		class EventAttribute;
	}

	namespace gui
	{
		class Window;
		class Table;
	}

	namespace editor
	{

		class ArrayEditArea;

		class ArrayViewController :
			public gui::BaseController
		{
		public:
			ArrayViewController(gui::Module& module, core::EventAttribute& attribute);
			~ArrayViewController(void);

			void Execute(void);

		private:

			void OnOK(void);
			void OnCancel(void);

			gui::Window* m_pWindow;
			ArrayEditArea* m_pArray;

			bool m_isConfirmed;
			core::EventAttribute* m_pRealAttribute;
		};
	}
}

