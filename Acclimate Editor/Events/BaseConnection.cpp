#include "BaseConnection.h"
#include "BaseEvent.h"
#include "Core\EventGraph.h"
#include "Core\EventCommand.h"
#include "Gfx\ILine.h"
#include "Gfx\Color.h"

namespace acl
{
	namespace editor
	{

		ConnectionData::ConnectionData(BaseEvent& event, core::EventCommand& command, unsigned int offY, unsigned int slot) : pEvent(&event), 
			offY(offY), slot(slot), pCommand(&command)
		{
		}

		BaseConnection::BaseConnection(gfx::ILine& line, render::IStage& stage, Type type, const ConnectionData& left, const ConnectionData& right) :
			m_pLine(&line), m_pStage(&stage), m_type(type), m_isValid(true), m_left(left), m_right(right), m_beziers(0.05f), m_reverseBeziers(0.05f),
			m_isBeziersReversed(false)
		{
			left.pEvent->SigEraseOutConnection.Connect(this, &BaseConnection::OnEraseOutConnection);
			right.pEvent->SigEraseInConnection.Connect(this, &BaseConnection::OnEraseInConnection);

			UpdateSplines();
		}

		BaseConnection::~BaseConnection(void)
		{
			m_left.pEvent->SigEraseOutConnection.Connect(this, &BaseConnection::OnEraseOutConnection);
			m_right.pEvent->SigEraseInConnection.Connect(this, &BaseConnection::OnEraseInConnection);
		}

		void BaseConnection::SetOffset(const math::Vector2& vOffset)
		{
			m_vOffset = vOffset;
		}

		void BaseConnection::Update(void)
		{
			if(m_isValid)
				UpdateSplines();
		}

		gfx::Color connectionTypeToColor(BaseConnection::Type type)
		{
			switch(type)
			{
			case BaseConnection::Type::IO:
				return gfx::Color(255, 255, 255);
			case BaseConnection::Type::BOOL:
				return gfx::Color(255, 0, 0);
			case BaseConnection::Type::FLOAT:
				return gfx::Color(102, 255, 0);
			case BaseConnection::Type::INT:
				return gfx::Color(0, 255, 168);
			case BaseConnection::Type::STRING:
				return gfx::Color(255, 143, 0);
			case BaseConnection::Type::OBJECT:
				return gfx::Color(0, 65, 255);
			default:
				ACL_ASSERT(false);
			}

			return gfx::Color();
		}

		void BaseConnection::Draw(void) const
		{
			if(m_isValid)
			{
				m_pLine->SetColor(connectionTypeToColor(m_type));
				if(m_isBeziersReversed)
					m_pLine->DrawMulti(m_reverseBeziers.GetPoints(), *m_pStage);
				else
					m_pLine->DrawMulti(m_beziers.GetPoints(), *m_pStage);
			}
		}

		void BaseConnection::OnInvalidate(void)
		{
			m_isValid = false;
			SigInvalidate();
		}

		BaseConnection::Type BaseConnection::GetType(void) const
		{
			return m_type;
		}

		void BaseConnection::UpdateSplines(void)
		{
			const auto vLeftPosition = GetLeftPosition() + math::Vector2(m_left.pEvent->GetWidth(), m_left.offY) + m_vOffset;
			const auto vRightPosition = GetRightPosition() + math::Vector2(0, m_right.offY) + m_vOffset;

			const auto distanceX = (vLeftPosition.x - vRightPosition.x) / 2;

			if(distanceX <= 0)
			{
				const math::Vector3 points[4] =
				{
					{ (float)vLeftPosition.x, (float)vLeftPosition.y, 0.6f },
					{ (float)vLeftPosition.x - distanceX, (float)vLeftPosition.y, 0.6f },
					{ (float)vRightPosition.x + distanceX, (float)vRightPosition.y, 0.6f },
					{ (float)vRightPosition.x, (float)vRightPosition.y, 0.6f }
				};

				m_beziers.SetPoints(points);

				m_isBeziersReversed = false;
			}
			else
			{
				const auto offsetX = distanceX / 2;
				const auto offsetY = (vRightPosition.y - vLeftPosition.y) / 16;

				const math::Vector3 points[6] =
				{
					{ (float)vLeftPosition.x, (float)vLeftPosition.y, 0.6f },
					{ (float)vLeftPosition.x + offsetX, (float)vLeftPosition.y + offsetY, 0.6f },
					{ (float)vLeftPosition.x - distanceX, (float)vLeftPosition.y, 0.6f },
					{ (float)vRightPosition.x + distanceX, (float)vRightPosition.y, 0.6f },
					{ (float)vRightPosition.x - offsetX, (float)vRightPosition.y - offsetY, 0.6f },
					{ (float)vRightPosition.x, (float)vRightPosition.y, 0.6f }
				};

				m_reverseBeziers.SetPoints(points);

				m_isBeziersReversed = true;
			}
		}

		const ConnectionData& BaseConnection::GetRightData(void) const
		{
			return m_right;
		}

		math::Vector2 BaseConnection::GetLeftPosition(void) const
		{
			return m_left.pCommand->GetPosition();
		}

		math::Vector2 BaseConnection::GetRightPosition(void) const
		{
			return m_right.pCommand->GetPosition();
		}

		void BaseConnection::OnEraseOutConnection(const SlotData& data)
		{
			if(m_left.slot == data.slot)
			{
				if(GetType() == Type::IO)
				{
					if(data.type == core::ConnectionType::OUTPUT)
					{
						auto pOutput = m_left.pCommand->QueryOutput();
						ACL_ASSERT(pOutput);
						pOutput->SetOutput(data.slot, core::EventCommand::NO_EVENT_ID);
						OnInvalidate();
					}
				}
				else if(data.type == core::ConnectionType::RETURN)
				{
					auto pReturn = m_left.pCommand->QueryReturn();
					pReturn->RemoveReturn(data.slot, m_right.pCommand->GetId());
					OnInvalidate();

					// set attribute connection
					auto pAttributes = m_right.pCommand->QueryAttribute();
					ACL_ASSERT(pAttributes);
					pAttributes->ConnectAttribute(m_right.slot, false);
				}
			}
		}

		void BaseConnection::OnEraseInConnection(const SlotData& data)
		{
			if(m_right.slot == data.slot)
			{
				if(GetType() == Type::IO)
				{
					if(data.type == core::ConnectionType::INPUT)
					{
						auto pOutput = m_left.pCommand->QueryOutput();
						ACL_ASSERT(pOutput);

						pOutput->SetOutput(m_left.slot, core::EventCommand::NO_EVENT_ID);
						OnInvalidate();
					}
				}
				else if(data.type == core::ConnectionType::ATTRIBUTE)
				{
					const auto id = m_left.pCommand->GetId();

					auto pReturn = m_left.pCommand->QueryReturn();
					ACL_ASSERT(pReturn);
					pReturn->RemoveReturn(m_left.slot, m_right.pCommand->GetId());
					OnInvalidate();

					// set attribute connection
					auto pAttributes = m_right.pCommand->QueryAttribute();
					ACL_ASSERT(pAttributes);
					pAttributes->ConnectAttribute(m_right.slot, false);
				}
			}
		}

	}
}

