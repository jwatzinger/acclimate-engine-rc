#pragma once
#include "Core\Signal.h"
#include "Gfx\Beziers.h"
#include "Math\Vector.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace core
	{
		class EventCommand;
	}

	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{
		class BaseEvent;
		struct SlotData;

		// TODO: find out why this is necessary
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;

		struct ConnectionData
		{
			ConnectionData(BaseEvent& event, core::EventCommand& command, unsigned int offY, unsigned int slot);

			BaseEvent* pEvent;
			core::EventCommand* pCommand;
			unsigned int offY, slot;
		};

		class BaseConnection
		{
			typedef gfx::Beziers<math::Vector3, 4> Bezier;
			typedef gfx::Beziers<math::Vector3, 6> ReverseBezier;
		public:

			enum class Type
			{
				IO, BOOL, INT, FLOAT, STRING, OBJECT
			};

			BaseConnection(gfx::ILine& line, render::IStage& stage, Type type, const ConnectionData& left, const ConnectionData& right);
			virtual ~BaseConnection(void);

			void SetOffset(const math::Vector2& vOffset);

			void Update(void);
			void Draw(void) const;

			core::Signal<> SigInvalidate;

		protected:

			void OnInvalidate(void);

			Type GetType(void) const;

			void UpdateSplines(void);

			const ConnectionData& GetRightData(void) const;

		private:

			math::Vector2 GetLeftPosition(void) const;
			math::Vector2 GetRightPosition(void) const;

			void OnEraseOutConnection(const SlotData& data);
			void OnEraseInConnection(const SlotData& data);

			Bezier m_beziers;
			ReverseBezier m_reverseBeziers;
			gfx::ILine* m_pLine;
			render::IStage* m_pStage;

			bool m_isValid, m_isBeziersReversed;
			math::Vector2 m_vOffset;
			Type m_type;
			ConnectionData m_left, m_right;
		};

	}
}


