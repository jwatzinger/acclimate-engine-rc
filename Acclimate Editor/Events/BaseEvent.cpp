#include "BaseEvent.h"
#include "ConnectionDragDropHandler.h"
#include "ArrayViewController.h"
#include "EventInputBox.h"
#include "EventUserDataIO.h"
#include "ClipboardData.h"
#include "Core\EventGraph.h"
#include "Core\EventAttribute.h"
#include "Core\EventCommand.h"
#include "Core\EventInstance.h"
#include "Gui\Window.h"
#include "Gui\Input.h"
#include "Gui\Icon.h"
#include "Gui\ContextMenu.h"
#include "Gui\Label.h"
#include "System\Clipboard.h"

namespace acl
{
	namespace editor
	{

		SlotData::SlotData(core::ConnectionType type, unsigned int slot) : type(type), slot(slot)
		{
		}

		AttributeArrayData::AttributeArrayData(core::AttributeType type, unsigned int slot) : type(type), slot(slot)
		{
		}

		const unsigned int BORDER = 8;
		const unsigned int MIDSECTION = 64;
		const unsigned int IO_SIZE = 16;
		const math::Rect rIcon(0, 0, 36, 36);

		BaseEvent::BaseEvent(gui::Module& module, gui::Widget& parent, core::EventInstance& instance, core::EventCommand& command, const core::EventGraph& graph, ConnectionDragDropHandler& handler) : gui::BaseController(module, parent, L"../Editor/Menu/Events/Widget.axm"),
			m_pHandler(&handler), m_pInstance(&instance), m_inWidth(BORDER), m_outWidth(BORDER), m_inPosY(BORDER), m_outPosY(BORDER), m_topOffset(24), m_pGraph(&graph), m_pEvent(&command)
		{
			// modify window
			auto pMain = GetWidgetByName<gui::Window>(L"Window");
			pMain->SigKey.Connect(this, &BaseEvent::OnKey);
			pMain->SigDrag.Connect(this, &BaseEvent::OnDragWindow);

			const auto id = m_pEvent->GetId();
			const auto targetType = m_pEvent->GetTargetType();
			auto& connections = graph.GetConnections(id);

			// input
			if(auto pInput = m_pEvent->QueryInput())
			{
				const bool isConnected = connections.IsConnected(core::ConnectionType::INPUT, 0);
				auto pUserData = new EventUserDataInput(id, targetType);
				AddInput(isConnected, L"", 0, pUserData);
			}

			// 	attributes
			if(auto pAttributes = m_pEvent->QueryAttribute())
			{
				unsigned int slot = 0;
				pAttributes->GetNumAttributes();
				for(auto& attribute : pAttributes->GetAttributeDeclaration())
				{
					// input-icon
					const bool isConnected = connections.IsConnected(core::ConnectionType::ATTRIBUTE, slot);
					auto pUserData = new EventUserDataAttribute(id, slot, attribute.GetType(), attribute.IsArray(), targetType);
					AddAttribute(isConnected, attribute.GetName(), slot, attribute.GetType(), attribute.IsArray(), pUserData);

					slot++;
				}
			}

			// outputs
			if(auto pOutputs = m_pEvent->QueryOutput())
			{
				unsigned slot = 0;
				for(auto& output : pOutputs->GetOutputDeclaration())
				{
					// output icon
					const bool isConnected = connections.IsConnected(core::ConnectionType::OUTPUT, slot);
					auto pUserData = new EventUserDataOutput(id, slot, targetType);

					AddOutput(isConnected, output, slot, pUserData);

					slot++;
				}
			}

			// returns
			if(auto pReturns = m_pEvent->QueryReturn())
			{
				unsigned int slot = 0;
				for(auto& ret : pReturns->GetReturnDeclaration())
				{
					// return-icon
					const bool isConnected = connections.IsConnected(core::ConnectionType::RETURN, slot);
					auto pUserData = new EventUserDataReturn(id, slot, ret.GetType(), ret.IsArray(), targetType);

					AddReturn(isConnected, ret.GetName(), slot, ret.GetType(), ret.IsArray(), pUserData);

					slot++;
				}
			}

			SetupWindow(m_pEvent->GetName(), m_pEvent->GetPosition(), L"EventFunction", rIcon);
		}

		BaseEvent::~BaseEvent(void)
		{
		}

		void BaseEvent::SetOffset(const math::Vector2& vOffset)
		{
			auto pMain = GetWidgetByName(L"Window");

			m_vOffset = vOffset;

			auto& vPosition = GetTargetPosition();
			pMain->SetX((float)vPosition.x + vOffset.x);
			pMain->SetY((float)vPosition.y + vOffset.y);
		}

		unsigned int BaseEvent::GetOutputSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vOutputOffsets.size());
			return m_vOutputOffsets.at(slot) + m_topOffset;
		}

		unsigned int BaseEvent::GetInputSlotPos(void) const
		{
			return m_topOffset + BORDER + IO_SIZE / 2;
		}

		unsigned int BaseEvent::GetReturnSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vReturnOffsets.size());
			return m_vReturnOffsets.at(slot) + m_topOffset;
		}

		unsigned int BaseEvent::GetAttributeSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vAttributeOffsets.size());
			return m_vAttributeOffsets.at(slot) + m_topOffset;
		}

		unsigned int BaseEvent::GetWidth(void) const
		{
			return (unsigned int)GetMainWidget()->GetRelWidth();
		}

		ConnectionDragDropHandler& BaseEvent::GetHandler(void)
		{
			return *m_pHandler;
		}

		std::wstring toEventFlowIcon(bool empty)
		{
			if(empty)
				return L"EventFlowEmptyIcon";
			else
				return L"EventFlowIcon";
		}

		void BaseEvent::AddOutput(bool isConnected, const std::wstring& stLabel, unsigned int slot, gui::BaseUserData* pData)
		{
			// output
			auto& icon = AddWidget<gui::Icon>(-(float)BORDER, (float)m_outPosY, (float)IO_SIZE, toEventFlowIcon(!isConnected), rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			icon.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			icon.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			icon.SetUserData(*pData);
			AddConnectionContext(icon, core::ConnectionType::OUTPUT, slot, isConnected);

			if(!stLabel.empty())
			{
				auto size = m_pModule->CalculateTextRect(stLabel);
				m_outWidth = max((unsigned int)size.width, m_outWidth);

				// return label
				auto& returnLabel = AddWidget<gui::Label>(-(float)(BORDER * 2 + IO_SIZE), (float)m_outPosY, (float)size.width, (float)size.height, stLabel);
				returnLabel.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
				returnLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				returnLabel.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			}

			m_vOutputOffsets.push_back(m_outPosY + IO_SIZE / 2);

			m_outPosY += IO_SIZE + BORDER;
		}

		std::wstring attributeTypeToIcon(core::AttributeType type, bool isEmpty, bool isArray)
		{
			std::wstring stName = L"EventInput";
			if(isEmpty)
				stName += L"Empty";
			if(isArray)
				stName += L"Array";

			switch(type)
			{
			case core::AttributeType::BOOL:
				stName += L"Bool";
				break;
			case core::AttributeType::FLOAT:
				stName += L"Float";
				break;
			case core::AttributeType::INT:
				stName += L"Int";
				break;
			case core::AttributeType::STRING:
				stName += L"String";
				break;
			case core::AttributeType::OBJECT:
				stName += L"Object";
				break;
			default:
				ACL_ASSERT(false);
			}

			stName += L"Icon";
			return stName;
		}

		void BaseEvent::AddReturn(bool isConnected, const std::wstring& stLabel, unsigned int slot, core::AttributeType type, bool isArray, gui::BaseUserData* pData)
		{
			// return-icon
			auto& icon = AddWidget<gui::Icon>(-(float)BORDER, (float)m_outPosY, (float)IO_SIZE, attributeTypeToIcon(type, !isConnected, isArray), rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			icon.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			icon.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			icon.SetUserData(*pData);

			// context menu
			AddConnectionContext(icon, core::ConnectionType::RETURN, slot, isConnected);

			auto size = m_pModule->CalculateTextRect(stLabel);
			m_outWidth = max((unsigned int)size.width, m_outWidth);

			// return label
			auto& returnLabel = AddWidget<gui::Label>(-(float)(BORDER * 2 + IO_SIZE), (float)m_outPosY, (float)size.width, (float)size.height, stLabel);
			returnLabel.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			returnLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			returnLabel.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);

			m_vReturnOffsets.push_back(m_outPosY + IO_SIZE / 2);

			m_outPosY += IO_SIZE + BORDER;
		}

		void BaseEvent::AddInput(bool isConnected, const std::wstring& stLabel, unsigned int slot, gui::BaseUserData* pData)
		{
			// output
			auto& icon = AddWidget<gui::Icon>((float)BORDER, (float)m_inPosY, (float)IO_SIZE, toEventFlowIcon(!isConnected), rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			icon.SetUserData(*pData);
			AddConnectionContext(icon, core::ConnectionType::INPUT, slot, isConnected);

			if(!stLabel.empty())
			{
				auto size = m_pModule->CalculateTextRect(stLabel);
				m_outWidth = max((unsigned int)size.width, m_outWidth);

				// return label
				auto& returnLabel = AddWidget<gui::Label>((float)(BORDER * 2 + IO_SIZE), (float)m_inPosY, (float)size.width, (float)size.height, stLabel);
				returnLabel.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			}

			m_vInputOffsets.push_back(m_inPosY + IO_SIZE / 2);

			m_inPosY += IO_SIZE + BORDER;
		}

		void BaseEvent::AddAttribute(bool isConnected, const std::wstring& stLabel, unsigned int slot, core::AttributeType type, bool isArray, gui::BaseUserData* pData)
		{
			auto size = m_pModule->CalculateTextRect(stLabel);
			m_inWidth = max((unsigned int)size.width, m_inWidth);

			// input-icon
			auto& icon = AddWidget<gui::Icon>((float)BORDER, (float)m_inPosY, (float)IO_SIZE, attributeTypeToIcon(type, !isConnected, isArray), rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			icon.SetUserData(*pData);

			// context menu
			AddConnectionContext(icon, core::ConnectionType::ATTRIBUTE, slot, isConnected);

			// label
			auto& label = AddWidget<gui::Label>((float)BORDER * 2 + IO_SIZE, (float)m_inPosY, (float)size.width, (float)size.height, stLabel);
			label.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);

			m_vAttributeOffsets.push_back(m_inPosY + IO_SIZE / 2);

			m_inPosY += IO_SIZE + BORDER;

			if(!isConnected)
			{
				if(!isArray)
				{
					auto pAttribute = GetAttribute(slot);
					ACL_ASSERT(pAttribute);

					auto size = IO_SIZE;  
					if(pAttribute->GetType() != core::AttributeType::BOOL)
					{
						const auto stValue = pAttribute->ToString();
						size = m_pModule->CalculateTextRect(stValue).width;
					}
					size += 32;

					m_inWidth = max((unsigned int)size, m_inWidth);

					// edit box
					if(type != core::AttributeType::OBJECT)
					{					
						AddController<EventInputBox>(*GetWidgetByName<gui::Window>(L"Window"), *pAttribute, BORDER * 2, m_inPosY, size, IO_SIZE);

						m_inPosY += IO_SIZE + BORDER;
					}
				}
				else
				{
					const AttributeArrayData data(type, slot);

					m_vArrays.emplace_back(icon, icon.SigReleased, data);
					auto container = m_vArrays.rbegin();
					container->SigAccess.Connect(this, &BaseEvent::OnEditArray);
				}
			}

			slot++;
		}

		void BaseEvent::SetupWindow(const std::wstring& stName, const math::Vector2& vPosition, const std::wstring& stIcon, const math::Rect& rSrc)
		{
			auto pMain = GetWidgetByName<gui::Window>(L"Window");
			pMain->SetX((float)vPosition.x);
			pMain->SetY((float)vPosition.y);
			pMain->SetWidth((float)GetTargetWidth());

			// height 
			if(stName.empty())
			{
				pMain->RemoveStyleFlags(gui::WindowStyleFlags::TopBar);
				m_topOffset = 8;
			}
			else
			{
				pMain->SetLabel(stName);
				m_topOffset = 24;
			}

			const float targetHeight = (float)GetTargetHeight() + m_topOffset;
			pMain->SetHeight(targetHeight);
			pMain->SetIcon(stIcon, rSrc.x, rSrc.y, rSrc.width, rSrc.height);
		}

		void BaseEvent::OnDragWindow(const math::Vector2& vDistance)
		{
			SetTargetPosition(math::Vector2((int)m_pMainWidget->GetRelX() - m_vOffset.x, (int)m_pMainWidget->GetRelY() - m_vOffset.y));
		}

		void BaseEvent::OnKey(gui::Keys keys)
		{
			switch(keys)
			{
			case gui::Keys::DELETE_KEY:
				OnDelete();
				break;
			case gui::Keys::F:
				SigFocus(GetTargetPosition());
				break;
			case gui::Keys::X:
				if(m_pModule->ComboKeysActive(gui::ComboKeys::CTRL))
				{
					m_pModule->m_clipboard.AddData<EventClipboardData>(m_pEvent->Clone());
					OnDelete();
				}
				break;
			case gui::Keys::C:
				if(m_pModule->ComboKeysActive(gui::ComboKeys::CTRL))
					m_pModule->m_clipboard.AddData<EventClipboardData>(m_pEvent->Clone());
				break;
			}
		}

		void BaseEvent::OnRemoveConnection(SlotData data)
		{
			switch(data.type)
			{
			case core::ConnectionType::INPUT:
				SigEraseInConnection(data);
				break;
			case core::ConnectionType::ATTRIBUTE:
				SigEraseInConnection(data);
				break;
			case core::ConnectionType::OUTPUT:
				SigEraseOutConnection(data);
				break;
			case core::ConnectionType::RETURN:
				SigEraseOutConnection(data);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void BaseEvent::OnEditArray(AttributeArrayData data)
		{
			auto pAttribute = GetAttribute(data.slot);
			ACL_ASSERT(pAttribute);

			ArrayViewController arrayView(*m_pModule, *pAttribute);

			arrayView.Execute();
		}

		void BaseEvent::OnAppendEvent(SlotData data)
		{
			const math::Vector2 vSize(m_pMainWidget->GetWidth(), m_pMainWidget->GetHeight());
			SigAppendEvent(GetId(), data.slot, data.type, vSize);
		}

		void BaseEvent::AddConnectionContext(gui::Widget& widget, core::ConnectionType type, unsigned int slot, bool isConnected)
		{
			// menu
			gui::ContextMenu* pMenu = nullptr;

			if(isConnected)
			{
				pMenu = new gui::ContextMenu(0.02f);
				auto& item = pMenu->AddItem(L"Remove connection");

				// container
				const SlotData data(type, slot);

				m_vData.emplace_back(item, item.SigReleased, data);
				auto container = m_vData.rbegin();
				container->SigAccess.Connect(this, &BaseEvent::OnRemoveConnection);
			}
			else
			{
				pMenu = new gui::ContextMenu(0.02f);

				gui::ContextMenuItem* pItem = nullptr;
				switch(type)
				{
				case core::ConnectionType::OUTPUT:
					pItem = &pMenu->AddItem(L"Append event");
					break;
				case core::ConnectionType::INPUT:
					pItem = &pMenu->AddItem(L"Append to new trigger");
					break;
				case core::ConnectionType::ATTRIBUTE:
					pItem = &pMenu->AddItem(L"Connect to new variable");
					break;
				case core::ConnectionType::RETURN:
					pItem = &pMenu->AddItem(L"Append function");
					break;
				default:
					ACL_ASSERT(false);
				}

				const SlotData data(type, slot);

				m_vData.emplace_back(*pItem, pItem->SigReleased, data);
				auto container = m_vData.rbegin();
				container->SigAccess.Connect(this, &BaseEvent::OnAppendEvent);
			}

			if(pMenu)
			{
				widget.AddChild(*pMenu);
				widget.SetContextMenu(pMenu);
			}
			widget.SetDragDrop(m_pHandler);
		}

		unsigned int BaseEvent::GetTargetWidth(void) const
		{
			return m_inWidth + m_outWidth + MIDSECTION + BORDER * 2 + IO_SIZE * 2;
		}

		unsigned int BaseEvent::GetTargetHeight(void) const
		{
			return max(m_inPosY, m_outPosY) + 8;
		}

		void BaseEvent::SetTargetPosition(const math::Vector2& vPosition)
		{
			m_pEvent->SetPosition(vPosition);
		}

		const math::Vector2& BaseEvent::GetTargetPosition(void) const
		{
			return m_pEvent->GetPosition();
		}

		core::EventAttribute* BaseEvent::GetAttribute(unsigned int slot) const
		{
			if(auto pAttribute = m_pEvent->QueryAttribute())
				return &pAttribute->GetAttribute(slot);
			else
				return nullptr;
		}

		unsigned int BaseEvent::GetId(void) const
		{
			return m_pEvent->GetId();
		}

		void BaseEvent::OnDelete(void)
		{
			const auto id = GetId();
			auto& connections = m_pGraph->GetConnections(id);

			// remove connected output
			const auto vInputTargets = connections.GetTargets(core::ConnectionType::INPUT, 0);
			if(!vInputTargets.empty())
			{
				ACL_ASSERT(vInputTargets.size() == 1);
				const auto pTarget = vInputTargets[0];

				if(auto pOutput = m_pInstance->GetEvent(pTarget->target).QueryOutput())
					pOutput->SetOutput(pTarget->targetSlot, core::EventCommand::NO_EVENT_ID);
			}

			// remove connected returns
			if(auto pAttributes = m_pEvent->QueryAttribute())
			{
				const auto numAttribute = pAttributes->GetNumAttributes();
				for(unsigned int i = 0; i < numAttribute; i++)
				{
					const auto vReturnTargets = connections.GetTargets(core::ConnectionType::ATTRIBUTE, i);
					for(auto pTarget : vReturnTargets)
					{
						if(auto pReturn = m_pInstance->GetEvent(pTarget->target).QueryReturn())
							pReturn->RemoveReturn(pTarget->targetSlot, id);
					}
				}
			}

			// remove connected attributes
			if(auto pReturn = m_pEvent->QueryReturn())
			{
				auto numReturns = pReturn->GetNumReturns();
				for(unsigned int i = 0; i < numReturns; i++)
				{
					for(auto target : pReturn->GetReturnTargets(0))
					{
						auto& targetEvent = m_pInstance->GetEvent(target.first);
						auto pAttribute = targetEvent.QueryAttribute();
						ACL_ASSERT(pAttribute);
						pAttribute->ConnectAttribute(target.second, false);
					}
				}
			}

			// remove event
			m_pInstance->RemoveEvent(*m_pEvent);

			SigInvalidate();
		}

	}
}

