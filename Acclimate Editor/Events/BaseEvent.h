#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"

namespace acl
{
	namespace core
	{
		class EventInstance;
		class EventAttribute;
		class EventCommand;
		class EventGraph;

		enum class ConnectionType;
		enum class AttributeType;
	}

	namespace editor
	{

		struct SlotData
		{
			SlotData(core::ConnectionType type, unsigned int slot);

			core::ConnectionType type;
			unsigned int slot;
		};

		struct AttributeArrayData
		{
			AttributeArrayData(core::AttributeType type, unsigned int slot);

			core::AttributeType type;
			unsigned int slot;
		};

		class ConnectionDragDropHandler;

		class BaseEvent :
			public gui::BaseController
		{
			typedef std::vector<unsigned int> SlotOffsetVector;
			typedef gui::DataContainer<SlotData> SlotContainer;
			typedef std::vector<SlotContainer> DataVector;
			typedef gui::DataContainer<AttributeArrayData> ArrayContainer;
			typedef std::vector<ArrayContainer> ArrayVector;
		public:
			BaseEvent(gui::Module& module, gui::Widget& parent, core::EventInstance& instance, core::EventCommand& command, const core::EventGraph& graph, ConnectionDragDropHandler& handler);
			virtual ~BaseEvent(void);

			void SetOffset(const math::Vector2& vOffset);

			unsigned int GetOutputSlotPos(unsigned int slot) const;
			unsigned int GetInputSlotPos(void) const;
			unsigned int GetReturnSlotPos(unsigned int slot) const;
			unsigned int GetAttributeSlotPos(unsigned int slot) const;

			unsigned int GetWidth(void) const;

			core::Signal<const SlotData&> SigEraseInConnection;
			core::Signal<const SlotData&> SigEraseOutConnection;
			core::Signal<> SigInvalidate;
			core::Signal<unsigned int, unsigned int, core::ConnectionType, const math::Vector2&> SigAppendEvent;
			core::Signal<const math::Vector2&> SigFocus;

		protected:

			ConnectionDragDropHandler& GetHandler(void);

			void AddOutput(bool isConnected, const std::wstring& stLabel, unsigned int slot, gui::BaseUserData* pData);
			void AddReturn(bool isConnected, const std::wstring& stLabel, unsigned int slot, core::AttributeType type, bool isArray, gui::BaseUserData* pData);
			void AddInput(bool isConnected, const std::wstring& stLabel, unsigned int slot, gui::BaseUserData* pData);
			void AddAttribute(bool isConnected, const std::wstring& stLabel, unsigned int slot, core::AttributeType type, bool isArray, gui::BaseUserData* pData);
			void SetupWindow(const std::wstring& stName, const math::Vector2& vPosition, const std::wstring& stIcon, const math::Rect& rSrc);

		private:

			void SetTargetPosition(const math::Vector2& vPosition);
			const math::Vector2& GetTargetPosition(void) const;
			core::EventAttribute* GetAttribute(unsigned int slot) const;
			unsigned int GetId(void) const;
			void OnDelete(void);

			void OnDragWindow(const math::Vector2& vDistance);
			void OnKey(gui::Keys keys);
			void OnRemoveConnection(SlotData data);
			void OnEditArray(AttributeArrayData data);
			void OnAppendEvent(SlotData data);

			void AddConnectionContext(gui::Widget& widget, core::ConnectionType type, unsigned int slot, bool isConnected);
			unsigned int GetTargetWidth(void) const;
			unsigned int GetTargetHeight(void) const;

			unsigned int m_inWidth, m_outWidth, m_inPosY, m_outPosY, m_attributeSlot, m_topOffset;
			math::Vector2 m_vOffset;
			core::EventInstance* m_pInstance;
			const core::EventGraph* m_pGraph;
			core::EventCommand* m_pEvent;
			ConnectionDragDropHandler* m_pHandler;

			DataVector m_vData;
			SlotOffsetVector m_vOutputOffsets, m_vReturnOffsets, m_vAttributeOffsets, m_vInputOffsets;
			ArrayVector m_vArrays;
		};

	}
}


