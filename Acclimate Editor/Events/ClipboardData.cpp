#include "ClipboardData.h"
#include "Core\EventCommand.h"

namespace acl
{
	namespace editor
	{
		
		EventClipboardData::EventClipboardData(const core::EventCommand& command) : m_pCommand(&command)
		{
		}

		EventClipboardData::~EventClipboardData(void)
		{
			delete m_pCommand;
		}

		const core::EventCommand& EventClipboardData::GetCommand(void) const
		{
			return *m_pCommand;
		}

	}
}

