#pragma once
#include "System\ClipboardData.h"

namespace acl
{
	namespace core
	{
		class EventCommand;
	}

	namespace editor
	{

		class EventClipboardData :
			public sys::ClipboardData<EventClipboardData>
		{
		public:
			EventClipboardData(const core::EventCommand& command);
			~EventClipboardData(void);

			const core::EventCommand& GetCommand(void) const;
			
		private:

			const core::EventCommand* m_pCommand;
		};

	}
}

