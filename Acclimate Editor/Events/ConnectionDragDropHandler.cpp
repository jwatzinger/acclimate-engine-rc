#include "ConnectionDragDropHandler.h"
#include "EventUserDataIO.h"
#include "Core\EventAttribute.h"
#include "Gfx\ILine.h"
#include "Gui\Widget.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		ConnectionDragDropHandler::ConnectionDragDropHandler(const gui::Widget& area) : m_beziers(0.05f), m_pArea(&area), m_canDraw(false)
		{
		}

		void ConnectionDragDropHandler::OnBegin(gui::Widget& dragWidget)
		{
			ACL_ASSERT(dragWidget.HasUserData());

			m_pDragWidget = &dragWidget;
		}

		gfx::Color attributeTypeToColor(core::AttributeType type)
		{
			switch(type)
			{
			case core::AttributeType::BOOL:
				return gfx::Color(255, 0, 0);
			case core::AttributeType::FLOAT:
				return gfx::Color(102, 255, 0);
			case core::AttributeType::INT:
				return gfx::Color(0, 255, 168);
			case core::AttributeType::STRING:
				return gfx::Color(255, 143, 0);
			case core::AttributeType::OBJECT:
				return gfx::Color(0, 65, 255);
			default:
				ACL_ASSERT(false);
			}

			return gfx::Color();
		}

		void ConnectionDragDropHandler::OnDrag(const math::Vector2& vMousePos)
		{
			// bezier points
			const math::Vector2 vOrigin = math::Vector2(m_pArea->GetX(), m_pArea->GetY());
			const math::Vector2 vLeft = math::Vector2(m_pDragWidget->GetX() + 8, m_pDragWidget->GetY() + 8) - vOrigin;
			const math::Vector2 vRight = vMousePos - vOrigin;

			const auto distanceX = (vRight.x - vLeft.x) / 2;

			const math::Vector3 points[4] =
			{
				{ (float)vLeft.x, (float)vLeft.y, 0.6f },
				{ (float)vLeft.x + distanceX, (float)vLeft.y, 0.6f },
				{ (float)vRight.x - distanceX, (float)vRight.y, 0.6f },
				{ (float)vRight.x, (float)vRight.y, 0.6f }
			};

			m_beziers.SetPoints(points);
			m_canDraw = true;
		}

		void ConnectionDragDropHandler::OnDrop(gui::Widget& targetWidget)
		{
			if(&targetWidget == m_pDragWidget || !targetWidget.HasUserData())
			{
				m_canDraw = false;
				m_pDragWidget = nullptr;
				return;
			}

			auto pInData = targetWidget.GetUserData<EventUserDataInput>();
			if(pInData)
			{
				auto pOutData = m_pDragWidget->GetUserData<EventUserDataOutput>();
				if(pOutData)
					SigConnectionIO(pOutData->event, pOutData->slot, pInData->event, pOutData->thisType);			
				else
				{
					// incompatible
				}
			}
			else
			{
				auto pOutData = targetWidget.GetUserData<EventUserDataOutput>();
				if(pOutData)
				{
					auto pInData = m_pDragWidget->GetUserData<EventUserDataInput>();
					if(pInData)
						SigConnectionIO(pOutData->event, pOutData->slot, pInData->event, pOutData->thisType);
					else
					{
						// incompatible
					}
				}
				else
				{
					auto pAttribute = targetWidget.GetUserData<EventUserDataAttribute>();
					if(pAttribute)
					{
						auto pReturnData = m_pDragWidget->GetUserData<EventUserDataReturn>();
						if(pReturnData)
						{
							if(pReturnData->attribType == pAttribute->attribType && pReturnData->isArray == pAttribute->isArray)
								SigConnectionAttributes(pReturnData->event, pReturnData->slot, pAttribute->event, pAttribute->slot, pReturnData->thisType);
							else
							{
								// incompatible
							}
						}
						else
						{
							// incompatible
						}
					}
					else
					{
						auto pAttribute = m_pDragWidget->GetUserData<EventUserDataAttribute>();
						if(pAttribute)
						{
							auto pReturnData = targetWidget.GetUserData<EventUserDataReturn>();
							if(pReturnData)
							{
								if(pReturnData->attribType == pAttribute->attribType && pReturnData->isArray == pAttribute->isArray)
									SigConnectionAttributes(pReturnData->event, pReturnData->slot, pAttribute->event, pAttribute->slot, pReturnData->thisType);
								else
								{
									// incompatible
								}
							}
							else
							{
								// incompatible
							}
						}
						else
						{
							// incompatible
						}
					}
				}
			}

			m_canDraw = false;
			m_pDragWidget = nullptr;
		}

		void ConnectionDragDropHandler::Draw(gfx::ILine& line, render::IStage& stage)
		{
			if(m_pDragWidget && m_canDraw)
			{
				// color
				auto pAttriuteData = m_pDragWidget->GetUserData<EventUserDataAttribute>();
				if(pAttriuteData)
					line.SetColor(attributeTypeToColor(pAttriuteData->attribType));
				else
				{
					auto pReturnData = m_pDragWidget->GetUserData<EventUserDataReturn>();
					if(pReturnData)
						line.SetColor(attributeTypeToColor(pReturnData->attribType));
					else
						line.SetColor(gfx::Color(255, 255, 255));
				}

				line.DrawMulti(m_beziers.GetPoints(), stage);
			}
		}

	}
}