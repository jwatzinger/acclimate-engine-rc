#pragma once
#include "Core\Signal.h"
#include "Gfx\Beziers.h"
#include "Gui\IDragDropHandler.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace core
	{
		enum class TargetType;
	}

	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class ConnectionDragDropHandler :
			public gui::IDragDropHandler
		{
		public:

			ConnectionDragDropHandler(const gui::Widget& area);

			void OnBegin(gui::Widget& dragWidget) override;
			void OnDrag(const math::Vector2& vMousePos) override;
			void OnDrop(gui::Widget& targetWidget) override;

			void Draw(gfx::ILine& line, render::IStage& stage);

			// out, in, in-slot, type
			core::Signal<unsigned int, unsigned int, unsigned int, core::TargetType> SigConnectionIO;
			// out, out-slot, in, in-slot, type
			core::Signal<unsigned int, unsigned int, unsigned int, unsigned int, core::TargetType> SigConnectionAttributes;

		private:

			bool m_canDraw;
			const gui::Widget* m_pDragWidget, *m_pArea;

			gfx::Beziers<math::Vector3, 4> m_beziers;
		};
	}
}