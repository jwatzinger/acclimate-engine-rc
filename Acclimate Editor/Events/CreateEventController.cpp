#include "CreateEventController.h"
#include "Gui\Window.h"

namespace acl
{
	namespace editor
	{

		CreateEventController::CreateEventController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Events/CreateEvent.axm"),
			m_create(false)
		{
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 0.0f, 1.0f, 21.0f, L"New Event");
			m_pBox->SetPadding(8, 8, 16, 0);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &CreateEventController::OnCreate);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &CreateEventController::OnCancel);
			GetWidgetByName(L"Create")->SigReleased.Connect(this, &CreateEventController::OnCreate);
		}

		CreateEventController::~CreateEventController()
		{
		}

		bool CreateEventController::Execute(std::wstring* pName)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();
				return true;
			}
			else
				return false;
		}

		void CreateEventController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void CreateEventController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
