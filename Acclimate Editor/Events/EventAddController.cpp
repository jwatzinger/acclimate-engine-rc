#include "EventAddController.h"
#include "Core\EventLoader.h"
#include "Core\EventInstance.h"
#include "Core\FunctionDeclaration.h"

namespace acl
{
	namespace editor
	{

		EventAddController::EventAddController(gui::Module& module, ItemType type) : BaseController(module, L"../Editor/Menu/Events/AddEvent.axm"),
			m_create(false), m_type(type)
		{
			m_pList = &AddWidget<EventList>(0.0f, 0.0f, 1.0f, 1.0f, 0.02f);
			m_pList->SetPadding(gui::PaddingSide::BOTTOM, 40);
			m_pList->SigConfirm.Connect(this, &EventAddController::OnCreate);
			m_pList->OnFocus(true);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");
			switch(m_type)
			{
			case ItemType::COMMAND:
				m_pWindow->SetLabel(L"Add event");
				break;
			case ItemType::TRIGGER:
				m_pWindow->SetLabel(L"Add trigger");
				break;
			case ItemType::FUNCTION:
				m_pWindow->SetLabel(L"Add function");
				break;
			default:
				ACL_ASSERT(false);
			}

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &EventAddController::OnCancel);
			m_pCreate = GetWidgetByName(L"Create");
			m_pCreate->SigReleased.Connect(this, &EventAddController::OnCreate);
		}

		EventAddController::~EventAddController(void)
		{
		}

		bool EventAddController::Execute(std::wstring* pName, core::EventInstance* pInstance)
		{
			m_create = false;

			// TODO: only call in case new event type is adde
			m_pList->Clear();

			core::EventLoader::NameVector vNames;
			switch(m_type)
			{
			case ItemType::COMMAND:
			{
				std::map<std::wstring, unsigned int> mNames;
				const auto vTempNames = core::EventLoader::GetRegisteredEvents();
				for(auto& stName : vTempNames)
				{
					mNames.emplace(stName, 0);
				}
				for(auto& name : mNames)
				{
					vNames.push_back(name.first);
				}
				break;
			}
			case ItemType::TRIGGER:
				ACL_ASSERT(pInstance);
				{
					auto mTriggers = core::EventLoader::GetRegisteredTriggers();
					for(auto trigger : pInstance->GetTriggerTypes())
					{
						ACL_ASSERT(mTriggers.count(trigger));
						mTriggers.erase(trigger);
					}
					for(auto& name : mTriggers)
					{
						vNames.push_back(name.second);
					}
				}
				break;
			case ItemType::FUNCTION:
			{
				const auto vTempFunctions = core::EventLoader::GetRegisteredFunctions();
				std::map<std::wstring, unsigned int> mFunctions;
				for(auto& stName : vTempFunctions)
				{
					mFunctions.emplace(stName, 0);
				}

				if(IsFilterActive(FilterType::ATTRIBUTES))
				{
					const auto& filter = GetFilter(FilterType::ATTRIBUTES);

					for(auto& function : mFunctions)
					{
						auto& declaration = core::EventLoader::GetFunctionDeclaration(function.first);

						for(auto& attribute : declaration.GetAttributes())
						{
							if(attribute.GetType() == filter.type && attribute.IsArray() == filter.isArray)
								vNames.push_back(function.first);

							break;
						}
					}
				}
				else
				{
					for(auto& function : mFunctions)
					{
						vNames.emplace_back(function.first);
					}
				}
				break;
			}
			default:
				ACL_ASSERT(false);
			}

			if(vNames.empty())
				m_pCreate->OnDisable();
			else
			{
				m_pCreate->OnEnable();
				for(auto& stEvent : vNames)
				{
					m_pList->AddItem(stEvent, stEvent);
				}
			}

			m_pList->SelectById(0);

			m_pWindow->Execute();

			if(m_create && !m_pList->IsEmpty())
			{
				*pName = m_pList->GetContent();
				return true;
			}
			else
				return false;
		}

		void EventAddController::SetFilter(FilterType filterType, core::AttributeType type, bool isArray)
		{
			auto& filter = GetFilter(filterType);

			filter.isActive = true;
			filter.type = type;
			filter.isArray = isArray;
		}

		EventAddController::Filter::Filter(void) : isActive(false)
		{
		}

		EventAddController::Filter& EventAddController::GetFilter(FilterType type)
		{
			if(type == FilterType::ATTRIBUTES)
				return m_filter[0];
			else
				return m_filter[1];
		}

		const EventAddController::Filter& EventAddController::GetFilter(FilterType type) const
		{
			if(type == FilterType::ATTRIBUTES)
				return m_filter[0];
			else
				return m_filter[1];
		}

		bool EventAddController::IsFilterActive(FilterType type) const
		{
			return GetFilter(type).isActive;
		}

		void EventAddController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void EventAddController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}

