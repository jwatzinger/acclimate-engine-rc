#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"
#include "Gui\Window.h"

namespace acl
{
	namespace core
	{
		class EventInstance;

		enum class AttributeType;
	}

	namespace editor
	{

		enum class ItemType
		{
			COMMAND, TRIGGER, FUNCTION
		};

		enum class FilterType
		{
			ATTRIBUTES, RETURNS
		};

		class EventAddController :
			public gui::BaseController
		{
			typedef gui::List<std::wstring> EventList;
		public:

			EventAddController(gui::Module& module, ItemType type);
			~EventAddController(void);

			bool Execute(std::wstring* pName, core::EventInstance* pInstance);

			void SetFilter(FilterType filter, core::AttributeType type, bool isArray);

		private:
			
			struct Filter
			{
				Filter(void);

				core::AttributeType type;
				bool isArray, isActive;
			};

			Filter& GetFilter(FilterType type);
			const Filter& GetFilter(FilterType type) const;
			bool IsFilterActive(FilterType type) const;

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;
			ItemType m_type;

			EventList* m_pList;
			gui::Widget* m_pCreate;
			gui::Window* m_pWindow;

			Filter m_filter[2];
		};

	}
}

