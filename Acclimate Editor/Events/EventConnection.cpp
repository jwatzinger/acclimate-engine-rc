#include "EventConnection.h"
#include "EventInstance.h"
#include "Core\Event.h"
#include "Core\EventGraph.h"
#include "Gfx\ILine.h"

namespace acl
{
	namespace editor
	{

		EventData::EventData(core::Event& event, unsigned int offY, EventInstance& instance, const core::ConnectionReflector& connections) : pEvent(&event), 
			offY(offY), pInstance(&instance), pConnections(&connections)
		{
		}

		gfx::Color connectionTypeToColor(EventConnection::Type type)
		{
			switch(type)
			{
			case EventConnection::Type::IO:
				return gfx::Color(255, 255, 255);
			case EventConnection::Type::BOOL:
				return gfx::Color(255, 0, 0);
			case EventConnection::Type::FLOAT:
				return gfx::Color(102, 255, 0);
			case EventConnection::Type::INT:
				return gfx::Color(0, 255, 168);
			case EventConnection::Type::STRING:
				return gfx::Color(255, 143, 0);
			default:
				ACL_ASSERT(false);
			}

			return gfx::Color();
		}

		EventConnection::EventConnection(gfx::ILine& line, render::IStage& stage, const EventData& left, const EventData& right, Type type) :
			m_pLine(&line), m_pStage(&stage), m_type(type), m_left(left), m_right(right), m_isValid(true)
		{
			m_left.pInstance->SigEraseOutConnection.Connect(this, &EventConnection::OnEraseOutConnection);
			m_right.pInstance->SigEraseInConnection.Connect(this, &EventConnection::OnEraseInConnection);
			UpdateSplines();
		}

		EventConnection::~EventConnection(void)
		{
			m_left.pInstance->SigEraseOutConnection.Disconnect(this, &EventConnection::OnEraseOutConnection);
			m_right.pInstance->SigEraseInConnection.Disconnect(this, &EventConnection::OnEraseInConnection);
		}

		bool EventConnection::IsValid(void) const
		{
			return m_isValid;
		}

		void EventConnection::Update(void)
		{
			if(m_isValid)
				UpdateSplines();
		}

		void EventConnection::Draw(void) const
		{
			if(m_isValid)
			{
				m_pLine->SetColor(connectionTypeToColor(m_type));
				m_pLine->DrawMulti(m_pBeziers->GetPoints(), *m_pStage);
			}
		}

		void EventConnection::UpdateSplines(void)
		{
			const auto vLeftPosition = m_left.pEvent->GetPosition() + math::Vector2(m_left.pInstance->GetWidth(), m_left.offY);
			const auto vRightPosition = m_right.pEvent->GetPosition() + math::Vector2(0, m_right.offY);

			const auto distanceX = (vLeftPosition.x - vRightPosition.x) / 2;

			const math::Vector3 points[4] =
			{
				{ (float)vLeftPosition.x, (float)vLeftPosition.y, 0.0f },
				{ (float)vLeftPosition.x - distanceX, (float)vLeftPosition.y, 0.0f },
				{ (float)vRightPosition.x + distanceX, (float)vRightPosition.y, 0.0f },
				{ (float)vRightPosition.x, (float)vRightPosition.y, 0.0f }
			};

			if(m_pBeziers)
				m_pBeziers->SetPoints(points);
			else
				m_pBeziers = new Bezier(points, 0.02f);
		}

		void EventConnection::OnEraseOutConnection(const SlotData& data)
		{
			if(m_type == Type::IO)
			{
				if(data.type == core::ConnectionType::OUTPUT)
				{
					m_left.pEvent->SetOutput(data.slot, core::Event::NO_EVENT_ID);
					m_isValid = false;
				}
			}
			else if(data.type == core::ConnectionType::RETURN)
			{
				m_left.pEvent->RemoveReturn(data.slot, m_right.pEvent->GetId());
				m_isValid = false;
			}
		}

		void EventConnection::OnEraseInConnection(const SlotData& data)
		{
			if(m_type == Type::IO)
			{
				if(data.type == core::ConnectionType::INPUT)
				{
					const auto id = m_left.pEvent->GetId();

					auto& vTargets = m_right.pConnections->GetTargets(core::ConnectionType::INPUT, data.slot);
					for(auto pTarget : vTargets)
					{
						if(pTarget->target == id)
						{
							m_left.pEvent->SetOutput(pTarget->slot, core::Event::NO_EVENT_ID);
							m_isValid = false;
							
							return;
						}
					}

					ACL_ASSERT(false); // should never be reached
				}
			}
			else if(data.type == core::ConnectionType::ATTRIBUTE)
			{
				const auto id = m_left.pEvent->GetId();

				auto& vTargets = m_right.pConnections->GetTargets(core::ConnectionType::ATTRIBUTE, data.slot);
				ACL_ASSERT(!vTargets.empty());

				for(auto pTarget : vTargets)
				{
					ACL_ASSERT(pTarget);
					if(pTarget->target == id)
					{
						m_left.pEvent->RemoveReturn(pTarget->targetSlot, m_right.pEvent->GetId());
						m_isValid = false;

						return;
					}
				}

				ACL_ASSERT(false); // should never be reached

			}
		}

	}
}

