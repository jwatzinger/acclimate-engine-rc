#pragma once
#include "Gfx\Beziers.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace core
	{
		class Event;
		class ConnectionReflector;
	}

	namespace gui
	{
		class Module;
	}

	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class EventInstance;

		struct EventData
		{
			EventData(core::Event& event, unsigned int offY, EventInstance& instance, const core::ConnectionReflector& connections);

			core::Event* pEvent;
			const core::ConnectionReflector* pConnections;
			unsigned int offY;
			EventInstance* pInstance;
		};

		struct SlotData;

		class EventConnection
		{
			typedef gfx::Beziers<math::Vector3, 4> Bezier;
		public:

			enum class Type
			{
				IO, BOOL, INT, FLOAT, STRING
			};

			EventConnection(gfx::ILine& line, render::IStage& stage, const EventData& left, const EventData& right, Type type);
			~EventConnection(void);

			bool IsValid(void) const;

			void Update(void);

			void Draw(void) const;

		private:

			void UpdateSplines(void);

			void OnEraseOutConnection(const SlotData& data);
			void OnEraseInConnection(const SlotData& data);

			Bezier* m_pBeziers;
			gfx::ILine* m_pLine;
			render::IStage* m_pStage;

			bool m_isValid;
			EventData m_left, m_right;
			Type m_type;
		};

	}
}


