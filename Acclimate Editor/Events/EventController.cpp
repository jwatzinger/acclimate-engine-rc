#include "EventController.h"
#include "EventListController.h"
#include "EventWorkingArea.h"
#include "VariableViewer.h"
#include "VariableExplorer.h"
#include "Core\EventSaver.h"
#include "Gui\DockArea.h"

namespace acl
{
	namespace editor
	{

		EventController::EventController(gui::Module& module, core::Events& events, gfx::ILine& line, render::IStage& stage) :
			BaseController(module, L"../Editor/Menu/Events/Events.axm"), m_isDirty(false), m_pEvents(&events)
		{
			auto pDockArea = GetWidgetByName<gui::DockArea>(L"Area");

			// list
			m_pList = &AddController<EventListController>(events);
			m_pList->SigPickedEvent.Connect(this, &EventController::OnPickEvent);
			m_pList->Dock(*pDockArea, gui::DockType::LEFT, true);

			// attribute viewer
			m_pVariableViewer = &AddController<VariableViewer>();
			m_pVariableViewer->Dock(*pDockArea, gui::DockType::RIGHT, false);
			m_pVariableViewer->SigVariableDirty.Connect(this, &EventController::OnVariableDirty);

			// attribute explorer
			m_pVariables = &AddController<VariableExplorer>();
			m_pVariables->Dock(*pDockArea, gui::DockType::RIGHT, false);
			m_pVariables->SigPickedVariable.Connect(m_pVariableViewer, &VariableViewer::SetVariable);
			m_pVariables->SigGetterAdded.Connect(this, &EventController::OnVariableDirty);

			// working area
			m_pWorkingArea = &AddController<EventWorkingArea>(line, stage);
			m_pWorkingArea->Dock(*pDockArea, gui::DockType::MIDDLE, true);
			m_pWorkingArea->SigVariableAdded.Connect(m_pVariables, &VariableExplorer::OnRefresh);

			m_pVariables->SigGetterAttached.Connect(m_pWorkingArea, &EventWorkingArea::OnAttachGetter);
			m_pVariables->SigSetterAttached.Connect(m_pWorkingArea, &EventWorkingArea::OnAttachSetter);

			pDockArea->SetDockState(gui::DockState::FIXED);
		}

		EventController::~EventController(void)
		{
		}

		void EventController::OnSave(const std::wstring& stFile)
		{
			const core::EventSaver saver(*m_pEvents);
			saver.Save(stFile);
		}

		void EventController::OnRefresh(void)
		{
			m_pList->OnRefresh();
			m_pWorkingArea->OnRefresh();
			m_pVariables->OnRefresh();
		}

		void EventController::OnPickEvent(core::EventInstance* pEvent)
		{
			m_pWorkingArea->SetEvent(pEvent);
			m_pVariables->SetEvent(pEvent);
		}

		void EventController::OnVariableDirty(void)
		{
			m_isDirty = true;
		}

		void EventController::Update(void)
		{
			if(m_isDirty)
			{
				OnRefresh();
				m_isDirty = false;
			}

			BaseController::Update();
		}

		void EventController::Draw(void)
		{
			m_pWorkingArea->Draw();
		}

	}
}

