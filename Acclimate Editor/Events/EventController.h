#pragma once
#include "Core\Events.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class EventListController;
		class EventWorkingArea;
		class VariableExplorer;
		class VariableViewer;

		class EventController :
			public gui::BaseController
		{
		public:
			EventController(gui::Module& module, core::Events& events, gfx::ILine& line, render::IStage& stage);
			~EventController();

			void OnSave(const std::wstring& stFile);
			void OnRefresh(void);

			void Update(void) override;
			void Draw(void);

		private:

			void OnPickEvent(core::EventInstance* pEvent);
			void OnVariableDirty(void);

			bool m_isDirty;

			EventListController* m_pList;
			VariableExplorer* m_pVariables;
			VariableViewer* m_pVariableViewer;
			EventWorkingArea* m_pWorkingArea;

			core::Events* m_pEvents;
		};

	}
}

