#include "EventInputBox.h"
#include "Gui\CheckBox.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace editor
	{

		EventInputBox::EventInputBox(gui::Module& module, gui::Widget& parent, core::EventAttribute& attribute, unsigned int x, unsigned int y, unsigned int width, unsigned int height) : 
			BaseController(module, parent), m_pAttribute(&attribute)
		{
			gui::Widget* pBox = nullptr;
			switch(attribute.GetType())
			{
			case core::AttributeType::BOOL:
			{
				auto& box = AddWidget<gui::CheckBox>((float)x, (float)y, (float)height);
				box.OnCheck(m_pAttribute->GetValue<bool>());
				pBox = &box;
				box.SigChecked.Connect(this, &EventInputBox::OnChange<bool>);
				break;
			}
			case core::AttributeType::FLOAT:
			{
				auto& box = AddWidget<gui::Textbox<float>>((float)x, (float)y, (float)width, (float)height, m_pAttribute->GetValue<float>());
				box.SigContentSet.Connect(this, &EventInputBox::OnChange<float>);
				pBox = &box;
				break;
			}
			case core::AttributeType::INT:
			{
				auto& box = AddWidget<gui::Textbox<int>>((float)x, (float)y, (float)width, (float)height, m_pAttribute->GetValue<int>());
				box.SigContentSet.Connect(this, &EventInputBox::OnChange<int>);
				pBox = &box;
				break;
			}
			case core::AttributeType::STRING:
			{
				auto& box = AddWidget<gui::Textbox<std::wstring>>((float)x, (float)y, (float)width, (float)height, m_pAttribute->GetValue<std::wstring>());
				box.SigContentSet.Connect(this, &EventInputBox::OnChange<std::wstring>);
				pBox = &box;
				break;
			}
			default:
				ACL_ASSERT(false);
			}

			pBox->SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			parent.AddChild(*pBox);
		}

	}
}

