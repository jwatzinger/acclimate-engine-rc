#pragma once
#include "Core\EventAttribute.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace editor
	{

		class EventInputBox :
			public gui::BaseController
		{
		public:
			EventInputBox(gui::Module& module, gui::Widget& parent, core::EventAttribute& attribute, unsigned int x, unsigned int y, unsigned int width, unsigned int height);

			core::Signal<> SigChanged;

		private:

			template<typename Type>
			void OnChange(Type data)
			{
				m_pAttribute->SetValue<Type>(data);
				SigChanged();
			}

			core::EventAttribute* m_pAttribute;
		};

	}
}


