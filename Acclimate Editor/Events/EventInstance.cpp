#include "EventInstance.h"
#include "ConnectionDragDropHandler.h"
#include "EventUserDataIO.h"
#include "ArrayViewController.h"
#include "Core\Event.h"
#include "Core\EventDeclaration.h"
#include "Core\EventGraph.h"
#include "Gui\Label.h"
#include "Gui\Icon.h"
#include "Gui\Window.h"
#include "Gui\TextBox.h"
#include "Gui\CheckBox.h"
#include "Gui\ContextMenu.h"

namespace acl
{
	namespace editor
	{

		SlotData::SlotData(core::ConnectionType type, unsigned int slot) : type(type), slot(slot)
		{
		}

		AttributeArrayData::AttributeArrayData(core::AttributeType type, unsigned int slot) : type(type), slot(slot)
		{
		}

		std::wstring toEventFlowIcon(bool empty)
		{
			if(empty)
				return L"EventFlowEmptyIcon";
			else
				return L"EventFlowIcon";
		}

		std::wstring attributeTypeToIcon(core::AttributeType type, bool isEmpty, bool isArray)
		{
			std::wstring stName = L"EventInput";
			if(isEmpty)
				stName += L"Empty";
			if(isArray)
				stName += L"Array";

			switch(type)
			{
			case core::AttributeType::BOOL:
				stName += L"Bool";
				break;
			case core::AttributeType::FLOAT:
				stName += L"Float";
				break;
			case core::AttributeType::INT:
				stName += L"Int";
				break;
			case core::AttributeType::STRING:
				stName += L"String";
				break;
			}

			stName += L"Icon";
			return stName;
		}

		const unsigned int BORDER = 8;
		const unsigned int MIDSECTION = 64;
		const unsigned int IO_SIZE = 16;
		const math::Rect rIcon(0, 0, 36, 36);
		
		struct BoxDef
		{
			BoxDef(unsigned int slot, unsigned int pos) : slot(slot), pos(pos)
			{

			}

			unsigned int slot;
			unsigned int pos;
		};

		typedef std::vector<BoxDef> BoxDefVector;

		EventInstance::EventInstance(gui::Module& module, gui::Widget& parent, core::Event& event, const core::EventGraph& graph, ConnectionDragDropHandler& handler) :
			gui::BaseController(module, parent, L"../Editor/Menu/Events/Widget.axm"), m_pEvent(&event), m_pHandler(&handler)
		{
			auto& connections = graph.GetConnections(event.GetId());
			auto& declaration = m_pEvent->GetDeclaration();

			// input
			const bool isConnected = connections.IsConnected(core::ConnectionType::INPUT, 0);
			auto& icon = AddWidget<gui::Icon>((float)BORDER, (float)BORDER, (float)IO_SIZE, toEventFlowIcon(!isConnected), rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			AddConnectionContext(icon, core::ConnectionType::INPUT, 0, isConnected);
			icon.SetUserData(*new EventUserDataInput(event.GetId()));

			// attributes
			int posY = 0;
			int width = CreateAttributes(event, declaration, connections, posY);

			// outputs
			int posYReturn = 0;
			int returnWidth = CreateOutputReturns(event, declaration, connections, posYReturn);

			returnWidth += BORDER;

			width += MIDSECTION + BORDER * 2 + IO_SIZE * 2;

			// modify window
			const auto& vPosition = event.GetPosition();
			auto pMain = GetWidgetByName<gui::Window>(L"Window");
			pMain->SetX((float)vPosition.x);
			pMain->SetY((float)vPosition.y);
			pMain->SetWidth((float)width + returnWidth);
			pMain->SetHeight((float)(max(posY, posYReturn) + 32));
			pMain->SetLabel(declaration.GetName());

			pMain->SigDrag.Connect(this, &EventInstance::OnDragWindow);
		}

		EventInstance::~EventInstance(void)
		{
			for(auto pSlot : m_vData)
			{
				delete pSlot;
			}

			for(auto pArray : m_vArrays)
			{
				delete pArray;
			}
		}

		unsigned int EventInstance::GetWidth(void) const
		{
			return (unsigned int)GetMainWidget()->GetRelWidth();
		}

		unsigned int EventInstance::GetOutputSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vOutputOffsets.size());
			return m_vOutputOffsets[slot] + 24;
		}

		unsigned int EventInstance::GetInputSlotPos(void) const
		{
			return 24 + BORDER + IO_SIZE / 2;
		}

		unsigned int EventInstance::GetReturnSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vReturnOffsets.size());
			return m_vReturnOffsets[slot] + 24;
		}

		unsigned int EventInstance::GetAttributeSlotPos(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vAttributeOffsets.size());
			return m_vAttributeOffsets[slot] + 24;
		}

		unsigned int EventInstance::CreateAttributes(const core::Event& event, const core::EventDeclaration& declaration, const core::ConnectionReflector& connections, int& posY)
		{
			posY = BORDER * 2 + IO_SIZE;
			int width = 0;

			BoxDefVector vBoxDefs;
			unsigned int slot = 0;
			for(auto& attribute : declaration.GetAttributes())
			{
				auto& stName = attribute.GetName();

				auto size = m_pModule->CalculateTextRect(stName);
				width = max(size.width, width);

				// input-icon
				const bool isConnected = connections.IsConnected(core::ConnectionType::ATTRIBUTE, slot);
				auto& icon = AddWidget<gui::Icon>((float)BORDER, (float)posY, (float)IO_SIZE, attributeTypeToIcon(attribute.GetType(), !isConnected, attribute.IsArray()), rIcon);
				icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
				icon.SetUserData(*new EventUserDataAttribute(event.GetId(), slot, attribute.GetType(), attribute.IsArray()));

				// context menu
				AddConnectionContext(icon, core::ConnectionType::ATTRIBUTE, slot, isConnected);

				// label
				auto& label = AddWidget<gui::Label>((float)BORDER * 2 + IO_SIZE, (float)posY, (float)size.width, (float)size.height, stName);
				label.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);

				m_vAttributeOffsets.push_back(posY + IO_SIZE / 2);

				posY += IO_SIZE + BORDER;

				if(!isConnected)
				{
					if(!attribute.IsArray())
					{					
						// add box definition
						vBoxDefs.emplace_back(slot, posY);
						// reserve space for boxes
						posY += IO_SIZE + BORDER;
					}
					else
					{
						const AttributeArrayData data(attribute.GetType(), slot);

						auto pContainer = new ArrayContainer(icon, icon.SigReleased, data);
						pContainer->SigAccess.Connect(this, &EventInstance::OnEditArray);
						m_vArrays.push_back(pContainer);
					}
				}

				slot++;
			}

			// attribute boxes
			for(auto& def : vBoxDefs)
			{
				auto& attribute = m_pEvent->GetAttribute(def.slot);
				const auto stValue = attribute.ToString();
				const auto size = m_pModule->CalculateTextRect(stValue);

				width = max(size.width + (int)BORDER*2, width);

				// edit box
				gui::Widget* pBox = nullptr;
				switch(attribute.GetType())
				{
				case core::AttributeType::BOOL:
				{
					auto& box = AddWidget<gui::CheckBox>(BORDER*2.0f, (float)def.pos, (float)IO_SIZE);
					box.OnCheck(conv::FromString<bool>(stValue));
					pBox = &box;
					break;
				}
				case core::AttributeType::FLOAT:
					pBox = &AddWidget<gui::Textbox<float>>(BORDER*2.0f, (float)def.pos, size.width + 32.0f, (float)IO_SIZE, conv::FromString<float>(stValue));
					break;
				case core::AttributeType::INT:
					pBox = &AddWidget<gui::Textbox<int>>(BORDER*2.0f, (float)def.pos, size.width + 32.0f, (float)IO_SIZE, conv::FromString<int>(stValue));
					break;
				case core::AttributeType::STRING:
					pBox = &AddWidget<gui::Textbox<std::wstring>>(BORDER*2.0f, (float)def.pos, size.width + 32.0f, (float)IO_SIZE, stValue);
					break;
				}

				pBox->SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			}

			return width;
		}

		unsigned int EventInstance::CreateOutputReturns(const core::Event& event, const core::EventDeclaration& declaration, const core::ConnectionReflector& connections, int& posY)
		{
			posY = BORDER;
			int width = 0;
			int slot = 0;

			for(auto& output : declaration.GetOutputs())
			{
				// output icon
				const bool isConnected = connections.IsConnected(core::ConnectionType::OUTPUT, slot);
				auto& icon = AddWidget<gui::Icon>(-(float)BORDER, (float)posY, (float)IO_SIZE, toEventFlowIcon(!isConnected), rIcon);
				icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
				icon.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				icon.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				icon.SetUserData(*new EventUserDataOutput(event.GetId(), slot));

				// context menu
				AddConnectionContext(icon, core::ConnectionType::OUTPUT, slot, isConnected);

				// output label
				if(!output.empty())
				{
					auto size = m_pModule->CalculateTextRect(output);
					width = max(size.width, width);

					// return label
					auto& returnLabel = AddWidget<gui::Label>(-(float)(BORDER * 2 + IO_SIZE), (float)posY, (float)size.width, (float)size.height, output);
					returnLabel.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
					returnLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
					returnLabel.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				}

				m_vOutputOffsets.push_back(posY + IO_SIZE / 2);

				posY += IO_SIZE + BORDER;
				slot++;
			}

			// returns
			slot = 0;
			for(auto& ret : declaration.GetReturns())
			{
				// return-icon
				const bool isConnected = connections.IsConnected(core::ConnectionType::RETURN, slot);
				auto& icon = AddWidget<gui::Icon>(-(float)BORDER, (float)posY, (float)IO_SIZE, attributeTypeToIcon(ret.GetType(), !isConnected, ret.IsArray()), rIcon);
				icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
				icon.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				icon.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				icon.SetUserData(*new EventUserDataReturn(event.GetId(), slot, ret.GetType(), ret.IsArray()));

				// context menu
				AddConnectionContext(icon, core::ConnectionType::RETURN, slot, isConnected);

				auto& stName = ret.GetName();
				auto size = m_pModule->CalculateTextRect(stName);
				width = max(size.width, width);

				// return label
				auto& returnLabel = AddWidget<gui::Label>(-(float)(BORDER * 2 + IO_SIZE), (float)posY, (float)size.width, (float)size.height, stName);
				returnLabel.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
				returnLabel.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
				returnLabel.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);

				m_vReturnOffsets.push_back(posY + IO_SIZE / 2);

				posY += IO_SIZE + BORDER;
				slot++;
			}

			return width;
		}

		void EventInstance::AddConnectionContext(gui::Widget& widget, core::ConnectionType type, unsigned int slot, bool isConnected)
		{
			if(isConnected)
			{
				// menu
				gui::ContextMenu* pMenu = new gui::ContextMenu(0.02f);
				auto& item = pMenu->AddItem(L"Remove connection");

				// container
				const SlotData data(type, slot);
				
				auto pContainer = new SlotContainer(item, item.SigReleased, data);
				pContainer->SigAccess.Connect(this, &EventInstance::OnRemoveConnection);
				m_vData.push_back(pContainer);

				widget.AddChild(*pMenu);
				widget.SetContextMenu(pMenu);
			}

			widget.SetDragDrop(m_pHandler);
		}

		void EventInstance::OnDragWindow(const math::Vector2& vDistance)
		{
			m_pEvent->SetPosition(math::Vector2((int)m_pMainWidget->GetRelX(), (int)m_pMainWidget->GetRelY()));
		}

		void EventInstance::OnRemoveConnection(SlotData data)
		{
			switch(data.type)
			{
			case core::ConnectionType::INPUT:
				SigEraseInConnection(data);
				break;
			case core::ConnectionType::ATTRIBUTE:
				SigEraseInConnection(data);
				break;
			case core::ConnectionType::OUTPUT:
				SigEraseOutConnection(data);
				break;
			case core::ConnectionType::RETURN:
				SigEraseOutConnection(data);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void EventInstance::OnEditArray(AttributeArrayData data)
		{
			ArrayViewController arrayView(*m_pModule, m_pEvent->GetAttribute(data.slot));
			
			arrayView.Execute();
		}

	}
}

