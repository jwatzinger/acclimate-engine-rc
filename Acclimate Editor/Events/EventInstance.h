#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"

namespace acl
{
	namespace core
	{
		class Event;
		class EventGraph;
		class EventDeclaration;
		class ConnectionReflector;

		enum class ConnectionType;
		enum class AttributeType;
	}

	namespace editor
	{

		struct SlotData
		{
			SlotData(core::ConnectionType type, unsigned int slot);

			core::ConnectionType type;
			unsigned int slot;
		};

		struct AttributeArrayData
		{
			AttributeArrayData(core::AttributeType type, unsigned int slot);

			core::AttributeType type;
			unsigned int slot;
		};

		class ConnectionDragDropHandler;

		class EventInstance :
			public gui::BaseController
		{
			typedef std::vector<unsigned int> SlotOffsetVector;
			typedef gui::DataContainer<SlotData> SlotContainer;
			typedef std::vector<SlotContainer*> DataVector;
			typedef gui::DataContainer<AttributeArrayData> ArrayContainer;
			typedef std::vector<ArrayContainer*> ArrayVector;
		public:
			EventInstance(gui::Module& module, gui::Widget& parent, core::Event& event, const core::EventGraph& graph, ConnectionDragDropHandler& handler);
			~EventInstance(void);

			unsigned int GetWidth(void) const;
			unsigned int GetOutputSlotPos(unsigned int slot) const;
			unsigned int GetInputSlotPos(void) const;
			unsigned int GetReturnSlotPos(unsigned int slot) const;
			unsigned int GetAttributeSlotPos(unsigned int slot) const;

			core::Signal<const SlotData&> SigEraseInConnection;
			core::Signal<const SlotData&> SigEraseOutConnection;

		private:

			unsigned int CreateAttributes(const core::Event& event, const core::EventDeclaration& declaration, const core::ConnectionReflector& connections, int& posY);
			unsigned int CreateOutputReturns(const core::Event& event, const core::EventDeclaration& declaration, const core::ConnectionReflector& connections, int& posY);
			void AddConnectionContext(gui::Widget& widget, core::ConnectionType type, unsigned int slot, bool isConnected);

			void OnDragWindow(const math::Vector2& vDistance);
			void OnRemoveConnection(SlotData data);
			void OnEditArray(AttributeArrayData data);

			core::Event* m_pEvent;
			SlotOffsetVector m_vOutputOffsets, m_vReturnOffsets, m_vAttributeOffsets;
			DataVector m_vData;
			ArrayVector m_vArrays;

			ConnectionDragDropHandler* m_pHandler;
		};

	}
}


