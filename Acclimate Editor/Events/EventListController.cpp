#include "EventListController.h"
#include "CreateEventController.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\ContextMenu.h"

namespace acl
{
	namespace editor
	{

		EventListController::EventListController(gui::Module& module, core::Events& events) :
			BaseController(module, L"../Editor/Menu/Events/EventExplorer.axm"), m_pEvents(&events)
		{
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			// events list
			m_pList = &AddWidget<EventList>(0.0f, 0.0f, 1.0f, 1.0f, 0.02f);
			m_pList->SigItemPicked.Connect(&SigPickedEvent, &core::Signal<core::EventInstance*>::operator());
			m_pList->SigKey.Connect(this, &EventListController::OnKey);

			m_pDock->AddChild(*m_pList);

			// TODO: memory-leak
			auto pListContext = new gui::ContextMenu(0.02f);
			auto& createItem = pListContext->AddItem(L"Create event");
			createItem.SigReleased.Connect(this, &EventListController::OnCreateEvent);

			m_pList->AddChild(*pListContext);
			m_pList->SetContextMenu(pListContext);
		}

		EventListController::~EventListController()
		{
		}

		void EventListController::OnRefresh(void)
		{
			const auto selected = m_pList->GetSelectedId();

			m_pList->Clear();
			for(auto& map : m_pEvents->Map())
			{
				m_pList->AddItem(map.first, map.second);
			}

			if(!m_pList->IsEmpty())
			{
				if(selected == -1)
					m_pList->SelectById(0);
				else
					m_pList->SelectById(selected);

				m_pList->OnFocus(true);
			}
			else
				SigPickedEvent(nullptr);
		}

		void EventListController::Dock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void EventListController::OnCreateEvent(void)
		{
			CreateEventController create(*m_pModule);

			std::wstring stName;
			if(create.Execute(&stName))
			{
				m_pEvents->Add(stName, *new core::EventInstance());

				OnRefresh();

				m_pList->SelectByName(stName);
			}
		}

		void EventListController::OnKey(gui::Keys key)
		{
			switch(key)
			{
			case gui::Keys::DELETE_KEY:
				if(!m_pList->IsEmpty())
				{
					m_pEvents->Delete(m_pList->GetSelectedName());
					OnRefresh();
				}
				break;
			}
		}


	}
}

