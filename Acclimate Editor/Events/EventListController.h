#pragma once
#include "Core\Events.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace gui
	{
		class Dock;
		class DockArea;

		enum class DockType;
	}

	namespace editor
	{

		class EventListController :
			public gui::BaseController
		{
			typedef gui::List<core::EventInstance*> EventList;
		public:
			EventListController(gui::Module& module, core::Events& events);
			~EventListController();

			void OnRefresh(void);

			void Dock(gui::DockArea& area, gui::DockType type, bool stack);

			core::Signal<core::EventInstance*> SigPickedEvent;

		private:

			void OnCreateEvent(void);
			void OnKey(gui::Keys key);

			gui::Dock* m_pDock;
			EventList* m_pList;

			core::Events* m_pEvents;
		};

	}
}


