#include "EventModule.h"
#include "EventController.h"
#include "Core\EventContext.h"
#include "Gui\BaseController.h"
#include "Gui\Menubar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Render\IRenderer.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		EventModule::EventModule(gui::BaseController& controller, gui::MenuBar& bar, core::EventContext& events, gfx::ILine& line, const render::IRenderer& render)
		{
			//m_pController = &controller.AddController<ScriptController>(script);

			auto& eventItem = bar.AddItem(L"EVENT");
			auto& eventOption = eventItem.AddOption(L"Edit events");
			//scriptOption.SigReleased.Connect(m_pController, &ScriptController::OnToggle);
			//scriptOption.SetShortcut(gui::ComboKeys::CTRL_ALT, 'S', gui::ShortcutState::ALWAYS);
			//scriptOption.SigShortcut.Connect(m_pController, &ScriptController::OnToggle);

			m_pController = new EventController(controller.GetModule(), events.events, line, *render.GetStage(L"eventsplines"));
			eventOption.SigReleased.Connect(m_pController, &EventController::OnToggle);
			eventOption.SetShortcut(gui::ComboKeys::CTRL_ALT, 'E', gui::ShortcutState::ALWAYS);
			eventOption.SigShortcut.Connect(m_pController, &EventController::OnToggle);
		}

		EventModule::~EventModule(void)
		{
			delete m_pController;
		}

		Project::ModuleType EventModule::GetModuleType(void) const
		{
			return Project::ModuleType::EVENTS;
		}

		void EventModule::Update(void)
		{
			m_pController->Update();
			m_pController->Draw();
		}

		void EventModule::OnNew(const std::wstring& stFile)
		{
		}

		void EventModule::OnLoad(const std::wstring& stFile)
		{
		}

		void EventModule::OnSave(const std::wstring& stFile)
		{
		}

		void EventModule::OnNewScene(const std::wstring& stScene, const std::wstring& stFile)
		{
			xml::Doc doc;
			
			doc.InsertNode(L"Events");

			doc.SaveFile(stFile);
		}

		void EventModule::OnSceneChanged(void)
		{
			m_pController->OnRefresh();
		}

		void EventModule::OnSaveScene(const std::wstring& stFile)
		{
			m_pController->OnSave(stFile);
		}

		void EventModule::OnClose(void)
		{
		}

		void EventModule::OnBeginTest(void)
		{
		}

		void EventModule::OnEndTest(void)
		{
		}

	}
}
