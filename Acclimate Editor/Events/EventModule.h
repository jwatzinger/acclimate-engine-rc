#pragma once
#include "..\IModule.h"

namespace acl
{
	namespace core
	{
		struct EventContext;
	}

	namespace gui
	{
		class BaseController;
		class MenuBar;
	}

	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class EventController;

		class EventModule :
			public IModule
		{
		public:
			EventModule(gui::BaseController& controller, gui::MenuBar& bar, core::EventContext& events, gfx::ILine& line, const render::IRenderer& render);
			~EventModule();

			Project::ModuleType GetModuleType(void) const;

			void Update(void) override;
			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override;
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			EventController* m_pController;

		};

	}
}

