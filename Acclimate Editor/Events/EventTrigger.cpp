#include "EventTrigger.h"
#include "Core\EventTrigger.h"
#include "Gui\Icon.h"
#include "Gui\Window.h"

namespace acl
{
	namespace editor
	{

		const unsigned int BORDER = 8;
		const unsigned int IO_SIZE = 16;
		const math::Rect rIcon(0, 0, 36, 36);

		EventTrigger::EventTrigger(gui::Module& module, gui::Widget& parent, core::EventTrigger& trigger) :
			BaseController(module, parent, L"../Editor/Menu/Events/Widget.axm"), m_pTrigger(&trigger)
		{
			// input
			//const bool isConnected = connections.IsConnected(core::ConnectionType::INPUT, 0);
			auto& icon = AddWidget<gui::Icon>(-(float)BORDER, (float)BORDER, (float)IO_SIZE, L"EventFlowEmptyIcon", rIcon);
			icon.SetPositionModes(gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
			icon.SetCenter(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			icon.SetAlignement(gui::HorizontalAlign::RIGHT, gui::VerticalAlign::TOP);
			//AddConnectionContext(icon, core::ConnectionType::INPUT, 0, isConnected);
			//icon.SetUserData(*new EventUserDataInput(event.GetId()));

			// modify window
			const auto& vPosition = trigger.GetPosition();
			auto pMain = GetWidgetByName<gui::Window>(L"Window");
			pMain->SetX((float)vPosition.x);
			pMain->SetY((float)vPosition.y);
			pMain->SetWidth(128.0f);
			pMain->SetHeight(64.0f);
			pMain->SetLabel(L"EventStartTrigger");

			pMain->SigDrag.Connect(this, &EventTrigger::OnDragWindow);
		}

		EventTrigger::~EventTrigger()
		{
		}

		void EventTrigger::OnDragWindow(const math::Vector2& vDistance)
		{
			m_pTrigger->SetPosition(math::Vector2((int)m_pMainWidget->GetRelX(), (int)m_pMainWidget->GetRelY()));
		}

	}
}
