#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		class EventTrigger;
	}

	namespace editor
	{

		class EventTrigger :
			public gui::BaseController
		{
		public:
			EventTrigger(gui::Module& module, gui::Widget& parent, core::EventTrigger& trigger);
			~EventTrigger();

		private:

			void OnDragWindow(const math::Vector2& vDistance);

			core::EventTrigger* m_pTrigger;
		};

	}
}


