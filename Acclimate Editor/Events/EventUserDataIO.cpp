#include "EventUSerDataIO.h"

namespace acl
{
	namespace editor
	{

		EventUserDataInput::EventUserDataInput(unsigned int event, core::TargetType type) : event(event),
			thisType(type)
		{
		}

		EventUserDataOutput::EventUserDataOutput(unsigned int event, unsigned int slot, core::TargetType type) : event(event), slot(slot),
			thisType(type)
		{
		}

		EventUserDataAttribute::EventUserDataAttribute(unsigned int event, unsigned int slot, core::AttributeType attribType, bool isArray, core::TargetType type) : event(event), 
			slot(slot), attribType(attribType), isArray(isArray), thisType(type)
		{
		}

		EventUserDataReturn::EventUserDataReturn(unsigned int event, unsigned int slot, core::AttributeType attribType, bool isArray, core::TargetType type) : event(event), 
			slot(slot), attribType(attribType), isArray(isArray), thisType(type)
		{
		}

		EventUserDataArea::EventUserDataArea(void) : pInstance(nullptr)
		{
		}

		EventUserDataVariable::EventUserDataVariable(core::EventVariable& variable) : pVariable(&variable)
		{
		}

	}
}

