#pragma once
#include "Gui\UserData.h"
#include "Math\Vector.h"

namespace acl
{
	namespace core
	{
		class EventInstance;
		class EventVariable;

		enum class AttributeType;
		enum class TargetType;
	}

	namespace editor
	{

		class EventUserDataInput :
			public gui::UserData<EventUserDataInput>
		{
		public:
			EventUserDataInput(unsigned int event, core::TargetType type);
			
			unsigned int event;
			core::TargetType thisType;
		};

		class EventUserDataOutput :
			public gui::UserData<EventUserDataOutput>
		{
		public:
			EventUserDataOutput(unsigned int event, unsigned int slot, core::TargetType type);

			unsigned int event, slot;
			core::TargetType thisType;
		};

		class EventUserDataAttribute :
			public gui::UserData<EventUserDataAttribute>
		{
		public:
			EventUserDataAttribute(unsigned int event, unsigned int slot, core::AttributeType attribType, bool isArray, core::TargetType type);

			unsigned int event, slot;
			core::AttributeType attribType;
			bool isArray;
			core::TargetType thisType;
		};

		class EventUserDataReturn :
			public gui::UserData<EventUserDataReturn>
		{
		public:
			EventUserDataReturn(unsigned int event, unsigned int slot, core::AttributeType attribType, bool isArray, core::TargetType type);

			unsigned int event, slot;
			core::AttributeType attribType;
			bool isArray;
			core::TargetType thisType;
		};

		class EventUserDataArea :
			public gui::UserData<EventUserDataArea>
		{
		public:
			EventUserDataArea(void);

			core::EventInstance* pInstance;
			math::Vector2 vOffset;
		};

		class EventUserDataVariable :
			public gui::UserData<EventUserDataVariable>
		{
		public:
			EventUserDataVariable(core::EventVariable& variable);

			core::EventVariable* pVariable;
		};

	}
}


