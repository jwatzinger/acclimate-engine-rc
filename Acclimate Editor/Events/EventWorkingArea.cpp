#include "EventWorkingArea.h"
#include "BaseEvent.h"
#include "BaseConnection.h"
#include "EventAddController.h"
#include "EventUserDataIO.h"
#include "ClipboardData.h"
#include "Core\EventCommand.h"
#include "Core\EventGraph.h"
#include "Core\EventLoader.h"
#include "Core\EventVariable.h"
#include "Gui\ContextMenu.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\Label.h"
#include "Gui\Image.h"

#include "Gfx\ILine.h"
#include "AddVariableController.h"

namespace acl
{
	namespace editor
	{

		const unsigned int CELL_SIZE = 32;
		const unsigned int COUNT_IN_CELL = 12;

		EventWorkingArea::EventWorkingArea(gui::Module& module, gfx::ILine& line, render::IStage& stage) : BaseController(module, L"../Editor/Menu/Events/WorkingArea.axm"),
			m_pLine(&line), m_pStage(&stage), m_pGraph(nullptr), m_isDirty(false), m_handler(*GetWidgetByName(L"WorkingArea")), m_pActiveEvent(nullptr)
		{
			auto pContextMenu = new gui::ContextMenu(0.02f);
			// events
			auto& item = pContextMenu->AddItem(L"Add event");
			item.SigReleased.Connect(this, &EventWorkingArea::OnAddEvent);
			// triggers
			auto& triggerItem = pContextMenu->AddItem(L"Add trigger");
			triggerItem.SigReleased.Connect(this, &EventWorkingArea::OnAddTrigger);
			// functions
			auto& functionItem = pContextMenu->AddItem(L"Add function");
			functionItem.SigReleased.Connect(this, &EventWorkingArea::OnAddFunction);

			pContextMenu->InsertSeperator();
			auto& gotoOrigin = pContextMenu->AddItem(L"Goto origin");
			gotoOrigin.SigReleased.Connect(this, &EventWorkingArea::OnGotoOrigin);

			m_pDock = GetWidgetByName<gui::Dock>(L"EventDock");

			// userdata
			m_pUserData = new EventUserDataArea();

			m_pArea = GetWidgetByName<gui::Image>(L"WorkingArea");
			m_pArea->AddChild(*pContextMenu);
			m_pArea->SetContextMenu(pContextMenu);
			m_pArea->SigDrag.Connect(this, &EventWorkingArea::OnScroll);
			m_pArea->SigKey.Connect(this, &EventWorkingArea::OnKey);
			m_pArea->SetUserData(*m_pUserData);
			m_pArea->SetFocusState(gui::FocusState::KEEP_FOCUS);

			// connect to drag handler
			m_handler.SigConnectionIO.Connect(this, &EventWorkingArea::OnMakeConnectionIO);
			m_handler.SigConnectionAttributes.Connect(this, &EventWorkingArea::OnMakeConnectionAttribute);
		}

		EventWorkingArea::~EventWorkingArea(void)
		{
			for(auto pConnection : m_vConnections)
			{
				delete pConnection;
			}

			delete m_pGraph;
		}

		void EventWorkingArea::SetEvent(core::EventInstance* pEvent)
		{
			if(m_pActiveEvent != pEvent)
			{
				m_pActiveEvent = pEvent;
				m_pUserData->pInstance = pEvent;
				OnRefresh();
			}
		}

		void EventWorkingArea::Update(void)
		{
			if(m_isDirty)
			{
				OnRefresh();
				m_isDirty = false;
			}

			for(auto pConnection : m_vConnections)
			{
				pConnection->Update();
			}
		}

		void EventWorkingArea::Dock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void EventWorkingArea::Draw(void)
		{
			if(GetMainWidget()->IsVisible())
			{
				DrawGrid();

				for(auto pConnection : m_vConnections)
				{
					pConnection->Draw();
				}

				m_handler.Draw(*m_pLine, *m_pStage);

				m_pArea->MarkDirtyRect();
			}
		}

		BaseConnection::Type attributeToConnectionType(core::AttributeType type)
		{
			switch(type)
			{
			case core::AttributeType::BOOL:
				return BaseConnection::Type::BOOL;
			case core::AttributeType::INT:
				return BaseConnection::Type::INT;
			case core::AttributeType::FLOAT:
				return BaseConnection::Type::FLOAT;
			case core::AttributeType::STRING:
				return BaseConnection::Type::STRING;
			case core::AttributeType::OBJECT:
				return BaseConnection::Type::OBJECT;
			default:
				ACL_ASSERT(false);
			}

			return BaseConnection::Type::IO;
		}

		void EventWorkingArea::OnRefresh(void)
		{
			// cleanup
			for(auto pConnection : m_vConnections)
			{
				delete pConnection;
			}
			m_vConnections.clear();

			for(auto pEvent : m_vEvents)
			{
				RemoveController(*pEvent);
			}
			m_vEvents.clear();
			m_mEvents.clear();

			delete m_pGraph;
			m_pGraph = nullptr;

			if(m_pActiveEvent)
			{
				m_pGraph = new core::EventGraph(*m_pActiveEvent);

				// events
				auto& mEvents = m_pActiveEvent->GetEvents();
				for(auto& event : mEvents)
				{
					// instance widget
					AddEvent(*event.second);
				}

				// connections
				for(auto& event : mEvents)
				{
					auto pLeftInstance = m_mEvents.at(event.first);
					ACL_ASSERT(pLeftInstance);

					// io-connections
					if(auto pOutputs = event.second->QueryOutput())
					{
						const auto numOutputs = pOutputs->GetNumOutputs();
						for(unsigned int i = 0; i < numOutputs; i++)
						{
							const auto targetId = pOutputs->GetOutputTarget(i);
							if(targetId != core::Event::NO_EVENT_ID && mEvents.count(targetId))
							{
								auto pTargetEvent = mEvents.at(targetId);
								auto pRightInstance = m_mEvents.at(targetId);
								ACL_ASSERT(pRightInstance);

								// left
								const ConnectionData left(*pLeftInstance, *event.second, pLeftInstance->GetOutputSlotPos(i), i);
								// right
								const ConnectionData right(*pRightInstance, *pTargetEvent, pRightInstance->GetInputSlotPos(), 0);

								auto pConnection = new BaseConnection(*m_pLine, *m_pStage, BaseConnection::Type::IO, left, right);
								AddConnection(*pConnection);
							}
						}
					}

					// attribute/return connections
					if(auto pReturns = event.second->QueryReturn())
					{
						const auto numReturns = pReturns->GetNumReturns();
						for(unsigned int i = 0; i < numReturns; i++)
						{
							const auto& vTargets = pReturns->GetReturnTargets(i);
							for(auto& target : vTargets)
							{
								const auto& targetId = target.first;

								if(targetId != core::Event::NO_EVENT_ID && mEvents.count(targetId))
								{
									auto pTargetEvent = mEvents.at(targetId);
									auto pRightInstance = m_mEvents.at(targetId);
									ACL_ASSERT(pRightInstance);
									auto pAttributes = pTargetEvent->QueryAttribute();
									ACL_ASSERT(pAttributes);
									auto& attribute = pAttributes->GetAttribute(target.second);

									// left
									const ConnectionData left(*pLeftInstance, *event.second, pLeftInstance->GetReturnSlotPos(i), i);
									// right
									const ConnectionData right(*pRightInstance, *pTargetEvent, pRightInstance->GetAttributeSlotPos(target.second), target.second);

									auto pConnection = new BaseConnection(*m_pLine, *m_pStage, attributeToConnectionType(attribute.GetType()), left, right);
									AddConnection(*pConnection);
								}
							}
						}
					}
				}
			}
		}

		void EventWorkingArea::OnAttachGetter(core::VariableGetter& getter)
		{
			getter.SetId(m_pActiveEvent->GenerateNextUID());
			m_pActiveEvent->AddEvent(getter);
			
			auto pReturn = getter.QueryReturn();
			
			auto& vTargets = pReturn->GetReturnTargets(0);
			ACL_ASSERT(vTargets.size() == 1);

			const auto targetPair = vTargets.front();
			const auto target = targetPair.first;
			const auto slot = targetPair.second;

			auto& event = m_pActiveEvent->GetEvent(target);
			auto pEventCtrl = m_mEvents.at(target);

			// make attribute connection
			auto pAttribute = event.QueryAttribute();
			auto& attribute = pAttribute->GetAttribute(slot);
			pAttribute->ConnectAttribute(slot, true);

			auto& getterCtrl = AddEvent(getter);

			getter.SetPosition(event.GetPosition() + math::Vector2(-32 - getterCtrl.GetWidth(), pEventCtrl->GetAttributeSlotPos(slot) - 24));

			auto& connections = m_pGraph->GetConnections(target);
			auto vConnections = connections.GetTargets(core::ConnectionType::ATTRIBUTE, slot);
			for(auto pConnection : vConnections)
			{
				auto& event = m_pActiveEvent->GetEvent(pConnection->target);
				auto pReturn = event.QueryReturn();
				ACL_ASSERT(pReturn);

				pReturn->RemoveReturn(pConnection->targetSlot, target);
			}
		}

		void EventWorkingArea::OnAttachSetter(unsigned int event, unsigned int slot, core::ConnectionType type, core::VariableSetter& setter)
		{
			setter.SetId(m_pActiveEvent->GenerateNextUID());
			m_pActiveEvent->AddEvent(setter);

			switch(type)
			{
			case core::ConnectionType::RETURN:
			{
				auto& ev = m_pActiveEvent->GetEvent(event);
				auto pReturn = ev.QueryReturn();
				ACL_ASSERT(pReturn);

				pReturn->AddReturn(slot, setter.GetId(), 0);

				auto pCtrl = m_mEvents.at(event);
				setter.SetPosition(ev.GetPosition() + math::Vector2(pCtrl->GetWidth() + 32, pCtrl->GetReturnSlotPos(slot) - 48));
				break;
			}
			case core::ConnectionType::OUTPUT:
			{
				auto& ev = m_pActiveEvent->GetEvent(event);
				auto pOutput = ev.QueryOutput();
				ACL_ASSERT(pOutput);

				pOutput->SetOutput(slot, setter.GetId());

				auto pCtrl = m_mEvents.at(event);
				setter.SetPosition(ev.GetPosition() + math::Vector2(pCtrl->GetWidth() + 32, pCtrl->GetOutputSlotPos(slot) - 24));

				break;
			}
			case core::ConnectionType::INPUT:
			{
				auto pOutput = setter.QueryOutput();
				ACL_ASSERT(pOutput);

				pOutput->SetOutput(slot, event);

				auto& connections = m_pGraph->GetConnections(event);
				auto vConnections = connections.GetTargets(core::ConnectionType::INPUT, slot);
				for(auto pConnection : vConnections)
				{
					auto& ev = m_pActiveEvent->GetEvent(pConnection->target);
					auto pOutput = ev.QueryOutput();
					ACL_ASSERT(pOutput);

					pOutput->SetOutput(pConnection->targetSlot, core::EventCommand::NO_EVENT_ID);
				}
					
				auto& ev = m_pActiveEvent->GetEvent(event);
				auto& ctrl = AddEvent(setter);
				setter.SetPosition(ev.GetPosition() + math::Vector2(-32 - ctrl.GetWidth(), ctrl.GetOutputSlotPos(0) - 24));

				break;
			}
			case core::ConnectionType::ATTRIBUTE:
				ACL_ASSERT(false);
			default:
				ACL_ASSERT(false);
			}
		}

		void EventWorkingArea::DrawGrid(void)
		{
			// draw grid
			gfx::PointVector vPoints;
			vPoints.reserve(32);
			const auto lineWidth = m_pMainWidget->GetWidth();
			const auto lineHeight = m_pMainWidget->GetHeight();
			const auto offX = m_vOffset.x % 32;
			const auto offY = m_vOffset.y % 32;
			const auto lineOffX = ((m_vOffset.x % (CELL_SIZE * COUNT_IN_CELL)) / CELL_SIZE) * CELL_SIZE;
			const auto lineOffY = ((m_vOffset.y % (CELL_SIZE * COUNT_IN_CELL)) / CELL_SIZE) * CELL_SIZE;
			for(int i = 0; i <= lineWidth; i += 32)
			{
				vPoints.emplace_back((float)i + offX, 0.0f, 0.5f);
				vPoints.emplace_back((float)i + offX, (float)lineHeight, 0.4f);

				// calculate color
				const auto realLinePos = i - lineOffX;
				if((realLinePos % (CELL_SIZE * COUNT_IN_CELL)) == 0)
					m_pLine->SetColor(gfx::Color(125, 125, 125));
				else if((realLinePos % (CELL_SIZE * COUNT_IN_CELL / 2)) == 0)
					m_pLine->SetColor(gfx::Color(0, 0, 0));
				else
					m_pLine->SetColor(gfx::Color(125, 125, 125));
				m_pLine->DrawMulti(vPoints, *m_pStage);
				vPoints.clear();
			}
			for(int i = 0; i <= lineHeight; i += 32)
			{
				vPoints.emplace_back(0.0f, (float)i + offY, 0.5f);
				vPoints.emplace_back((float)lineWidth, (float)i + offY, 0.4f);

				const auto realLinePos = i - lineOffY;
				if((realLinePos % (CELL_SIZE * COUNT_IN_CELL)) == 0)
					m_pLine->SetColor(gfx::Color(125, 125, 125));
				else if((realLinePos % (CELL_SIZE * COUNT_IN_CELL / 2)) == 0)
					m_pLine->SetColor(gfx::Color(0, 0, 0));
				else
					m_pLine->SetColor(gfx::Color(125, 125, 125));
				m_pLine->DrawMulti(vPoints, *m_pStage);
				vPoints.clear();
			}
		}

		BaseEvent& EventWorkingArea::AddEvent(core::EventCommand& event)
		{
			// instance widget
			auto& instance = AddController<BaseEvent>(*m_pArea, *m_pActiveEvent, event, *m_pGraph, m_handler);
			instance.SetOffset(m_vOffset);
			instance.SigInvalidate.Connect(this, &EventWorkingArea::OnInvalidate);
			instance.SigAppendEvent.Connect(this, &EventWorkingArea::OnAppendEvent);
			instance.SigFocus.Connect(this, &EventWorkingArea::OnFocusPoint);
			m_mEvents.emplace(event.GetId(), &instance);

			m_vEvents.push_back(&instance);

			return instance;
		}

		void EventWorkingArea::OnKey(gui::Keys key)
		{
			switch(key)
			{
			case gui::Keys::V:
				if(m_pModule->ComboKeysActive(gui::ComboKeys::CTRL))
				{
					if(auto pData = m_pModule->m_clipboard.GetCurrentData<EventClipboardData>())
					{
						auto& event = pData->GetCommand().Clone();
						event.SetId(m_pActiveEvent->GenerateNextUID());
						
						if(auto pOutput = event.QueryOutput())
							pOutput->ClearOutput();
						if(auto pReturn = event.QueryReturn())
							pReturn->ClearReturnTargets();
						m_pActiveEvent->AddEvent(event);

						AddEvent(event);
					}
				}
			}
		}

		void EventWorkingArea::OnAddEvent(void)
		{
			const math::Vector2 vCursorPos = m_pModule->m_cursor.GetPosition() - m_vOffset;
			EventAddController controller(*m_pModule, ItemType::COMMAND);

			std::wstring stEvent;
			if(controller.Execute(&stEvent, nullptr))
			{
				auto pWorkingArea = GetWidgetByName(L"WorkingArea");
				const math::Vector2 vAreaPos(pWorkingArea->GetX(), pWorkingArea->GetY());

				auto& event = core::EventLoader::Add(stEvent, *m_pActiveEvent);
				event.SetPosition(vCursorPos - vAreaPos);

				AddEvent(event);
			}
		}

		void EventWorkingArea::OnAddTrigger(void)
		{
			const math::Vector2 vCursorPos = m_pModule->m_cursor.GetPosition() - m_vOffset;
			EventAddController controller(*m_pModule, ItemType::TRIGGER);

			std::wstring stTrigger;
			if(controller.Execute(&stTrigger, m_pActiveEvent))
			{
				auto pWorkingArea = GetWidgetByName(L"WorkingArea");
				const math::Vector2 vAreaPos(pWorkingArea->GetX(), pWorkingArea->GetY());

				auto& trigger = core::EventLoader::AddTrigger(stTrigger, *m_pActiveEvent);
				trigger.SetPosition(vCursorPos - vAreaPos);

				AddEvent(trigger);
			}
		}

		void EventWorkingArea::OnAddFunction(void)
		{
			const math::Vector2 vCursorPos = m_pModule->m_cursor.GetPosition() - m_vOffset;
			EventAddController controller(*m_pModule, ItemType::FUNCTION);

			std::wstring stTrigger;
			if(controller.Execute(&stTrigger, m_pActiveEvent))
			{
				auto pWorkingArea = GetWidgetByName(L"WorkingArea");
				const math::Vector2 vAreaPos(pWorkingArea->GetX(), pWorkingArea->GetY());

				auto& function = core::EventLoader::AddFunction(stTrigger, *m_pActiveEvent);
				function.SetPosition(vCursorPos - vAreaPos);

				AddEvent(function);
			}
		}

		void EventWorkingArea::AddConnection(BaseConnection& connection)
		{
			connection.SetOffset(m_vOffset);
			connection.SigInvalidate.Connect(this, &EventWorkingArea::OnInvalidate);
			m_vConnections.push_back(&connection);
		}

		void EventWorkingArea::SetOffset(const math::Vector2& vOffset)
		{
			if(m_vOffset != vOffset)
			{
				m_vOffset = vOffset;
				if(m_pUserData)
					m_pUserData->vOffset = m_vOffset;

				for(auto pEvent : m_vEvents)
				{
					pEvent->SetOffset(m_vOffset);
				}

				for(auto pConnection : m_vConnections)
				{
					pConnection->SetOffset(m_vOffset);
				}

				// label
				const std::wstring stLabel = L"X: " + conv::ToString(m_vOffset.x) + L"/ Y: " + conv::ToString(m_vOffset.y);

				GetWidgetByName<gui::Label>(L"Positions")->SetString(stLabel);
			}
		}

		void EventWorkingArea::OnMakeConnectionIO(unsigned int outEvent, unsigned int outSlot, unsigned int inEvent, core::TargetType type)
		{
			ACL_ASSERT(m_pActiveEvent);

			auto& command = m_pActiveEvent->GetEvent(outEvent);
			auto pOutput = command.QueryOutput();
			ACL_ASSERT(pOutput);

			if(pOutput->GetOutputTarget(outSlot) == inEvent)
				return;
			else
				pOutput->SetOutput(outSlot, inEvent);

			// remove pending output connections
			auto& connections = m_pGraph->GetConnections(inEvent);
			auto& vConnections = connections.GetTargets(core::ConnectionType::INPUT, 0);
			if(!vConnections.empty())
			{
				ACL_ASSERT(vConnections.size() == 1);
				auto pInConnection = vConnections[0];

				auto& inCommand = m_pActiveEvent->GetEvent(pInConnection->target);
				auto pInOutput = inCommand.QueryOutput();
				pInOutput->SetOutput(pInConnection->targetSlot, core::Event::NO_EVENT_ID);
			}
			
			m_isDirty = true;
		}

		void EventWorkingArea::OnMakeConnectionAttribute(unsigned int outEvent, unsigned int outSlot, unsigned int inEvent, unsigned int inSlot, core::TargetType type)
		{
			ACL_ASSERT(m_pActiveEvent);

			auto& command = m_pActiveEvent->GetEvent(outEvent);
			auto pReturn = command.QueryReturn();
			ACL_ASSERT(pReturn);

			pReturn->AddReturn(outSlot, inEvent, inSlot);

			// remove pending return connections
			auto& connections = m_pGraph->GetConnections(inEvent);
			auto& vConnections = connections.GetTargets(core::ConnectionType::ATTRIBUTE, inSlot);

			for(auto pConnection : vConnections)
			{
				auto& inCommand = m_pActiveEvent->GetEvent(pConnection->target);
				auto pInReturn = inCommand.QueryReturn();
				ACL_ASSERT(pInReturn);

				pInReturn->RemoveReturn(pConnection->targetSlot, inEvent);
			}

			// set attribute connection
			auto& attributeEvent = m_pActiveEvent->GetEvent(inEvent);
			auto pAttributes = attributeEvent.QueryAttribute();
			ACL_ASSERT(pAttributes);
			pAttributes->ConnectAttribute(inSlot, true);

			m_isDirty = true;
		}

		void EventWorkingArea::OnScroll(const math::Vector2& vOffset)
		{
			SetOffset(m_vOffset - vOffset);
		}

		void EventWorkingArea::OnGotoOrigin(void)
		{
			m_vOffset = math::Vector2(0, 0);
			if(m_pUserData)
				m_pUserData->vOffset = m_vOffset;
			OnScroll(math::Vector2(0, 0));
		}

		void EventWorkingArea::OnInvalidate(void)
		{
			m_isDirty = true;
		}

		const unsigned int APPEND_GAP = 32;

		void EventWorkingArea::OnAppendEvent(unsigned int id, unsigned int slot, core::ConnectionType type, const math::Vector2& vSize)
		{
			math::Vector2 vPosition;
			switch(type)
			{
			case core::ConnectionType::OUTPUT:
			{
				EventAddController controller(*m_pModule, ItemType::COMMAND);

				std::wstring stEvent;
				if(controller.Execute(&stEvent, nullptr))
				{
					auto& targetEvent = m_pActiveEvent->GetEvent(id);
					vPosition = targetEvent.GetPosition();

					const int offset = m_mEvents.at(id)->GetOutputSlotPos(slot);
					vPosition += math::Vector2(vSize.x + APPEND_GAP, offset - 40);

					auto& event = core::EventLoader::Add(stEvent, *m_pActiveEvent);

					event.SetPosition(vPosition);

					// make connection
					auto pOutput = targetEvent.QueryOutput();
					ACL_ASSERT(pOutput);
					pOutput->SetOutput(slot, event.GetId());

					OnInvalidate();
				}
				else
					return;
				break;
			}
			case core::ConnectionType::INPUT:
			{
				EventAddController controller(*m_pModule, ItemType::TRIGGER);

				std::wstring stTrigger;
				if(controller.Execute(&stTrigger, m_pActiveEvent))
				{
					auto& targetEvent = m_pActiveEvent->GetEvent(id);
					vPosition = targetEvent.GetPosition();

					auto& trigger = core::EventLoader::AddTrigger(stTrigger, *m_pActiveEvent);
					auto& triggerCtrl = AddEvent(trigger);
					const auto width = triggerCtrl.GetWidth();

					vPosition -= math::Vector2(APPEND_GAP + width, 0);

					trigger.SetPosition(vPosition);

					// make connection
					trigger.SetOutput(0, id);

					OnInvalidate();
				}
				else
					return;
				break;
			}
			case core::ConnectionType::ATTRIBUTE:
			{
				auto& targetEvent = m_pActiveEvent->GetEvent(id);

				auto pAttributes = targetEvent.QueryAttribute();
				ACL_ASSERT(pAttributes);

				auto& attribute = pAttributes->GetAttribute(slot);
				AddVariableController create(*m_pModule, attribute.GetType(), attribute.IsArray());

				std::wstring stName;
				core::AttributeType type;
				bool isArray, isConst;
				unsigned int objectType;
				if(create.Execute(&stName, &type, &objectType, &isArray, &isConst))
				{
					vPosition = targetEvent.GetPosition();
					const int offset = m_mEvents.at(id)->GetAttributeSlotPos(slot);

					core::EventVariable* pVariable;
					if(type == core::AttributeType::OBJECT)
						pVariable = new core::EventVariable(stName, objectType, isArray, m_pActiveEvent->GenerateNextVariableUID());
					else
						pVariable = new core::EventVariable(stName, type, isConst, isArray, m_pActiveEvent->GenerateNextVariableUID());
					m_pActiveEvent->AddVariable(*pVariable);

					// make connection
					auto& getter = pVariable->CreateGetter();
					getter.SetId(m_pActiveEvent->GenerateNextUID());
					m_pActiveEvent->AddEvent(getter);
					auto& getterCtrl = AddEvent(getter);

					vPosition -= math::Vector2(APPEND_GAP + getterCtrl.GetWidth(), -offset + 24);

					getter.SetPosition(vPosition);

					auto pReturn = getter.QueryReturn();
					ACL_ASSERT(pReturn);
					pReturn->AddReturn(0, id, slot);

					// set attribute connection
					pAttributes->ConnectAttribute(slot, true);

					SigVariableAdded();
					OnInvalidate();
				}
				else
					return;
				break;
			}
			case core::ConnectionType::RETURN:
			{
				auto& targetEvent = m_pActiveEvent->GetEvent(id);

				auto pReturns = targetEvent.QueryReturn();
				ACL_ASSERT(pReturns);

				EventAddController controller(*m_pModule, ItemType::FUNCTION);
			
				// set filter
				auto& ret = pReturns->GetReturnDeclaration()[slot];
				controller.SetFilter(FilterType::ATTRIBUTES, ret.GetType(), ret.IsArray());

				std::wstring stName;
				if(controller.Execute(&stName, nullptr))
				{
					auto& function = core::EventLoader::AddFunction(stName, *m_pActiveEvent);
					vPosition = targetEvent.GetPosition();

					auto pCtrl = m_mEvents.at(id);
					vPosition += math::Vector2(pCtrl->GetWidth() + 32, pCtrl->GetReturnSlotPos(0)-40);

					function.SetPosition(vPosition);

					auto pReturn = targetEvent.QueryReturn();
					ACL_ASSERT(pReturn);
					pReturn->AddReturn(slot, function.GetId(), 0);

					function.ConnectAttribute(0, true);

					OnInvalidate();
				}
				else
					return;
				break;
			}
			default:
				ACL_ASSERT(false);
			}

			OnFocusPoint(vPosition);
		}

		void EventWorkingArea::OnFocusPoint(const math::Vector2& vPosition)
		{
			SetOffset(math::Vector2(m_pArea->GetWidth() / 2, m_pArea->GetHeight() / 2) - vPosition);
		}

	}
}
