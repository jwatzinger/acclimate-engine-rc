#pragma once
#include "ConnectionDragDropHandler.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		class EventGraph;
		class EventInstance;
		class EventCommand;
		class EventTrigger;
		class EventVariable;
		class VariableGetter;
		class VariableSetter;

		enum class ConnectionType;
	}

	namespace gui
	{
		class DockArea;
		class Dock;
		class Image;

		enum class DockType;
	}

	namespace gfx
	{
		class ILine;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class BaseConnection;
		class BaseEvent;
		class EventUserDataArea;

		class EventWorkingArea :
			public gui::BaseController
		{
			typedef std::unordered_map<unsigned int, BaseEvent*> EventMap;
			typedef std::vector<BaseEvent*> EventVector;
			typedef std::vector<BaseConnection*> ConnectionVector;
		public:
			EventWorkingArea(gui::Module& module, gfx::ILine& line, render::IStage& stage);
			~EventWorkingArea(void);

			void SetEvent(core::EventInstance* pEvent);

			void Update(void) override;
			void Dock(gui::DockArea& area, gui::DockType type, bool stack);
			void Draw(void);

			void OnRefresh(void);
			void OnAttachGetter(core::VariableGetter& getter);
			void OnAttachSetter(unsigned int event, unsigned int slot, core::ConnectionType type, core::VariableSetter& setter);

			core::Signal<> SigVariableAdded;

		private:

			void DrawGrid(void);
			BaseEvent& AddEvent(core::EventCommand& event);
			void AddConnection(BaseConnection& connection);
			void SetOffset(const math::Vector2& vOffset);

			void OnKey(gui::Keys key);
			void OnAddEvent(void);
			void OnAddTrigger(void);
			void OnAddFunction(void);
			void OnMakeConnectionIO(unsigned int outEvent, unsigned int outSlot, unsigned int inEvent, core::TargetType type);
			void OnMakeConnectionAttribute(unsigned int outEvent, unsigned int outSlot, unsigned int inEvent, unsigned int inSlot, core::TargetType type);
			void OnScroll(const math::Vector2& vOffset);
			void OnGotoOrigin(void);
			void OnInvalidate(void);
			void OnAppendEvent(unsigned int id, unsigned int slot, core::ConnectionType type, const math::Vector2& vSize);
			void OnFocusPoint(const math::Vector2& vPosition);

			bool m_isDirty;
			math::Vector2 m_vOffset;
			EventMap m_mEvents;
			EventVector m_vEvents;
			ConnectionVector m_vConnections;
			const core::EventGraph* m_pGraph;
			core::EventInstance* m_pActiveEvent;

			gui::Image* m_pArea;
			gui::Dock* m_pDock;
			ConnectionDragDropHandler m_handler;
			EventUserDataArea* m_pUserData;

			gfx::ILine* m_pLine;
			render::IStage* m_pStage;
			
			core::Signal<bool> SigEntityLoaded;
		};

	}
}

