#include "TypeBox.h"
#include "Core\EventAttribute.h"
#include "Core\EventLoader.h"

namespace acl
{
	namespace editor
	{
		namespace
		{
			std::wstring variableTypeToIcon(core::AttributeType type)
			{
				switch(type)
				{
				case core::AttributeType::BOOL:
					return L"EventVariableBool";
				case core::AttributeType::FLOAT:
					return L"EventVariableFloat";
				case core::AttributeType::INT:
					return L"EventVariableInt";
				case core::AttributeType::STRING:
					return L"EventVariableString";
				case core::AttributeType::OBJECT:
					return L"EventVariableObject";
				default:
					ACL_ASSERT(false);
				}

				return L"";
			}

			std::wstring variableTypeToName(core::AttributeType type)
			{
				switch(type)
				{
				case core::AttributeType::BOOL:
					return L"Bool";
				case core::AttributeType::FLOAT:
					return L"Float";
				case core::AttributeType::INT:
					return L"Int";
				case core::AttributeType::STRING:
					return L"String";
				default:
					ACL_ASSERT(false);
				}

				return L"";
			}
		}

		void addItem(TypeBox& box, core::AttributeType type)
		{
			auto& item = box.AddItem(variableTypeToName(type), std::make_pair(type, -1));
			item.SetIcon(variableTypeToIcon(type), math::Rect(0, 0, 36, 36));
		}

		void fillTypeBox(TypeBox& box)
		{
			box.Clear();

			addItem(box, core::AttributeType::BOOL);
			addItem(box, core::AttributeType::INT);
			addItem(box, core::AttributeType::FLOAT);
			addItem(box, core::AttributeType::STRING);

			for(auto& object : core::EventLoader::GetRegisteredObjects())
			{
				auto& item = box.AddItem(object.first, std::make_pair(core::AttributeType::OBJECT, object.second));
				item.SetIcon(variableTypeToIcon(core::AttributeType::OBJECT), math::Rect(0, 0, 36, 36));
			}

			box.SelectByItem(std::make_pair(core::AttributeType::BOOL, -1));
		}

	}
}