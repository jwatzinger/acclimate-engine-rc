#pragma once
#include "Gui\ComboBox.h"

namespace acl
{
	namespace core
	{
		enum class AttributeType;
	}

	namespace editor
	{

		typedef gui::ComboBox<std::pair<core::AttributeType, unsigned int>> TypeBox;

		void fillTypeBox(TypeBox& box);
	}
}