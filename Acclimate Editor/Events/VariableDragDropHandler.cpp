#include "VariableDragDropHandler.h"
#include "EventUserDataIO.h"
#include "Core\EventGraph.h"
#include "Core\EventVariable.h"
#include "Core\EventInstance.h"
#include "Gui\Widget.h"
#include "Gui\Module.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		VariableDragDropHandler::VariableDragDropHandler(void) : m_pVariable(nullptr)
		{
		}

		void VariableDragDropHandler::OnBegin(gui::Widget& dragWidget)
		{
			auto pUserData = dragWidget.GetUserData<EventUserDataVariable>();
			ACL_ASSERT(pUserData);
			m_pVariable = pUserData->pVariable;
		}

		void VariableDragDropHandler::OnDrag(const math::Vector2& vMousePos)
		{
			m_vMousePos = vMousePos;
		}

		void VariableDragDropHandler::OnDrop(gui::Widget& targetWidget)
		{
			auto modifierPressed = targetWidget.GetModule()->ComboKeysActive(gui::ComboKeys::CTRL);
			if(auto pUserData = targetWidget.GetUserData<EventUserDataArea>())
			{
				core::EventCommand* pEvent = nullptr;
				if(modifierPressed)
					pEvent = &m_pVariable->CreateSetter();
				else
					pEvent = &m_pVariable->CreateGetter();

				pEvent->SetId(pUserData->pInstance->GenerateNextUID());
				pEvent->SetPosition(m_vMousePos - math::Vector2(targetWidget.GetX(), targetWidget.GetY()) - pUserData->vOffset);

				pUserData->pInstance->AddEvent(*pEvent);
				m_pVariable = nullptr;

				SigGetterAdded();
			}
			else if(modifierPressed)
			{
				if(auto pUserData = targetWidget.GetUserData<EventUserDataReturn>())
				{
					if(pUserData->attribType == m_pVariable->GetType() && pUserData->isArray == m_pVariable->IsArray())
					{
						auto& setter = m_pVariable->CreateSetter();
						setter.SetPosition(m_vMousePos - math::Vector2(targetWidget.GetX(), targetWidget.GetY()));

						SigSetterAttached(pUserData->event, pUserData->slot, core::ConnectionType::RETURN, setter);
						SigGetterAdded();
					}
				}
				else if(auto pUserData = targetWidget.GetUserData<EventUserDataOutput>())
				{
					auto& setter = m_pVariable->CreateSetter();
					setter.SetPosition(m_vMousePos - math::Vector2(targetWidget.GetX(), targetWidget.GetY()));

					SigSetterAttached(pUserData->event, pUserData->slot, core::ConnectionType::OUTPUT, setter);
					SigGetterAdded();
				}
				else if(auto pUserData = targetWidget.GetUserData<EventUserDataInput>())
				{
					auto& setter = m_pVariable->CreateSetter();
					setter.SetPosition(m_vMousePos - math::Vector2(targetWidget.GetX(), targetWidget.GetY()));

					SigSetterAttached(pUserData->event, 0, core::ConnectionType::INPUT, setter);
					SigGetterAdded();
				}
			}
			else if(auto pUserData = targetWidget.GetUserData<EventUserDataAttribute>())
			{
				if(pUserData->attribType == m_pVariable->GetType() && pUserData->isArray == m_pVariable->IsArray())
				{
					auto& getter = m_pVariable->CreateGetter();
					getter.SetPosition(m_vMousePos - math::Vector2(targetWidget.GetX(), targetWidget.GetY()));

					auto pReturn = getter.QueryReturn();
					ACL_ASSERT(pReturn);
					pReturn->AddReturn(0, pUserData->event, pUserData->slot);

					m_pVariable = nullptr;

					SigGetterAttached(getter);
					SigGetterAdded();
				}
			}
		}

	}
}

