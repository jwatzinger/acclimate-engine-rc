#pragma once
#include "Core\Signal.h"
#include "Gui\IDragDropHandler.h"
#include "Math\Vector.h"

namespace acl
{
	namespace core
	{
		class EventVariable;
		class VariableGetter;
		class VariableSetter;

		enum class ConnectionType;
	}

	namespace editor
	{

		class VariableDragDropHandler :
			public gui::IDragDropHandler
		{
		public:

			VariableDragDropHandler(void);

			void OnBegin(gui::Widget& dragWidget) override;
			void OnDrag(const math::Vector2& vMousePos) override;
			void OnDrop(gui::Widget& targetWidget) override;

			core::Signal<> SigGetterAdded;
			core::Signal<core::VariableGetter&> SigGetterAttached;
			core::Signal<unsigned int, unsigned int, core::ConnectionType, core::VariableSetter&> SigSetterAttached;

		private:

			core::EventVariable* m_pVariable;
			math::Vector2 m_vMousePos;
		};

	}
}


