#include "VariableExplorer.h"
#include "AddVariableController.h"
#include "EventUserDataIO.h"
#include "Core\EventInstance.h"
#include "Core\EventVariable.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\ContextMenu.h"

namespace acl
{
	namespace editor
	{

		std::wstring variableTypeToIcon(core::AttributeType type, bool isArray)
		{
			if(isArray)
			{
				switch(type)
				{
				case core::AttributeType::BOOL:
					return L"EventInputArrayBoolIcon";
				case core::AttributeType::FLOAT:
					return L"EventInputArrayFloatIcon";
				case core::AttributeType::INT:
					return L"EventInputArrayIntIcon";
				case core::AttributeType::STRING:
					return L"EventInputArrayStringIcon";
				case core::AttributeType::OBJECT:
					return L"EventInputArrayObjectIcon";
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(type)
				{
				case core::AttributeType::BOOL:
					return L"EventVariableBool";
				case core::AttributeType::FLOAT:
					return L"EventVariableFloat";
				case core::AttributeType::INT:
					return L"EventVariableInt";
				case core::AttributeType::STRING:
					return L"EventVariableString";
				case core::AttributeType::OBJECT:
					return L"EventVariableObject";
				default:
					ACL_ASSERT(false);
				}
			}
			
			return L"";
		}

		VariableExplorer::VariableExplorer(gui::Module& module) :
			BaseController(module, L"../Editor/Menu/Events/VariableExplorer.axm"), m_pInstance(nullptr)
		{
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			// events list
			m_pList = &AddWidget<EventList>(0.0f, 0.0f, 1.0f, 1.0f, 0.02f);
			m_pList->SigItemPicked.Connect(&SigPickedVariable, &core::Signal<core::EventVariable*>::operator());
			m_pList->SigKey.Connect(this, &VariableExplorer::OnKey);

			m_pDock->AddChild(*m_pList);

			// TODO: memory-leak
			auto pListContext = new gui::ContextMenu(0.02f);
			auto& createItem = pListContext->AddItem(L"Add variable");
			createItem.SigReleased.Connect(this, &VariableExplorer::OnAddVariable);

			m_pList->AddChild(*pListContext);
			m_pList->SetContextMenu(pListContext);

			m_handler.SigGetterAdded.Connect(&SigGetterAdded, &core::Signal<>::operator());
			m_handler.SigGetterAttached.Connect(&SigGetterAttached, &core::Signal<core::VariableGetter&>::operator());
			m_handler.SigSetterAttached.Connect(&SigSetterAttached, &core::Signal<unsigned int, unsigned int, core::ConnectionType, core::VariableSetter&>::operator());
		}

		VariableExplorer::~VariableExplorer(void)
		{
		}

		void VariableExplorer::SetEvent(core::EventInstance* pInstance)
		{
			m_pInstance = pInstance;
			OnRefresh();
		}

		void VariableExplorer::OnRefresh(void)
		{
			const auto selected = m_pList->GetSelectedId();

			m_pList->Clear();

			if(m_pInstance)
			{
				for(auto& map : m_pInstance->GetVariables())
				{
					auto pVariable = map.second;
					auto& item = m_pList->AddItem(pVariable->GetName(), pVariable);
					auto pUserData = new EventUserDataVariable(*pVariable);
					item.SetUserData(*pUserData);
					item.SetDragDrop(&m_handler);

					// icon
					const auto isArray = pVariable->IsArray();
					auto pIcon = item.SetIcon(variableTypeToIcon(pVariable->GetType(), isArray), math::Rect(0, 0, 36, 36));
					ACL_ASSERT(pIcon);
					if(isArray)
						pIcon->SetPadding(10, 2, 0, 8);
				}

				if(!m_pList->IsEmpty())
				{
					if(selected == -1)
						m_pList->SelectById(0);
					else
						m_pList->SelectById(selected);

					m_pList->OnFocus(true);
				}
				else
					SigPickedVariable(nullptr);
			}
			else
				SigPickedVariable(nullptr);
		}

		void VariableExplorer::Dock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void VariableExplorer::OnAddVariable(void)
		{
			AddVariableController create(*m_pModule);

			std::wstring stName;
			core::AttributeType type;
			bool isArray, isConst;
			unsigned int objectType = -1;
			if(create.Execute(&stName, &type, &objectType, &isArray, &isConst))
			{
				core::EventVariable* pVariable;
				if(type == core::AttributeType::OBJECT)
					pVariable = new core::EventVariable(stName, objectType, isArray, m_pInstance->GenerateNextVariableUID());
				else
					pVariable = new core::EventVariable(stName, type, isConst, isArray, m_pInstance->GenerateNextVariableUID());

				m_pInstance->AddVariable(*pVariable);

				OnRefresh();

				m_pList->SelectByName(stName);
			}
		}

		void VariableExplorer::OnKey(gui::Keys key)
		{
			switch(key)
			{
			case gui::Keys::DELETE_KEY:
				if(!m_pList->IsEmpty())
				{
					m_pInstance->RemoveVariable(*m_pList->GetContent());
					OnRefresh();
				}
				break;
			}
		}

	}
}

