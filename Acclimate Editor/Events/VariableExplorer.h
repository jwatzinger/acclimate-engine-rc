#pragma once
#include "VariableDragDropHandler.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace core
	{
		class EventVariable;
		class EventInstance;
	}

	namespace gui
	{
		class Dock;
		class DockArea;

		enum class DockType;
	}

	namespace editor
	{

		class VariableExplorer :
			public gui::BaseController
		{
			typedef gui::List<core::EventVariable*> EventList;
		public:
			VariableExplorer(gui::Module& module);
			~VariableExplorer(void);

			void SetEvent(core::EventInstance* pInstance);
			void OnRefresh(void);

			void Dock(gui::DockArea& area, gui::DockType type, bool stack);

			core::Signal<core::EventVariable*> SigPickedVariable;
			core::Signal<> SigGetterAdded;
			core::Signal<core::VariableGetter&> SigGetterAttached;
			core::Signal<unsigned int, unsigned int, core::ConnectionType, core::VariableSetter&> SigSetterAttached;

		private:

			void OnAddVariable(void);
			void OnKey(gui::Keys key);

			gui::Dock* m_pDock;
			EventList* m_pList;
			VariableDragDropHandler m_handler;

			core::EventInstance* m_pInstance;
		};

	}
}


