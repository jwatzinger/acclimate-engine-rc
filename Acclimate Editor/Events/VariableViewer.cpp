#include "VariableViewer.h"
#include "EventInputBox.h"
#include "ArrayEditArea.h"
#include "Core\EventVariable.h"
#include "Core\EventAttribute.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\CheckBox.h"

namespace acl
{
	namespace editor
	{

		VariableViewer::VariableViewer(gui::Module& module) :
			BaseController(module, L"../Editor/Menu/Events/VariableViewer.axm"), m_pVariable(nullptr), m_pInput(nullptr), 
			m_pArea(nullptr)
		{
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			// name box
			m_pName = &AddWidget<NameBox>(0.0f, 0.0f, 1.0f, 21.0f, L"");
			m_pName->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pName->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pName->SigContentSet.Connect(this, &VariableViewer::OnSetName);

			// combo box
			m_pType = &AddWidget<TypeBox>(0.0f, 26.0f, 1.0f, 21.0f, 10.0f);
			m_pType->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pType->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);

			fillTypeBox(*m_pType);

			m_pType->SigItemPicked.Connect(this, &VariableViewer::OnSetType);

			// checkbox
			m_pConst = &AddWidget<gui::CheckBox>(0.0f, 52.0f, 21.0f);
			m_pConst->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pConst->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pConst->SigChecked.Connect(this, &VariableViewer::OnSetConst);
		}

		VariableViewer::~VariableViewer(void)
		{
		}

		void VariableViewer::SetVariable(core::EventVariable* pVariable)
		{
			if(m_pVariable != pVariable)
			{
				auto pArea = GetWidgetByName(L"Area");

				m_pVariable = pVariable;

				if(m_pVariable)
				{
					const auto type = m_pVariable->GetType();
					if(type == core::AttributeType::OBJECT)
						m_pType->SelectByItem(std::make_pair(core::AttributeType::OBJECT, m_pVariable->GetObjectType()));
					else
						m_pType->SelectByItem(std::make_pair(type, -1));

					pArea->SetVisible(true);
					OnRefresh();

					// value controller
					CreateValueBox();
				}
				else
					pArea->SetVisible(false);
			}
		}

		void VariableViewer::OnRefresh(void)
		{
			if(m_pVariable)
			{
				m_pName->SetText(m_pVariable->GetName());

				m_pType->SigItemPicked.Disconnect(this, &VariableViewer::OnSetType);
				m_pType->SelectByItem(std::make_pair(m_pVariable->GetType(), m_pVariable->GetObjectType()));
				m_pType->SigItemPicked.Connect(this, &VariableViewer::OnSetType);

				m_pConst->OnCheck(m_pVariable->IsConst());

				CreateValueBox();
			}
		}

		void VariableViewer::Dock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void VariableViewer::CreateValueBox(void)
		{
			if(m_pInput)
				RemoveController(*m_pInput);
			else if(m_pArea)
				RemoveController(*m_pArea);

			if(!m_pVariable->IsArray())
			{
				if(m_pVariable->GetType() != core::AttributeType::OBJECT)
					m_pInput = &AddController<EventInputBox>(*m_pMainWidget, m_pVariable->GetAttribute(), 0, 78, m_pMainWidget->GetWidth(), 21);
				else
				{
					auto& label = AddWidget<gui::Label>(0.0f, 78.0f, (float)m_pMainWidget->GetWidth(), 21.0f, L"Object variable content cannot be shown.");
					label.SetPositionModes(gui::PositionMode::REL, gui::PositionMode::ABS, gui::PositionMode::ABS, gui::PositionMode::ABS);
					label.SetAlign(4);
				}
				m_pArea = nullptr;
			}
			else
			{
				m_pArea = &AddController<ArrayEditArea>(*GetWidgetByName(L"Area"), m_pVariable->GetAttribute());
				m_pArea->GetMainWidget()->SetPadding(0, 99, 0, 99);
				m_pInput = nullptr;
			}
		}

		void VariableViewer::OnSetConst(bool isConst)
		{
			ACL_ASSERT(m_pVariable);
			m_pVariable->SetConst(isConst);
		}

		void VariableViewer::OnSetName(std::wstring stName)
		{
			ACL_ASSERT(m_pVariable);

			if(m_pVariable->GetName() != stName)
			{
				m_pVariable->SetName(stName);
				SigVariableDirty();
			}
		}

		void VariableViewer::OnSetType(std::pair<core::AttributeType, unsigned int> type)
		{
			ACL_ASSERT(m_pVariable);

			if(m_pVariable->GetType() != type.first || m_pVariable->GetObjectType() != type.second)
			{
				if(type.first != core::AttributeType::OBJECT)
					m_pVariable->SetType(type.first);
				else
					m_pVariable->SetType(type.second);
				// in case of type change, all connected nodes have to be disconnected
				// TODO: reimplement
				CreateValueBox();
				SigVariableDirty();
			}
		}

	}
}

