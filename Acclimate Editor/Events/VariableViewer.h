#pragma once
#include "TypeBox.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"
#include "Gui\Textbox.h"
#include "Gui\ComboBox.h"

namespace acl
{
	namespace core
	{
		class EventVariable;
	}

	namespace gui
	{
		class Dock;
		class DockArea;
		class CheckBox;

		enum class DockType;
	}

	namespace editor
	{

		class EventInputBox;
		class ArrayEditArea;

		class VariableViewer :
			public gui::BaseController
		{
			typedef gui::Textbox<> NameBox;
		public:
			VariableViewer(gui::Module& module);
			~VariableViewer(void);

			void SetVariable(core::EventVariable* pVariable);
			void OnRefresh(void);

			void Dock(gui::DockArea& area, gui::DockType type, bool stack);

			core::Signal<> SigVariableDirty;

		private:
			
			void CreateValueBox(void);

			void OnSetConst(bool isConst);
			void OnSetName(std::wstring stName);
			void OnSetType(std::pair<core::AttributeType, unsigned int>);

			gui::Dock* m_pDock;
			NameBox* m_pName;
			TypeBox* m_pType;
			gui::CheckBox* m_pConst;
			EventInputBox* m_pInput;
			ArrayEditArea* m_pArea;

			core::EventVariable* m_pVariable;
		};

	}
}


