#include "FolderTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		FolderTreeCallback::FolderTreeCallback(bool bLocked) : m_bLocked(bLocked)
		{
		}

		const std::wstring* FolderTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"LockIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"FolderIcon";
				return &stIcon;
			}	
		}

		bool FolderTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool FolderTreeCallback::IsDeletable(void) const
		{
			return false;
		}

		bool FolderTreeCallback::IsDefaultExpanded(void) const
		{
			return true;
		}

		void FolderTreeCallback::OnSelect(void)
		{
		}

		void FolderTreeCallback::OnDelete(void)
		{
		}

		gui::ContextMenu* FolderTreeCallback::OnContextMenu(void)
		{
			return nullptr;
		}

		void FolderTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void FolderTreeCallback::OnRename(const std::wstring& stName)
		{
		}

	}
}