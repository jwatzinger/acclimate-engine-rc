#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

	namespace editor
	{

		class FolderTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			FolderTreeCallback(bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

		private:

			bool m_bLocked;
		};

	}
}


