#pragma once
#include <string>
#include <unordered_map>

namespace acl
{
	namespace ecs
	{
		class Entity;
	}

	namespace gui
	{
		class Widget;
	}

	namespace editor
	{

		class IComponentAttribute
		{
		public:

			typedef std::unordered_map<std::wstring, gui::Widget*> TableMap;

			virtual ~IComponentAttribute(void) = 0 {};

			virtual bool OnHasComponent(const ecs::Entity& entity) const = 0;
			virtual void OnSelectEntity(const ecs::Entity& entity) = 0;
			virtual void OnFillTable(TableMap& table) = 0;
			virtual void OnUpdate(void) = 0;
		};
	}
}