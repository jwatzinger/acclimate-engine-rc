#pragma once

namespace acl
{
	namespace ecs
	{
		class Entity;
		struct BaseComponent;
	}

	namespace gui
	{
		class BaseController;
	}

	namespace editor
	{

		class IComponentController
		{
		public:

			virtual IComponentController& Clone(void) const = 0;

			virtual void Attach(ecs::Entity& entity) const = 0;

			virtual gui::BaseController& GetController(void) = 0;
			virtual size_t GetComponentId(void) const = 0;
		};

	}
}