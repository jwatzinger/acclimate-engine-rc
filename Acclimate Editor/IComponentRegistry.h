#pragma once
#include <string>

namespace acl
{
	namespace editor
	{
		
		class IComponentController;
		class IComponentAttribute;

		class IComponentRegistry
		{
		public:

			virtual ~IComponentRegistry(void) {};

			virtual void AddComponentAttachController(const std::wstring& stName, IComponentController& controller) = 0;
			virtual void AddComponentAtttributeController(const std::wstring& stName, IComponentAttribute& controller) = 0;

			virtual void RemoveComponentAttachController(const std::wstring& stName) = 0;
			virtual void RemoveComponentAtttributeController(const std::wstring& stName) = 0;

			virtual void OnEntityChanged(void) = 0;
		};
	}
}