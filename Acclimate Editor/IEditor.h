#pragma once
#include <string>
#include "Entity\Entity.h"

namespace acl
{
	namespace ecs
	{
		class MessageManager;
	}

	namespace gfx
	{
		class IModel;
		class ITexture;
	}

	namespace gui
	{
		class Module;
	}

	namespace editor
	{

		class IGameView;
		class IComponentRegistry;
		class ISceneExtention;

		class IEditor
		{
		public:

			virtual ~IEditor() = 0 {};

			virtual void SetGameView(IGameView* pGameView) = 0;
			virtual void AddSceneExtention(ISceneExtention& extention) = 0;

			virtual gui::Module& GetModule(void) const = 0;
			virtual IComponentRegistry& GetComponentRegistry(void) const = 0;
			virtual const ecs::MessageManager& GetMessageManager(void) const = 0;

			virtual bool PickTexture(const gfx::ITexture** pTexture, std::wstring* pName) const = 0;
			virtual bool PickTexture(const std::wstring& stDefault, const gfx::ITexture** pTexture, std::wstring* pName) const = 0;
			virtual bool PickModel(const gfx::IModel** pModel, std::wstring* pName) const = 0;
			virtual bool PickModel(const std::wstring& stDefault, const gfx::IModel** pModel, std::wstring* pName) const = 0;
			virtual bool PickEntity(ecs::EntityHandle* ppEntity, std::wstring* pName) const = 0;
			virtual bool PickEntity(const std::wstring& stDefault, ecs::EntityHandle* ppEntity, std::wstring* pName) const = 0;
			virtual bool PickScriptClass(const std::string& stInterface, std::string* pName) const = 0;
			virtual bool PickScriptClass(const std::string& stDefault, const std::string& stInterface, std::string* pName) const = 0;

			virtual void OnEntityChanged(void) = 0;
			virtual void DeleteEntity(const ecs::EntityHandle& entity) = 0;
			virtual void CopyEntity(const ecs::Entity& entity) = 0;
			virtual void PasteEntity(void) = 0;

			virtual void Render(void) const = 0;
		};

	}
}


