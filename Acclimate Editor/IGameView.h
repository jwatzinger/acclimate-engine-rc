#pragma once
#include "Core\Signal.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gfx
	{
		struct Context;
	}

	namespace gui
	{
		class Dock;
	}

	namespace math
	{
		struct Vector2;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class IGameView
		{
		public:

			virtual ~IGameView(void) = 0 {};

			virtual gui::Dock& GetDock(void) = 0;

			virtual void OnInit(const gfx::Context& gfx, const render::IRenderer& renderer) = 0;
			virtual void OnSelectEntity(const ecs::EntityHandle* pEntity) = 0;
			virtual void OnGotoEntity(const ecs::Entity& entity) = 0;
			virtual ecs::EntityHandle OnPickEntity(const ecs::EntityManager& entities, const math::Vector2& vMouse) = 0;
			virtual core::Signal<>& GetClickedSignal(void) = 0;

			virtual void OnRender(void) const = 0;
		};

	}
}