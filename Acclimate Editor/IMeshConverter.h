#pragma once
#include <string>
#include <vector>
#include "Gfx\MeshFile.h"

namespace acl
{
	namespace gfx
	{
		class AnimationSet;
	}

	namespace editor
	{

		class IMeshConverter
		{
		public:

			typedef std::vector<std::wstring> TextureVector;

			virtual ~IMeshConverter(void) {};

			virtual gfx::MeshFile Convert(const std::wstring& stIn, TextureVector* pMaterials, gfx::AnimationSet* pSet) const = 0;
		};

	}
}