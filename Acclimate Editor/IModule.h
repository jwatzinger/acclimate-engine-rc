#pragma once
#include <string>
#include "Project.h"

namespace acl
{
	namespace editor
	{

		class IModule
		{
		public:

			virtual ~IModule(void) = 0 {}

			virtual Project::ModuleType GetModuleType(void) const = 0;
			
			virtual void Update(void) = 0;

			virtual void OnNew(const std::wstring& stFile) = 0;
			virtual void OnLoad(const std::wstring& stFile) = 0;
			virtual void OnSave(const std::wstring& stFile) = 0;
			virtual void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) = 0;
			virtual void OnSceneChanged(void) = 0;
			virtual void OnSaveScene(const std::wstring& stFile) = 0;
			virtual void OnClose(void) = 0;
			virtual void OnBeginTest(void) = 0;
			virtual void OnEndTest(void) = 0;
		};

	}
}