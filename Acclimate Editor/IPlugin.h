#pragma once

namespace acl
{
	namespace core
	{
		class IModule;
	}

	namespace gfx
	{
		struct LoadContext;
	}

	namespace gui
	{
		class Widget;
	}

	namespace editor
	{

		class IEditor;

		class IPlugin
		{
		public:
			virtual ~IPlugin(void) = 0 {}

			virtual void OnInit(IEditor& editor) = 0;
			virtual void OnLoadResources(const gfx::LoadContext& context) = 0;
			virtual void OnUpdate(void) = 0;
			virtual void OnRender(void) const = 0;
			virtual void OnUninit(void) = 0;
			virtual void OnBeginTest(void) = 0;
			virtual void OnEndTest(void) = 0;
		};

	}
}