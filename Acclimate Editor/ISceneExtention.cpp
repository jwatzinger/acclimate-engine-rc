#include "ISceneExtention.h"

namespace acl
{
	namespace editor
	{

		MenuSection::MenuSection(gui::MenuItem& item) : m_pItem(&item)
		{
			m_pItem->InsertSeperator();
		}

		MenuItem::MenuItem(gui::MenuItem& item) : m_pItem(&item)
		{
		}

	}
}