#pragma once
#include "Core\Dll.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"

namespace acl
{
	namespace editor
	{

		class MenuSection
		{
		public:
			MenuSection(gui::MenuItem& item);

			template<typename X, typename Y>
			void AddOption(const std::wstring& stName, Y * obj, void (X::*func)())
			{
				auto& option = m_pItem->AddOption(stName);
				option.SigReleased.Connect(obj, func);
			}

		private:

			gui::MenuItem* m_pItem;
		};

		class MenuItem
		{
		public:
			MenuItem(gui::MenuItem& item);

			MenuSection AddSection(void)
			{
				return MenuSection(*m_pItem);
			}

		private:

			gui::MenuItem* m_pItem;
		};

		class ISceneExtention
		{
		public:

			virtual ~ISceneExtention(void) = 0 {}

			virtual void OnInit(gui::Module& module) = 0;

			virtual const std::wstring& OnGetName(void) const = 0;

			virtual void OnAddMenuItems(MenuSection& section) = 0 {}
		};

	}
}