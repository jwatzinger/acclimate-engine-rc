#include "InputModule.h"
#include "Gui\Menubar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Input\Context.h"
#include "Input\ILoader.h"

namespace acl
{
	namespace editor
	{

		InputModule::InputModule(gui::BaseController& controller, gui::MenuBar& bar, const input::Context& input) :
			m_pInput(&input)
		{
			auto* pLocalizationItem = bar.GetItem(L"TOOLS");
			pLocalizationItem->InsertSeperator();

			auto& option = pLocalizationItem->AddOption(L"Input editor");
			option.SetShortcut(gui::ComboKeys::CTRL_ALT, 'I', gui::ShortcutState::ALWAYS);
		}

		InputModule::~InputModule(void)
		{
		}

		Project::ModuleType InputModule::GetModuleType(void) const
		{
			return Project::ModuleType::INPUT;
		}

		void InputModule::Update(void)
		{
		}

		void InputModule::OnNew(const std::wstring& stFile)
		{
		}

		void InputModule::OnLoad(const std::wstring& stFile)
		{
			m_pInput->loader.Load(stFile);
		}

		void InputModule::OnSave(const std::wstring& stFile)
		{
		}

		void InputModule::OnSceneChanged(void)
		{
		}

		void InputModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void InputModule::OnClose(void)
		{
		}

		void InputModule::OnBeginTest(void)
		{
		}

		void InputModule::OnEndTest(void)
		{
		}

	}
}
