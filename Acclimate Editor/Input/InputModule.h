#pragma once
#include "..\IModule.h"

namespace acl
{
	namespace gui
	{
		class BaseController;
		class MenuBar;
	}

	namespace input
	{
		struct Context;
	}

	namespace editor
	{

		class InputModule :
			public IModule
		{
		public:
			InputModule(gui::BaseController& controller, gui::MenuBar& bar, const input::Context& input);
			~InputModule(void);

			Project::ModuleType GetModuleType(void) const;

			void Update(void) override;
			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			const input::Context* m_pInput;
		};

	}
}

