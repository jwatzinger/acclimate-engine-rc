#include "AddCategoryController.h"
#include "Core\Localization.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddCategoryController::AddCategoryController(gui::Module& module, const core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/AddCategory.axm"),
			m_create(false), m_pLocalization(&localization)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"New category");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddCategoryController::OnCreate);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddCategoryController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddCategoryController::OnCreate);
		}

		AddCategoryController::~AddCategoryController()
		{
		}

		bool AddCategoryController::Execute(std::wstring* pName)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				const auto& stCategory = m_pBox->GetContent();

				if(!m_pLocalization->HasCategory(stCategory))
				{
					if(pName)
						*pName = stCategory;
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}

		void AddCategoryController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddCategoryController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
