#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace gui
	{
		class Window;
	}

	namespace editor
	{
		
		class AddCategoryController :
			public gui::BaseController
		{
		public:
			AddCategoryController(gui::Module& module, const core::Localization& localization);
			~AddCategoryController(void);

			bool Execute(std::wstring* pName);

		private:

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;

			gui::Textbox<>* m_pBox;
			gui::Window* m_pWindow;

			const core::Localization* m_pLocalization;
		};

	}
}

