#include "AddLanguageController.h"
#include "Core\Localization.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddLanguageController::AddLanguageController(gui::Module& module, const core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/AddLanguage.axm"),
			m_create(false), m_pLocalization(&localization)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"New language");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddLanguageController::OnCreate);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddLanguageController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddLanguageController::OnCreate);
		}

		AddLanguageController::~AddLanguageController()
		{
		}

		bool AddLanguageController::Execute(std::wstring* pName)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				const auto& stLanguage = m_pBox->GetContent();

				if(!m_pLocalization->HasLanguage(stLanguage))
				{
					if(pName)
						*pName = stLanguage;
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}

		void AddLanguageController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddLanguageController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
