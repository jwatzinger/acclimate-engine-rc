#include "AddLocalController.h"
#include "Core\Localization.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddLocalController::AddLocalController(gui::Module& module, const core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/AddLocal.axm"),
			m_create(false)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"New local");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddLocalController::OnCreate);

			// type
			m_pCategory = &AddWidget<CategoryBox>(0.0f, 58.0f, 1.0f, 21.0f, 10.0f);
			m_pCategory->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pCategory->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pCategory->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pCategory->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);

			for(auto& category : localization.GetCategories())
			{
				m_pCategory->AddItem(category.first, category.first);
			}
			m_pCategory->Select(0);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddLocalController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddLocalController::OnCreate);
		}

		AddLocalController::~AddLocalController()
		{
		}

		bool AddLocalController::Execute(std::wstring* pName, std::wstring* pCategory)
		{
			m_create = false;
			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();
				if(pCategory)
					*pCategory = m_pCategory->GetContent();
				return true;
			}
			else
				return false;
		}

		bool AddLocalController::Execute(std::wstring* pName, const std::wstring& stCategory)
		{
			m_create = false;

			m_pCategory->SelectByName(stCategory);
			m_pCategory->OnDisable();

			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();

				return true;
			}
			else
				return false;
		}

		void AddLocalController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddLocalController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
