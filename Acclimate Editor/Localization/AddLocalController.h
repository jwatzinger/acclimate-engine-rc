#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"
#include "Gui\ComboBox.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace gui
	{
		class Window;
	}

	namespace editor
	{
		
		class AddLocalController :
			public gui::BaseController
		{
			typedef gui::ComboBox<std::wstring> CategoryBox;
		public:
			AddLocalController(gui::Module& module, const core::Localization& localization);
			~AddLocalController(void);

			bool Execute(std::wstring* pName, std::wstring* pCategory);
			bool Execute(std::wstring* pName, const std::wstring& stCategory);

		private:

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;

			gui::Textbox<>* m_pBox;
			gui::Window* m_pWindow;
			CategoryBox* m_pCategory;
		};

	}
}

