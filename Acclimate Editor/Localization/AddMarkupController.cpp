#include "AddMarkupController.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddMarkupController::AddMarkupController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Localization/AddMarkup.axm"),
			m_create(false)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddMarkupController::OnCreate);

			// value
			m_pValue = &AddWidget<gui::Textbox<>>(0.0f, 66.0f, 1.0f, 21.0f, L"");
			m_pValue->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pValue->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pValue->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pValue->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pValue->SigConfirm.Connect(this, &AddMarkupController::OnCreate);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddMarkupController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddMarkupController::OnCreate);
		}

		AddMarkupController::~AddMarkupController()
		{
		}

		bool AddMarkupController::Execute(std::wstring* pName, std::wstring* pValue)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();
				if(pValue)
					*pValue = m_pValue->GetContent();

				return true;
			}
			else
				return false;
		}

		void AddMarkupController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddMarkupController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
