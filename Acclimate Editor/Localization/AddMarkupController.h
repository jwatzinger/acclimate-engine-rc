#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gui
	{
		class Window;
	}

	namespace editor
	{
		
		class AddMarkupController :
			public gui::BaseController
		{
		public:
			AddMarkupController(gui::Module& module);
			~AddMarkupController(void);

			bool Execute(std::wstring* pName, std::wstring* pValue);

		private:

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;

			gui::Textbox<>* m_pBox, *m_pValue;
			gui::Window* m_pWindow;
		};

	}
}

