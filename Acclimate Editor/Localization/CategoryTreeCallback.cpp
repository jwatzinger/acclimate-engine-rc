#include "CategoryTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		CategoryTreeCallback::CategoryTreeCallback(const std::wstring& stCategory) : m_stCategory(stCategory)
		{
		}

		const std::wstring* CategoryTreeCallback::GetIconName(void) const
		{
			static const std::wstring stIcon = L"FolderIcon";
			return &stIcon;
		}

		bool CategoryTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool CategoryTreeCallback::IsDeletable(void) const
		{
			return true;
		}

		bool CategoryTreeCallback::IsDefaultExpanded(void) const
		{
			return true;
		}

		void CategoryTreeCallback::OnSelect(void)
		{
		}

		void CategoryTreeCallback::OnDelete(void)
		{
			SigDelete(m_stCategory);
		}

		gui::ContextMenu* CategoryTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pMenu = new gui::ContextMenu(0.02f);

			// add markup
			auto& add = pMenu->AddItem(L"Add local to category");
			add.SigReleased.Connect(this, &CategoryTreeCallback::OnAddLocal);
			
			pMenu->InsertSeperator();

			return pMenu;
		}

		void CategoryTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void CategoryTreeCallback::OnRename(const std::wstring& stName)
		{
			//SigRename(m_entity, stName);
		}

		void CategoryTreeCallback::OnAddLocal(void)
		{
			SigAddLocal(m_stCategory);
		}

	}
}