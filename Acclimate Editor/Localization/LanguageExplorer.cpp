#include "LanguageExplorer.h"
#include "AddLanguageController.h"
#include "Core\Localization.h"
#include "Gui\Dock.h"
#include "Gui\DockArea.h"
#include "Gui\TabBarItem.h"
#include "Gui\MultiTable.h"
#include "Gui\TableItem.h"
#include "Gui\Textbox.h"
#include "Gui\ContextMenu.h"
#include "Gui\ContextMenuItem.h"

namespace acl
{
	namespace editor
	{

		LanguageExplorer::LanguageExplorer(gui::Module& module, core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/LanguageExplorer.axm"),
			m_pLocalization(&localization), m_isDirty(false)
		{
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			// tab bar
			m_pBar = GetWidgetByName<gui::TabBar>(L"Bar");

			auto& contextMenu = AddWidget<gui::ContextMenu>(0.02f);
			auto& addLanguage = contextMenu.AddItem(L"Add language");
			addLanguage.SigReleased.Connect(this, &LanguageExplorer::OnAddLanguage);

			m_pBar->SetContextMenu(&contextMenu);

			// view table
			m_pTable = GetWidgetByName<gui::MultiTable>(L"Table");
		}

		LanguageExplorer::~LanguageExplorer(void)
		{
		}

		void LanguageExplorer::Update(void)
		{
			if(m_isDirty)
			{
				OnRefresh();

				m_isDirty = false;
			}
		}

		void LanguageExplorer::OnRefresh(void)
		{
			m_pBar->Clear();

			gui::TabBarItem* pFirstItem = nullptr;
			for(auto& stLanguage : m_pLocalization->GetLanguages())
			{
				auto& item = m_pBar->AddItem(stLanguage);
				item.SigSelection.Connect(this, &LanguageExplorer::OnSelectLanguage);

				if(!pFirstItem)
					pFirstItem = &item;
			}

			if(!m_stSelected.empty())
				m_pBar->SelectByName(m_stSelected);
			else if(pFirstItem)
				m_pBar->SelectByName(m_pLocalization->GetDefaultLanguage());
		}

		void LanguageExplorer::OnDock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void LanguageExplorer::OnSelectLanguage(gui::TabBarItem* pItem)
		{
			const auto& stLanguage = pItem->GetName();

			m_pTable->Clear();

			for(auto& categories : m_pLocalization->GetCategories())
			{
				auto& table = m_pTable->AddTable(categories.first);
				table.SetLeftColumnSize(0.2f);

				for(auto& locals : categories.second)
				{
					auto& item = table.AddRow(locals.first);

					auto& textbox = AddWidget<Box>(0.0f, 0.0f, 1.0f, 1.0f, m_pLocalization->GetLocalString(stLanguage, locals.first));
					item.AddChild(textbox);

					DataContainer data(textbox, textbox.SigContentSet, locals.first);
					data.SigAccess.Connect(this, &LanguageExplorer::OnChangeLocal);
					m_vContainer.push_back(std::move(data));
				}
			}
			m_stSelected = stLanguage;
		}

		void LanguageExplorer::OnAddLanguage(void)
		{
			AddLanguageController add(*m_pModule, *m_pLocalization);

			std::wstring stName;
			if(add.Execute(&stName))
			{
				m_pLocalization->AddLanguage(stName);

				m_stSelected = stName;

				m_isDirty = true;
			}
		}

		void LanguageExplorer::OnChangeLocal(std::wstring stIdentifier, std::wstring stContent)
		{
			m_pLocalization->SetString(m_stSelected, stIdentifier, stContent);
		}

	}
}

