#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"
#include "Gui\DataContainer.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace gui
	{
		class DockArea;
		class Dock;
		class TabBar;
		class TabBarItem;
		class MultiTable;

		enum class DockType;
	}

	namespace editor
	{

		class LanguageExplorer :
			public gui::BaseController
		{
			typedef gui::Textbox<> Box;
			typedef gui::DataContainer<std::wstring, Box, std::wstring> DataContainer;
			typedef std::vector<DataContainer> ContainerVector;
		public:
			LanguageExplorer(gui::Module& module, core::Localization& localization);
			~LanguageExplorer(void);
			
			void Update(void) override;

			void OnRefresh(void);

			void OnDock(gui::DockArea& area, gui::DockType type, bool stack);

		private:

			void OnSelectLanguage(gui::TabBarItem* pItem);
			void OnAddLanguage(void);
			void OnChangeLocal(std::wstring stIdentifier, std::wstring stContent);

			bool m_isDirty;
			std::wstring m_stSelected;

			gui::Dock* m_pDock;
			gui::TabBar* m_pBar;
			gui::MultiTable* m_pTable;

			core::Localization* m_pLocalization;

			ContainerVector m_vContainer;
		};

	}
}

