#include "LocalizationController.h"
#include "LocalizationExplorer.h"
#include "LanguageExplorer.h"
#include "MarkupController.h"
#include "Gui\DockArea.h"

namespace acl
{
	namespace editor
	{

		LocalizationController::LocalizationController(gui::Module& module, core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/Localization.axm")
		{
			auto pArea = GetWidgetByName<gui::DockArea>(L"Area");

			m_pLanguages = &AddController<LanguageExplorer>(localization);
			m_pLanguages->OnDock(*pArea, gui::DockType::MIDDLE, false);

			// markups
			auto& markup = AddController<MarkupController>();
			markup.OnDock(*pArea, gui::DockType::LEFT, false);

			// locals
			m_pExplorer = &AddController<LocalizationExplorer>(localization);
			m_pExplorer->OnDock(*pArea, gui::DockType::LEFT, false);
			m_pExplorer->SigLocalAdded.Connect(this, &LocalizationController::OnLocalAdded);
			m_pExplorer->SigLocalDeleted.Connect(this, &LocalizationController::OnLocalAdded);
			m_pExplorer->SigSelectLocal.Connect(&markup, &MarkupController::SetLocal);
		}

		void LocalizationController::OnRefresh(void)
		{
			m_pExplorer->OnRefresh();
			m_pLanguages->OnRefresh();
		}

		void LocalizationController::OnLocalAdded(void)
		{
			m_pLanguages->OnRefresh();
		}

	}
}

