#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace gui
	{
		class DockArea;
	}

	namespace editor
	{

		class LocalizationExplorer;
		class LanguageExplorer;

		class LocalizationController :
			public gui::BaseController
		{
		public:
			LocalizationController(gui::Module& module, core::Localization& localization);

			void OnRefresh(void);

		private:

			void OnLocalAdded(void);

			gui::DockArea* m_pDock;
			LocalizationExplorer* m_pExplorer;
			LanguageExplorer* m_pLanguages;
		};

	}
}

