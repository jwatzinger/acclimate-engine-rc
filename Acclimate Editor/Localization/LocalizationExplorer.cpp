#include "LocalizationExplorer.h"
#include "LocalizationTreeModel.h"
#include "AddLocalController.h"
#include "AddCategoryController.h"
#include "AddMarkupController.h"
#include "Core\Localization.h"
#include "Gui\Dock.h"
#include "Gui\DockArea.h"
#include "Gui\TreeView.h"
#include "Gui\ContextMenu.h"
#include "Gui\ContextMenuItem.h"

namespace acl
{
	namespace editor
	{

		LocalizationExplorer::LocalizationExplorer(gui::Module& module, core::Localization& localization) : BaseController(module, L"../Editor/Menu/Localization/LocalizationExplorer.axm"),
			m_pLocalization(&localization)
		{
			m_pModel = new LocalizationTreeModel(localization);
			m_pModel->SigDeleteLocal.Connect(this, &LocalizationExplorer::OnDeleteLocal);
			m_pModel->SigDeleteMarkup.Connect(this, &LocalizationExplorer::OnDeleteMarkup);
			m_pModel->SigAddMarkup.Connect(this, &LocalizationExplorer::OnAddMarkup);
			m_pModel->SigSelectLocal.Connect(this, &LocalizationExplorer::OnSelectLocal);
			m_pModel->SigAddLocalToCategory.Connect(this, &LocalizationExplorer::OnAddLocal);
			m_pModel->SigDeleteCategory.Connect(this, &LocalizationExplorer::OnDeleteCategory);

			// tree
			auto pTree = GetWidgetByName<gui::TreeView>(L"Locals");
			pTree->SetModel(m_pModel);

			// dock
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			// context
			auto& context = AddWidget<gui::ContextMenu>(0.02f);

			auto& addLocal = context.AddItem(L"Add local");
			addLocal.SigReleased.Connect(this, &LocalizationExplorer::OnAddLocal);

			auto& addCategory = context.AddItem(L"Add category");
			addCategory.SigReleased.Connect(this, &LocalizationExplorer::OnAddCategory);

			pTree->SetContextMenu(&context);
		}

		LocalizationExplorer::~LocalizationExplorer(void)
		{
			delete m_pModel;
		}

		void LocalizationExplorer::Update(void)
		{
			m_pModel->Update();
		}

		void LocalizationExplorer::OnRefresh(void)
		{
			m_pModel->OnRefresh();
		}

		void LocalizationExplorer::OnDock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void LocalizationExplorer::OnAddLocal(void)
		{
			AddLocalController add(*m_pModule, *m_pLocalization);

			std::wstring stName, stCategory;
			if(add.Execute(&stName, &stCategory))
			{
				m_pLocalization->AddIdentifier(stName, stCategory);
				m_pModel->OnRefresh();

				SigLocalAdded();
			}
		}

		void LocalizationExplorer::OnAddLocal(const std::wstring& stCategory)
		{
			AddLocalController add(*m_pModule, *m_pLocalization);

			std::wstring stName;
			if(add.Execute(&stName, stCategory))
			{
				m_pLocalization->AddIdentifier(stName, stCategory);
				m_pModel->OnRefresh();

				SigLocalAdded();
			}
		}

		void LocalizationExplorer::OnAddCategory(void)
		{
			AddCategoryController add(*m_pModule, *m_pLocalization);

			std::wstring stName;
			if(add.Execute(&stName))
			{
				m_pLocalization->AddCategory(stName);
				m_pModel->OnRefresh();
			}
		}

		void LocalizationExplorer::OnAddMarkup(const std::wstring& stIdentifier)
		{
			AddMarkupController add(*m_pModule);

			std::wstring stMarkup, stValue;
			if(add.Execute(&stMarkup, &stValue))
			{
				m_pLocalization->AddMarkup(stIdentifier, stMarkup, stValue);
				m_pModel->OnRefresh();
			}
		}

		void LocalizationExplorer::OnDeleteLocal(const std::wstring& stIdentifier)
		{
			m_pLocalization->RemoveIdentifier(stIdentifier);
			OnRefresh();
			SigLocalDeleted();
		}

		void LocalizationExplorer::OnDeleteMarkup(const std::wstring& stMarkup, const std::wstring& stIdentifier)
		{
			m_pLocalization->RemoveMarkup(stIdentifier, stMarkup);
			OnRefresh();
			SigLocalDeleted();
		}

		void LocalizationExplorer::OnDeleteCategory(const std::wstring& stCategory)
		{
			m_pLocalization->RemoveCategory(stCategory);
			OnRefresh();
			SigLocalDeleted();
		}

		void LocalizationExplorer::OnSelectLocal(const std::wstring& stLocal)
		{
			SigSelectLocal(m_pLocalization->GetString(stLocal));
		}

	}
}

