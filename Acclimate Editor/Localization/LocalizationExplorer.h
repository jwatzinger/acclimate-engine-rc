#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		class Localization;
		class LocalizedString;
	}

	namespace gui
	{
		class DockArea;
		class Dock;
		class TreeView;

		enum class DockType;
	}

	namespace editor
	{

		class LocalizationTreeModel;

		class LocalizationExplorer :
			public gui::BaseController
		{
		public:
			LocalizationExplorer(gui::Module& module, core::Localization& localization);
			~LocalizationExplorer();

			void Update(void) override;

			void OnRefresh(void);

			void OnDock(gui::DockArea& area, gui::DockType type, bool stack);

			core::Signal<> SigLocalAdded;
			core::Signal<> SigLocalDeleted;
			core::Signal<core::LocalizedString&> SigSelectLocal;

		private:

			void OnAddLocal(void);
			void OnAddLocal(const std::wstring& stCategory);
			void OnAddCategory(void);
			void OnAddMarkup(const std::wstring& stIdentifier);
			void OnDeleteLocal(const std::wstring& stIdentifier);
			void OnDeleteMarkup(const std::wstring& stMarkup, const std::wstring& stIdentifier);
			void OnDeleteCategory(const std::wstring& stCategory);
			void OnSelectLocal(const std::wstring& stLocal);

			LocalizationTreeModel* m_pModel;
			gui::Dock* m_pDock;

			core::Localization* m_pLocalization;
		};

	}
}

