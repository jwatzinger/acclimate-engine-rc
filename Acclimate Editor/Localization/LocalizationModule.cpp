#include "LocalizationModule.h"
#include "LocalizationController.h"
#include "Gui\Menubar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"

#include "Core\LocalizationLoader.h"
#include "Core\LocalizationSaver.h"

namespace acl
{
	namespace editor
	{

		LocalizationModule::LocalizationModule(gui::BaseController& controller, gui::MenuBar& bar, core::Localization& localization):
			m_pLocalization(&localization)
		{
			auto* pLocalizationItem = bar.GetItem(L"TOOLS");
			pLocalizationItem->InsertSeperator();

			auto& option = pLocalizationItem->AddOption(L"Localization viewer");
			option.SetShortcut(gui::ComboKeys::CTRL_ALT, 'l', gui::ShortcutState::ALWAYS);

			m_pController = new LocalizationController(controller.GetModule(), localization);
			option.SigReleased.Connect(m_pController, &LocalizationController::OnToggle);
			option.SigShortcut.Connect(m_pController, &LocalizationController::OnToggle);
		}

		LocalizationModule::~LocalizationModule(void)
		{
			delete m_pController;
		}

		Project::ModuleType LocalizationModule::GetModuleType(void) const
		{
			return Project::ModuleType::LOCALIZATION;
		}

		void LocalizationModule::Update(void)
		{
			m_pController->Update();
		}

		void LocalizationModule::OnNew(const std::wstring& stFile)
		{
		}

		void LocalizationModule::OnLoad(const std::wstring& stFile)
		{
			core::LocalizationLoader loader(*m_pLocalization);
			loader.Load(stFile);

			m_pController->OnRefresh();
		}

		void LocalizationModule::OnSave(const std::wstring& stFile)
		{
			core::LocalizationSaver saver(*m_pLocalization);
			saver.Save(stFile);
		}

		void LocalizationModule::OnSceneChanged(void)
		{
		}

		void LocalizationModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void LocalizationModule::OnClose(void)
		{
		}

		void LocalizationModule::OnBeginTest(void)
		{
		}

		void LocalizationModule::OnEndTest(void)
		{
		}

	}
}
