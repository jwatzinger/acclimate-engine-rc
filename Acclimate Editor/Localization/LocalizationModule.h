#pragma once
#include "..\IModule.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace gui
	{
		class BaseController;
		class MenuBar;
	}

	namespace editor
	{

		class LocalizationController;

		class LocalizationModule :
			public IModule
		{
		public:
			LocalizationModule(gui::BaseController& controller, gui::MenuBar& bar, core::Localization& localization);
			~LocalizationModule();

			Project::ModuleType GetModuleType(void) const;

			void Update(void) override;
			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			core::Localization* m_pLocalization;

			LocalizationController* m_pController;
		};

	}
}

