#include "LocalizationTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		LocalizationTreeCallback::LocalizationTreeCallback(const std::wstring& stIdentifier) : m_stIdentifier(stIdentifier)
		{
		}

		const std::wstring* LocalizationTreeCallback::GetIconName(void) const
		{
			static const std::wstring stIcon = L"StringIcon";
			return &stIcon;
		}

		bool LocalizationTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool LocalizationTreeCallback::IsDeletable(void) const
		{
			return true;
		}

		bool LocalizationTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void LocalizationTreeCallback::OnSelect(void)
		{
			SigSelect(m_stIdentifier);
		}

		void LocalizationTreeCallback::OnDelete(void)
		{
			SigDelete(m_stIdentifier);
		}

		gui::ContextMenu* LocalizationTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pMenu = new gui::ContextMenu(0.02f);

			// add markup
			auto& add = pMenu->AddItem(L"Add markup");
			add.SigReleased.Connect(this, &LocalizationTreeCallback::OnAddMarkup);
			
			pMenu->InsertSeperator();

			return pMenu;
		}

		void LocalizationTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void LocalizationTreeCallback::OnRename(const std::wstring& stName)
		{
			//SigRename(m_entity, stName);
		}

		void LocalizationTreeCallback::OnAddMarkup(void)
		{
			SigAddMarkup(m_stIdentifier);
		}

	}
}