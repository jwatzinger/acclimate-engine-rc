#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace editor
	{

		class LocalizationTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			LocalizationTreeCallback(const std::wstring& stIdentifier);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const std::wstring&> SigSelect;
			core::Signal<const std::wstring&> SigDelete;
			core::Signal<const std::wstring&> SigAddMarkup;

		private:

			void OnAddMarkup(void);

			std::wstring m_stIdentifier;

		};

	}
}


