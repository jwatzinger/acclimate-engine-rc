#include "LocalizationTreeModel.h"
#include "LocalizationTreeCallback.h"
#include "MarkupTreeCallback.h"
#include "CategoryTreeCallback.h"
#include "Core\Localization.h"
#include "Core\LocalizedString.h"

namespace acl
{
	namespace editor
	{

		LocalizationTreeModel::LocalizationTreeModel(core::Localization& localization) : m_root(L"Localization"),
			m_isDirty(true), m_pLocalization(&localization)
		{
		}

		const gui::Node& LocalizationTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& LocalizationTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void LocalizationTreeModel::Update(void)
		{
			if(m_isDirty)
			{
				m_root.vChilds.clear();

				for(auto& category : m_pLocalization->GetCategories())
				{
					gui::Node categoryNode(category.first);
					auto pCategoryCallback = new CategoryTreeCallback(category.first);
					pCategoryCallback->SigAddLocal.Connect(&SigAddLocalToCategory, &core::Signal<const std::wstring&>::operator());
					pCategoryCallback->SigDelete.Connect(&SigDeleteCategory, &core::Signal<const std::wstring&>::operator());
					categoryNode.pCallback = pCategoryCallback;

					for(auto& identifier : category.second)
					{
						const auto& stIdentifier = identifier.first;
						gui::Node localNode(stIdentifier);
						auto pCallback = new LocalizationTreeCallback(stIdentifier);
						pCallback->SigDelete.Connect(&SigDeleteLocal, &core::Signal<const std::wstring&>::operator());
						pCallback->SigAddMarkup.Connect(&SigAddMarkup, &core::Signal<const std::wstring&>::operator());
						pCallback->SigSelect.Connect(&SigSelectLocal, &core::Signal<const std::wstring&>::operator());
						localNode.pCallback = pCallback;

						for(auto& markup : identifier.second->GetMarkups())
						{
							gui::Node markupNode(markup.first);
							auto pMarkupCallback = new MarkupTreeCallback(markup.first, stIdentifier);
							pMarkupCallback->SigDelete.Connect(&SigDeleteMarkup, &core::Signal<const std::wstring&, const std::wstring&>::operator());
							markupNode.pCallback = pMarkupCallback;
							localNode.vChilds.push_back(markupNode);
						}

						categoryNode.vChilds.push_back(localNode);
					}

					m_root.vChilds.push_back(categoryNode);
				}

				SigUpdated();

				m_isDirty = false;
			}
		}

		void LocalizationTreeModel::OnRefresh(void)
		{
			m_isDirty = true;
		}

	}
}

