#pragma once
#include "Gui\ITreeModel.h"

namespace acl
{
	namespace core
	{
		class Localization;
	}

	namespace editor
	{

		class LocalizationTreeModel :
			public gui::ITreeModel
		{
		public:
			LocalizationTreeModel(core::Localization& localization);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnRefresh(void);

			core::Signal<const std::wstring&> SigSelectLocal;
			core::Signal<const std::wstring&> SigDeleteLocal;
			core::Signal<const std::wstring&> SigAddMarkup;
			core::Signal<const std::wstring&, const std::wstring&> SigDeleteMarkup;

			core::Signal<const std::wstring&> SigDeleteCategory;
			core::Signal<const std::wstring&> SigAddLocalToCategory;

		private:
			
			bool m_isDirty;

			gui::Node m_root;
			core::Signal<> SigUpdated;

			core::Localization* m_pLocalization;
		};

	}
}


