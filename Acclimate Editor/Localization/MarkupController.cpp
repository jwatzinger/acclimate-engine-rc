#include "MarkupController.h"
#include "Core\LocalizedString.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\Table.h"
#include "Gui\TableItem.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MarkupController::MarkupController(gui::Module& module) :
			BaseController(module, L"../Editor/Menu/Localization/Markups.axm"), m_pLocal(nullptr)
		{
			m_pDock = GetWidgetByName<gui::Dock>(L"Dock");

			m_pTable = GetWidgetByName<gui::Table>(L"Table");
		}

		void MarkupController::SetLocal(core::LocalizedString& local)
		{
			if(m_pLocal != &local)
			{
				m_pLocal = &local;
				OnRefresh();
			}
		}

		void MarkupController::OnDock(gui::DockArea& area, gui::DockType type, bool stack)
		{
			area.AddDock(*m_pDock, type, stack);
		}

		void MarkupController::OnRefresh(void)
		{
			m_pTable->Clear();

			for(auto& data : m_vData)
			{
				RemoveWidget(*data.GetWidget());
			}
			m_vData.clear();

			for(auto& markup : m_pLocal->GetMarkups())
			{
				auto& item = m_pTable->AddRow(markup.first);
				auto& textbox = AddWidget<gui::Textbox<>>(0.0f, 0.0f, 1.0f, 1.0f, markup.second);
				item.AddChild(textbox);

				DataContainer data(textbox, textbox.SigContentSet, markup.first);
				data.SigAccess.Connect(this, &MarkupController::OnChangeMarkup);
				m_vData.emplace_back(std::move(data));

			}
		}

		void MarkupController::OnChangeMarkup(std::wstring stMarkup, std::wstring stValue)
		{
			ACL_ASSERT(m_pLocal);
			ACL_ASSERT(m_pLocal->HasMarkup(stMarkup));

			m_pLocal->AddMarkup(stMarkup, stValue);
		}
	}
}

