#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace core
	{
		class LocalizedString;
	}

	namespace gui
	{
		class DockArea;
		class Dock;
		class Table;

		enum class DockType;
	}

	namespace editor
	{

		class MarkupController :
			public gui::BaseController
		{
			typedef gui::DataContainer<std::wstring, gui::Textbox<>, std::wstring> DataContainer;
			typedef std::vector<DataContainer> DataVector;
		public:
			MarkupController(gui::Module& module);

			void SetLocal(core::LocalizedString& local);

			void OnDock(gui::DockArea& area, gui::DockType type, bool stack);
			void OnRefresh(void);

		private:

			void OnChangeMarkup(std::wstring stMarkup, std::wstring stValue);

			core::LocalizedString* m_pLocal;

			gui::Dock* m_pDock;
			gui::Table* m_pTable;

			DataVector m_vData;
		};

	}
}
