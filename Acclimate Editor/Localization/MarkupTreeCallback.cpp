#include "MarkupTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MarkupTreeCallback::MarkupTreeCallback(const std::wstring& stMarkup, const std::wstring& stIdentifier) : m_stMarkup(stMarkup),
			m_stIdentifier(stIdentifier)
		{
		}

		const std::wstring* MarkupTreeCallback::GetIconName(void) const
		{
			static const std::wstring stIcon = L"MarkupIcon";
			return &stIcon;
		}

		bool MarkupTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool MarkupTreeCallback::IsDeletable(void) const
		{
			return true;
		}

		bool MarkupTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void MarkupTreeCallback::OnSelect(void)
		{
			//SigSelect(m_entity);
		}

		void MarkupTreeCallback::OnDelete(void)
		{
			SigDelete(m_stMarkup, m_stIdentifier);
		}

		gui::ContextMenu* MarkupTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);

			// attach component
			//auto& attach = pEntityMenu->AddItem(L"Attach component");
			//attach.SigReleased.Connect(this, &EntityTreeCallback::OnAttach);
			//attach.SigShortcut.Connect(this, &EntityTreeCallback::OnAttach);
			//attach.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'A', gui::ShortcutState::ALWAYS);
			//pEntityMenu->InsertSeperator();

			//// goto
			//auto& gotoItem = pEntityMenu->AddItem(L"Goto");
			//gotoItem.SigReleased.Connect(this, &EntityTreeCallback::OnGoto);
			//gotoItem.SigShortcut.Connect(this, &EntityTreeCallback::OnGoto);
			//gotoItem.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'G', gui::ShortcutState::ALWAYS);
			//pEntityMenu->InsertSeperator();

			//// properties 
			//pEntityMenu->AddItem(L"Properties");
			return pEntityMenu;
		}

		void MarkupTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void MarkupTreeCallback::OnRename(const std::wstring& stName)
		{
			//SigRename(m_entity, stName);
		}

	}
}