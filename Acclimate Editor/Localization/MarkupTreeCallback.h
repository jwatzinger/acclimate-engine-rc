#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace editor
	{

		class MarkupTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			MarkupTreeCallback(const std::wstring& stMarkup, const std::wstring& stIdentifier);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const std::wstring&, const std::wstring&> SigDelete;

		private:

			std::wstring m_stMarkup;
			std::wstring m_stIdentifier;
		};

	}
}


