#include "MainController.h"
#include "Editor.h"
#include "Utility.h"

#include "Core\Scene.h"
#include "Core\BaseContext.h"
#include "Core\ModuleManager.h"
#include "Entity\Utility.h"

#include "Gui\MenuBar.h"
#include "Gui\Toolbar.h"
#include "Gui\Window.h"
#include "Gui\DockArea.h"
#include "File\File.h"
#include "Script\Core.h"
#include "System\WorkingDirectory.h"

#include "ProjectModule.h"
#include "Project.h"
#include "EntityModule.h"
#include "AssetModule.h"
#include "ToolModule.h"
#include "ScriptModule.h"
#include "Events\EventModule.h"
#include "Localization\LocalizationModule.h"
#include "Audio\AudioModule.h"
#include "Input\InputModule.h"
#include "PluginController.h"
#include "Scenes\SceneModule.h"

namespace acl
{
	namespace editor
	{

		void setGameViewDummy(const std::wstring& stName)
		{
		}

		void activateScene(const std::wstring& stName)
		{
		}

		MainController::MainController(gui::Module& module, const core::GameStateContext& context): BaseController(module, L"../Editor/Menu/MainWindow.axm"),
			m_ctx(context)
		{
			// main window
			m_pMainWindow = GetWidgetByName<gui::Window>(L"MainWindow");

			// menu bar
			auto pMenuBar = GetWidgetByName<gui::MenuBar>(L"Menubar");
			auto pToolBar = GetWidgetByName<gui::Toolbar>(L"Toolbar");

			auto pDockArea = GetWidgetByName<gui::DockArea>(L"MainArea");

			// project module
			auto& projects = AddModule<ProjectModule>(*pMenuBar);
			projects.SigNew.Connect(this, &MainController::OnProjectNew);
			projects.SigLoaded.Connect(this, &MainController::OnProjectLoaded);
			projects.SigSaved.Connect(this, &MainController::OnProjectSaved);
			projects.SigQuit.Connect(this, &MainController::OnQuit);
			projects.SigClose.Connect(this, &MainController::OnProjectClosed);
			m_pMainWindow->SigClose.Connect(&projects, &ProjectModule::OnQuit);

			// asset module
			m_pAssets = &AddModule<AssetModule>(*this, *pMenuBar, context.gfx, context.render.renderer);

			// entity module
			auto& entity = AddModule<EntityModule>(*this, *pMenuBar, *pDockArea, context.ecs.entities);

			// tool module
			m_pTools = &AddModule<ToolModule>(*this, module, *pMenuBar, *context.core.pModules, context.render.renderer);

			// script module
			AddModule<ScriptModule>(*this, *pMenuBar, context.script);

			// scene module
			m_pScenes = &AddModule<SceneModule>(*this, *pMenuBar, *pDockArea, context.core.scenes, context.core.sceneLoader);
			m_pScenes->SigSceneChanged.Connect(this, &MainController::OnSceneChanged);
			m_pScenes->SigSave.Connect(this, &MainController::OnSceneSave);
			m_pScenes->SigSceneAdded.Connect(this, &MainController::OnNewScene);

			// event module
			AddModule<EventModule>(*this, *pMenuBar, context.core.events, context.gfx.line, context.render.renderer);

			// localization
			AddModule<LocalizationModule>(*this, *pMenuBar, context.core.localization);

			// audio
			AddModule<AudioModule>(*this, *pMenuBar, context.audio);

			// input
			AddModule<InputModule>(*this, *pMenuBar, context.input);

			// editor interface
			m_pEditor = new Editor(entity.GetComponentRegistry(), context, *pDockArea, entity.GetEntityExplorer(), *pMenuBar->GetItem(L"SCENES"));

			// register setGameView dummy function
			context.script.core.RegisterGlobalFunction("void setGameView(const string& in)", asFUNCTION(setGameViewDummy));

			// toolbuttons
			m_pPlay = pToolBar->AddButton(L"", L"Game");
			m_pStop = pToolBar->AddButton(L"", L"Game");

			m_pPlay->SigReleased.Connect(this, &MainController::OnPlay);
			m_pStop->SigReleased.Connect(this, &MainController::OnStop);

			m_pStop->OnDisable();
		}

		MainController::~MainController(void)
		{
			for(auto pModules : m_vModules)
			{
				delete pModules;
			}

			delete m_pEditor;
		}

		IEditor& MainController::GetEditor(void)
		{
			return *m_pEditor;
		}

		void MainController::Update(void)
		{
			gui::BaseController::Update();

			for(auto pModule : m_vModules)
			{
				pModule->Update();
			}
		}

		void MainController::Render(void) const
		{
			m_pEditor->Render();
		}

		void MainController::OnQuit(void)
		{
			SigQuit();
		}

		void MainController::OnProjectNew(const Project& project)
		{
			sys::WorkingDirectory dir(file::FilePath(project.GetPath()) + L"/Game/");
			setProjectDirectory(dir.GetDirectory());

			const std::wstring stDataPath = L"Data/";

			for(auto pModule : m_vModules)
			{
				const auto type = pModule->GetModuleType();
				if(project.HasModule(type))
					pModule->OnNew(stDataPath + project.GetModule(type));
			}
		}

		void MainController::OnProjectLoaded(const Project& project)
		{
			m_pMainWindow->SetLabel(L"Acclimate engine - " + project.GetName());

			sys::WorkingDirectory dir(file::FilePath(project.GetPath()) + L"/Game/");
			setProjectDirectory(dir.GetDirectory());

			const std::wstring stDataPath = L"Data/";

			for(auto pModule : m_vModules)
			{
				const auto type = pModule->GetModuleType();
				if(project.HasModule(type))
					pModule->OnLoad(stDataPath + project.GetModule(type));
			}

			for(auto& resources : m_pTools->GetPlugins().GetResources())
			{
				m_pAssets->AddResourceBlock(resources.first, *resources.second, true);
			}

			// call script init
			auto startUp = m_ctx.script.core.GetGlobalFunction("void startUp()");
			startUp.Call<void>();

			dir.Restore();

			SigProjectLoaded();
		}

		void MainController::OnProjectSaved(const Project& project)
		{
			const std::wstring stDataPath = file::FilePath(project.GetPath()) + L"/Game/Data/";

			for(auto pModule : m_vModules)
			{
				const auto type = pModule->GetModuleType();
				if(project.HasModule(type))
					pModule->OnSave(stDataPath + project.GetModule(type));
			}
		}

		void MainController::OnProjectClosed(void)
		{
			// call script destroy
			auto shutDown = m_ctx.script.core.GetGlobalFunction("void shutDown()");
			shutDown.Call<void>();

			SigProjectClosed();

			for(auto pModule : m_vModules)
			{
				pModule->OnClose();
			}

			m_ctx.script.core.DiscardModule();

			ecs::ResetIdCounts();

			setProjectDirectory(L"");
		}

		void MainController::OnSceneChanged(void)
		{
			m_pAssets->AddResourceBlock(L"Scene", *m_pScenes->GetResources(), false);
			for(auto pModule : m_vModules)
			{
				pModule->OnSceneChanged();
			}
		}

		core::Scene::ModuleType projectToSceneModule(Project::ModuleType type)
		{
			switch(type)
			{
			case Project::ModuleType::EVENTS:
				return core::Scene::ModuleType::EVENTS;
			case Project::ModuleType::PHYSICS:
				return core::Scene::ModuleType::PHYSICS;
			case Project::ModuleType::RESOURCES:
				return core::Scene::ModuleType::RESOURCES;
			case Project::ModuleType::SCRIPTS:
				return core::Scene::ModuleType::SCRIPTS;
			case Project::ModuleType::ENTITIES:
				return core::Scene::ModuleType::ENTITIES;
			default:
				return core::Scene::ModuleType::UNKNOWN;
			}
		}

		Project::ModuleType sceneToProjectModule(core::Scene::ModuleType type)
		{
			switch(type)
			{
			case core::Scene::ModuleType::EVENTS:
				return Project::ModuleType::EVENTS;
			case core::Scene::ModuleType::PHYSICS:
				return Project::ModuleType::PHYSICS;
			case core::Scene::ModuleType::RESOURCES:
				return Project::ModuleType::RESOURCES;
			case core::Scene::ModuleType::SCRIPTS:
				return Project::ModuleType::SCRIPTS;
			case core::Scene::ModuleType::ENTITIES:
				return Project::ModuleType::ENTITIES;
			default:
				return Project::ModuleType::UNKNOWN;
			}
		}

		void MainController::OnNewScene(const core::Scene& scene)
		{
			sys::WorkingDirectory dir(scene.GetPath());
			for(auto pModule : m_vModules)
			{
				const auto sceneModule = projectToSceneModule(pModule->GetModuleType());
				if(sceneModule != core::Scene::ModuleType::UNKNOWN)
				{
					if(scene.HasModule(sceneModule))
						pModule->OnNewScene(scene.GetName(), scene.GetModule(sceneModule));
				}
			}
		}

		void MainController::OnSceneSave(const core::Scene& scene)
		{
			const std::wstring& stPath = scene.GetPath() + L"\\";
			for(auto pModule : m_vModules)
			{
				const auto sceneModule = projectToSceneModule(pModule->GetModuleType());
				if(sceneModule != core::Scene::ModuleType::UNKNOWN)
				{
					if(scene.HasModule(sceneModule))
						pModule->OnSaveScene(stPath + scene.GetModule(sceneModule));
				}
			}
		}

		void MainController::OnPlay(void)
		{
			for(auto pModule : m_vModules)
			{
				pModule->OnBeginTest();
			}

			m_pPlay->OnDisable();
			m_pStop->OnEnable();

			SigBeginTest();
		}

		void MainController::OnStop(void)
		{
			for(auto pModule : m_vModules)
			{
				pModule->OnEndTest();
			}

			m_pPlay->OnEnable();
			m_pStop->OnDisable();

			SigEndTest();
		}

	}
}