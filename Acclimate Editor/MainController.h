#pragma once
#include "IModule.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		struct GameStateContext;
		class Scene;
	}

	namespace gui
	{
		class Window;
	}

	namespace editor
	{

		class IEditor;
		class Project;
		class AssetModule;
		class ToolModule;
		class SceneModule;

		class MainController :
			public gui::BaseController
		{
			typedef std::vector<IModule*> ModuleVector;
		public:
			MainController(gui::Module& module, const core::GameStateContext& context);
			~MainController(void);

			IEditor& GetEditor(void);

			void Update(void);
			void Render(void) const;

			core::Signal<> SigQuit;
			core::Signal<> SigProjectLoaded;
			core::Signal<> SigProjectClosed;
			core::Signal<> SigBeginTest;
			core::Signal<> SigEndTest;

		private:

			template<typename Module, typename... Args>
			Module& AddModule(Args&&... args)
			{
				auto pModule = new Module(args...);
				m_vModules.push_back(pModule);
				return *pModule;
			}
			
			void OnQuit(void);
			void OnProjectNew(const Project& project);
			void OnProjectLoaded(const Project& project);
			void OnProjectSaved(const Project& project);
			void OnProjectClosed(void);
			void OnNewScene(const core::Scene& scene);
			void OnSceneChanged(void);
			void OnSceneSave(const core::Scene& scene);
			void OnPlay(void);
			void OnStop(void);

			IEditor* m_pEditor;
			AssetModule* m_pAssets;
			ToolModule* m_pTools;
			SceneModule* m_pScenes;

			ModuleVector m_vModules;

			gui::Widget* m_pPlay, *m_pStop;
			gui::Window* m_pMainWindow;

			const core::GameStateContext& m_ctx;
		};

	}
}

