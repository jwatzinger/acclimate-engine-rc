#include "MainState.h"
#include "ApiDef.h"
#include "MainController.h"
#include "PluginLoader.h"
#include "PluginManager.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\ILine.h"
#include "Gui\Renderer.h"
#include "Gui\InputHandler.h"
#include "Gui\LayoutLoader.h"
#include "Gui\RegisterScript.h"
#include "Input\Loader.h"
#include "Render\ILoader.h"
#include "Render\IRenderer.h"

namespace acl
{
	namespace editor
	{

		MainState::MainState(const core::GameStateContext& ctx, BaseEngine& engine) : BaseState(ctx),
			m_package(ctx.gfx.resources.fonts, ctx.gfx.resources.textures), m_pEngine(&engine)
		{
			ctx.gfx.load.pLoader->Load(L"../Editor/Data/Resources.axm");
			ctx.render.loader.Load(L"../Editor/Data/Render.axm");

			InitGui();

			m_pController = new MainController(m_package.GetContext().module, ctx);
			m_pController->SigQuit.Connect(this, &MainState::OnQuit);
			m_pController->SigBeginTest.Connect(this, &MainState::OnBeginTest);
			m_pController->SigEndTest.Connect(this, &MainState::OnEndTest);
			
			m_pPlugins = new PluginManager(m_pController->GetEditor(), ctx.gfx.load);

			m_pLoader = new PluginLoader(*m_pPlugins, *ctx.core.pModules);
			m_pController->SigProjectClosed.Connect(m_pPlugins, &PluginManager::Clear);
			m_pController->SigProjectLoaded.Connect(m_pLoader, &PluginLoader::LoadFromModules);

			// set line ffect
			ctx.gfx.line.SetEffect(*ctx.gfx.resources.effects[L"EditorGUILines"]);
			
			// stop module update
			m_pEngine->AllowModuleUpdate(false);
		}

		MainState::~MainState(void)
		{
			delete m_pLoader;
			delete m_pPlugins;
			delete m_pController;
		}

		core::IState* MainState::Run(double dt)
		{
			if(m_pCurrentState)
			{
				m_ctx.gui.module.m_cursor.SetVisible(true);

				// run gui
				m_package.Render();
				m_package.Update();

				m_pController->Update();
				m_pPlugins->UpdatePlugins();
			}
			
			return m_pCurrentState;
		}

		void MainState::Render(void) const
		{
			m_pController->Render();
		}

		void MainState::InitGui(void)
		{
			m_package.InitRenderer(m_ctx.gfx);
			auto& gui = m_package.GetContext();

			gui.input.SetHandler(*m_ctx.input.loader.Load(L"../Editor/Data/Input.axm"));

#ifdef ACL_API_DX9
			gfx::IEffect& effect = *m_ctx.gfx.resources.effects.Get(L"EditorGUI");
#else
			gfx::IEffect& effect = *m_ctx.gfx.resources.effects.Get(L"EditorGUIGeometry");
#endif
			gui.pRenderer->SetEffect(effect);
			gui.pRenderer->SetStage(*m_ctx.render.renderer.GetStage(L"editorgui"));
			gui.loader.Load(L"../Editor/Menu/Editor.axl");

			gui.module.SigMessageLoop.Connect(this, &MainState::OnGuiMessageLoop);
			gui.module.SigMessageLoopEnd.Connect(m_pEngine, &BaseEngine::OnGuiMessageLoopEnd);

			gui::setScriptRenderer(*gui.pRenderer);
		}

		void MainState::OnGuiMessageLoop(void)
		{
			m_package.Render();
			m_pEngine->OnGuiMessageLoop();
		}

		void MainState::OnQuit(void)
		{
			m_pCurrentState = nullptr;
			m_pPlugins->Clear();
		}

		void MainState::OnBeginTest(void)
		{
			m_pPlugins->BeginTest();
			m_pEngine->AllowModuleUpdate(true);
		}

		void MainState::OnEndTest(void)
		{
			m_pEngine->AllowModuleUpdate(false);
			m_pPlugins->EndTest();
		}

	}
}
