#pragma once
#include "Engine.h"
#include "Core\BaseState.h"
#include "Gui\Package.h"

namespace acl
{
	namespace editor
	{

		class MainController;
		class PluginManager;
		class PluginLoader;

		class MainState :
			public core::BaseState
		{
		public:
			MainState(const core::GameStateContext& ctx, BaseEngine& engine);
			~MainState(void);

			core::IState* Run(double dt) override;

			void Render(void) const override;

		private:

			void InitGui(void);

			void OnQuit(void);
			void OnGuiMessageLoop(void);
			void OnBeginTest(void);
			void OnEndTest(void);

			MainController* m_pController;

			PluginManager* m_pPlugins;
			PluginLoader* m_pLoader;

			BaseEngine* m_pEngine;
			gui::Package m_package;

		};

	}
}