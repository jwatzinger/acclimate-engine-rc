#include "MaterialAttributeController.h"
#include "TextureViewerController.h"
#include "MaterialTextureController.h"
#include "TexturePickController.h"
#include "Gui\Image.h"
#include "System\Assert.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		MaterialAttributeController::MaterialAttributeController(gui::Module& module, gui::Widget& parent, const gfx::Effects& effects, const gfx::Textures& textures) : 
			BaseController(module, parent, L"../Editor/Menu/Asset/MaterialAttributes.axm"), m_pTextures(&textures), m_pEffects(&effects)
		{
			m_pViewer = &AddController<TextureViewerController>(textures);

			// name box
			m_pName = GetWidgetByName<gui::Textbox<>>(L"Name");

			// delete button
			GetWidgetByName<gui::Widget>(L"Delete")->SigReleased.Connect(this, &MaterialAttributeController::OnDelete);

			// add texture button
			m_pAddTexture = GetWidgetByName<gui::Widget>(L"AddTexture");
			m_pAddTexture->SigReleased.Connect(this, &MaterialAttributeController::OnAddTexture);
			
			// texture previews
			for(unsigned int i = 0; i < 4; i++)
			{
				AddTextureCtrl(i);
			}

			// effects
			m_pEffectBox = GetWidgetByName<EffectBox>(L"Effects");
			for(auto& effect : effects.Map())
			{
				m_pEffectBox->AddItem(effect.first, effect.first);
			}
			m_pEffectBox->SigItemPicked.Connect(this, &MaterialAttributeController::OnChangeEffect);

			// permutation
			m_pPermutation = GetWidgetByName<PermutationBox>(L"Permutation");
			m_pPermutation->SigContentSet.Connect(this, &MaterialAttributeController::OnChangePermutation);
		}

		void MaterialAttributeController::OnSelectMaterial(const gfx::MaterialLoadInfo& info)
		{
			m_info = info;

			Refresh();
		}

		void MaterialAttributeController::OnEffectsChanged(void)
		{
			m_pEffectBox->Clear();
			for(auto& effect : m_pEffects->Map())
			{
				m_pEffectBox->AddItem(effect.first, effect.first);
			}
		}

		void MaterialAttributeController::AddTextureCtrl(unsigned int id)
		{
			const std::wstring stId = conv::ToString(id);
			if(gui::Image* pImage = GetWidgetByName<gui::Image>(L"Texture" + stId))
			{
				TextureContainer container(*pImage, pImage->SigReleased, pImage);
				container.SigAccess.Connect(this, &MaterialAttributeController::OnPreviewTexture);
				m_vTextures.push_back(std::move(container));
			}
			if(gui::Widget* pArea = GetWidgetByName<gui::Widget>(L"TextureArea" + stId))
			{
				auto& controller = AddController<MaterialTextureController>(*pArea, id, *m_pTextures);
				controller.SigTextureChangedId.Connect(this, &MaterialAttributeController::OnChangeTexture);
				controller.SigDelete.Connect(this, &MaterialAttributeController::OnDeleteTexture);
				m_vTextureController.push_back(&controller);
			}
				
			ACL_ASSERT(m_vTextures.size() == m_vTextureController.size());
		}

		void MaterialAttributeController::Refresh(void)
		{
			// name
			m_pName->SetText(m_info.stName);

			// effect
			m_pEffectBox->SelectByName(m_info.stEffect);

			const size_t numTextures = m_info.vTextures.size();
			// add texture button
			m_pAddTexture->SetVisible(numTextures < 4);
			m_pAddTexture->SetY(0.03f + numTextures*0.25f);

			/**************************************
			* TEXTURES
			**************************************/
			ACL_ASSERT(numTextures <= m_vTextures.size());

			unsigned int id = 0;
			for(auto& stTexture : m_info.vTextures)
			{
				// texture preview
				auto& texture = m_vTextures[id];
				texture->SetFileName(stTexture);
				texture->SetVisible(true);

				// texture controller
				auto& textureController = m_vTextureController[id];
				textureController->SetTexture(stTexture);
				textureController->OnToggle(true);
				id++;
			}

			for(unsigned int i = id; i < m_vTextures.size(); i++)
			{
				m_vTextures[i]->SetVisible(false);
				m_vTextureController[i]->OnToggle(false);
			}
		}

		void MaterialAttributeController::OnPreviewTexture(gui::Image* pImage)
		{
			m_pViewer->Execute(pImage->GetFilename());
		}

		void MaterialAttributeController::OnAddTexture(void)
		{
			TexturePickController pick(*m_pModule, *m_pTextures);

			std::wstring stName;
			if (pick.Execute(nullptr, &stName))
			{
				m_info.vTextures.push_back(stName);

				SigMaterialChanged(m_info);

				Refresh();
			}
		}

		void MaterialAttributeController::OnDeleteTexture(unsigned int id)
		{
			ACL_ASSERT(id < m_info.vTextures.size());

			m_info.vTextures.erase(m_info.vTextures.begin()+id);

			SigMaterialChanged(m_info);

			Refresh();
		}

		void MaterialAttributeController::OnChangeEffect(std::wstring stName)
		{
			m_info.stEffect = stName;

			SigMaterialChanged(m_info);
		}

		void MaterialAttributeController::OnChangeTexture(unsigned int id, std::wstring stName)
		{
			m_info.vTextures[id] = stName;

			m_vTextures[id]->SetFileName(stName);

			SigMaterialChanged(m_info);
		}

		void MaterialAttributeController::OnDelete(void)
		{
			SigDelete();
		}

		void MaterialAttributeController::OnChangePermutation(int permutation)
		{
			m_info.permutation = permutation;

			SigMaterialChanged(m_info);
		}

	}
}

