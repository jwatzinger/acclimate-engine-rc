#pragma once
#include "Gui\BaseController.h"
#include "Gui\DataContainer.h"
#include "Gui\ComboBox.h"
#include "Gui\Textbox.h"
#include "Gfx\Effects.h"
#include "Gfx\Textures.h"
#include "Gfx\IMaterial.h"

namespace acl
{
	namespace gui
	{
		class Image;
	}
	
	namespace editor
	{

		class TextureViewerController;
		class MaterialTextureController;

		class MaterialAttributeController:
			public gui::BaseController
		{
			typedef gui::ComboBox<std::wstring> EffectBox;
			typedef gui::Textbox<int> PermutationBox;
			typedef gui::DataContainer<gui::Image*, gui::Image> TextureContainer;
			typedef std::vector<TextureContainer> TextureVector;
			typedef std::vector<MaterialTextureController*> TextureControllerVector;
		public:
			MaterialAttributeController(gui::Module& module, gui::Widget& parent, const gfx::Effects& effects, const gfx::Textures& textures);

			core::Signal<> SigDelete;

			void OnSelectMaterial(const gfx::MaterialLoadInfo& info);
			void OnEffectsChanged(void);

			core::Signal<const gfx::MaterialLoadInfo&> SigMaterialChanged;
			
		private:

			void AddTextureCtrl(unsigned int id);
			void Refresh(void);

			void OnPreviewTexture(gui::Image* pImage);
			void OnAddTexture(void);
			void OnDelete(void);
			void OnDeleteTexture(unsigned int id);
			void OnChangeEffect(std::wstring stName);
			void OnChangeTexture(unsigned int id, std::wstring stName);
			void OnChangePermutation(int permutation);

			gfx::MaterialLoadInfo m_info;
			const gfx::Textures* m_pTextures;
			const gfx::Effects* m_pEffects;

			gui::Textbox<>* m_pName;
			gui::Widget* m_pAddTexture;
			TextureVector m_vTextures;
			EffectBox* m_pEffectBox;
			PermutationBox* m_pPermutation;
			TextureControllerVector m_vTextureController;


			TextureViewerController* m_pViewer;
		};

	}
}