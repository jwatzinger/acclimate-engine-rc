#pragma once
#include "Gfx\Materials.h"

namespace acl
{
	namespace editor
	{

		struct MaterialBlockData
		{
			MaterialBlockData(void) : pBlock(nullptr), bLocked(false)
			{
			}

			MaterialBlockData(const gfx::Materials::Block& block, bool bLocked) : pBlock(&block),
			bLocked(bLocked)
			{
			}

			const gfx::Materials::Block* pBlock;
			bool bLocked;
		};

	}
}

