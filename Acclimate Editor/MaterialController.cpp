#include "MaterialController.h"
#include "MaterialCreatorController.h"
#include "MaterialAttributeController.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\IMaterial.h"
#include "Gui\TreeView.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MaterialController::MaterialController(gui::Module& module, gfx::Materials& materials, const gfx::IMaterialLoader& loader, const gfx::Effects& effects, const gfx::Textures& textures) : 
			BaseController(module, L"../Editor/Menu/Asset/Materials.axm"), m_materials(materials), m_pLoader(&loader), m_pEffects(&effects), m_pTextures(&textures),
			m_model(m_mMaterials)
		{
			m_pTree = GetWidgetByName<gui::TreeView>(L"Materials");
			m_pTree->SetModel(&m_model);

			GetWidgetByName<gui::Widget>(L"NewMaterial")->SigReleased.Connect(this, &MaterialController::OnNewMaterial);

			// attributes
			m_pAttributes = GetWidgetByName<gui::Widget>(L"Attributes");
			m_pAttributeController = &AddController<MaterialAttributeController>(*m_pAttributes, effects, textures);
			m_pAttributeController->SigDelete.Connect(this, &MaterialController::OnDeleteSelected);
			m_pAttributeController->SigMaterialChanged.Connect(this, &MaterialController::OnMaterialChanged);

			OnRefreshList();

			m_model.SigMaterialSelected.Connect(this, &MaterialController::OnPickMaterial);
		}

		MaterialController::~MaterialController(void)
		{
			m_pTree->SetModel(nullptr);
			m_materials.Clear();
		}

		void MaterialController::Update(void)
		{
			BaseController::Update();

			m_model.Update();
		}

		void MaterialController::AddBlock(const std::wstring& stName, const gfx::Materials::Block& materials, bool bLocked)
		{
			m_mMaterials[stName] = MaterialBlockData(materials, bLocked);
		}

		void MaterialController::Begin(void)
		{
			m_materials.Begin();
		}

		void MaterialController::End(void)
		{
			m_materials.End();
			OnMaterialsChanged();
		}

		void MaterialController::Clear(void)
		{
			m_materials.Clear();
			OnMaterialsChanged();
		}

		void MaterialController::OnEffectsChanged(void)
		{
			m_pAttributeController->OnEffectsChanged();
		}

		void MaterialController::OnMaterialsChanged(void)
		{
			OnRefreshList();
			SigMaterialsChanged();
		}

		void MaterialController::OnNewMaterial(void)
		{
			gfx::MaterialLoadInfo info;
			info.permutation = 0;
			MaterialCreatorController creator(*m_pModule, *m_pEffects);
			if(creator.Execute(info))
			{
				gfx::TextureVector vTextures;
				if(m_pLoader->Load(info.stName, info.stEffect, 0, vTextures))
					//m_pList->SelectByName(info.stName);
					int i = 0;
			}

			m_pTree->OnFocus(true);
		}

		void MaterialController::OnRefreshList(void)
		{
			m_model.OnMaterialsChanged();
		}

		void MaterialController::OnPickMaterial(const gfx::IMaterial* pMaterial)
		{
			bool bMaterialExists = pMaterial != nullptr;

			m_pAttributes->SetVisible(bMaterialExists);

			if(bMaterialExists)
			{
				m_pAttributeController->OnSelectMaterial(*pMaterial->GetLoadInfo());
			}	
		}

		void MaterialController::OnDeleteSelected(void)
		{
			//int id = m_pList->GetSelectedId();
			//if(id != -1)
			//{
			//	m_materials.Delete(id);
			//	SigMaterialsChanged();
			//	OnRefreshList();
			//}
		}

		void MaterialController::OnMaterialChanged(const gfx::MaterialLoadInfo& info)
		{
			////auto pMaterial = m_pList->GetContent();
			//auto& thisInfo = *pMaterial->GetLoadInfo();

			//if(thisInfo.stEffect != info.stEffect || thisInfo.permutation != info.permutation)
			//	pMaterial->SetEffect(*m_pEffects->Get(info.stEffect)->GetPermutation(info.permutation));

			//unsigned int i = 0;
			//for(auto& stTexture : info.vTextures)
			//{
			//	if(i >= thisInfo.vTextures.size() || stTexture != thisInfo.vTextures[i])
			//		pMaterial->SetTexture(i, m_pTextures->Get(stTexture));
			//	i++;
			//}
		}

	}
}

