#pragma once
#include "MaterialBlockData.h"
#include "MaterialTreeModel.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"
#include "Gfx\Materials.h"
#include "Gfx\Effects.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gfx
	{
		class IMaterialLoader;
		struct MaterialLoadInfo;
	}

	namespace gui
	{
		class TreeView;
	}

	namespace editor
	{

		class MaterialAttributeController;

		class MaterialController :
			public gui::BaseController
		{
			typedef std::map<std::wstring, MaterialBlockData> MaterialMap;
		public:

			MaterialController(gui::Module& module, gfx::Materials& materials, const gfx::IMaterialLoader& loader, const gfx::Effects& effects, const gfx::Textures& textures);
			~MaterialController(void);

			void Update(void) override;

			void AddBlock(const std::wstring& stName, const gfx::Materials::Block& materials, bool bLocked);

			void Begin(void);
			void End(void);
			void Clear(void);

			void OnEffectsChanged(void);

			core::Signal<> SigMaterialsChanged;

		private:

			void OnMaterialsChanged(void);
			void OnNewMaterial(void);
			void OnRefreshList(void);
			void OnPickMaterial(const gfx::IMaterial* pMaterial);
			void OnMaterialChanged(const gfx::MaterialLoadInfo& info);
			void OnDeleteSelected(void);

			MaterialMap m_mMaterials;
			gfx::Materials::Block m_materials;
			const gfx::Effects* m_pEffects;
			const gfx::Textures* m_pTextures;
			const gfx::IMaterialLoader* m_pLoader;

			gui::TreeView* m_pTree;
			gui::Widget* m_pAttributes;
			MaterialTreeModel m_model;

			MaterialAttributeController* m_pAttributeController;
		};

	}
}

