#include "MaterialCreatorController.h"
#include "Gui\BaseWindow.h"
#include "Gfx\IMaterial.h"

namespace acl
{
	namespace editor
	{

		MaterialCreatorController::MaterialCreatorController(gui::Module& module, const gfx::Effects& effects) : 
			BaseController(module, L"../Editor/Menu/Asset/MaterialCreator.axm"), m_pEffects(&effects), m_bConfirmed(false)
		{	
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"CreatorWindow");
			m_pWindow->SetAreaClipping(false);

			m_pEffectBox = &AddWidget<EffectBox>(0.05f, 0.5f, 0.9f, 22.0f, 10.0f);
			m_pEffectBox->SetPositionModes(gui::PositionMode::REL, gui::PositionMode::REL, gui::PositionMode::REL, gui::PositionMode::ABS);

			m_pNameBox = GetWidgetByName<gui::Textbox<>>(L"MaterialName");
			m_pNameBox->SigConfirm.Connect(this, &MaterialCreatorController::OnConfirm);

			GetWidgetByName<gui::Widget>(L"OkButton")->SigReleased.Connect(this, &MaterialCreatorController::OnConfirm);
			GetWidgetByName<gui::Widget>(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);

			OnRefreshEffects();
		}

		bool MaterialCreatorController::Execute(gfx::MaterialLoadInfo& info)
		{
			m_bConfirmed = false;
			m_pNameBox->OnFocus(true);
			m_pWindow->Execute();

			if(m_bConfirmed)
			{
				info.stName = m_pNameBox->GetContent();
				info.stEffect = m_pEffectBox->GetContent();
			}
			return m_bConfirmed;
		}

		void MaterialCreatorController::OnRefreshEffects(void)
		{
			m_pEffectBox->Clear();
			if(m_pEffects->Size())
			{
				for(auto& effect : m_pEffects->Map())
				{
					m_pEffectBox->AddItem(effect.first, effect.first);
				}
				m_pEffectBox->Select(0);
			}
		}

		void MaterialCreatorController::OnConfirm(void)
		{
			m_pWindow->OnClose();
			m_bConfirmed = true;
		}

		void MaterialCreatorController::OnConfirm(std::wstring)
		{
			OnConfirm();
		}

	}
}

