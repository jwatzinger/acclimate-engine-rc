#pragma once
#include "Gui\BaseController.h"
#include "Gui\ComboBox.h"
#include "Gfx\Effects.h"

namespace acl
{
	namespace  gui
	{
		class BaseWindow;
	}

	namespace gfx
	{
		class IEffect;		
		struct MaterialLoadInfo;
	}

	namespace editor
	{ 

		class MaterialCreatorController :
			public gui::BaseController
		{
			typedef gui::ComboBox<std::wstring> EffectBox;
		public:
			MaterialCreatorController(gui::Module& module, const gfx::Effects& effects);

			bool Execute(gfx::MaterialLoadInfo& info);

			void OnRefreshEffects(void);

		private:

			void OnConfirm(void);
			void OnConfirm(std::wstring);

			bool m_bConfirmed;

			gui::Textbox<>* m_pNameBox;
			EffectBox* m_pEffectBox;
			gui::BaseWindow* m_pWindow;

			const gfx::Effects* m_pEffects;
		};

	}
}

