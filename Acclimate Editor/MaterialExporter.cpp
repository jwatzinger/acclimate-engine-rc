#include "MaterialExporter.h"
#include <unordered_map>
#include "System\Convert.h"
#include "System\Log.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		void MaterialExporter::Export(const std::wstring& stInFilepath, MaterialVector& vMaterials, const std::wstring& stFilename) const
		{
			std::unordered_map<std::wstring, std::wstring> mTextures;
			
			const std::wstring stExport = L"Export";
			for(auto& stTexture : vMaterials)
			{
				auto itr = mTextures.find(stTexture);
				if(itr == mTextures.end())
				{
					const auto stName = stExport + conv::ToString(mTextures.size());
					mTextures.emplace(stInFilepath + stTexture, stName);
					stTexture = stName;
				}
				else
				{
					stTexture = itr->second;
				}
			}

			xml::Doc doc;
			auto& resources = doc.InsertNode(L"Resources");

			auto& textures = resources.InsertNode(L"Textures");
			for(auto& texture : mTextures)
			{
				auto& textureNode = textures.InsertNode(L"Texture");
				textureNode.ModifyAttribute(L"name", texture.second);
				textureNode.ModifyAttribute(L"file", texture.first);
				textureNode.ModifyAttribute(L"read", L"0");
			}

			auto& materials = resources.InsertNode(L"Materials");
			auto& materialNode = materials.InsertNode(L"Material");
			materialNode.ModifyAttribute(L"name", L"Exported");
			materialNode.ModifyAttribute(L"perm", L"0");

			auto& effect = materialNode.InsertNode(L"Effect");
			effect.ModifyAttribute(L"name", L"BaseActor");

			auto& subsets = materialNode.InsertNode(L"Subsets");

			for(auto& material : vMaterials)
			{
				auto& subset = subsets.InsertNode(L"Subset");
				auto& texture = subset.InsertNode(L"Texture");
				texture.ModifyAttribute(L"name", material);
			}

			try
			{
				doc.SaveFile(stFilename);
			}
			catch(...)
			{
				sys::log->Out(sys::LogModule::EDITOR, sys::LogType::WARNING, "failed to save materials for mesh to file", stFilename);
			}
		}

	}
}

