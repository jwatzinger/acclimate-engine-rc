#pragma once
#include <vector>
#include <string>

namespace acl
{
	namespace editor
	{

		class MaterialExporter
		{
		public:

			typedef std::vector<std::wstring> MaterialVector;

			void Export(const std::wstring& stInFilepath, MaterialVector& vMaterials, const std::wstring& stFilename) const;
		};

	}
}