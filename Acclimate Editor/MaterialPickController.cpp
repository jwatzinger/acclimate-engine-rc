#include "MaterialPickController.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		MaterialPickController::MaterialPickController(gui::Module& module, const gfx::Materials& materials) : 
			BaseController(module, L"../Editor/Menu/Asset/MaterialPick.axm"), m_bConfirmed(false), m_pMaterials(&materials)
		{
			// window
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"MaterialWindow");

			m_pList = &AddWidget<MaterialList>(0.0f, 0.0f, 0.3f, 0.9f, 0.02f);
			m_pList->SigConfirm.Connect(this, &MaterialPickController::OnConfirm);

			// confirm button
			GetWidgetByName<gui::Widget>(L"Pick")->SigReleased.Connect(this, &MaterialPickController::OnConfirm);

			OnRefresh();
		}

		bool MaterialPickController::Execute(gfx::IMaterial** pMaterial, std::wstring* pName)
		{
			return Execute(L"", pMaterial, pName);
		}

		bool MaterialPickController::Execute(const std::wstring& stDefault, gfx::IMaterial** pMaterial, std::wstring* pName)
		{
			m_bConfirmed = false;
			m_pList->OnFocus(true);

			if(!stDefault.empty())
				m_pList->SelectByName(stDefault);

			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				if(pMaterial)
					*pMaterial = m_pList->GetContent();
				if(pName)
					*pName = m_pList->GetSelectedName();
			}
			return m_bConfirmed;
		}

		void MaterialPickController::OnRefresh(void)
		{
			m_pList->Clear();
			
			for(auto& material : m_pMaterials->Map())
			{
				m_pList->AddItem(material.first, material.second);
			}

			if(m_pList->GetSize())
				m_pList->SelectById(0);
		}

		void MaterialPickController::OnConfirm(void)
		{
			m_bConfirmed = true;
			m_pWindow->OnClose();
		}

	}
}

