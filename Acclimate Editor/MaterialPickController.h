#pragma once
#include "Gfx\Materials.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class MaterialPickController :
			public gui::BaseController
		{
			typedef gui::List<gfx::IMaterial*> MaterialList;
		public:
			MaterialPickController(gui::Module& module, const gfx::Materials& materials);
			
			bool Execute(gfx::IMaterial** pMesh, std::wstring* pName);
			bool Execute(const std::wstring& stDefault, gfx::IMaterial** pMesh, std::wstring* pName);

			void OnRefresh(void);

		private:

			void OnConfirm(void);

			bool m_bConfirmed;

			MaterialList* m_pList;
			gui::BaseWindow* m_pWindow;

			const gfx::Materials* m_pMaterials;
		};

	}
}


