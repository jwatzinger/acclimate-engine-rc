#include "MaterialTextureController.h"
#include "Gui\Label.h"
#include "System\Convert.h"
#include "TexturePickController.h"

namespace acl
{
	namespace editor
	{

		MaterialTextureController::MaterialTextureController(gui::Module& module, gui::Widget& parent, unsigned int id, const gfx::Textures& textures) : 
			BaseController(module, parent, L"../Editor/Menu/Asset/MaterialTexture.axm"), m_id(id), m_pTextures(&textures)
		{
			GetWidgetByName<gui::Label>(L"Label")->SetString(L"Texture" + conv::ToString(id));

			GetWidgetByName<gui::Widget>(L"Delete")->SigReleased.Connect(this, &MaterialTextureController::OnDelete);

			m_pName = GetWidgetByName<gui::Textbox<>>(L"TextureName");
			m_pName->SigReleased.Connect(this, &MaterialTextureController::OnPickTexture);
		}

		void MaterialTextureController::SetTexture(const std::wstring& stTexture)
		{
			m_pName->SetText(stTexture);
		}

		void MaterialTextureController::OnPickTexture(void)
		{
			TexturePickController pick(*m_pModule, *m_pTextures);

			std::wstring stName;
			if(pick.Execute(m_pName->GetContent(), nullptr, &stName))
			{
				m_pName->SetText(stName);
				SigTextureChanged(stName);
				SigTextureChangedId(m_id, stName);
			}
		}

		void MaterialTextureController::OnDelete(void)
		{
			SigDelete(m_id);
		}

	}
}
