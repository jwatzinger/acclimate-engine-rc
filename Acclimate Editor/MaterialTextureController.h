#pragma once
#include "Gfx\Textures.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace editor
	{

		class MaterialTextureController :
			public gui::BaseController
		{
		public:
			MaterialTextureController(gui::Module& module, gui::Widget& parent, unsigned int id, const gfx::Textures& textures);

			core::Signal<unsigned int> SigDelete;
			core::Signal<std::wstring> SigTextureChanged;
			core::Signal<unsigned int, std::wstring> SigTextureChangedId;

			void SetTexture(const std::wstring& stTexture);

		private:

			void OnPickTexture(void);
			void OnDelete(void);

			unsigned int m_id;

			gui::Textbox<>* m_pName;

			const gfx::Textures* m_pTextures;
		};

	}
}

