#include "MaterialTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MaterialTreeCallback::MaterialTreeCallback(const gfx::IMaterial& Material, bool bLocked) : m_pMaterial(&Material),
			m_bLocked(bLocked)
		{
		}

		const std::wstring* MaterialTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"MaterialLockedIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"MaterialIcon";
				return &stIcon;
			}
		}

		bool MaterialTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool MaterialTreeCallback::IsDeletable(void) const
		{
			return !m_bLocked;
		}

		bool MaterialTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void MaterialTreeCallback::OnSelect(void)
		{
			SigSelect(*m_pMaterial);
		}

		void MaterialTreeCallback::OnDelete(void)
		{
			SigDelete(*m_pMaterial);
		}

		gui::ContextMenu* MaterialTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);
			return pEntityMenu;
		}

		void MaterialTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void MaterialTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(*m_pMaterial, stName);
		}

	}
}