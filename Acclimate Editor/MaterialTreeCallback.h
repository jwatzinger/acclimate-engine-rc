#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class IMaterial;
	}

	namespace editor
	{

		class MaterialTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			MaterialTreeCallback(const gfx::IMaterial& Material, bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const gfx::IMaterial&> SigSelect;
			core::Signal<const gfx::IMaterial&> SigDelete;
			core::Signal<const gfx::IMaterial&, const std::wstring&> SigRename;

		private:

			bool m_bLocked;
			const gfx::IMaterial* m_pMaterial;
		};

	}
}


