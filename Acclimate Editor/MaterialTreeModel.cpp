#include "MaterialTreeModel.h"
#include "FolderTreeCallback.h"
#include "MaterialTreeCallback.h"

namespace acl
{
	namespace editor
	{

		MaterialTreeModel::MaterialTreeModel(const MaterialMap& Materials) : m_root(L"Materials"),
			m_bDirty(false), m_pMaterials(&Materials)
		{
		}

		const gui::Node& MaterialTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& MaterialTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void MaterialTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& Materials : *m_pMaterials)
				{
					gui::Node MaterialBlockNode(Materials.first);
					auto pCallback = new FolderTreeCallback(Materials.second.bLocked);
					MaterialBlockNode.pCallback = pCallback;

					const size_t size = Materials.second.pBlock->Size();
					if(size)
					{
						for(unsigned int i = 0; i < size; i++)
						{
							auto& Material = Materials.second.pBlock->GetPair(i);
							gui::Node component(Material.first);
							auto pMaterialCallback = new MaterialTreeCallback(*Material.second, Materials.second.bLocked);
							pMaterialCallback->SigDelete.Connect(this, &MaterialTreeModel::OnDeleteMaterial);
							pMaterialCallback->SigSelect.Connect(this, &MaterialTreeModel::OnSelectMaterial);
							component.pCallback = pMaterialCallback;
							MaterialBlockNode.vChilds.push_back(component);
						}

						m_root.vChilds.push_back(MaterialBlockNode);
					}
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void MaterialTreeModel::OnMaterialsChanged(void)
		{
			m_bDirty = true;
		}

		void MaterialTreeModel::OnDeleteMaterial(const gfx::IMaterial& Material)
		{

		}

		void MaterialTreeModel::OnSelectMaterial(const gfx::IMaterial& Material)
		{
			SigMaterialSelected(&Material);
		}

		//void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		//{
		//	SigGotoEntity(entity);
		//}

		//void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		//{
		//	SigEntitySelected(&entity);
		//}

		//void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		//{
		//	m_pEntities->RemoveEntity(entity);
		//	SigEntitySelected(nullptr);
		//	m_bDirty = true;
		//}

		//void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		//{
		//	if(m_pAttach->Execute(entity))
		//		m_bDirty = true;
		//}

		//void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		//{
		//	if(entity.GetName() != stName)
		//	{
		//		entity.SetName(stName);
		//		m_bDirty = true;
		//	}
		//}


	}
}

