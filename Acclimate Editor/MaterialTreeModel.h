#pragma once
#include "Gui\ITreeModel.h"
#include <map>
#include "MaterialBlockData.h"
#include "Gfx\Materials.h"

namespace acl
{
	namespace editor
	{

		class MaterialTreeModel :
			public gui::ITreeModel
		{
			typedef std::map<std::wstring, MaterialBlockData> MaterialMap;
		public:
			MaterialTreeModel(const MaterialMap& Materials);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnMaterialsChanged(void);

			core::Signal<const gfx::IMaterial*> SigMaterialSelected;

		private:

			void OnDeleteMaterial(const gfx::IMaterial& Material);
			void OnSelectMaterial(const gfx::IMaterial& Material);

			bool m_bDirty;
			const MaterialMap* m_pMaterials;

			gui::Node m_root;
			core::Signal<> SigUpdated;
		};

	}
}


