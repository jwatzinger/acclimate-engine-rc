#pragma once
#include "Gfx\Meshes.h"

namespace acl
{
	namespace editor
	{

		struct MeshBlockData
		{
			MeshBlockData(void) : pBlock(nullptr), bLocked(false)
			{
			}

			MeshBlockData(const gfx::Meshes::Block& block, bool bLocked) : pBlock(&block),
			bLocked(bLocked)
			{
			}

			const gfx::Meshes::Block* pBlock;
			bool bLocked;
		};

	}
}

