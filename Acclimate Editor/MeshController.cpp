#include "MeshController.h"
#include "NewMeshController.h"
#include "Gfx\IMeshLoader.h"
#include "Gui\MsgBox.h"
#include "Gui\Label.h"
#include "Gui\TreeView.h"
#include "Render\IRenderer.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MeshController::MeshController(gui::Module& module, gfx::Meshes& meshes, const gfx::IMeshLoader& loader, const gfx::Models& models, const gfx::Effects& effects, const render::IRenderer& renderer) :
			BaseController(module, L"../Editor/Menu/Asset/Meshes.axm"), m_meshes(meshes), m_pLoader(&loader), m_model(m_mMeshes)
		{
			// list
			m_pTree = GetWidgetByName<gui::TreeView>(L"Meshes");
			m_pTree->SetModel(&m_model);

			// name
			m_pName = GetWidgetByName<gui::Textbox<>>(L"Name");

			// delete button
			GetWidgetByName<gui::Widget>(L"Delete")->SigReleased.Connect(this, &MeshController::OnDelete);

			// attribute labels
			m_pFile = GetWidgetByName<gui::Label>(L"File");
			m_pSubsets = GetWidgetByName <gui::Label>(L"Subsets");

			// attribute group
			m_pAttributes = GetWidgetByName<gui::Widget>(L"Attributes");

			// new mesh button
			GetWidgetByName<gui::Widget>(L"NewMesh")->SigReleased.Connect(this, &MeshController::OnNewMesh);

			// image preview
			m_pPreview = &AddController<MeshPreviewController>(*GetWidgetByName<gui::Widget>(L"Preview"), meshes, *models[L"MeshPreview"], *effects[L"MeshPreview"], *renderer.GetStage(L"meshpreview"), MeshPreviewController::ZOOM | MeshPreviewController::FILLMODE);

			m_model.SigMeshSelected.Connect(this, &MeshController::OnPickMesh);

			OnRefresh();
		}

		MeshController::~MeshController(void)
		{
			m_pTree->SetModel(nullptr);
			m_meshes.Clear();
		}

		void MeshController::AddBlock(const std::wstring& stName, const gfx::Meshes::Block& meshes, bool bLocked)
		{
			m_mMeshes[stName] = MeshBlockData(meshes, bLocked);
		}

		void MeshController::Begin(void)
		{
			m_meshes.Begin();
		}

		void MeshController::End(void)
		{
			m_meshes.End();
		}

		void MeshController::Clear(void)
		{
			m_meshes.Clear();
			OnRefresh();
		}

		void MeshController::Update(void)
		{
			BaseController::Update();

			if(!m_pMainWidget->IsVisible())
				return;

			m_model.Update();

			m_pPreview->Render();
		}

		void MeshController::OnNewMesh(void)
		{
			NewMeshController controller(*m_pModule);

			gfx::MeshLoadInfo info;
			if(controller.Execute(info))
			{
				try
				{
					m_pLoader->Load(info.stName, info.stPath);
					//m_pList->SelectByName(info.stName);
				}
				catch(...)
				{
					gui::MsgBox& messageBox = AddWidget<gui::MsgBox>(L"Error", (L"Failed to load mesh from file " + info.stPath + L".").c_str());
					messageBox.Execute();
					RemoveWidget(messageBox);
				}
			}
		}

		void MeshController::OnDelete(void)
		{
			/*int id = m_pList->GetSelectedId();
			if(id > -1)
			{
				m_meshes.Delete(id);
				OnRefresh();
				SigMeshesChanged();
			}*/
		}

		void MeshController::OnPickMesh(const gfx::IMesh* pMesh)
		{
			bool bMeshExists = pMesh != nullptr;

			if(bMeshExists)
			{
				if(auto pInfo = pMesh->GetLoadInfo())
				{
					m_pName->SetText(pInfo->stName);
					m_pFile->SetString(L"File: " + pInfo->stPath);
					m_pPreview->SetMesh(pInfo->stName);
				}
				else
				{
					m_pName->SetText(L"");
					m_pFile->SetString(L"");
					m_pPreview->SetMesh(L"");
				}

				m_pSubsets->SetString(L"Subsets: " + conv::ToString(pMesh->GetNumSubsets()));
			}
			else
			{
				m_pName->SetText(L"");
				m_pFile->SetString(L"");
				m_pPreview->SetMesh(L"");
			}
			
			m_pAttributes->SetVisible(bMeshExists);
		}

		void MeshController::OnRefresh(void)
		{
			m_model.OnMeshesChanged();
		}

	}
}

