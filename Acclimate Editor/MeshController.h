#pragma once
#include "MeshPreviewController.h"
#include "MeshBlockData.h"
#include "MeshTreeModel.h"
#include "Gfx\Meshes.h"
#include "Gfx\Models.h"
#include "Gfx\Effects.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gfx
	{
		class IMeshLoader;
	}

	namespace gui
	{
		class Label;
		class TreeView;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class MeshController :
			public gui::BaseController
		{
			typedef std::map<std::wstring, MeshBlockData> MeshMap;
		public:

			MeshController(gui::Module& module, gfx::Meshes& meshes, const gfx::IMeshLoader& loader, const gfx::Models& models, const gfx::Effects& effects, const render::IRenderer& renderer);
			~MeshController(void);

			void AddBlock(const std::wstring& stName, const gfx::Meshes::Block& meshes, bool bLocked);

			void Begin(void);
			void End(void);
			void Clear(void);
			void Update(void) override;

			core::Signal<> SigMeshesChanged;

		private:

			void OnRefresh(void);
			void OnNewMesh(void);
			void OnDelete(void);
			void OnPickMesh(const gfx::IMesh* pMesh);

			MeshMap m_mMeshes;
			gfx::Meshes::Block m_meshes;
			const gfx::IMeshLoader* m_pLoader;

			gui::Textbox<>* m_pName;
			gui::Label* m_pFile, *m_pSubsets;
			gui::Widget* m_pAttributes;
			gui::TreeView* m_pTree;
			MeshTreeModel m_model;

			MeshPreviewController* m_pPreview;
		};

	}
}


