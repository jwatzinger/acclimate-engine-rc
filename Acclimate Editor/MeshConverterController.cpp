#include "MeshConverterController.h"
#include "MaterialExporter.h"
#include "Gui\ComboBox.h"
#include "Gui\FileDialog.h"
#include "Gui\BaseWindow.h"
#include "Gui\CheckBox.h"
#include "Gfx\MeshFileSaver.h"
#include "Gfx\MeshFileBinarySaver.h"
#include "Gfx\Animation.h"
#include "Gfx\AnimationFileSaver.h"
#include "File\File.h"
#include "Render\RendererInternalAccessor.h"

#include "MeshXConverter.h"
#include "MeshXFileConverter.h"

namespace acl
{
	namespace editor
	{

		MeshConverterController::MeshConverterController(gui::Module& module, render::IRenderer& render) : BaseController(module, L"../Editor/Menu/MeshImporter.axm")
		{
			// format selection box
			m_pBox = &AddWidget<MeshTypeBox>(0.1f, 0.05f, 0.8f, 22.0f, 5.0f);
			m_pBox->SetPositionModes(gui::PositionMode::REL, gui::PositionMode::REL, gui::PositionMode::REL, gui::PositionMode::ABS);
			
#ifdef ACL_API_DX9
			m_pBox->AddItem(L"X-mesh (requires dx9)", new MeshXConverter(render::getDevice(render)));
#endif
			m_pBox->AddItem(L"X-file", new MeshXFileConverter());

			// import file 
			auto pInput = GetWidgetByName<gui::Widget>(L"Input");
			pInput->SigReleased.Connect(this, &MeshConverterController::OnImport);

			// input textbox
			m_pInputBox = GetWidgetByName<gui::Textbox<std::wstring>>(L"Input");

			// main window
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Import");
			// save button
			GetWidgetByName<gui::Widget>(L"SaveButton")->SigReleased.Connect(this, &MeshConverterController::OnSave);
			// cancel button
			GetWidgetByName<gui::Widget>(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);

			//auto messageBox = gui::MsgBox(L"Error", L"ErrorMessage");
			//m_pModule->RegisterWidget(messageBox);
			//messageBox.Execute();
		}

		void MeshConverterController::Execute(void) const
		{
			if (m_pBox->GetContent() == nullptr)
				m_pBox->Select(0);

			m_pWindow->Execute();
		}

		void MeshConverterController::OnImport(void) const
		{
			gui::FileDialog dialog(gui::DialogType::OPEN);
			if (dialog.Execute())
			{
				m_pInputBox->SetText(dialog.GetFullPath());
			}
		}

		void MeshConverterController::OnSave(void) const
		{
			gui::FileDialog dialog(gui::DialogType::SAVE);
			if (dialog.Execute())
			{
				IMeshConverter::TextureVector vTextures;
				gfx::AnimationSet set;
				auto mesh = m_pBox->GetContent()->Convert(m_pInputBox->GetContent(), &vTextures, &set);
				MaterialExporter exporter;
				exporter.Export(file::FilePath(m_pInputBox->GetContent()), vTextures, L"Materials.axm");
				gfx::AnimationFileSaver animationSaver;
				animationSaver.Save(set, L"Animation.aca");
				
				if(mesh.IsValid())
				{
					if(GetWidgetByName<gui::CheckBox>(L"Binary")->IsChecked())
					{
						gfx::MeshFileBinarySaver saver;
						saver.Save(mesh, dialog.GetFullPath());
					}
					else
					{
						gfx::MeshFileSaver saver;
						saver.Save(mesh, dialog.GetFullPath());
					}
				}
			}
		}

	}
}