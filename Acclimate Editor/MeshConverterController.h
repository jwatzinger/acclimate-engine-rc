#pragma once
#include "Gui\BaseController.h"
#include "Gui\ComboBox.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class IMeshConverter;

		class MeshConverterController :
			public gui::BaseController
		{
			typedef gui::ComboBox<IMeshConverter*> MeshTypeBox;
		public:
			MeshConverterController(gui::Module& module, render::IRenderer& render);

			void Execute(void) const;

		private:

			void OnImport(void) const;
			void OnSave(void) const;

			MeshTypeBox* m_pBox;
			gui::Textbox<std::wstring>* m_pInputBox;
			gui::BaseWindow* m_pWindow;
		};

	}
}

