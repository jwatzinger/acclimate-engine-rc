#include "MeshPickController.h"
#include "MeshPreviewController.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		MeshPickController::MeshPickController(gui::Module& module, const gfx::Meshes& meshes, gfx::IModel& model, gfx::IEffect& effect, render::IStage& stage) : 
			BaseController(module, L"../Editor/Menu/Asset/MeshPick.axm"), m_pMeshes(&meshes), m_bConfirmed(false)
		{
			// window
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");
			m_pWindow->SigExecute.Connect(this, &MeshPickController::OnUpdate);

			// list
			m_pList = &AddWidget<MeshList>(0.0f, 0.0f, 0.3f, 0.9f, 0.02f);
			m_pList->SigItemPicked.Connect(this, &MeshPickController::OnPick);
			m_pList->SigConfirm.Connect(this, &MeshPickController::OnConfirm);

			// pick button
			GetWidgetByName<gui::Widget>(L"PickMesh")->SigReleased.Connect(this, &MeshPickController::OnConfirm);

			// preview
			m_pPreview = &AddController<MeshPreviewController>(*GetWidgetByName<gui::Widget>(L"Preview"), meshes, model, effect, stage, MeshPreviewController::ZOOM);

			OnRefresh();
		}

		void MeshPickController::OnRefresh(void)
		{
			m_pList->Clear();

			for(auto& mesh : m_pMeshes->Map())
			{
				m_pList->AddItem(mesh.first, mesh.second);
			}

			if(m_pList->GetSize())
				m_pList->SelectById(0);
		}

		bool MeshPickController::Execute(const gfx::IMesh** pMesh, std::wstring* pName)
		{
			return Execute(L"", pMesh, pName);
		}

		bool MeshPickController::Execute(const std::wstring& stDefault, const gfx::IMesh** pMesh, std::wstring* pName)
		{
			m_bConfirmed = false;
			m_pList->OnFocus(true);

			if(!stDefault.empty())
				m_pList->SelectByName(stDefault);

			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				if(pMesh)
					*pMesh = m_pList->GetContent();
				if(pName)
					*pName = m_pList->GetSelectedName();
			}
			return m_bConfirmed;
		}

		void MeshPickController::OnUpdate(void)
		{
			if(m_pMainWidget->IsVisible())
				m_pPreview->Render();
		}

		void MeshPickController::OnPick(const gfx::IMesh* pMesh)
		{
			m_pPreview->SetMesh(m_pList->GetSelectedName());
		}

		void MeshPickController::OnConfirm(void)
		{
			m_pWindow->OnClose();
			m_bConfirmed = true;
		}

	}
}

