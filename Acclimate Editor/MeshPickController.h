#pragma once
#include "Gfx\Meshes.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace gfx
	{
		class IModel;
		class IEffect;
	}

	namespace gui
	{
		class BaseWindow;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class MeshPreviewController;

		class MeshPickController :
			public gui::BaseController
		{
			typedef gui::List<const gfx::IMesh*> MeshList;
		public:
			MeshPickController(gui::Module& module, const gfx::Meshes& meshes, gfx::IModel& model, gfx::IEffect& effect, render::IStage& stage);

			bool Execute(const gfx::IMesh** pMesh, std::wstring* pName);
			bool Execute(const std::wstring& stDefault, const gfx::IMesh** pMesh, std::wstring* pName);

			void OnRefresh(void);

		private:

			void OnUpdate(void);
			void OnPick(const gfx::IMesh* pMesh);
			void OnConfirm(void);

			bool m_bConfirmed;

			MeshList* m_pList;
			gui::BaseWindow* m_pWindow;

			const gfx::Meshes* m_pMeshes;

			MeshPreviewController* m_pPreview;
		};

	}
}

