#include "MeshPreviewController.h"
#include "Gfx\IModel.h"
#include "Gfx\IMaterial.h"
#include "Gfx\IEffect.h"
#include "Gfx\ModelInstance.h"
#include "Gui\Window.h"
#include "Math\Matrix.h"
#include "Math\Utility.h"
#include "Render\IStage.h"

namespace acl
{
	namespace editor
	{

		MeshPreviewController::MeshPreviewController(gui::Module& module, gui::Widget& parent, const gfx::Meshes& meshes, gfx::IModel& model, gfx::IEffect& effect, render::IStage& stage, int controls) :
			BaseController(module, parent, L"../Editor/Menu/Asset/MeshPreview.axm"), m_pModel(&model), m_pStage(&stage), m_pEffect(&effect),
			m_camera(math::Vector2(512, 512), math::Vector3(5.0f, 0.0f, 0.0f), math::PI / 4.0f), m_meshRotationX(0.0f), m_meshRotationY(0.0f),
			m_pMeshes(&meshes), m_stMesh(L""), m_bDirty(true)
		{
			m_pInstance = &m_pModel->CreateInstance();

			SetupGui(controls);

			effect.SetRasterizerState(gfx::CullMode::NONE, gfx::FillMode::WIREFRAME, false);

			m_camera.SetLookAt(0.0f, 0.0f, 0.0f);
			m_camera.SetClipPlanes(0.5f, 5000.0f);
		}

		MeshPreviewController::MeshPreviewController(const MeshPreviewController& ctrl, gui::Widget& parent, int controls) : 
			BaseController(*ctrl.m_pModule, parent, L"../Editor/Menu/Asset/MeshPreview.axm"), m_pModel(ctrl.m_pModel), m_pStage(ctrl.m_pStage), 
			m_pEffect(ctrl.m_pEffect), m_camera(ctrl.m_camera), m_meshRotationX(ctrl.m_meshRotationX), m_meshRotationY(ctrl.m_meshRotationY),
			m_pMeshes(ctrl.m_pMeshes), m_stMesh(ctrl.m_stMesh), m_bDirty(true)
		{
			m_pInstance = &m_pModel->CreateInstance();

			SetupGui(controls);
		}

		const gfx::Camera& MeshPreviewController::GetCamera(void) const
		{
			return m_camera;
		}

		float MeshPreviewController::GetRotationX(void) const
		{
			return m_meshRotationX;
		}

		float MeshPreviewController::GetRotationY(void) const
		{
			return m_meshRotationY;
		}

		void MeshPreviewController::SetupGui(int controls)
		{
			/************************************
			* GUI
			************************************/
			m_pFill = GetWidgetByName<gui::Widget>(L"Fill");
			m_pWireframe = GetWidgetByName<gui::Widget>(L"Wireframe");

			// fillmode
			if(controls & FILLMODE)
			{
				m_pFill->SigReleased.Connect(this, &MeshPreviewController::OnSetFill);
				m_pWireframe->SigReleased.Connect(this, &MeshPreviewController::OnSetWireframe);
			}
			else
			{
				m_pFill->OnInvisible();
				m_pWireframe->OnInvisible();
			}

			// zoom
			gui::Widget* pZoom = GetWidgetByName<gui::Widget>(L"Zoom");
			if(controls & ZOOM)
				pZoom->SigReleased.Connect(this, &MeshPreviewController::OnZoom);
			else
				pZoom->OnInvisible();

			m_pImage = GetWidgetByName<gui::Widget>(L"Preview");
			m_pImage->SigWheelMoved.Connect(this, &MeshPreviewController::OnWheelMove);
			m_pImage->SigDrag.Connect(this, &MeshPreviewController::OnDrag);
		}

		void MeshPreviewController::SetMesh(const std::wstring& stName)
		{
			auto pMesh = m_pMeshes->Get(stName);
			if(pMesh != m_pModel->GetMesh())
			{
				m_bDirty = true;

				m_meshRotationX = 0.0f;
				m_meshRotationY = 0.0f;
				m_camera.SetPosition(5.0f, 0.0f, 0.0f);

				m_pModel->SetMesh(*pMesh);
				m_stMesh = stName;
			}
		}

		void MeshPreviewController::Render(void)
		{
			if (m_bDirty)
			{
				if (!m_pMainWidget->IsVisible())
					return;

				const math::Vector2 vSize(m_pImage->GetWidth(), m_pImage->GetHeight());
				if (vSize != m_vImageSize)
				{
					OnResize(vSize);
					m_camera.SetScreenSize(vSize);
					m_pImage->MarkDirtyRect();
					m_vImageSize = vSize;
				}

				math::Matrix mModel;
				mModel = math::MatYawPitchRoll(m_meshRotationX, 0.0f, m_meshRotationY);
				m_pInstance->SetVertexConstant(0, (float*)&mModel, 4);

				m_pStage->SetVertexConstant(0, (float*)&m_camera.GetViewProjectionMatrix(), 4);
				m_pInstance->Draw(*m_pStage, 0);

				m_pImage->MarkDirtyRect();
				m_bDirty = false;
			}
		}

		void MeshPreviewController::OnWheelMove(int step)
		{
			if(m_camera.GetDistance() <= -(float)step*0.01f)
				return;

			const math::Vector3& vDir(m_camera.GetDirection());
			const math::Vector3 vPos = m_camera.GetPosition() - vDir*(float)step*0.01f;

			m_camera.SetPosition(vPos);
			m_bDirty = true;
		}

		void MeshPreviewController::OnDrag(const math::Vector2& vMove)
		{
			m_meshRotationX += vMove.x*0.01f;
			m_meshRotationY += vMove.y*0.01f;
			
			m_bDirty = true;
		}

		void MeshPreviewController::OnResize(const math::Vector2& vSize)
		{
			m_pStage->SetViewport(math::Rect(0, 0, vSize.x, vSize.y));
		}

		void MeshPreviewController::OnSetFill(void)
		{
			m_pEffect->SetRasterizerState(gfx::CullMode::BACK, gfx::FillMode::SOLID, false);
			m_pFill->OnInvisible();
			m_pWireframe->OnVisible();
			
			m_bDirty = true;
		}

		void MeshPreviewController::OnSetWireframe(void)
		{
			m_pEffect->SetRasterizerState(gfx::CullMode::NONE, gfx::FillMode::WIREFRAME, false);
			m_pFill->OnVisible();
			m_pWireframe->OnInvisible();
			
			m_bDirty = true;
		}

		void MeshPreviewController::OnZoom(void)
		{
			gui::Window window(0.0f, 0.0f, 1.0f, 1.0f, m_stMesh);
			m_pModule->RegisterWidget(window);

			MeshPreviewController controller(*this, window, MeshPreviewController::FILLMODE);
			window.SigExecute.Connect(&controller, &MeshPreviewController::Render);
			window.Execute();

			m_camera = controller.GetCamera();
			m_meshRotationX = controller.GetRotationX();
			m_meshRotationY = controller.GetRotationY();

			m_vImageSize = math::Vector2(0, 0); // marks the image area as dirty so that the viewport gets resized correctly
			m_bDirty = true;
		}

	}
}

