#pragma once
#include "Gui\BaseController.h"
#include "Gfx\Camera.h"
#include "Gfx\Meshes.h"

namespace acl
{
	namespace gfx
	{
		class IModel;
		class IMesh;
		class IEffect;
		class ModelInstance;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class MeshPreviewController : 
			public gui::BaseController
		{
		public:

			enum Controls
			{
				NONE = 0, 
				ZOOM = 1, 
				FILLMODE = 2
			};

			MeshPreviewController(gui::Module& module, gui::Widget& parent, const gfx::Meshes& meshes, gfx::IModel& model, gfx::IEffect& effect, render::IStage& stage, int controls);

			void SetMesh(const std::wstring& stMesh);

			const gfx::Camera& GetCamera(void) const;
			float GetRotationX(void) const;
			float GetRotationY(void) const;

			void Render(void);

		private:

			MeshPreviewController(const MeshPreviewController& ctrl, gui::Widget& parent, int controls);

			void SetupGui(int controls);

			void OnWheelMove(int step);
			void OnDrag(const math::Vector2& vMove);
			void OnResize(const math::Vector2& vSize);
			void OnSetFill(void);
			void OnSetWireframe(void);
			void OnZoom(void);

			bool m_bDirty, m_bLastVisible;
			math::Vector2 m_vImageSize;
			float m_meshRotationX, m_meshRotationY;
			std::wstring m_stMesh;

			gui::Widget* m_pImage, *m_pFill, *m_pWireframe;
			
			gfx::Camera m_camera;
			gfx::IModel* m_pModel;
			gfx::ModelInstance* m_pInstance;
			gfx::IEffect* m_pEffect;
			render::IStage* m_pStage;
			const gfx::Meshes* m_pMeshes;
		};

	}
}


