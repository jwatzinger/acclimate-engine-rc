#include "MeshTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		MeshTreeCallback::MeshTreeCallback(const gfx::IMesh& Mesh, bool bLocked) : m_pMesh(&Mesh),
			m_bLocked(bLocked)
		{
		}

		const std::wstring* MeshTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"MeshLockedIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"MeshIcon";
				return &stIcon;
			}
		}

		bool MeshTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool MeshTreeCallback::IsDeletable(void) const
		{
			return !m_bLocked;
		}

		bool MeshTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void MeshTreeCallback::OnSelect(void)
		{
			SigSelect(*m_pMesh);
		}

		void MeshTreeCallback::OnDelete(void)
		{
			SigDelete(*m_pMesh);
		}

		gui::ContextMenu* MeshTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);
			return pEntityMenu;
		}

		void MeshTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void MeshTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(*m_pMesh, stName);
		}

	}
}