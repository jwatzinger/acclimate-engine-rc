#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class IMesh;
	}

	namespace editor
	{

		class MeshTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			MeshTreeCallback(const gfx::IMesh& Mesh, bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const gfx::IMesh&> SigSelect;
			core::Signal<const gfx::IMesh&> SigDelete;
			core::Signal<const gfx::IMesh&, const std::wstring&> SigRename;

		private:

			bool m_bLocked;
			const gfx::IMesh* m_pMesh;
		};

	}
}


