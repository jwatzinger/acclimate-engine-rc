#include "MeshTreeModel.h"
#include "FolderTreeCallback.h"
#include "MeshTreeCallback.h"

namespace acl
{
	namespace editor
	{

		MeshTreeModel::MeshTreeModel(const MeshMap& Meshs) : m_root(L"Meshs"),
			m_bDirty(false), m_pMeshs(&Meshs)
		{
		}

		const gui::Node& MeshTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& MeshTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void MeshTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& Meshs : *m_pMeshs)
				{
					gui::Node MeshBlockNode(Meshs.first);
					auto pCallback = new FolderTreeCallback(Meshs.second.bLocked);
					MeshBlockNode.pCallback = pCallback;

					const size_t size = Meshs.second.pBlock->Size();
					if(size)
					{
						for(unsigned int i = 0; i < size; i++)
						{
							auto& Mesh = Meshs.second.pBlock->GetPair(i);
							gui::Node component(Mesh.first);
							auto pMeshCallback = new MeshTreeCallback(*Mesh.second, Meshs.second.bLocked);
							pMeshCallback->SigDelete.Connect(this, &MeshTreeModel::OnDeleteMesh);
							pMeshCallback->SigSelect.Connect(this, &MeshTreeModel::OnSelectMesh);
							component.pCallback = pMeshCallback;
							MeshBlockNode.vChilds.push_back(component);
						}

						m_root.vChilds.push_back(MeshBlockNode);
					}
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void MeshTreeModel::OnMeshesChanged(void)
		{
			m_bDirty = true;
		}

		void MeshTreeModel::OnDeleteMesh(const gfx::IMesh& Mesh)
		{

		}

		void MeshTreeModel::OnSelectMesh(const gfx::IMesh& Mesh)
		{
			SigMeshSelected(&Mesh);
		}

		//void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		//{
		//	SigGotoEntity(entity);
		//}

		//void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		//{
		//	SigEntitySelected(&entity);
		//}

		//void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		//{
		//	m_pEntities->RemoveEntity(entity);
		//	SigEntitySelected(nullptr);
		//	m_bDirty = true;
		//}

		//void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		//{
		//	if(m_pAttach->Execute(entity))
		//		m_bDirty = true;
		//}

		//void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		//{
		//	if(entity.GetName() != stName)
		//	{
		//		entity.SetName(stName);
		//		m_bDirty = true;
		//	}
		//}


	}
}

