#pragma once
#include "Gui\ITreeModel.h"
#include <map>
#include "MeshBlockData.h"
#include "Gfx\Meshes.h"

namespace acl
{
	namespace editor
	{

		class MeshTreeModel :
			public gui::ITreeModel
		{
			typedef std::map<std::wstring, MeshBlockData> MeshMap;
		public:
			MeshTreeModel(const MeshMap& Meshs);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnMeshesChanged(void);

			core::Signal<const gfx::IMesh*> SigMeshSelected;

		private:

			void OnDeleteMesh(const gfx::IMesh& Mesh);
			void OnSelectMesh(const gfx::IMesh& Mesh);

			bool m_bDirty;
			const MeshMap* m_pMeshs;

			gui::Node m_root;
			core::Signal<> SigUpdated;
		};

	}
}


