#include "MeshXConverter.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include <unordered_map>
#include "XMesh.h"
#include "Gfx\MeshFile.h"
#include "Gfx\MeshSkeleton.h"
#include "Gfx\Animation.h"
#include "DX9\Device.h"
#include "System\Exception.h"
#include "System\Convert.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		union BreakApartFloat
		{
			float asFloat;
			char byte[sizeof(float)];
		};

		typedef std::unordered_map<std::string, unsigned int> IdMap;
		typedef std::vector<const LPD3DXFRAME> FrameVector;

		void traverseFrame(gfx::Bone& bone, gfx::Bone* pParent, const LPD3DXFRAME lpFrame, unsigned int& id, IdMap& mNameToId)
		{
			mNameToId[lpFrame->Name] = id;

			// TODO: proper conversion method
			bone.mRelative = *(math::Matrix*)&lpFrame->TransformationMatrix;

			if(lpFrame->pFrameFirstChild)
			{
				bone.m_vChildren.resize(bone.m_vChildren.size() + 1);
				auto child = bone.m_vChildren.rbegin();
				id++;
				traverseFrame(*child, &bone, lpFrame->pFrameFirstChild, id, mNameToId);
			}

			if(pParent && lpFrame->pFrameSibling)
			{
				pParent->m_vChildren.resize(pParent->m_vChildren.size() + 1);
				auto sibling = pParent->m_vChildren.rbegin();
				id++;
				traverseFrame(*sibling, pParent, lpFrame->pFrameSibling, id, mNameToId);
			}
		}

		void getMeshFrames(const LPD3DXFRAME lpFrame, FrameVector& vFrames)
		{
			if(lpFrame->pMeshContainer)
				vFrames.push_back(lpFrame);
			if(lpFrame->pFrameFirstChild)
				getMeshFrames(lpFrame->pFrameFirstChild, vFrames);
			if(lpFrame->pFrameSibling)
				getMeshFrames(lpFrame->pFrameSibling, vFrames);
		}

		MeshXConverter::MeshXConverter(const AclDevice& device) : m_device(device)
		{
		}

		gfx::MeshFile MeshXConverter::Convert(const std::wstring& stInName, TextureVector* pMaterials, gfx::AnimationSet* pSet) const
		{
			/****************************************
			* Load mesh from file to graphic memory
			****************************************/

			AllocMeshHierarchy allocHierachy;
			LoadUserData userData;
			LPD3DXFRAME lpFrame = nullptr;
			LPD3DXANIMATIONCONTROLLER lpController = nullptr;
			if(D3DXLoadMeshHierarchyFromX(stInName.c_str(), D3DXMESH_MANAGED, m_device.GetDevice(), &allocHierachy, &userData, &lpFrame, &lpController))
				throw resourceLoadException(stInName);

			DWORD numMaterials = 0;

			/****************************************
			* Skeleton
			****************************************/

			gfx::Bone root;
			IdMap mNameToId;
			unsigned int id = 0;

			FrameVector vMeshFrames;
			//{
			//	AllocMeshHierarchy allocHierachy;
			//	LoadUserData userData;
			//	LPD3DXFRAME lpFrame = nullptr;
			//	LPD3DXANIMATIONCONTROLLER lpController = nullptr;
			//	if(D3DXLoadMeshHierarchyFromX(L"C:\\Acclimate Engine\\Animations\\PlayerRun.X", D3DXMESH_MANAGED, m_device.GetDevice(), &allocHierachy, &userData, &lpFrame, &lpController))
			//		throw resourceLoadException(stInName);

			//	traverseFrame(root, nullptr, lpFrame, id, mNameToId);
			//}

			getMeshFrames(lpFrame, vMeshFrames);

			//traverseFrame(root, nullptr, lpFrame, id, mNameToId

			/****************************************
			* Animation
			****************************************/

			if(pSet && lpController)
			{
				LPD3DXANIMATIONSET lpSet = nullptr;
				const unsigned int numSets = lpController->GetNumAnimationSets();
				lpController->GetAnimationSet(0, &lpSet);
				auto pDynSet = static_cast<ID3DXKeyframedAnimationSet*>(lpSet);

				const unsigned int numAnimations = pDynSet->GetNumAnimations();
				gfx::Animation animation((unsigned int)lpSet->GetPeriod());
				for(unsigned int i = 0; i < numAnimations; i++)
				{
					const char* pName = nullptr;
					pDynSet->GetAnimationNameByIndex(i, &pName);

					unsigned int animationId = mNameToId[pName];

					gfx::AnimationBone bone(animationId);

					const unsigned int numTranslation = pDynSet->GetNumTranslationKeys(i);
					LPD3DXKEY_VECTOR3 vTranslationKeys = new _D3DXKEY_VECTOR3[numTranslation];
					pDynSet->GetTranslationKeys(i, vTranslationKeys);

					for(unsigned int j = 0; j < numTranslation; j++)
					{
						gfx::TranslationKey key((unsigned int)vTranslationKeys[j].Time, (const math::Vector3&)vTranslationKeys[j].Value);
						bone.AddTranslationKey(key);
					}

					const unsigned int numRotation = pDynSet->GetNumRotationKeys(i);
					LPD3DXKEY_QUATERNION vRotationKeys = new _D3DXKEY_QUATERNION[numRotation];
					pDynSet->GetRotationKeys(i, vRotationKeys);

					for(unsigned int j = 0; j < numRotation; j++)
					{
						auto& quat = vRotationKeys[j].Value;
						gfx::RotationKey key((unsigned int)vRotationKeys[j].Time, math::Quaternion(quat.w, quat.x, quat.y, quat.z));
						bone.AddRotationKey(key);
					}

					const unsigned int numScales = pDynSet->GetNumScaleKeys(i);
					LPD3DXKEY_VECTOR3 vScaleKeys = new _D3DXKEY_VECTOR3[numScales];
					pDynSet->GetScaleKeys(i, vScaleKeys);

					for(unsigned int j = 0; j < numScales; j++)
					{
						gfx::ScaleKey key((unsigned int)vScaleKeys[j].Time, (const math::Vector3&)vScaleKeys[j].Value);
						bone.AddScaleKey(key);
					}

					delete[] vTranslationKeys;
					delete[] vRotationKeys;
					delete[] vScaleKeys;

					animation.AddBone(bone);
				}

				pSet->AddAnimation(conv::ToW(pDynSet->GetName()), animation);
			}

			if(vMeshFrames.empty())
				return gfx::MeshFile();

			/**************************************
			* Skinning
			**************************************/

			gfx::MeshSkeleton* pSkeleton = nullptr;
			gfx::MeshSkeleton::OrderVector vOrder;
			gfx::MeshSkeleton::MatrixVector vInvBindPoses;

			unsigned int boneOffset = 0;
			std::vector<unsigned int> vBoneOffsets;

			for(auto lpCurrentFrame : vMeshFrames)
			{
				ACL_ASSERT(lpCurrentFrame->pMeshContainer);

				if(lpCurrentFrame->pMeshContainer->pSkinInfo)
				{
					vBoneOffsets.push_back(boneOffset);

					const unsigned int numBones = lpCurrentFrame->pMeshContainer->pSkinInfo->GetNumBones();
					boneOffset += numBones;

					for(unsigned int i = 0; i < numBones; i++)
					{
						const auto name = lpCurrentFrame->pMeshContainer->pSkinInfo->GetBoneName(i);

						const unsigned int order = mNameToId.at(name);
						vOrder.push_back(order);
						vInvBindPoses.push_back(*(const math::Matrix*)lpCurrentFrame->pMeshContainer->pSkinInfo->GetBoneOffsetMatrix(i));
					}
				}

				/****************************************
				* Materials
				****************************************/

				if(pMaterials)
				{
					LPD3DXMATERIAL pMaterialBuffer = lpCurrentFrame->pMeshContainer->pMaterials;
					const DWORD numMaterials = lpCurrentFrame->pMeshContainer->NumMaterials;

					for(unsigned int i = 0; i < numMaterials; i++)
					{
						if(pMaterialBuffer->pTextureFilename)
							pMaterials->push_back(conv::ToW(pMaterialBuffer->pTextureFilename));
						else
							pMaterials->push_back(L"");
						pMaterialBuffer++;
					}
				}
			}

			pSkeleton = new gfx::MeshSkeleton(root, vOrder, vInvBindPoses);

			/****************************************
			* Mesh preparations
			****************************************/

			bool hasWeights = false, hasIndices = false;
			unsigned int numVertices = 0;
			unsigned int numIndices = 0;

			for(auto lpCurrentFrame : vMeshFrames)
			{
				LPD3DXMESH lpMesh = lpCurrentFrame->pMeshContainer->MeshData.pMesh;

				D3DVERTEXELEMENT9 Elements[] =
				{
					{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
					{ 0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
					{ 0, 20, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
					{ 0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT, 0 },
					{ 0, 44, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL, 0 },
				};

				std::vector<D3DVERTEXELEMENT9> vElements(Elements, Elements + 5);

				D3DVERTEXELEMENT9 declaration[65];
				lpMesh->GetDeclaration(declaration);
				for(unsigned int i = 0; i < 65; i++)
				{
					if(declaration[i].Type == D3DDECLTYPE_UNUSED)
						break;

					if(declaration[i].Usage == D3DDECLUSAGE_BLENDWEIGHT)
						hasWeights = true;
					else if(declaration[i].Usage == D3DDECLUSAGE_BLENDINDICES)
						hasIndices = true;

					if(hasWeights && hasIndices)
					{
						vElements.emplace_back(D3DVERTEXELEMENT9{ 0, 56, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 });
						vElements.emplace_back(D3DVERTEXELEMENT9{ 0, 72, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 });
						break;
					}
				}

				vElements.push_back(D3DDECL_END());

				LPD3DXMESH pMeshTemp = nullptr;
				if(FAILED(lpMesh->CloneMesh(lpMesh->GetOptions(), &vElements[0], m_device.GetDevice(), &pMeshTemp)))
				{
					throw resourceLoadException(stInName);
				}

				lpMesh->Release();
				lpMesh = pMeshTemp;

				LPD3DXBUFFER pBuffer = nullptr;

				// Anzahl der Faces (Dreiecke) ermitteln
				const unsigned int numFaces = lpMesh->GetNumFaces();

				// Adjazenz Informationen generieren
				DWORD *pAdjacency1 = new DWORD[numFaces * 3];

				if(FAILED(lpMesh->GenerateAdjacency(0.0f, pAdjacency1)))
				{
					delete[] pAdjacency1;
					throw resourceLoadException(stInName);
				}

				DWORD *pAdjacency2 = new DWORD[numFaces * 3];

				if(FAILED(lpMesh->OptimizeInplace(D3DXMESHOPT_ATTRSORT | D3DXMESHOPT_VERTEXCACHE, pAdjacency1, pAdjacency2, nullptr, nullptr)))
				{
					delete[] pAdjacency1;
					delete[] pAdjacency2;
					throw resourceLoadException(stInName);
				}

				delete[] pAdjacency1;

				if(FAILED(D3DXComputeNormals(lpMesh, pAdjacency2))) //pAdjacency3
				{
					delete[] pAdjacency2;
					throw resourceLoadException(stInName);
				}

				if(FAILED(D3DXComputeTangentFrameEx(lpMesh, D3DDECLUSAGE_TEXCOORD, 0, D3DDECLUSAGE_TANGENT, 0, D3DDECLUSAGE_BINORMAL, 0, D3DX_DEFAULT, 0, D3DXTANGENT_GENERATE_IN_PLACE, pAdjacency2, -1.01f, -0.01f, -1.01f, nullptr, nullptr))) //pAdjacency3
				{
					delete[] pAdjacency2;
					throw resourceLoadException(stInName);
				}

				// temporaere Objekte freigeben
				delete[] pAdjacency2;

				numIndices += numFaces * 3;
				numVertices += lpMesh->GetNumVertices();

				lpCurrentFrame->pMeshContainer->MeshData.pMesh = lpMesh;
			}

			gfx::MeshOptions options;
			size_t vertexSize;

			if(hasWeights && hasIndices)
			{
				options = (gfx::MeshOptions)(gfx::POS | gfx::TEX | gfx::NRM | gfx::TAN | gfx::BIN | gfx::WEIGHTS | gfx::INDICES);
				vertexSize = 19;
			}
			else
			{
				options = (gfx::MeshOptions)(gfx::POS | gfx::TEX | gfx::NRM | gfx::TAN | gfx::BIN);
				vertexSize = 14;
			}

			/****************************************
			* Fill into mesh file class
			****************************************/

			// vertices
			float* pData = nullptr, *pDataCpy = new float[numVertices*vertexSize];
			unsigned int *pIndexDataCpy = new unsigned int[numIndices];
			unsigned int vertexOffset = 0, indexOffset = 0;

			unsigned int meshId = 0;
			for(auto lpCurrentFrame : vMeshFrames)
			{
				LPD3DXMESH lpMesh = lpCurrentFrame->pMeshContainer->MeshData.pMesh;

				LPDIRECT3DVERTEXBUFFER9 lpVertexBuffer;
				lpMesh->GetVertexBuffer(&lpVertexBuffer);

				const auto numCurrentVertices = lpMesh->GetNumVertices();

				lpVertexBuffer->Lock(0, 0, (void**)&pData, D3DLOCK_READONLY);
				memcpy(pDataCpy + vertexOffset * vertexSize, pData, numCurrentVertices * vertexSize* sizeof(float));
				lpVertexBuffer->Unlock();
				lpVertexBuffer->Release();

				// modify bone values
				if(hasIndices && hasWeights)
				{
					const auto numBones = vBoneOffsets[meshId];
					auto pCurrentData = pDataCpy + vertexOffset * vertexSize;

					for(unsigned int i = 0; i < numCurrentVertices; i++)
					{
						pCurrentData += 18;

						BreakApartFloat indices;
						indices.asFloat = *pCurrentData;

						for(unsigned int j = 0; j < 4; j++)
						{
							indices.byte[j] += numBones;
						}

						*pCurrentData = indices.asFloat;

						pCurrentData++;
					}
				}

				// indicies / faces
				const DWORD meshOptions = lpMesh->GetOptions();

				LPDIRECT3DINDEXBUFFER9 lpIndexBuffer;
				lpMesh->GetIndexBuffer(&lpIndexBuffer);

				const auto numCurrentIndices = lpMesh->GetNumFaces() * 3;

				if(meshOptions & D3DXMESH_32BIT)
				{
					DWORD* pIndexData = nullptr;
					lpIndexBuffer->Lock(0, 0, (void**)&pIndexData, D3DLOCK_READONLY);
					for(unsigned int i = 0; i < numCurrentIndices; i++)
						pIndexDataCpy[i + indexOffset] = pIndexData[i] + vertexOffset;
				}
				else
				{
					WORD* pIndexData = nullptr;
					lpIndexBuffer->Lock(0, 0, (void**)&pIndexData, D3DLOCK_READONLY);
					for(unsigned int i = 0; i < numCurrentIndices; i++)
						pIndexDataCpy[i + indexOffset] = pIndexData[i] + vertexOffset;
				}

				lpIndexBuffer->Unlock();
				lpIndexBuffer->Release();

				vertexOffset += numCurrentVertices;
				indexOffset += numCurrentIndices;

				meshId++;
			}

			// subsets


			if(vMeshFrames.size() == 1)
			{
				auto lpMesh = vMeshFrames[0]->pMeshContainer->MeshData.pMesh;

				DWORD numAttributes = 0;
				lpMesh->GetAttributeTable(nullptr, &numAttributes);

				if(numAttributes <= 1)
				{
					vMeshFrames[0]->pMeshContainer->MeshData.pMesh->Release();

					gfx::MeshFile file(options, vertexSize, numVertices, pDataCpy, 3, numIndices / 3, pIndexDataCpy, pSkeleton);
					return file;
				}
				else
				{
					D3DXATTRIBUTERANGE* pAttributes = new D3DXATTRIBUTERANGE[numAttributes];
					lpMesh->GetAttributeTable(pAttributes, &numAttributes);

					unsigned int* pSubsetData = new unsigned int[numAttributes];
					for(unsigned int i = 0; i < numAttributes; i++)
					{
						pSubsetData[i] = pAttributes[i].FaceCount;
					}

					delete[] pAttributes;

					lpMesh->Release();
					gfx::MeshFile file(options, vertexSize, numVertices, pDataCpy, 3, numIndices / 3, pIndexDataCpy, numAttributes, pSubsetData, pSkeleton);
					return file;
				}
			}
			else
			{
				unsigned int numTotalAttributes = 0;
				for(auto pFrame : vMeshFrames)
				{
					auto lpMesh = pFrame->pMeshContainer->MeshData.pMesh;
					DWORD numAttributes = 0;
					lpMesh->GetAttributeTable(nullptr, &numAttributes);
					numTotalAttributes += numAttributes;
				}

				unsigned int* pSubsetData = new unsigned int[numTotalAttributes];
				unsigned int offset = 0;
				for(auto pFrame : vMeshFrames)
				{
					auto lpMesh = pFrame->pMeshContainer->MeshData.pMesh;
					DWORD numAttributes = 0;
					lpMesh->GetAttributeTable(nullptr, &numAttributes);
					D3DXATTRIBUTERANGE* pAttributes = new D3DXATTRIBUTERANGE[numAttributes];
					lpMesh->GetAttributeTable(pAttributes, &numAttributes);

					for(unsigned int i = 0; i < numAttributes; i++)
					{
						pSubsetData[i + offset] = pAttributes[i].FaceCount;
					}

					delete[] pAttributes;

					lpMesh->Release();

					offset += numAttributes;
				}
				gfx::MeshFile file(options, vertexSize, numVertices, pDataCpy, 3, numIndices / 3, pIndexDataCpy, numTotalAttributes, pSubsetData, pSkeleton);
				return file;
			}

			D3DXFrameDestroy(lpFrame, &allocHierachy);
		}

	}
}

#endif