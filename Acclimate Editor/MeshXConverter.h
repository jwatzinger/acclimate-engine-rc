#pragma once
#include "IMeshConverter.h"
#include "ApiDef.h"

#ifdef ACL_API_DX9

#include <string>

namespace acl
{
	namespace editor
	{

		class MeshXConverter :
			public IMeshConverter
		{
		public:
			MeshXConverter(const AclDevice& device);

			gfx::MeshFile Convert(const std::wstring& stInName, TextureVector* pMaterials, gfx::AnimationSet* pSet) const;

		private:

			const AclDevice& m_device;
		};

	}
}

#endif