#include "MeshXFileConverter.h"
#include <fstream>
#include <d3d9.h>
#include "DXFile.h"
#include "File\File.h"
#include "System\Convert.h"

#pragma comment(lib, "D3dxof.lib")

namespace acl
{
	namespace editor
	{

		gfx::MeshFile MeshXFileConverter::Convert(const std::wstring& stIn, TextureVector* pMaterials, gfx::AnimationSet* pSet) const
		{
			const std::string file = file::LoadFileA(stIn);

			LPDIRECTXFILE lpFile = nullptr;
			DirectXFileCreate(&lpFile);

			lpFile->RegisterTemplates((void*)file.c_str(), file.size());

			LPDIRECTXFILEENUMOBJECT lpEnum = nullptr; 
			lpFile->CreateEnumObject((void*)conv::ToA(stIn).c_str(), DXFILELOAD_FROMFILE, &lpEnum);

			LPDIRECTXFILEDATA lpData = nullptr;

			while(true)
			{
				HRESULT hr;
				if(FAILED(hr = lpEnum->GetNextDataObject(&lpData)))
				{
				}

				const GUID* pGuid;
				lpData->GetType(&pGuid);
				DWORD size;
				void* pData;
				lpData->GetData(nullptr, &size, &pData);

				if(!lpData)
					break;
			}
			

			gfx::MeshFile* pMesh = nullptr;
			return *pMesh;
		}

	}
}
