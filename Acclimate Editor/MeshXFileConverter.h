#pragma once
#include "IMeshConverter.h"

namespace acl
{
	namespace editor
	{

		class MeshXFileConverter :
			public IMeshConverter
		{
		public:

			gfx::MeshFile Convert(const std::wstring& stIn, TextureVector* pMaterials, gfx::AnimationSet* pSet) const override;
		};

	}
}


