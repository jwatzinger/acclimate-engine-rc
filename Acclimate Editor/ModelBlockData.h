#pragma once
#include "Gfx\Models.h"

namespace acl
{
	namespace editor
	{

		struct ModelBlockData
		{
			ModelBlockData(void) : pBlock(nullptr), bLocked(false)
			{
			}

			ModelBlockData(const gfx::Models::Block& block, bool bLocked) : pBlock(&block),
			bLocked(bLocked)
			{
			}

			const gfx::Models::Block* pBlock;
			bool bLocked;
		};

	}
}

