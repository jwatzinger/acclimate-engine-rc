#include "ModelController.h"
#include "MeshPickController.h"
#include "MaterialPickController.h"
#include "NewModelController.h"
#include "ModelPreviewController.h"
#include "Gfx\IModelLoader.h"
#include "Gui\TreeView.h"
#include "Render\IRenderer.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		ModelController::ModelController(gui::Module& module, gfx::Models& models, const gfx::IModelLoader& loader, const gfx::Meshes& meshes, const gfx::Materials& materials, const gfx::Effects& effects, const render::IRenderer& renderer) :
			BaseController(module, L"../Editor/Menu/Asset/Models.axm"), m_models(models), m_pLoader(&loader), m_pMeshes(&meshes), m_pActiveModels(nullptr),
			m_model(m_mModels)
		{			
			// list
			m_pTree = GetWidgetByName<gui::TreeView>(L"Models");
			m_pTree->SetModel(&m_model);

			// attribute area
			m_pAttributes = GetWidgetByName<gui::Widget>(L"Attributes");

			// name box
			m_pName = GetWidgetByName<gui::Textbox<>>(L"Name");
			// mesh box
			m_pMesh = GetWidgetByName<gui::Textbox<>>(L"Mesh");
			m_pMesh->SigReleased.Connect(this, &ModelController::OnChangeMesh);
			// material box
			m_pMaterial = GetWidgetByName<gui::Textbox<>>(L"Material");
			m_pMaterial->SigReleased.Connect(this, &ModelController::OnChangeMaterial);

			// new button
			GetWidgetByName<gui::Widget>(L"NewModel")->SigReleased.Connect(this, &ModelController::OnNewModel);
			// delete button
			GetWidgetByName<gui::Widget>(L"Delete")->SigReleased.Connect(this, &ModelController::OnDeleteSelected);

			// pick mesh
			m_pPick = &AddController<MeshPickController>(meshes, *models[L"MeshPreview"], *effects[L"MeshPreview"], *renderer.GetStage(L"meshpreview"));
			// pick material
			m_pPickMaterial = &AddController<MaterialPickController>(materials);

			// preview
			m_pPreview = &AddController<ModelPreviewController>(*GetWidgetByName<gui::Widget>(L"Preview"), models, *renderer.GetStage(L"modelpreview"), ModelPreviewController::ZOOM);

			m_model.SigModelSelected.Connect(this, &ModelController::OnPickModel);

			OnRefresh();
		}

		ModelController::~ModelController(void)
		{
			m_pTree->SetModel(nullptr);
			m_models.Clear();
		}
		  
		void ModelController::AddBlock(const std::wstring& stName, const gfx::Models::Block& models, bool bLocked)
		{
			m_mModels[stName] = ModelBlockData(models, bLocked);
		}

		void ModelController::Begin(void)
		{
			m_models.Begin();
		}

		void ModelController::End(void)
		{
			m_models.End();
			OnRefresh();
		}

		void ModelController::Clear(void)
		{
			m_models.Clear();
			OnRefresh();
		}

		void ModelController::Update(void)
		{
			BaseController::Update();

			if(m_pMainWidget->IsVisible())
				m_pPreview->Render();

			m_model.Update();
		}

		void ModelController::OnMeshesChanged(void)
		{
			m_pPick->OnRefresh();
		}

		void ModelController::OnMaterialsChanged(void)
		{
			m_pPickMaterial->OnRefresh();
		}

		void ModelController::OnRefresh(void)
		{
			m_model.OnModelsChanged();
		}

		void ModelController::OnNewModel(void)
		{
			NewModelController controller(*m_pModule, *m_pMeshes, *m_pPick);

			gfx::ModelLoadInfo info;
			if(controller.Execute(info))
			{
				gfx::IModelLoader::PassVector vPasses;
				m_pLoader->Load(info.stName, info.stMesh, vPasses);
				//m_pList->SelectByName(info.stName);
			}
		}

		void ModelController::OnDeleteSelected(void)
		{
			//size_t id = m_pList->GetSelectedId();
			//if(id != -1)
			//{
			//	m_models.Delete(id);
			//	OnRefresh();
			//}
		}

		void ModelController::OnPickModel(const gfx::IModel* pModel)
		{
			bool bModelExists = pModel != nullptr;

			if(bModelExists)
			{
				auto& info = *pModel->GetLoadInfo();
				m_pName->SetText(info.stName);
				m_pPreview->SetModel(info.stName);
				m_pMesh->SetText(info.stMesh);

				if(info.mMaterials.empty())
					m_pMaterial->SetText(L"");
				else
					m_pMaterial->SetText(info.mMaterials.begin()->second);
			}
			else
				m_pPreview->SetModel(L"");

			m_pAttributes->SetVisible(bModelExists);
		}

		void ModelController::OnChangeMesh(void)
		{
			//std::wstring stMesh;
			//if(m_pPick->Execute(m_pMesh->GetContent(), nullptr, &stMesh))
			//{
			//	auto& stOldMesh = m_pList->GetContent()->GetLoadInfo()->stMesh;
			//	if(stOldMesh != stMesh)
			//	{
			//		// TODO: reimplement
			//		//stOldMesh = stMesh;
			//		m_pList->GetContent()->SetMesh(*m_pMeshes->Get(stMesh));
			//		m_pMesh->SetText(stMesh);
			//	}
			//}	
		}

		void ModelController::OnChangeMaterial(void)
		{
			//gfx::IMaterial* pMaterial = nullptr;
			//std::wstring stMaterial;
			//if(m_pPickMaterial->Execute(m_pMaterial->GetContent(), &pMaterial, &stMaterial))
			//{
			//	auto& info = *m_pList->GetContent()->GetLoadInfo();
			//	// TODO: reimplement
			//	//info.mMaterials[0] = stMaterial;
			//	m_pMaterial->SetText(stMaterial);
			//	m_pList->GetContent()->SetMaterial(0, pMaterial);
			//}
		}

	}
}

