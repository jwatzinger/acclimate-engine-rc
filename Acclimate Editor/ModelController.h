#pragma once
#include "ModelBlockData.h"
#include "ModelTreeModel.h"
#include "Gfx\Models.h"
#include "Gfx\Meshes.h"
#include "Gfx\Effects.h"
#include "Gfx\Materials.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gfx
	{
		class IModelLoader;
	}

	namespace gui
	{
		class TreeView;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class MeshPickController;
		class MaterialPickController;
		class ModelPreviewController;

		class ModelController:
			public gui::BaseController
		{
			typedef std::map<std::wstring, ModelBlockData> ModelMap;
		public:
			ModelController(gui::Module& module, gfx::Models& models, const gfx::IModelLoader& loader, const gfx::Meshes& meshes, const gfx::Materials& materials, const gfx::Effects& effects, const render::IRenderer& renderer);
			~ModelController(void);

			void AddBlock(const std::wstring& stName, const gfx::Models::Block& models, bool bLocked);

			void Begin(void);
			void End(void);
			void Clear(void);
			void Update(void) override;

			void OnMeshesChanged(void);
			void OnMaterialsChanged(void);

		private:

			void OnRefresh(void);
			void OnNewModel(void);
			void OnDeleteSelected(void);
			void OnPickModel(const gfx::IModel* pModel);
			void OnChangeMesh(void);
			void OnChangeMaterial(void);

			ModelMap m_mModels;
			gfx::Models::Block m_models;
			const gfx::Models::Block* m_pActiveModels;
			const gfx::IModelLoader* m_pLoader;
			const gfx::Meshes* m_pMeshes;

			gui::TreeView* m_pTree;
			gui::Widget* m_pAttributes;
			gui::Textbox<>* m_pName, *m_pMesh, *m_pMaterial;
			ModelTreeModel m_model;

			MeshPickController* m_pPick;
			MaterialPickController* m_pPickMaterial;
			ModelPreviewController* m_pPreview;
		};

	}
}

