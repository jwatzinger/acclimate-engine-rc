#include "ModelPickController.h"
#include "ModelPreviewController.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		ModelPickController::ModelPickController(gui::Module& module, const gfx::Models& models, render::IStage& stage) : 
			BaseController(module, L"../Editor/Menu/Asset/ModelPick.axm"), m_pModels(&models)
		{
			// window
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");
			m_pWindow->SigExecute.Connect(this, &ModelPickController::OnUpdate);

			// list
			m_pList = &AddWidget<ModelList>(0.0f, 0.0f, 0.3f, 0.9f, 0.02f);
			m_pList->SigItemPicked.Connect(this, &ModelPickController::OnPick);
			m_pList->SigConfirm.Connect(this, &ModelPickController::OnConfirm);

			// pick button
			GetWidgetByName<gui::Widget>(L"PickModel")->SigReleased.Connect(this, &ModelPickController::OnConfirm);

			// preview
			m_pPreview = &AddController<ModelPreviewController>(*GetWidgetByName<gui::Widget>(L"Preview"), models, stage, ModelPreviewController::ZOOM);

			OnRefresh();
		}	

		void ModelPickController::OnRefresh(void)
		{
			m_pList->Clear();

			for(auto& models : m_pModels->Map())
			{
				m_pList->AddItem(models.first, models.second);
			}

			if(m_pList->GetSize())
				m_pList->SelectById(0);
		}

		bool ModelPickController::Execute(const gfx::IModel** pModel, std::wstring* pName)
		{
			return Execute(L"", pModel, pName);
		}

		bool ModelPickController::Execute(const std::wstring& stDefault, const gfx::IModel** pModel, std::wstring* pName)
		{
			m_bConfirmed = false;
			m_pList->OnFocus(true);

			if(!stDefault.empty())
				m_pList->SelectByName(stDefault);

			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				if(pModel)
					*pModel = m_pList->GetContent();
				if(pName)
					*pName = m_pList->GetSelectedName();
			}
			return m_bConfirmed;
		}

		void ModelPickController::OnUpdate(void)
		{
			if(m_pMainWidget->IsVisible())
				m_pPreview->Render();
		}

		void ModelPickController::OnPick(const gfx::IModel* pModel)
		{
			m_pPreview->SetModel(m_pList->GetSelectedName());
		}

		void ModelPickController::OnConfirm(void)
		{
			m_pWindow->OnClose();
			m_bConfirmed = true;
		}

	}
}

