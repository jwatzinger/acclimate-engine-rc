#pragma once
#include "Gfx\Models.h"
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{
		
		class ModelPreviewController;

		class ModelPickController :
			public gui::BaseController
		{
			typedef gui::List<const gfx::IModel*> ModelList;
		public:
			ModelPickController(gui::Module& module, const gfx::Models& models, render::IStage& stage);

			bool Execute(const gfx::IModel** pModel, std::wstring* pName);
			bool Execute(const std::wstring& stDefault, const gfx::IModel** pModel, std::wstring* pName);

			void OnRefresh(void);

		private:

			void OnUpdate(void);
			void OnPick(const gfx::IModel* pModel);
			void OnConfirm(void);

			bool m_bConfirmed;

			ModelList* m_pList;
			gui::BaseWindow* m_pWindow;

			const gfx::Models* m_pModels;

			ModelPreviewController* m_pPreview;
		};

	}
}


