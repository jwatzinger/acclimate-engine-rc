#include "ModelPreviewController.h"
#include "Gfx\IModel.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\IMaterial.h"
#include "Gfx\IEffect.h"
#include "Gui\Window.h"
#include "Math\Matrix.h"
#include "Math\Utility.h"
#include "Render\IStage.h"

namespace acl
{
	namespace editor
	{

		ModelPreviewController::ModelPreviewController(gui::Module& module, gui::Widget& parent, const gfx::Models& models, render::IStage& stage, int controls) :
			BaseController(module, parent, L"../Editor/Menu/Asset/ModelPreview.axm"), m_pModel(nullptr), m_pStage(&stage), m_pModels(&models),
			m_camera(math::Vector2(512, 512), math::Vector3(5.0f, 0.0f, 0.0f), math::PI / 4.0f), m_meshRotationX(0.0f), m_meshRotationY(0.0f),
			m_stModel(L""), m_bDirty(true)
		{
			SetupGui(controls);

			// setup camera
			m_camera.SetLookAt(0.0f, 0.0f, 0.0f);
			m_camera.SetClipPlanes(0.5f, 5000.0f);
		}

		ModelPreviewController::ModelPreviewController(const ModelPreviewController& ctrl, gui::Widget& parent, int controls):
			BaseController(*ctrl.m_pModule, parent, L"../Editor/Menu/Asset/ModelPreview.axm"), m_pModel(ctrl.m_pModel), m_pStage(ctrl.m_pStage),
			m_pModels(ctrl.m_pModels), m_camera(ctrl.m_camera), m_meshRotationX(ctrl.m_meshRotationX), m_meshRotationY(ctrl.m_meshRotationY),
			m_stModel(ctrl.m_stModel), m_bDirty(true)
		{
			SetupGui(controls);
		}

		void ModelPreviewController::SetupGui(int controls)
		{
			// zoom
			gui::Widget* pZoom = GetWidgetByName<gui::Widget>(L"Zoom");
			if(controls & ZOOM)
				pZoom->SigReleased.Connect(this, &ModelPreviewController::OnZoom);
			else
				pZoom->OnInvisible();

			m_pImage = GetWidgetByName<gui::Widget>(L"Preview");
			m_pImage->SigWheelMoved.Connect(this, &ModelPreviewController::OnWheelMove);
			m_pImage->SigDrag.Connect(this, &ModelPreviewController::OnDrag);
			m_pImage->MarkDirtyRect();
		}

		void ModelPreviewController::SetModel(const std::wstring& stName)
		{
			if(m_stModel != stName)
			{
				gfx::IModel* pModel = nullptr;
				if(!stName.empty())
					pModel = m_pModels->Get(stName);

				m_bDirty = true;

				m_meshRotationX = 0.0f;
				m_meshRotationY = 0.0f;
				m_camera.SetPosition(5.0f, 0.0f, 0.0f);

				if(pModel)
					m_pModel = &pModel->CreateInstance();

				m_stModel = stName;
			}
		}

		const gfx::Camera& ModelPreviewController::GetCamera(void) const
		{
			return m_camera;
		}

		float ModelPreviewController::GetRotationX(void) const
		{
			return m_meshRotationX;
		}

		float ModelPreviewController::GetRotationY(void) const
		{
			return m_meshRotationY;
		}

		void ModelPreviewController::Render(void)
		{
			if(m_bDirty)
			{
				if(!m_pMainWidget->IsVisible() || !m_pModel)
					return;

				const math::Vector2 vSize(m_pImage->GetWidth(), m_pImage->GetHeight());
				if(vSize != m_vImageSize)
				{
					OnResize(vSize);
					m_camera.SetScreenSize(vSize);
					m_pImage->MarkDirtyRect();
					m_vImageSize = vSize;
				}

				math::Matrix mModel;
				mModel = math::MatYawPitchRoll(m_meshRotationX, 0.0f, m_meshRotationY);
				m_pModel->SetVertexConstant(0, (float*)&mModel, 4);

				m_pStage->SetVertexConstant(0, (float*)&m_camera.GetViewProjectionMatrix(), 4);
				m_pModel->Draw(*m_pStage, 0);

				m_pImage->MarkDirtyRect();
				m_bDirty = false;
			}
		}

		void ModelPreviewController::OnWheelMove(int step)
		{
			if(m_camera.GetDistance() <= -(float)step*0.01f)
				return;

			const math::Vector3& vDir(m_camera.GetDirection());
			const math::Vector3 vPos = m_camera.GetPosition() - vDir*(float)step*0.01f;

			m_camera.SetPosition(vPos);
			m_bDirty = true;
		}

		void ModelPreviewController::OnDrag(const math::Vector2& vMove)
		{
			m_meshRotationX += vMove.x*0.01f;
			m_meshRotationY += vMove.y*0.01f;
			m_bDirty = true;
		}

		void ModelPreviewController::OnResize(const math::Vector2& vSize)
		{
			m_pStage->SetViewport(math::Rect(0, 0, vSize.x, vSize.y));
		}

		void ModelPreviewController::OnZoom(void)
		{
			gui::Window window(0.0f, 0.0f, 1.0f, 1.0f, m_stModel);
			m_pModule->RegisterWidget(window);

			ModelPreviewController controller(*this, window, ModelPreviewController::NONE);
			controller.SetModel(m_stModel);

			window.SigExecute.Connect(&controller, &ModelPreviewController::Render);
			window.Execute();

			m_camera = controller.GetCamera();
			m_meshRotationX = controller.GetRotationX();
			m_meshRotationY = controller.GetRotationY();

			m_vImageSize = math::Vector2(0, 0); // marks the image area as dirty so that the viewport gets resized correctly
			m_bDirty = true;
		}

	}
}

