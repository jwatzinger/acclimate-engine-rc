#pragma once
#include "Gui\BaseController.h"
#include "Gfx\Camera.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
	}

	namespace render
	{
		class IStage;
	}

	namespace editor
	{

		class ModelPreviewController : 
			public gui::BaseController
		{
		public:

			enum Controls
			{
				NONE = 0, 
				ZOOM = 1
			};

			ModelPreviewController(gui::Module& module, gui::Widget& parent, const gfx::Models& models, render::IStage& stage, int controls);

			void SetModel(const std::wstring& stModel);

			const gfx::Camera& GetCamera(void) const;
			float GetRotationX(void) const;
			float GetRotationY(void) const;

			void Render(void);

		private:

			ModelPreviewController(const ModelPreviewController& ctrl, gui::Widget& parent, int controls);

			void SetupGui(int controls);

			void OnWheelMove(int step);
			void OnDrag(const math::Vector2& vMove);
			void OnResize(const math::Vector2& vSize);
			void OnZoom(void);

			bool m_bDirty;
			math::Vector2 m_vImageSize;
			float m_meshRotationX, m_meshRotationY;
			std::wstring m_stModel;

			gui::Widget* m_pImage;
			
			gfx::Camera m_camera;
			gfx::ModelInstance* m_pModel;
			render::IStage* m_pStage;
			const gfx::Models* m_pModels;
		};

	}
}


