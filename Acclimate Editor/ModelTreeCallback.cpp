#include "ModelTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		ModelTreeCallback::ModelTreeCallback(const gfx::IModel& Model, bool bLocked) : m_pModel(&Model),
			m_bLocked(bLocked)
		{
		}

		const std::wstring* ModelTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"ModelLockedIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"ModelIcon";
				return &stIcon;
			}
		}

		bool ModelTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool ModelTreeCallback::IsDeletable(void) const
		{
			return !m_bLocked;
		}

		bool ModelTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void ModelTreeCallback::OnSelect(void)
		{
			SigSelect(*m_pModel);
		}

		void ModelTreeCallback::OnDelete(void)
		{
			SigDelete(*m_pModel);
		}

		gui::ContextMenu* ModelTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);
			return pEntityMenu;
		}

		void ModelTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void ModelTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(*m_pModel, stName);
		}

	}
}