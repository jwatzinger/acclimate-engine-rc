#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class IModel;
	}

	namespace editor
	{

		class ModelTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			ModelTreeCallback(const gfx::IModel& Model, bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const gfx::IModel&> SigSelect;
			core::Signal<const gfx::IModel&> SigDelete;
			core::Signal<const gfx::IModel&, const std::wstring&> SigRename;

		private:

			bool m_bLocked;
			const gfx::IModel* m_pModel;
		};

	}
}


