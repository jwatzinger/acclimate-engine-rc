#include "ModelTreeModel.h"
#include "FolderTreeCallback.h"
#include "ModelTreeCallback.h"

namespace acl
{
	namespace editor
	{

		ModelTreeModel::ModelTreeModel(const ModelMap& Models) : m_root(L"Models"),
			m_bDirty(false), m_pModels(&Models)
		{
		}

		const gui::Node& ModelTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& ModelTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void ModelTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& Models : *m_pModels)
				{
					gui::Node ModelBlockNode(Models.first);
					auto pCallback = new FolderTreeCallback(Models.second.bLocked);
					ModelBlockNode.pCallback = pCallback;

					const size_t size = Models.second.pBlock->Size();
					if(size)
					{
						for(unsigned int i = 0; i < size; i++)
						{
							auto& Model = Models.second.pBlock->GetPair(i);
							gui::Node component(Model.first);
							auto pModelCallback = new ModelTreeCallback(*Model.second, Models.second.bLocked);
							pModelCallback->SigDelete.Connect(this, &ModelTreeModel::OnDeleteModel);
							pModelCallback->SigSelect.Connect(this, &ModelTreeModel::OnSelectModel);
							component.pCallback = pModelCallback;
							ModelBlockNode.vChilds.push_back(component);
						}

						m_root.vChilds.push_back(ModelBlockNode);
					}
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void ModelTreeModel::OnModelsChanged(void)
		{
			m_bDirty = true;
		}

		void ModelTreeModel::OnDeleteModel(const gfx::IModel& Model)
		{

		}

		void ModelTreeModel::OnSelectModel(const gfx::IModel& Model)
		{
			SigModelSelected(&Model);
		}

		//void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		//{
		//	SigGotoEntity(entity);
		//}

		//void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		//{
		//	SigEntitySelected(&entity);
		//}

		//void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		//{
		//	m_pEntities->RemoveEntity(entity);
		//	SigEntitySelected(nullptr);
		//	m_bDirty = true;
		//}

		//void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		//{
		//	if(m_pAttach->Execute(entity))
		//		m_bDirty = true;
		//}

		//void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		//{
		//	if(entity.GetName() != stName)
		//	{
		//		entity.SetName(stName);
		//		m_bDirty = true;
		//	}
		//}


	}
}

