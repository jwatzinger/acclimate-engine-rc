#pragma once
#include "Gui\ITreeModel.h"
#include <map>
#include "ModelBlockData.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace editor
	{

		class ModelTreeModel :
			public gui::ITreeModel
		{
			typedef std::map<std::wstring, ModelBlockData> ModelMap;
		public:
			ModelTreeModel(const ModelMap& Models);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnModelsChanged(void);

			core::Signal<const gfx::IModel*> SigModelSelected;

		private:

			void OnDeleteModel(const gfx::IModel& Model);
			void OnSelectModel(const gfx::IModel& Model);

			bool m_bDirty;
			const ModelMap* m_pModels;

			gui::Node m_root;
			core::Signal<> SigUpdated;
		};

	}
}


