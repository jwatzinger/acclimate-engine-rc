#include "NewMeshController.h"
#include "Gfx\IMesh.h"
#include "Gui\BaseWindow.h"
#include "Gui\FileDialog.h"

namespace acl
{
	namespace editor
	{

		NewMeshController::NewMeshController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Asset/NewMesh.axm"),
			m_bConfirmed(false)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"ImportWindow");

			m_pName = GetWidgetByName<gui::Textbox<>>(L"TargetName");
			m_pName->SigConfirm.Connect(this, &NewMeshController::OnConfirm);

			GetWidgetByName<gui::Widget>(L"OkButton")->SigReleased.Connect(this, &NewMeshController::OnConfirm);
			GetWidgetByName<gui::Widget>(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);
		}

		bool NewMeshController::Execute(gfx::MeshLoadInfo& info)
		{	
			m_bConfirmed = false;

			gui::FileDialog dialog(gui::DialogType::OPEN, L"Choose mesh file:");

			if(dialog.Execute())
			{
				m_pName->OnFocus(true);

				m_pWindow->Execute();

				if(m_bConfirmed)
				{
					info.stName = m_pName->GetContent();
					info.stPath = dialog.GetFullPath();
				}
				return m_bConfirmed;
			}
			else
				return false;
		}

		void NewMeshController::OnConfirm(void)
		{
			m_bConfirmed = true;
			m_pWindow->OnClose();
		}

		void NewMeshController::OnConfirm(std::wstring)
		{
			OnConfirm();
		}

	}
}

