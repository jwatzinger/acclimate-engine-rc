#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gfx
	{
		struct MeshLoadInfo;
	}

	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class NewMeshController :
			public gui::BaseController
		{
		public:
			NewMeshController(gui::Module& module);

			bool Execute(gfx::MeshLoadInfo& info);

		private:

			void OnConfirm(void);
			void OnConfirm(std::wstring);

			bool m_bConfirmed;

			gui::Textbox<>* m_pName;
			gui::BaseWindow* m_pWindow;
		};

	}
}

