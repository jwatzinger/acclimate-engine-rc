#include "NewModelController.h"
#include "MeshPickController.h"
#include "Gfx\IModel.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		NewModelController::NewModelController(gui::Module& module, const gfx::Meshes& meshes, MeshPickController& meshPick) :
			BaseController(module, L"../Editor/Menu/Asset/NewModel.axm"), m_bConfirmed(false), m_pPick(&meshPick)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");

			// name
			m_pName = GetWidgetByName<gui::Textbox<>>(L"TargetName");
			m_pName->SigConfirm.Connect(this, &NewModelController::OnConfirm);

			// mesh
			m_pMesh = GetWidgetByName<gui::Textbox<>>(L"Mesh");
			m_pMesh->SigReleased.Connect(this, &NewModelController::OnChooseMesh);

			// dialog buttons
			GetWidgetByName<gui::Widget>(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);
			GetWidgetByName<gui::Widget>(L"OkButton")->SigReleased.Connect(this, &NewModelController::OnConfirm);
		}

		bool NewModelController::Execute(gfx::ModelLoadInfo& info)
		{
			m_bConfirmed = false;
			m_pName->OnFocus(true);

			m_pWindow->Execute();

			if(m_bConfirmed)
			{
				info.stName = m_pName->GetContent();
				info.stMesh = m_pMesh->GetContent();
			}
			
			return m_bConfirmed;
		}

		void NewModelController::OnConfirm(void)
		{
			m_bConfirmed = true;
			m_pWindow->OnClose();
		}

		void NewModelController::OnConfirm(std::wstring)
		{
			OnConfirm();
		}

		void NewModelController::OnChooseMesh(void)
		{
			std::wstring stMesh;
			if(m_pPick->Execute(m_pMesh->GetContent(), nullptr, &stMesh))
				m_pMesh->SetText(stMesh);
		}

	}
}

