#pragma once
#include "Gfx\Meshes.h"
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gfx
	{
		struct ModelLoadInfo;
	}

	namespace gui
	{
		class BaseWindow;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class MeshPickController;

		class NewModelController :
			public gui::BaseController
		{
		public:
			NewModelController(gui::Module& module, const gfx::Meshes& meshes, MeshPickController& meshPick);

			bool Execute(gfx::ModelLoadInfo& info);

		private:

			void OnConfirm(void);
			void OnConfirm(std::wstring);
			void OnChooseMesh(void);

			bool m_bConfirmed;

			gui::BaseWindow* m_pWindow;
			gui::Textbox<>* m_pName, *m_pMesh;

			MeshPickController* m_pPick;
		};

	}
}

