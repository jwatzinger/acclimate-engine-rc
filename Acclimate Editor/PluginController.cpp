#include "PluginController.h"
#include "Core\ModuleManager.h"
#include "Core\ModuleLoader.h"
#include "Core\ModuleSaver.h"
#include "Gui\FileDialog.h"

namespace acl
{
	namespace core
	{
		class ModuleManager;
	}

	namespace editor
	{

		PluginController::PluginController(gui::Module& module, core::ModuleManager& modules): 
			BaseController(module, L"../Editor/Menu/Plugin.axm"), m_pModules(&modules)
		{
			m_pList = &AddWidget<PluginList>(0.0f, 0.0f, 1.0f, 1.0f, 0.02f);
			m_pList->SetPadding(16, 16, 32, 32);

			ListPlugins();
		}

		PluginController::ResourceMap PluginController::GetResources(void) const
		{
			auto vNames = m_pModules->GetModuleNames();

			ResourceMap mResources;
			for(auto& stName : vNames)
			{
				mResources[stName] = m_pModules->GetModule(stName)->pResources;
			}

			return mResources;
		}

		void PluginController::ListPlugins(void)
		{
			m_pList->Clear();

			auto vModules = m_pModules->GetModuleNames();
			for(auto& module : vModules)
			{
				m_pList->AddItem(module, module);
			}
		}

		void PluginController::OnLoad(const std::wstring& stModules)
		{
			core::ModuleLoader loader(*m_pModules);
			loader.Load(stModules);

			ListPlugins();
		}

		void PluginController::OnSave(const std::wstring& stModules)
		{
			core::ModuleSaver saver(*m_pModules);
			saver.Save(stModules);
		}

		void PluginController::OnClose(void)
		{
			m_pModules->Clear();
		}

	}
}