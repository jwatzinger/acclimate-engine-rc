#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace core
	{
		class ModuleManager;
	}

	namespace gfx
	{
		class ResourceBlock;
	}

	namespace editor
	{

		class PluginController :
			public gui::BaseController
		{
			typedef gui::List<std::wstring> PluginList;
			typedef std::map<std::wstring, gfx::ResourceBlock*> ResourceMap;
		public:
			PluginController(gui::Module& module, core::ModuleManager& modules);

			ResourceMap GetResources(void) const;

			void OnLoad(const std::wstring& stModules);
			void OnSave(const std::wstring& stModules);
			void OnClose(void);

		private:

			void ListPlugins(void);

			PluginList* m_pList;

			core::ModuleManager* m_pModules;
		};

	}
}

