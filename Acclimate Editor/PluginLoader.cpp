#include "PluginLoader.h"
#include "PluginManager.h"
#include "Core\ModuleManager.h"
#include "System\Log.h"

namespace acl
{
	namespace editor
	{

		typedef IPlugin& (*Plugin)(core::IModule&);

		PluginLoader::PluginLoader(PluginManager& manager, const core::ModuleManager& modules) : 
			m_pManager(&manager), m_pModules(&modules)
		{
		}

		void PluginLoader::LoadFromModules(void) const
		{
			auto vModules = m_pModules->GetModuleNames();
			for (auto& stModule : vModules)
			{
				auto pData = m_pModules->GetModule(stModule);

				if(Plugin plugin = (Plugin)GetProcAddress(pData->hInstance, "CreatePlugin"))
					m_pManager->AddPlugin(stModule, pData->stPath, plugin(*m_pModules->GetModule(stModule)->pModule));
				else
					sys::log->Out(sys::LogModule::EDITOR, sys::LogType::WARNING, "Dll from module", stModule, "does not contain editor plugin binding.");
			}

			m_pManager->LoadResources();
			m_pManager->InitPlugins();
		}

	}
}


