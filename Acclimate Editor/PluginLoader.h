#pragma once

namespace acl
{
	namespace core
	{
		class ModuleManager;
	}

	namespace editor
	{

		class PluginManager;

		class PluginLoader
		{
		public:
			PluginLoader(PluginManager& manager, const core::ModuleManager& modules);

			void LoadFromModules(void) const;

		private:

			PluginManager* m_pManager;
			const core::ModuleManager* m_pModules;
		};

	}
}

