#include "PluginManager.h"
#include "IPlugin.h"
#include "System\WorkingDirectory.h"

namespace acl
{
	namespace editor
	{

		PluginManager::PluginManager(IEditor& editor, const gfx::LoadContext& load) : m_pEditor(&editor),
			m_pLoad(&load)
		{
		}

		PluginManager::~PluginManager(void)
		{
			for(auto& data : m_mPlugins)
			{
				data.second.pPlugin->OnUninit();
				delete data.second.pPlugin;
			}
		}

		void PluginManager::AddPlugin(const std::wstring& stName, const std::wstring& stPath, IPlugin& plugin)
		{
			PluginData data = { &plugin, stPath };
			m_mPlugins[stName] = data;
		}

		void PluginManager::InitPlugins(void) const
		{
			for (auto& plugin : m_mPlugins)
			{
				sys::WorkingDirectory dir(plugin.second.stPath);
				plugin.second.pPlugin->OnInit(*m_pEditor);
			}
		}

		void PluginManager::LoadResources(void) const
		{
			for(auto& plugin : m_mPlugins)
			{
				sys::WorkingDirectory dir(plugin.second.stPath);
				plugin.second.pPlugin->OnLoadResources(*m_pLoad);
			}
		}

		void PluginManager::UpdatePlugins(void)
		{
			for (auto& plugin : m_mPlugins)
			{
				plugin.second.pPlugin->OnUpdate();
			}
		}

		void PluginManager::Clear(void)
		{
			for(auto& data : m_mPlugins)
			{
				data.second.pPlugin->OnUninit();
				delete data.second.pPlugin;
			}
			m_mPlugins.clear();
		}

		void PluginManager::BeginTest(void)
		{
			for(auto& data : m_mPlugins)
			{
				data.second.pPlugin->OnBeginTest();
			}
		}

		void PluginManager::EndTest(void)
		{
			for(auto& data : m_mPlugins)
			{
				data.second.pPlugin->OnEndTest();
			}
		}

	}
}
