#pragma once
#include <map>
#include <string>

namespace acl
{
	namespace gfx
	{
		struct LoadContext;
	}

	namespace gui
	{
		class Widget;
	}

	namespace editor
	{

		class IEditor;
		class IPlugin;
		class IComponentRegistry;

		struct PluginData
		{
			IPlugin* pPlugin;
			std::wstring stPath;
		};

		class PluginManager
		{
			typedef std::map<std::wstring, PluginData> PluginMap;
		public:
			PluginManager(IEditor& editor, const gfx::LoadContext& load);
			~PluginManager(void);

			void AddPlugin(const std::wstring& stName, const std::wstring& stPath, IPlugin& plugin);

			void InitPlugins(void) const;
			void LoadResources(void) const;
			void UpdatePlugins(void);
			void BeginTest(void);
			void EndTest(void);

			void Clear(void);

		private:

			PluginMap m_mPlugins;

			IEditor* m_pEditor;
			const gfx::LoadContext* m_pLoad;
		};

	}
}


