#include "Project.h"

namespace acl
{
	namespace editor
	{

		Project::Project(const std::wstring& stName, const std::wstring& stPath, const ModuleMap& mModules) :
			m_stName(stName), m_stPath(stPath), m_mModules(mModules)
		{
		}

		const std::wstring& Project::GetName(void) const
		{
			return m_stName;
		}

		const std::wstring& Project::GetPath(void) const
		{
			return m_stPath;
		}

		const std::wstring& Project::GetModule(ModuleType type) const
		{
			auto itr = m_mModules.find(type);
			if(itr != m_mModules.end())
				return itr->second;
			else
			{
				static const std::wstring stEmpty = L"";
				return stEmpty;
			}
		}

		const Project::ModuleMap& Project::GetModules(void) const
		{
			return m_mModules;
		}

		bool Project::HasModule(ModuleType type) const
		{
			return m_mModules.count(type) != 0;
		}

	}
}

