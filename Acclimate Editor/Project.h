#pragma once
#include <string>
#include <map>

namespace acl
{
	namespace editor
	{

		class Project
		{
		public:

			enum class ModuleType
			{
				RESOURCES, SCRIPTS, SCENES, PHYSICS, MODULES, EVENTS, ENTITIES, LOCALIZATION, AUDIO, INPUT, UNKNOWN
			};

			typedef std::map<ModuleType, std::wstring> ModuleMap;

			Project(const std::wstring& stName, const std::wstring& stPath, const ModuleMap& mModules);

			const std::wstring& GetName(void) const;
			const std::wstring& GetPath(void) const;
			const std::wstring& GetModule(ModuleType type) const;
			const ModuleMap& GetModules(void) const;
			bool HasModule(ModuleType type) const;

		private:

			std::wstring m_stName, m_stPath;
			ModuleMap m_mModules;
		};

	}
}


