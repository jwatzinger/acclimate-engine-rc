#include "ProjectFileLoader.h"
#include "Project.h"
#include "File\File.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{
		
		Project* ProjectFileLoader::Load(const std::wstring& stFile)
		{
			xml::Doc doc;
			doc.LoadFile(stFile);

			if(auto pRoot = doc.Root(L"Project"))
			{
				const std::wstring stPath = file::FilePath(stFile) + L"/";

				if(auto pDirectory = pRoot->FirstNode(L"Directory"))
				{
					std::wstring stFinalPath = stPath;
					if(auto pPath = pDirectory->Attribute(L"path"))
						stFinalPath += *pPath;

					Project::ModuleMap mModules;

					if(auto pResources = pDirectory->FirstNode(L"Resources"))
						mModules.emplace(Project::ModuleType::RESOURCES, pResources->Attribute(L"file")->GetValue());

					if(auto pScripts = pDirectory->FirstNode(L"Scripts"))
						mModules.emplace(Project::ModuleType::SCRIPTS, pScripts->Attribute(L"file")->GetValue());

					if(auto pScenes = pDirectory->FirstNode(L"Scenes"))
						mModules.emplace(Project::ModuleType::SCENES, pScenes->Attribute(L"file")->GetValue());

					if(auto pPhysics = pDirectory->FirstNode(L"Physics"))
						mModules.emplace(Project::ModuleType::PHYSICS, pPhysics->Attribute(L"file")->GetValue());

					if(auto pModules = pDirectory->FirstNode(L"Modules"))
						mModules.emplace(Project::ModuleType::MODULES, pModules->Attribute(L"file")->GetValue());

					if(auto pModules = pDirectory->FirstNode(L"Events"))
						mModules.emplace(Project::ModuleType::EVENTS, pModules->Attribute(L"file")->GetValue());

					if(auto pLocalization = pDirectory->FirstNode(L"Localization"))
						mModules.emplace(Project::ModuleType::LOCALIZATION, pLocalization->Attribute(L"file")->GetValue());

					if(auto pAudio = pDirectory->FirstNode(L"Audio"))
						mModules.emplace(Project::ModuleType::AUDIO, pAudio->Attribute(L"file")->GetValue());

					if(auto pAudio = pDirectory->FirstNode(L"Input"))
						mModules.emplace(Project::ModuleType::INPUT, pAudio->Attribute(L"file")->GetValue());

					return new Project(*pRoot->Attribute(L"name"), stFile, mModules);
				}
			}

			return nullptr;
		}

	}
}

