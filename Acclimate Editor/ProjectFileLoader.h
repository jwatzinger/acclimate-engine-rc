#pragma once
#include <string>

namespace acl
{
	namespace editor
	{

		class Project;

		class ProjectFileLoader
		{
		public:

			Project* Load(const std::wstring& stFilename);
		};

	}
}

