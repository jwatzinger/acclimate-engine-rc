#include "ProjectFileSaver.h"
#include "Project.h"
#include "File\File.h"
#include "System\Assert.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		std::wstring moduleTypeToString(Project::ModuleType type)
		{
			switch(type)
			{
			case Project::ModuleType::RESOURCES:
				return L"Resources";
			case Project::ModuleType::SCENES:
				return L"Scenes";
			case Project::ModuleType::SCRIPTS:
				return L"Scripts";
			case Project::ModuleType::PHYSICS:
				return L"Physics";
			case Project::ModuleType::MODULES:
				return L"Modules";
			case Project::ModuleType::EVENTS:
				return L"Events";
			case Project::ModuleType::LOCALIZATION:
				return L"Localization";
			case Project::ModuleType::AUDIO:
				return L"Audio";
			case Project::ModuleType::INPUT:
				return L"Input";
			default:
				ACL_ASSERT(false);
			}

			return L"";
		}

		void ProjectFileSaver::Save(const Project& project)
		{
			xml::Doc doc;

			auto& root = doc.InsertNode(L"Project");
			root.ModifyAttribute(L"name", project.GetName());
			
			auto& directory = root.InsertNode(L"Directory");
			directory.ModifyAttribute(L"path", L"Game/Data/");

			auto& mModules = project.GetModules();

			for(auto& module : mModules)
			{
				std::wstring stModule = moduleTypeToString(module.first);

				auto& node = directory.InsertNode(stModule);
				node.ModifyAttribute(L"file", module.second);
			}

			doc.SaveFile(project.GetPath());

		}

	}
}

