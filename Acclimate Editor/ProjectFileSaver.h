#pragma once
#include <string>

namespace acl
{
	namespace editor
	{

		class Project;

		class ProjectFileSaver
		{
		public:

			void Save(const Project& project);
		};

	}
}


