#include "ProjectModule.h"
#include "Project.h"
#include "RecentlyOpened.h"
#include "RecentlyOpenedLoader.h"
#include "RecentlyOpenedSaver.h"
#include "Utility.h"
#include "Core\ModuleLoader.h"
#include "File\Dir.h"
#include "File\File.h"
#include "Gui\FileDialog.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Gui\MsgBox.h"
#include "Gui\Module.h"

namespace acl
{
	namespace editor
	{

		ProjectModule::ProjectModule(gui::MenuBar& bar) :
			m_pProject(nullptr), m_pModule(bar.GetModule()), m_pRecentlyOpened(nullptr)
		{
			auto& fileItem = bar.AddItem(L"FILE");

			auto& newOption = fileItem.AddOption(L"New project");
			newOption.SigReleased.Connect(this, &ProjectModule::OnSelfNew);
			newOption.SetShortcut(gui::ComboKeys::CTRL, 'N', gui::ShortcutState::ALWAYS);
			newOption.SigShortcut.Connect(this, &ProjectModule::OnSelfNew);

			auto& loadOption = fileItem.AddOption(L"Load project");
			loadOption.SigReleased.Connect(this, &ProjectModule::OnSelfLoad);
			loadOption.SetShortcut(gui::ComboKeys::CTRL, 'O', gui::ShortcutState::ALWAYS);
			loadOption.SigShortcut.Connect(this, &ProjectModule::OnSelfLoad);

			auto& saveOption = fileItem.AddOption(L"Save project");
			saveOption.SigReleased.Connect(this, &ProjectModule::OnSelfSave);
			saveOption.SetShortcut(gui::ComboKeys::CTRL, 'S', gui::ShortcutState::ALWAYS);
			saveOption.SigShortcut.Connect(this, &ProjectModule::OnSelfSave);
			saveOption.OnDisable();
			SigSetProjectLoaded.Connect(&saveOption, &gui::Widget::SetEnabled);

			auto& closeOption = fileItem.AddOption(L"Close project");
			closeOption.SigReleased.Connect(this, &ProjectModule::OnSelfClose);
			closeOption.OnDisable();
			SigSetProjectLoaded.Connect(&closeOption, &gui::Widget::SetEnabled);

			/***************************************
			* Recently opened
			****************************************/
			
			fileItem.InsertSeperator();
			
			m_pRecentlyOpened = new RecentlyOpened(fileItem);
			m_pRecentlyOpened->SigLoad.Connect(this, &ProjectModule::OnSelfLoad);

			// load
			RecentlyOpenedLoader recentLoader(*m_pRecentlyOpened);
			recentLoader.Load(L"../Editor/Config/RecentlyLoaded.axm");

			fileItem.InsertSeperator();

			auto& quitOption = fileItem.AddOption(L"Quit");
			quitOption.SigReleased.Connect(this, &ProjectModule::OnQuit);
		}

		ProjectModule::~ProjectModule(void)
		{
			delete m_pProject;
		}

		Project::ModuleType ProjectModule::GetModuleType(void) const
		{
			return Project::ModuleType::UNKNOWN;
		}

		void ProjectModule::Update(void)
		{
			m_pRecentlyOpened->Update();
		}

		void ProjectModule::OnNew(const std::wstring& stFile)
		{
		}

		void ProjectModule::OnLoad(const std::wstring& stFile)
		{
		}

		void ProjectModule::OnSave(const std::wstring& stFile)
		{
		}

		void ProjectModule::OnSceneChanged(void)
		{
		}

		void ProjectModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void ProjectModule::OnClose(void)
		{
		}

		void ProjectModule::OnQuit(void)
		{
			if(ClosePrompt())
				SigQuit();
		}

		void ProjectModule::OnSelfNew(void)
		{
			if(m_pProject)
			{
				gui::MsgBox msgBox(L"", L"Save current project?");
				m_pModule->RegisterWidget(msgBox);
				if(!msgBox.Execute())
					OnSelfSave();
			}

			gui::FileDialog dialog(gui::DialogType::SAVE, L"Select destination folder", L"Acclimate Project files(.acp)\0*.acp\0\0");
			if(dialog.Execute())
			{
				const std::wstring& stFullPath = dialog.GetFullPath();
				const std::wstring stPath = file::FilePath(dialog.GetFullPath());
				file::DirCreate(stPath);
				const std::wstring stGameDir = stPath + L"/Game";
				file::DirCreate(stGameDir);
				file::DirCreate(stGameDir + L"/Data");
				file::DirCreate(stGameDir + L"/Scenes");
				file::DirCreate(stGameDir + L"/Resources");
				file::DirCreate(stGameDir + L"/Modules");
				file::DirCreate(stGameDir + L"/Scripts");

				Project::ModuleMap mModules;
				mModules.emplace(Project::ModuleType::RESOURCES, L"Resources.axm");
				mModules.emplace(Project::ModuleType::SCRIPTS, L"Script.axm");
				mModules.emplace(Project::ModuleType::SCENES, L"Scenes.axm");
				mModules.emplace(Project::ModuleType::MODULES, L"Modules.axm");
				mModules.emplace(Project::ModuleType::PHYSICS, L"Physics.axm");

				delete m_pProject;
				m_pProject = new Project(L"Test", stFullPath, mModules);

				SigNew(*m_pProject);
				OnSelfSave();

				SigLoaded(*m_pProject);
				SigSetProjectLoaded(m_pProject != nullptr);
			}
		}

		void ProjectModule::OnSelfSave(void)
		{
			if(m_pProject)
			{
				m_saver.Save(*m_pProject);
				SigSaved(*m_pProject);
			}
		}

		void ProjectModule::OnSelfLoad(void)
		{
			gui::FileDialog dialog(gui::DialogType::OPEN, L"Select project file to open", L"Acclimate Project files(.acp)\0*.acp\0\0");
			if(dialog.Execute())
			{
				const std::wstring& stPath = dialog.GetFullPath();
				OnSelfLoad(stPath);
			}
		}

		void ProjectModule::OnSelfLoad(const std::wstring& stName)
		{
			delete m_pProject;
			m_pProject = m_loader.Load(stName);
			m_pRecentlyOpened->PushProject(stName);

			RecentlyOpenedSaver saver(*m_pRecentlyOpened);
			saver.Save(editor::GetEditorDirectory() + L"Config/RecentlyLoaded.axm");

			SigLoaded(*m_pProject);
			SigSetProjectLoaded(m_pProject != nullptr);
		}

		void ProjectModule::OnSelfClose(void)
		{
			if(ClosePrompt())
			{
				delete m_pProject;
				m_pProject = nullptr;
				SigSetProjectLoaded(m_pProject != nullptr);
				SigClose();
			}	
		}

		bool ProjectModule::ClosePrompt(void)
		{
			if(m_pProject)
			{
				gui::MsgBox::CustomButtonVector vButtons = { L"Save", L"Don't save", L"Cancel" };
				gui::MsgBox msgBox(L"", L"Do you want to save before closing the project?", gui::IconType::QUESTION, &vButtons);
				m_pModule->RegisterWidget(msgBox);
				switch(msgBox.Execute())
				{
				case 0:
					OnSelfSave();
					break;
				case 2:
					return false;
				}
			}

			return true;
		}

		void ProjectModule::OnBeginTest(void)
		{
		}

		void ProjectModule::OnEndTest(void)
		{
		}

	}
}

