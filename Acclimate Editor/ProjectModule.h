#pragma once
#include "IModule.h"
#include "ProjectFileLoader.h"
#include "ProjectFileSaver.h"
#include "Core\Signal.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gfx
	{
		class IResourceLoader;
	}

	namespace gui
	{
		class MenuBar;
		class Module;
	}

	namespace editor
	{

		class RecentlyOpened;

		class ProjectModule :
			public IModule
		{
		public:
			ProjectModule(gui::MenuBar& bar);
			~ProjectModule(void);

			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;
			
			void OnQuit(void);

			core::Signal<const Project&> SigNew;
			core::Signal<const Project&> SigLoaded;
			core::Signal<const Project&> SigSaved;
			core::Signal<> SigClose;
			core::Signal<> SigQuit;

		private:

			core::Signal<bool> SigSetProjectLoaded;

			void OnSelfNew(void);
			void OnSelfSave(void);
			void OnSelfLoad(void);
			void OnSelfLoad(const std::wstring& stName);
			void OnSelfClose(void);

			bool ClosePrompt(void);

			Project* m_pProject;
			RecentlyOpened* m_pRecentlyOpened;
			ProjectFileLoader m_loader;
			ProjectFileSaver m_saver;

			gui::Module* m_pModule;
		};

	}
}


