#include "RecentlyOpened.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		RecentlyOpened::RecentlyOpened(gui::MenuItem& item) : m_bDirty(false)
		{
			m_pOption = &item.AddOption(L"Recently opened projects");
			m_pOption->OnDisable();
		}

		const RecentlyOpened::PathVector& RecentlyOpened::GetVector(void) const
		{
			return m_vRecent;
		}

		void RecentlyOpened::PushProject(const std::wstring& stFilepath)
		{
			auto itr = std::find(m_vRecent.begin(), m_vRecent.end(), stFilepath);
			if(itr != m_vRecent.end())
				m_vRecent.erase(itr);

			m_vRecent.push_back(stFilepath);
			m_bDirty = true;
		}

		void RecentlyOpened::Update(void)
		{
			if(m_bDirty)
			{
				m_vContainer.clear();
				m_pOption->Clear();

				m_pOption->SetEnabled(m_vRecent.size() != 0);
				unsigned int count = 0;
				for(auto& itr = m_vRecent.rbegin(); itr != m_vRecent.rend(); ++itr)
				{
					auto& option = m_pOption->AddOption(conv::ToString(count + 1) + L" " + *itr);
					m_vContainer.emplace_back(option, option.SigReleased, *itr);
					m_vContainer[count].SigAccess.Connect(this, &RecentlyOpened::OnOpen);
					count++;
				}
				m_bDirty = false;
			}
		}

		void RecentlyOpened::OnOpen(std::wstring stName)
		{
			SigLoad(stName);
		}

	}
}

