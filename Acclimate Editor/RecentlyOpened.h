#pragma once
#include <vector>
#include <string>
#include "Gui\DataContainer.h"

namespace acl
{
	namespace gui
	{
		class MenuItem;
		class MenuOption;
	}

	namespace editor
	{

		class RecentlyOpened
		{
			typedef std::vector<std::wstring> PathVector;

			typedef gui::DataContainer<std::wstring> IdContainer;
			typedef std::vector<IdContainer> ContainerVector;
		public:

			RecentlyOpened(gui::MenuItem& item);

			const PathVector& GetVector(void) const;

			void PushProject(const std::wstring& stFilepath);

			void Update(void);

			core::Signal<const std::wstring&> SigLoad;

		private:

			void OnOpen(std::wstring stName);

			bool m_bDirty;

			PathVector m_vRecent;
			ContainerVector m_vContainer;

			gui::MenuOption* m_pOption;
		};

	}
}


