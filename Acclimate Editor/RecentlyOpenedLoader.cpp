#include "RecentlyOpenedLoader.h"
#include <map>
#include "RecentlyOpened.h"
#include "System\Convert.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		RecentlyOpenedLoader::RecentlyOpenedLoader(RecentlyOpened& recent) : m_pRecent(&recent)
		{
		}

		void RecentlyOpenedLoader::Load(const std::wstring& stFilename)
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			if(auto pRoot = doc.Root(L"Projects"))
			{
				if(auto pProjects = pRoot->Nodes(L"Project"))
				{
					std::map<unsigned int, std::wstring> mSorted;
					for(auto pProject : *pProjects)
					{
						const unsigned int order = conv::FromString<unsigned int>(pProject->GetValue());

						mSorted.emplace(order, *pProject->Attribute(L"file"));
					}

					for(auto& sorted : mSorted)
					{
						m_pRecent->PushProject(sorted.second);
					}
				}
			}
		}

	}
}
