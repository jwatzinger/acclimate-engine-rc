#pragma once
#include <string>

namespace acl
{
	namespace editor
	{

		class RecentlyOpened;

		class RecentlyOpenedLoader
		{
		public:
			RecentlyOpenedLoader(RecentlyOpened& recent);

			void Load(const std::wstring& stFilename);

		private:

			RecentlyOpened* m_pRecent;

		};

	}
}
