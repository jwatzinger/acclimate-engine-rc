#include "RecentlyOpenedSaver.h"
#include "RecentlyOpened.h"
#include "System\Convert.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		RecentlyOpenedSaver::RecentlyOpenedSaver(RecentlyOpened& recent):
			m_pRecent(&recent)
		{
		}

		void RecentlyOpenedSaver::Save(const std::wstring& stName)
		{
			xml::Doc doc;
			auto& root = doc.InsertNode(L"Projects");

			auto& vProjects = m_pRecent->GetVector();
			unsigned int i = vProjects.size()-1;
			for(auto itr = vProjects.rbegin(); itr != vProjects.rend(); ++itr)
			{
				auto& project = root.InsertNode(L"Project");
				project.ModifyAttribute(L"file", *itr);
				project.SetValue(conv::ToString(i));
				i--;
			}

			doc.SaveFile(stName);
		}

	}
}

