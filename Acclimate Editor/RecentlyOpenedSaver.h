#pragma once
#include <string>

namespace acl
{
	namespace editor
	{

		class RecentlyOpened;

		class RecentlyOpenedSaver
		{
		public:
			RecentlyOpenedSaver(RecentlyOpened& recent);

			void Save(const std::wstring& stName);

		private:

			RecentlyOpened* m_pRecent;
		};

	}
}


