#include "AddSceneController.h"
#include "Gui\Window.h"
#include "Gui\Checkbox.h"

namespace acl
{
	namespace editor
	{

		AddSceneController::AddSceneController(gui::Module& module) : BaseController(module, L"../Editor/Menu/Scenes/AddScene.axm"),
			m_create(false)
		{
			// name
			m_pBox = &AddWidget<gui::Textbox<>>(0.0f, 8.0f, 1.0f, 21.0f, L"New scene");
			m_pBox->SetPadding(gui::PaddingSide::LEFT, 8);
			m_pBox->SetPadding(gui::PaddingSide::RIGHT, 16);
			m_pBox->SetPositionMode(gui::Position::Y, gui::PositionMode::ABS);
			m_pBox->SetPositionMode(gui::Position::HEIGHT, gui::PositionMode::ABS);
			m_pBox->OnFocus(true);
			m_pBox->SigConfirm.Connect(this, &AddSceneController::OnCreate);

			m_pWindow = GetWidgetByName<gui::Window>(L"Window");

			GetWidgetByName(L"Cancel")->SigReleased.Connect(this, &AddSceneController::OnCancel);
			GetWidgetByName(L"Add")->SigReleased.Connect(this, &AddSceneController::OnCreate);
		}

		AddSceneController::~AddSceneController()
		{
		}

		bool AddSceneController::Execute(std::wstring* pName)
		{
			m_create = false;

			m_pWindow->Execute();

			if(m_create)
			{
				if(pName)
					*pName = m_pBox->GetContent();
				return true;
			}
			else
				return false;
		}

		void AddSceneController::OnCreate(void)
		{
			m_create = true;
			m_pWindow->OnClose();
		}

		void AddSceneController::OnCancel(void)
		{
			m_create = false;
			m_pWindow->OnClose();
		}

	}
}
