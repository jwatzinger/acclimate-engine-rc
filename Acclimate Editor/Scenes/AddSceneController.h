#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gui
	{
		class Window;
	}

	namespace editor
	{
		
		class AddSceneController :
			public gui::BaseController
		{
		public:
			AddSceneController(gui::Module& module);
			~AddSceneController(void);

			bool Execute(std::wstring* pName);

		private:

			void OnCreate(void);
			void OnCancel(void);

			bool m_create;

			gui::Textbox<>* m_pBox;
			gui::Window* m_pWindow;
		};

	}
}

