#include "SceneExplorer.h"
#include <algorithm>
#include "AddSceneController.h"
#include "Core\SceneManager.h"
#include "Core\SceneSaver.h"
#include "File\Dir.h"
#include "Gui\DockArea.h"
#include "Gui\Dock.h"
#include "Gui\ContextMenu.h"
#include "Gui\ContextMenuItem.h"
#include "..\Utility.h"

#include "System\WorkingDirectory.h"

namespace acl
{
	namespace editor
	{

		SceneExplorer::SceneExplorer(gui::Module& module, gui::DockArea& dock, core::SceneManager& scenes) :
			BaseController(module, L"../Editor/Menu/Scenes/Explorer.axm"), m_pScenes(&scenes)
		{
			dock.AddDock(*GetWidgetByName<gui::Dock>(L"Dock"), gui::DockType::RIGHT, true);

			m_pList = GetWidgetByName<SceneList>(L"Scenes");
			m_pList->SigItemPicked.Connect(this, &SceneExplorer::OnPickScene);

			auto& contextMenu = AddWidget<gui::ContextMenu>(0.02f);
			auto& addScene = contextMenu.AddItem(L"Add scene");
			addScene.SigReleased.Connect(this, &SceneExplorer::OnAddScene);
			
			m_pList->SetContextMenu(&contextMenu);

			OnRefreshList();
		}

		SceneExplorer::~SceneExplorer(void)
		{
		}

		void SceneExplorer::OnScenesChanged(void)
		{
			OnRefreshList();
		}

		void SceneExplorer::OnRefreshList(void)
		{
			const auto& scenes = m_pScenes->GetScenes();

			const size_t sceneSize = scenes.Size();
			const size_t selectedId = max(0, min((int)sceneSize - 1, m_pList->GetSelectedId()));
			m_pList->Clear();

			if(!sceneSize)
				return;

			for(auto scene : scenes.Map())
			{
				m_pList->AddItem(scene.first, scene.first);
			}

			m_pList->SelectById(selectedId);
		}

		void SceneExplorer::OnPickScene(std::wstring stScene)
		{
			SigSelected(stScene);
		}

		void SceneExplorer::OnAddScene(void)
		{
			AddSceneController addScene(*m_pModule);
			
			std::wstring stScene;
			if(addScene.Execute(&stScene))
			{
				const std::wstring& stDir = getProjectDirectory() + L"\\Scenes\\" + stScene + L"\\";
				file::DirCreate(stDir);
				sys::WorkingDirectory dir(stDir);
				
				core::Scene::ModuleMap mModules;
				mModules.emplace(core::Scene::ModuleType::RESOURCES, L"Resources.axm");
				mModules.emplace(core::Scene::ModuleType::SCRIPTS, L"Script.axm");
				mModules.emplace(core::Scene::ModuleType::ENTITIES, L"Entities.axm");
				mModules.emplace(core::Scene::ModuleType::EVENTS, L"Events.axm");
				//mModules.emplace(core::Scene::ModuleType::PHYSICS, L"Physics.axm");

				const auto& mExtentions = m_pScenes->GetExtentions();
				core::Scene::ExtentionVector vExtentions;
				vExtentions.reserve(mExtentions.size());

				for(auto& extention : mExtentions)
				{
					vExtentions.emplace_back(extention.first);
					extention.second->OnNew();
				}

				auto& stSceneA = conv::ToA(stScene);
				std::string::iterator end_pos = std::remove(stSceneA.begin(), stSceneA.end(), ' ');
				stSceneA.erase(end_pos, stSceneA.end());

				core::Scene& scene = *new core::Scene(stScene, stDir, mModules, vExtentions, stSceneA, core::Scene::CustomMap(), core::Scene::InputVector());

				m_pScenes->AddScene(scene);

				core::SceneSaver saver(*m_pScenes);
				saver.SaveScene(stScene, L"Scene.axm");

				SigSceneAdded(scene);
			}
		}

	}
}

