#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace core
	{
		class SceneManager;
		class Scene;
	}

	namespace gui
	{
		class DockArea;
	}

	namespace editor
	{

		class SceneExplorer :
			public gui::BaseController
		{
			typedef gui::List<std::wstring> SceneList;
		public:
			SceneExplorer(gui::Module& module, gui::DockArea& dock, core::SceneManager& scenes);
			~SceneExplorer();

			void OnScenesChanged(void);

			core::Signal<const std::wstring&> SigSelected;
			core::Signal<const core::Scene&> SigSceneAdded;

		private:

			void OnPickScene(std::wstring stScene);
			void OnRefreshList(void);
			void OnAddScene(void);

			SceneList* m_pList;

			core::SceneManager* m_pScenes;
		};

	}
}


