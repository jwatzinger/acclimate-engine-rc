#include "SceneModule.h"
#include "SceneExplorer.h"
#include "Core\SceneManager.h"
#include "Core\SceneSaver.h"
#include "Core\SceneInstance.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"

namespace acl
{
	namespace editor
	{

		SceneModule::SceneModule(gui::BaseController& controller, gui::MenuBar& bar, gui::DockArea& dock, core::SceneManager& scenes, const core::SceneLoader& loader) :
			m_pLoader(&loader), m_pScenes(&scenes), m_bDirty(nullptr)
		{
			scenes.SetLocked(true);

			auto& sceneItem = bar.AddItem(L"SCENES");

			// explorer
			m_pExplorer = new SceneExplorer(*bar.GetModule(), dock, scenes);
			m_pExplorer->SigSelected.Connect(this, &SceneModule::OnChangeScene);
			m_pExplorer->SigSceneAdded.Connect(this, &SceneModule::OnSceneAdded);

			auto& newScene = sceneItem.AddOption(L"New scene");
			//newScene.SigReleased.Connect(m_pTextures, &TextureController::OnToggle);
			newScene.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'S', gui::ShortcutState::ALWAYS);
			//newScene.SigShortcut.Connect(m_pTextures, &TextureController::OnToggle);

			sceneItem.InsertSeperator();

			auto& viewScenes = sceneItem.AddOption(L"View scenes");
			viewScenes.SetShortcut(gui::ComboKeys::SHIFT_CTRL, 'V', gui::ShortcutState::ALWAYS);
		}

		gfx::ResourceBlock* SceneModule::GetResources(void)
		{
			if(auto pScene = m_pScenes->GetActiveSceneInstance())
				return &pScene->GetResources();
			else
				return nullptr;
		}

		Project::ModuleType SceneModule::GetModuleType(void) const
		{
			return Project::ModuleType::SCENES;
		}

		void SceneModule::OnNew(const std::wstring& stFile)
		{
		}

		void SceneModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void SceneModule::Update(void)
		{
			if(m_bDirty)
			{
				if(auto pActiveScene = m_pScenes->GetActiveScene())
				{
					SigSceneChanged();
					m_bDirty = false;
				}	
			}
		}

		void SceneModule::OnLoad(const std::wstring& stFile)
		{
			m_pLoader->Load(stFile);

			m_pExplorer->OnScenesChanged();
		}

		void SceneModule::OnSave(const std::wstring& stFile)
		{
			core::SceneSaver saver(*m_pScenes);
			saver.Save(stFile);

			if(auto pScene = m_pScenes->GetActiveScene())
				SigSave(*pScene);
		}

		void SceneModule::OnClose(void)
		{
			m_pScenes->Clear();

			m_pExplorer->OnScenesChanged();
		}

		void SceneModule::OnSceneChanged(void)
		{
		}

		void SceneModule::OnChangeScene(const std::wstring& stScene)
		{
			m_pScenes->SetLocked(false);
			m_pScenes->ActivateScene(stScene);
			m_pScenes->SetLocked(true);

			m_bDirty = true;
		}

		void SceneModule::OnSceneAdded(const core::Scene& scene)
		{
			OnChangeScene(scene.GetName());

			m_pExplorer->OnScenesChanged();

			SigSceneAdded(scene);
		}

		void SceneModule::OnBeginTest(void)
		{
		}

		void SceneModule::OnEndTest(void)
		{
		}

	}
}
