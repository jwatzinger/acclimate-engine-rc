#pragma once
#include "..\IModule.h"
#include "Core\Signal.h"
#include "Core\SceneLoader.h"

namespace acl
{
	namespace core
	{
		class Scene;
	}

	namespace gfx
	{
		class ResourceBlock;
	}

	namespace gui
	{
		class BaseController;
		class MenuBar;
		class DockArea;
	}

	namespace editor
	{

		class SceneExplorer;

		class SceneModule :
			public IModule
		{
		public:
			SceneModule(gui::BaseController& controller, gui::MenuBar& bar, gui::DockArea& dock, core::SceneManager& scenes, const core::SceneLoader& loader);

			gfx::ResourceBlock* GetResources(void);
			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

			core::Signal<> SigSceneChanged;
			core::Signal<const core::Scene&> SigSave;
			core::Signal<const core::Scene&> SigSceneAdded;

		private:

			void OnChangeScene(const std::wstring& stScene);
			void OnSceneAdded(const core::Scene& scene);

			bool m_bDirty;

			SceneExplorer* m_pExplorer;

			core::SceneManager* m_pScenes;
			const core::SceneLoader* m_pLoader;

		};

	}
}


