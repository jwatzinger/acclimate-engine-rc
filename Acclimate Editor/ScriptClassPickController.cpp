#include "ScriptClassPickController.h"
#include "Gui\BaseWindow.h"
#include "Gui\TextArea.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		ScriptClassPickController::ScriptClassPickController(gui::Module& module, const script::Core& script) :
			BaseController(module, L"../Editor/Menu/Script/PickClass.axm"), m_pScript(&script), m_bPick(false)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"ClassWindow");

			m_pList = &AddWidget<ClassList>(0.0f, 0.0f, 0.4f, 0.9f, 0.02f);
			m_pList->SigConfirm.Connect(this, &ScriptClassPickController::OnPick);

			GetWidgetByName(L"Pick")->SigReleased.Connect(this, &ScriptClassPickController::OnPick);

			OnRefresh("");
		}

		bool ScriptClassPickController::Execute(std::string* pName)
		{
			return Execute("", pName);
		}

		bool ScriptClassPickController::Execute(const std::string& stDefault, std::string* pName)
		{
			m_bPick = false;
			m_pList->OnFocus(true);

			if(!stDefault.empty())
				m_pList->SelectByName(conv::ToW(stDefault));

			m_pWindow->Execute();
			if(m_bPick)
			{
				if(pName)
					*pName = conv::ToA(m_pList->GetSelectedName());
			}
			return m_bPick;
		}

		void ScriptClassPickController::OnRefresh(const std::string& stInterface)
		{
			m_pList->Clear();

			auto vTypes = m_pScript->GetTypesWithInterface(stInterface.c_str());
			for(auto& type : vTypes)
			{
				const std::string stName = type.GetName();
				m_pList->AddItem(conv::ToW(stName), stName);
			}

			if(m_pList->GetSize())
			{
				m_pList->SelectById(0);
				//GetWidgetByName<gui::TextArea>(L"Script")->SetText(conv::ToW(m_pScript->GetScript(L"Scripts/FreeCameraController.as")));
			}
		}

		void ScriptClassPickController::OnPick(void)
		{
			m_bPick = true;
			m_pWindow->OnClose();
		}


	}
}

