#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace script
	{
		class Core;
	}

	namespace editor
	{

		class ScriptClassPickController :
			public gui::BaseController
		{
			typedef gui::List<std::string> ClassList;
		public:
			ScriptClassPickController(gui::Module& module, const script::Core& script);

			bool Execute(std::string* pName);
			bool Execute(const std::string& stDefault, std::string* pName);

			void OnRefresh(const std::string& stInterface);

		private:

			void OnPick(void);

			bool m_bPick;

			ClassList* m_pList;
			gui::BaseWindow* m_pWindow;

			const script::Core* m_pScript;
		};

	}
}


