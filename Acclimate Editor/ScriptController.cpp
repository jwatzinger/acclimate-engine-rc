#include "ScriptController.h"
#include "ScriptLoadController.h"
#include "Gui\FileDialog.h"
#include "Gui\MsgBox.h"
#include "Gui\TextArea.h"
#include "Script\Context.h"
#include "Script\Core.h"

namespace acl
{
	namespace editor
	{

		ScriptController::ScriptController(gui::Module& module, const script::Context& ctx) : m_scripts(ctx.scripts),
			BaseController(module, L"../Editor/Menu/Script/Scripts.axm"), m_pCtx(&ctx)
		{
			// texture list
			m_pList = &AddWidget<ScriptList>(0.0f, 0.0f, 0.3f, 0.9f, 0.02f);
			m_pList->SigItemPicked.Connect(this, &ScriptController::OnPickScript);

			// texture button
			GetWidgetByName<gui::Widget>(L"AddScript")->SigReleased.Connect(this, &ScriptController::OnNewTexture);

			m_pText = GetWidgetByName<gui::TextArea>(L"Text");

			//// delete button
			//m_pDelete = GetWidgetByName<gui::Widget>(L"Delete");
			//m_pDelete->SigReleased.Connect(this, &TextureController::OnDeleteSelected);

			OnRefresh();
		}

		ScriptController::~ScriptController(void)
		{
			m_scripts.Clear();
		}

		const script::Scripts::Block& ScriptController::GetResources(void) const
		{
			return m_scripts;
		}

		void ScriptController::Clear(void)
		{
			m_scripts.Clear();
			OnRefresh();
			m_pCtx->core.Reload();
		}

		void ScriptController::OnLoad(const std::wstring& stFile)
		{
			m_scripts.Begin();
			m_pCtx->loader.FromConfig(stFile);
			m_scripts.End();
			OnRefresh();
		}

		void ScriptController::OnLoadScript(const std::wstring& stName, const std::wstring& stFile)
		{
			m_scripts.Begin();
			m_pCtx->loader.FromFile(stName, stFile);
			m_scripts.End();
			OnRefresh();
		}

		void ScriptController::OnRefresh(void)
		{
			const size_t scriptSize = m_pCtx->scripts.Size();
			const size_t selectedId = max(0, min((int)scriptSize - 1, m_pList->GetSelectedId()));
			m_pList->Clear();

			if(!scriptSize)
			{
				OnPickScript(nullptr);
				return;
			}

			for(auto& script : m_pCtx->scripts.Map())
			{
				m_pList->AddItem(script.first, script.second);
			}

			m_pList->SelectById(selectedId);
		}

		void ScriptController::OnNewTexture(void)
		{
			ScriptLoadController loader(*m_pModule);

			std::wstring stName, stFile;
			if(loader.Execute(stName, stFile))
			{
				m_pCtx->loader.FromFile(stName, stFile);
				m_scripts.Add(stName);
				m_pCtx->core.Reload();
				OnRefresh();
			}
		}

		void ScriptController::OnPickScript(const script::File* pScript)
		{
			if(pScript)
				m_pText->SetText(conv::ToW(pScript->GetData()));
			else
				m_pText->SetText(L"");
		}

		void ScriptController::OnDeleteSelected(void)
		{
			//int id = m_pList->GetSelectedId();
			//if(id > -1)
			//{
			//	m_textures.Delete(id);
			//	m_vLoadInfos.erase(m_vLoadInfos.begin() + id);
			//	OnRefreshList();
			//}
		}

	}
}

