#pragma once
#include <vector>
#include "Gui\BaseController.h"
#include "Gui\List.h"
#include "Script\Resources.h"

namespace acl
{
	namespace gui
	{
		class TextArea;
	}

	namespace script
	{
		struct Context;
	}

	namespace editor
	{

		class ScriptController :
			public gui::BaseController
		{
			typedef gui::List<const script::File*> ScriptList;
		public:

			ScriptController(gui::Module& module, const script::Context& ctx);
			~ScriptController(void);

			const script::Scripts::Block& GetResources(void) const;

			void Clear(void);

			void OnLoad(const std::wstring& stFile);
			void OnLoadScript(const std::wstring& stName, const std::wstring& stFile);
			void OnRefresh(void);

		private:

			void OnNewTexture(void);
			void OnPickScript(const script::File* pFile);
			void OnDeleteSelected(void);

			ScriptList* m_pList;
			gui::TextArea* m_pText;

			const script::Context* m_pCtx;
			script::Scripts::Block m_scripts;
		};

	}
}


