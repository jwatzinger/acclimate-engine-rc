#include "ScriptLoadController.h"
#include "Gui\BaseWindow.h"
#include "Gui\FileDialog.h"

namespace acl
{
	namespace editor
	{

		ScriptLoadController::ScriptLoadController(gui::Module& module):
			BaseController(module, L"../Editor/Menu/Script/Loader.axm"), m_bConfirmed(true)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");

			m_pFile = GetWidgetByName<gui::Textbox<>>(L"File");
			m_pFile->SigReleased.Connect(this, &ScriptLoadController::OnPickFile);
			m_pName = GetWidgetByName<gui::Textbox<>>(L"TargetName");

			GetWidgetByName(L"OkButton")->SigReleased.Connect(this, &ScriptLoadController::OnConfirm);
			GetWidgetByName(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);
		}
	
		bool ScriptLoadController::Execute(std::wstring& stName, std::wstring& stFile)
		{
			m_bConfirmed = false;
			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				stFile = m_pFile->GetContent();
				stName = m_pName->GetContent();
			}
			
			return m_bConfirmed;
		}

		void ScriptLoadController::OnConfirm(void)
		{
			m_bConfirmed = true;
			m_pWindow->OnClose();
		}

		void ScriptLoadController::OnPickFile(void)
		{
			gui::FileDialog dialog(gui::DialogType::OPEN, L"Open script");

			if(dialog.Execute())
				m_pFile->SetText(dialog.GetFullPath());
		}

	}
}
