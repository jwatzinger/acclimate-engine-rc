#pragma once
#include "Gui\BaseController.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class ScriptLoadController :
			public gui::BaseController
		{
		public:
			ScriptLoadController(gui::Module& module);

			bool Execute(std::wstring& stName, std::wstring& stFile);
		
		private:

			void OnConfirm(void);
			void OnPickFile(void);

			bool m_bConfirmed;

			gui::BaseWindow* m_pWindow;
			gui::Textbox<>* m_pFile, *m_pName;
		};

	}
}

