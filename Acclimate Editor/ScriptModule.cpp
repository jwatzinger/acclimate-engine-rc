#include "ScriptModule.h"
#include <algorithm>
#include "ScriptController.h"
#include "File\File.h"
#include "File\Dir.h"
#include "Gui\BaseController.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"
#include "Script\Context.h"
#include "ScriptSaver.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		ScriptModule::ScriptModule(gui::BaseController& controller, gui::MenuBar& bar, const script::Context& script)
		{
			m_pController = &controller.AddController<ScriptController>(script);

			auto& scriptItem = bar.AddItem(L"SCRIPT");
			auto& scriptOption = scriptItem.AddOption(L"Edit scripts");
			scriptOption.SigReleased.Connect(m_pController, &ScriptController::OnToggle);
			scriptOption.SetShortcut(gui::ComboKeys::CTRL_ALT, 'S', gui::ShortcutState::ALWAYS);
			scriptOption.SigShortcut.Connect(m_pController, &ScriptController::OnToggle);
		}

		ScriptModule::~ScriptModule(void)
		{
		}

		Project::ModuleType ScriptModule::GetModuleType(void) const
		{
			return Project::ModuleType::SCRIPTS;
		}

		void ScriptModule::Update(void)
		{
		}

		void ScriptModule::OnNew(const std::wstring& stFile)
		{
			const std::wstring stScriptFile = L"Scripts/main.as";
			const std::string stScript = "void startUp(void)\n{\n}\n\nvoid update(double dt)\n{\n}\n\nvoid shutDown(void){\n}";

			file::SaveFileA(stScriptFile, stScript);

			m_pController->OnLoadScript(L"main", stScriptFile);
		}

		void ScriptModule::OnLoad(const std::wstring& stFile)
		{
			m_pController->OnLoad(stFile);
		}

		void ScriptModule::OnSave(const std::wstring& stFile)
		{
			ScriptSaver saver(m_pController->GetResources());
			saver.Save(stFile);
		}

		void ScriptModule::OnNewScene(const std::wstring& stScene, const std::wstring& stFile)
		{
			file::DirCreate(L"Scripts");

			// make a valid class name out of scene name - convert to ANSCI & remove whitespaces
			std::string stSceneA = conv::ToA(stScene);
			std::string::iterator end_pos = std::remove(stSceneA.begin(), stSceneA.end(), ' ');
			stSceneA.erase(end_pos, stSceneA.end());

			const std::string stScript = "class " + stSceneA + " : IScene\n{\n\t" + stSceneA + "()\n\t{\n\t}\n\n\tvoid OnUninit()\n\t{\n\t}\n\n\tvoid OnUpdate(double dt)\n\t{\n\t}\n}";

			file::SaveFileA(L"Scripts/main.as", stScript);

			// save fake config file => TODO: real saving-routine
			xml::Doc doc;
			auto& root = doc.InsertNode(L"Scripts");
			root.ModifyAttribute(L"path", L"Scripts");

			auto& script = root.InsertNode(L"Script");
			script.ModifyAttribute(L"name", stScene);
			script.ModifyAttribute(L"file", L"main.as");

			doc.SaveFile(stFile);
		}

		void ScriptModule::OnSceneChanged(void)
		{
		}

		void ScriptModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void ScriptModule::OnClose(void)
		{
			m_pController->Clear();
		}

		void ScriptModule::OnBeginTest(void)
		{
		}

		void ScriptModule::OnEndTest(void)
		{
		}

	}
}

