#pragma once
#include "IModule.h"

namespace acl
{
	namespace gui
	{
		class MenuBar;
		class BaseController;
	}

	namespace script
	{
		struct Context;
	}

	namespace editor
	{

		class ScriptController;

		class ScriptModule :
			public IModule
		{
		public:
			ScriptModule(gui::BaseController& controller, gui::MenuBar& bar, const script::Context& script);
			~ScriptModule(void);

			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override;
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			ScriptController* m_pController;
		};

	}
}


