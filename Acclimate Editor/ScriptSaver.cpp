#include "ScriptSaver.h"
#include "Script\File.h"
#include "XML\Doc.h"

namespace acl
{
	namespace editor
	{

		ScriptSaver::ScriptSaver(const script::Scripts::Block& scripts) : m_pScripts(&scripts)
		{
		}

		void ScriptSaver::Save(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			
			auto& root = doc.InsertNode(L"Scripts");
			for(unsigned int i = 0; i < m_pScripts->Size(); i++)
			{
				auto& script = root.InsertNode(L"Script");
				auto pair = m_pScripts->GetPair(i);
				script.ModifyAttribute(L"name", pair.first);
				script.ModifyAttribute(L"file", pair.second->GetFile());
			}

			doc.SaveFile(stFilename);
		}

	}
}
