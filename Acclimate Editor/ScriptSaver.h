#pragma once
#include <vector>
#include "Script\Resources.h"

namespace acl
{
	namespace editor
	{

		class ScriptSaver
		{
		public:
			ScriptSaver(const script::Scripts::Block& scripts);

			void Save(const std::wstring& stFilename) const;

		private:

			const script::Scripts::Block* m_pScripts;
		};

	}
}

