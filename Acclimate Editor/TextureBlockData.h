#pragma once
#include "Gfx\Textures.h"

namespace acl
{
	namespace editor
	{

		struct TextureBlockData
		{
			TextureBlockData(void) : pBlock(nullptr), bLocked(false)
			{
			}

			TextureBlockData(gfx::Textures::Block& block, bool bLocked) : pBlock(&block),
			bLocked(bLocked)
			{
			}

			gfx::Textures::Block* pBlock;
			bool bLocked;
		};

	}
}

