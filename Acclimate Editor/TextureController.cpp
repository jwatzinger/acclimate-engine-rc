#include "TextureController.h"
#include "TextureImportController.h"
#include "TextureViewerController.h"
#include "TextureFormats.h"
#include "Utility.h"
#include "File\File.h"
#include "File\Dir.h"
#include "Gui\Texture.h"
#include "Gui\FileDialog.h"
#include "Gui\MsgBox.h"
#include "Gui\TreeView.h"
#include "Gui\ContextMenu.h"
#include "Gfx\ITextureLoader.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		TextureController::TextureController(gui::Module& module, gfx::Textures::Block& textures, const gfx::ITextureLoader& loader) :
			BaseController(module, L"../Editor/Menu/Asset/Textures.axm"), m_pLoader(&loader), m_pTextures(&textures),
			m_model(m_mTextures), m_pSelected(nullptr)
		{
			// texture list
			m_pTree = GetWidgetByName<gui::TreeView>(L"Textures");
			m_pTree->SetModel(&m_model);
			auto& context = AddWidget<gui::ContextMenu>(0.02f);
			auto& item = context.AddItem(L"Import texture");
			item.SigReleased.Connect(this, &TextureController::OnNewTexture);
			m_pTree->SetContextMenu(&context);

			// name box
			m_pNameBox = GetWidgetByName<gui::Textbox<>>(L"Name");

			// attributes
			m_pAttributes = GetWidgetByName<gui::Widget>(L"Attributes");

			// zoom button
			GetWidgetByName<gui::Widget>(L"Zoom")->SigReleased.Connect(this, &TextureController::OnZoom);

			// delete button
			m_pDelete = GetWidgetByName<gui::Widget>(L"Delete");
			m_pDelete->SigReleased.Connect(this, &TextureController::OnDeleteSelected);

			// import controller
			m_pTextureImport = &AddController<TextureImportController>();

			// model
			m_model.SigTextureSelected.Connect(this, &TextureController::OnPickTexture);

			AddBlock(L"Project", *m_pTextures, false);
		}

		TextureController::~TextureController(void)
		{
			m_pTree->SetModel(nullptr);
			m_pTextures->Clear();
		}

		void TextureController::Update(void)
		{
			BaseController::Update();

			m_model.Update();
		}

		void TextureController::AddBlock(const std::wstring& stName, gfx::Textures::Block& textures, bool bLocked)
		{
			if(!m_mTextures.count(stName) && !bLocked)
				m_pTextureImport->AddBlock(stName);

			m_mTextures[stName] = TextureBlockData(textures, bLocked);
		}

		void TextureController::Refresh(void)
		{
			OnRefreshList();
		}

		void TextureController::OnNewTexture(void)
		{
			gui::FileDialog dialog(gui::DialogType::OPEN);
			if(dialog.Execute())
			{
				while (true)
				{
					gfx::TextureLoadInfo info;
					std::wstring stBlock, stFile;
					bool copy;
					info.stPath = dialog.GetFullPath();

					if(m_pTextureImport->Execute(info, stBlock, copy, stFile))
					{
						auto pBlock = m_mTextures.at(stBlock).pBlock;

						if(pBlock->Has(info.stName))
						{
							gui::MsgBox messageBox(L"Error", (L"Texture with name \"" + info.stName + L"\" already exists.").c_str(), gui::IconType::WARNING);
							m_pModule->RegisterWidget(messageBox);
							messageBox.Execute();
						}
						else
						{
							// handle copy
							if(copy)
							{
								const auto& stBlockPath = pBlock->GetPath();
								file::DirCreate(stBlockPath);

								const auto& stTargetPath = stBlockPath + stFile;
								file::Copy(info.stPath, stTargetPath);
								info.stPath = stTargetPath;
							}

							// load texture
							pBlock->Begin();
							if (!m_pLoader->Load(info.stName, info.stPath, info.bRead, info.format))
							{
								gui::MsgBox messageBox(L"Error", (L"Failed to load texture from file " + info.stPath + L".").c_str(), gui::IconType::ALERT);
								m_pModule->RegisterWidget(messageBox);
								messageBox.Execute();
							}
							else
							{
								m_pTextureImport->OnLoadedTexture(info.stName);
								m_model.OnTexturesChanged();
								//m_pTree->SelectByName(info.stName);
							}
							pBlock->End();

							break;
						}
					}
					else
						break;
				}
			}

			m_pTree->OnFocus(true);
		}

		void TextureController::OnRefreshList(void)
		{
			m_model.OnTexturesChanged();
		}

		void TextureController::OnPickTexture(const gfx::ITexture* pTexture)
		{
			m_pSelected = pTexture;

			bool bTextureExists = pTexture != nullptr;

			if(bTextureExists)
			{
				if(const auto pInfo = pTexture->GetLoadInfo())
				{
					// name
					m_pNameBox->SetText(pInfo->stName);

					// size
					GetWidgetByName<gui::Label>(L"Size")->SetString(L"Size: " + conv::ToString(pTexture->GetSize().x) + L"x" + conv::ToString(pTexture->GetSize().y));

					// format
					const auto format = pInfo->format;
					const std::wstring stFormat = TextureFormatToString(pTexture->GetFormat());
					if(format == gfx::TextureFormats::UNKNOWN)
						GetWidgetByName<gui::Label>(L"Format")->SetString(L"Format: Auto(" + stFormat + L")");
					else
						GetWidgetByName<gui::Label>(L"Format")->SetString(L"Format: " + stFormat);

					GetWidgetByName<gui::Label>(L"File")->SetString(L"File: " + pInfo->stPath);
				}
				else
					bTextureExists = false;
			}

			SetAttributesVisible(bTextureExists);

			GetWidgetByName<gui::Texture>(L"Texture")->SetTexture(pTexture);
		}

		void TextureController::OnDeleteSelected(void)
		{
			//int id = m_pList->GetSelectedId();
			//if(id > -1)
			//{
			//	m_textures.Delete(id);
			//	OnRefreshList();
			//}
		}

		void TextureController::OnZoom(void)
		{
			TextureViewerController controller(*m_pModule, m_pTextures->Resources());
			controller.Execute(m_pSelected->GetLoadInfo()->stName);
		}

		void TextureController::SetAttributesVisible(bool bVisible) const
		{
			m_pDelete->SetVisible(bVisible);
			m_pNameBox->SetVisible(bVisible);
			m_pAttributes->SetVisible(bVisible);
		}

	}
}

