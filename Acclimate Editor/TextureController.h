#pragma once
#include "Gui\BaseController.h"
#include "TextureTreeModel.h"
#include "TextureBlockData.h"
#include "Gui\Textbox.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gfx
	{
		class ITextureLoader;
	}
	
	namespace gui
	{
		class TreeView;
	}

	namespace editor
	{

		class TextureImportController;

		class TextureController :
			public gui::BaseController
		{
			typedef std::map<std::wstring, TextureBlockData> TextureMap;
		public:

			TextureController(gui::Module& module, gfx::Textures::Block& textures, const gfx::ITextureLoader& loader);
			~TextureController(void);

			void Update(void) override;

			void AddBlock(const std::wstring& stName, gfx::Textures::Block& textures, bool bLocked);
			void Refresh(void);

		private:

			void OnNewTexture(void);
			void OnRefreshList(void);
			void OnPickTexture(const gfx::ITexture* pTexture);
			void OnDeleteSelected(void);
			void OnZoom(void);

			void SetAttributesVisible(bool bVisible) const;

			TextureMap m_mTextures;
			gfx::Textures::Block* m_pTextures;
			const gfx::ITextureLoader* m_pLoader;
			const gfx::ITexture* m_pSelected;

			gui::TreeView* m_pTree;
			gui::Textbox<>* m_pNameBox;
			gui::Widget* m_pDelete, *m_pAttributes;
			TextureTreeModel m_model;

			TextureImportController* m_pTextureImport;
		};

	}
}


