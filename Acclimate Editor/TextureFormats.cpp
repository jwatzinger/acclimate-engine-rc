#include "TextureFormats.h"

namespace acl
{
	namespace editor
	{

		std::wstring TextureFormatToString(gfx::TextureFormats format)
		{
			switch(format)
			{
			case gfx::TextureFormats::UNKNOWN:
				return L"Auto";
			case gfx::TextureFormats::X32:
				return L"R8G8B8";
			case gfx::TextureFormats::A32:
				return L"R8G8B8A8";
			case gfx::TextureFormats::L8:
				return L"L8";
			case gfx::TextureFormats::L16:
				return L"L16";
			case gfx::TextureFormats::R32:
				return L"R32";
			default:
				return L"Unsupported.";
			}
		}

		std::wstring TextureFormatToResourceString(gfx::TextureFormats format)
		{
			return L"Unimplemented";
		}

	}
}
