#pragma once
#include <string>
#include "Gfx\TextureFormats.h"

namespace acl
{
	namespace editor
	{

		const gfx::TextureFormats TextureFormats[] = 
		{
			gfx::TextureFormats::UNKNOWN,
			gfx::TextureFormats::X32,
			gfx::TextureFormats::A32,
			gfx::TextureFormats::L8,
			gfx::TextureFormats::L16,
			gfx::TextureFormats::R32
		};

		const unsigned int NumTextureFormats = sizeof(TextureFormats) / sizeof(gfx::TextureFormats);

		std::wstring TextureFormatToString(gfx::TextureFormats format);
		std::wstring TextureFormatToResourceString(gfx::TextureFormats format);

	}
}