#include "TextureImportController.h"
#include "TextureFormats.h"
#include "File\File.h"
#include "Gfx\ITexture.h"
#include "Gui\BaseWindow.h"
#include "Gui\CheckBox.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		TextureImportController::TextureImportController(gui::Module& module) :
			BaseController(module, L"../Editor/Menu/Asset/TextureImport.axm"), m_bConfirmed(false)
		{
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"ImportWindow");
			m_pWindow->SetAreaClipping(false);

			m_pTextbox = GetWidgetByName<gui::Textbox<>>(L"TargetName");
			m_pTextbox->SigConfirm.Connect(this, &TextureImportController::OnConfirm);
			m_pTextbox->SetFocusState(gui::FocusState::KEEP_FOCUS);
			m_pCheckBox = GetWidgetByName<gui::CheckBox>(L"Read");
			m_pCopyBox = GetWidgetByName<gui::CheckBox>(L"Copy");

			m_pFile = GetWidgetByName<gui::Textbox<>>(L"FileName");
			m_pFile->SigConfirm.Connect(this, &TextureImportController::OnConfirm);

			m_pFormatBox = &AddWidget<FormatBox>(0.05f, 95.0f, 0.9f, 22.0f, 10.0f);
			m_pFormatBox->SetPositionModes(gui::PositionMode::REL, gui::PositionMode::ABS, gui::PositionMode::REL, gui::PositionMode::ABS);
			for(unsigned int i = 0; i < NumTextureFormats; i++)
			{
				gfx::TextureFormats format = TextureFormats[i];
				m_pFormatBox->AddItem(TextureFormatToString(format), format);
			}
			m_pFormatBox->Select(0);

			m_pBlockBox = &AddWidget<BlockBox>(0.05f, 153.0f, 0.9f, 22.0f, 10.0f);
			m_pBlockBox->SetPositionModes(gui::PositionMode::REL, gui::PositionMode::ABS, gui::PositionMode::REL, gui::PositionMode::ABS);

			GetWidgetByName(L"OkButton")->SigReleased.Connect(this, &TextureImportController::OnConfirm);
			GetWidgetByName(L"CancelButton")->SigReleased.Connect(m_pWindow, &gui::BaseWindow::OnClose);
		}

		void TextureImportController::AddBlock(const std::wstring& stBlock)
		{
			m_pBlockBox->AddItem(stBlock, stBlock);
			if(m_pBlockBox->GetSize() == 1)
				m_pBlockBox->Select(0);
		}

		bool TextureImportController::Execute(gfx::TextureLoadInfo& info, std::wstring& stBlock, bool& copyToDir, std::wstring& stFile)
		{
			const auto& stExtention = file::Extention(info.stPath);
			m_pFile->SetText(file::SubExtention(file::FileName(info.stPath)));

			m_bConfirmed = false;
			m_pTextbox->OnFocus(true);
			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				info.stName = m_pTextbox->GetContent();
				info.format = m_pFormatBox->GetContent();
				info.bRead = m_pCheckBox->IsChecked();

				stBlock = m_pBlockBox->GetContent();

				copyToDir = m_pCopyBox->IsChecked();

				if(copyToDir)
					stFile = m_pFile->GetContent() + L"." + stExtention;
			}
			return m_bConfirmed;
		}

		void TextureImportController::OnLoadedTexture(const std::wstring& stName)
		{
			if(m_pTextbox->GetContent() == stName)
			{
				const size_t posLeftBracket = stName.find_last_of(L"(");
				const size_t posRightBracket = stName.find_last_of(L")");
				if(posLeftBracket != std::wstring::npos && posRightBracket == stName.size()-1 && 
					posLeftBracket + 1 < posRightBracket )
				{
					size_t id;
					conv::FromString(&id, stName.substr(posLeftBracket + 1, 1).c_str());
					id++;
					m_pTextbox->SetText(stName.substr(0, posLeftBracket) + L"(" + conv::ToString(id) + L")");
				}
				else
				{
					m_pTextbox->SetText(stName + L"(1)");
				}
			}
		}

		void TextureImportController::OnConfirm(void)
		{
			m_pWindow->OnClose();
			m_bConfirmed = true;
		}

	}
}

