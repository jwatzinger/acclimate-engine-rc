#pragma once
#include "Gui\BaseController.h"
#include "Gui\ComboBox.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
		class CheckBox;
	}

	namespace gfx
	{
		struct TextureLoadInfo;

		enum class TextureFormats;
	}

	namespace editor
	{

		class TextureImportController :
			public gui::BaseController
		{
			typedef gui::ComboBox<gfx::TextureFormats> FormatBox;
			typedef gui::ComboBox<std::wstring> BlockBox;
		public:

			TextureImportController(gui::Module& module);

			void AddBlock(const std::wstring& stBlock);
			bool Execute(gfx::TextureLoadInfo& info, std::wstring& stBlock, bool& copyToDir, std::wstring& stFile);

			void OnLoadedTexture(const std::wstring& stName);

		private:

			void OnConfirm(void);

			bool m_bConfirmed;

			FormatBox* m_pFormatBox;
			BlockBox* m_pBlockBox;
			gui::Textbox<>* m_pTextbox, *m_pFile;
			gui::BaseWindow* m_pWindow;
			gui::CheckBox* m_pCheckBox, *m_pCopyBox;
		};

	}
}


