#include "TexturePickController.h"
#include "Gui\Texture.h"
#include "Gui\BaseWindow.h"

namespace acl
{
	namespace editor
	{

		TexturePickController::TexturePickController(gui::Module& module, const gfx::Textures& textures):
			BaseController(module, L"../Editor/Menu/Asset/TexturePick.axm"), m_bConfirmed(false), m_pTextures(&textures)
		{
			// window
			m_pWindow = GetWidgetByName<gui::BaseWindow>(L"Window");

			// texture list
			m_pList = &AddWidget<TextureList>(0.0f, 0.0f, 0.3f, 0.9f, 0.02f);
			m_pList->SigItemPicked.Connect(this, &TexturePickController::OnPickTexture);
			m_pList->SigConfirm.Connect(this, &TexturePickController::OnConfirm);

			// confirm button
			GetWidgetByName<gui::Widget>(L"Pick")->SigReleased.Connect(this, &TexturePickController::OnConfirm);

			OnRefresh();
		}

		void TexturePickController::OnRefresh(void)
		{
			m_pList->Clear();

			for(auto& texture : m_pTextures->Map())
			{
				m_pList->AddItem(texture.first, texture.second);
			}

			if(m_pList->GetSize())
				m_pList->SelectById(0);
		}

		bool TexturePickController::Execute(const gfx::ITexture** pTexture, std::wstring* pName)
		{
			return Execute(L"", pTexture, pName);
		}

		bool TexturePickController::Execute(const std::wstring& stDefault, const gfx::ITexture** pTexture, std::wstring* pName)
		{
			m_bConfirmed = false;
			m_pList->OnFocus(true);
			if(!stDefault.empty())
				m_pList->SelectByName(stDefault);

			m_pWindow->Execute();
			if(m_bConfirmed)
			{
				if(pTexture)
					*pTexture = m_pList->GetContent();
				if(pName)
					*pName = m_pList->GetSelectedName();
			}
			return m_bConfirmed;
		}

		void TexturePickController::OnPickTexture(const gfx::ITexture* pTexture)
		{
			GetWidgetByName<gui::Texture>(L"Texture")->SetTexture(pTexture);
		}

		void TexturePickController::OnConfirm(void)
		{
			m_bConfirmed = true;
			m_pWindow->OnClose();
		}

	}
}

