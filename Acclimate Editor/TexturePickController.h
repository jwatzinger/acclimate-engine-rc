#pragma once
#include "Gui\BaseController.h"
#include "Gui\List.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gui
	{
		class BaseWindow;
	}

	namespace editor
	{

		class TexturePickController :
			public gui::BaseController
		{
			typedef gui::List<const gfx::ITexture*> TextureList;
		public:

			TexturePickController(gui::Module& module, const gfx::Textures& textures);

			bool Execute(const gfx::ITexture** pTexture, std::wstring* pName);
			bool Execute(const std::wstring& stDefault, const gfx::ITexture** pTexture, std::wstring* pName);

			void OnRefresh(void);

		private:

			void OnRefreshList(void);
			void OnPickTexture(const gfx::ITexture* pTexture);
			void OnConfirm(void);

			bool m_bConfirmed;

			gui::BaseWindow* m_pWindow;
			TextureList* m_pList;

			const gfx::Textures* m_pTextures;
		};

	}
}


