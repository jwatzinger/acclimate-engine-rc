#include "TextureTreeCallback.h"
#include "Gui\ContextMenu.h"
#include "System\Assert.h"

namespace acl
{
	namespace editor
	{

		TextureTreeCallback::TextureTreeCallback(const gfx::ITexture& texture, bool bLocked) : m_pTexture(&texture),
			m_bLocked(bLocked)
		{
		}

		const std::wstring* TextureTreeCallback::GetIconName(void) const
		{
			if(m_bLocked)
			{
				static const std::wstring stIcon = L"TextureLockedIcon";
				return &stIcon;
			}
			else
			{
				static const std::wstring stIcon = L"TextureIcon";
				return &stIcon;
			}
		}

		bool TextureTreeCallback::IsRenameable(void) const
		{
			return false;
		}

		bool TextureTreeCallback::IsDeletable(void) const
		{
			return !m_bLocked;
		}

		bool TextureTreeCallback::IsDefaultExpanded(void) const
		{
			return false;
		}

		void TextureTreeCallback::OnSelect(void)
		{
			SigSelect(*m_pTexture);
		}

		void TextureTreeCallback::OnDelete(void)
		{
			SigDelete(*m_pTexture);
		}

		gui::ContextMenu* TextureTreeCallback::OnContextMenu(void)
		{
			gui::ContextMenu* pEntityMenu = new gui::ContextMenu(0.02f);
			return pEntityMenu;
		}

		void TextureTreeCallback::OnContextMenuClose(gui::ContextMenu& menu)
		{
			delete &menu;
		}

		void TextureTreeCallback::OnRename(const std::wstring& stName)
		{
			SigRename(*m_pTexture, stName);
		}

	}
}