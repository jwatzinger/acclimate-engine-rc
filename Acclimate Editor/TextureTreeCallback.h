#pragma once
#include "Core\Signal.h"
#include "Gui\ITreeNodeCallback.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

	namespace editor
	{

		class TextureTreeCallback :
			public gui::ITreeNodeCallback
		{
		public:
			TextureTreeCallback(const gfx::ITexture& texture, bool bLocked);

			const std::wstring* GetIconName(void) const override;
			bool IsDeletable(void) const override;
			bool IsRenameable(void) const override;
			bool IsDefaultExpanded(void) const override;

			void OnSelect(void) override;
			void OnDelete(void) override;
			gui::ContextMenu* OnContextMenu(void) override;
			void OnContextMenuClose(gui::ContextMenu& menu) override;
			void OnRename(const std::wstring& stName) override;

			core::Signal<const gfx::ITexture&> SigSelect;
			core::Signal<const gfx::ITexture&> SigDelete;
			core::Signal<const gfx::ITexture&, const std::wstring&> SigRename;

		private:

			bool m_bLocked;
			const gfx::ITexture* m_pTexture;
		};

	}
}


