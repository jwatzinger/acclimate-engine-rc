#include "TextureTreeModel.h"
#include "FolderTreeCallback.h"
#include "TextureTreeCallback.h"

namespace acl
{
	namespace editor
	{

		TextureTreeModel::TextureTreeModel(const TextureMap& textures) : m_root(L"Textures"),
			m_bDirty(false), m_pTextures(&textures)
		{
		}

		const gui::Node& TextureTreeModel::GetRoot(void) const
		{
			return m_root;
		}

		core::Signal<>& TextureTreeModel::GetUpdateSignal(void)
		{
			return SigUpdated;
		}

		void TextureTreeModel::Update(void)
		{
			if(m_bDirty)
			{
				m_root.vChilds.clear();

				for(auto& textures : *m_pTextures)
				{
					gui::Node textureBlockNode(textures.first);
					auto pCallback = new FolderTreeCallback(textures.second.bLocked);
					textureBlockNode.pCallback = pCallback;

					const size_t size = textures.second.pBlock->Size();
					if(size)
					{
						for(unsigned int i = 0; i < size; i++)
						{
							auto& texture = textures.second.pBlock->GetPair(i);
							gui::Node component(texture.first);
							auto pTextureCallback = new TextureTreeCallback(*texture.second, textures.second.bLocked);
							pTextureCallback->SigDelete.Connect(this, &TextureTreeModel::OnDeleteTexture);
							pTextureCallback->SigSelect.Connect(this, &TextureTreeModel::OnSelectTexture);
							component.pCallback = pTextureCallback;
							textureBlockNode.vChilds.push_back(component);
						}

						m_root.vChilds.push_back(textureBlockNode);
					}
				}

				SigUpdated();

				m_bDirty = false;
			}
		}

		void TextureTreeModel::OnTexturesChanged(void)
		{
			m_bDirty = true;
		}

		void TextureTreeModel::OnDeleteTexture(const gfx::ITexture& texture)
		{

		}

		void TextureTreeModel::OnSelectTexture(const gfx::ITexture& texture)
		{
			SigTextureSelected(&texture);
		}

		//void EntityTreeModel::OnGotoEntity(ecs::Entity& entity)
		//{
		//	SigGotoEntity(entity);
		//}

		//void EntityTreeModel::OnSelectComponent(ecs::EntityHandle& entity, const ecs::BaseComponent& component)
		//{
		//	SigEntitySelected(&entity);
		//}

		//void EntityTreeModel::OnDeleteEntity(ecs::EntityHandle& entity)
		//{
		//	m_pEntities->RemoveEntity(entity);
		//	SigEntitySelected(nullptr);
		//	m_bDirty = true;
		//}

		//void EntityTreeModel::OnAttachComponents(ecs::Entity& entity)
		//{
		//	if(m_pAttach->Execute(entity))
		//		m_bDirty = true;
		//}

		//void EntityTreeModel::OnRenameEntity(ecs::Entity& entity, const std::wstring& stName)
		//{
		//	if(entity.GetName() != stName)
		//	{
		//		entity.SetName(stName);
		//		m_bDirty = true;
		//	}
		//}


	}
}

