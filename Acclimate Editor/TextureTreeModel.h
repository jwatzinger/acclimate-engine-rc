#pragma once
#include "Gui\ITreeModel.h"
#include <map>
#include "TextureBlockData.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace editor
	{

		class TextureTreeModel :
			public gui::ITreeModel
		{
			typedef std::map<std::wstring, TextureBlockData> TextureMap;
		public:
			TextureTreeModel(const TextureMap& textures);

			const gui::Node& GetRoot(void) const override;
			core::Signal<>& GetUpdateSignal(void) override;

			void Update(void);

			void OnTexturesChanged(void);

			core::Signal<const gfx::ITexture*> SigTextureSelected;

		private:

			void OnDeleteTexture(const gfx::ITexture& texture);
			void OnSelectTexture(const gfx::ITexture& texture);

			bool m_bDirty;
			const TextureMap* m_pTextures;

			gui::Node m_root;
			core::Signal<> SigUpdated;
		};

	}
}


