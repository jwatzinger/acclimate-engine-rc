#include "TextureViewerController.h"
#include "Gui\Window.h"
#include "Gui\Texture.h"
#include "Gfx\ITexture.h"
#include "System\Convert.h"

namespace acl
{
	namespace editor
	{

		TextureViewerController::TextureViewerController(gui::Module& module, const gfx::Textures& textures) : 
			BaseController(module, L"../Editor/Menu/Asset/TextureViewer.axm"), m_pTextures(&textures), m_zoomFactor(1.0f), m_stTextureName(L"")
		{
			m_pWindow = GetWidgetByName<gui::Window>(L"Viewer");

			m_pTexture = GetWidgetByName<gui::Texture>(L"Texture");
			m_pTexture->SigReleased.Connect(this, &TextureViewerController::OnZoomIn);
		}

		void TextureViewerController::Execute(const std::wstring& stTextureName)
		{
			m_zoomFactor = 1.0f;
			m_stTextureName = stTextureName;

			m_pTexture->SetTexture(m_pTextures->Get(m_stTextureName));
			Refresh();

			m_pWindow->Execute();
		}

		void TextureViewerController::Refresh(void) const
		{
			if(const gfx::ITexture* pTexture = m_pTexture->GetTexture())
			{
				m_pWindow->SetLabel(m_stTextureName + L": " + conv::ToString(m_zoomFactor, 0) + L"00%");

				const math::Vector2& vSize = pTexture->GetSize();
				m_pWindow->SetWidth(vSize.x * m_zoomFactor + 16);
				m_pWindow->SetHeight(vSize.y * m_zoomFactor + 32);
			}
			else
			{
				m_pWindow->SetWidth(256);
				m_pWindow->SetHeight(256);
				m_pWindow->SetLabel(m_stTextureName + L" (error: doesn't exist)");
			}
		}

		void TextureViewerController::OnZoomIn(void)
		{
			m_zoomFactor += 1.0f;
			
			Refresh();
		}

	}
}