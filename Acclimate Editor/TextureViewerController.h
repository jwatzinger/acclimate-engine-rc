#pragma once
#include "Gui\BaseController.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gui
	{
		class Window;
		class Texture;
	}

	namespace editor
	{

		class TextureViewerController :
			public gui::BaseController
		{
		public:
			TextureViewerController(gui::Module& module, const gfx::Textures& textures);

			void Execute(const std::wstring& stTextureName);

		private:

			void Refresh(void) const;
			void OnZoomIn(void);

			float m_zoomFactor;
			std::wstring m_stTextureName;

			gui::Window* m_pWindow;
			gui::Texture* m_pTexture;

			const gfx::Textures* m_pTextures;
		};

	}
}


