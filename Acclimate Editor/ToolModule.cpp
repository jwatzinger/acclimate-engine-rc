#include "ToolModule.h"
#include "PluginController.h"
#include "MeshConverterController.h"
#include "Gui\MenuBar.h"
#include "Gui\MenuItem.h"
#include "Gui\MenuOption.h"

namespace acl
{
	namespace editor
	{

		ToolModule::ToolModule(gui::BaseController& controller, gui::Module& module, gui::MenuBar& bar, core::ModuleManager& modules, render::IRenderer& render):
			m_pModule(&module), m_pRender(&render)
		{
			auto& item = bar.AddItem(L"TOOLS");

			// plugin controller
			m_pPlugins = &controller.AddController<PluginController>(modules);
			auto& option = item.AddOption(L"Plugins");
			option.SigReleased.Connect(m_pPlugins, &PluginController::OnToggle);
			option.SetShortcut(gui::ComboKeys::CTRL_ALT, 'P', gui::ShortcutState::ALWAYS);
			option.SigShortcut.Connect(m_pPlugins, &PluginController::OnToggle);

			// mesh converter
			auto& meshOption = item.AddOption(L"Mesh converter");
			meshOption.SigReleased.Connect(this, &ToolModule::OnMeshConverter);
			meshOption.SetShortcut(gui::ComboKeys::CTRL_ALT, 'I', gui::ShortcutState::ALWAYS);
			meshOption.SigShortcut.Connect(this, &ToolModule::OnMeshConverter);
		}

		const PluginController& ToolModule::GetPlugins(void) const
		{
			return *m_pPlugins;
		}

		void ToolModule::OnMeshConverter(void)
		{
			MeshConverterController controller(*m_pModule, *m_pRender);
			controller.Execute();
		}

		Project::ModuleType ToolModule::GetModuleType(void) const
		{
			return Project::ModuleType::MODULES;
		}

		void ToolModule::OnNew(const std::wstring& stFile)
		{
		}

		void ToolModule::OnSceneChanged(void)
		{
		}

		void ToolModule::OnSaveScene(const std::wstring& stFile)
		{
		}

		void ToolModule::Update(void)
		{
		}

		void ToolModule::OnLoad(const std::wstring& stFile)
		{
			m_pPlugins->OnLoad(stFile);
		}

		void ToolModule::OnSave(const std::wstring& stFile)
		{
			m_pPlugins->OnSave(stFile);
		}

		void ToolModule::OnClose(void)
		{
			m_pPlugins->OnClose();
		}

		void ToolModule::OnBeginTest(void)
		{
		}

		void ToolModule::OnEndTest(void)
		{
		}

	}
}
