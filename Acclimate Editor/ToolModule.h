#pragma once
#include "IModule.h"

namespace acl
{
	namespace core
	{
		class ModuleManager;
	}

	namespace gui
	{
		class MenuBar;
		class Module;
		class BaseController;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace editor
	{

		class PluginController;

		class ToolModule :
			public IModule
		{
		public:
			ToolModule(gui::BaseController& controller, gui::Module& module, gui::MenuBar& bar, core::ModuleManager& modules, render::IRenderer& render);

			const PluginController& GetPlugins(void) const;
			Project::ModuleType GetModuleType(void) const override;

			void Update(void) override;

			void OnNew(const std::wstring& stFile) override;
			void OnLoad(const std::wstring& stFile) override;
			void OnSave(const std::wstring& stFile) override;
			void OnNewScene(const std::wstring& stScene, const std::wstring& stFile) override {}
			void OnSceneChanged(void) override;
			void OnSaveScene(const std::wstring& stFile) override;
			void OnClose(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			void OnMeshConverter(void);

			gui::Module* m_pModule;
			render::IRenderer* m_pRender;

			PluginController* m_pPlugins;
		};

	}
}


