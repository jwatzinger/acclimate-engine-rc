#pragma once
#include <string>

namespace acl
{
	namespace editor
	{

		const std::wstring& GetEditorDirectory(void);

		void setProjectDirectory(const std::wstring& stName);
		const std::wstring& getProjectDirectory(void);

	}
}