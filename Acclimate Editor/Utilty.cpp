#include "Utility.h"
#include "System\WorkingDirectory.h"

namespace acl
{
	namespace editor
	{
		namespace detail
		{
			std::wstring _stProjectDir;
		}

		const std::wstring& GetEditorDirectory(void)
		{
			static std::wstring stPath;
			static bool bInit = false;
			if(!bInit)
			{
				sys::WorkingDirectory dir;
				stPath = dir.GetOldDirectory();
				const size_t pos = stPath.find_last_of(L"\\");
				stPath = stPath.substr(0, pos);
				stPath += L"\\Editor\\";
				bInit = true;
			}
			return stPath;
		}

		void setProjectDirectory(const std::wstring& stName)
		{
			detail::_stProjectDir = stName;
		}

		const std::wstring& getProjectDirectory(void)
		{
			return detail::_stProjectDir;
		}

	}
}