#include "XMesh.h"

namespace acl
{
	namespace editor
	{

		HRESULT STDMETHODCALLTYPE LoadUserData::LoadTopLevelData(LPD3DXFILEDATA pXofChildData)
		{
			int i = 0;
			return S_OK;
		}

		HRESULT STDMETHODCALLTYPE LoadUserData::LoadFrameChildData(LPD3DXFRAME pFrame, LPD3DXFILEDATA pXofChildData)
		{
			int i = 0;
			return S_OK;
		}

		HRESULT STDMETHODCALLTYPE LoadUserData::LoadMeshChildData(LPD3DXMESHCONTAINER pMeshContainer, LPD3DXFILEDATA pXofChildData)
		{
			int i = 0;
			return S_OK;
		}

		HRESULT STDMETHODCALLTYPE AllocMeshHierarchy::CreateFrame(LPCSTR Name, D3DXFRAME** ppNewFrame)
		{
			if(ppNewFrame)
			{
				D3DXFRAME* pFrame = new D3DXFRAME;
				ZeroMemory(pFrame, sizeof(D3DXFRAME));

				if(Name)
				{
					const size_t nameLength = strlen(Name)+1;
					pFrame->Name = new char[nameLength];
					strcpy_s(pFrame->Name, nameLength, Name);
				}
				else
				{
					pFrame->Name = new char[1];
					pFrame->Name[0] = '\0';
				}

				*ppNewFrame = pFrame;
			}
			
			return D3D_OK;
		}
			
		HRESULT STDMETHODCALLTYPE AllocMeshHierarchy::CreateMeshContainer(PCSTR Name,
			const D3DXMESHDATA* pMeshData,
			const D3DXMATERIAL* pMaterials,
			const D3DXEFFECTINSTANCE* pEffectInstances,
			DWORD NumMaterials,
			const DWORD *pAdjacency,
			ID3DXSkinInfo* pSkinInfo,
			D3DXMESHCONTAINER** ppNewMeshContainer)
		{
			if(pMeshData->Type != D3DXMESHTYPE_MESH)
				return E_FAIL;

			D3DXMESHCONTAINER* pMeshContainer = new D3DXMESHCONTAINER;
			ZeroMemory(pMeshContainer, sizeof(D3DXMESHCONTAINER));
			pMeshContainer->MeshData.Type = D3DXMESHTYPE_MESH;

			const size_t nameLength = strlen(Name)+1;
			pMeshContainer->Name = new char[nameLength];
			strcpy_s(pMeshContainer->Name, nameLength, Name);

			const size_t numVertices = pMeshData->pMesh->GetNumFaces()*3;
			pMeshContainer->pAdjacency = new DWORD[numVertices];
			memcpy(pMeshContainer->pAdjacency, pAdjacency, sizeof(DWORD)*numVertices);

			pMeshContainer->MeshData.pMesh = pMeshData->pMesh;
			pMeshContainer->MeshData.pMesh->AddRef();

			pMeshContainer->NumMaterials = NumMaterials;
			pMeshContainer->pMaterials = new D3DXMATERIAL[NumMaterials];
			for(unsigned int i = 0; i < NumMaterials; i++)
			{
				pMeshContainer->pMaterials[i].MatD3D = pMaterials[i].MatD3D;

				if(pMaterials[i].pTextureFilename)
				{
					const size_t materialLength = strlen(pMaterials[i].pTextureFilename) + 1;
					pMeshContainer->pMaterials[i].pTextureFilename = new char[materialLength];
					strcpy_s(pMeshContainer->pMaterials[i].pTextureFilename, materialLength, pMaterials[i].pTextureFilename);
				}
				else
				{
					pMeshContainer->pMaterials[i].pTextureFilename = new char[1];
					pMeshContainer->pMaterials[i].pTextureFilename[0] = '\0';
				}
			}

			if(pSkinInfo)
			{
				pMeshContainer->pSkinInfo = pSkinInfo;
				pSkinInfo->AddRef();

				DWORD maxVertices, numCombination;
				LPD3DXBUFFER lpBuffer = nullptr;
				LPD3DXMESH pMesh = nullptr;
				pSkinInfo->ConvertToIndexedBlendedMesh(pMeshContainer->MeshData.pMesh,
					D3DXMESH_MANAGED | D3DXMESHOPT_VERTEXCACHE,
					pSkinInfo->GetNumBones(),
					pMeshContainer->pAdjacency,
					nullptr,
					nullptr,
					nullptr,
					&maxVertices,
					&numCombination,
					&lpBuffer,
					&pMesh);

				pMeshContainer->MeshData.pMesh->Release();
				pMeshContainer->MeshData.pMesh = pMesh;
			}
			
			*ppNewMeshContainer = pMeshContainer;

			return D3D_OK;
		}

		HRESULT STDMETHODCALLTYPE AllocMeshHierarchy::DestroyFrame(THIS_ D3DXFRAME* pFrameToFree)
		{
			delete[] pFrameToFree->Name;
			delete pFrameToFree;

			return D3D_OK;
		}

		HRESULT STDMETHODCALLTYPE AllocMeshHierarchy::DestroyMeshContainer(THIS_ D3DXMESHCONTAINER* pMeshContainerBase)
		{
			if(pMeshContainerBase->pSkinInfo)
				pMeshContainerBase->pSkinInfo->Release();
			if(pMeshContainerBase->MeshData.pMesh)
				pMeshContainerBase->MeshData.pMesh->Release();

			delete[] pMeshContainerBase->Name;
			delete[] pMeshContainerBase->pAdjacency;
			delete[] pMeshContainerBase->pMaterials;

			for(unsigned int i = 0; i < pMeshContainerBase->NumMaterials; i++)
			{
				delete pMeshContainerBase->pMaterials[i].pTextureFilename;
			}

			delete pMeshContainerBase;

			return D3D_OK;
		}

	}
}