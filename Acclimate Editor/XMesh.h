#pragma once
#include <d3dx9.h>

namespace acl
{
	namespace editor
	{

		class LoadUserData:
			public ID3DXLoadUserData
		{
			HRESULT STDMETHODCALLTYPE LoadTopLevelData(LPD3DXFILEDATA pXofChildData) override;
			HRESULT STDMETHODCALLTYPE LoadFrameChildData(LPD3DXFRAME pFrame, LPD3DXFILEDATA pXofChildData) override;
			HRESULT STDMETHODCALLTYPE LoadMeshChildData(LPD3DXMESHCONTAINER pMeshContainer, LPD3DXFILEDATA pXofChildData) override;
		};

		class AllocMeshHierarchy :
			public ID3DXAllocateHierarchy
		{
			public
			:
				HRESULT STDMETHODCALLTYPE CreateFrame(LPCSTR Name, D3DXFRAME** ppNewFrame) override;
				HRESULT STDMETHODCALLTYPE CreateMeshContainer(PCSTR Name, 
																const D3DXMESHDATA* pMeshData, 
																const D3DXMATERIAL* pMaterials, 
																const D3DXEFFECTINSTANCE* pEffectInstances,
																DWORD NumMaterials,
																const DWORD *pAdjacency,
																ID3DXSkinInfo* pSkinInfo,
																D3DXMESHCONTAINER** ppNewMeshContainer) override;
				HRESULT STDMETHODCALLTYPE DestroyFrame(THIS_ D3DXFRAME* pFrameToFree) override;
				HRESULT STDMETHODCALLTYPE DestroyMeshContainer(THIS_ D3DXMESHCONTAINER* pMeshContainerBase) override;
		};

	}
}