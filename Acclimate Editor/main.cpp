#include "Engine.h"
#include "MainState.h"
#include "Utility.h"

using namespace acl;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	editor::GetEditorDirectory(); //todo: fix need for calling this at start of program to init the return value correctly

	BaseEngine& engine = createEngine(hInstance);
	engine.Run<editor::MainState>(engine);
	delete& engine;

	return 0;
}
