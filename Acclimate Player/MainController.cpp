#include "MainController.h"
#include "Gui\Image.h"
#include "Gui\Label.h"
#include "Script\Core.h"

#include "System\Log.h"

namespace acl
{
	namespace player
	{

		MainController::MainController(gui::Module& module, script::Core& script) : BaseController(module, L"Menu/Game.axm"),
			m_pScript(&script)
		{
			m_pImage = GetWidgetByName<gui::Image>(L"GameView");
		}

		void MainController::SetGameView(const std::wstring& stTexture)
		{
			m_pImage->SetFileName(stTexture);
		}	

		void MainController::Update(void)
		{
			m_pImage->MarkDirtyRect();
		}

	}
}