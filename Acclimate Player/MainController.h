#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace gui
	{
		class Image;
	}

	namespace script
	{
		class Core;
	}

	namespace player
	{
		class MainController :
			public gui::BaseController
		{
		public:
			MainController(gui::Module& module, script::Core& script);

			void SetGameView(const std::wstring& stTexture);

			void Update(void) override;

		private:

			gui::Image* m_pImage;
			script::Core* m_pScript;
		};

	}
}

