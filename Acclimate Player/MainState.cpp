#include "MainState.h"
#include "MainController.h"
#include "Audio\ILoader.h"
#include "Core\SettingLoader.h"
#include "Core\SettingModule.h"
#include "Core\ConfigLoader.h"
#include "Core\ModuleLoader.h"
#include "Core\SceneLoader.h"
#include "Core\LocalizationLoader.h"
#include "Entity\PrefabLoader.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\Screen.h"
#include "Gui\LayoutLoader.h"
#include "Input\ILoader.h"
#include "Script\Core.h"
#include "Script\Loader.h"
#include "System\WorkingDirectory.h"

#include <fstream>

namespace acl
{
	namespace player
	{
		namespace detail
		{
			MainController* _pController = nullptr;

			void SetGameView(const std::wstring& stTexture)
			{
				_pController->SetGameView(stTexture);
			}
		}

		MainState::MainState(const core::GameStateContext& ctx) : BaseState(ctx)
		{
			// load project directory
			std::wifstream file(L"../debug.project");

			if(file.is_open())
			{
				// get length of file:
				file.seekg(0, std::ios::end);
				const long length = (long)file.tellg();
				file.seekg(0, std::ios::beg);

				wchar_t* pDir = new wchar_t[length+1];
				file.read(pDir, length);
				pDir[length] = '\0';

				sys::setDirectory(pDir);

				delete[] pDir;
			}

			sys::setDirectory(L"../Game/");

			/*******************************************
			* LOADING
			*******************************************/
			
			// load settings
			m_ctx.core.settingLoader.Load(L"Data/Settings.axm");

			// load modules
			core::ModuleLoader moduleLoader(*ctx.core.pModules);
			moduleLoader.Load(L"Data/Modules.axm");

			sys::setDirectory(L"../bin");
			HandleSettings();
			sys::setDirectory(L"../Game");

			// load prefabs
			ecs::PrefabLoader prefabLoader(ctx.ecs.prefabs);
			prefabLoader.Load(L"Data/Prefabs.axm");

			// load localization
			try
			{
				core::LocalizationLoader loader(ctx.core.localization);
				loader.Load(L"Data/Localization.axm");
			}
			catch(...)
			{
				sys::log->Out(sys::LogModule::API, sys::LogType::WARNING, "No localized strings were loaded.");
			}

			// load resources
			ctx.gfx.load.pLoader->Load(L"Data/Resources.axm");

			// load sounds
			ctx.audio.loader.Load(L"Data/Audio.axm");

			// load layout
			ctx.gui.loader.Load(L"Menu/Game.axl");

			// load scenes
			ctx.core.sceneLoader.Load(L"Data/Scenes.axm");

			// load input
			ctx.input.loader.Load(L"Data/Input.axm");

			// create controller
			detail::_pController = new MainController(ctx.gui.module, ctx.script.core);

			/*******************************************
			* SCRIPT
			*******************************************/

			// bind game view registration method
			ctx.script.core.RegisterGlobalFunction("void setGameView(const string& in)", asFUNCTION(detail::SetGameView));

			// load scripts
			ctx.script.loader.FromConfig(L"Data/Scripts.axm");

			// call script init
			auto function = ctx.script.core.GetGlobalFunction("void startUp()");
			function.Call<void>();
		}

		MainState::~MainState(void)
		{
			delete detail::_pController;
			detail::_pController = nullptr;
		}

		core::IState* MainState::Run(double dt)
		{
			auto update = m_ctx.script.core.GetGlobalFunction("void update(double)");
			update.Call<void>(dt);

			detail::_pController->Update();

			return m_pCurrentState;
		}

		void MainState::Render(void) const
		{
		}

		void MainState::HandleSettings(void) const
		{
			auto& settings = m_ctx.core.settings;

			// load config
			core::ConfigLoader configLoader(settings);
			configLoader.Load(L"Config.axm");

			auto* pScreenX = settings.GetSetting(L"screenX");
			auto* pScreenY = settings.GetSetting(L"screenY");

			auto vSize = m_ctx.gfx.screen.GetSize();

			if(!pScreenX || !pScreenY)
				return;

			if(pScreenX->GetData<unsigned int>() != 0 && pScreenY->GetData<unsigned int>() != 0)
			{
				math::Vector2 vNewSize;
				vNewSize = math::Vector2(pScreenX->GetData<unsigned int>(), pScreenY->GetData<unsigned int>());

				if(vSize.x < vNewSize.x || vSize.y < vNewSize.y)
				{
					pScreenX->Set<unsigned int>(vSize.x);
					pScreenY->Set<unsigned int>(vSize.y);
					settings.Refresh();
					return;
				}
				else
					m_ctx.gfx.screen.Resize(vNewSize);
			}
			else
			{
				pScreenX->Set<unsigned int>(vSize.x);
				pScreenY->Set<unsigned int>(vSize.y);
				settings.Refresh();
			}
		}

	}
}
