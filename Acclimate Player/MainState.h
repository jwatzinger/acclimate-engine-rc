#pragma once
#include "Core\BaseState.h"

namespace acl
{
	namespace player
	{

		class MainState :
			public core::BaseState
		{
		public:
			MainState(const core::GameStateContext& ctx);
			~MainState(void);

			core::IState* Run(double dt) override;
			void Render(void) const override;

		private:

			void HandleSettings(void) const;
		};

	}
}