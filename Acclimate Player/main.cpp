//#define ACL_DEBUG_LEAKS

#ifdef ACL_DEBUG_LEAKS
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "Engine.h"
#include "MainState.h"

using namespace acl;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef ACL_DEBUG_LEAKS
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);

	//_CrtSetBreakAlloc(108877);
#endif

	BaseEngine& engine = createEngine(hInstance);
	engine.Run<player::MainState>();
	delete &engine;

	return 0;
}
