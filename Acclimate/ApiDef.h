#pragma once

//#define USE_API_DX9
#define USE_API_DX11
//#define USE_API_GL4

#ifdef USE_API_DX9
#include "DX9\Def.h"
#elif defined(USE_API_DX11)
#include "DX11\Def.h"
#elif defined(USE_API_GL4)
#include "GL4\Def.h"
#else
#error "No api used."
#endif

#if defined(USE_API_DX9) && defined(USE_API_DX11) && defined(USE_API_GL4) || \
	defined(USE_API_DX9) && defined(USE_API_DX11) || \
	defined(USE_API_DX9) && defined(USE_API_GL4) || \
	defined(USE_API_DX11) && defined(USE_API_GL4)
#error "Can only use one graphics API at a time."
#endif