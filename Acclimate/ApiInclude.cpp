#include "ApiInclude.h"
#include "ApiDef.h"

namespace acl
{

	GraphicAPI getUsedAPI(void)
	{
#ifdef ACL_API_DX9
		return GraphicAPI::DX9;
#elif defined(ACL_API_DX11)
		return GraphicAPI::DX11;
#elif defined(ACL_API_GL4)
		return GraphicAPI::GL4;
#endif
	}

}