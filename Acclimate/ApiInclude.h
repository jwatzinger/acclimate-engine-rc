#pragma once
#include "Core\Dll.h"

namespace acl
{
	enum class GraphicAPI
	{
		DX9, DX11, GL4
	};

	ACCLIMATE_API GraphicAPI getUsedAPI(void);
}