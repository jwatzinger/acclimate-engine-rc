#pragma once
#include "Files.h"

namespace acl
{
	namespace audio
	{

		class Device;
		class ILoader;

		struct Context
		{
			Context(Device& device, Files& files, const ILoader& loader): device(device), files(files), loader(loader) {}

			Device& device;
			Files& files;
			const ILoader& loader;
		};

	}
}