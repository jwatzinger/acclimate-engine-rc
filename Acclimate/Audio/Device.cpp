#include "Device.h"
#include "Listener.h"
#include "..\System\Exception.h"

#pragma comment(lib, "x3daudio.lib")

namespace acl
{
	namespace audio
	{

		Device::Device(void): m_pXAudio2(nullptr), m_pMasterVoice(nullptr), m_pListener(nullptr), m_dspSettings()
		{
			CoInitializeEx(NULL, COINIT_MULTITHREADED);

			// base setup
			if(FAILED( XAudio2Create(&m_pXAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR) ))
				throw audioException();

			if(FAILED( m_pXAudio2->CreateMasteringVoice(&m_pMasterVoice) ))
				throw audioException();

			// 3d setup
			XAUDIO2_DEVICE_DETAILS deviceDetails;
			m_pXAudio2->GetDeviceDetails(0, &deviceDetails);
			DWORD channelMask = deviceDetails.OutputFormat.dwChannelMask;

			X3DAudioInitialize(channelMask, X3DAUDIO_SPEED_OF_SOUND, m_x3dInstance);

			// init dsp settings
			m_dspSettings.SrcChannelCount = 2;
			m_dspSettings.DstChannelCount = deviceDetails.OutputFormat.Format.nChannels;
			m_dspSettings.pMatrixCoefficients = new float[m_dspSettings.SrcChannelCount*m_dspSettings.DstChannelCount*2];
		}

		Device::~Device(void)
		{
			delete[] m_dspSettings.pMatrixCoefficients;
			m_pMasterVoice->DestroyVoice();
			m_pXAudio2->Release();

			CoUninitialize();
		}

		IXAudio2SourceVoice* Device::CreateSourceVoice(const WAVEFORMATEX& sourceFormat) const
		{
			IXAudio2SourceVoice* pVoice = nullptr;
			if(FAILED( m_pXAudio2->CreateSourceVoice(&pVoice, &sourceFormat) ))
				throw audioException();

			return pVoice;
		}

		void Device::SetListener(const Listener* pListener)
		{
			m_pListener = pListener;
		}

		const X3DAUDIO_DSP_SETTINGS& Device::Calculate3DSound(const X3DAUDIO_EMITTER& emitter)
		{
			if(m_pListener)
				X3DAudioCalculate(m_x3dInstance, &m_pListener->GetListener(), &emitter, X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_DOPPLER | X3DAUDIO_CALCULATE_LPF_DIRECT | X3DAUDIO_CALCULATE_REVERB, &m_dspSettings);

			return m_dspSettings;
		}



	}
}
