#pragma once
#include <XAudio2.h>
#include <X3DAudio.h>
#include "..\Core\Dll.h"

namespace acl
{
	namespace audio
	{

		class Listener;

		class ACCLIMATE_API Device
		{
		public:
			Device(void);
			~Device(void);

			IXAudio2SourceVoice* CreateSourceVoice(const WAVEFORMATEX& sourceFormat) const;

			void SetListener(const Listener* pListener);

			const X3DAUDIO_DSP_SETTINGS& Calculate3DSound(const X3DAUDIO_EMITTER& emitter);

		private:

			IXAudio2* m_pXAudio2;
			IXAudio2MasteringVoice* m_pMasterVoice;

			X3DAUDIO_HANDLE m_x3dInstance;
			X3DAUDIO_DSP_SETTINGS m_dspSettings;

			const Listener* m_pListener;

		};

	}
}

