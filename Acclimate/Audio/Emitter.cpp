#include "Emitter.h"
#include "Device.h"
#include "File.h"
#include "IAudioStream.h"
#include <math.h>

namespace acl
{
	namespace audio
	{

		Emitter::Emitter(Device& device, const File& file, bool bLoop, float volume) : m_pDevice(&device), m_pFile(&file),
			m_vLastPosition(0.0f, 0.0f, 0.0f), m_emitter(), m_pStream(nullptr)
		{
			auto pFormat = m_pFile->GetFormat();
			m_pVoice = m_pDevice->CreateSourceVoice(*pFormat);
			m_pVoice->SetVolume(volume);

			if(auto pStream = file.GetStream())
				m_pStream = &pStream->Clone();
			else
			{
				XAUDIO2_BUFFER buffer = { 0 };
				buffer.pAudioData = m_pFile->GetBuffer();
				buffer.Flags = XAUDIO2_END_OF_STREAM;
				buffer.AudioBytes = m_pFile->GetSize();

				// set loop
				if(bLoop)
					buffer.LoopCount = XAUDIO2_LOOP_INFINITE;

				m_pVoice->SubmitSourceBuffer(&buffer);
			}

			// init 3d emitter
			m_emitter.OrientFront = *(X3DAUDIO_VECTOR*)&math::Vector3(1.0f, 0.0f, 0.0f);
			m_emitter.OrientTop = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 1.0f, 0.0f);
			m_emitter.Velocity = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 0.0f, 0.0f);
			m_emitter.ChannelCount = pFormat->nChannels;
			m_emitter.CurveDistanceScaler = 1.0f;
			if(m_emitter.ChannelCount > 1)
			{
				m_emitter.pChannelAzimuths = new float[m_emitter.ChannelCount];
				m_emitter.pChannelAzimuths[0] = 1.0f;
				m_emitter.pChannelAzimuths[1] = 1.0f;
			}
		}

		Emitter::~Emitter(void)
		{
			delete[] m_emitter.pChannelAzimuths;
			m_pVoice->DestroyVoice();
		}

		void Emitter::Play(void)
		{
			m_pVoice->Start();
		}

		void Emitter::Stop(void)
		{
			m_pVoice->Stop();
		}

		void Emitter::Update(double dt)
		{
			if(m_pStream)
				m_pStream->Update(dt, *m_pVoice);
		}

		bool Emitter::IsPlaying(void)
		{
			XAUDIO2_VOICE_STATE state;
			m_pVoice->GetState(&state);
			return state.BuffersQueued > 0;
		}

		void Emitter::SetPosition(const math::Vector3* pPosition)
		{
			if(pPosition)
			{
				m_emitter.Position = *(X3DAUDIO_VECTOR*)pPosition;
				m_emitter.Velocity = *(X3DAUDIO_VECTOR*)&((*pPosition) - m_vLastPosition);
				m_vLastPosition = *pPosition;

				const X3DAUDIO_DSP_SETTINGS& settings = m_pDevice->Calculate3DSound(m_emitter);

				m_pVoice->SetOutputMatrix(nullptr, settings.SrcChannelCount, settings.DstChannelCount, settings.pMatrixCoefficients);
				m_pVoice->SetFrequencyRatio(settings.DopplerFactor);

				XAUDIO2_FILTER_PARAMETERS FilterParametersDirect = { LowPassFilter, 2.0f * sin(X3DAUDIO_PI/6.0f * settings.LPFDirectCoefficient), 1.0f }; // see XAudio2CutoffFrequencyToRadians() in XAudio2.h for more information on the formula used here
				m_pVoice->SetOutputFilterParameters(nullptr, &FilterParametersDirect);
			}
			else
			{
			}
		}

		void Emitter::SetVolume(float volume)
		{
			m_pVoice->SetVolume(volume);
		}

	}
}