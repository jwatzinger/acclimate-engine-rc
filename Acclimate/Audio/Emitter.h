#pragma once
#include <XAudio2.h>
#include <X3DAudio.h>
#include "..\Core\Dll.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace math
	{
		struct Vector3;
	}

	namespace audio
	{

		class Device;
		class File;
		class IAudioStream;

		class ACCLIMATE_API Emitter
		{
		public:
			Emitter(Device& device, const File& file, bool bLoop, float volume);
			~Emitter(void);

			void Play(void);
			void Stop(void);
			bool IsPlaying(void);
			void Update(double dt);

			void SetPosition(const math::Vector3* pPosition);
			void SetVolume(float volume);

		private:

			Device* m_pDevice;
			const File* m_pFile;
			IAudioStream* m_pStream;

			math::Vector3 m_vLastPosition;

			IXAudio2SourceVoice* m_pVoice;

			X3DAUDIO_EMITTER m_emitter;
		};

	}
}

