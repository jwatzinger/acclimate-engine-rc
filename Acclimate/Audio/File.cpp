#include "File.h"

namespace acl
{
	namespace audio
	{

		File::File(const std::wstring& stPath, const WAVEFORMATEX* pSourceFormat, const BYTE* pBuffer, const unsigned int size) : m_pSourceFormat(pSourceFormat), m_pBuffer(pBuffer),
			m_size(size), m_pStream(nullptr), m_stPath(stPath)
		{
		}

		File::File(const std::wstring& stPath, const WAVEFORMATEX* pSourceFormat, const IAudioStream& stream) : m_pSourceFormat(pSourceFormat), m_pBuffer(nullptr),
			m_size(0), m_pStream(&stream), m_stPath(stPath)
		{
		}

		File::~File(void)
		{
			delete m_pSourceFormat;
			delete[] m_pBuffer;
			delete m_pStream;
		}
		
		const IAudioStream* File::GetStream(void) const
		{
			return m_pStream;
		}

		const WAVEFORMATEX* File::GetFormat(void) const
		{
			return m_pSourceFormat;
		}

		const BYTE* File::GetBuffer(void) const
		{
			return m_pBuffer;
		}

		unsigned int File::GetSize(void) const
		{
			return m_size;
		}

		const std::wstring& File::GetPath(void) const
		{
			return m_stPath;
		}

	}
}
