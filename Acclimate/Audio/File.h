#pragma once
#include <XAudio2.h>
#include <string>

namespace acl
{
	namespace audio
	{

		class IAudioStream;

		class File
		{
		public:
			File(const std::wstring& stPath, const WAVEFORMATEX* pSourceFormat, const BYTE* pBuffer, const unsigned int size);
			File(const std::wstring& stPath, const WAVEFORMATEX* pSourceFormat, const IAudioStream& stream);
			~File(void);

			const IAudioStream* GetStream(void) const;
			const WAVEFORMATEX* GetFormat(void) const;
			const BYTE* GetBuffer(void) const;
			unsigned int GetSize(void) const;
			const std::wstring& GetPath(void) const;

		private:

			const IAudioStream* m_pStream;
			const WAVEFORMATEX* m_pSourceFormat;
			const BYTE* m_pBuffer;
			const unsigned int m_size;
			const std::wstring m_stPath;
		};

	}
}
