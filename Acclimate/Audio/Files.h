#pragma once
#include <string>
#include "File.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace audio
    {

        typedef core::Resources<std::wstring, File> Files;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, File>;

    }
}