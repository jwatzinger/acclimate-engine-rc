#pragma once
#include <XAudio2.h>

namespace acl
{
	namespace audio
	{

		class IAudioStream
		{
		public:

			virtual ~IAudioStream(void) = 0 {};

			virtual IAudioStream& Clone(void) const = 0;

			virtual void Update(double dt, IXAudio2SourceVoice& voice) = 0;
		};

	}
}