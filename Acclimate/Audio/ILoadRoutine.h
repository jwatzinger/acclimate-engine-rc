#pragma once
#include <string>

namespace acl
{
	namespace audio
	{
		
		class File;

		class ILoadRoutine
		{
		public:

			virtual ~ILoadRoutine(void) = 0 {}

			virtual File* Load(const std::wstring& stFilename) const = 0;
		};

	}
}