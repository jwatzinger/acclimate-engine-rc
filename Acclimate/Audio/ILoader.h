#pragma once
#include <string>

namespace acl
{
	namespace audio
	{

		class File;
		
		class ILoader
		{
		public:

			virtual void Load(const std::wstring& stFilename) const = 0;

			virtual File* LoadFile(const std::wstring& stFilename, const std::wstring& stName) const = 0;
		};

	}
}