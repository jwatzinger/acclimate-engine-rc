#include "Listener.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace audio
	{

		Listener::Listener(const X3DAUDIO_CONE& cone): m_vLastPosition(0.0f, 0.0f, 0.0f)
		{
			m_listener.Position = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 0.0f, 0.0f);
			m_listener.OrientFront = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 0.0f, 1.0f);
			m_listener.OrientTop = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 1.0f, 0.0f);
			m_listener.Velocity = *(X3DAUDIO_VECTOR*)&math::Vector3(0.0f, 0.0f, 0.0f);
			m_listener.pCone = (X3DAUDIO_CONE*)&cone;
		}

		const X3DAUDIO_LISTENER& Listener::GetListener(void) const
		{
			return m_listener;
		}

		void Listener::Update(const math::Vector3& vPosition, const math::Vector3& vOrientation)
		{
			m_listener.Position = *(X3DAUDIO_VECTOR*)&vPosition;
			m_listener.Velocity = *(X3DAUDIO_VECTOR*)&(vPosition - m_vLastPosition);
			
			math::Vector3 vUp(0.0f, 1.0f, 0.0f), vRight;
			vRight = vOrientation.Cross(vUp);
			vUp = vRight.Cross(vOrientation);

			m_listener.OrientFront = *(X3DAUDIO_VECTOR*)&vOrientation;
			m_listener.OrientTop = *(X3DAUDIO_VECTOR*)&vUp;

			m_vLastPosition = vPosition;
		}

	}
}