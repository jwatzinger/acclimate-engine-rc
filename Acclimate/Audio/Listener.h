#pragma once
#include <XAudio2.h>
#include <X3DAudio.h>
#include "..\Core\Dll.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace audio
	{

		static const X3DAUDIO_CONE listenerDefaultCone = { X3DAUDIO_PI*5.0f/6.0f, X3DAUDIO_PI*11.0f/6.0f, 1.0f, 0.75f, 0.0f, 0.25f, 0.708f, 1.0f };

		class ACCLIMATE_API Listener
		{
		public:

			Listener(const X3DAUDIO_CONE& cone = listenerDefaultCone);
			
			const X3DAUDIO_LISTENER& GetListener(void) const;

			void Update(const math::Vector3& vPosition, const math::Vector3& vOrientation);

		private:

			X3DAUDIO_LISTENER m_listener;

			math::Vector3 m_vLastPosition;
		};

	}
}
