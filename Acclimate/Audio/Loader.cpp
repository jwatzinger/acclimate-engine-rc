#include "Loader.h"
#include "WaveLoadRoutine.h"
#include "SPC\SPCLoadRoutine.h"
#include "..\File\File.h"
#include "..\System\WorkingDirectory.h"
#include "..\System\Log.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace audio
	{

		Loader::Loader(Files& files): m_files(files)
		{
		}

		void Loader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			sys::WorkingDirectory dir(stFilename, true);

			if(const xml::Node* pSounds = doc.Root(L"Sounds"))
			{
				sys::WorkingDirectory dir2;

				if(auto pPath = pSounds->Attribute(L"path"))
					dir2 = sys::WorkingDirectory(pPath->GetValue());

				//loop through sound list
				if(const xml::Node::NodeVector* pSoundList = pSounds->Nodes(L"Sound"))
                {
				    for(auto pSound : *pSoundList)
				    {
						const std::wstring& stFile = pSound->Attribute(L"file")->GetValue();

						LoadFile(file::FullPath(stFile), pSound->GetValue());
					}
				}
			}
		}

		File* Loader::LoadFile(const std::wstring& stFilename, const std::wstring& stName) const
		{
			File* pFile = nullptr;

			const auto stFormat = file::Extention(stFilename);

			if(stFormat == L"spc")
			{
				SPCLoadRoutine routine;
				pFile = routine.Load(stFilename);
			}
			else if(stFormat == L"wav")
			{
				WaveLoadRoutine waveLoader;
				pFile = waveLoader.Load(stFilename);
			}
			else
				sys::log->Out(sys::LogModule::AUDIO, sys::LogType::WARNING, "failed to load audio file", stFilename, ". Invalid format", stFormat);

			if(!pFile)
				sys::log->Out(sys::LogModule::AUDIO, sys::LogType::WARNING, "failed to load audio file", stFilename);
			else
				m_files.Add(stName, *pFile);

			return pFile;
		}

	}
}