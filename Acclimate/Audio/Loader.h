#pragma once
#include "ILoader.h"
#include "Files.h"

namespace acl
{
	namespace audio
	{

		class Loader :
			public ILoader
		{
		public:

			Loader(Files& files);
			
			void Load(const std::wstring& stFilename) const override;

			File* LoadFile(const std::wstring& stFilename, const std::wstring& stName) const override;

		private:

			Files& m_files;
		};

	}
}

