#include "Package.h"

namespace acl
{
	namespace audio
	{

		Package::Package(void): m_loader(m_files), m_context(m_device, m_files, m_loader)
		{
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

	}
}