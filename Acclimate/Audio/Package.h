#pragma once
#include "Device.h"
#include "Files.h"
#include "Loader.h"
#include "Context.h"

namespace acl
{
	namespace audio
	{

		class ILoader;

		class Package
		{
		public:
			Package(void);
			
			const Context& GetContext(void) const;

		private:

			Device m_device;
			Files m_files;
			Loader m_loader;
			Context m_context;
		};

	}
}
