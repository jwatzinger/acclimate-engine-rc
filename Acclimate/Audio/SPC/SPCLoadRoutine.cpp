#include "SPCLoadRoutine.h"
#include "spc.h"
#include "SPCStream.h"
#include "..\..\File\File.h"

#include "..\File.h"

namespace acl
{
	namespace audio
	{

		File* SPCLoadRoutine::Load(const std::wstring& stFilename) const
		{
			auto stFile = file::LoadFileA(stFilename);

			WAVEFORMATEX* pFormat = new WAVEFORMATEX;
			pFormat->nChannels = 2;
			pFormat->nSamplesPerSec = spc_sample_rate;
			pFormat->wBitsPerSample = 16;
			pFormat->nAvgBytesPerSec = (pFormat->wBitsPerSample * pFormat->nSamplesPerSec) / 4;
			pFormat->wFormatTag = 1;
			pFormat->cbSize = 0;
			pFormat->nBlockAlign = 4;

			SPCStream* pStream = new SPCStream(stFile);

			return new File(stFilename, pFormat, *pStream);
		}

	}
}
