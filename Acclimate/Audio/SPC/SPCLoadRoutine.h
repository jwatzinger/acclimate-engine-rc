#pragma once
#include "..\ILoadRoutine.h"

namespace acl
{
	namespace audio
	{

		class SPCLoadRoutine :
			public audio::ILoadRoutine
		{
		public:
			
			File* Load(const std::wstring& stFilename) const;
		};

	}
}
