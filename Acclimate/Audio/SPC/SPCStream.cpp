#include "SPCStream.h"
#include "spc.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace audio
	{

		SPCStream::SPCStream(const std::string& stFileContent) : m_bufferSize(15 * spc_sample_rate),
			m_currentBufferFill(0), m_pCurrentBuffer(nullptr),
			m_time(0.0f), m_stFile(stFileContent)
		{
			m_pSpc = spc_new();
			m_pFilter = spc_filter_new();

			/* Load SPC data into emulator */
			spc_load_spc(m_pSpc, stFileContent.data(), stFileContent.size());

			/* Most SPC files have garbage data in the echo buffer, so clear that */
			spc_clear_echo(m_pSpc);

			/* Clear filter before playing */
			spc_filter_clear(m_pFilter);

			m_pBuffer[0] = new short[m_bufferSize];
			m_pBuffer[1] = new short[m_bufferSize];
			m_pBuffer[2] = new short[m_bufferSize];

			for(unsigned int i = 0; i < NUM_BUFFERS; i++)
			{
				m_vEmptyBuffers.push_back(m_pBuffer[i]);
			}
		}

		SPCStream::SPCStream(const SPCStream& stream) : SPCStream(stream.m_stFile)
		{
		}

		SPCStream::~SPCStream(void)
		{
			spc_filter_delete(m_pFilter);
			spc_delete(m_pSpc);

			for(unsigned int i = 0; i < NUM_BUFFERS; i++)
			{
				delete m_pBuffer[i];
			}
		}

		IAudioStream& SPCStream::Clone(void) const
		{
			return *new SPCStream(*this);
		}

		void SPCStream::Update(double dt, IXAudio2SourceVoice& voice)
		{
			XAUDIO2_VOICE_STATE state;
			voice.GetState(&state);

			m_time += (float)dt;
			if(m_time >= 1.0f)
			{
				if(m_currentBufferFill != m_bufferSize)
					WriteBuffer((int)m_time * spc_sample_rate * 4);

				m_time = 0.0f;
			}
			
			if(state.BuffersQueued <= 1)
			{
				if(!m_vUsedBuffers.empty())
				{
					FinishedUsing();
					if(state.BuffersQueued == 0)
						FinishedUsing();
				}

				if(m_vFullBuffers.empty())
				{
					WriteBuffer(m_bufferSize);
					if(state.BuffersQueued == 0)
						WriteBuffer(m_bufferSize);
				}

				SubmitFullBuffers(voice);
			}
		}

		void SPCStream::WriteBuffer(unsigned int size)
		{
			if(!m_pCurrentBuffer)
			{
				if(m_vEmptyBuffers.empty())
					return;
				else
				{
					m_pCurrentBuffer = m_vEmptyBuffers.back();
					m_vEmptyBuffers.pop_back();
				}
			}

			if(m_currentBufferFill + size > m_bufferSize)
				size = m_bufferSize - m_currentBufferFill;

			spc_play(m_pSpc, size, &m_pCurrentBuffer[m_currentBufferFill]);
			spc_filter_run(m_pFilter, &m_pCurrentBuffer[m_currentBufferFill], size);

			m_currentBufferFill += size;
			if(m_currentBufferFill == m_bufferSize)
			{
				m_vFullBuffers.push_back(m_pCurrentBuffer);
				m_currentBufferFill = 0;
				m_pCurrentBuffer = nullptr;
			}
		}

		void SPCStream::SubmitFullBuffers(IXAudio2SourceVoice& voice)
		{
			for(auto pBuffer : m_vFullBuffers)
			{
				XAUDIO2_BUFFER buffer = { 0 };
				buffer.pAudioData = (const BYTE*)pBuffer;
				buffer.Flags = 0;
				buffer.AudioBytes = m_bufferSize * 2;

				voice.SubmitSourceBuffer(&buffer);
				m_vUsedBuffers.push_back(pBuffer);
			}
			m_vFullBuffers.clear();
		}

		void SPCStream::FinishedUsing(void)
		{
			ACL_ASSERT(!m_vUsedBuffers.empty());

			m_vEmptyBuffers.push_back(m_vUsedBuffers.front());
			m_vUsedBuffers.erase(m_vUsedBuffers.begin());
		}

	}
}

