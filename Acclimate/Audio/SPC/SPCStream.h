#pragma once
#include "..\IAudioStream.h"
#include <vector>
#include <string>

struct SNES_SPC;
struct SPC_Filter;

namespace acl
{
	namespace audio
	{

		class SPCStream final :
			public IAudioStream
		{
			typedef std::vector<short*> BufferVector;

			static const unsigned int NUM_BUFFERS = 3;
		public:
			SPCStream(const std::string& stFileContent);
			SPCStream(const SPCStream& stream);
			~SPCStream(void);

			IAudioStream& Clone(void) const override;

			void Update(double dt, IXAudio2SourceVoice& voice) override;

		private:

			void WriteBuffer(unsigned int size);
			void SubmitFullBuffers(IXAudio2SourceVoice& voice);
			void FinishedUsing(void);

			SNES_SPC* m_pSpc;
			SPC_Filter* m_pFilter;

			const unsigned int m_bufferSize;
			unsigned int m_currentBufferFill;
			BufferVector m_vFullBuffers, m_vEmptyBuffers, m_vUsedBuffers;
			short* m_pCurrentBuffer;
			short* m_pBuffer[NUM_BUFFERS];
			float m_time;

			std::string m_stFile;
		};

	}
}

