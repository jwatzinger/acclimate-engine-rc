#include "Saver.h"
#include "..\File\File.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace audio
	{
		Saver::Saver(const Files& files) : m_pFiles(&files)
		{
		}

		Saver::~Saver()
		{
		}

		void Saver::Save(const std::wstring& stFile) const
		{
			xml::Doc doc;

			sys::WorkingDirectory dir(stFile, true);

			auto& root = doc.InsertNode(L"Sounds");
			// TODO: un-hardcode
			root.ModifyAttribute(L"path", L"../Sounds");

			dir = sys::WorkingDirectory(L"../Sounds");

			for(auto& file : m_pFiles->Map())
			{
				auto& soundNode = root.InsertNode(L"Sound");
				soundNode.ModifyAttribute(L"file", file::RelativeForSubPath(dir.GetDirectory(), file.second->GetPath()));
				soundNode.SetValue(file.first);
			}

			doc.SaveFile(stFile);
		}

	}
}

