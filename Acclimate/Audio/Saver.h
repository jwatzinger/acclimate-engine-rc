#pragma once
#include "Files.h"

namespace acl
{
	namespace audio
	{

		class ACCLIMATE_API Saver
		{
		public:
			Saver(const Files& files);
			~Saver();

			void Save(const std::wstring& stFile) const;

		private:

			const Files* m_pFiles;
		};

	}
}

