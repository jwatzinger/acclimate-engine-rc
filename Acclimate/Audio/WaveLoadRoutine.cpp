#include "WaveLoadRoutine.h"
#include "File.h"
#include "wave.h"

namespace acl
{
	namespace audio
	{

		File* WaveLoadRoutine::Load(const std::wstring& stFilename) const
		{
			WCHAR strFilePath[MAX_PATH];
			int i = 0;
			for(auto c : stFilename)
			{
				strFilePath[i++] = c;
			}
			strFilePath[i] = '\0';

			CWaveFile wav;
			if( FAILED(wav.Open( strFilePath, nullptr, WAVEFILE_READ ) ) )
				return nullptr;

			WAVEFORMATEX* pWfx = wav.GetFormat();
			WAVEFORMATEX* pCpyFormat = new WAVEFORMATEX;
			memcpy(pCpyFormat, pWfx, sizeof(WAVEFORMATEX));

			DWORD cbWaveSize = wav.GetSize();
			BYTE* pbWaveData = new BYTE[ cbWaveSize ];

			if( FAILED(wav.Read( pbWaveData, cbWaveSize, &cbWaveSize ) ) )
				return nullptr;

			return new File(stFilename, pCpyFormat, pbWaveData, cbWaveSize); 
		}

	}
}
