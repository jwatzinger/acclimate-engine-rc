#include "BaseEngine.h"

#include <time.h>
#include "Core\Window.h"
#include "Core\ModuleLoader.h"
#include "Core\BaseContext.h"
#include "Core\ModuleManager.h"
#include "Core\Context.h"
#include "Core\SceneManager.h"
#include "Core\SceneLoader.h"
#include "Core\Package.h"
#include "Gfx\Screen.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\ICbufferLoader.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\IEffectLoader.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IFontLoader.h"
#include "Gfx\IGeometryCreator.h"
#include "Gfx\ILine.h"
#include "Gfx\IText.h"
#include "Gfx\ISprite.h"
#include "Gfx\ISpriteBatch.h"
#include "Gfx\BaseModelLoader.h"
#include "Gfx\AxmLoader.h"
#include "System\Exception.h"
#include "System\Log.h"
#include "Render\AxmLoader.h"
#include "Render\Context.h"
#include "Render\IRenderer.h"
#include "Render\Postprocessor.h"
#include "Math\Vector.h"

// script bindings
#include "Core\RegisterScript.h"
#include "Entity\RegisterScript.h"
#include "Gfx\RegisterScript.h"
#include "Gui\RegisterScript.h"
#include "Input\RegisterScript.h"
#include "Math\RegisterScript.h"
#include "System\RegisterScript.h"
#include "Physics\RegisterScript.h"

// event bindings
#include "Core\RegisterEvents.h"
#include "Entity\RegisterEvents.h"
#include "Input\RegisterEvents.h"
#include "System\RegisterEvents.h"

namespace acl
{

	BaseEngine::BaseEngine(HINSTANCE hInstance) : m_windowModule(hInstance), m_bQuit(false), m_bMessage(false), m_pStates(nullptr),
		m_gfxResources(m_textures, m_effects, m_meshes, m_materials, m_fonts, m_models, m_zbuffers, m_animations),
		m_guiPackage(m_fonts, m_textures), m_pCbufferLoader(nullptr), m_pText(nullptr), m_pLine(nullptr), m_pSprite(nullptr), 
		m_animationLoader(m_animations), m_updateModules(true)
	{
		sys::log->Out(sys::LogModule::CORE, sys::LogType::INFO, "Creating window...");

		m_pWindow = m_windowModule.NewMainWindow(L"Acclimate Engine DX11");
	}

	BaseEngine::~BaseEngine(void)
	{
		m_ecsPackage.Clear();
		m_scriptPackage.GetContext().core.DiscardModule();

		delete m_pCorePackage;
		delete m_pStates;

		delete m_pRenderStateHandler;
		delete m_pCtx;
		delete m_pCbufferLoader;
		delete m_pMeshLoader;
		delete m_pMaterialLoader;
		delete m_pModelLoader;
		delete m_pFontLoader;
		delete m_pTextureLoader;
		delete m_pEffectLoader;
		delete m_pGeometryCreator;
		delete m_pGfxContext;
		delete m_pGfxLoad;
		delete m_pLine;
		delete m_pText;
		delete m_pSprite;
		delete m_pSpriteBatch;
		delete m_pFullscreenEffect;
		delete m_pRenderContext;
		delete m_pRenderLoader;
		delete m_pPostProcessor;
		delete m_pRenderer;
	}

	void BaseEngine::Setup(void)
	{
		srand ( (unsigned int)time(nullptr) );

		// allow engines to setup their API
		auto vModes = OnSetupAPI();
		ACL_ASSERT(!vModes.empty());
		m_pScreen = new gfx::Screen(vModes, vModes.size()-1);

		/******************************************************
		* Common GFX
		******************************************************/

		gfx::RenderStateHandler::SetInstance(*m_pRenderStateHandler);

		m_pModelLoader = new gfx::BaseModelLoader(m_materials, m_meshes, m_models, *m_pCbufferLoader);
		m_pGfxLoad = new gfx::LoadContext(*m_pTextureLoader, *m_pEffectLoader, *m_pMeshLoader, *m_pMaterialLoader, *m_pFontLoader, *m_pModelLoader, *m_pZbufferLoader, *m_pGeometryCreator, m_animationLoader);
		m_pResourceLoader = new gfx::AxmLoader(*m_pScreen, *m_pGfxLoad);
		m_pGfxLoad->pLoader = m_pResourceLoader;

		// fullscreen effect
		m_pFullscreenEffect = new gfx::FullscreenEffect(m_textures, *m_pModelLoader, *m_pRenderer);

		m_pGfxContext = new gfx::Context(m_gfxResources, *m_pGfxLoad, *m_pSprite, *m_pText, *m_pSpriteBatch, *m_pScreen, *m_pFullscreenEffect, *m_pLine);

		/******************************************************
		* GUI
		******************************************************/

		// prepare context
		m_guiPackage.InitRenderer(*m_pGfxContext);

		const gui::Context& guiContext = m_guiPackage.GetContext();
		guiContext.module.SigMessageLoop.Connect(this, &BaseEngine::OnGuiMessageLoop);
		guiContext.module.SigMessageLoopEnd.Connect(this, &BaseEngine::OnGuiMessageLoopEnd);

		/******************************************************
		* Render
		******************************************************/

		// render loader
		m_pPostProcessor = new render::Postprocessor(m_textures, *m_pTextureLoader, m_effects, *m_pScreen, *m_pCbufferLoader, *m_pRenderer);
		m_pRenderLoader = new render::AxmLoader(*m_pRenderer, *m_pScreen, m_textures, m_zbuffers);
		m_pRenderContext = new render::Context(*m_pRenderer, *m_pRenderLoader, *m_pPostProcessor);

		/******************************************************
		* Core
		******************************************************/

		auto& audioContext = m_audioPackage.GetContext();
		auto& scriptContext = m_scriptPackage.GetContext();
		auto& entityContext = m_ecsPackage.GetContext();
		auto& physicsContext = m_physicsPackage.GetContext();
		auto& inputContext = m_inputPackage.GetContext();

		m_pCorePackage = new core::Package(*m_pGfxContext, entityContext, scriptContext, physicsContext, inputContext);
		auto& coreContext = m_pCorePackage->GetContext();

		coreContext.settings.SigUpdateSettings.Connect(this, &BaseEngine::OnSettingsChanged);

		m_pCtx = new core::GameStateContext(audioContext, guiContext, *m_pGfxContext, entityContext, inputContext, *m_pRenderContext, scriptContext, coreContext, physicsContext);

		// module manager
		m_pCorePackage->SetupModuleManager(*m_pCtx);

		/******************************************************
		* Register scripts
		******************************************************/

		sys::RegisterScript(scriptContext.core);
		ecs::RegisterScript(entityContext, scriptContext.core);
		input::RegisterScript(scriptContext.core);
		core::RegisterScript(scriptContext.core, coreContext.settings, coreContext.scenes);
		math::RegisterScript(scriptContext.core);
		gfx::RegisterScript(scriptContext.core);
		physics::RegisterScript(scriptContext.core, physicsContext.world);
		gui::RegisterScript(scriptContext.core, guiContext.module, *guiContext.pRenderer);

		/******************************************************
		* Register events
		******************************************************/

		core::registerEvents(coreContext);
		ecs::registerEvents(entityContext);
		input::registerEvents();
		sys::registerEvents();

		m_scriptPackage.FinishSDKRegistration();
	}

	void BaseEngine::AllowModuleUpdate(bool update)
	{
		m_updateModules = update;
	}

	void BaseEngine::OnGuiMessageLoop(void)
	{
		Render();
		m_inputPackage.Update();
	}

	void BaseEngine::OnGuiMessageLoopEnd(void)
	{
		m_pRenderer->Finish();
	}

	void BaseEngine::GameLoop(void)
	{
		while(!m_bQuit)
		{
			MessageLoop();
			if(!m_bMessage)
			{    
				const double dt = m_tGameLoopTimer.Duration();
				m_tGameLoopTimer.Reset();

				if(GetForegroundWindow() == m_pWindow->GethWnd())
				{
					if(!m_bQuit)
						Render();

					Update(dt);

					m_pRenderer->Finish();
				}
			}
		}
	}

	void BaseEngine::MessageLoop(void)
	{
		MSG msg = { 0 };
		//if there is an message
		if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if(msg.message == WM_QUIT)
				m_bQuit = true;
			m_bMessage = true;
		}
		else
			m_bMessage = false;
	}

	void BaseEngine::Update(double dt)
	{
		m_inputPackage.Update();

		UpdatePhysics(dt);

		m_ecsPackage.Update();

		auto& coreContext = m_pCorePackage->GetContext();
		coreContext.scenes.Update(dt);
		if(m_updateModules)
			coreContext.pModules->UpdateModules(dt);

		m_guiPackage.Update();

		if(!m_pStates->Run(dt))
			m_bQuit = true;
	}

	void BaseEngine::UpdatePhysics(double dt)
	{
		static const float timeStep = 1.0f/60.0f;
		static float accumulator = 0.0f;

		if(dt > 0.2f)
			dt = 0.2f;

		accumulator += dt;

		while(accumulator >= timeStep)
		{
			m_physicsPackage.Update(timeStep);
			accumulator -= timeStep;

			// check for deleted entities
			m_ecsPackage.Update();
		}
	}

	void BaseEngine::Render(void)
	{
		m_pRenderer->Prepare();

		m_pCorePackage->GetContext().pModules->RenderModules();
		m_pStates->Render();
		m_pPostProcessor->Execute();
		m_guiPackage.Render();

		m_pRenderer->Execute();
	}

	void BaseEngine::OnSettingsChanged(const core::SettingModule& settings)
	{
		auto pScreenX = settings.GetSetting(L"screenX");
		auto pScreenY = settings.GetSetting(L"screenY");
		auto pIsFullscreen = settings.GetSetting(L"fullscreen");

		if(pScreenX && pScreenY && pIsFullscreen)
		{
			math::Vector2 vNewScreenSize = math::Vector2(pScreenX->GetData<unsigned int>(), pScreenY->GetData<unsigned int>());

			if(!m_pScreen->IsValidSize(vNewScreenSize))
				return;

			math::Vector2 vOldSize;
			if(OnScreenResize(vNewScreenSize, vOldSize, pIsFullscreen->GetData<bool>()))
			{
				for(auto& texture : m_textures.Map())
				{
					if(texture.second->GetSize() == vOldSize)
						texture.second->Resize(vNewScreenSize);
				}

				for(auto& zbuffer : m_zbuffers.Map())
				{
					if(zbuffer.second->GetSize() == vOldSize)
						zbuffer.second->Resize(vNewScreenSize);
				}

				m_pRenderer->OnResizeScreen(vOldSize, vNewScreenSize);

				m_pCtx->gui.module.SetScreenSize(vNewScreenSize);

				m_pScreen->Resize(vNewScreenSize);
			}
		}
	}

}