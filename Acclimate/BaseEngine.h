#pragma once
#include "Audio\Package.h"
#include "Core\Dll.h"
#include "Core\WindowModule.h"
#include "Core\Timer.h"
#include "Core\BaseMachine.h"
#include "Entity\Package.h"
#include "Gfx\Context.h"
#include "Gfx\Animations.h"
#include "Gfx\AnimationLoader.h"
#include "Gfx\VideoMode.h"
#include "Gfx\RenderStateHandler.h"
#include "Gui\Package.h"
#include "Input\Package.h"
#include "Script\Package.h"
#include "Physics\Package.h"

namespace acl
{
	namespace core
	{
		class IState;
		struct GameStateContext;
		class Package;
		class SettingModule;
	}

	namespace gfx
	{
		class ILine;
        class IFontWriter;
		class ICbufferLoader;
		class FullscreenEffect;
		class Screen;
	}

	namespace render
	{
		class IRenderer;
		class ILoader;
		class Postprocessor;
		struct Context;
	}

	class ACCLIMATE_API BaseEngine
	{
	public:
		BaseEngine(HINSTANCE hInstance);
		virtual ~BaseEngine(void);

		template<typename S, typename... Args>
		void Run(Args&&... args);

		void AllowModuleUpdate(bool update);

		void OnGuiMessageLoop(void);
		void OnGuiMessageLoopEnd(void);
		void OnModuleUpdate(void);

	protected:

		typedef std::vector<gfx::VideoMode> VideoModeVector;

		virtual VideoModeVector OnSetupAPI(void) = 0;
		virtual bool OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen) = 0;

		core::Window* m_pWindow;
		gfx::Screen* m_pScreen;
		gfx::ResourceContext m_gfxResources;
		gfx::ICbufferLoader* m_pCbufferLoader;
		gfx::IEffectLoader* m_pEffectLoader;
		gfx::ITextureLoader* m_pTextureLoader;
		gfx::IMeshLoader* m_pMeshLoader;
		gfx::IMaterialLoader* m_pMaterialLoader;
		gfx::IFontLoader* m_pFontLoader;
		gfx::IModelLoader* m_pModelLoader;
		gfx::IZBufferLoader* m_pZbufferLoader;
		gfx::IGeometryCreator* m_pGeometryCreator;
		gfx::RenderStateHandler::IInstance* m_pRenderStateHandler;
		gfx::Textures m_textures;
		gfx::Effects m_effects;
		gfx::Materials m_materials;
		gfx::Meshes m_meshes;
		gfx::Fonts m_fonts;
		gfx::Models m_models;
		gfx::ZBuffers m_zbuffers;
		gfx::Animations m_animations;
		gfx::AnimationLoader m_animationLoader;
		gfx::ISprite* m_pSprite;
		gfx::ISpriteBatch* m_pSpriteBatch;
		gfx::IText* m_pText;
		gfx::ILine* m_pLine;
		gui::Package m_guiPackage;
		render::IRenderer* m_pRenderer;

	private:
#pragma warning( disable: 4251 )

		void Setup(void);

		void GameLoop(void);
		void MessageLoop(void);

		void Update(double dt);
		void UpdatePhysics(double dt);
		void Render(void);

		void OnSettingsChanged(const core::SettingModule& settings);

		bool m_bQuit, m_bMessage, m_updateModules;

		audio::Package m_audioPackage;
		core::Timer m_tGameLoopTimer;
		core::WindowModule m_windowModule;	
		core::BaseMachine* m_pStates;
		core::GameStateContext* m_pCtx;
		gfx::IResourceLoader* m_pResourceLoader;
		gfx::Context* m_pGfxContext;
		gfx::LoadContext* m_pGfxLoad;
		gfx::FullscreenEffect* m_pFullscreenEffect;
		render::Context* m_pRenderContext;
		render::ILoader* m_pRenderLoader;
		render::Postprocessor* m_pPostProcessor;

		core::Package* m_pCorePackage;
		script::Package m_scriptPackage;
		ecs::Package m_ecsPackage;
		input::Package m_inputPackage;
		physics::Package m_physicsPackage;

#pragma warning( default: 4251 )
			
	};

	template<typename S, typename... Args>
	void BaseEngine::Run(Args&&... args)
	{
		if(!m_pStates)
			Setup();
		else
			delete m_pStates;

		// create state and start statemachine
		core::IState* pState = new S(*m_pCtx, args...);
		m_pStates = new core::BaseMachine(pState);

		m_tGameLoopTimer.Reset();

		//enter gameloop
		GameLoop();
	}

}