#pragma once
#include "Context.h"
#include "..\Audio\Context.h"
#include "..\Core\Dll.h"
#include "..\Entity\Context.h"
#include "..\Gfx\Context.h"
#include "..\Gui\Context.h"
#include "..\Input\Context.h"
#include "..\Render\Context.h"
#include "..\Script\Context.h"
#include "..\Physics\Context.h"

namespace acl
{	
    namespace core
    {
        /// Packs resources for a game state
		/** Implements the context pattern for the BaseState, by storing all resources
        *   needed by a game state to function. */

        struct ACCLIMATE_API GameStateContext
        {
            GameStateContext(const audio::Context& audio, const gui::Context& gui, gfx::Context& gfx, const ecs::Context& ecs, const input::Context& input, render::Context& render, const script::Context& script, const Context& core, const physics::Context& physics): 
				audio(audio), gui(gui), gfx(gfx), ecs(ecs), input(input), render(render), core(core), script(script), physics(physics)
			{
			}

            const ecs::Context& ecs;
			const audio::Context& audio;
			const gui::Context& gui;
            gfx::Context& gfx;
			const input::Context& input;
            render::Context& render;
			const script::Context& script;
			const Context& core;
			const physics::Context& physics;
        };

    }
}