#include "BaseEvents.h"
#include "EventDeclaration.h"
#include "..\Script\Instance.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace core
	{

		/*****************************************
		* Condition
		******************************************/

		void Condition::Reset(void)
		{
		}

		void Condition::OnExecute(double dt)
		{
			if(GetAttribute<bool>(0))
				CallOutput(0);
			else
				CallOutput(1);
		}

		const EventDeclaration& Condition::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"condition", AttributeType::BOOL, false }
			};

			const core::OutputVector vOutputs = { L"True", L"False" };

			static const EventDeclaration decl(L"Condition", vAttributes, AttributeVector(), vOutputs);

			return decl;
		}

		/*****************************************
		* BlockingCondition
		******************************************/

		BlockingCondition::BlockingCondition(void) : m_nowTime(0.0f)
		{
		}

		void BlockingCondition::Reset(void)
		{
			m_nowTime = 0.0f;
		}

		void BlockingCondition::OnExecute(double dt)
		{
			if(GetAttribute<bool>(0))
				CallOutput(0);
			else
			{
				auto& timeOut = GetAttribute<float>(1);
				// in case condition is not met, check for timeout
				if(timeOut > 0.0f)
				{
					m_nowTime += dt;
					if(m_nowTime >= timeOut)
						CallOutput(1);
				}
			}
		}

		const EventDeclaration& BlockingCondition::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"condition", AttributeType::BOOL, false },
				{ L"timeout", AttributeType::FLOAT, false }
			};

			const core::OutputVector vOutputs = { L"True", L"False" };

			static const EventDeclaration decl(L"BlockingCondition", vAttributes, AttributeVector(), vOutputs);

			return decl;
		}

		/*****************************************
		* Wait
		******************************************/

		Wait::Wait(void) : m_nowTime(0.0f)
		{
		}

		Wait::Wait(const Wait& wait) :
			BaseEvent(wait), m_nowTime(wait.m_nowTime)
		{
		}

		void Wait::Reset(void)
		{
			m_nowTime = 0.0f;
		}

		void Wait::OnExecute(double dt)
		{
			m_nowTime += dt;
			if(m_nowTime >= GetAttribute<float>(0))
				CallOutput(0);
		}

		const EventDeclaration& Wait::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"time", AttributeType::FLOAT, false }
			};

			static const EventDeclaration decl(L"Wait", vAttributes, AttributeVector());

			return decl;
		}

		/*****************************************
		* Parallel
		******************************************/

		void Parallel::Reset(void)
		{
		}

		void Parallel::OnExecute(double dt)
		{
			auto& mOutputs = GetOutputs();
			for(auto slot : mOutputs)
			{
				CallOutput(slot.first);
			}
		}

		const EventDeclaration& Parallel::GenerateDeclaration(void)
		{
			static const core::OutputVector vOutputs = { L"", L"", L"", L"" };
			static const EventDeclaration decl(L"Parallel", AttributeVector(), AttributeVector(), vOutputs);

			return decl;
		}

		/*****************************************
		* CallFunctionBool
		******************************************/

		void CallFunctionBool::Reset(void)
		{
		}

		void CallFunctionBool::OnExecute(double dt)
		{
			if(m_method.IsValid())
				ReturnValue<bool>(0, m_method.Call<bool>());
			CallOutput(0);
		}

		const EventDeclaration& CallFunctionBool::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"function", AttributeType::STRING, false }
			};

			const AttributeVector vReturns =
			{
				{ L"result", AttributeType::BOOL, false }
			};

			static const EventDeclaration decl(L"CallFunctionBool", vAttributes, vReturns);

			return decl;
		}

		bool CallFunctionBool::OnChangeScriptInstance(script::Instance& instance)
		{
			m_method = instance.GetMethod(("bool " + conv::ToA(GetAttribute<std::wstring>(0)) + "()").c_str());
			return m_method.IsValid();
		}

	}
}
