#pragma once
#include "Event.h"
#include "..\Script\Function.h"

namespace acl
{
	namespace core
	{

		/*****************************************
		* Condition
		******************************************/

		class Condition :
			public BaseEvent<Condition>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* BlockingCondition
		******************************************/

		class BlockingCondition :
			public BaseEvent<BlockingCondition>
		{
		public:

			BlockingCondition(void);

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			float m_nowTime;
		};

		/*****************************************
		* Wait
		******************************************/

		class Wait :
			public BaseEvent<Wait>
		{
		public:

			Wait(void);
			Wait(const Wait& wait);

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			float m_nowTime;
		};

		/*****************************************
		* Parallel
		******************************************/

		class Parallel :
			public BaseEvent<Parallel>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* CallFunctionBool
		******************************************/

		class CallFunctionBool :
			public BaseEvent<CallFunctionBool>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;
			bool OnChangeScriptInstance(script::Instance& instance) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			script::Method m_method;
		};
	}
}


