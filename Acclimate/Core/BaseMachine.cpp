#include "BaseMachine.h"
#include "IState.h"

namespace acl
{
	namespace core
	{

		BaseMachine::BaseMachine(IState* pState): m_pState(pState)
		{
		}

		BaseMachine::~BaseMachine(void)
		{
            //delete current state
			delete m_pState;
		}

		void BaseMachine::SetState(IState* pState)
		{
            //delete current state
			delete m_pState;

            //set pointer to new state
			m_pState = pState;
		}

		bool BaseMachine::Run(double dt)
		{
			IState* pState = nullptr;
            //run current state if there is any
			if(m_pState)
				pState = m_pState->Run(dt);
            //otherwise exit
			else 
				return false;

            //check if there is eigther no new state
            //or new state differes from current
			if(!pState || m_pState != pState)
			{
                //delete current state
				delete m_pState;

                //set new state
				m_pState = pState;
			}

            //return whether there is a state or not
			return pState != nullptr;
		}

		void BaseMachine::Render(void) const
		{
			if(m_pState)
				m_pState->Render();
		}

	}
}
