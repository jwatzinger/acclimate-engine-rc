#pragma once
#include "..\Core\Dll.h"
#include "IStateMachine.h"

namespace acl
{
	namespace core
	{

		class IState;

        /// Represents a basic state machine
		/** This class is a basic state machine implementation. It handles one state at 
        *   a time, and takes care of deleting states that are no longer active. */
		class ACCLIMATE_API BaseMachine final :
			public IStateMachine
		{
		public:
			BaseMachine(IState* pState);
			~BaseMachine(void) override;

            /** Sets the current running state.
             *  This method also deletes the last running state
             *
             *  @param[in] pState A pointer to the new state.
             */
			void SetState(IState* pState) override;

            /** Tells the machine to run the current active state.
             *  This method runs the active state. It handles the setting
             *  of the currently running state, depening on the output of
             *  the currently active states Run() method.
             *
             *  @param[in] dt The delta timestep.
             *
             *  @return Indicates whether there was a state to run.
             */
			bool Run(double dt) override;
			
			void Render(void) const;

		private: 

			IState* m_pState;
		};

	}
}

