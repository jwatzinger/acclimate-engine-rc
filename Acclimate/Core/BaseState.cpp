#include "BaseState.h"
#include "..\Entity\Context.h"

namespace acl
{
	namespace core
	{

        BaseState::BaseState(const GameStateContext& ctx): m_ctx(ctx), m_pSystems(&ctx.ecs.systems)
		{
			m_pCurrentState = this;
		}

	}
}