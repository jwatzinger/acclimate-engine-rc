#pragma once
#include "IState.h"
#include "BaseContext.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace ecs
    {
        class SystemManager;
    }

	namespace core
	{

        /// The game base state class
		/** This class describes a main state of the game. Any class that derives from 
        *   this should represent an mutual exlusive game state, like the main menu or
        *   the actual ingame state. The BaseState-class offers mainly convenience helper
        *   functions for passing around the GameStateContext. 
        *   The NewState method can be used to set the next active state, for which the 
        *   Run method must be implemented returning the CurrentState member. Any state
        *   derived from this should only transition into another such state, or at least
        *   store the GameStateContext manually to allow a transition back.*/
		class ACCLIMATE_API BaseState : 
			public IState
		{
		public:
			BaseState(const GameStateContext& ctx);
            virtual ~BaseState(void) override {};

		protected:

            /** Creates a new state.
             *  This method creates a new state and sets it as the
             *  current. Will automatically pass the GameStateContext member
             *  to the constructor. */
			template<class S>
			void NewState(void);

               /** Creates a new state.
             *  This method creates a new state using one constructor argument and 
             *  sets it as the current. Will automatically pass the GameStateContext 
             *  member to the constructor. */
            template<class S, typename Arg1>
			void NewState(Arg1 arg);

            /** Creates a new state.
             *  This method creates a new state using a variable number of 
             *  constructor argument and sets it as the current. Will 
             *  automatically pass the GameStateContext member to the 
             *  constructor. */
			template<class S, typename Arg1, typename... Args>
			void NewState(Arg1 arg, Args&&... args);

			GameStateContext m_ctx; ///< The game state context

			IState* m_pCurrentState; /**< Pointer to the current state.
                * Is initially set to \c this, and should be returned in the
                * Run method to automate the transition into another state. */

            ecs::SystemManager* m_pSystems; /**< Pointer to the system manager
                * to allow instant creation and update of systems */
		};

		template<class S>
		void BaseState::NewState(void)
		{
			m_pCurrentState = new S(m_ctx);
		}

        template<class S, typename Arg1>
		void BaseState::NewState(Arg1 arg)
		{
			m_pCurrentState = new S(m_ctx, arg);
		}

		template<class S, typename Arg1, typename... Args>
		void BaseState::NewState(Arg1 arg, Args&&... args)
		{
			m_pCurrentState = new S(m_ctx, arg, args...);
		}

	}
}
