#include "ConfigLoader.h"
#include "SettingModule.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		ConfigLoader::ConfigLoader(SettingModule& module) : m_pModule(&module)
		{
		}

		void ConfigLoader::Load(const std::wstring& stName) const
		{
			xml::Doc doc;
			doc.LoadFile(stName);

			if(auto pRoot = doc.Root(L"Config"))
			{
				if(auto pVars = pRoot->Nodes(L"Var"))
				{
					for(auto pVar : *pVars)
					{
						const std::wstring& stName = pVar->GetValue();

						if(auto pSetting = m_pModule->GetSetting(stName))
						{
							if(pSetting->GetAccessType() != AccessType::PUBLIC)
							{
								sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Variable", stName, "cannot be altered via config file.");
								continue;
							}

							switch(pSetting->GetType())
							{
							case SettingType::FLOAT:
								pSetting->Set<float>(pVar->Attribute(L"value")->AsFloat());
								break;
							case SettingType::INT:
								pSetting->Set<int>(pVar->Attribute(L"value")->AsInt());
								break;
							case SettingType::UINT:
								pSetting->Set<unsigned int>(pVar->Attribute(L"value")->AsInt());
								break;
							case SettingType::BOOL:
								pSetting->Set<bool>(pVar->Attribute(L"value")->AsBool());
								break;
							case SettingType::STRING:
								pSetting->Set<std::wstring>(*pVar->Attribute(L"value"));
								break;
							default:
								ACL_ASSERT(false);
							}
						}
						else
							sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Unknown variable", stName, "in config file(with value", pVar->Attribute(L"value")->GetValue(), ").");
					}
					m_pModule->Refresh();
				}
			}
		}

	}
}

