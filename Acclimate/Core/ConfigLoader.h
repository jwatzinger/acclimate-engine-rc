#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace core
	{

		class SettingModule;

		class ACCLIMATE_API ConfigLoader
		{
		public:
			ConfigLoader(SettingModule& module);

			void Load(const std::wstring& stName) const;

		private:

			SettingModule* m_pModule;
		};

	}
}


