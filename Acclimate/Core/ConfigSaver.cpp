#include "ConfigSaver.h"
#include <map>
#include "SettingModule.h"
#include "..\System\Convert.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		ConfigSaver::ConfigSaver(const SettingModule& settings) : m_pSettings(&settings)
		{
		}

		void ConfigSaver::Save(const std::wstring& stFile) const
		{
			xml::Doc doc;
			auto& root = doc.InsertNode(L"Config");

			const auto& mSettings = m_pSettings->GetSettings();
			
			std::map<std::wstring, Setting*> mSettingsSorted;
			for(auto& setting : mSettings)
			{
				mSettingsSorted.emplace(setting.first, setting.second);
			}

			for(auto& setting : mSettingsSorted)
			{
				auto pSetting = setting.second;

				if(pSetting->GetAccessType() == AccessType::PUBLIC)
				{
					auto& var = root.InsertNode(L"Var");
					var.SetValue(setting.first);

					std::wstring stValue;
					switch(pSetting->GetType())
					{
					case SettingType::FLOAT:
						stValue = conv::ToString(pSetting->GetData<float>());
						break;
					case SettingType::INT:
						stValue = conv::ToString(pSetting->GetData<int>());
						break;
					case SettingType::UINT:
						stValue = conv::ToString(pSetting->GetData<unsigned int>());
						break;
					case SettingType::BOOL:
						stValue = conv::ToString(pSetting->GetData<bool>());
						break;
					case SettingType::STRING:
						stValue = pSetting->GetData<std::wstring>();
						break;
					default:
						ACL_ASSERT(false);
					}

					var.ModifyAttribute(L"value", stValue);
				}
			}

			doc.SaveFile(stFile);
		}

	}
}

