#pragma once
#include <string>

namespace acl
{
	namespace core
	{

		class SettingModule;

		class ConfigSaver
		{
		public:
			ConfigSaver(const SettingModule& module);

			void Save(const std::wstring& stFile) const;

		private:

			const SettingModule* m_pSettings;
		};

	}
}

