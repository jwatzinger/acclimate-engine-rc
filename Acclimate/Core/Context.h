#pragma once
#include "Dll.h"
#include "EventContext.h"

namespace acl
{
	namespace core
	{

		class ModuleManager;
		class SettingModule;
		class SettingLoader;
		class SceneManager;
		class SceneLoader;
		class ResourceManager;
		class Localization;

		struct ACCLIMATE_API Context
		{
			Context(SettingModule& settings, SettingLoader& settingLoader, SceneManager& scenes, SceneLoader& sceneLoader, ResourceManager& resources, EventContext& events, Localization& localization) :
				settings(settings), settingLoader(settingLoader), scenes(scenes), sceneLoader(sceneLoader), resources(resources), events(events), pModules(nullptr), localization(localization)
			{
			}

			SettingModule& settings;
			SettingLoader& settingLoader;
			SceneManager& scenes;
			SceneLoader& sceneLoader;
			ResourceManager& resources;
			EventContext& events;
			Localization& localization;
			ModuleManager* pModules;
		};

	}
}