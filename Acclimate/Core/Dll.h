#pragma once

#ifdef ACCLIMATE_EXPORT
#define ACCLIMATE_API __declspec (dllexport)
#define EXPORT_TEMPLATE 
#else
#define ACCLIMATE_API __declspec (dllimport)
#define EXPORT_TEMPLATE extern
#endif