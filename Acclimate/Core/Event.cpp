#include "Event.h"
#include "..\System\Log.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace core
	{

		Event::Event(void) : EventCommand(core::SlotType::ATTRIBUTE | core::SlotType::INPUT | core::SlotType::OUTPUT | core::SlotType::RETURN, TargetType::COMMAND)
		{
		}

		void Event::Execute(double dt)
		{
			OnExecute(dt);
		}

		unsigned int Event::OnGetId(void) const
		{
			return GetId();
		}


		void Event::OnReturn(void)
		{

		}

	}
}

