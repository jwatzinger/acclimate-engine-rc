#pragma once
#include "EventCommand.h"
#include "EventAttribute.h"
#include "EventDeclaration.h"

namespace acl
{
	namespace core
	{

		class AttributesNotSetException :
			public std::exception
		{
		};

		class ACCLIMATE_API Event :
			public EventCommand, public InputComponent, public OutputComponent, public AttributeComponent, public ReturnComponent
		{
		public:

			virtual const EventDeclaration& GetDeclaration(void) const = 0;

			void Execute(double dt) override final;

		protected:

			Event(void);

		private:

			void OnReturn(void) override final;

			unsigned int OnGetId(void) const override final;

			virtual void OnExecute(double dt) = 0;
		};

		template<typename Type>
		class BaseEvent :
			public Event
		{
		public:

			BaseEvent(void)
			{
			}

			BaseEvent(const BaseEvent& event) : Event(event)
			{
			}

			EventCommand& Clone(void) const override final
			{
				return *new Type(*(Type*)this);
			}

			static void SetDeclaration(const EventDeclaration& declaration)
			{
				m_pDeclaration = &declaration;
			}

			const EventDeclaration& GetDeclaration(void) const override final
			{
				ACL_ASSERT(m_pDeclaration);
				return *m_pDeclaration;
			}

			const std::wstring& GetName(void) const override final
			{
				return m_pDeclaration->GetName();
			}

			const core::OutputVector& GetOutputDeclaration(void) const override final
			{
				return m_pDeclaration->GetOutputs();
			}

			const AttributeVector& GetAttributeDeclaration(void) const override final
			{
				return m_pDeclaration->GetAttributes();
			}

			const AttributeVector& GetReturnDeclaration(void) const override final
			{
				return m_pDeclaration->GetReturns();
			}

		private:

			static const EventDeclaration* m_pDeclaration;

		};

		template<typename Type>
		const EventDeclaration* BaseEvent<Type>::m_pDeclaration = nullptr;

	}
}