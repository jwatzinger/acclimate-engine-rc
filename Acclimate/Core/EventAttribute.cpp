#include "EventAttribute.h"
#include "EventLoader.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace core
	{

		EventAttribute::EventAttribute(unsigned int objectId, bool isArray) : m_type(AttributeType::OBJECT), m_isConnected(true), m_isArray(isArray),
			m_isSet(false), m_objectType(objectId)
		{
			if(!m_isArray)
				m_pData = &EventLoader::CreateObject(objectId);
			else
				m_pData = &EventLoader::CreateObjectArray(objectId);
		}

		EventAttribute::EventAttribute(AttributeType type, bool isArray) : m_type(type), m_isConnected(true), m_isSet(false),
			m_isArray(isArray), m_objectType(-1)
		{
			ACL_ASSERT(type != AttributeType::OBJECT);

			if(isArray)
			{
				switch(type)
				{
				case AttributeType::BOOL:
					m_pData = new BoolVector;
					break;
				case AttributeType::FLOAT:
					m_pData = new FloatVector;
					break;
				case AttributeType::INT:
					m_pData = new IntVector;
					break;
				case AttributeType::STRING:
					m_pData = new StringVector;
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(type)
				{
				case AttributeType::BOOL:
					m_pData = new bool;
					break;
				case AttributeType::FLOAT:
					m_pData = new float;
					break;
				case AttributeType::INT:
					m_pData = new int;
					break;
				case AttributeType::STRING:
					m_pData = new std::wstring;
					break;
				default:
					ACL_ASSERT(false);
				}
			}
		}

		template<typename Type>
		void setData(void** ppData, const std::wstring& stValue)
		{
			*ppData = new Type(conv::FromString<Type>(stValue));
		}

		EventAttribute::EventAttribute(AttributeType type, const std::wstring& stValue) : m_type(type), m_isConnected(false),
			m_isSet(true), m_isArray(false), m_objectType(-1)
		{
			ACL_ASSERT(type != AttributeType::OBJECT);

			switch(type)
			{
			case AttributeType::BOOL:
				setData<bool>(&m_pData, stValue);
				break;
			case AttributeType::FLOAT:
				setData<float>(&m_pData, stValue);
				break;
			case AttributeType::INT:
				setData<int>(&m_pData, stValue);
				break;
			case AttributeType::STRING:
				m_pData = new std::wstring(stValue);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		template<typename Type>
		void setDataArray(void** ppData, const EventAttribute::ValueVector& vValues)
		{
			auto pData = new std::vector<Type>;
			for(auto& stValue : vValues)
			{
				pData->push_back(conv::FromString<Type>(stValue));
			}
			*ppData = pData;
		}

		EventAttribute::EventAttribute(AttributeType type, const ValueVector& vValues) : m_type(type), m_isConnected(false),
			m_isSet(true), m_isArray(true), m_objectType(-1)
		{
			ACL_ASSERT(type != AttributeType::BOOL);

			switch(type)
			{
			case AttributeType::BOOL:
				setDataArray<bool>(&m_pData, vValues);
				break;
			case AttributeType::FLOAT:
				setDataArray<float>(&m_pData, vValues);
				break;
			case AttributeType::INT:
				setDataArray<int>(&m_pData, vValues);
				break;
			case AttributeType::STRING:
				m_pData = new StringVector(vValues);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		template<typename Type>
		void initData(void** ppData, const void* pData)
		{
			*ppData = new Type(*(Type*)pData);
		}

		template<typename Type>
		void initDataArray(void** ppData, const void* pData)
		{
			*ppData = new std::vector<Type>(*(std::vector<Type>*)pData);
		}

		EventAttribute::EventAttribute(const EventAttribute& attribute) : m_type(attribute.m_type), m_isConnected(attribute.m_isConnected),
			m_isSet(attribute.m_isSet), m_isArray(attribute.m_isArray), m_objectType(attribute.m_objectType)
		{
			if(m_isArray)
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					initDataArray<bool>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::FLOAT:
					initDataArray<float>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::INT:
					initDataArray<int>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::STRING:
					initDataArray<std::wstring>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::OBJECT:
					m_pData = new ObjectVector;
					for(auto pObject : *(ObjectVector*)attribute.m_pData)
					{
						((ObjectVector*)m_pData)->push_back(&pObject->Clone());
					}
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					initData<bool>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::FLOAT:
					initData<float>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::INT:
					initData<int>(&m_pData, attribute.m_pData);
					break;
				case AttributeType::STRING:
					m_pData = new std::wstring(*(std::wstring*)attribute.m_pData);
					break;
				case AttributeType::OBJECT:
					m_pData = &((EventObject*)attribute.m_pData)->Clone();
					break;
				default:
					ACL_ASSERT(false);
				}
			}
		}

		EventAttribute::~EventAttribute(void)
		{
			DeleteData();
		}

		template<typename Type>
		void assignData(void* pOutData, const void* pData)
		{
			(*static_cast<Type*>(pOutData)) = *(Type*)pData;
		}

		template<typename Type>
		void assignDataArray(void* pOutData, const void* pData)
		{
			(*static_cast<std::vector<Type>*>(pOutData)) = *(std::vector<Type>*)pData;
		}

		EventAttribute& EventAttribute::operator=(const EventAttribute& attribute)
		{
			ACL_ASSERT(m_type == attribute.m_type);
			ACL_ASSERT(m_isArray == attribute.m_isArray);
			ACL_ASSERT(m_objectType == attribute.m_objectType);

			if(m_isArray)
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					assignDataArray<bool>(m_pData, attribute.m_pData);
					break;
				case AttributeType::FLOAT:
					assignDataArray<float>(m_pData, attribute.m_pData);
					break;
				case AttributeType::INT:
					assignDataArray<int>(m_pData, attribute.m_pData);
					break;
				case AttributeType::STRING:
					assignDataArray<std::wstring>(m_pData, attribute.m_pData);
					break;
				case AttributeType::OBJECT:
				{
					auto pArray = (EventObjectArray*)m_pData;
					auto pOtherArray = (EventObjectArray*)attribute.m_pData;
					*pArray = *pOtherArray;
				}
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					assignData<bool>(m_pData, attribute.m_pData);
					break;
				case AttributeType::FLOAT:
					assignData<float>(m_pData, attribute.m_pData);
					break;
				case AttributeType::INT:
					assignData<int>(m_pData, attribute.m_pData);
					break;
				case AttributeType::STRING:
					assignData<std::wstring>(m_pData, attribute.m_pData);
					break;
				case AttributeType::OBJECT:
					assignData<EventObject>(m_pData, attribute.m_pData);
					break;
				default:
					ACL_ASSERT(false);
				}
			}

			m_isSet = true;

			return *this;
		}

		void EventAttribute::SetValue(const void* pData)
		{
			ACL_ASSERT(m_type != AttributeType::OBJECT);

			if(!m_isArray)
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					assignData<bool>(m_pData, pData);
					break;
				case AttributeType::FLOAT:
					assignData<float>(m_pData, pData);
					break;
				case AttributeType::INT:
					assignData<int>(m_pData, pData);
					break;
				case AttributeType::STRING:
					assignData<std::wstring>(m_pData, pData);
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
				ACL_ASSERT(false);
		}

		void EventAttribute::SetConnected(bool isConnected)
		{
			if(isConnected)
			{
				m_isConnected = true;
				m_isSet = false;
			}
			else
			{
				m_isConnected = false;
				m_isSet = true;
			}
		}

		AttributeType EventAttribute::GetType(void) const
		{
			return m_type;
		}

		const void* EventAttribute::GetData(void) const
		{
			return m_pData;
		}

		unsigned int EventAttribute::GetObjectType(void) const
		{
			return m_objectType;
		}

		bool EventAttribute::IsConnected(void) const
		{
			return m_isConnected;
		}

		bool EventAttribute::IsSet(void) const
		{
			return m_isSet;
		}

		bool EventAttribute::IsArray(void) const
		{
			return m_isArray;
		}

		template<typename Type>
		std::wstring toString(const void* pData)
		{
			return conv::ToString(*(Type*)pData);
		}

		std::wstring EventAttribute::ToString(void) const
		{
			ACL_ASSERT(!m_isArray);

			switch(m_type)
			{
			case AttributeType::BOOL:
				return toString<bool>(m_pData);
			case AttributeType::FLOAT:
				return toString<float>(m_pData);
			case AttributeType::INT:
				return toString<int>(m_pData);
			case AttributeType::STRING:
				return toString<std::wstring>(m_pData);
			case AttributeType::OBJECT:
				return conv::ToString(((EventObject*)m_pData)->GetType());
			default:
				ACL_ASSERT(false);
			}

			return L"";
		}

		template<typename Type>
		void fillVector(EventAttribute::ValueVector& vValues, const EventAttribute& attribute)
		{
			auto& vArray = attribute.GetValueArray<Type>();
			for(const auto& data : vArray)
			{
				vValues.push_back(conv::ToString(data));
			}
		}

		EventAttribute::ValueVector EventAttribute::ToStringArray(void) const
		{
			ACL_ASSERT(m_isArray);

			ValueVector vValues;

			switch(m_type)
			{
			case AttributeType::BOOL:
				fillVector<bool>(vValues, *this);
				break;
			case AttributeType::FLOAT:
				fillVector<bool>(vValues, *this);
				break;
			case AttributeType::INT:
				fillVector<int>(vValues, *this);
				break;
			case AttributeType::STRING:
				fillVector<std::wstring>(vValues, *this);
				break;
			case AttributeType::OBJECT:
				for(auto pObject : (*(ObjectVector*)m_pData))
				{
					vValues.push_back(conv::ToString(pObject->GetType()));
				}
				break;
			default:
				ACL_ASSERT(false);
			}

			return vValues;
		}

		void EventAttribute::Reset(void)
		{
			ACL_ASSERT(m_isConnected);
			// TODO: possibly reset data too, if necessary
			m_isSet = false;
		}

		template<typename Old, typename New>
		struct ConvertHelper
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				Old* pData = (Old*)*ppData;
				auto pNewData = new New((New)*pData);
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename Old>
		struct ConvertHelper<Old, bool>
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				Old* pData = (Old*)*ppData;
				auto pNewData = new bool(*pData != 0);
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename Old>
		struct ConvertHelper<Old, std::wstring>
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				Old* pData = (Old*)*ppData;
				auto pNewData = new std::wstring(conv::ToString(*pData));
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename New>
		struct ConvertHelperString
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				std::wstring* pData = (std::wstring*)*ppData;
				auto pNewData = new New(conv::FromString<New>(*pData));
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename Old, typename New>
		struct ConvertHelperArray
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				std::vector<Old>* pData = (std::vector<Old>*)*ppData;
				auto pNewData = new std::vector<New>;
				for(auto old : *pData)
				{
					pNewData->push_back((New)old);
				}
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename Old>
		struct ConvertHelperArray<Old, bool>
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				std::vector<Old>* pData = (std::vector<Old>*)*ppData;
				auto pNewData = new std::vector<bool>;
				for(auto old : *pData)
				{
					pNewData->push_back(old != 0);
				}
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename Old>
		struct ConvertHelperArray<Old, std::wstring>
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				std::vector<Old>* pData = (std::vector<Old>*)*ppData;
				auto pNewData = new std::vector<std::wstring>;
				for(auto old : *pData)
				{
					pNewData->push_back(conv::ToString(old));
				}
				delete pData;
				*ppData = pNewData;
			}
		};

		template<typename New>
		struct ConvertHelperArrayString
		{
			static void convertData(void** ppData)
			{
				ACL_ASSERT(ppData);
				std::vector<std::wstring>* pData = (std::vector<std::wstring>*)*ppData;
				auto pNewData = new std::vector<New>;
				for(auto& stOld : *pData)
				{
					pNewData->push_back(conv::FromString<New>(stOld));
				}
				delete pData;
				*ppData = pNewData;
			}
		};

		void EventAttribute::ConvertToType(AttributeType type)
		{
			if(m_type != type)
			{
				if(m_isArray)
				{
					switch(m_type)
					{
					case AttributeType::BOOL:
						switch(type)
						{
						case AttributeType::FLOAT:
							ConvertHelperArray<bool, float>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelperArray<bool, int>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelperArray<bool, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::FLOAT:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelperArray<float, bool>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelperArray<float, int>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelperArray<float, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::INT:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelperArray<int, bool>::convertData(&m_pData);
							break;
						case AttributeType::FLOAT:
							ConvertHelperArray<int, float>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelperArray<int, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::STRING:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelperArrayString<bool>::convertData(&m_pData);
							break;
						case AttributeType::FLOAT:
							ConvertHelperArrayString<float>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelperArrayString<int>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					default:
						ACL_ASSERT(false);
					}
				}
				else
				{
					switch(m_type)
					{
					case AttributeType::BOOL:
						switch(type)
						{
						case AttributeType::FLOAT:
							ConvertHelper<bool, float>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelper<bool, int>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelper<bool, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::INT:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelper<int, bool>::convertData(&m_pData);
							break;
						case AttributeType::FLOAT:
							ConvertHelper<int, float>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelper<int, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::FLOAT:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelper<float, bool>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelper<float, int>::convertData(&m_pData);
							break;
						case AttributeType::STRING:
							ConvertHelper<float, std::wstring>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::STRING:
						switch(type)
						{
						case AttributeType::BOOL:
							ConvertHelperString<bool>::convertData(&m_pData);
							break;
						case AttributeType::FLOAT:
							ConvertHelperString<float>::convertData(&m_pData);
							break;
						case AttributeType::INT:
							ConvertHelperString<int>::convertData(&m_pData);
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					case AttributeType::OBJECT:
						delete (EventObject*)m_pData;
						switch(type)
						{
						case AttributeType::BOOL:
							m_pData = new bool(false);
							break;
						case AttributeType::FLOAT:
							m_pData = new float(0.0f);
							break;
						case AttributeType::INT:
							m_pData = new int(0);
							break;
						case AttributeType::STRING:
							m_pData = new std::wstring(L"");
							break;
						default:
							ACL_ASSERT(false);
						}
						break;
					default:
						ACL_ASSERT(false);
					}
				}

				m_type = type;
			}
		}

		void EventAttribute::ConvertToType(unsigned int objectType)
		{
			ACL_ASSERT(!m_isArray);

			if(m_type != AttributeType::OBJECT)
			{
				DeleteData();
				m_pData = &EventLoader::CreateObject(objectType);
			}
			else
			{
				auto pObject = (EventObject*)m_pData;
				if(pObject->GetType() != objectType)
				{
					DeleteData();
					m_pData = &EventLoader::CreateObject(objectType);
				}
			}

			m_type = AttributeType::OBJECT;
		}

		void EventAttribute::DeleteData(void)
		{
			if(m_isArray)
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					delete (BoolVector*)m_pData;
					break;
				case AttributeType::FLOAT:
					delete (FloatVector*)m_pData;
					break;
				case AttributeType::INT:
					delete (IntVector*)m_pData;
					break;
				case AttributeType::STRING:
					delete (StringVector*)m_pData;
					break;
				case AttributeType::OBJECT:
					delete (EventObjectArray*)m_pData;
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(m_type)
				{
				case AttributeType::BOOL:
					delete (bool*)m_pData;
					break;
				case AttributeType::FLOAT:
					delete (float*)m_pData;
					break;
				case AttributeType::INT:
					delete (int*)m_pData;
					break;
				case AttributeType::STRING:
					delete (std::wstring*)m_pData;
					break;
				case AttributeType::OBJECT:
					delete (EventObject*)m_pData;
					break;
				default:
					ACL_ASSERT(false);
				}
			}
		}

		EventObjectArray& EventAttribute::GetValueArray(void)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(m_isArray);
			return *(EventObjectArray*)m_pData;
		}

	}
}
