#pragma once
#include <string>
#include <vector>
#include "EventObject.h"
#include "..\Core\Dll.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace core
	{

		enum class AttributeType
		{
			UNKNOWN = -1, BOOL, FLOAT, INT, STRING, OBJECT
		};

		class ACCLIMATE_API EventAttribute
		{
			typedef std::vector<bool> BoolVector;
			typedef std::vector<float> FloatVector;
			typedef std::vector<int> IntVector;
			typedef std::vector<std::wstring> StringVector;
			typedef std::vector<EventObject*> ObjectVector;
		public:
			typedef std::vector<std::wstring> ValueVector;

			template<typename Type>
			EventAttribute(const Type& value);
			template<>
			EventAttribute(const bool& value);
			template<>
			EventAttribute(const float& value);
			template<>
			EventAttribute(const int& value);
			template<>
			EventAttribute(const std::wstring& stValue);
			template<>
			EventAttribute(const BoolVector& vValues);
			template<>
			EventAttribute(const FloatVector& vValues);
			template<>
			EventAttribute(const IntVector& vValues);
			template<>
			EventAttribute(const StringVector& vValues);
			EventAttribute(unsigned int objectId, bool isArray);
			EventAttribute(AttributeType type, bool isArray);
			EventAttribute(AttributeType type, const std::wstring& stValues);
			EventAttribute(AttributeType type, const ValueVector& vValues);
			EventAttribute(const EventAttribute& attribute);
			~EventAttribute(void);

			EventAttribute& operator=(const EventAttribute& attribute);

			void SetConnected(bool isConnected);

			AttributeType GetType(void) const;
			const void* GetData(void) const;
			unsigned int GetObjectType(void) const;
			bool IsConnected(void) const;
			bool IsSet(void) const;
			bool IsArray(void) const;
			std::wstring ToString(void) const;
			ValueVector ToStringArray(void) const;

			void Reset(void);
			void ConvertToType(AttributeType type);
			void ConvertToType(unsigned int objectType);

			void SetValue(const void* pData);
			template<typename Type>
			void SetValue(Type& value);
			template<>
			void SetValue<bool>(bool& value);
			template<>
			void SetValue<float>(float& value);
			template<>
			void SetValue<int>(int& value);
			template<>
			void SetValue<std::wstring>(std::wstring& value);
			template<>
			void SetValue<EventObject>(EventObject& value);
			template<typename Type>
			void SetValue(const Type& value);
			template<>
			void SetValue<bool>(const bool& value);
			template<>
			void SetValue<float>(const float& value);
			template<>
			void SetValue<int>(const int& value);
			template<>
			void SetValue<std::wstring>(const std::wstring& value);
			template<>
			void SetValue<EventObject>(const EventObject& value);
			template<typename Type>
			void SetValueArray(const std::vector<Type>& vValues);
			template<>
			void SetValueArray<bool>(const BoolVector& value);
			template<>
			void SetValueArray<int>(const IntVector& value);
			template<>
			void SetValueArray<float>(const FloatVector& value);
			template<>
			void SetValueArray<std::wstring>(const StringVector& value);

			template<typename Type>
			Type& GetValue(void);
			template<>
			bool& GetValue<bool>(void);
			template<>
			float& GetValue<float>(void);
			template<>
			int& GetValue<int>(void);
			template<>
			std::wstring& GetValue<std::wstring>(void);
			template<typename Type>
			const Type& GetValue(void) const;
			template<>
			const bool& GetValue<bool>(void) const;
			template<>
			const float& GetValue<float>(void) const;
			template<>
			const int& GetValue<int>(void) const;
			template<>
			const std::wstring& GetValue<std::wstring>(void) const;

			template<typename Type>
			std::vector<Type>& GetValueArray(void);
			template<>
			BoolVector& GetValueArray<bool>(void);
			template<>
			IntVector& GetValueArray<int>(void);
			template<>
			FloatVector& GetValueArray<float>(void);
			template<>
			StringVector& GetValueArray<std::wstring>(void);
			EventObjectArray& GetValueArray(void);

			template<typename Type>
			const std::vector<Type>& GetValueArray(void) const;
			template<>
			const BoolVector& GetValueArray<bool>(void) const;
			template<>
			const IntVector& GetValueArray<int>(void) const;
			template<>
			const FloatVector& GetValueArray<float>(void) const;
			template<>
			const StringVector& GetValueArray<std::wstring>(void) const;

		private:

			void DeleteData(void);

			void* m_pData;
			bool m_isConnected, m_isSet, m_isArray;
			unsigned int m_objectType;
			AttributeType m_type;
		};

		template<typename Type>
		EventAttribute::EventAttribute(const Type& type) : m_type(AttributeType::OBJECT), m_isConnected(false), m_isSet(true),
			m_isArray(false), m_objectType(getObjectType<Type>())
		{
			m_pData = new EventObjectContainer<Type>(type);
			//static_assert(false, "unknown event variable type.");
		}

		template<>
		EventAttribute::EventAttribute(const bool& value): m_type(AttributeType::BOOL), m_isConnected(false), m_isSet(true),
			m_isArray(false), m_objectType(-1)
		{
			m_pData = new bool(value);
		}

		template<>
		EventAttribute::EventAttribute(const float& value) : m_type(AttributeType::FLOAT), m_isConnected(false), m_isSet(true),
			m_isArray(false), m_objectType(-1)
		{
			m_pData = new float(value);
		}

		template<>
		EventAttribute::EventAttribute(const int& value) : m_type(AttributeType::INT), m_isConnected(false), m_isSet(true),
			m_isArray(false), m_objectType(-1)
		{
			m_pData = new int(value);
		}

		template<>
		EventAttribute::EventAttribute(const std::wstring& stValue) : m_type(AttributeType::STRING), m_isConnected(false), m_isSet(true),
			m_isArray(false), m_objectType(-1)
		{
			m_pData = new std::wstring(stValue);
		}

		template<>
		EventAttribute::EventAttribute(const BoolVector& vValues) : m_type(AttributeType::BOOL), m_isConnected(false), m_isSet(true),
			m_isArray(true), m_objectType(-1)
		{
			m_pData = new BoolVector(vValues);
		}

		template<>
		EventAttribute::EventAttribute(const FloatVector& vValues) : m_type(AttributeType::FLOAT), m_isConnected(false), m_isSet(true),
			m_isArray(true), m_objectType(-1)
		{
			m_pData = new FloatVector(vValues);
		}

		template<>
		EventAttribute::EventAttribute(const IntVector& vValues) : m_type(AttributeType::INT), m_isConnected(false), m_isSet(true),
			m_isArray(true), m_objectType(-1)
		{
			m_pData = new IntVector(vValues);
		}

		template<>
		EventAttribute::EventAttribute(const StringVector& vValues) : m_type(AttributeType::STRING), m_isConnected(false), m_isSet(true),
			m_isArray(true), m_objectType(-1)
		{
			m_pData = new StringVector(vValues);
		}

		template<typename Type>
		void EventAttribute::SetValue(Type& value)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);

			auto pObject = (EventObject*)m_pData;
			ACL_ASSERT(pObject->GetType() == getObjectType<Type>());

			pObject->SetObject<Type>(value);
		}

		template<>
		void EventAttribute::SetValue<bool>(bool& value)
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(bool*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<float>(float& value)
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(float*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<int>(int& value)
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(int*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<std::wstring>(std::wstring& value)
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(std::wstring*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<EventObject>(EventObject& value)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(EventObject*)m_pData) = value;
		}

		template<typename Type>
		void EventAttribute::SetValue(const Type& value)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);

			auto pObject = (EventObject*)m_pData;
			ACL_ASSERT(pObject->GetType() == getObjectType<Type>());

			pObject->SetObject<Type>(value);
		}

		template<>
		void EventAttribute::SetValue<bool>(const bool& value)
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(bool*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<float>(const float& value)
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(float*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<int>(const int& value)
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(int*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<std::wstring>(const std::wstring& value)
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(std::wstring*)m_pData) = value;
		}

		template<>
		void EventAttribute::SetValue<EventObject>(const EventObject& value)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);
			m_isSet = true;
			(*(EventObject*)m_pData) = value;
		}

		template<typename Type>
		void EventAttribute::SetValueArray(const std::vector<Type>& vValues)
		{
			static_assert(false, "unknown event variable type.");
		}

		template<>
		void EventAttribute::SetValueArray<bool>(const BoolVector& vValues)
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(m_isArray);
			m_isSet = true;
			(*(BoolVector*)m_pData) = vValues;
		}

		template<>
		void EventAttribute::SetValueArray<int>(const IntVector& vValues)
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(m_isArray);
			m_isSet = true;
			(*(IntVector*)m_pData) = vValues;
		}

		template<>
		void EventAttribute::SetValueArray<float>(const FloatVector& vValues)
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(m_isArray);
			m_isSet = true;
			(*(FloatVector*)m_pData) = vValues;
		}

		template<>
		void EventAttribute::SetValueArray<std::wstring>(const StringVector& vValues)
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(m_isArray);
			m_isSet = true;
			(*(StringVector*)m_pData) = vValues;
		}

		template<typename Type>
		Type& EventAttribute::GetValue(void)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);

			auto pObject = (EventObject*)m_pData;
			ACL_ASSERT(pObject->GetType() == getObjectType<Type>());

			return pObject->GetObject<Type>();
		}

		template<>
		bool& EventAttribute::GetValue<bool>(void)
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(!m_isArray);
			return *(bool*)m_pData;
		}

		template<>
		float& EventAttribute::GetValue<float>(void)
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(!m_isArray);
			return *(float*)m_pData;
		}

		template<>
		int& EventAttribute::GetValue<int>(void)
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(!m_isArray);
			return *(int*)m_pData;
		}

		template<>
		std::wstring& EventAttribute::GetValue<std::wstring>(void)
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(!m_isArray);
			return *(std::wstring*)m_pData;
		}

		template<typename Type>
		const Type& EventAttribute::GetValue(void) const
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(!m_isArray);

			auto pObject = (EventObject*)m_pData;
			ACL_ASSERT(pObject->GetType() == getObjectType<Type>());

			return pObject->GetObject<Type>();
		}

		template<>
		const bool& EventAttribute::GetValue<bool>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(!m_isArray);
			return *(bool*)m_pData;
		}

		template<>
		const float& EventAttribute::GetValue<float>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(!m_isArray);
			return *(float*)m_pData;
		}

		template<>
		const int& EventAttribute::GetValue<int>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(!m_isArray);
			return *(int*)m_pData;
		}

		template<>
		const std::wstring& EventAttribute::GetValue<std::wstring>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(!m_isArray);
			return *(std::wstring*)m_pData;
		}

		template<typename Type>
		std::vector<Type>& EventAttribute::GetValueArray(void)
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(m_isArray);
			
			auto pArray = (EventObjectArray*)m_pData;
			return pArray->GetVector<Type>();
		}

		template<>
		EventAttribute::BoolVector& EventAttribute::GetValueArray<bool>(void)
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(m_isArray);
			return *(BoolVector*)m_pData;
		}

		template<>
		EventAttribute::IntVector& EventAttribute::GetValueArray<int>(void)
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(m_isArray);
			return *(IntVector*)m_pData;
		}

		template<>
		EventAttribute::FloatVector& EventAttribute::GetValueArray<float>(void)
		{
			ACL_ASSERT(m_type == AttributeType::FLOAT);
			ACL_ASSERT(m_isArray);
			return *(FloatVector*)m_pData;
		}

		template<>
		EventAttribute::StringVector& EventAttribute::GetValueArray<std::wstring>(void)
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(m_isArray);
			return *(StringVector*)m_pData;
		}

		template<typename Type>
		const std::vector<Type>& EventAttribute::GetValueArray(void) const
		{
			ACL_ASSERT(m_type == AttributeType::OBJECT);
			ACL_ASSERT(m_isArray);

			return ((EventObjectArray<Type>*)m_pData)->GetVector();
			//static_assert(false, "unknown event variable type.");
		}

		template<>
		const EventAttribute::BoolVector& EventAttribute::GetValueArray<bool>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::BOOL);
			ACL_ASSERT(m_isArray);
			return *(const BoolVector*)m_pData;
		}

		template<>
		const EventAttribute::IntVector& EventAttribute::GetValueArray<int>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(m_isArray);
			return *(const IntVector*)m_pData;
		}

		template<>
		const EventAttribute::FloatVector& EventAttribute::GetValueArray<float>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::INT);
			ACL_ASSERT(m_isArray);
			return *(const FloatVector*)m_pData;
		}

		template<>
		const EventAttribute::StringVector& EventAttribute::GetValueArray<std::wstring>(void) const
		{
			ACL_ASSERT(m_type == AttributeType::STRING);
			ACL_ASSERT(m_isArray);
			return *(const StringVector*)m_pData;
		}

	}
}

