#include "EventCommand.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		EventCommand::EventCommand(unsigned int slots, TargetType type) : m_slots(slots), m_id(-1),
			m_targetType(type)
		{
		}

		EventCommand::~EventCommand(void)
		{
		}

		void EventCommand::SetPosition(const math::Vector2& vPosition)
		{
			m_vPosition = vPosition;
		}

		void EventCommand::SetId(unsigned int id)
		{
			m_id = id;
		}

		void EventCommand::SetEntity(const ecs::EntityHandle& entity)
		{
		}

		const math::Vector2& EventCommand::GetPosition(void) const
		{
			return m_vPosition;
		}
		
		unsigned int EventCommand::GetId(void) const
		{
			return m_id;
		}

		TargetType EventCommand::GetTargetType(void) const
		{
			return m_targetType;
		}

		bool EventCommand::HasSlot(SlotType type) const
		{
			return (m_slots & type) == type;
		}

		InputComponent* EventCommand::QueryInput(void)
		{
			return ComponentCast<InputComponent>(SlotType::INPUT);
		}

		OutputComponent* EventCommand::QueryOutput(void)
		{
			return ComponentCast<OutputComponent>(SlotType::OUTPUT);
		}

		AttributeComponent* EventCommand::QueryAttribute(void)
		{
			return ComponentCast<AttributeComponent>(SlotType::ATTRIBUTE);
		}

		ReturnComponent* EventCommand::QueryReturn(void)
		{
			return ComponentCast<ReturnComponent>(SlotType::RETURN);
		}

		const InputComponent* EventCommand::QueryInput(void) const
		{
			return ComponentCast<InputComponent>(SlotType::INPUT);
		}

		const OutputComponent* EventCommand::QueryOutput(void) const
		{
			return ComponentCast<OutputComponent>(SlotType::OUTPUT);
		}

		const AttributeComponent* EventCommand::QueryAttribute(void) const
		{
			return ComponentCast<AttributeComponent>(SlotType::ATTRIBUTE);
		}

		const ReturnComponent* EventCommand::QueryReturn(void) const
		{
			return ComponentCast<ReturnComponent>(SlotType::RETURN);
		}

		/**********************************
		* InputComponent
		**********************************/

		InputComponent::InputComponent(void)
		{
		}

		InputComponent::InputComponent(const InputComponent& input)
		{
		}

		void InputComponent::TakeTime(double time)
		{
			SigTakeTime(OnGetId(), time);
		}

		/**********************************
		* OutputComponent
		**********************************/

		OutputComponent::OutputComponent(void) : m_flowControl(false)
		{
		}

		OutputComponent::OutputComponent(const OutputComponent& output) : m_mOutputs(output.m_mOutputs),
			m_flowControl(output.m_flowControl)
		{
		}

		void OutputComponent::SetupOutputs(const OutputMap& mOutputs)
		{
			m_mOutputs.clear();

			const auto numOutputs = GetNumOutputs();

			for(auto slot : mOutputs)
			{
				ACL_ASSERT(slot.first < numOutputs);
				m_mOutputs[slot.first] = slot.second;
			}
		}

		void OutputComponent::SetOutput(unsigned int slot, unsigned int id)
		{
			if(m_mOutputs.count(slot))
			{
				if(id == EventCommand::NO_EVENT_ID)
					m_mOutputs.erase(slot);
				else
					m_mOutputs[slot] = id;
			}
			else if(id != EventCommand::NO_EVENT_ID)
			{
				if(slot < GetNumOutputs())
					m_mOutputs[slot] = id;
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Faild to set output slot", slot, "to event id", id, ". Slot is invalid for given event(type).");
			}
		}

		void OutputComponent::ClearOutput(void)
		{
			m_mOutputs.clear();
		}

		unsigned int OutputComponent::GetNumOutputs(void) const
		{
			return GetOutputDeclaration().size();
		}

		unsigned int OutputComponent::GetOutputTarget(unsigned int slot) const
		{
			auto itr = m_mOutputs.find(slot);
			if(itr != m_mOutputs.end())
				return itr->second;
			else
				return EventCommand::NO_EVENT_ID;
		}

		bool OutputComponent::HasFlowControl(void) const
		{
			return m_flowControl;
		}

		void OutputComponent::CallOutput(unsigned int slot) const
		{
			auto itr = m_mOutputs.find(slot);
			if(itr != m_mOutputs.end())
				SigOutput(itr->second);
			else
				SigOutput(EventCommand::NO_EVENT_ID);
		}

		const OutputComponent::OutputMap& OutputComponent::GetOutputs(void) const
		{
			return m_mOutputs;
		}

		void OutputComponent::ActivateFlowControl(void)
		{
			m_flowControl = true;
		}

		void OutputComponent::DeactivateFlowControl(void)
		{
			m_flowControl = false;
		}

		/**********************************
		* AttributeComponent
		**********************************/

		AttributeData::AttributeData(const std::wstring& stData) : isConnected(false)
		{
			vData.push_back(stData);
		}

		AttributeData::AttributeData(void) : isConnected(true)
		{
		}

		AttributeData::AttributeData(ValueVector&& vValues) : vData(vValues), isConnected(false)
		{
		}

		AttributeData::AttributeData(AttributeData&& data) : vData(std::move(data.vData)), isConnected(data.isConnected)
		{
		}

		AttributeComponent::AttributeComponent(void)
		{
		}

		AttributeComponent::AttributeComponent(const AttributeComponent& attribute)
		{
			m_vAttributes.reserve(attribute.m_vAttributes.size());

			for(auto pAttribute : attribute.m_vAttributes)
			{
				m_vAttributes.push_back(new EventAttribute(*pAttribute));
			}
		}

		void AttributeComponent::SetupAttributes(const ValueVector& vValues)
		{
			for(auto pAttribute : m_vAttributes)
			{
				delete pAttribute;
			}
			m_vAttributes.clear();

			auto& vAttributes = GetAttributeDeclaration();

			ACL_ASSERT(vValues.size() <= vAttributes.size());

			unsigned int i = 0;
			for(auto& value : vValues)
			{
				auto& attribute = vAttributes[i];
				const auto type = attribute.GetType();
				const auto isArray = attribute.IsArray();

				if(type == AttributeType::OBJECT)
				{
					ACL_ASSERT(value.isConnected); // currently objects cannot be hardcoded
					const auto id = attribute.GetObjectId();
					ACL_ASSERT(id != -1);
					m_vAttributes.push_back(new EventAttribute(id, attribute.IsArray()));
				}
				else
				{
					if(value.isConnected)
						m_vAttributes.push_back(new EventAttribute(type, isArray));
					else
					{
						if(isArray)
							m_vAttributes.push_back(new EventAttribute(type, value.vData));
						else
						{
							ACL_ASSERT(value.vData.size() == 1);
							m_vAttributes.push_back(new EventAttribute(type, value.vData[0]));
						}
					}
				}

				i++;
			}

			for(unsigned int j = i; j < vAttributes.size(); j++)
			{
				auto& attribute = vAttributes[i];
				const auto type = attribute.GetType();
				const auto isArray = attribute.IsArray();

				if(type == AttributeType::OBJECT)
				{
					const auto id = attribute.GetObjectId();
					ACL_ASSERT(id != -1);
					m_vAttributes.push_back(new EventAttribute(id, attribute.IsArray()));
				}
				else
				{
					if(isArray)
						m_vAttributes.push_back(new EventAttribute(type, AttributeData::ValueVector()));
					else
						m_vAttributes.push_back(new EventAttribute(type, std::wstring()));
				}
			}

			RegisterAttributeSet();
		}

		unsigned int AttributeComponent::GetNumAttributes(void) const
		{
			return GetAttributeDeclaration().size();
		}

		EventAttribute& AttributeComponent::GetAttribute(unsigned int slot)
		{
			ACL_ASSERT(slot < m_vAttributes.size());
			return *m_vAttributes.at(slot);
		}

		const EventAttribute& AttributeComponent::GetAttribute(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vAttributes.size());
			return *m_vAttributes.at(slot);
		}

		bool AttributeComponent::NoConnected(void) const
		{
			for(auto pAttributes : m_vAttributes)
			{
				if(pAttributes->IsConnected())
					return false;
			}

			return true;
		}

		void AttributeComponent::ConnectAttribute(unsigned int slot, bool isConnected)
		{
			ACL_ASSERT(slot < m_vAttributes.size());
			auto pAttribute = m_vAttributes[slot];
			pAttribute->SetConnected(isConnected);

			RegisterAttributeSet();
		}

		void AttributeComponent::Invalidate(unsigned int slot)
		{
			auto pAttribute = m_vAttributes.at(slot);
			ACL_ASSERT(pAttribute->GetType() == AttributeType::OBJECT);

			auto pObject = (EventObject*)pAttribute->GetData();
			pObject->Invalidate();
		}

		void AttributeComponent::RegisterAttributeSet(void)
		{
			for(auto pAttribute : m_vAttributes)
			{
				if(pAttribute->IsConnected())
				{
					if(!pAttribute->IsSet())
						return;
				}
				else if(pAttribute->GetType() == core::AttributeType::OBJECT)
					return;
			}
		}

		/**********************************
		* ReturnComponent
		**********************************/

		ReturnComponent::ReturnComponent(void)
		{
		}

		ReturnComponent::ReturnComponent(const ReturnComponent& ret) : m_mReturn(ret.m_mReturn)
		{
		}

		void ReturnComponent::SetupReturns(const ReturnMap& mReturns)
		{
			m_mReturn.clear();

			auto& vReturns = GetReturnDeclaration();

			for(auto& ret : mReturns)
			{
				ACL_ASSERT(ret.first < vReturns.size());
				auto& returnValue = vReturns[ret.first];
				const auto type = returnValue.GetType();

				if(type == core::AttributeType::OBJECT)
					m_mReturn.emplace(std::piecewise_construct, std::make_tuple(ret.first), std::make_tuple(returnValue.GetObjectId(), returnValue.IsArray(), ret.second));
				else
					m_mReturn.emplace(std::piecewise_construct, std::make_tuple(ret.first), std::make_tuple(type, returnValue.IsArray(), ret.second));
			}
		}

		void ReturnComponent::ClearReturnTargets(void)
		{
			m_mReturn.clear();
		}

		void ReturnComponent::AddReturn(unsigned int slot, unsigned int target, unsigned int targetSlot)
		{
			ACL_ASSERT(target != EventCommand::NO_EVENT_ID);
			if(!m_mReturn.count(slot))
			{
				auto& vReturns = GetReturnDeclaration();

				ACL_ASSERT(slot < vReturns.size());

				auto& returnValue = vReturns.at(slot);
				const auto type = returnValue.GetType();

				if(type == core::AttributeType::OBJECT)
					m_mReturn.emplace(std::piecewise_construct, std::make_tuple(slot), std::make_tuple(returnValue.GetObjectId(), returnValue.IsArray()));
				else
					m_mReturn.emplace(std::piecewise_construct, std::make_tuple(slot), std::make_tuple(type, returnValue.IsArray()));
			}

			m_mReturn[slot].vTargets.emplace_back(target, targetSlot);
		}

		void ReturnComponent::RemoveReturn(unsigned int slot, unsigned int target)
		{
			ACL_ASSERT(target != EventCommand::NO_EVENT_ID);

			auto itr = m_mReturn.find(slot);
			ACL_ASSERT(itr != m_mReturn.end()); // remove return should only ever be called in case the return-connection really exists

			for(auto ii = itr->second.vTargets.begin(); ii != itr->second.vTargets.end(); ++ii)
			{
				if(ii->first == target)
				{
					itr->second.vTargets.erase(ii);

					if(itr->second.vTargets.empty())
						m_mReturn.erase(slot);

					return;
				}
			}

			// should never be reached
			ACL_ASSERT(false);
		}

		void ReturnComponent::RemoveReturn(unsigned int slot)
		{
			ACL_ASSERT(m_mReturn.count(slot));

			m_mReturn.erase(slot);
		}

		unsigned int ReturnComponent::GetNumReturns(void) const
		{
			return GetReturnDeclaration().size();
		}

		const ReturnComponent::ReturnTargetVector& ReturnComponent::GetReturnTargets(unsigned int slot) const
		{
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
				return itr->second.vTargets;
			else
			{
				static const ReturnComponent::ReturnTargetVector vReturns;
				return vReturns;
			}
		}

		void ReturnComponent::ReturnAttribute(unsigned int slot, const EventAttribute& attribute)
		{
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				// TODO: probably return attribute itself?
				*itr->second.pAttribute = attribute;
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, itr->second.pAttribute->IsArray());
				}
			}
		}

		ReturnComponent::ReturnData::ReturnData(void) : pAttribute(nullptr)
		{
		}

		ReturnComponent::ReturnData::ReturnData(const ReturnData& data) : vTargets(data.vTargets)
		{
			pAttribute = new EventAttribute(*data.pAttribute);
		}

		ReturnComponent::ReturnData::ReturnData(unsigned int objectType, bool isArray)
		{
			pAttribute = new EventAttribute(objectType, isArray);
		}

		ReturnComponent::ReturnData::ReturnData(AttributeType type, bool isArray)
		{
			pAttribute = new EventAttribute(type, isArray);
		}

		ReturnComponent::ReturnData::ReturnData(unsigned int objectType, bool isArray, const ReturnTargetVector& vTargets) : vTargets(vTargets)
		{
			pAttribute = new EventAttribute(objectType, isArray);
		}

		ReturnComponent::ReturnData::ReturnData(AttributeType type, bool isArray, const ReturnTargetVector& vTargets) : vTargets(vTargets)
		{
			pAttribute = new EventAttribute(type, isArray);
		}

		ReturnComponent::ReturnData::ReturnData(ReturnData&& data) : vTargets(std::move(data.vTargets)), pAttribute(data.pAttribute)
		{
			data.pAttribute = nullptr;
		}

		ReturnComponent::ReturnData::~ReturnData(void)
		{
			delete pAttribute;
		}

	}
}

