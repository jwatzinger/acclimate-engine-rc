#pragma once
#include <unordered_map>
#include "EventAttribute.h"
#include "Signal.h"
#include "..\Entity\Entity.h"
#include "..\Math\Vector.h"

#include "EventDeclaration.h"

namespace acl
{
	namespace core
	{

		enum SlotType
		{
			INPUT = 1, 
			OUTPUT = 2, 
			ATTRIBUTE = 4, 
			RETURN = 8
		};

		enum class TargetType
		{
			COMMAND, TRIGGER, GETTER, FUNCTION, SETTER
		};

		class InputComponent;
		class OutputComponent;
		class AttributeComponent;
		class ReturnComponent;

		class ACCLIMATE_API EventCommand
		{
		public:
			static const unsigned int NO_EVENT_ID = -1;

			virtual ~EventCommand(void);

			virtual EventCommand& Clone(void) const = 0;

			void SetPosition(const math::Vector2& vPosition);
			void SetId(unsigned int id);
			virtual void SetEntity(const ecs::EntityHandle& entity);

			const math::Vector2& GetPosition(void) const;
			unsigned int GetId(void) const;
			TargetType GetTargetType(void) const;
			virtual const std::wstring& GetName(void) const = 0;
			bool HasSlot(SlotType type) const;
			InputComponent* QueryInput(void);
			OutputComponent* QueryOutput(void);
			AttributeComponent* QueryAttribute(void);
			ReturnComponent* QueryReturn(void);
			const InputComponent* QueryInput(void) const;
			const OutputComponent* QueryOutput(void) const;
			const AttributeComponent* QueryAttribute(void) const;
			const ReturnComponent* QueryReturn(void) const;

		protected:

			EventCommand(unsigned int slots, TargetType type);

		private:

			template<typename ComponentType>
			ComponentType* ComponentCast(SlotType type)
			{
				if(m_slots & type)
					return dynamic_cast<ComponentType*>(this);
				else
					return nullptr;
			}

			template<typename ComponentType>
			const ComponentType* ComponentCast(SlotType type) const
			{
				if(m_slots & type)
					return dynamic_cast<const ComponentType*>(this);
				else
					return nullptr;
			}

			unsigned int m_id;
			math::Vector2 m_vPosition;
			unsigned int m_slots;
			TargetType m_targetType;
		};

		/**********************************
		* InputComponent
		**********************************/

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<unsigned int, double>;

		class ACCLIMATE_API InputComponent
		{
		public:

			InputComponent(void);
			InputComponent(const InputComponent& input);

			virtual void Execute(double dt) = 0;
			virtual void Reset(void) = 0;

			Signal<unsigned int, double> SigTakeTime;

		protected:

			void TakeTime(double time);

		private:
#pragma warning( disable: 4251 )

			virtual unsigned int OnGetId(void) const = 0;

			OutputVector m_vOutputs;
#pragma warning( default: 4251 )
		};
			
		/**********************************
		* OutputComponent
		**********************************/

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<unsigned int>;

		class ACCLIMATE_API OutputComponent
		{
		public:
			typedef std::unordered_map<unsigned int, unsigned int> OutputMap;

			OutputComponent(void);
			OutputComponent(const OutputComponent& output);

			void SetupOutputs(const OutputMap& mOutput);

			void SetOutput(unsigned int slot, unsigned int id);
			void ClearOutput(void);

			unsigned int GetNumOutputs(void) const;
			unsigned int GetOutputTarget(unsigned int slot) const;
			virtual const OutputVector& GetOutputDeclaration(void) const = 0;
			bool HasFlowControl(void) const;

			Signal<unsigned int> SigOutput;

		protected:

			void ActivateFlowControl(void);
			void DeactivateFlowControl(void);

			void CallOutput(unsigned int slot) const;
			const OutputMap& GetOutputs(void) const;

		private:
#pragma warning( disable: 4251 )

			bool m_flowControl;
			OutputMap m_mOutputs;
#pragma warning( default: 4251 )
		};

		/**********************************
		* AttributeComponent
		**********************************/

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<unsigned int, unsigned int>;

		struct AttributeData
		{
			typedef std::vector<std::wstring> ValueVector;

			AttributeData(void);
			AttributeData(const std::wstring& stData);
			AttributeData(ValueVector&& vValues);
			AttributeData(AttributeData&& data);

			bool isConnected;
			ValueVector vData;
		};

		class ACCLIMATE_API AttributeComponent
		{
			typedef std::vector<EventAttribute*> AttribVector;
		public:
			typedef std::vector<AttributeData> ValueVector;

			AttributeComponent(void);
			AttributeComponent(const AttributeComponent& attribute);

			void SetupAttributes(const ValueVector& vValues);

			template<typename Type>
			void SetAttribute(unsigned int slot, const Type& value);
			template<typename Type>
			void SetAttributeArray(unsigned int slot, const std::vector<Type>& vValues);

			unsigned int GetNumAttributes(void) const;
			EventAttribute& GetAttribute(unsigned int slot);
			const EventAttribute& GetAttribute(unsigned int slot) const;
			virtual const AttributeVector& GetAttributeDeclaration(void) const = 0;
			bool NoConnected(void) const;

			void ConnectAttribute(unsigned int slot, bool isConnected);

			core::Signal<unsigned int, unsigned int> SigRequest;

		protected:

			template<typename Type>
			Type& GetAttribute(unsigned int slot) const;
			template<typename Type>
			std::vector<Type>& GetAttributeArray(unsigned int slot) const;

			void Invalidate(unsigned int slot);

		private:
#pragma warning( disable: 4251 )

			virtual unsigned int OnGetId(void) const = 0;

			void RegisterAttributeSet(void);

			AttribVector m_vAttributes;
		};

		template<typename Type>
		void AttributeComponent::SetAttribute(unsigned int slot, const Type& value)
		{
			ACL_ASSERT(slot < m_vAttributes.size());
			auto pAttribute = m_vAttributes.at(slot);
			pAttribute->SetValue<Type>(value);
			RegisterAttributeSet();
		}

		template<typename Type>
		void AttributeComponent::SetAttributeArray(unsigned int slot, const std::vector<Type>& vValues)
		{
			ACL_ASSERT(slot < m_vAttributes.size());
			auto pAttribute = m_vAttributes.at(slot);
			pAttribute->SetValueArray<Type>(vValues);
			RegisterAttributeSet();
		}

		template<typename Type>
		Type& AttributeComponent::GetAttribute(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vAttributes.size());

			auto pAttribute = m_vAttributes.at(slot);
			if(pAttribute->IsConnected())
				SigRequest(OnGetId(), slot);

			ACL_ASSERT(pAttribute->IsSet());
			ACL_ASSERT(!pAttribute->IsArray());

			if(pAttribute->GetType() == AttributeType::OBJECT)
			{
				auto pObject = ((EventObject*)pAttribute->GetData());
				if(!pObject->IsValid())
					throw ObjectInvalidException(pAttribute->GetObjectType(), slot);
			}

			return pAttribute->GetValue<Type>();
		}

		template<typename Type>
		std::vector<Type>& AttributeComponent::GetAttributeArray(unsigned int slot) const
		{
			ACL_ASSERT(slot < m_vAttributes.size());

			auto pAttribute = m_vAttributes.at(slot);
			if(pAttribute->IsConnected())
				SigRequest(OnGetId(), slot);

			ACL_ASSERT(pAttribute->IsSet());
			ACL_ASSERT(pAttribute->IsArray());

			return pAttribute->GetValueArray<Type>();
		}

		/**********************************
		* ReturnComponent
		**********************************/

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<unsigned int, unsigned int, const EventAttribute&, bool>;
		
		class ACCLIMATE_API ReturnComponent
		{
		public:
			typedef std::vector<std::pair<unsigned int, unsigned int>> ReturnTargetVector;
			typedef std::unordered_map<unsigned int, ReturnTargetVector> ReturnMap;

			ReturnComponent(void);
			ReturnComponent(const ReturnComponent& ret);

			void SetupReturns(const ReturnMap& mReturn);
			void ClearReturnTargets(void);

			void AddReturn(unsigned int slot, unsigned int target, unsigned int targetSlot);
			void RemoveReturn(unsigned int slot, unsigned int target);
			void RemoveReturn(unsigned int slot);

			unsigned int GetNumReturns(void) const;
			const ReturnTargetVector& GetReturnTargets(unsigned int slot) const;
			virtual const AttributeVector& GetReturnDeclaration(void) const = 0;

			Signal<unsigned int, unsigned int, const EventAttribute&, bool> SigReturn;

			virtual void OnReturn(void) = 0;

		protected:

			void ReturnAttribute(unsigned int slot, const EventAttribute& attribute);
			template<typename Type>
			void ReturnValue(unsigned int slot, Type& value);
			template<typename Type>
			void ReturnValue(unsigned int slot, const Type& value);
			template<>
			void ReturnValue(unsigned int slot, const bool& value);
			template<>
			void ReturnValue(unsigned int slot, const float& value);
			template<>
			void ReturnValue(unsigned int slot, const int& value);
			template<>
			void ReturnValue(unsigned int slot, const std::wstring& value);
			template<typename Type>
			void ReturnValueArray(unsigned int slot, const std::vector<Type>& vValues);

		private:
#pragma warning( disable: 4251 )

			struct ReturnData
			{
				ReturnData(void);
				ReturnData(const ReturnData& data);
				ReturnData(AttributeType type, bool isarray);
				ReturnData(unsigned int objectType, bool isarray);
				ReturnData(AttributeType type, bool isarray, const ReturnTargetVector& vTargets);
				ReturnData(unsigned int objectType, bool isarray, const ReturnTargetVector& vTargets);
				ReturnData(ReturnData&& data);
				~ReturnData(void);

				EventAttribute* pAttribute;
				ReturnTargetVector vTargets;
			};
			typedef std::unordered_map<unsigned int, ReturnData> ReturnDataMap;

			ReturnDataMap m_mReturn;
#pragma warning( default: 4251 )
		};


		template<typename Type>
		void ReturnComponent::ReturnValue(unsigned int slot, Type& value)
		{
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValue<Type>(value);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, false);
				}
			}
		}

		template<>
		void ReturnComponent::ReturnValue(unsigned int slot, const bool& value)
		{
			// TODO: check if type & array fit
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValue<bool>(value);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, false);
				}
			}
		}

		template<>
		void ReturnComponent::ReturnValue(unsigned int slot, const float& value)
		{
			// TODO: check if type & array fit
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValue<float>(value);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, false);
				}
			}
		}

		template<>
		void ReturnComponent::ReturnValue(unsigned int slot, const int& value)
		{
			// TODO: check if type & array fit
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValue<int>(value);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, false);
				}
			}
		}

		template<>
		void ReturnComponent::ReturnValue(unsigned int slot, const std::wstring& value)
		{
			// TODO: check if type & array fit
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValue<std::wstring>(value);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, false);
				}
			}
		}

		template<typename Type>
		void ReturnComponent::ReturnValueArray(unsigned int slot, const std::vector<Type>& vValues)
		{
			// TODO: check if type & array fit
			auto itr = m_mReturn.find(slot);
			if(itr != m_mReturn.end())
			{
				itr->second.pAttribute->SetValueArray<Type>(vValues);
				for(auto& target : itr->second.vTargets)
				{
					SigReturn(target.first, target.second, *itr->second.pAttribute, true);
				}
			}
		}

	}
}

