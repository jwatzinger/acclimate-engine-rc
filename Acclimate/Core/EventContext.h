#pragma once
#include "Dll.h"
#include "Events.h"

namespace acl
{
	namespace core
	{

		class EventTriggerHandler;

		struct ACCLIMATE_API EventContext
		{
			EventContext(Events& events, EventTriggerHandler& trigger) : events(events), trigger(trigger)
			{
			}

			Events& events;
			EventTriggerHandler& trigger;
		};
	}
}