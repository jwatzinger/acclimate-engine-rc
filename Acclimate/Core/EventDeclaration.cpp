#include "EventDeclaration.h"
#include "EventAttribute.h"

namespace acl
{
	namespace core
	{

		/********************************
		* AttributeDeclaration
		*********************************/

		AttributeDeclaration::AttributeDeclaration(const std::wstring& stName, AttributeType type, bool isArray) :
			stName(stName), type(type), isArray(isArray), objectId(-1)
		{
		}

		AttributeDeclaration::AttributeDeclaration(AttributeDeclaration&& declaration) : stName(std::move(declaration.stName)),
			type(declaration.type), isArray(declaration.isArray), objectId(declaration.objectId)
		{
			declaration.type = AttributeType::UNKNOWN;
			declaration.objectId = -1;
		}

		AttributeDeclaration::AttributeDeclaration(const std::wstring& stName, unsigned int objectId, bool isArray) :
			stName(stName), type(AttributeType::OBJECT), isArray(isArray), objectId(objectId)
		{
		}

		const std::wstring& AttributeDeclaration::GetName(void) const
		{
			return stName;
		}

		AttributeType AttributeDeclaration::GetType(void) const
		{
			return type;
		}

		bool AttributeDeclaration::IsArray(void) const
		{
			return isArray;
		}

		unsigned int AttributeDeclaration::GetObjectId(void) const
		{
			return objectId;
		}

		/********************************
		* EventDeclaration
		*********************************/

		EventDeclaration::EventDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns) : m_stName(stName),
			m_vAttributes(vAttributes), m_vReturns(vReturns)
		{
			m_vOutputs.emplace_back(L"");
		}

		EventDeclaration::EventDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns, const OutputVector& vOutputs) : m_stName(stName),
			m_vAttributes(vAttributes), m_vReturns(vReturns), m_vOutputs(vOutputs)
		{
		}

		const std::wstring& EventDeclaration::GetName(void) const
		{
			return m_stName;
		}

		const OutputVector& EventDeclaration::GetOutputs(void) const
		{
			return m_vOutputs;
		}

		const AttributeVector& EventDeclaration::GetAttributes(void) const
		{
			return m_vAttributes;
		}

		const AttributeVector& EventDeclaration::GetReturns(void) const
		{
			return m_vReturns;
		}


	}
}

