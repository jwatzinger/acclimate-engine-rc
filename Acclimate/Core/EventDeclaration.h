#pragma once
#include <string>
#include <vector>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		/********************************
		* AttributeDeclaration
		*********************************/

		enum class AttributeType;

		struct ACCLIMATE_API AttributeDeclaration
		{
			AttributeDeclaration(const std::wstring& stName, AttributeType type, bool isArray);
			AttributeDeclaration(const std::wstring& stName, unsigned int objectId, bool isArray);
			AttributeDeclaration(AttributeDeclaration&& declaration);

			const std::wstring& GetName(void) const;
			AttributeType GetType(void) const;
			bool IsArray(void) const;
			unsigned int GetObjectId(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring stName;
			AttributeType type;
			bool isArray;
			unsigned int objectId;
#pragma warning( default: 4251 )
		};

		typedef std::vector<AttributeDeclaration> AttributeVector;

		/********************************
		* OutputDeclaration
		*********************************/

		typedef std::vector<std::wstring> OutputVector;

		/********************************
		* EventDeclaration
		*********************************/

		class ACCLIMATE_API EventDeclaration
		{
		public:
			EventDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns);
			EventDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns, const OutputVector& vOutputs);

			const std::wstring& GetName(void) const;
			const OutputVector& GetOutputs(void) const;
			const AttributeVector& GetAttributes(void) const;
			const AttributeVector& GetReturns(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stName;
			OutputVector m_vOutputs;
			AttributeVector m_vAttributes;
			AttributeVector m_vReturns;
#pragma warning( default: 4251 )
		};

	}
}


