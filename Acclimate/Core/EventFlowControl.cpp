#include "EventFlowControl.h"
#include <minmax.h>
#include "..\System\Assert.h"

namespace acl
{
	namespace core
	{

		EventFlowControl::EventFlowControl(void) : m_time(0.0)
		{
		}

		void EventFlowControl::StartFrame(double dt)
		{
			m_time = dt;

			for(auto& time : m_vTimes)
			{
				time = dt;
			}

			if(m_vTimes.empty())
				m_vTimes.push_back(dt);
		}

		double EventFlowControl::GetTimeLeft(unsigned int event) const
		{
			ACL_ASSERT(!m_vTimes.empty());

			if(m_mTimes.count(event))
				return m_vTimes.at(m_mTimes.at(event));
			else
			{
				//ACL_ASSERT(m_vTimes.size() == 1);
				return m_vTimes.front();
			}
		}

		void EventFlowControl::Called(unsigned int source, unsigned int target)
		{
			ACL_ASSERT(!m_mTargets.count(target));

			m_mTargets.emplace(target, source);

			if(m_mReferences.count(source))
			{
				const auto numReferences = m_mReferences[source];

				ACL_ASSERT(m_mTimes.count(source));
				const auto id = m_mTimes[source];
				ACL_ASSERT(m_vTimes.size() > id);
				const auto time = m_vTimes[id];

				if(numReferences == 1)
				{
					const auto thisId = m_vTimes.size();
					m_mTimeConnections[id].push_back(thisId);
					m_mTimeConnections[thisId].push_back(id);
					m_vTimes.emplace_back(time);
				}
				const auto thisId = m_vTimes.size();
				m_mTimeConnections[id].push_back(thisId);
				m_mTimeConnections[thisId].push_back(id);
				m_vTimes.emplace_back(time);

				unsigned int count = 0;
				const unsigned int firstId = m_vTimes.size() - 2;
				for(auto& target : m_mTargets)
				{
					if(target.second == source)
					{
						count++;
						if(count == numReferences)
						{
							m_mTimes[target.first] = firstId;
							break;
						}
					}

				}

				ACL_ASSERT(count == numReferences);

				m_mTimes.emplace(target, firstId + 1);
			}
			else
			{
				auto itr = m_mTimes.find(source);
				if(itr != m_mTimes.end())
				{
					const auto id = itr->second;
					m_mTimes.emplace(target, id);
				}
				else
				{
					m_vTimes.push_back(m_time);
					const auto id = m_vTimes.size() - 1;
					m_mTimes.emplace(source, id);
					m_mTimes.emplace(target, id);
				}
			}

			m_mReferences[source]++;
		}

		void EventFlowControl::ReachedEnd(unsigned int event)
		{
			if(m_mTargets.count(event))
			{
				const auto source = m_mTargets[event];
				ACL_ASSERT(m_mReferences.count(source));

				auto& reference = m_mReferences[source];
				ACL_ASSERT(reference != 0);
				reference--;

				if(reference == 0)
				{
					m_mReferences.erase(source);
					SigFinished(source);
				}

				m_mTargets.erase(event);
			}
		}

		void EventFlowControl::TakeTime(unsigned int event, double deltaTime)
		{
			ACL_ASSERT(!m_vTimes.empty());

			auto itr = m_mTimes.find(event);

			double* pTime = nullptr;
			if(itr != m_mTimes.end())
			{
				pTime = &m_vTimes.at(itr->second);
				
				const auto source = itr->second;
				if(m_mTimeConnections.count(source))
				{
					const auto vTargets = m_mTimeConnections[source];

					// first target is always the parent
					ACL_ASSERT(!vTargets.empty());
					const auto targetId = vTargets.front();

					if(m_mTimeConnections.count(targetId))
					{
						ACL_ASSERT(m_vTimes.size() > targetId);

						double& time = m_vTimes[targetId];
						for(auto t : m_mTimeConnections[targetId])
						{
							ACL_ASSERT(m_vTimes.size() > t);
							time = min(m_vTimes[t], time);
						}
					}
				}
			}
			else
			{
				ACL_ASSERT(m_vTimes.size() == 1);
				pTime = &m_vTimes.front();
			}

			ACL_ASSERT(pTime);

			*pTime -= deltaTime;
			if(*pTime <= 0.000001)
				*pTime = 0.0;
		}

	}
}

