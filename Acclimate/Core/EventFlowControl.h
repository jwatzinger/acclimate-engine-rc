#pragma once
#include <unordered_map>
#include "Signal.h"

namespace acl
{
	namespace core
	{

		class EventFlowControl
		{
			typedef std::unordered_map<unsigned int, unsigned int> ValueMap;
			typedef std::vector<double> TimeVector;
			typedef std::unordered_map<unsigned int, std::vector<unsigned int>> ConnectionMap;
		public:

			EventFlowControl(void);

			void StartFrame(double dt);

			double GetTimeLeft(unsigned int event) const;

			void Called(unsigned int source, unsigned int target);
			void ReachedEnd(unsigned int event);
			void TakeTime(unsigned int event, double time);

			Signal<unsigned int> SigFinished;

		private:

			float m_time;
			ValueMap m_mTargets, m_mReferences, m_mTimes;
			TimeVector m_vTimes;
			ConnectionMap m_mTimeConnections;
		};

	}
}

