#include "EventFunction.h"

namespace acl
{
	namespace core
	{

		EventFunction::EventFunction(bool noAttributes) : EventCommand(SlotType::ATTRIBUTE | SlotType::RETURN, TargetType::FUNCTION),
			m_isAttributesReady(noAttributes)
		{
		}

		void EventFunction::OnReturn(void)
		{
			OnCalculate();
		}

	}
}

