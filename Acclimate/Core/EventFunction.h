#pragma once
#include "EventCommand.h"
#include "FunctionDeclaration.h"

namespace acl
{
	namespace core
	{
		
		struct AttributeData;

		class ACCLIMATE_API EventFunction :
			public EventCommand, public AttributeComponent, public ReturnComponent
		{
		public:

			virtual const FunctionDeclaration& GetDeclaration(void) const = 0;

			void OnReturn(void) override final;

		protected:

			EventFunction(bool noAttributes);

		private:

			virtual void OnCalculate(void) = 0;

			bool m_isAttributesReady;
		};

		template<typename Type>
		class BaseEventFunction :
			public EventFunction
		{
		public:

			BaseEventFunction(void) : EventFunction(m_pDeclaration->GetAttributes().empty())
			{
			}

			EventCommand& Clone(void) const override final
			{
				return *new Type(*(Type*)this);
			}

			static void SetDeclaration(const FunctionDeclaration& declaration)
			{
				m_pDeclaration = &declaration;
			}

			const FunctionDeclaration& GetDeclaration(void) const override final
			{
				ACL_ASSERT(m_pDeclaration);
				return *m_pDeclaration;
			}

			const std::wstring& GetName(void) const override final
			{
				return m_pDeclaration->GetName();
			}

			const AttributeVector& GetAttributeDeclaration(void) const override final
			{
				return m_pDeclaration->GetAttributes();
			}

			const AttributeVector& GetReturnDeclaration(void) const override final
			{
				return m_pDeclaration->GetReturns();
			}

		private:

			unsigned int OnGetId(void) const override final
			{
				return GetId();
			}

			static const FunctionDeclaration* m_pDeclaration;

		};

		template<typename Type>
		const FunctionDeclaration* BaseEventFunction<Type>::m_pDeclaration = nullptr;

	}
}

