#include "EventGraph.h"
#include "Event.h"
#include "EventInstance.h"
#include "EventVariable.h"

namespace acl
{
	namespace core
	{

		/*********************************
		* Connection
		**********************************/

		Connection::Connection(ConnectionType type, unsigned int slot, unsigned int target, unsigned int targetSlot, TargetType targetType) : type(type), 
			slot(slot), target(target), targetSlot(targetSlot), targetType(targetType)
		{
		}

		/*********************************
		* ConnectionReflector
		**********************************/

		bool ConnectionReflector::IsConnected(ConnectionType type, unsigned int slot) const
		{
			auto& connections = SelectVector(type);

			for(auto& connection : connections)
			{
				ACL_ASSERT(connection.type == type);
				if(connection.slot == slot)
					return true;
			}

			return false;
		}

		ConnectionReflector::TargetVector ConnectionReflector::GetTargets(ConnectionType type, unsigned int slot) const
		{
			TargetVector vTargets;
			
			auto& connections = SelectVector(type);
			for(auto& connection : connections)
			{
				ACL_ASSERT(connection.type == type);
				if(connection.slot == slot)
					vTargets.push_back(&connection);
			}

			return vTargets;
		}

		void ConnectionReflector::AddConnection(ConnectionType type, unsigned int slot, unsigned int target, unsigned int targetSlot, TargetType targetType)
		{
			auto& connections = SelectVector(type);

			connections.emplace_back(type, slot, target, targetSlot, targetType);
		}

#pragma warning( disable: 4715 )

		ConnectionReflector::ConnectionVector& ConnectionReflector::SelectVector(ConnectionType type)
		{
			switch(type)
			{
			case ConnectionType::INPUT:
				return m_vInput;
			case ConnectionType::OUTPUT:
				return m_vOutput;
			case ConnectionType::ATTRIBUTE:
				return m_vAttribute;
			case ConnectionType::RETURN:
				return m_vReturn;
			default:
				ACL_ASSERT(false);
			}
		}

		const ConnectionReflector::ConnectionVector& ConnectionReflector::SelectVector(ConnectionType type) const
		{
			switch(type)
			{
			case ConnectionType::INPUT:
				return m_vInput;
			case ConnectionType::OUTPUT:
				return m_vOutput;
			case ConnectionType::ATTRIBUTE:
				return m_vAttribute;
			case ConnectionType::RETURN:
				return m_vReturn;
			default:
				ACL_ASSERT(false);
			}
		}

#pragma warning (default : 4715)

		/*********************************
		* EventGraph
		**********************************/

		EventGraph::EventGraph(const EventInstance& instance)
		{
			auto& mEvents = instance.GetEvents();
			auto& mVariables = instance.GetVariables();

			// events
			for(auto& event : mEvents)
			{
				auto pEvent = event.second;
				const auto thisId = pEvent->GetId();
				const auto thisType = pEvent->GetTargetType();

				// outputs
				if(auto pOutput = pEvent->QueryOutput())
				{
					const auto numOutputs = pOutput->GetNumOutputs();
					for(unsigned int i = 0; i < numOutputs; i++)
					{
						auto id = pOutput->GetOutputTarget(i);

						if(id != Event::NO_EVENT_ID)
						{
							auto pTargetEvent = mEvents.at(id);
							ACL_ASSERT(pTargetEvent->QueryInput());

							const auto targetType = pTargetEvent->GetTargetType();

							AddConnection(id, ConnectionType::INPUT, 0, targetType, thisId, i, thisType);
							AddConnection(thisId, ConnectionType::OUTPUT, i, thisType, id, 0, targetType);
						}
					}
				}


				// returns
				if(auto pReturn = pEvent->QueryReturn())
				{
					const auto numReturns = pReturn->GetNumReturns();
					for(unsigned int i = 0; i < numReturns; i++)
					{
						auto& vTargets = pReturn->GetReturnTargets(i);
						for(auto& target : vTargets)
						{
							auto pTargetEvent = mEvents.at(target.first);
							ACL_ASSERT(pTargetEvent->QueryAttribute());
							const auto targetType = pTargetEvent->GetTargetType();

							AddConnection(target.first, ConnectionType::ATTRIBUTE, target.second, targetType, thisId, i, thisType);
							AddConnection(thisId, ConnectionType::RETURN, i, thisType, target.first, target.second, targetType);
						}
					}
				}
			}
		}

		EventGraph::~EventGraph(void)
		{
			for(auto& connection : m_mConnections)
			{
				delete connection.second;
			}
		}

		const ConnectionReflector& EventGraph::GetConnections(unsigned int id) const
		{
			auto itr = m_mConnections.find(id);
			if(itr != m_mConnections.end())
				return *itr->second;
			else
			{
				static const ConnectionReflector reflector;
				return reflector;
			}
		}

		void EventGraph::AddConnection(unsigned int event, ConnectionType type, unsigned int slot, TargetType sourceType, unsigned int target, unsigned int targetSlot, TargetType targetType)
		{
			ConnectionReflector* pReflector = nullptr;

			auto itr = m_mConnections.find(event);
			if(itr != m_mConnections.end())
				pReflector = itr->second;
			else
			{
				pReflector = new ConnectionReflector;
				m_mConnections.emplace(event, pReflector);
			}

			pReflector->AddConnection(type, slot, target, targetSlot, targetType);
		}

	}
}

