#pragma once
#include <vector>
#include <unordered_map>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		/*********************************
		* Connection
		**********************************/

		enum class TargetType;

		enum class ConnectionType
		{
			INPUT, OUTPUT, ATTRIBUTE, RETURN
		};

		struct ACCLIMATE_API Connection
		{
			Connection(ConnectionType type, unsigned int slot, unsigned int target, unsigned int targetSlot, TargetType targetType);

			ConnectionType type;
			unsigned int slot, target, targetSlot;
			TargetType targetType;
		};

		class EventInstance;

		/*********************************
		* ConnectionReflector
		**********************************/

		class ACCLIMATE_API ConnectionReflector
		{
			typedef std::vector<Connection> ConnectionVector;
		public:
			typedef std::vector<const Connection*> TargetVector;

			bool IsConnected(ConnectionType type, unsigned int slot) const;
			TargetVector GetTargets(ConnectionType type, unsigned int slot) const;

			void AddConnection(ConnectionType type, unsigned int slot, unsigned int target, unsigned int targetSlot, TargetType targetType);

		private:
#pragma warning( disable: 4251 )
			ConnectionVector& SelectVector(ConnectionType type);
			const ConnectionVector& SelectVector(ConnectionType type) const;

			ConnectionVector m_vInput, m_vOutput, m_vAttribute, m_vReturn;
#pragma warning( default: 4251 )
		};

		/*********************************
		* EventGraph
		**********************************/

		class ACCLIMATE_API EventGraph
		{
			typedef std::unordered_map<unsigned int, ConnectionReflector*> ConnectionMap;
		public:

			EventGraph(const EventInstance& instance);
			~EventGraph(void);

			const ConnectionReflector& GetConnections(unsigned int id) const;

		private:
#pragma warning( disable: 4251 )
			void AddConnection(unsigned int event, ConnectionType type, unsigned int slot, TargetType sourceType, unsigned int target, unsigned int targetSlot, TargetType targetType);

			ConnectionMap m_mConnections;
#pragma warning( default: 4251 )
		};

	}
}


