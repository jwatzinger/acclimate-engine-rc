#include "EventInstance.h"
#include "Event.h"
#include "EventVariable.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		EventInstance::EventInstance(void) : EventInstance(0, 0)
		{
		}

		EventInstance::EventInstance(unsigned int startUid, unsigned int startVariableUID) : m_uid(startUid),
			m_isRunning(false), m_variableUID(startVariableUID), m_pNextFlowEvent(nullptr), m_pGraph(nullptr)
		{
			m_flow.SigFinished.Connect(this, &EventInstance::OnFlowControlFinished);
		}

		EventInstance::~EventInstance(void)
		{
			for(auto event : m_mEvents)
			{
				delete event.second;
			}

			for(auto& variable : m_mVariables)
			{
				delete variable.second;
			}

			delete m_pGraph;
		}

		EventInstance::EventInstance(const EventInstance& instance) : m_uid(instance.m_uid),
			m_isRunning(instance.m_isRunning), m_flow(), m_pNextFlowEvent(nullptr)
		{
			m_flow.SigFinished.Connect(this, &EventInstance::OnFlowControlFinished);

			for(auto event : instance.m_mEvents)
			{
				auto pEvent = &event.second->Clone();
				OnAddEvent(*pEvent);
			}

			for(auto trigger : instance.m_mTrigger)
			{
				auto pTrigger = m_mEvents.at(trigger.second->GetId());
#ifdef _DEBUG
				m_mTrigger.emplace(trigger.first, dynamic_cast<EventTrigger*>(pTrigger));
#else
				m_mTrigger.emplace(trigger.first, (EventTrigger*)pTrigger);
#endif
			}

			for(auto& variable : instance.m_mVariables)
			{
				auto pVariable = new EventVariable(*variable.second);
				m_mVariables.emplace(variable.first, pVariable);
			}

			for(auto& getter : instance.m_mGetter)
			{
#ifdef _DEBUG
				auto pGetter = dynamic_cast<VariableGetter*>(m_mEvents.at(getter.first));
				ACL_ASSERT(pGetter->GetTargetType() == TargetType::GETTER);
#else
				auto pGetter = (VariableGetter*)m_mEvents.at(getter.first);
#endif
				auto pVariable = m_mVariables.at(getter.second->GetVariableId());
				pGetter->SetVariable(*pVariable);
				
				m_mGetter.emplace(getter.first, pGetter);
			}

			for(auto& setter : instance.m_mSetter)
			{
#ifdef _DEBUG
				auto pSetter = dynamic_cast<VariableSetter*>(m_mEvents.at(setter.first));
				ACL_ASSERT(pSetter->GetTargetType() == TargetType::SETTER);
#else
				auto pSetter = (VariableSetter*)m_mEvents.at(setter.first);
#endif
				auto pVariable = m_mVariables.at(setter.second->GetVariableId());
				pSetter->SetVariable(*pVariable);

				m_mSetter.emplace(setter.first, pSetter);
			}
			
			for(auto pEvent : instance.m_vActiveEvents)
			{
				auto pThisEvent = m_mEvents.at(pEvent->GetId());
				ACL_ASSERT(pThisEvent->QueryInput());
				m_vActiveEvents.push_back(pThisEvent);
			}

			m_pGraph = new EventGraph(*this);
		}

		void EventInstance::SetEntity(ecs::EntityHandle& entity)
		{
			if(m_entity != entity)
			{
				m_entity = entity;
				// TODO: apply to all events
				for(auto event : m_mEvents)
				{
					event.second->SetEntity(m_entity);
				}
			}
		}

		EventCommand& EventInstance::GetEvent(unsigned int id) const
		{
			auto itr = m_mEvents.find(id);
			ACL_ASSERT(itr != m_mEvents.end());
			return *itr->second;
		}

		const EventInstance::EventMap& EventInstance::GetEvents(void) const
		{
			return m_mEvents;
		}

		unsigned int EventInstance::GenerateNextUID(void)
		{
			return m_uid++;
		}

		unsigned int EventInstance::GetUID(void) const
		{
			return m_uid;
		}

		unsigned int EventInstance::GenerateNextVariableUID(void)
		{
			return m_variableUID++;
		}

		unsigned int EventInstance::GetVariableUID(void) const
		{
			return m_variableUID;
		}

		EventInstance::TriggerVector EventInstance::GetTriggerTypes(void) const
		{
			EventInstance::TriggerVector vTrigger;
			vTrigger.reserve(m_mTrigger.size());

			for(auto trigger : m_mTrigger)
			{
				ACL_ASSERT(trigger.first == trigger.second->GetType());
				vTrigger.push_back(trigger.first);
			}
			return vTrigger;
		}

		EventVariable& EventInstance::GetVariable(unsigned int id) const
		{
			return *m_mVariables.at(id);
		}

		const EventInstance::VariableMap& EventInstance::GetVariables(void) const
		{
			return m_mVariables;
		}

		void EventInstance::AddEvent(EventCommand& event)
		{
			const auto id = event.GetId();
			if(!m_mEvents.count(id))
			{
				OnAddEvent(event);

				const auto type = event.GetTargetType();
				switch(type)
				{
				case TargetType::COMMAND:
					break;
				case TargetType::TRIGGER:
				{
#ifdef _DEBUG
					auto* pTrigger = dynamic_cast<EventTrigger*>(&event);
#else
					auto* pTrigger = (EventTrigger*)&event;
#endif
					ACL_ASSERT(pTrigger);

					m_mTrigger.emplace(pTrigger->GetType(), pTrigger);
					break;
				}
				case TargetType::GETTER:
				{
#ifdef _DEBUG
					auto* pGetter = dynamic_cast<VariableGetter*>(&event);
#else
					auto* pGetter = (VariableGetter*)&event;
#endif
					ACL_ASSERT(pGetter);

					m_mGetter.emplace(id, pGetter);
					break;
				}
				case TargetType::SETTER:
				{
#ifdef _DEBUG
					auto* pSetter = dynamic_cast<VariableSetter*>(&event);
#else
					auto* pSetter = (VariableSetter*)&event;
#endif
					ACL_ASSERT(pSetter);

					m_mSetter.emplace(id, pSetter);
					break;
				}
				case TargetType::FUNCTION:
					break;
				default:
					ACL_ASSERT(false);
				}

			}
			else
			{
				if(m_mEvents[id] == &event)
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add event with id", id, "to event instance. Event is already added to this instance.");
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add event with id", id, "to event instance. Event with same id already exists.");
			}
		}

		void EventInstance::RemoveEvent(EventCommand& event)
		{
			const auto id = event.GetId();
			ACL_ASSERT(m_mEvents.count(id));
			ACL_ASSERT(m_mEvents.find(id)->second == &event);

			switch(event.GetTargetType())
			{
			case TargetType::COMMAND:
				break;
			case TargetType::TRIGGER:
			{
#ifdef _DEBUG
				auto pTrigger = dynamic_cast<EventTrigger*>(&event);
#else
				auto pTrigger = (EventTrigger*)&event;
#endif
				ACL_ASSERT(pTrigger);
				m_mTrigger.erase(pTrigger->GetType());
				break;
			}
			case TargetType::GETTER:
				ACL_ASSERT(m_mGetter.count(id));
				m_mGetter.erase(id);
				break;
			case TargetType::SETTER:
				ACL_ASSERT(m_mSetter.count(id));
				m_mSetter.erase(id);
				break;
			case TargetType::FUNCTION:
				break;
			default:
				ACL_ASSERT(false);
			}

			// in case we removed the last event, we can safely decrement the uid-counter, this keeps it at a lower level
			if(id == m_uid - 1)
				m_uid--;

			delete m_mEvents.at(id);
			m_mEvents.erase(id);
		}

		void EventInstance::AddVariable(EventVariable& variable)
		{
			auto id = variable.GetId();
			ACL_ASSERT(!m_mVariables.count(id));

			m_mVariables.emplace(id, &variable);
		}

		void EventInstance::RemoveVariable(EventVariable& variable)
		{
			auto id = variable.GetId();
			ACL_ASSERT(m_mVariables.count(id));
			ACL_ASSERT(m_mVariables.at(id) == &variable);

			// in case we removed the last variable, we can safely decrement the uid-counter, this keeps it at a lower level
			if(id == m_variableUID - 1)
				m_variableUID--;

			delete &variable;
			m_mVariables.erase(id);
		}

		void EventInstance::Run(double dt)
		{
			m_flow.StartFrame(dt);

			for(auto pEvent : m_vOutputEvents)
			{
				if(pEvent)
				{
					auto pInput = pEvent->QueryInput();
					ACL_ASSERT(pInput);

					pInput->Reset();
					m_vActiveEvents.push_back(pEvent);
				}
			}
			m_vOutputEvents.clear();

			EventVector vEventsToProcess(m_vActiveEvents.begin(), m_vActiveEvents.end());

			while(!vEventsToProcess.empty())
			{
				EventVector vNextEvents;

				for(auto pEvent : vEventsToProcess)
				{
					const auto id = pEvent->GetId();

					try
					{
						auto pInput = pEvent->QueryInput();
						ACL_ASSERT(pInput);

						const auto time = m_flow.GetTimeLeft(id);
						if(time <= 0.0)
							continue;

						pInput->Execute(time);

						bool anyNonNull = false;

						for(auto pNewEvent : m_vOutputEvents)
						{
							if(pNewEvent)
							{
								auto pNewInput = pNewEvent->QueryInput();
								ACL_ASSERT(pNewInput);
								ACL_ASSERT(pNewEvent != pEvent);
								// push the next event to the stack in case there is one
								if(pNewEvent)
								{
									pNewInput->Reset();
									m_vActiveEvents.push_back(pNewEvent);
									vNextEvents.push_back(pNewEvent);
								}

								m_flow.Called(id, pNewEvent->GetId());

								anyNonNull = true;
							}
						}

						if(!m_vOutputEvents.empty())
						{
							if(!anyNonNull)
							{
								m_flow.ReachedEnd(id);
								if(m_pNextFlowEvent)
								{
									vNextEvents.push_back(m_pNextFlowEvent);
									m_vActiveEvents.push_back(m_pNextFlowEvent);
									m_pNextFlowEvent = nullptr;
								}
							}

							auto itr = std::find(m_vActiveEvents.begin(), m_vActiveEvents.end(), pEvent);
							ACL_ASSERT(itr != m_vActiveEvents.end());
							m_vActiveEvents.erase(itr);
						}

						m_vOutputEvents.clear();
					}
					catch(ObjectInvalidException& exception)
					{
						sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Failed to execute event", id, "in event instance. Object at attribute slot", exception.GetSlot(), "is invalid (object type", exception.GetType(), ")");

						auto itr = std::find(m_vActiveEvents.begin(), m_vActiveEvents.end(), pEvent);
						ACL_ASSERT(itr != m_vActiveEvents.end());
						m_vActiveEvents.erase(itr);
					}
					catch(AttributesNotSetException& exc)
					{
						sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Failed to execute event", id, "in event instance. Not all attributes are set, because some connected events aren't executed already.");

						auto itr = std::find(m_vActiveEvents.begin(), m_vActiveEvents.end(), pEvent);
						ACL_ASSERT(itr != m_vActiveEvents.end());
						m_vActiveEvents.erase(itr);
					}
				}

				// set next events
				vEventsToProcess = vNextEvents;
			}
		}

		template<typename Type>
		void setEventAttribute(EventCommand& event, unsigned int slot, const EventAttribute& attribute)
		{
			auto pAttribute = event.QueryAttribute();
			ACL_ASSERT(pAttribute);
			pAttribute->SetAttribute<Type>(slot, attribute.GetValue<Type>());
		}

		template<typename Type>
		void setEventAttributeArray(EventCommand& event, unsigned int slot, const EventAttribute& attribute)
		{
			auto pAttribute = event.QueryAttribute();
			ACL_ASSERT(pAttribute);
			pAttribute->SetAttributeArray<Type>(slot, attribute.GetValueArray<Type>());
		}

		void EventInstance::OnAddEvent(EventCommand& event)
		{
			if(auto pOutput = event.QueryOutput())
				pOutput->SigOutput.Connect(this, &EventInstance::OnHandleOutput);
			if(auto pReturn = event.QueryReturn())
				pReturn->SigReturn.Connect(this, &EventInstance::OnHandleReturn);
			if(auto pInput = event.QueryInput())
				pInput->SigTakeTime.Connect(this, &EventInstance::OnTakeTime);
			if(auto pAttribute = event.QueryAttribute())
				pAttribute->SigRequest.Connect(this, &EventInstance::OnRequestReturn);
			m_mEvents.emplace(event.GetId(), &event);
		}

		void EventInstance::OnHandleReturn(unsigned int target, unsigned int slot, const EventAttribute& attribute, bool isArray)
		{
			auto& event = GetEvent(target);

			const auto type = attribute.GetType();
			if(isArray)
			{
				switch(type)
				{
				case AttributeType::BOOL:
					setEventAttributeArray<bool>(event, slot, attribute);
					break;
				case AttributeType::FLOAT:
					setEventAttributeArray<float>(event, slot, attribute);
					break;
				case AttributeType::INT:
					setEventAttributeArray<int>(event, slot, attribute);
					break;
				case AttributeType::STRING:
					setEventAttributeArray<std::wstring>(event, slot, attribute);
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(type)
				{
				case AttributeType::BOOL:
					setEventAttribute<bool>(event, slot, attribute);
					break;
				case AttributeType::FLOAT:
					setEventAttribute<float>(event, slot, attribute);
					break;
				case AttributeType::INT:
					setEventAttribute<int>(event, slot, attribute);
					break;
				case AttributeType::STRING:
					setEventAttribute<std::wstring>(event, slot, attribute);
					break;
				case AttributeType::OBJECT:
				{
					auto pAttribute = event.QueryAttribute();
					ACL_ASSERT(pAttribute);
					pAttribute->SetAttribute<EventObject>(slot, *(EventObject*)attribute.GetData());
					break;
				}
				default:
					ACL_ASSERT(false);
				}
			}
		}

		void EventInstance::OnHandleOutput(unsigned int target)
		{
			if(target != EventCommand::NO_EVENT_ID)
			{
				auto itr = m_mEvents.find(target);

				if(itr != m_mEvents.end())
				{
					ACL_ASSERT(itr->second->QueryInput());
					m_vOutputEvents.push_back(itr->second);
				}
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to switch to event with id", target, "in event instance. ID not found.");
			}
			else
				m_vOutputEvents.push_back(nullptr);
		}

		void EventInstance::OnRequestReturn(unsigned int source, unsigned int slot)
		{
			auto& connection = m_pGraph->GetConnections(source);
			auto& vTargets = connection.GetTargets(core::ConnectionType::ATTRIBUTE, slot);
			ACL_ASSERT(vTargets.size() == 1);
			
			auto pTarget = vTargets.front();
			
			auto pEvent = m_mEvents[pTarget->target];
			auto pReturn = pEvent->QueryReturn();
			ACL_ASSERT(pReturn);

			pReturn->OnReturn();
		}

		void EventInstance::OnFlowControlFinished(unsigned int event)
		{
			ACL_ASSERT(event != EventCommand::NO_EVENT_ID);
			ACL_ASSERT(m_mEvents.count(event));

			auto pEvent = m_mEvents[event];
			auto pOutput = pEvent->QueryOutput();
			ACL_ASSERT(pOutput);

			if(pOutput->HasFlowControl())
			{
				ACL_ASSERT(pEvent->QueryInput());
				ACL_ASSERT(std::find(m_vActiveEvents.begin(), m_vActiveEvents.end(), pEvent) == m_vActiveEvents.end());

				ACL_ASSERT(!m_pNextFlowEvent);
				m_pNextFlowEvent = pEvent;
			}
			else
				m_flow.ReachedEnd(event);
		}

		void EventInstance::OnTakeTime(unsigned int event, double time)
		{
			m_flow.TakeTime(event, time);
		}

	}
}

