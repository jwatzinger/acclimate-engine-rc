#pragma once
#include <unordered_map>
#include "Dll.h"
#include "Event.h"
#include "EventTrigger.h"
#include "EventFlowControl.h"
#include "..\Entity\Entity.h"

// TODO: find better solution for return request
#include "EventGraph.h"

namespace acl
{
	namespace core
	{

		class Event;
		class EventVariable;
		class VariableGetter;
		class VariableSetter;
		enum class AttributeType;

		class ACCLIMATE_API EventInstance
		{
			typedef std::vector<EventCommand*> EventVector;
			typedef std::unordered_map<EventTrigger::Type, EventTrigger*> TriggerMap;
			typedef std::unordered_map<unsigned int, VariableGetter*> GetterMap;
			typedef std::unordered_map<unsigned int, VariableSetter*> SetterMap;
		public:
			typedef std::unordered_map<unsigned int, EventCommand*> EventMap;
			typedef std::unordered_map<unsigned int, EventVariable*> VariableMap;
			typedef std::vector<EventTrigger::Type> TriggerVector;
			typedef std::vector<EventAttribute> AttributeVector;

			EventInstance(void);
			EventInstance(unsigned int startUid, unsigned int startVariableUID);
			~EventInstance(void);
			EventInstance(const EventInstance& instance);

			void SetEntity(ecs::EntityHandle& entity);
			void AddEvent(EventCommand& event);
			void RemoveEvent(EventCommand& event);
			void AddVariable(EventVariable& variable);
			void RemoveVariable(EventVariable& variable);

			EventCommand& GetEvent(unsigned int id) const;
			const EventMap& GetEvents(void) const;
			EventVariable& GetVariable(unsigned int id) const;
			const VariableMap& GetVariables(void) const;
			unsigned int GenerateNextUID(void);
			unsigned int GetUID(void) const;
			unsigned int GenerateNextVariableUID(void);
			unsigned int GetVariableUID(void) const;
			TriggerVector GetTriggerTypes(void) const;
			template<typename Trigger>
			bool HasTrigger(void) const;

			void Run(double dt);

			template<typename Trigger>
			void Trigger(void);
			template<typename Trigger>
			void Trigger(const AttributeVector& vAttributes);

		private:
#pragma warning( disable: 4251 )

			void OnAddEvent(EventCommand& event);
			void OnHandleReturn(unsigned int target, unsigned int slot, const EventAttribute& attribute, bool isArray);
			void OnHandleOutput(unsigned int target);
			void OnRequestReturn(unsigned int source, unsigned int slot);
			void OnFlowControlFinished(unsigned int event);
			void OnTakeTime(unsigned int event, double time);

			EventMap m_mEvents;
			TriggerMap m_mTrigger;
			GetterMap m_mGetter;
			SetterMap m_mSetter;
			VariableMap m_mVariables;
			EventVector m_vActiveEvents, m_vOutputEvents;
			EventCommand* m_pNextFlowEvent;
			EventGraph* m_pGraph;

			unsigned int m_uid, m_variableUID;
			bool m_isRunning;

			ecs::EntityHandle m_entity;

			EventFlowControl m_flow;
#pragma warning( default: 4251 )
		};

		template<typename TriggerType>
		bool EventInstance::HasTrigger(void) const
		{
			return m_mTrigger.count(TriggerType::type()) != 0;
		}

		template<typename Trigger>
		void EventInstance::Trigger(void)
		{
			const auto type = Trigger::type();
			auto itr = m_mTrigger.find(type);
			if(itr != m_mTrigger.end())
				itr->second->Trigger();
		}

		template<typename Trigger>
		void EventInstance::Trigger(const AttributeVector& vAttributes)
		{
			const auto type = Trigger::type();
			auto itr = m_mTrigger.find(type);
			if(itr != m_mTrigger.end())
				itr->second->Trigger(vAttributes);
		}

	}
}

