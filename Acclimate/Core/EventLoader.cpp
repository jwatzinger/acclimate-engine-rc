#include "EventLoader.h"
#include "Event.h"
#include "EventInstance.h"
#include "EventVariable.h"
#include "..\System\Log.h"
#include "..\System\Convert.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		void parseReturns(const xml::Node::NodeVector& vReturnNodes, Event::ReturnMap& mReturns)
		{
			for(auto pReturn : vReturnNodes)
			{
				std::vector<std::pair<unsigned int, unsigned int>> vTargets;
				if(auto pTargets = pReturn->Nodes(L"Target"))
				{
					for(auto pTarget : *pTargets)
					{
						vTargets.emplace_back(conv::FromString<unsigned int>(pTarget->GetValue()), pTarget->Attribute(L"slot")->AsInt());
					}
				}
				mReturns.emplace(pReturn->Attribute(L"slot")->AsInt(), vTargets);
			}
		}

		template<typename Type>
		void parseVariableArrayValues(const xml::Node::NodeVector& vValueNodes, EventAttribute& attribute)
		{
			auto& vValues = attribute.GetValueArray<Type>();
			for(auto pValue : vValueNodes)
			{
				vValues.push_back(conv::FromString<Type>(pValue->GetValue()));
			}
		}

		EventLoader::EventLoader(Events& events) : m_pEvents(&events)
		{
		}

		void EventLoader::Load(const std::wstring& stName) const
		{
			xml::Doc doc;
			doc.LoadFile(stName);

			if(auto pRoot = doc.Root(L"Events"))
			{
				if(auto pEvents = pRoot->Nodes(L"Event"))
				{
					for(auto pEvent : *pEvents)
					{
						const std::wstring& stEventName = *pEvent->Attribute(L"name");
						const auto uid = pEvent->Attribute(L"uid")->AsInt();
						const auto variableUid = pEvent->Attribute(L"varUid")->AsInt();

						auto& instance = *new EventInstance(uid, variableUid);
						m_pEvents->Add(stEventName, instance);

						/**********************************
						* Commands
						***********************************/

						if(auto pCommands = pEvent->FirstNode(L"Commands"))
							ParseEventCommand(instance, *pCommands, stName, TargetType::COMMAND);
						if(auto pCommands = pEvent->FirstNode(L"Trigger"))
							ParseEventCommand(instance, *pCommands, stName, TargetType::TRIGGER);
						if(auto pFunctions = pEvent->FirstNode(L"Functions"))
							ParseEventCommand(instance, *pFunctions, stName, TargetType::FUNCTION);

						/**********************************
						* Variables
						***********************************/

						if(auto pVariables = pEvent->FirstNode(L"Variables"))
						{
							if(auto pVariable = pVariables->Nodes(L"Variable"))
							{
								for(auto pVariableNode : *pVariable)
								{
									const auto& stName = pVariableNode->Attribute(L"name")->GetValue();

									const unsigned int type = pVariableNode->Attribute(L"type")->AsInt();
									if(type > 4)
										sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Invalid event attribute type", type, "for variable", stName, "while loading event", stEventName);
									else
									{
										const auto isConst = pVariableNode->Attribute(L"isConst")->AsBool();
										const auto id = pVariableNode->Attribute(L"id")->AsInt();

										const auto isArray = pVariableNode->Attribute(L"isArray")->AsBool();

										EventVariable* pVariable;
										if(type == 4)
										{
											const auto& stObject = pVariableNode->Attribute(L"object")->GetValue();

											const auto objectId = m_mObjectTypes.at(stObject);
											pVariable = new EventVariable(stName, objectId, isArray, id);
										}
										else
										{
											pVariable = new EventVariable(stName, (AttributeType)type, isConst, isArray, id);
											if((AttributeType)type != AttributeType::OBJECT)
											{
												if(!isArray)
												{
													const auto& stValue = pVariableNode->FirstNode(L"Value")->GetValue();

													switch((AttributeType)type)
													{
													case AttributeType::BOOL:
														pVariable->SetValue(conv::FromString<bool>(stValue));
														break;
													case AttributeType::FLOAT:
														pVariable->SetValue(conv::FromString<float>(stValue));
														break;
													case AttributeType::INT:
														pVariable->SetValue(conv::FromString<int>(stValue));
														break;
													case AttributeType::STRING:
														pVariable->SetValue(stValue);
														break;
													default:
														ACL_ASSERT(false);
													}
												}
												else
												{
													if(auto pValues = pVariableNode->Nodes(L"Value"))
													{
														auto& attribute = pVariable->GetAttribute();
														switch((AttributeType)type)
														{
														case AttributeType::BOOL:
															parseVariableArrayValues<bool>(*pValues, attribute);
															break;
														case AttributeType::FLOAT:
															parseVariableArrayValues<float>(*pValues, attribute);
															break;
														case AttributeType::INT:
															parseVariableArrayValues<int>(*pValues, attribute);
															break;
														case AttributeType::STRING:
															parseVariableArrayValues<std::wstring>(*pValues, attribute);
															break;
														default:
															ACL_ASSERT(false);
														}
													}
												}
											}
										}

										instance.AddVariable(*pVariable);
									}
								}
							}
						}

						if(auto pGetter = pEvent->FirstNode(L"Getters"))
							ParseEventCommand(instance, *pGetter, stName, TargetType::GETTER);
						if(auto pSetter = pEvent->FirstNode(L"Setters"))
							ParseEventCommand(instance, *pSetter, stName, TargetType::SETTER);
					}
				}
			}
		}

		void setupDefault(AttributeComponent& attributes)
		{
			Event::ValueVector vAttributes;
			for(auto& attribute : attributes.GetAttributeDeclaration())
			{
				if(attribute.IsArray())
					vAttributes.emplace_back(AttributeData::ValueVector());
				else
				{
					switch(attribute.GetType())
					{
					case AttributeType::BOOL:
						vAttributes.emplace_back(L"0");
						break;
					case AttributeType::FLOAT:
						vAttributes.emplace_back(L"0.0f");
						break;
					case AttributeType::INT:
						vAttributes.emplace_back(L"0");
						break;
					case AttributeType::STRING:
						vAttributes.emplace_back(L"");
						break;
					case AttributeType::OBJECT:
						vAttributes.emplace_back();
						break;
					default:
						ACL_ASSERT(false);
					}
				}
			}

			attributes.SetupAttributes(vAttributes);
		}

		Event& EventLoader::Add(const std::wstring& stName, EventInstance& instance)
		{
			const auto pFunction = m_mFunctions.at(stName);
			auto& event = pFunction();
			// id
			const auto uid = instance.GenerateNextUID();
			event.SetId(uid);

			// default attributes - TODO: add to event class instead, this can probably allow us to skip the whole string-parsing process
			setupDefault(event);

			instance.AddEvent(event);

			return event;
		}

		EventTrigger& EventLoader::AddTrigger(const std::wstring& stName, EventInstance& instance)
		{
			const auto pFunction = m_mTriggerFunctions.at(stName);
			auto& trigger = pFunction();
			// id
			const auto uid = instance.GenerateNextUID();
			trigger.SetId(uid);

			instance.AddEvent(trigger);

			return trigger;
		}

		EventFunction& EventLoader::AddFunction(const std::wstring& stName, EventInstance& instance)
		{
			const auto pFunction = m_mFunctionFunctions.at(stName);
			auto& function = pFunction();
			// id
			const auto uid = instance.GenerateNextUID();
			function.SetId(uid);

			setupDefault(function);

			instance.AddEvent(function);

			return function;
		}

		EventObject& EventLoader::CreateObject(unsigned int objectId)
		{
			auto pFunction = m_mObjectFunctions.at(objectId);
			
			return pFunction();
		}

		EventObjectArray& EventLoader::CreateObjectArray(unsigned int objectId)
		{
			auto pFunction = m_mObjectArrayFunctions.at(objectId);

			return pFunction();
		}

		EventLoader::NameVector EventLoader::GetRegisteredEvents(void)
		{
			NameVector vEvents;

			for(auto& function : m_mFunctions)
			{
				vEvents.push_back(function.first);
			}

			return vEvents;
		}

		const EventLoader::TriggerMap& EventLoader::GetRegisteredTriggers(void)
		{
			return m_mTriggers;
		}

		EventLoader::NameVector EventLoader::GetRegisteredFunctions(void)
		{
			NameVector vEvents;

			for(auto& function : m_mFunctionFunctions)
			{
				vEvents.push_back(function.first);
			}

			return vEvents;
		}

		const EventLoader::ObjectTypeMap& EventLoader::GetRegisteredObjects(void)
		{
			return m_mObjectTypes;
		}

		const FunctionDeclaration& EventLoader::GetFunctionDeclaration(const std::wstring& stName)
		{
			return *m_mFunctionDeclaration.at(stName);
		}

		void EventLoader::ParseEventCommand(EventInstance& instance, xml::Node& node, const std::wstring& stEventName, TargetType type) const
		{
			for(auto pNode : node.GetNodes())
			{
				const auto& stName = pNode->GetName();

				EventCommand* pEvent;
				switch(type)
				{
				case TargetType::COMMAND:
					if(m_mFunctions.count(stName))
						pEvent = &m_mFunctions.at(stName)();
					else
						sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to load event command for event", stEventName, ". Command type", stName, "does not exist.");
					break;
				case TargetType::TRIGGER:
					if(m_mTriggerFunctions.count(stName))
						pEvent = &m_mTriggerFunctions.at(stName)();
					else
						sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to load event trigger for event", stEventName, ". Trigger type", stName, "does not exist.");
					break;
				case TargetType::GETTER:
					pEvent = &instance.GetVariable(pNode->Attribute(L"varId")->AsInt()).CreateGetter();
					break;
				case TargetType::FUNCTION:
					if(m_mFunctionFunctions.count(stName))
						pEvent = &m_mFunctionFunctions.at(stName)();
					else
						sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to load event function for event", stEventName, ". Function type", stName, "does not exist.");
					break;				
				case TargetType::SETTER:
					pEvent = &instance.GetVariable(pNode->Attribute(L"varId")->AsInt()).CreateSetter();
					break;
				default:
					ACL_ASSERT(false);
				}

				const auto id = pNode->Attribute(L"id")->AsInt();
				pEvent->SetId(id);

				// parse attributes
				if(auto pAttributes = pNode->Nodes(L"Attribute"))
				{
					auto pAttributeComp = pEvent->QueryAttribute();
					ACL_ASSERT(pAttributeComp);

					Event::ValueVector vValues;
					for(auto pAttribute : *pAttributes)
					{
						if(auto pConnected = pAttribute->Attribute(L"connected"))
						{
							if(pConnected->AsBool())
								vValues.emplace_back();
							else
								vValues.emplace_back(pAttribute->GetValue());
						}
						else if(auto pValues = pAttribute->Nodes(L"Value"))
						{
							AttributeData::ValueVector vAttributeValues;
							for(auto pValue : *pValues)
							{
								vAttributeValues.push_back(pValue->GetValue());
							}
							vValues.emplace_back(std::move(vAttributeValues));
						}
						else
							vValues.emplace_back(pAttribute->GetValue());
					}

					pAttributeComp->SetupAttributes(vValues);
				}

				// parse outputs
				if(auto pOutputs = pNode->Nodes(L"Output"))
				{
					auto pOutputComp = pEvent->QueryOutput();
					ACL_ASSERT(pOutputComp);

					Event::OutputMap mOutputs;
					for(auto pOutput : *pOutputs)
					{
						mOutputs.emplace(conv::FromString<unsigned int>(pOutput->GetValue()), pOutput->Attribute(L"target")->AsType<unsigned int>());
					}
					pOutputComp->SetupOutputs(mOutputs);
				}

				// parse returns
				if(auto pReturns = pNode->Nodes(L"Return"))
				{
					auto pReturnComp = pEvent->QueryReturn();
					ACL_ASSERT(pReturnComp);

					Event::ReturnMap mReturns;
					parseReturns(*pReturns, mReturns);
					pReturnComp->SetupReturns(mReturns);
				}

				// position
				if(auto pPosition = pNode->FirstNode(L"Position"))
					pEvent->SetPosition(math::Vector2(pPosition->Attribute(L"x")->AsInt(), pPosition->Attribute(L"y")->AsInt()));

				instance.AddEvent(*pEvent);
			}
		}

		EventLoader::FunctionMap EventLoader::m_mFunctions;
		EventLoader::TriggerFunctionMap EventLoader::m_mTriggerFunctions;
		EventLoader::TriggerMap EventLoader::m_mTriggers;
		EventLoader::FunctionFunctionMap EventLoader::m_mFunctionFunctions;
		EventLoader::ObjectFunctionMap EventLoader::m_mObjectFunctions;
		EventLoader::ObjectTypeMap EventLoader::m_mObjectTypes;
		EventLoader::ObjectArrayMap EventLoader::m_mObjectArrayFunctions;
		EventLoader::FunctionDeclarationMap EventLoader::m_mFunctionDeclaration;

	}
}
