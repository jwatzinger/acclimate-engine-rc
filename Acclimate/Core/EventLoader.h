#pragma once
#include <string>
#include <unordered_map>
#include "Events.h"
#include "EventDeclaration.h"
#include "EventTrigger.h"
#include "EventFunction.h"
#include "EventObject.h"
#include "TriggerDeclaration.h"

namespace acl
{
	namespace xml
	{
		class Node;
	}

	namespace core
	{

		class ACCLIMATE_API EventLoader
		{
		private:
			typedef Event&(*EventLoadFunction)(void);
			typedef EventTrigger&(*TriggerLoadFunction)(void);
			typedef EventFunction&(*FunctionLoadFunction)(void);
			typedef EventObject&(*ObjectLoadFunction)(void);
			typedef EventObjectArray&(*ObjectArrayLoadFunction)(void);
			typedef std::unordered_map<std::wstring, EventLoadFunction> FunctionMap;
			typedef std::unordered_map<std::wstring, TriggerLoadFunction> TriggerFunctionMap;
			typedef std::unordered_map<std::wstring, FunctionLoadFunction> FunctionFunctionMap;
			typedef std::unordered_map<unsigned int, ObjectLoadFunction> ObjectFunctionMap;
			typedef std::unordered_map<unsigned int, ObjectArrayLoadFunction> ObjectArrayMap;
			typedef std::unordered_map<std::wstring, const FunctionDeclaration*> FunctionDeclarationMap;
		public:
			typedef std::vector<std::wstring> NameVector;
			typedef std::unordered_map<unsigned int, std::wstring> TriggerMap;
			typedef std::unordered_map<std::wstring, unsigned int> ObjectTypeMap;

			EventLoader(Events& events);

			template<typename EventType>
			static void RegisterEvent(void);
			template<typename TriggerType>
			static void RegisterTrigger(void);
			template<typename FunctionType>
			static void RegisterFunction(void);
			template<typename ObjectType>
			static void RegisterObject(const std::wstring& stName);

			static NameVector GetRegisteredEvents(void);
			static const TriggerMap& GetRegisteredTriggers(void);
			static NameVector GetRegisteredFunctions(void);
			static const ObjectTypeMap& GetRegisteredObjects(void);
			static const FunctionDeclaration& GetFunctionDeclaration(const std::wstring& stName);

			void Load(const std::wstring& stName) const;
			static Event& Add(const std::wstring& stName, EventInstance& instance);
			static EventTrigger& AddTrigger(const std::wstring& stName, EventInstance& instance);
			static EventFunction& AddFunction(const std::wstring& stName, EventInstance& instance);
			static EventObject& CreateObject(unsigned int objectId);
			static EventObjectArray& CreateObjectArray(unsigned int objectId);

		private:
#pragma warning( disable: 4251 )

			template<typename Event>
			static Event& LoadEvent(void);
			template<typename Trigger>
			static EventTrigger& LoadTrigger(void);
			template<typename Trigger>
			static EventFunction& LoadFunction(void);
			template<typename Object>
			static EventObject& LoadObject(void);
			template<typename Object>
			static EventObjectArray& LoadObjectArray(void);

			void ParseEventCommand(EventInstance& instance, xml::Node& node, const std::wstring& stEventName, TargetType type) const;

			static FunctionMap m_mFunctions;
			static TriggerFunctionMap m_mTriggerFunctions;
			static TriggerMap m_mTriggers;
			static FunctionFunctionMap m_mFunctionFunctions;
			static ObjectFunctionMap m_mObjectFunctions;
			static ObjectArrayMap m_mObjectArrayFunctions;
			static ObjectTypeMap m_mObjectTypes;
			static FunctionDeclarationMap m_mFunctionDeclaration;

			Events* m_pEvents;
#pragma warning( default: 4251 )
		};

		template<typename Event>
		Event& EventLoader::LoadEvent(void)
		{
			return *new Event();
		}

		template<typename EventType>
		void EventLoader::RegisterEvent(void)
		{
			static_assert(std::is_base_of<Event, EventType>::value, "EventType must be a descendant of Event");

			auto& declaration = EventType::GenerateDeclaration();
			EventType::SetDeclaration(declaration);

			auto& stName = declaration.GetName();
			ACL_ASSERT(!m_mFunctions.count(stName));
			m_mFunctions.emplace(stName, (EventLoadFunction)&LoadEvent<EventType>);
		}

		template<typename Trigger>
		EventTrigger& EventLoader::LoadTrigger(void)
		{
			return *new Trigger();
		}

		template<typename Trigger>
		void EventLoader::RegisterTrigger(void)
		{
			static_assert(std::is_base_of<EventTrigger, Trigger>::value, "Trigger must be a descendant of EventTrigger");

			auto& declaration = Trigger::GenerateDeclaration();
			Trigger::SetDeclaration(declaration);

			auto& stName = declaration.GetName();
			ACL_ASSERT(!m_mTriggerFunctions.count(stName));
			m_mTriggerFunctions.emplace(stName, (TriggerLoadFunction)&LoadTrigger<Trigger>);
			m_mTriggers.emplace(Trigger::type(), stName);
		}

		template<typename FunctionType>
		EventFunction& EventLoader::LoadFunction(void)
		{
			return *new FunctionType();
		}

		template<typename FunctionType>
		void EventLoader::RegisterFunction(void)
		{
			static_assert(std::is_base_of<EventFunction, FunctionType>::value, "FunctionType must be a descendant of EventFunction");

			auto& declaration = FunctionType::GenerateDeclaration();
			FunctionType::SetDeclaration(declaration);

			auto& stName = declaration.GetName();
			ACL_ASSERT(!m_mFunctionFunctions.count(stName));
			m_mFunctionFunctions.emplace(stName, (FunctionLoadFunction)&LoadFunction<FunctionType>);

			m_mFunctionDeclaration.emplace(stName, &declaration);
		}

		template<typename Object>
		EventObject& EventLoader::LoadObject(void)
		{
			return *new EventObjectContainer<Object>();
		}

		template<typename Object>
		EventObjectArray& EventLoader::LoadObjectArray(void)
		{
			return *new EventObjectArrayContainer<Object>();
		}

		template<typename ObjectType>
		void EventLoader::RegisterObject(const std::wstring& stName)
		{
			auto type = EventObjectContainer<ObjectType>::Type();
			ACL_ASSERT(!m_mObjectFunctions.count(type));
			ACL_ASSERT(!m_mObjectTypes.count(stName));

			m_mObjectTypes[stName] = type;
			m_mObjectFunctions[type] = &LoadObject<ObjectType>;
			m_mObjectArrayFunctions[type] = &LoadObjectArray<ObjectType>;
		}

	}
}

