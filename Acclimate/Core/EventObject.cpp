#include "EventObject.h"

namespace acl
{
	namespace core
	{

		/***********************************
		* ObjectRefCounter
		************************************/

		ObjectRefCounter::ObjectRefCounter(void) : m_refs(1),
			m_isValid(true)
		{
		}

		void ObjectRefCounter::AddRef(void)
		{
			m_refs++;
		}

		void ObjectRefCounter::Release(void)
		{
			m_refs--;
			if(m_refs == 0)
				delete this;
		}

		bool ObjectRefCounter::IsValid(void)
		{
			return m_isValid;
		}

		void ObjectRefCounter::Invalidate(void)
		{
			m_isValid = false;
		}

		/***********************************
		* EventObject
		************************************/

		EventObject::EventObject(unsigned int type) : m_type(type)
		{
			m_pCounter = new ObjectRefCounter();
		}

		EventObject::EventObject(const EventObject& object) : m_type(object.m_type),
			m_pCounter(object.m_pCounter)
		{
			m_pCounter->AddRef();
		}

		EventObject::~EventObject()
		{
			m_pCounter->Release();
		}

		EventObject& EventObject::operator=(const EventObject& object)
		{
			ACL_ASSERT(object.m_pCounter);

			object.m_pCounter->AddRef();
			m_pCounter->Release();
			m_pCounter = object.m_pCounter;

			SetObject(object.GetType(), object.GetObject(m_type));

			return *this;
		}

		void EventObject::SetObject(unsigned int objectId, void* pObject)
		{
			ACL_ASSERT(m_type == objectId);

			OnSetObject(pObject);
		}

		unsigned int EventObject::GetType(void) const
		{
			return m_type;
		}

		void* EventObject::GetObject(unsigned int objectType) const
		{
			ACL_ASSERT(m_type == objectType);

			return OnGetObject();
		}

		bool EventObject::HasObject(void) const
		{
			return OnGetObject() != nullptr;
		}

		bool EventObject::IsValid(void) const
		{
			return m_pCounter->IsValid();
		}

		void EventObject::Invalidate(void)
		{
			m_pCounter->Invalidate();
		}

		unsigned int EventObject::GenerateTypeId(void)
		{
			return m_typeCounter++;
		}

		void EventObject::NewCounter(void)
		{
			ACL_ASSERT(m_pCounter);
			m_pCounter->Release();
			m_pCounter = new ObjectRefCounter();
		}

		unsigned int EventObject::m_typeCounter = 0;

		/***********************************
		* EventObjectArray
		************************************/

		EventObjectArray::EventObjectArray(unsigned int type) :
			m_type(type)
		{
		}

		EventObjectArray::~EventObjectArray(void)
		{
		}

		/***********************************
		* ObjectInvalidException
		************************************/

		ObjectInvalidException::ObjectInvalidException(unsigned int objectType, unsigned int slot) : m_objectType(objectType),
			m_slot(slot)
		{
		}

		unsigned int ObjectInvalidException::GetType(void) const
		{
			return m_objectType;
		}
		
		unsigned int ObjectInvalidException::GetSlot(void) const
		{
			return m_slot;
		}

	}
}

