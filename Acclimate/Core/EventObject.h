#pragma once
#include <vector>
#include "Dll.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace core
	{

		/***********************************
		* ObjectRefCounter
		************************************/

		class ACCLIMATE_API ObjectRefCounter
		{
		public:
			ObjectRefCounter(void);

			void AddRef(void);
			void Release(void);

			bool IsValid(void);
			void Invalidate(void);

		private:

			unsigned int m_refs;
			bool m_isValid;
		};

		/***********************************
		* EventObject
		************************************/

		class ACCLIMATE_API EventObject
		{
		public:
			virtual ~EventObject() = 0;
			EventObject(const EventObject& object);

			virtual EventObject& Clone(void) const = 0;
			EventObject& operator=(const EventObject& object);

			template<typename Object>
			void SetObject(Object& object)
			{
				NewCounter();
				ACL_ASSERT(m_type == getObjectType<Object>());

				EventObjectContainer<Object>* pThisChildType = (EventObjectContainer<Object>*)this;
				pThisChildType->SetObject(object);
			}

			template<typename Object>
			void SetObject(const Object& object)
			{
				NewCounter();
				ACL_ASSERT(m_type == getObjectType<Object>());

				EventObjectContainer<Object>* pThisChildType = (EventObjectContainer<Object>*)this;
				pThisChildType->SetObject(object);
			}

			void SetObject(unsigned int objectId, void* pObject);

			template<typename Object>
			Object& GetObject(void) const
			{
				ACL_ASSERT(m_type == getObjectType<Object>());
				ACL_ASSERT(m_pCounter->IsValid());

				const EventObjectContainer<Object>* pThisChildType = (const EventObjectContainer<Object>*)this;
				
				return pThisChildType->GetObject();
			}

			unsigned int GetType(void) const;
			void* GetObject(unsigned int objectType) const;
			bool HasObject(void) const;
			bool IsValid(void) const;

			void Invalidate(void);

		protected:
			EventObject(unsigned int type);

			static unsigned int GenerateTypeId(void);

		private:

			void NewCounter(void);

			virtual void OnSetObject(void* pObject) = 0;
			virtual void* OnGetObject(void) const = 0;

			unsigned int m_type;
			ObjectRefCounter* m_pCounter;

			static unsigned int m_typeCounter;
		};

		/***********************************
		* EventObjectContainer
		************************************/

		template<typename Object>
		class EventObjectContainer :
			public EventObject
		{
		public:

			EventObjectContainer(void) : EventObject(Type()),
				m_pObject(nullptr)
			{
			}

			EventObjectContainer(Object& object) : EventObject(Type()),
				m_pObject(&object)
			{
			}

			// TODO: get rid of const-cast
			EventObjectContainer(const Object& object) : EventObject(Type()),
				m_pObject(&const_cast<Object&>(object))
			{
			}

			EventObjectContainer(const EventObjectContainer& container) : EventObject(container),
				m_pObject(container.m_pObject)
			{
			}

			EventObject& Clone(void) const
			{
				return *new EventObjectContainer(*this);
			}

			static unsigned int Type(void)
			{
				//increase base family count on first access
				static unsigned int type = GenerateTypeId();
				return type;
			}

			void SetObject(Object& object)
			{
				m_pObject = &object;
			}

			void SetObject(const Object& object)
			{
				m_pObject = &const_cast<Object&>(object);
			}

			Object& GetObject(void) const
			{
				return *m_pObject;
			}

		private:

			void OnSetObject(void* pObject) override
			{
				m_pObject = (Object*)pObject;
			}

			void* OnGetObject(void) const override
			{
				return m_pObject;
			}

			Object* m_pObject;

		};

		template<typename Object>
		unsigned int getObjectType(void)
		{
			return EventObjectContainer<Object>::Type();
		}

#define EXPORT_OBJECT(Type) EXPORT_TEMPLATE template class ACCLIMATE_API acl::core::EventObjectContainer<Type>;
#define USE_OBJECT(Type) EXPORT_TEMPLATE template class ACCLIMATE_API acl::core::EventObjectContainer<Type>;

		/***********************************
		* EventObjectArray
		************************************/

		class ACCLIMATE_API EventObjectArray
		{
		public:

			virtual ~EventObjectArray(void) = 0;

			virtual size_t Size(void) const = 0;
			virtual void PushBack(void) = 0;
			virtual void PopBack(void) = 0;

			template<typename Object>
			std::vector<Object*>& GetVector(void)
			{
				ACL_ASSERT(m_type == getObjectType<Object>());

				auto pContainer = (const EventObjectArrayContainer<Object>*)(*this);

				return pContainer->GetVector();
			}

		protected:

			EventObjectArray(unsigned int type);

		private:

			unsigned int m_type;
		};

		/***********************************
		* EventObjectArrayContainer
		************************************/

		template<typename Type>
		class EventObjectArrayContainer :
			public EventObjectArray
		{
		public:
			typedef std::vector<Type*> ObjectVector;

			EventObjectArrayContainer(void) : EventObjectArray(getObjectType<Type>())
			{
			}

			Type& GetObject(size_t slot) const
			{
				return m_objects.at(slot);
			}

			ObjectVector& GetObject(void)
			{
				return m_objects;
			}

			void PushBack(void) override
			{
				m_objects.push_back(nullptr);
			}

			void PopBack(void) override
			{
				m_objects.pop_back();
			}

			size_t Size(void) const override
			{
				return m_objects.size();
			}

		private:

			ObjectVector m_objects;
		};

		/***********************************
		* ObjectInvalidException
		************************************/
		class ACCLIMATE_API ObjectInvalidException
		{
		public:
			ObjectInvalidException(unsigned int objectType, unsigned int slot);

			unsigned int GetType(void) const;
			unsigned int GetSlot(void) const;
		private:

			unsigned int m_objectType, m_slot;
		};
	}
}

