#include "EventSaver.h"
#include "Event.h"
#include "EventLoader.h"
#include "EventVariable.h"
#include "EventDeclaration.h"
#include "TriggerDeclaration.h"
#include "..\System\Convert.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{


		EventSaver::EventSaver(Events& events) : m_pEvents(&events)
		{
		}

		void EventSaver::Save(const std::wstring& stName) const
		{
			xml::Doc doc;

			auto& root = doc.InsertNode(L"Events");

			for(auto& instance : m_pEvents->Map())
			{
				auto& instanceNode = root.InsertNode(L"Event");
				instanceNode.ModifyAttribute(L"name", instance.first);
				instanceNode.ModifyAttribute(L"uid", conv::ToString(instance.second->GetUID()));
				instanceNode.ModifyAttribute(L"varUid", conv::ToString(instance.second->GetVariableUID()));

				auto& commandNode = instanceNode.InsertNode(L"Commands");
				auto& triggersNode = instanceNode.InsertNode(L"Trigger");
				auto& functionsNode = instanceNode.InsertNode(L"Functions");
				auto& getterNode = instanceNode.InsertNode(L"Getters");
				auto& setterNode = instanceNode.InsertNode(L"Setters");

				/**************************************
				* Events
				**************************************/
				auto& mEvents = instance.second->GetEvents();
				if(!mEvents.empty())
				{
					std::map<unsigned int, const core::EventCommand*> mSorted;
					for(auto& event : mEvents)
					{
						mSorted.emplace(event.second->GetId(), event.second);
					}

					for(auto& event : mSorted)
					{
						auto pEvent = event.second;
						const auto type = pEvent->GetTargetType();

						xml::Node* pEventNode;
						switch(type)
						{
						case TargetType::COMMAND:
							pEventNode = &commandNode.InsertNode(pEvent->GetName());
							break;
						case TargetType::TRIGGER:
							pEventNode = &triggersNode.InsertNode(pEvent->GetName());
							break;
						case TargetType::GETTER:
							pEventNode = &getterNode.InsertNode(L"Getter");
							pEventNode->ModifyAttribute(L"varId", conv::ToString(((VariableGetter*)pEvent)->GetVariableId()));
							break;
						case TargetType::FUNCTION:
							pEventNode = &functionsNode.InsertNode(pEvent->GetName());
							break;
						case TargetType::SETTER:
							pEventNode = &setterNode.InsertNode(L"Setter");
							pEventNode->ModifyAttribute(L"varId", conv::ToString(((VariableSetter*)pEvent)->GetVariableId()));
							break;
						default:
							ACL_ASSERT(false);
						}

						pEventNode->ModifyAttribute(L"id", conv::ToString(pEvent->GetId()));

						// outputs
						if(auto pOutput = pEvent->QueryOutput())
						{
							for(unsigned int i = 0; i < pOutput->GetNumOutputs(); i++)
							{
								const auto target = pOutput->GetOutputTarget(i);
								if(target != Event::NO_EVENT_ID)
								{
									auto& outputNode = pEventNode->InsertNode(L"Output");
									outputNode.SetValue(conv::ToString(i));
									outputNode.ModifyAttribute(L"target", conv::ToString(target));
								}
							}
						}

						// attributes
						if(auto pAttributes = pEvent->QueryAttribute())
						{
							for(unsigned int i = 0; i < pAttributes->GetNumAttributes(); i++)
							{
								const auto& attribute = pAttributes->GetAttribute(i);

								auto& attributeNode = pEventNode->InsertNode(L"Attribute");
								if(attribute.IsConnected())
									attributeNode.ModifyAttribute(L"connected", L"1");
								else if(!attribute.IsArray())
									attributeNode.SetValue(attribute.ToString());
								else
								{
									auto& vValues = attribute.ToStringArray();
									for(auto& stValue : vValues)
									{
										auto& valueNode = attributeNode.InsertNode(L"Value");
										valueNode.SetValue(stValue);
									}
								}
							}
						}

						// returns
						if(auto pReturns = pEvent->QueryReturn())
						{
							for(unsigned int i = 0; i < pReturns->GetNumReturns(); i++)
							{
								auto& vTargets = pReturns->GetReturnTargets(i);
								if(!vTargets.empty())
								{
									auto& returnNode = pEventNode->InsertNode(L"Return");
									returnNode.ModifyAttribute(L"slot", conv::ToString(i));

									for(auto& target : vTargets)
									{
										auto& targetNode = returnNode.InsertNode(L"Target");
										targetNode.SetValue(conv::ToString(target.first));
										targetNode.ModifyAttribute(L"slot", conv::ToString(target.second));
									}
								}
							}
						}

						// position
						auto& vPosition = pEvent->GetPosition();
						auto& position = pEventNode->InsertNode(L"Position");
						position.ModifyAttribute(L"x", conv::ToString(vPosition.x));
						position.ModifyAttribute(L"y", conv::ToString(vPosition.y));
					}
				}

				/**************************************
				* Variables
				**************************************/
				auto& mVariables = instance.second->GetVariables();
				if(!mVariables.empty())
				{
					const auto& mObjects = EventLoader::GetRegisteredObjects();

					auto& variablesNode = instanceNode.InsertNode(L"Variables");
					for(auto& variable : mVariables)
					{
						auto pVariable = variable.second;
						const auto isArray = pVariable->IsArray();
						
						auto& variableNode = variablesNode.InsertNode(L"Variable");
						variableNode.ModifyAttribute(L"name", pVariable->GetName());
						variableNode.ModifyAttribute(L"type", conv::ToString((unsigned int)pVariable->GetType()));
						variableNode.ModifyAttribute(L"isConst", conv::ToString(pVariable->IsConst()));
						variableNode.ModifyAttribute(L"isArray", conv::ToString(isArray));
						variableNode.ModifyAttribute(L"id", conv::ToString(pVariable->GetId()));

						// value
						if(pVariable->GetType() != AttributeType::OBJECT)
						{
							if(isArray)
							{
								auto& vValues = pVariable->ValueToStringArray();
								for(auto& stValue : vValues)
								{
									auto& valueNode = variableNode.InsertNode(L"Value");
									valueNode.SetValue(stValue);
								}
							}
							else
							{
								auto& valueNode = variableNode.InsertNode(L"Value");
								valueNode.SetValue(pVariable->ValueToString());
							}
						}
						else
						{
							const auto type = pVariable->GetObjectType();

							std::wstring stObject;

							for(auto& object : mObjects)
							{
								if(object.second == type)
								{
									stObject = object.first;
									break;
								}
							}

							ACL_ASSERT(!stObject.empty());

							variableNode.ModifyAttribute(L"object", stObject);
						}
					}
				}
			}

			doc.SaveFile(stName);
		}

	}
}

