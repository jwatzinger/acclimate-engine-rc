#pragma once
#include "Events.h"

namespace acl
{
	namespace core
	{

		class ACCLIMATE_API EventSaver
		{
		public:
			EventSaver(Events& events);

			void Save(const std::wstring& stFile) const;
			void Save(const std::wstring& stFile, const Events::Block& block) const;

		private:

			Events* m_pEvents;
		};

	}
}


