#include "EventTrigger.h"
#include "EventAttribute.h"

namespace acl
{
	namespace core
	{

		EventTrigger::EventTrigger(Type type) : EventCommand(SlotType::OUTPUT | SlotType::RETURN, TargetType::TRIGGER), m_type(type)
		{
		}

		EventTrigger::EventTrigger(const EventTrigger& trigger) : EventCommand(trigger), OutputComponent(trigger),
			ReturnComponent(trigger), m_type(trigger.m_type)
		{
		}

		EventTrigger::~EventTrigger(void)
		{
		}

		EventTrigger::Type EventTrigger::GetType(void) const
		{
			return m_type;
		}

		void EventTrigger::Trigger(void) const
		{
			CallOutput(0);
		}

		void EventTrigger::Trigger(const AttributeTriggerVector& vAttributes)
		{
			ACL_ASSERT(vAttributes.size() == GetNumReturns());
			unsigned int slot = 0;
			for(auto& attribute : vAttributes)
			{
				ReturnAttribute(slot, attribute);
			}
			CallOutput(0);
		}

		const OutputVector& EventTrigger::GetOutputDeclaration(void) const
		{
			static const OutputVector vOutputs = { { L"" } };
			return vOutputs;
		}

		void EventTrigger::OnReturn(void)
		{
		}

		EventTrigger::Type EventTrigger::type_count = 0;

		EventTrigger::Type EventTrigger::GenerateTypeId(void)
		{
			return type_count++;
		}

	}
}

