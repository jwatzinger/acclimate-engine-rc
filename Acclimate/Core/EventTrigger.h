#pragma once
#include "Signal.h"
#include "EventCommand.h"
#include "TriggerDeclaration.h"

namespace acl
{
	namespace core
	{

		/*********************************
		* EventTrigger
		*********************************/

		class TriggerDeclaration;

		EXPORT_TEMPLATE template class ACCLIMATE_API Signal<unsigned int>;

		class ACCLIMATE_API EventTrigger :
			public EventCommand, public OutputComponent, public ReturnComponent
		{
		public:
			typedef unsigned int Type; ///< Component struct type
			typedef std::vector<EventAttribute> AttributeTriggerVector;

			EventTrigger(const EventTrigger& trigger);
			virtual ~EventTrigger(void) = 0;

			virtual EventTrigger& Clone(void) const = 0;

			virtual const TriggerDeclaration& GetDeclaration(void) const = 0;
			Type GetType(void) const;

			void Trigger(void) const;
			void Trigger(const AttributeTriggerVector& vAttributes);

		protected:
			EventTrigger(Type type);

			static Type GenerateTypeId(void);

		private:
#pragma warning( disable: 4251 )

			void OnReturn(void) override final;

			const OutputVector& GetOutputDeclaration(void) const override final;

			Type m_type;
			static Type type_count;

#pragma warning( default: 4251 )
		};

		/*********************************
		* BaseTrigger
		**********************************/

		template <typename Derived>
		class BaseTrigger :
			public EventTrigger
		{
		public:
			BaseTrigger(void) : EventTrigger(type())
			{
			}

			virtual ~BaseTrigger(void) = 0 {};

			EventTrigger& Clone(void) const override final
			{
				return *new Derived(*(Derived*)this);
			}

			static Type type(void)
			{
				//increase base family count on first access
				static EventTrigger::Type Type = GenerateTypeId();
				return Type;
			}

			static void SetDeclaration(const TriggerDeclaration& declaration)
			{
				m_pDeclaration = &declaration;
			}

			const TriggerDeclaration& GetDeclaration(void) const override final
			{
				ACL_ASSERT(m_pDeclaration);
				return *m_pDeclaration;
			}

			const std::wstring& GetName(void) const override final
			{
				return m_pDeclaration->GetName();
			}

			const AttributeVector& GetReturnDeclaration(void) const override final
			{
				return m_pDeclaration->GetReturns();
			}

		private:

			static const TriggerDeclaration* m_pDeclaration;

		};

		template<typename Type>
		const TriggerDeclaration* BaseTrigger<Type>::m_pDeclaration = nullptr;

	}
}


