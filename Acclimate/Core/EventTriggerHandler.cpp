#include "EventTriggerHandler.h"

namespace acl
{
	namespace core
	{

		void EventTriggerHandler::AddInstance(EventInstance& instance)
		{
			auto vTrigger = instance.GetTriggerTypes();

			for(auto trigger : vTrigger)
			{
				auto& vInstances = m_mTriggerInstances[trigger];
				ACL_ASSERT(std::find(vInstances.begin(), vInstances.end(), &instance) == vInstances.end());

				vInstances.push_back(&instance);
			}
		}

		void EventTriggerHandler::RemoveInstance(EventInstance& instance)
		{
			auto vTrigger = instance.GetTriggerTypes();

			for(auto trigger : vTrigger)
			{
				auto& vInstances = m_mTriggerInstances[trigger];
				auto itr = std::find(vInstances.begin(), vInstances.end(), &instance);
				ACL_ASSERT(itr != vInstances.end());

				vInstances.erase(itr);
			}
		}

	}
}

