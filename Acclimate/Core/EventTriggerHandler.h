#pragma once
#include <vector>
#include <unordered_map>
#include "EventTrigger.h"
#include "EventInstance.h"

namespace acl
{
	namespace core
	{

		class ACCLIMATE_API EventTriggerHandler
		{
			typedef std::vector<EventInstance*> InstanceVector;
			typedef std::unordered_map<EventTrigger::Type, std::vector<EventInstance*>> TriggerMap;
			typedef std::vector<EventAttribute> AttributeVector;
		public:

			void AddInstance(EventInstance& instance);
			void RemoveInstance(EventInstance& instance);

			template<typename Trigger>
			void TriggerAll(void) const;
			template<typename Trigger, typename Arg, typename... Args>
			void TriggerAll(Arg&& arg, Args&&... args) const;
			template<typename Trigger, typename Arg, typename... Args>
			static void TriggerInstance(EventInstance& instance, Arg&& arg, Args&&... args);

		private:
#pragma warning( disable: 4251 )

			template<typename Arg, typename... Args>
			static const EventTriggerHandler::AttributeVector& PrepareAttributes(Arg&& arg, Args&&... args);

			template<typename Arg1, typename Arg2, typename... Args>
			static void FillAttributes(AttributeVector& vAttributes, Arg1&& arg1, Arg2&& arg2, Args&&... args);
			template<typename Arg>
			static void FillAttributes(AttributeVector& vAttributes, Arg&& arg);

			template<typename Arg1, typename Arg2, typename... Args>
			static void SetAttributes(AttributeVector::iterator& itr, Arg1&& arg1, Arg2&& arg2, Args&&... args);
			template<typename Arg>
			static void SetAttributes(AttributeVector::iterator& itr, Arg&& arg);

			TriggerMap m_mTriggerInstances;
#pragma warning( default: 4251 )
		};

		template<typename Trigger>
		void EventTriggerHandler::TriggerAll(void) const
		{
			const auto type = Trigger::type();

			auto itr = m_mTriggerInstances.find(type);
			if(itr != m_mTriggerInstances.end())
			{
				for(auto pInstance : itr->second)
				{
					pInstance->Trigger<Trigger>();
				}
			}
		}

		template<typename Arg, typename... Args>
		static const EventTriggerHandler::AttributeVector& EventTriggerHandler::PrepareAttributes(Arg&& arg, Args&&... args)
		{
			// prepare arguments
			const size_t n = 1 + sizeof...(Args);

			static AttributeVector vAttributes;
			static bool isFilled = false;
			if(!isFilled)
			{
				vAttributes.reserve(n);

				FillAttributes(vAttributes, arg, args...);

				isFilled = true;
			}
			else
			{
				auto itr = vAttributes.begin();
				SetAttributes(itr, arg, args...);
			}

			return vAttributes;
		}

		template<typename Trigger, typename Arg, typename... Args>
		void EventTriggerHandler::TriggerAll(Arg&& arg, Args&&... args) const
		{
			const auto type = Trigger::type();

			auto itr = m_mTriggerInstances.find(type);
			if(itr != m_mTriggerInstances.end())
			{
				auto& vAttributes = PrepareAttributes(arg, args...);

				for(auto pInstance : itr->second)
				{
					pInstance->Trigger<Trigger>(vAttributes);
				}
			}
		}

		template<typename Trigger, typename Arg, typename... Args>
		void EventTriggerHandler::TriggerInstance(EventInstance& instance, Arg&& arg, Args&&... args)
		{
			if(instance.HasTrigger<Trigger>())
			{
				auto& vAttributes = PrepareAttributes(arg, args...);
				instance.Trigger<Trigger>(vAttributes);
			}
		}

		template<typename Arg1, typename Arg2, typename... Args>
		void EventTriggerHandler::FillAttributes(AttributeVector& vAttributes, Arg1&& arg1, Arg2&& arg2, Args&&... args)
		{
			FillAttributes(vAttributes, arg1);
			FillAttributes(vAttributes, arg2);
			FillAttributes(vAttributes, args...);
		}

		template<typename Arg>
		void EventTriggerHandler::FillAttributes(AttributeVector& vAttributes, Arg&& arg)
		{
			vAttributes.emplace_back(arg);
		}

		template<typename Arg1, typename Arg2, typename... Args>
		void EventTriggerHandler::SetAttributes(AttributeVector::iterator& itr, Arg1&& arg1, Arg2&& arg2, Args&&... args)
		{
			SetAttributes(itr, arg1);
			SetAttributes(itr, arg2);
			SetAttributes(itr, args...);
		}

		template<typename Arg>
		void EventTriggerHandler::SetAttributes(AttributeVector::iterator& itr, Arg&& arg)
		{
			itr->SetValue(arg);
			itr++;
		}

	}
}

