#include "EventVariable.h"
#include "EventAttribute.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace core
	{

		/**********************************
		* EventVariable
		***********************************/

		EventVariable::EventVariable(const std::wstring& stName, AttributeType type, bool isConst, bool isArray, unsigned int id) :
			m_stName(stName), m_isConst(isConst), m_id(id)
		{
			if(isArray)
			{
				switch(type)
				{
				case AttributeType::BOOL:
					m_pAttribute = new EventAttribute(std::vector<bool>());
					break;
				case AttributeType::FLOAT:
					m_pAttribute = new EventAttribute(std::vector<float>());
					break;
				case AttributeType::INT:
					m_pAttribute = new EventAttribute(std::vector<int>());
					break;
				case AttributeType::STRING:
					m_pAttribute = new EventAttribute(std::vector<std::wstring>());
					break;
				default:
					ACL_ASSERT(false);
				}
			}
			else
			{
				switch(type)
				{
				case AttributeType::BOOL:
					m_pAttribute = new EventAttribute(false);
					break;
				case AttributeType::FLOAT:
					m_pAttribute = new EventAttribute(0.0f);
					break;
				case AttributeType::INT:
					m_pAttribute = new EventAttribute(0);
					break;
				case AttributeType::STRING:
					m_pAttribute = new EventAttribute(std::wstring(L""));
					break;
				default:
					ACL_ASSERT(false);
				}
			}
		}

		EventVariable::EventVariable(const std::wstring& stName, unsigned int objectId, bool isArray, unsigned int id):
			m_stName(stName), m_isConst(false), m_id(id)
		{
			m_pAttribute = new EventAttribute(objectId, isArray);
		}

		EventVariable::EventVariable(const EventVariable& variable) :
			m_stName(variable.m_stName), m_isConst(variable.m_isConst), m_id(variable.m_id)
		{
			m_pAttribute = new EventAttribute(*variable.m_pAttribute);
		}

		EventVariable::~EventVariable(void)
		{
			delete m_pAttribute;
		}

		void EventVariable::SetConst(bool isConst)
		{
			m_isConst = isConst;
		}

		void EventVariable::SetName(const std::wstring& stName)
		{
			m_stName = stName;
		}

		void EventVariable::SetType(AttributeType type)
		{
			m_pAttribute->ConvertToType(type);
		}

		void EventVariable::SetType(unsigned int objectType)
		{
			m_pAttribute->ConvertToType(objectType);
		}

		const std::wstring& EventVariable::GetName(void) const
		{
			return m_stName;
		}

		const void* EventVariable::GetData(void) const
		{
			return m_pAttribute->GetData();
		}

		AttributeType EventVariable::GetType(void) const
		{
			return m_pAttribute->GetType();
		}

		unsigned int EventVariable::GetId(void) const
		{
			return m_id;
		}

		EventAttribute& EventVariable::GetAttribute(void)
		{
			return *m_pAttribute;
		}

		const EventAttribute& EventVariable::GetAttribute(void) const
		{
			return *m_pAttribute;
		}

		unsigned int EventVariable::GetObjectType(void) const
		{
			return m_pAttribute->GetObjectType();
		}

		bool EventVariable::IsArray(void) const
		{
			return m_pAttribute->IsArray();
		}

		bool EventVariable::IsConst(void) const
		{
			return m_isConst;
		}

		std::wstring EventVariable::ValueToString(void) const
		{
			return m_pAttribute->ToString();
		}

		EventVariable::StringVector EventVariable::ValueToStringArray(void) const
		{
			return m_pAttribute->ToStringArray();
		}

		VariableGetter& EventVariable::CreateGetter(void)
		{
			return *new VariableGetter(*this);
		}

		namespace
		{
			void setupDefault(AttributeComponent& attributes)
			{
				VariableSetter::ValueVector vAttributes;
				for(auto& attribute : attributes.GetAttributeDeclaration())
				{
					if(attribute.IsArray())
						vAttributes.emplace_back(AttributeData::ValueVector());
					else
					{
						switch(attribute.GetType())
						{
						case AttributeType::BOOL:
							vAttributes.emplace_back(L"0");
							break;
						case AttributeType::FLOAT:
							vAttributes.emplace_back(L"0.0f");
							break;
						case AttributeType::INT:
							vAttributes.emplace_back(L"0");
							break;
						case AttributeType::STRING:
							vAttributes.emplace_back(L"");
							break;
						case AttributeType::OBJECT:
							vAttributes.emplace_back();
							break;
						default:
							ACL_ASSERT(false);
						}
					}
				}

				attributes.SetupAttributes(vAttributes);
			}
		}

		VariableSetter& EventVariable::CreateSetter(void)
		{
			auto& setter = *new VariableSetter(*this);
			setupDefault(setter);
			return setter;
		}

		/**********************************
		* VariableGetter
		***********************************/

		VariableGetter::VariableGetter(EventVariable& variable) : EventCommand(SlotType::RETURN, TargetType::GETTER),
			m_pVariable(nullptr), m_variableId(variable.GetId())
		{
			SetVariable(variable);
		}

		VariableGetter::VariableGetter(const VariableGetter& getter) : EventCommand(getter),
			ReturnComponent(getter), m_pVariable(nullptr), m_variableId(getter.m_variableId)
		{
		}

		VariableGetter::~VariableGetter(void)
		{
			if(m_pVariable)
				m_pVariable->SigChanged.Disconnect(this, &VariableGetter::OnVariableChanged);
		}

		EventCommand& VariableGetter::Clone(void) const
		{
			return *new VariableGetter(*this);
		}

		void VariableGetter::SetVariable(EventVariable& variable)
		{
			ACL_ASSERT(!m_pVariable);
			ACL_ASSERT(variable.GetId() == m_variableId);

			const auto type = variable.GetType();
			if(type == AttributeType::OBJECT)
				m_vReturns.emplace_back(variable.GetName(), variable.GetObjectType(), variable.IsArray());
			else
				m_vReturns.emplace_back(variable.GetName(), variable.GetType(), variable.IsArray());

			m_pVariable = &variable;
			m_pVariable->SigChanged.Connect(this, &VariableGetter::OnVariableChanged);
		}

		unsigned int VariableGetter::GetVariableId(void) const
		{
			ACL_ASSERT(m_pVariable);
			return m_pVariable->GetId();
		}

		const AttributeVector& VariableGetter::GetReturnDeclaration(void) const
		{
			ACL_ASSERT(m_pVariable);
			return m_vReturns;
		}

		const std::wstring& VariableGetter::GetName(void) const
		{
			static const std::wstring& stName(L"");
			return stName;
		}

		void VariableGetter::OnReturn(void)
		{
			ACL_ASSERT(m_pVariable);
			ReturnAttribute(0, m_pVariable->GetAttribute());
		}

		void VariableGetter::OnVariableChanged(const EventVariable& variable)
		{
			ACL_ASSERT(m_pVariable == &variable);
			OnReturn();
		}

		/**********************************
		* VariableSetter
		***********************************/

		VariableSetter::VariableSetter(EventVariable& variable) : EventCommand(SlotType::ATTRIBUTE | SlotType::INPUT | SlotType::OUTPUT, TargetType::SETTER),
			m_pVariable(&variable), m_variableId(variable.GetId())
		{
			const auto type = variable.GetType();
			if(type == AttributeType::OBJECT)
				m_vAttributes.emplace_back(variable.GetName(), variable.GetObjectType(), variable.IsArray());
			else
				m_vAttributes.emplace_back(variable.GetName(), variable.GetType(), variable.IsArray());
		}

		VariableSetter::VariableSetter(const VariableSetter& setter) : EventCommand(setter),
			AttributeComponent(setter), InputComponent(setter), OutputComponent(setter), m_pVariable(nullptr), m_variableId(setter.m_variableId),
			m_vAttributes(setter.m_vAttributes)
		{
		}

		EventCommand& VariableSetter::Clone(void) const
		{
			return *new VariableSetter(*this);
		}

		void VariableSetter::SetVariable(EventVariable& variable)
		{
			ACL_ASSERT(!m_pVariable);
			ACL_ASSERT(variable.GetId() == m_variableId);

			const auto type = variable.GetType();
			if(type == AttributeType::OBJECT)
				m_vAttributes.emplace_back(variable.GetName(), variable.GetObjectType(), variable.IsArray());
			else
				m_vAttributes.emplace_back(variable.GetName(), variable.GetType(), variable.IsArray());

			m_pVariable = &variable;
		}

		unsigned int VariableSetter::GetVariableId(void) const
		{
			ACL_ASSERT(m_pVariable);
			return m_pVariable->GetId();
		}

		const std::wstring& VariableSetter::GetName(void) const
		{
			static const std::wstring& stName(L"");
			return stName;
		}

		void VariableSetter::Execute(double dt)
		{
			m_pVariable->SetValue(GetAttribute(0));
			CallOutput(0);
		}

		void VariableSetter::Reset(void)
		{
		}

		unsigned int VariableSetter::OnGetId(void) const
		{
			return GetId();
		}

		const core::OutputVector& VariableSetter::GetOutputDeclaration(void) const
		{
			static const core::OutputVector vOutput = 
			{
				L""
			};

			return vOutput;
		}

		const AttributeVector& VariableSetter::GetAttributeDeclaration(void) const
		{
			return m_vAttributes;
		}

	}
}

