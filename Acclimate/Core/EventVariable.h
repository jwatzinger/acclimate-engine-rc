#pragma once
#include <string>
#include "Signal.h"
#include "EventCommand.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace core
	{

		/**********************************
		* EventVariable
		***********************************/

		class EventAttribute;
		class VariableGetter;
		class VariableSetter;
		enum class AttributeType;

		class EventVariable;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const EventVariable&>;

		class ACCLIMATE_API EventVariable
		{
		public:
			typedef std::vector<std::wstring> StringVector;

			EventVariable(const std::wstring& stName, AttributeType type, bool isConst, bool isArray, unsigned int id);
			EventVariable(const std::wstring& stName, unsigned int objectId, bool isArray, unsigned int id);
			EventVariable(const EventVariable& variable);
			~EventVariable(void);

			void SetConst(bool isConst);
			void SetName(const std::wstring& stName);
			void SetType(AttributeType type);
			void SetType(unsigned int objectType);
			template<typename Type>
			void SetValue(const Type& type);
			template<>
			void SetValue(const EventAttribute& attribute);

			const std::wstring& GetName(void) const;
			const void* GetData(void) const;
			AttributeType GetType(void) const;
			unsigned int GetId(void) const;
			EventAttribute& GetAttribute(void);
			const EventAttribute& GetAttribute(void) const;
			unsigned int GetObjectType(void) const;
			bool IsArray(void) const;
			bool IsConst(void) const;
			std::wstring ValueToString(void) const;
			StringVector ValueToStringArray(void) const;

			VariableGetter& CreateGetter(void);
			VariableSetter& CreateSetter(void);

			Signal<const EventVariable&> SigChanged;

		private:
#pragma warning( disable: 4251 )
			EventAttribute* m_pAttribute;

			unsigned int m_id;
			std::wstring m_stName;
			bool m_isConst;
#pragma warning( default: 4251 )
		};

		template<typename Type>
		void EventVariable::SetValue(const Type& type)
		{
			m_pAttribute->SetValue(type);
		}

		template<>
		void EventVariable::SetValue(const EventAttribute& attribute)
		{
			*m_pAttribute = attribute;
			SigChanged(*this);
		}

		/**********************************
		* VariableGetter
		***********************************/

		class ACCLIMATE_API VariableGetter final :
			public EventCommand, public ReturnComponent
		{
		public:
			VariableGetter(EventVariable& variable);
			VariableGetter(const VariableGetter& getter);
			~VariableGetter(void);

			EventCommand& Clone(void) const override;

			void SetVariable(EventVariable& variable);

			unsigned int GetVariableId(void) const;
			const AttributeVector& GetReturnDeclaration(void) const override;
			const std::wstring& GetName(void) const override;

			void OnReturn(void) override;

		private:
#pragma warning( disable: 4251 )
			void OnVariableChanged(const EventVariable& variable);

			unsigned int m_variableId;
			EventVariable* m_pVariable;
			AttributeVector m_vReturns;
#pragma warning( default: 4251 )
		};

		/**********************************
		* VariableSetter
		***********************************/

		class ACCLIMATE_API VariableSetter final :
		public EventCommand, public AttributeComponent, public InputComponent, public OutputComponent
		{
		public:
			VariableSetter(EventVariable& variable);
			VariableSetter(const VariableSetter& setter);

			EventCommand& Clone(void) const override;

			void SetVariable(EventVariable& variable);

			unsigned int GetVariableId(void) const;
			const std::wstring& GetName(void) const override;

			void Execute(double dt) override;
			void Reset(void) override;

		private:
#pragma warning( disable: 4251 )

			unsigned int OnGetId(void) const override;

			const core::OutputVector& GetOutputDeclaration(void) const override;
			const AttributeVector& GetAttributeDeclaration(void) const override;

			unsigned int m_variableId;
			EventVariable* m_pVariable;
			AttributeVector m_vAttributes;
#pragma warning( default: 4251 )
		};

	}
}


