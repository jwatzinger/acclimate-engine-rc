#pragma once
#include <string>
#include "EventInstance.h"
#include "Dll.h"
#include "Resources.h"

namespace acl
{
	namespace core
	{

		typedef Resources<std::wstring, EventInstance> Events;

		EXPORT_TEMPLATE template class ACCLIMATE_API Resources<std::wstring, EventInstance>;

	}
}