#include "FunctionDeclaration.h"
#include "EventAttribute.h"

namespace acl
{
	namespace core
	{

		FunctionDeclaration::FunctionDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns) : m_stName(stName),
			m_vReturns(vReturns), m_vAttributes(vAttributes)
		{
		}

		const std::wstring& FunctionDeclaration::GetName(void) const
		{
			return m_stName;
		}

		const AttributeVector& FunctionDeclaration::GetAttributes(void) const
		{
			return m_vAttributes;
		}

		const AttributeVector& FunctionDeclaration::GetReturns(void) const
		{
			return m_vReturns;
		}

	}
}

