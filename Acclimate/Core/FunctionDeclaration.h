#pragma once
#include <string>
#include <vector>
#include "Dll.h"
#include "EventDeclaration.h"

namespace acl
{
	namespace core
	{

		class ACCLIMATE_API FunctionDeclaration
		{
		public:
			FunctionDeclaration(const std::wstring& stName, const AttributeVector& vAttributes, const AttributeVector& vReturns);

			const std::wstring& GetName(void) const;
			const AttributeVector& GetAttributes(void) const;
			const AttributeVector& GetReturns(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stName;

			AttributeVector m_vAttributes, m_vReturns;
#pragma warning( default: 4251 )
		};

	}
}

