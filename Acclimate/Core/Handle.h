/*
* Acclimate Engine: Core
*
* Created by Julian Watzinger
*/

#pragma once
#include "..\System\Assert.h"

namespace acl
{
	namespace core
	{

		template<typename Type>
		class Owner;

		/// Handles the interaction between owner and handle
		/** This class is used to allow a handle to query for validity of its owner, even after the owner has
		*   been destroyed, and without needing to directly notify the handle. It uses reference counting to
		*   free its memory after the owner and all handles are removed. */
		template<typename Type>
		class Shared
		{
		public:

			Shared(Owner<Type>& owner) : m_pOwner(&owner),
				m_references(1)
			{
			}

			void AddRef(void)
			{
				m_references++;
			}

			void Release(void)
			{
				m_references--;
				if(!m_references)
					delete this;
			}

			void SetOwner(Owner<Type>* pOwner)
			{
				m_pOwner = pOwner;
			}

			bool HasValidOwner(void) const
			{
				return m_pOwner != nullptr;
			}

			Type& GetObject(void)
			{
				return m_pOwner->m_object;
			}

			const Type& GetObject(void) const
			{
				return m_pOwner->m_object;
			}

		private:

			Shared(const Shared<Type>&) = delete;
			Shared(Shared<Type>&&) = delete;
			void operator=(const Shared<Type>&) = delete;
			void operator=(Shared<Type>&&) = delete;

			int m_references;
			Owner<Type>* m_pOwner;
		};

		/// Stores a safe, weak reference to an object
		/** This class acts as sort of a weak smart pointer. It automatically handles the lifetime and should
		*	always be stored by value, and checked for validity before accessing the object it referes to.
		*   A handle is typically created by an owner, but it can be default-initialized and copyied/moved
		*	from any other handle. The refered object can be directly accessed, though checking validity beforehands
		*	is recommended unless its asolutely safe the parent still exists. The raw pointer accessed this way may never
		*	be stored inside a class or as a global, in which case validity could not be queried and/or quaranted anymore.*/
		template<typename Type>
		class Handle final
		{
		public:

			Handle(void) : m_pShared(nullptr)
			{
			}

			Handle(Shared<Type>& shared) : m_pShared(&shared)
			{
				m_pShared->AddRef();
			}

			Handle(const Handle& handle) : m_pShared(handle.m_pShared)
			{
				if(m_pShared)
					m_pShared->AddRef();
			}

			Handle(Handle&& handle) : m_pShared(handle.m_pShared)
			{
				handle.m_pShared = nullptr;
			}

			Handle& operator=(const Handle& handle)
			{
				if(handle.m_pShared)
					handle.m_pShared->AddRef();

				if(m_pShared)
					m_pShared->Release();

				m_pShared = handle.m_pShared;

				return *this;
			}

			Handle& operator=(Handle&& handle)
			{
				if(m_pShared != handle.m_pShared)
				{
					if(m_pShared)
						m_pShared->Release();
					m_pShared = handle.m_pShared;

					handle.m_pShared = nullptr;
				}
				
				return *this;
			}

			bool operator==(const Handle& handle) const
			{
				return m_pShared == handle.m_pShared;
			}

			bool operator!=(const Handle& handle) const
			{
				return m_pShared != handle.m_pShared;
			}

			bool operator==(const Type& object) const
			{
				return m_pShared->GetObject() == object;
			}

			bool operator==(const Type* pObject) const
			{
				return &m_pShared->GetObject() == pObject;
			}

			~Handle(void)
			{
				if(m_pShared)
					m_pShared->Release();
			}

			bool IsValid(void) const
			{
				return m_pShared && m_pShared->HasValidOwner();
			}

			void Invalidate(void)
			{
				if(m_pShared)
				{
					m_pShared->Release();
					m_pShared = nullptr;
				}
			}

			operator Type&(void)
			{
				return m_pShared->GetObject();
			}

			operator const Type&(void) const
			{
				return m_pShared->GetObject();
			}

			operator Type*(void)
			{
				return &m_pShared->GetObject();
			}

			operator const Type*(void) const
			{
				return &m_pShared->GetObject();
			}

			Type* operator->(void)
			{
				return &m_pShared->GetObject();
			}

			const Type* operator->(void) const
			{
				return &m_pShared->GetObject();
			}

		private:

			Shared<Type>* m_pShared;
		};

		/// Manages a single, non-trivially movable ownership to an object
		/** This class creates an instance of the specified type directly on the stack when initialized.
		*	It also creates a shared instance to communicate with its handles. Ownership cannot be reassigned,
		*	though copy/move construction is possible, if the object supports a copy/move constructor.*/
		template<typename Type>
		class Owner final
		{
		public:

			template<typename... Args>
			Owner(Args&&... args) : m_object(args...)
			{
				m_pShared = new Shared<Type>(*this);
			}

			Owner(const Owner& owner) : m_object(owner.m_object)
			{
				m_pShared = new Shared<Type>(*this);
			}

			Owner(Owner<Type>&& owner) : m_object(std::move(owner.m_object)),
				m_pShared(owner.m_pShared)
			{
				m_pShared->SetOwner(this);
				owner.m_pShared = nullptr;
			}

			~Owner(void)
			{
				m_pShared->SetOwner(nullptr);
				m_pShared->Release();
			}

			void operator=(Owner<Type>&& owner)
			{
				if(&owner != this)
				{
					m_object = std::move(owner.m_object);
					m_pShared = owner.m_pShared;
					m_pShared->SetOwner(this);
					owner.m_pShared = nullptr;
				}
			}

			void operator=(const Owner<Type>& owner)
			{
				ACL_ASSERT(false);
			}

			Handle<Type> CreateHandle(void)
			{
				return Handle<Type>(*m_pShared);
			}

			Owner& Clone(void) const
			{
				return *new Owner(*this);
			}

		private:

			friend class Shared<Type>;

			Type m_object;
			Shared<Type>* m_pShared;
		};

	}
}