#pragma once

namespace acl
{
	namespace gfx
	{
		struct LoadContext;
	}

	namespace render
	{
		struct Context;
	}

	namespace core
	{
		struct GameStateContext;

		class IModule
		{
		public:

			virtual ~IModule(void) = 0 {};

			virtual void OnLoadResources(const gfx::LoadContext& context) = 0;
			virtual void OnLoadRender(const render::Context& context) = 0;
			virtual void OnInit(const GameStateContext& context) = 0;
			virtual void OnUninit(const GameStateContext& contex) = 0;

			virtual void OnUpdate(double dt) = 0;
			virtual void OnRender(void) const = 0;

		};

	}
}