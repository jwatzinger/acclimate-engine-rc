#pragma once

namespace acl
{
	namespace core
	{

		class ISceneExtention
		{
		public:

			virtual ~ISceneExtention(void) = 0 {}

			virtual void OnNew(void) = 0;
			virtual void OnLoad(void) = 0;
			virtual void OnUnload(void) = 0;
			virtual void OnSave(void) = 0;
			
		};
	}
}