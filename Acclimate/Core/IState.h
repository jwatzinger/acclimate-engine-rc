#pragma once

namespace acl
{
	namespace core
	{
        /// Interface for states
		/** This interface should be implemented for any class that describes a
        *   state in game logic. It should only expose its internal by overriding the
        *   pure virtual Run method, and perform any other tasks via this. The state
        *   is also responsible for deciding which state should be run next in its near
        *   context. */
		class IState
		{
		public:

			virtual ~IState(void) = 0 {};

            /** Advances the state
             *  This method tells the state that is should advance. Any logic processed by the
             *  state should at least be called from this method. It is also responsible
             *  for determining which state should be present next. The state shall return
             *  its \c this pointer, if it wants to continue. If another state should be set,
             *  the state must dynamically create it and return its pointer instead. If the
             *  \c nullptr is returned, the state signalizes that it is completely done.
             *
             *  @param[in] dt The delta timestep.
             *
             *  @return Pointer to the state that should be run next.
             */
			virtual IState* Run(double dt) = 0;
			virtual void Render(void) const = 0;

		};

	}
}