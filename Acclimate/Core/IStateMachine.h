#pragma once

namespace acl
{
	namespace core
	{
		class IState;

        /// Interface for state machines
		/** This interface should be implemented for any class that is a state machine.
        *   It should not contain any logic other than handling the lifetime of states.
        *   It should implement a sutable memory management, so that as specified in IState
        *   each state can create the next running state dynamically. */
		class IStateMachine
		{
		public:

			virtual ~IStateMachine(void) = 0 {};

            /** Sets the current running state.
             *  This method tells the state machine which state it should run next. It
             *  should make sure that current active state gets properly deleted.
             *
             *  @param[in] pState A pointer to the new state.
             */
			virtual void SetState(IState* pState) = 0;

            /** Tells the machine to run the current active state.
             *  This method runs the active state. It should not implement any other logic,
             *  except that it should handle the output of the states Run() method, so that
             *  a state can activate a state transition.
             *
             *  @param[in] dt The delta timestep.
             *
             *  @return Indicates whether there was a state to run.
             */
			virtual bool Run(double dt) = 0;

		};

	}
}

