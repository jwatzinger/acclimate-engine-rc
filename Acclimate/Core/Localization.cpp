#include "Localization.h"
#include "LocalizedString.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		Localization::Localization(void)
		{
		}

		Localization::~Localization(void)
		{
			for(auto& mStrings : m_mStrings)
			{
				for(auto& strings : mStrings.second)
				{
					delete strings.second;
				}
			}

			for(auto& local : m_mLocals)
			{
				delete local.second;
			}
		}

		void Localization::SetLanguage(const std::wstring& stLanguage)
		{
			if(m_stLanguage != stLanguage)
			{
				m_stLanguage = stLanguage;

				auto& mStrings = m_mStrings[stLanguage];

				for(auto& local : m_mLocals)
				{
					auto itr = mStrings.find(local.first);
					if(itr != mStrings.end())
						local.second->SetString(*itr->second);
				}
			}
		}

		void Localization::SetDefaultLanguage(const std::wstring& stLanguage)
		{
			m_stDefault = stLanguage;
			if(m_stLanguage.empty())
				SetLanguage(m_stDefault);
		}

		void Localization::SetString(const std::wstring& stLanguage, const std::wstring& stIdentifier, const std::wstring& stContent)
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
			{
				LocalizedString* pLocalized = itr->second;

				auto itr = m_mStrings.find(stLanguage);
				if(itr != m_mStrings.end())
				{
					auto itr2 = itr->second.find(stIdentifier);
					if(itr2 != itr->second.end())
						*itr2->second = stContent;
					else
					{
						auto pString = new std::wstring(stContent);
						itr->second[stIdentifier] = pString;

						if(m_stLanguage == stLanguage)
							pLocalized->SetString(*pString);
					}
				}
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to set language string for identifier", stIdentifier, "with content \"", stContent, "\" and language", stLanguage, ". Language does not exist.");
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to set language string for identifier", stIdentifier, "with content \"", stContent, "\" and language", stLanguage, ". Identifier does not exist.");
		}

		void Localization::AddCategory(const std::wstring& stCategory)
		{
			if(m_mCategories.count(stCategory))
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add category", stCategory, "to localization cache. Category is already registered.");
			else
				m_mCategories[stCategory];
		}

		void Localization::AddLanguage(const std::wstring& stLanguage)
		{
			if(m_mStrings.count(stLanguage))
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add language", stLanguage, "to localization cache. Language is already registered.");
			else
				m_mStrings[stLanguage];
		}

		void Localization::AddIdentifier(const std::wstring& stIdentifier, const std::wstring& stCategory)
		{
			if(!m_mLocals.count(stIdentifier))
			{
				if(m_mCategories.count(stCategory))
				{
					auto pLocalizedString = new LocalizedString(stCategory);
					m_mLocals.emplace(stIdentifier, pLocalizedString);
					m_mCategories[stCategory].emplace(stIdentifier, pLocalizedString);
				}
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add localization identifier", stIdentifier, "to category", stCategory, ". Category does not exist.");
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed toi add localization identifier", stIdentifier, "to category", stCategory, ". Identifier already exists.");
		}

		void Localization::AddMarkup(const std::wstring& stIdentifier, const std::wstring& stMarkup, const std::wstring& stValue)
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
				itr->second->AddMarkup(stMarkup, stValue);
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to add markup to localization identifier", stIdentifier, ". Identifier doesn't exist.");
		}

		void Localization::RemoveMarkup(const std::wstring& stIdentifier, const std::wstring& stMarkup)
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
				itr->second->RemoveMarkup(stMarkup);
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to remove markup to localization identifier", stIdentifier, ". Identifier doesn't exist.");
		}

		void Localization::RemoveIdentifier(const std::wstring& stIdentifier)
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
			{
				// erase from category

				auto& mIdentifiers = m_mCategories.at(itr->second->GetCategory());
				mIdentifiers.erase(stIdentifier);

				// erase identfier
				delete itr->second;
				m_mLocals.erase(stIdentifier);

				// erase from language
				for(auto& language : m_mStrings)
				{
					auto itr2 = language.second.find(stIdentifier);
					if(itr2 != language.second.end())
					{
						delete itr2->second;
						language.second.erase(itr2);
					}
				}
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to remove markup from localization identifier", stIdentifier, ". Identifier doesn't exist.");
		}

		void Localization::RemoveCategory(const std::wstring& stCategory)
		{
			auto itr = m_mCategories.find(stCategory);
			if(itr != m_mCategories.end())
			{
				// erase locals
				for(auto& local : itr->second)
				{
					delete local.second;
					m_mLocals.erase(local.first);

					// erase strings
					for(auto& language : m_mStrings)
					{
						auto itr2 = language.second.find(local.first);
						if(itr2 != language.second.end())
						{
							delete itr2->second;
							language.second.erase(itr2);
						}
					}
				}

				m_mCategories.erase(itr);
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to remove category", stCategory, "from localization. Category doesn't exist.");
		}

		bool Localization::HasLanguage(const std::wstring& stLanguage) const
		{
			return m_mStrings.count(stLanguage) != 0;
		}

		bool Localization::HasCategory(const std::wstring& stCategory) const
		{
			return m_mCategories.count(stCategory) != 0;
		}

		LocalizedString& Localization::GetString(const std::wstring& stIdentifier)
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
				return *itr->second;
			else
			{
				static const std::wstring stNoString = L"???";
				static LocalizedString string(stNoString);
				return string;
			}
		}

		const LocalizedString& Localization::GetString(const std::wstring& stIdentifier) const
		{
			auto itr = m_mLocals.find(stIdentifier);
			if(itr != m_mLocals.end())
				return *itr->second;
			else
			{
				static const std::wstring stNoString = L"???";
				static const LocalizedString string(stNoString);
				return string;
			}
		}

		Localization::IdentifierVector Localization::GetIdentifiers(void) const
		{
			IdentifierVector vIdentifiers;
			vIdentifiers.reserve(m_mLocals.size());
			for(auto& local : m_mLocals)
			{
				vIdentifiers.push_back(local.first);
			}
			return vIdentifiers;
		}

		const std::wstring& Localization::GetDefaultLanguage(void) const
		{
			return m_stDefault;
		}

		const std::wstring& Localization::GetLocalString(const std::wstring& stLanguage, const std::wstring& stIdentifier)
		{
			static const std::wstring stEmpty = L"";

			auto itr = m_mStrings.find(stLanguage);
			if(itr != m_mStrings.end())
			{
				auto itr2 = itr->second.find(stIdentifier);
				if(itr2 != itr->second.end())
					return *itr2->second;
				else
					return stEmpty;
			}
			else
				return stEmpty;
		}

		Localization::LanguageVector Localization::GetLanguages(void) const
		{
			LanguageVector vLanguages;
			vLanguages.reserve(m_mStrings.size());
			for(auto& language : m_mStrings)
			{
				vLanguages.push_back(language.first);
			}
			return vLanguages;
		}

		Localization::LocaleMap Localization::GetLocales(const std::wstring& stLanguage) const
		{
			LocaleMap mLocales;

			auto itr = m_mStrings.find(stLanguage);
			if(itr != m_mStrings.end())
			{
				mLocales.reserve(itr->second.size());
				for(auto& string : itr->second)
				{
					mLocales.emplace(string.first, string.second);
				}
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to access locals of language", stLanguage, ". Language does not exist.");

			return mLocales;
		}

		const Localization::CategoryMap& Localization::GetCategories(void) const
		{
			return m_mCategories;
		}

	}
}

