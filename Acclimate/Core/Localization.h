#pragma once
#include <string>
#include <unordered_map>
#include <map>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class LocalizedString;

		class ACCLIMATE_API Localization
		{
			typedef std::unordered_map<std::wstring, std::unordered_map<std::wstring, std::wstring*>> StringMap;
			typedef std::unordered_map<std::wstring, LocalizedString*> LocalizeMap;
		public:
			typedef std::vector<std::wstring> IdentifierVector;
			typedef std::vector<std::wstring> LanguageVector;
			typedef std::unordered_map<std::wstring, const std::wstring*> LocaleMap;
			typedef std::map<std::wstring, std::map<std::wstring, const LocalizedString*>> CategoryMap;

			Localization(void);
			~Localization(void);

			void SetLanguage(const std::wstring& stLanguage);
			void SetDefaultLanguage(const std::wstring& stLanguage);
			void SetString(const std::wstring& stLanguage, const std::wstring& stIdentifier, const std::wstring& stContent);
			void AddCategory(const std::wstring& stCategory);
			void AddLanguage(const std::wstring& stLanguage);
			void AddIdentifier(const std::wstring& stIdentifier, const std::wstring& stCategory);
			void AddMarkup(const std::wstring& stIdentifier, const std::wstring& stMarkup, const std::wstring& stValue);
			void RemoveMarkup(const std::wstring& stIdentifier, const std::wstring& stMarkup);
			void RemoveIdentifier(const std::wstring& stIdentifier);
			void RemoveCategory(const std::wstring& stCategory);

			bool HasLanguage(const std::wstring& stLanguage) const;
			bool HasCategory(const std::wstring& stCategory) const;
			LocalizedString& GetString(const std::wstring& stIdentifier);
			const LocalizedString& GetString(const std::wstring& stIdentifier) const;
			IdentifierVector GetIdentifiers(void) const;
			const std::wstring& GetDefaultLanguage(void) const;
			const std::wstring& GetLocalString(const std::wstring& stLanguage, const std::wstring& stIdentifier);
			LanguageVector GetLanguages(void) const;
			LocaleMap GetLocales(const std::wstring& stLanguage) const;
			const CategoryMap& GetCategories(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stLanguage, m_stDefault;
			StringMap m_mStrings;
			LocalizeMap m_mLocals;
			CategoryMap m_mCategories;
#pragma warning( default: 4251 )
		};

	}
}
