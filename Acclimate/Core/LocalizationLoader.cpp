#include "LocalizationLoader.h"
#include "Localization.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{
		
		LocalizationLoader::LocalizationLoader(Localization& localization) : m_pLocalization(&localization)
		{
		}

		void LocalizationLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			if(auto pRoot = doc.Root(L"Localization"))
			{
				if(auto pDefault = pRoot->Attribute(L"default"))
					m_pLocalization->SetDefaultLanguage(*pDefault);

				// identifiers
				if(auto pCategories = pRoot->FirstNode(L"Categories"))
				{
					if(auto pCategorieNodes = pCategories->Nodes(L"Category"))
					{
						for(auto pCategory : *pCategorieNodes)
						{
							auto& stCategory = pCategory->Attribute(L"name")->GetValue();
							m_pLocalization->AddCategory(stCategory);

							if(auto pIdentifierNodes = pCategory->Nodes(L"Identifier"))
							{
								for(auto pIdentifier : *pIdentifierNodes)
								{
									auto& stName = pIdentifier->Attribute(L"name")->GetValue();

									m_pLocalization->AddIdentifier(stName, stCategory);

									if(auto pMarkups = pIdentifier->Nodes(L"Markup"))
									{
										for(auto pMarkup : *pMarkups)
										{
											auto& stMarkup = pMarkup->GetValue();

											std::wstring stValue;
											if(auto pValue = pMarkup->Attribute(L"value"))
												stValue = pValue->GetValue();
											else
												stValue = L"";

											m_pLocalization->AddMarkup(stName, stMarkup, stValue);
										}
									}
								}
							}
						}
					}	
				}

				// languages
				if(auto pLanguages = pRoot->FirstNode(L"Languages"))
				{
					if(auto pLanguageNodes = pLanguages->Nodes(L"Language"))
					{
						for(auto pLanguage : *pLanguageNodes)
						{
							const std::wstring& stName = *pLanguage->Attribute(L"name");

							if(!m_pLocalization->HasLanguage(stName))
								m_pLocalization->AddLanguage(stName);

							if(auto pTexts = pLanguage->Nodes(L"Text"))
							{
								for(auto pText : *pTexts)
								{
									const std::wstring& stId = *pText->Attribute(L"id");
									const std::wstring& stContent = pText->GetValue();

									m_pLocalization->SetString(stName, stId, stContent);
								}
							}
						}
					}
				}
			}
		}

	}
}

