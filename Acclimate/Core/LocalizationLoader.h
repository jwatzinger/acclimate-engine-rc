#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class Localization;

		class ACCLIMATE_API LocalizationLoader
		{
		public:
			LocalizationLoader(Localization& localization);

			void Load(const std::wstring& stFilename) const;

		private:

			Localization* m_pLocalization;
		};

	}
}


