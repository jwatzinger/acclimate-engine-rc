#include "LocalizationSaver.h"
#include "Localization.h"
#include "LocalizedString.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		LocalizationSaver::LocalizationSaver(const Localization& localization) : m_pLocalization(&localization)
		{
		}

		LocalizationSaver::~LocalizationSaver(void)
		{
		}

		void LocalizationSaver::Save(const std::wstring& stFile) const
		{
			xml::Doc doc;

			auto& localization = doc.InsertNode(L"Localization");
			localization.ModifyAttribute(L"default", m_pLocalization->GetDefaultLanguage());

			// identifiers
			auto& categoriesNode = localization.InsertNode(L"Categories");
			for(auto& category : m_pLocalization->GetCategories())
			{
				auto& categorieNode = categoriesNode.InsertNode(L"Category");
				categorieNode.ModifyAttribute(L"name", category.first);

				for(auto& identifier : category.second)
				{
					auto& identifierNode = categorieNode.InsertNode(L"Identifier");
					identifierNode.ModifyAttribute(L"name", identifier.first);

					auto pLocal = identifier.second;

					std::map<std::wstring, std::wstring> mMarkups;
					for(auto& markup : pLocal->GetMarkups())
					{
						mMarkups.emplace(markup.first, markup.second);
					}

					for(auto& markup : mMarkups)
					{
						auto& markupNode = identifierNode.InsertNode(L"Markup");
						markupNode.SetValue(markup.first);
						markupNode.ModifyAttribute(L"value", markup.second);
					}
				}
			}

			// languages
			auto& languagesNode = localization.InsertNode(L"Languages");
			for(auto& stLanguage : m_pLocalization->GetLanguages())
			{
				auto& languageNode = languagesNode.InsertNode(L"Language");
				languageNode.ModifyAttribute(L"name", stLanguage);

				std::map<std::wstring, const std::wstring*> mSorted;

				for(auto& locales : m_pLocalization->GetLocales(stLanguage))
				{
					mSorted.emplace(locales.first, locales.second);
				}

				for(auto& locales : mSorted)
				{
					auto& textNode = languageNode.InsertNode(L"Text");
					textNode.ModifyAttribute(L"id", locales.first);
					textNode.SetValue(*locales.second);
				}
			}

			doc.SaveFile(stFile);
		}

	}
}

