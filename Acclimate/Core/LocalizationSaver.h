#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class Localization;

		class ACCLIMATE_API LocalizationSaver
		{
		public:
			LocalizationSaver(const Localization& localization);
			~LocalizationSaver(void);

			void Save(const std::wstring& stFile) const;

		private:

			const Localization* m_pLocalization;
		};

	}
}

