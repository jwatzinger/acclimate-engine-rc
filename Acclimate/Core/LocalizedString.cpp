#include "LocalizedString.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		LocalizedString::LocalizedString(const std::wstring& stCategory) : m_stCategory(stCategory), m_pString(nullptr)
		{
		}

		LocalizedString::LocalizedString(const std::wstring& stCategory, const std::wstring& stString) : m_stCategory(stCategory), m_pString(&stString)
		{
		}

		LocalizedString::~LocalizedString(void)
		{
		}

		void LocalizedString::SetString(const std::wstring& stString)
		{
			m_pString = &stString;
		}

		void LocalizedString::AddMarkup(const std::wstring& stMarkup, const std::wstring& stValue)
		{
			m_mMarkups[stMarkup] = stValue;
		}

		void LocalizedString::RemoveMarkup(const std::wstring& stMarkup)
		{
			auto itr = m_mMarkups.find(stMarkup);
			if(itr != m_mMarkups.end())
				m_mMarkups.erase(itr);
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to remove markup", stMarkup, "from localized string. Markup does not exist.");
		}

		bool LocalizedString::HasMarkup(const std::wstring& stMarkup) const
		{
			return m_mMarkups.count(stMarkup) != 0;
		}

		const std::wstring& LocalizedString::GetMarkup(const std::wstring& stMarkup) const
		{
			auto itr = m_mMarkups.find(stMarkup);
			if(itr != m_mMarkups.end())
				return itr->second;
			else
			{
				static const std::wstring stEmpty(L"");
				return stEmpty;
			}
		}

		const std::wstring& LocalizedString::GetString(void) const
		{
			if(m_pString)
				return *m_pString;
			else
			{
				static const std::wstring stEmpty = L"";
				return stEmpty;
			}
		}

		const LocalizedString::MarkupMap& LocalizedString::GetMarkups(void) const
		{
			return m_mMarkups;
		}

		const std::wstring& LocalizedString::GetCategory(void) const
		{
			return m_stCategory;
		}

		LocalizedString::operator const std::wstring&(void) const
		{
			return GetString();
		}

	}
}
