#pragma once
#include <string>
#include <unordered_map>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class ACCLIMATE_API LocalizedString
		{
		public:
			typedef std::unordered_map<std::wstring, std::wstring> MarkupMap;

			LocalizedString(const std::wstring& stCategory);
			LocalizedString(const std::wstring& stCategory, const std::wstring& stString);
			~LocalizedString(void);

			void SetString(const std::wstring& stString);
			void AddMarkup(const std::wstring& stMarkup, const std::wstring& stValue);
			void RemoveMarkup(const std::wstring& stMarkup);

			bool HasMarkup(const std::wstring& stMarkup) const;
			const std::wstring& GetMarkup(const std::wstring& stMarkup) const;
			const std::wstring& GetString(void) const;
			const MarkupMap& GetMarkups(void) const;
			const std::wstring& GetCategory(void) const;

			operator const std::wstring&(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stCategory;
			const std::wstring* m_pString;

			MarkupMap m_mMarkups;
#pragma warning( default: 4251 )
		};

	}
}

