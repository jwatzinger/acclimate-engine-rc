#include "ModuleLoader.h"
#include <Windows.h>
#include "ModuleManager.h"
#include "..\System\Log.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{
		class IModule;
		typedef IModule& (*module)(); 

		ModuleLoader::ModuleLoader(ModuleManager& manager): m_pModuleManager(&manager)
		{
		}

		void ModuleLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			sys::WorkingDirectory dir(stFilename, true);

			if(const auto pRoot = doc.Root(L"Modules"))
			{
				if(const auto pModules = pRoot->Nodes(L"Module"))
				{
					sys::WorkingDirectory dir2;
					if(auto pPath = pRoot->Attribute(L"path"))
						dir2 = sys::WorkingDirectory(pPath->GetValue());

					for(const auto& pModule : *pModules)
					{
						const std::wstring& stName = pModule->Attribute(L"name")->GetValue();

						LoadModule(stName, dir2.GetDirectory() + L"\\" + stName + L"\\", stName + L".dll");
					}

					m_pModuleManager->LoadResources();
					m_pModuleManager->LoadRender();
					m_pModuleManager->InitModules();
				}
			}
		}

		void ModuleLoader::LoadModule(const std::wstring& stName, const std::wstring& stPath, const std::wstring& stFilename) const
		{
			sys::log->Out(sys::LogModule::CORE, sys::LogType::INFO, "loading module", stName, "from dll.");

			HINSTANCE hGetProcIDDLL = LoadLibrary((stPath + stFilename).c_str());

			if(!hGetProcIDDLL)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Failed to load dll for module", stName, ".");
				return;
			}

			if(module mod = (module)GetProcAddress(hGetProcIDDLL, "CreateModule"))
			{
				IModule& module = mod();
				m_pModuleManager->AddModule(stName, stPath, module, hGetProcIDDLL);
			}
			else
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Dll for module", stName, "has no game binding. (might be pure editor plugin)");
			}
		}

	}
}