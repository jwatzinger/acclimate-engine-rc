#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class ModuleManager;

		class ACCLIMATE_API ModuleLoader
		{
		public:

			ModuleLoader(ModuleManager& manager);
		
			void Load(const std::wstring& stFilename) const;
			void LoadModule(const std::wstring& stName, const std::wstring& stPath, const std::wstring& stFilename) const;

		private:

			ModuleManager* m_pModuleManager;
			
		};

	}
}

