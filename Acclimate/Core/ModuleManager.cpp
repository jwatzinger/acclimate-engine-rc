#include "ModuleManager.h"
#include "BaseContext.h"
#include "..\Gfx\ResourceBlock.h"
#include "..\System\WorkingDirectory.h"

namespace acl
{
	namespace core
	{

		const gfx::PathData paths(L"Resources/", L"Textures/", L"Effects/", L"Meshes/");

		ModuleManager::ModuleManager(const GameStateContext& context): m_pContext(&context)
		{
		}

		ModuleManager::~ModuleManager(void)
		{
			Clear();
		}

		void ModuleManager::AddModule(const std::wstring& stName, const std::wstring& stPath, IModule& module, HINSTANCE hInstance)
		{
			ModuleStorage storage = { &module, hInstance, new gfx::ResourceBlock(m_pContext->gfx.resources, paths), stPath, stName };
			m_mModules.push_back(storage);
		}

		std::vector<std::wstring> ModuleManager::GetModuleNames(void) const
		{
			std::vector<std::wstring> vModules;
			for(auto& module : m_mModules)
			{
				vModules.push_back(module.stName);
			}

			return vModules;
		}

		const ModuleStorage* ModuleManager::GetModule(const std::wstring& stName) const
		{
			for(auto& module : m_mModules)
			{
				if(module.stName == stName)
					return &module;
			}
			
			return nullptr;
		}

		void ModuleManager::LoadResources(void) const
		{
			for(auto& module : m_mModules)
			{
				if(!module.stPath.empty())
				{
					sys::WorkingDirectory dir(module.stPath.c_str());

					module.pResources->Begin();
					module.pModule->OnLoadResources(m_pContext->gfx.load);
					module.pResources->End();
				}
				else
					module.pModule->OnLoadResources(m_pContext->gfx.load);
			}
		}

		void ModuleManager::LoadRender(void) const
		{
			for(auto& module : m_mModules)
			{
				if(!module.stPath.empty())
				{
					sys::WorkingDirectory dir(module.stPath.c_str());

					module.pModule->OnLoadRender(m_pContext->render);
				}
				else
					module.pModule->OnLoadRender(m_pContext->render);
			}
		}

		void ModuleManager::InitModules(void) const
		{
			for(auto& module : m_mModules)
			{
				sys::WorkingDirectory dir(module.stPath.c_str());

				module.pResources->Begin();
				module.pModule->OnInit(*m_pContext);
				module.pResources->End();
			}
		}

		void ModuleManager::UninitModules(void) const
		{
			for(auto& module : m_mModules)
			{
				module.pModule->OnUninit(*m_pContext);
				module.pResources->Clear();
			}
		}

		void ModuleManager::UpdateModules(double dt) const
		{
			for(auto& module : m_mModules)
			{
				module.pModule->OnUpdate(dt);
			}
		}

		void ModuleManager::RenderModules(void) const
		{
			for(auto& module : m_mModules)
			{
				module.pModule->OnRender();
			}
		}

		void ModuleManager::Clear(void)
		{
			for(auto& module : m_mModules)
			{
				module.pModule->OnUninit(*m_pContext);
				module.pResources->Clear();
				delete module.pResources;
				delete module.pModule;

				if(module.hInstance)
					FreeLibrary(module.hInstance);
			}
			m_mModules.clear();
		}

	}
}