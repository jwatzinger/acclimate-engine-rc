#pragma once
#include <string>
#include <vector>
#include <Windows.h>
#include "IModule.h"
#include "..\Core\Dll.h"

#undef nullptr

namespace acl
{
	namespace gfx
	{
		class ResourceBlock;
	}

	namespace core
	{
		struct GameStateContext;

		class IModule;

		struct ModuleStorage
		{
			IModule* pModule;
			HINSTANCE hInstance;
			gfx::ResourceBlock* pResources;
			std::wstring stPath;
			std::wstring stName;
		};

		class ACCLIMATE_API ModuleManager
		{
			typedef std::vector<ModuleStorage> ModuleMap;
		public:
			ModuleManager(const GameStateContext& context);
			~ModuleManager(void);

			void AddModule(const std::wstring& stName, const std::wstring& stPath, IModule& module, HINSTANCE hInstance);
			template<typename Module, typename... Args>
			void AddModule(const std::wstring& stName, Args&& ...args);

			std::vector<std::wstring> GetModuleNames(void) const;
			const ModuleStorage* GetModule(const std::wstring& stName) const;

			void LoadResources(void) const;
			void LoadRender(void) const;
			void InitModules(void) const;
			void UninitModules(void) const;
			void UpdateModules(double dt) const;
			void RenderModules(void) const;

			void Clear(void);

		private:

			const GameStateContext* m_pContext;

#pragma warning( disable: 4251 )
			ModuleMap m_mModules;
#pragma warning( default: 4251 )
		};

		template<typename Module, typename... Args>
		void ModuleManager::AddModule(const std::wstring& stName, Args&& ...args)
		{
			Module& module = *new Module(args...);
			AddModule(stName, L"", module, nullptr);
		}

	}
}

