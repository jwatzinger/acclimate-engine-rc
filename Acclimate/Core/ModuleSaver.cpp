#include "ModuleSaver.h"
#include "ModuleManager.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		ModuleSaver::ModuleSaver(ModuleManager& modules):
			m_pManager(&modules)
		{
		}

		void ModuleSaver::Save(const std::wstring& stName) const
		{
			xml::Doc doc;
			
			auto& root = doc.InsertNode(L"Modules");
			root.ModifyAttribute(L"path", L"../Modules"); //TODO: un-hardcode

			for(auto& stModule : m_pManager->GetModuleNames())
			{
				auto pStorage = m_pManager->GetModule(stModule);

				auto& module = root.InsertNode(L"Module");
				module.ModifyAttribute(L"name", pStorage->stName);
			}

			doc.SaveFile(stName);
		}

	}
}