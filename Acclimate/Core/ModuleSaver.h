#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class ModuleManager;

		class ACCLIMATE_API ModuleSaver
		{
		public:
			ModuleSaver(ModuleManager& modules);

			void Save(const std::wstring& stName) const;

		private:

			ModuleManager* m_pManager;
		};

	}
}

