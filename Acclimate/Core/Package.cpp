#include "Package.h"
#include "ModuleManager.h"
#include "..\Entity\Context.h"
#include "..\Gfx\Context.h"

namespace acl
{
	namespace core
	{

		Package::Package(const gfx::Context& gfxContext, const ecs::Context& entityContext, const script::Context& scriptContext, const physics::Context& physicsContext, const input::Context& inputContext):
			m_settingLoader(m_settings), m_scenes(gfxContext.resources, *gfxContext.load.pLoader, scriptContext, entityContext.entities, m_resources, m_events, physicsContext, inputContext),
			m_sceneLoader(m_scenes), m_context(m_settings, m_settingLoader, m_scenes, m_sceneLoader, m_resources, m_eventContext, m_localization), m_pModules(nullptr), m_eventContext(m_events, m_trigger)
		{
		}

		Package::~Package(void)
		{
			m_resources.Clear();
			delete m_pModules;
		}

		void Package::SetupModuleManager(const GameStateContext& ctx)
		{
			delete m_pModules;
			m_pModules = new ModuleManager(ctx);
			m_context.pModules = m_pModules;
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

	}
}

