#pragma once
#include "SettingModule.h"
#include "SettingLoader.h"
#include "SceneManager.h"
#include "SceneLoader.h"
#include "ResourceManager.h"
#include "Events.h"
#include "Localization.h"
#include "Context.h"
#include "EventContext.h"
#include "EventTriggerHandler.h"

namespace acl
{
	namespace ecs
	{
		struct Context;
	}

	namespace gfx
	{
		struct Context;
	}

	namespace input
	{
		struct Context;
	}

	namespace physics
	{
		struct Context;
	}

	namespace script
	{
		struct Context;
	}

	namespace core
	{

		class SceneManager;
		class SceneLoader;
		struct GameStateContext;
		class ModuleManager;

		class Package
		{
		public:
			Package(const gfx::Context& gfxContext, const ecs::Context& entityContext, const script::Context& scriptContext, const physics::Context& physicsContext, const input::Context& inputContext);
			~Package(void);

			void SetupModuleManager(const GameStateContext& ctx);

			const Context& GetContext(void) const;
			
		private:

			SettingModule m_settings;
			SettingLoader m_settingLoader;
			ResourceManager m_resources;
			SceneManager m_scenes;
			SceneLoader m_sceneLoader;
			Localization m_localization;
			Events m_events;
			EventTriggerHandler m_trigger;
			EventContext m_eventContext;
			Context m_context;
			ModuleManager* m_pModules;
		};

	}
}


