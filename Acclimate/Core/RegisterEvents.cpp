#include "RegisterEvents.h"
#include "EventLoader.h"
#include "SceneManager.h"
#include "Context.h"
#include "EventDeclaration.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace core
	{
		namespace detail
		{
			SceneManager* _pScenes = nullptr;
		}

		/*****************************************
		* Condition
		******************************************/

		void registerEvents(const core::Context& context)
		{
			detail::_pScenes = &context.scenes;

			// events
			EventLoader::RegisterEvent<Condition>();
			EventLoader::RegisterEvent<Block>();
			EventLoader::RegisterEvent<Wait>();
			EventLoader::RegisterEvent<Parallel>();
			EventLoader::RegisterEvent<ForLoop>();
			EventLoader::RegisterEvent<WhileLoop>();
			EventLoader::RegisterEvent<ForEachIntLoop>();
			EventLoader::RegisterEvent<ActivateScene>();

			// functions
			EventLoader::RegisterFunction<BoolOr>();
			EventLoader::RegisterFunction<BoolToInt>();
			EventLoader::RegisterFunction<AddFloat>();
			EventLoader::RegisterFunction<SubFloat>();
			EventLoader::RegisterFunction<MulFloat>();
			EventLoader::RegisterFunction<GreaterEqualFloat>();
			EventLoader::RegisterFunction<FloatToInt>();
			EventLoader::RegisterFunction<AddInt>();
			EventLoader::RegisterFunction<SubInt>();
			EventLoader::RegisterFunction<MulInt>();
			EventLoader::RegisterFunction<DivInt>();
			EventLoader::RegisterFunction<ModInt>();
			EventLoader::RegisterFunction<EqualsInt>();
			EventLoader::RegisterFunction<LessEqualInt>();
			EventLoader::RegisterFunction<GreaterEqualInt>();
			EventLoader::RegisterFunction<SelectInt>();
			EventLoader::RegisterFunction<IntToString>();
			EventLoader::RegisterFunction<GetStringArrayElement>();
			EventLoader::RegisterFunction<GetStringSubArray>();
		}

		/*****************************************
		* ----------------------------------------
		* EVENTS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* Condition
		******************************************/

		void Condition::Reset(void)
		{
		}

		void Condition::OnExecute(double dt)
		{
			if(GetAttribute<bool>(0))
				CallOutput(0);
			else
				CallOutput(1);
		}

		const EventDeclaration& Condition::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"condition", AttributeType::BOOL, false }
			};

			const OutputVector vOutputs = { L"True", L"False" };

			static const EventDeclaration decl(L"Condition", vAttributes, AttributeVector(), vOutputs);

			return decl;
		}

		/*****************************************
		* Block
		******************************************/

		void Block::Reset(void)
		{
		}

		void Block::OnExecute(double dt)
		{
			if(GetAttribute<bool>(0))
				CallOutput(0);
		}

		const EventDeclaration& Block::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"condition", AttributeType::BOOL, false }
			};

			static const EventDeclaration decl(L"Block", vAttributes, AttributeVector());

			return decl;
		}

		/*****************************************
		* Wait
		******************************************/

		Wait::Wait(void) : m_nowTime(0.0f)
		{
		}

		Wait::Wait(const Wait& wait) :
			BaseEvent(wait), m_nowTime(wait.m_nowTime)
		{
		}

		void Wait::Reset(void)
		{
			m_nowTime = 0.0f;
		}

		void Wait::OnExecute(double dt)
		{
			const auto wait = GetAttribute<float>(0);
			TakeTime(wait - m_nowTime);

			m_nowTime += dt;
			if(m_nowTime >= wait)
				CallOutput(0);
		}

		const EventDeclaration& Wait::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"time", AttributeType::FLOAT, false }
			};

			static const EventDeclaration decl(L"Wait", vAttributes, AttributeVector());

			return decl;
		}

		/*****************************************
		* Parallel
		******************************************/

		void Parallel::Reset(void)
		{
		}

		void Parallel::OnExecute(double dt)
		{
			auto& mOutputs = GetOutputs();
			for(auto slot : mOutputs)
			{
				CallOutput(slot.first);
			}
		}

		const EventDeclaration& Parallel::GenerateDeclaration(void)
		{
			const OutputVector vOutputs = { L"", L"", L"", L"" };

			static const EventDeclaration decl(L"Parallel", AttributeVector(), AttributeVector(), vOutputs);

			return decl;
		}

		/*****************************************
		* ForLoop
		******************************************/

		void ForLoop::Reset(void)
		{
			m_firstIndex = GetAttribute<int>(0);
			m_lastIndex = GetAttribute<int>(1);

			m_currentIndex = m_firstIndex;

			ActivateFlowControl();
		}

		void ForLoop::OnExecute(double dt)
		{
			if(m_currentIndex <= m_lastIndex)
			{
				ReturnValue<int>(0, m_currentIndex);
				CallOutput(0);
				m_currentIndex++;
			}
			else
			{
				DeactivateFlowControl();
				CallOutput(1);
			}
		}

		const EventDeclaration& ForLoop::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"First index", AttributeType::INT, false },
				{ L"Last index", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"Index", AttributeType::INT, false },
			};

			const OutputVector vOutputs = 
			{ 
				L"Body", 
				L"Finished" 
			};

			static const EventDeclaration decl(L"ForLoop", vAttributes, vReturns, vOutputs);

			return decl;
		}

		/*****************************************
		* WhileLoop
		******************************************/

		void WhileLoop::Reset(void)
		{
			ActivateFlowControl();
		}

		void WhileLoop::OnExecute(double dt)
		{
			if(GetAttribute<bool>(0))
				CallOutput(0);
			else
			{
				DeactivateFlowControl();
				CallOutput(1);
			}
		}

		const EventDeclaration& WhileLoop::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"condition", AttributeType::BOOL, false },
			};

			const AttributeVector vReturns =
			{
			};

			const OutputVector vOutputs =
			{
				L"Body",
				L"Finished"
			};

			static const EventDeclaration decl(L"WhileLoop", vAttributes, vReturns, vOutputs);

			return decl;
		}

		/*****************************************
		* ForEachIntLoop
		******************************************/

		void ForEachIntLoop::Reset(void)
		{
			m_size = GetAttributeArray<int>(0).size();

			m_currentIndex = 0;

			ActivateFlowControl();
		}

		void ForEachIntLoop::OnExecute(double dt)
		{
			if(m_currentIndex < m_size)
			{
				ReturnValue<int>(0, GetAttributeArray<int>(0).at(m_currentIndex));
				ReturnValue<int>(1, m_currentIndex);
				CallOutput(0);
				m_currentIndex++;
			}
			else
			{
				DeactivateFlowControl();
				CallOutput(1);
			}
		}

		const EventDeclaration& ForEachIntLoop::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"items", AttributeType::INT, true },
			};

			const AttributeVector vReturns =
			{
				{ L"Item", AttributeType::INT, false },
				{ L"Index", AttributeType::INT, false },
			};

			const OutputVector vOutputs =
			{
				L"Body",
				L"Finished"
			};

			static const EventDeclaration decl(L"ForEachIntLoop", vAttributes, vReturns, vOutputs);

			return decl;
		}

		/*****************************************
		* ActivateScene
		******************************************/

		void ActivateScene::Reset(void)
		{
		}

		void ActivateScene::OnExecute(double dt)
		{
			detail::_pScenes->ActivateScene(GetAttribute<std::wstring>(0));
			CallOutput(0);
		}

		const EventDeclaration& ActivateScene::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"scene", core::AttributeType::STRING, false }
			};

			static const EventDeclaration decl(L"ActivateScene", vAttributes, AttributeVector());

			return decl;
		}

		/*****************************************
		* ----------------------------------------
		* FUNCTIONS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* BoolOr
		******************************************/

		void BoolOr::OnCalculate(void)
		{
			const bool result = GetAttribute<bool>(0) || GetAttribute<bool>(1);

			ReturnValue<bool>(0, result);
		}

		const FunctionDeclaration& BoolOr::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::BOOL, false },
				{ L"", AttributeType::BOOL, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::BOOL, false }
			};

			static const FunctionDeclaration decl(L"BoolOr", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* BoolToInt
		******************************************/

		void BoolToInt::OnCalculate(void)
		{
			const bool result = GetAttribute<bool>(0);

			ReturnValue<int>(0, result != 0);
		}

		const FunctionDeclaration& BoolToInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::BOOL, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"BoolToInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* AddFloat
		******************************************/

		void AddFloat::OnCalculate(void)
		{
			const float sum = GetAttribute<float>(0) + GetAttribute<float>(1);

			ReturnValue<float>(0, sum);
		}

		const FunctionDeclaration& AddFloat::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::FLOAT, false },
				{ L"", AttributeType::FLOAT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::FLOAT, false }
			};

			static const FunctionDeclaration decl(L"AddFloat", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* SubFloat
		******************************************/

		void SubFloat::OnCalculate(void)
		{
			const float sum = GetAttribute<float>(0) - GetAttribute<float>(1);

			ReturnValue<float>(0, sum);
		}

		const FunctionDeclaration& SubFloat::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::FLOAT, false },
				{ L"", AttributeType::FLOAT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::FLOAT, false }
			};

			static const FunctionDeclaration decl(L"SubFloat", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* MulFloat
		******************************************/

		void MulFloat::OnCalculate(void)
		{
			const float product = GetAttribute<float>(0) * GetAttribute<float>(1);

			ReturnValue<float>(0, product);
		}

		const FunctionDeclaration& MulFloat::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::FLOAT, false },
				{ L"", AttributeType::FLOAT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::FLOAT, false }
			};

			static const FunctionDeclaration decl(L"MulFloat", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* GreaterEqualFloat
		******************************************/

		void GreaterEqualFloat::OnCalculate(void)
		{
			const bool result = GetAttribute<float>(0) >= GetAttribute<float>(1);

			ReturnValue<bool>(0, result);
		}

		const FunctionDeclaration& GreaterEqualFloat::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::FLOAT, false },
				{ L"", AttributeType::FLOAT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::BOOL, false }
			};

			static const FunctionDeclaration decl(L"GreaterEqualFloat", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* FloatToInt
		******************************************/

		void FloatToInt::OnCalculate(void)
		{
			ReturnValue<int>(0, (int)GetAttribute<float>(0));
		}

		const FunctionDeclaration& FloatToInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::FLOAT, false },
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"FloatToInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* AddInt
		******************************************/

		void AddInt::OnCalculate(void)
		{
			const int product = GetAttribute<int>(0) + GetAttribute<int>(1);

			ReturnValue<int>(0, product);
		}

		const FunctionDeclaration& AddInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"AddInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* SubInt
		******************************************/

		void SubInt::OnCalculate(void)
		{
			const int product = GetAttribute<int>(0) - GetAttribute<int>(1);

			ReturnValue<int>(0, product);
		}

		const FunctionDeclaration& SubInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"SubInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* MulInt
		******************************************/

		void MulInt::OnCalculate(void)
		{
			const int product = GetAttribute<int>(0) * GetAttribute<int>(1);

			ReturnValue<int>(0, product);
		}

		const FunctionDeclaration& MulInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"MulInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* DivInt
		******************************************/

		void DivInt::OnCalculate(void)
		{
			const int product = GetAttribute<int>(0) / GetAttribute<int>(1);

			ReturnValue<int>(0, product);
		}

		const FunctionDeclaration& DivInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"DivInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* ModInt
		******************************************/

		void ModInt::OnCalculate(void)
		{
			const auto mod = GetAttribute<int>(1);
			if(mod != 0)
			{
				const int product = GetAttribute<int>(0) % mod;

				ReturnValue<int>(0, product);
			}
		}

		const FunctionDeclaration& ModInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"ModInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* EqualsInt
		******************************************/

		void EqualsInt::OnCalculate(void)
		{
			const bool result = GetAttribute<int>(0) == GetAttribute<int>(1);

			ReturnValue<bool>(0, result);
		}

		const FunctionDeclaration& EqualsInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::BOOL, false }
			};

			static const FunctionDeclaration decl(L"EqualsInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* LessEqualInt
		******************************************/

		void LessEqualInt::OnCalculate(void)
		{
			const bool result = GetAttribute<int>(0) <= GetAttribute<int>(1);

			ReturnValue<bool>(0, result);
		}

		const FunctionDeclaration& LessEqualInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::BOOL, false }
			};

			static const FunctionDeclaration decl(L"LessEqualInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* GreaterEqualInt
		******************************************/

		void GreaterEqualInt::OnCalculate(void)
		{
			const bool result = GetAttribute<int>(0) >= GetAttribute<int>(1);

			ReturnValue<bool>(0, result);
		}

		const FunctionDeclaration& GreaterEqualInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
				{ L"", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::BOOL, false }
			};

			static const FunctionDeclaration decl(L"GreaterEqualInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* SelectInt
		******************************************/

		void SelectInt::OnCalculate(void)
		{
			if(GetAttribute<bool>(2))
				ReturnValue<int>(0, GetAttribute<int>(0));
			else
				ReturnValue<int>(0, GetAttribute<int>(1));
		}

		const FunctionDeclaration& SelectInt::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"true", AttributeType::INT, false },
				{ L"false", AttributeType::INT, false },
				{ L"condition", AttributeType::BOOL, false }
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::INT, false }
			};

			static const FunctionDeclaration decl(L"SelectInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* IntToString
		******************************************/

		void IntToString::OnCalculate(void)
		{
			ReturnValue<std::wstring>(0, conv::ToString(GetAttribute<int>(0)));
		}

		const FunctionDeclaration& IntToString::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"", AttributeType::INT, false },
			};

			const AttributeVector vReturns =
			{
				{ L"", AttributeType::STRING, false }
			};

			static const FunctionDeclaration decl(L"IntToString", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* GetStringArrayElement
		******************************************/

		void GetStringArrayElement::OnCalculate(void)
		{
			const auto& vValues = GetAttributeArray<std::wstring>(0);
			const unsigned int index = GetAttribute<int>(1);

			if(vValues.size() > index)
				ReturnValue<std::wstring>(0, vValues[index]);
		}

		const FunctionDeclaration& GetStringArrayElement::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"array", AttributeType::STRING, true },
				{ L"index", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"value", AttributeType::STRING, false }
			};

			static const FunctionDeclaration decl(L"GetStringArrayElement", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* GetStringSubArray
		******************************************/

		void GetStringSubArray::OnCalculate(void)
		{
			const auto& vValues = GetAttributeArray<std::wstring>(0);
			const unsigned int first = GetAttribute<int>(1);
			const unsigned int last = GetAttribute<int>(2);

			const auto size = vValues.size();
			if(size > first && size > last)
			{
				std::vector<std::wstring> vNewValues;
				for(unsigned int i = first; i <= last; i++)
				{
					vNewValues.push_back(vValues[i]);
				}

				ReturnValueArray<std::wstring>(0, vNewValues);
			}
		}

		const FunctionDeclaration& GetStringSubArray::GenerateDeclaration(void)
		{
			const AttributeVector vAttributes =
			{
				{ L"array", AttributeType::STRING, true },
				{ L"first", AttributeType::INT, false },
				{ L"last", AttributeType::INT, false }
			};

			const AttributeVector vReturns =
			{
				{ L"new array", AttributeType::STRING, true }
			};

			static const FunctionDeclaration decl(L"GetStringSubArray", vAttributes, vReturns);

			return decl;
		}

	}
}