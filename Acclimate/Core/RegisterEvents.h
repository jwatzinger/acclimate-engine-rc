#pragma once
#include "Event.h"
#include "EventFunction.h"

namespace acl
{
	namespace core
	{

		struct Context;

		void registerEvents(const core::Context& context);

		/*****************************************
		* ----------------------------------------
		* EVENTS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* Condition
		******************************************/

		class Condition :
			public BaseEvent<Condition>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* Block
		******************************************/

		class Block :
			public BaseEvent<Block>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* Wait
		******************************************/

		class Wait :
			public BaseEvent<Wait>
		{
		public:

			Wait(void);
			Wait(const Wait& wait);

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			float m_nowTime;
		};

		/*****************************************
		* Parallel
		******************************************/

		class Parallel :
			public BaseEvent<Parallel>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* ForLoop
		******************************************/

		class ForLoop :
			public BaseEvent<ForLoop>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			int m_firstIndex, m_lastIndex, m_currentIndex;
		};

		/*****************************************
		* WhileLoop
		******************************************/

		class WhileLoop :
			public BaseEvent<WhileLoop>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* ForEachIntLoop
		******************************************/

		class ForEachIntLoop :
			public BaseEvent<ForEachIntLoop>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);

		private:

			unsigned int m_currentIndex;
			unsigned int m_size;
		};

		/*****************************************
		* ActivateScene
		******************************************/

		class ActivateScene :
			public BaseEvent<ActivateScene>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* ----------------------------------------
		* FUNCTIONS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* BoolOr
		******************************************/

		class BoolOr :
			public BaseEventFunction<BoolOr>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* BoolToInt
		******************************************/

		class BoolToInt :
			public BaseEventFunction<BoolToInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* AddFloat
		******************************************/

		class AddFloat :
			public BaseEventFunction<AddFloat>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* SubFloat
		******************************************/

		class SubFloat :
			public BaseEventFunction<SubFloat>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* MulFloat
		******************************************/

		class MulFloat :
			public BaseEventFunction<MulFloat>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* GreaterEqualFloat
		******************************************/

		class GreaterEqualFloat :
			public BaseEventFunction<GreaterEqualFloat>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* FloatToInt
		******************************************/

		class FloatToInt :
			public BaseEventFunction<FloatToInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* AddInt
		******************************************/

		class AddInt :
			public BaseEventFunction<AddInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* SubInt
		******************************************/

		class SubInt :
			public BaseEventFunction<SubInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* MulInt
		******************************************/

		class MulInt :
			public BaseEventFunction<MulInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* DivInt
		******************************************/

		class DivInt :
			public BaseEventFunction<DivInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* ModInt
		******************************************/

		class ModInt :
			public BaseEventFunction<ModInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* EqualsInt
		******************************************/

		class EqualsInt :
			public BaseEventFunction<EqualsInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* LessEqualInt
		******************************************/

		class LessEqualInt :
			public BaseEventFunction<LessEqualInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* GreaterEqualInt
		******************************************/

		class GreaterEqualInt :
			public BaseEventFunction<GreaterEqualInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* SelectInt
		******************************************/

		class SelectInt :
			public BaseEventFunction<SelectInt>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* IntToString
		******************************************/

		class IntToString :
			public BaseEventFunction<IntToString>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* GetStringArrayElement
		******************************************/

		class GetStringArrayElement :
			public BaseEventFunction<GetStringArrayElement>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* GetStringSubArray
		******************************************/

		class GetStringSubArray :
			public BaseEventFunction<GetStringSubArray>
		{
		public:

			void OnCalculate(void) override;

			static const FunctionDeclaration& GenerateDeclaration(void);
		};

	}
}