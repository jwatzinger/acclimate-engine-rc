#include "RegisterScript.h"
#include "SettingModule.h"
#include "SceneManager.h"
#include "Setting.h"
#include "ConfigSaver.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace core
	{
		namespace
		{
			SettingModule* _pSettings;
			SceneManager* _pScenes;
		}

		/*************************************
		* SETTINGS
		**************************************/

		Setting* getSetting(const std::wstring& stName)
		{
			return _pSettings->GetSetting(stName);
		}

		void refreshSettings(void)
		{
			core::ConfigSaver saver(*_pSettings);
			saver.Save(L"../bin/Config.axm"); // TODO: un-hardcode, make this work for other paths like release and debug too
			_pSettings->Refresh();
		}

		/*************************************
		* SCENE
		**************************************/

		void activateScene(const std::wstring& stName)
		{
			_pScenes->ActivateScene(stName);
		}

		void RegisterScript(script::Core& script, SettingModule& settings, SceneManager& scenes)
		{
			_pSettings = &settings;
			_pScenes = &scenes;

			/*************************************
			* SETTINGS
			**************************************/

			// setting class
			auto setting = script.RegisterReferenceType("Setting", script::REF_NOCOUNT);
			setting.RegisterMethod("void SetInt(const int& in)", asMETHODPR(Setting, Set<int>, (const int& data), void));
			setting.RegisterMethod("void SetFloat(const float& in)", asMETHODPR(Setting, Set<float>, (const float& data), void)); 
			setting.RegisterMethod("void SetBool(const bool& in)", asMETHODPR(Setting, Set<bool>, (const bool& data), void));
			setting.RegisterMethod("void SetString(const string& in)", asMETHODPR(Setting, Set<std::wstring>, (const std::wstring& data), void));
			setting.RegisterMethod("const int& GetInt() const", asMETHODPR(Setting, GetData<int>, () const, const int&));
			setting.RegisterMethod("const float& GetFloat() const", asMETHODPR(Setting, GetData<float>, () const, const float&));
			setting.RegisterMethod("const bool& GetBool() const", asMETHODPR(Setting, GetData<bool>, () const, const bool&));

			script.RegisterGlobalFunction("Setting@ getSetting(const string& in)", asFUNCTION(getSetting));
			script.RegisterGlobalFunction("void refreshSettings()", asFUNCTION(refreshSettings));

			/*************************************
			* SCENE
			**************************************/

			script.RegisterGlobalFunction("void activateScene(const string& in)", asFUNCTION(activateScene));

			auto scriptScene = script.RegisterInterface("IScene");
			scriptScene.RegisterMethod("void OnUninit()");
			scriptScene.RegisterMethod("void OnUpdate(double dt)");
		}

	}
}
