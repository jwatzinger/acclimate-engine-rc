#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace core
	{

		class SettingModule;
		class SceneManager;

		void RegisterScript(script::Core& core, SettingModule& settings, SceneManager& scenes);

	}
}