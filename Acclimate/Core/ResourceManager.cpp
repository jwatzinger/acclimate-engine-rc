#include "ResourceManager.h"
namespace acl
{
	namespace core
	{

		ResourceManager::~ResourceManager(void)
		{
			for(auto& set : m_mSets)
			{
				delete set.second;
			}
		}

		void ResourceManager::RemoveSet(const std::wstring& stName)
		{
			if(m_mSets.count(stName))
			{
				delete m_mSets[stName];
				m_mSets.erase(stName);
			}
		}

		void ResourceManager::Clear(void)
		{
			for(auto& set : m_mSets)
			{
				delete set.second;
			}
			m_mSets.clear();
		}

		void ResourceManager::LoadSetResources(const std::wstring& stSet, const std::wstring& stGroup, const std::wstring& stFile)
		{
			if(m_mSets.count(stSet))
			{
				auto pSet = m_mSets.at(stSet);

				auto* pBlock = m_mBlocks[stGroup][stSet];
				if(!pBlock)
				{
					auto& block = pSet->CreateBlock();
					m_mBlocks[stGroup][stSet] = &block;
					pBlock = &block;
				}

				pBlock->Begin();
				pSet->Load(stFile);
				pBlock->End();
			}
		}

		void ResourceManager::UnloadSetResources(const std::wstring& stGroup)
		{
			if(m_mBlocks.count(stGroup))
			{
				for(auto& blocks : m_mBlocks[stGroup])
				{
					blocks.second->Clear();
					delete blocks.second;
				}

				m_mBlocks.erase(stGroup);
			}
		}

	}
}
