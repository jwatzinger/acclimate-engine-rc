#pragma once
#include "ResourceSet.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		class ResourceManager
		{
			typedef std::map<std::wstring, IResourceSet*> SetMap;
			typedef std::map<std::wstring, std::map<std::wstring, IResourceBlock*>> BlockMap;
		public:

			~ResourceManager(void);

			template<typename Key, typename Type>
			ResourceSet<Key, Type>& AddSet(const std::wstring& stName, const std::wstring& stFile, ResourceLoader<Key, Type>& loader)
			{
				if(m_mSets.count(stName))
				{
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Resource set with name ", stName, "already exists.");
					delete m_mSets[stName];
				}

				auto pSet = new ResourceSet<Key, Type>(stName, stFile, loader);
				m_mSets[stName] = pSet;
				return *pSet;
			}

			void RemoveSet(const std::wstring& stName);
			void Clear(void);

			void LoadSetResources(const std::wstring& stSet, const std::wstring& stGroup, const std::wstring& stFile);
			void UnloadSetResources(const std::wstring& stGroup);

		private:

			SetMap m_mSets;
			BlockMap m_mBlocks;
		};

	}
}


