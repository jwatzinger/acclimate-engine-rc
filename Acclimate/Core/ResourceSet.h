#pragma once
#include "Resources.h"

namespace acl
{
	namespace core
	{

		/************************************************
		* Block
		************************************************/

		class IResourceBlock
		{
		public:

			virtual ~IResourceBlock(void) = 0 {}

			virtual void Begin(void) = 0;
			virtual void End(void) = 0;
			virtual void Clear(void) = 0;
		};

		class IResourceSet
		{
		public:

			virtual ~IResourceSet(void) = 0 {}

			virtual const std::wstring& GetName(void) const = 0;
			virtual const std::wstring& GetFile(void) const = 0;

			virtual IResourceBlock& CreateBlock(void) = 0;

			virtual void Load(const std::wstring& stFile) const = 0;
		};

		/************************************************
		* Loader
		************************************************/

		template<typename Key, typename Type>
		class ResourceLoader
		{
		public:

			ResourceLoader(void) : m_pResources(nullptr)
			{
			}

			virtual ~ResourceLoader(void) = 0 {}

			void SetResources(Resources<Key, Type>& resources)
			{
				m_pResources = &resources;
			}

			virtual void Load(const std::wstring& stName) const = 0;

		protected:

			void AddResource(const Key& key, Type& resource) const
			{
				m_pResources->Add(key, resource);
			}

		private:

			Resources<Key, Type>* m_pResources;
		};

		/************************************************
		* Set
		************************************************/

		template<typename Key, typename Type>
		class ResourceSet :
			public IResourceSet
		{
			class ResourceBlock :
				public IResourceBlock
			{
			public:

				ResourceBlock(Resources& resources):
					m_block(resources)
				{
				}

				void Begin(void) override
				{
					m_block.Begin();
				}

				void End(void) override
				{
					m_block.End();
				}

				void Clear(void) override
				{
					m_block.Clear();
				}

			private:

				core::ResourceBlock<Key, Type> m_block;
			};

		public:

			typedef Resources<Key, Type> Resources;

			ResourceSet(const std::wstring& stName, const std::wstring& stFile, ResourceLoader<Key, Type>& loader) :
				m_stName(stName), m_stFile(stFile), m_pLoader(&loader)
			{
				loader.SetResources(m_resources);
			}

			~ResourceSet(void)
			{
				delete m_pLoader;
			}

			const std::wstring& GetName(void) const override final
			{
				return m_stName;
			}

			const std::wstring& GetFile(void) const override final
			{
				return m_stFile;
			}
			
			IResourceBlock& CreateBlock(void) override final
			{
				return *new ResourceBlock(m_resources);
			}

			void Load(const std::wstring& stName) const override final
			{
				m_pLoader->Load(stName);
			}

			Resources& GetResources(void)
			{
				return m_resources;
			}

			const Resources& GetResources(void) const
			{
				return m_resources;
			}

		private:

			std::wstring m_stName, m_stFile;
			Resources m_resources;
			ResourceLoader<Key, Type>* m_pLoader;
		};
	}
}