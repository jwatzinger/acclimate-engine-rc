#pragma once
#include <map>
#include <vector>

namespace acl
{
    namespace core
    {

		template<typename Key, typename Resource>
		class Resources;

		template<typename Key, typename Resource>
		class ResourceBlock
		{
			typedef Resources<Key, Resource> ResourceContainer;
			typedef std::vector<Key> KeyVector;
		public:
			
			ResourceBlock(ResourceContainer& resources): m_pResources(&resources)
			{
			}

			ResourceBlock(ResourceContainer& resources, const std::wstring& stPath) : m_pResources(&resources), m_stPath(stPath)
			{
			}

			void SetResources(ResourceContainer* pResources)
			{
				if(m_pResources != pResources)
				{
					m_pResources = pResources;
					m_vKeys.clear();
				}
			}

			Resource* operator[](size_t id) const
			{
				return m_pResources->Get(m_vKeys.at(id));
			}

			const Key& GetKey(size_t id) const
			{
				return m_vKeys.at(id);
			}

			std::pair<Key, Resource*> GetPair(size_t id) const
			{
				const Key& key = m_vKeys.at(id);
				return std::make_pair(key, m_pResources->Get(key));
			}

			Resource* Get(const Key& key) const
			{
				if(!Has(key))
					return nullptr;
				else
					return m_pResources->Get(key);
			}

			void SetPath(const std::wstring& stPath)
			{
				m_stPath = stPath;
			}

			bool HasPath(void) const
			{
				return !m_stPath.empty();
			}

			const std::wstring& GetPath(void) const
			{
				return m_stPath;
			}

			bool Has(const Key& key) const
			{
				return std::find(m_vKeys.begin(), m_vKeys.end(), key) != m_vKeys.end();
			}

			size_t Size(void) const
			{
				return m_vKeys.size();
			}

			void Begin(void)
			{
				if(m_pResources)
					m_pResources->RegisterBlock(*this);
			}

			void Add(const Key& key)
			{
				auto itr = std::find(m_vKeys.begin(), m_vKeys.end(), key);
				if(itr == m_vKeys.end())
					m_vKeys.push_back(key);
			}

			void Remove(const Key& key)
			{
				auto itr = std::find(m_vKeys.begin(), m_vKeys.end(), key);
				if (itr != m_vKeys.end())
					m_vKeys.erase(itr);
			}

			void Delete(size_t id)
			{
				if(m_pResources && id < m_vKeys.size())
				{
					auto& key = m_vKeys[id];
					m_pResources->Delete(key);
					m_vKeys.erase(m_vKeys.begin() + id);
				}
			}

			void End(void)
			{
				if(m_pResources)
					m_pResources->RemoveBlock(*this);
			}

			void Clear(void)
			{
				if(m_pResources)
				{
					for(auto& key: m_vKeys)
					{
						m_pResources->Delete(key);
					}
				}

				m_vKeys.clear();
			}

			ResourceContainer& Resources(void)
			{
				return *m_pResources;
			}

			const ResourceContainer& Resources(void) const
			{
				return *m_pResources;
			}

		private:

			KeyVector m_vKeys;
			std::wstring m_stPath;

			ResourceContainer* m_pResources;
		};

        /// Generic resource cache
		/** The Resources class represents a generic resource cache.
        *   It manages the lifetime of resources, and grants restricted access
        *   to them, as well as methods for checking for keys and resources.
        *   It also offers the possibility to selectively clear certain
        *   resources. */
        template<typename Key, typename Resource>
        class Resources
        {
		public:
			typedef ResourceBlock<Key, Resource> Block;
		private:
			typedef std::vector<Block*> BlockVector;
        public:

            typedef std::map<Key, Resource*> ResourceMap; ///< Typedef for resource map

            ~Resources(void)
            {
                for(const auto resource : m_mResources)
                {
                    delete resource.second;
                }

				for(auto pBlock : m_vBlocks)
				{
					pBlock->SetResources(nullptr);
				}
            }

            /** Adds a resource with a certain key.
             *  This method caches a resource to a certain key. Only one resource can be 
             *  associated per key. Currently, any later added resource will overwrite 
             *  the earlier added ones with the same key.
             *
             *  @param[in] key Key for the resource
             *  @param[in] resource Reference to the resource
             */
            void Add(const Key& key, Resource& resource)
            {
                m_mResources[key] = &resource;

				for(auto block : m_vBlocks)
				{
					block->Add(key);
				}
            }

            /** Accesses a resource with a certain key.
             *  This operator redirects the input to the Get() method.
             *
             *  @param[in] key Key for the resource to aquire.
             */
            Resource* operator[](const Key& key) const
            {
                return Get(key);
            }

            /** Accesses a resource with a certain key.
             *  This method returns a pointer to a resource with a certain key. If the
             *  resource is not contained, it returns \c nullptr.
             *
             *  @param[in] key Key for the resource to aquire.
             *
             *  @return Pointer to the resource.
             */
            Resource* Get(const Key& key) const
            {
                if(Has(key))
                    return m_mResources.at(key);
                else 
                    return nullptr;
            }

            /** Check for a certain resource.
             *  This method checks if a resource to a certain key exists.
             *
             *  @param[in] key Key for the resource to check.
             *
             *  @return Indicates whether the resource was found.
             */
            bool Has(const Key& key) const
            {
                return m_mResources.count(key) == 1;
            }

			/** Deletes a resource.
             *  This method removes a resource with a given key from cache, if it exists,
			 *	and deletes it.
             *
             *  @param[in] key Key for the resource to delete
             */
            void Delete(const Key& key)
            {
                auto resourceItr = m_mResources.find(key);

				if(resourceItr != m_mResources.end())
				{
					delete m_mResources[key];
					m_mResources.erase(resourceItr);

					for(auto block : m_vBlocks)
					{
						block->Remove(key);
					}
				}
            }

			/** Removes a resource.
			*  This method removes a resourcefrom cache, if it exists,
			*  and deletes it.
			*
			*  @param[in] resource The resource to delete
			*/
			void Remove(const Resource& resource)
			{
				for(auto& itr : m_mResources)
				{
					if(itr.second == &resource)
					{
						delete itr.second;
						m_mResources.erase(itr.first);

						for(auto block : m_vBlocks)
						{
							block->Remove(itr.first);
						}

						return;
					}
				}
			}

            /** Size of the cache.
             *  This method returns the size of the resource cache.
             *
             *  @return The size of the cache.
             */
            size_t Size(void) const
            {
                return m_mResources.size();
            }

            /** Accesses the internal cache structure.
             *  This method returns a const reference to the internal map structure 
             *  the cache is build upon. Allows for direct iteration over the cache.
             *
             *  @return Const reference to the cache map structure.
             */
            const ResourceMap& Map(void) const
            {
                return m_mResources;
            }

            /** Returns the key of a given resource.
             *  This method returns a pointer to the key to a certain resource. If
             *  the resource is not found in this cache, a \c nullptr is returned.
             *
             *  @return Const pointer to the key related to the resource.
             */
            const Key* const Key(const Resource* pResource) const
            {
                for(const auto resource : m_mResources)
                {
                    if(resource.second == pResource)
                        return &resource.first;
                }

                return nullptr;
            }

            /** Clears the cache.
             *  This method clears the entire cache and deletes all stored resources. */
			void Clear(void)
			{
				for(const auto resource : m_mResources)
				{
					delete resource.second;
				}

				m_mResources.clear();
			}

			void RegisterBlock(Block& block) 
			{
				m_vBlocks.push_back(&block);
			}

			void RemoveBlock(Block& block) 
			{
				auto itr = std::find(m_vBlocks.begin(), m_vBlocks.end(), &block);

				if(itr != m_vBlocks.end())
				{
					m_vBlocks.erase(itr);
				}
			}

        private:

#pragma warning( disable: 4251 )
            ResourceMap m_mResources; ///< Map of the resources.

			BlockVector m_vBlocks;
#pragma warning( default: 4251 )
        };

    }
}

