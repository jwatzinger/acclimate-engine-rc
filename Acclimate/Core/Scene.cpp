#include "Scene.h"

namespace acl
{
	namespace core
	{

		Scene::Scene(const std::wstring& stName, const std::wstring& stPath, const ModuleMap& mModules, const ExtentionVector& vExtentions, const std::string& stScriptClass, const CustomMap& customs, const InputVector& vInput) :
			m_stName(stName), m_stPath(stPath), m_mModules(mModules), m_stScriptClass(stScriptClass), m_mCustoms(customs), m_vInput(vInput), m_vExtentions(vExtentions)
		{
		}

		const std::wstring& Scene::GetName(void) const
		{
			return m_stName;
		}

		const std::wstring& Scene::GetPath(void) const
		{
			return m_stPath;
		}

		const std::wstring& Scene::GetModule(ModuleType type) const
		{
			auto itr = m_mModules.find(type);
			if(itr != m_mModules.end())
				return itr->second;
			else
			{
				static const std::wstring stEmpty = L"";
				return stEmpty;
			}
		}

		const Scene::ModuleMap& Scene::GetModules(void) const
		{
			return m_mModules;
		}

		const Scene::ExtentionVector& Scene::GetExtentions(void) const
		{
			return m_vExtentions;
		}

		const std::string& Scene::GetScriptClass(void) const
		{
			return m_stScriptClass;
		}

		const Scene::InputVector& Scene::GetInput(void) const
		{
			return m_vInput;
		}

		const Scene::CustomMap& Scene::GetCustoms(void) const
		{
			return m_mCustoms;
		}

		bool Scene::HasModule(ModuleType type) const
		{
			return m_mModules.count(type) != 0;
		}

	}
}

