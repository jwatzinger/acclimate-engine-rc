#pragma once
#include <string>
#include <map>
#include <vector>
#include "..\Core\Dll.h"

namespace acl
{
	namespace core
	{

		class ACCLIMATE_API Scene
		{
		public:

			enum class ModuleType
			{
				RESOURCES, SCRIPTS, ENTITIES, PHYSICS, EVENTS, UNKNOWN
			};

			typedef std::vector<std::wstring> InputVector;
			typedef std::map<ModuleType, std::wstring> ModuleMap;
			typedef std::map<std::wstring, std::wstring> CustomMap;
			typedef std::vector<std::wstring> ExtentionVector;

			Scene(const std::wstring& stName, const std::wstring& stPath, const ModuleMap& mModules, const ExtentionVector& vExtentions, const std::string& stScriptClass, const CustomMap& mCustoms, const InputVector& vInput);

			const std::wstring& GetName(void) const;
			const std::wstring& GetPath(void) const;
			const std::wstring& GetModule(ModuleType type) const;
			const ModuleMap& GetModules(void) const;
			const ExtentionVector& GetExtentions(void) const;
			const InputVector& GetInput(void) const;
			const std::string& GetScriptClass(void) const;
			bool HasModule(ModuleType type) const;

			const CustomMap& GetCustoms(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stName, m_stPath;
			std::string m_stScriptClass;
			ModuleMap m_mModules;
			ExtentionVector m_vExtentions;
			CustomMap m_mCustoms;
			InputVector m_vInput;
#pragma warning( default: 4251 )
		};

	}
}


