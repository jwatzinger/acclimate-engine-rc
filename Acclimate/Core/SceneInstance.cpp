#include "SceneInstance.h"
#include "..\Input\Module.h"
#include "..\Physics\Context.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace core
	{

		const gfx::PathData paths(L"Resources/", L"Textures/", L"Effects/", L"Meshes/");

		SceneInstance::SceneInstance(const std::wstring& stName, const std::wstring& stPath, const gfx::ResourceContext& resources, const physics::Context& physics, script::ConfigGroup&& group, script::Scripts& scripts, input::Module& input, const HandlerVector& vHandler, Events& events) :
			m_stName(stName), m_resources(resources, paths), m_materials(physics.materials), m_shapes(physics.shapes), m_scriptGroup(group), m_pInstance(nullptr), m_vHandler(vHandler), m_pInput(&input), m_scripts(scripts), m_events(events)
		{
			m_resources.SetPath(stPath + L"/Resources/");
		}

		SceneInstance::~SceneInstance()
		{
			m_resources.Clear();
			m_materials.Clear();
			m_shapes.Clear();
			m_scripts.Clear();
			m_events.Clear();
			m_scriptGroup.Remove();

			for(auto pHandler : m_vHandler)
			{
				m_pInput->RemoveHandler(*pHandler);
			}

			if(m_pInstance)
			{
				m_pInstance->CallMethod<void>("void OnUninit()");
				delete m_pInstance;
			}
		}

		void SceneInstance::SetScriptInstance(script::Instance& instance)
		{
			m_pInstance = &instance;
		}

		const std::wstring& SceneInstance::GetName(void) const
		{
			return m_stName;
		}

		gfx::ResourceBlock& SceneInstance::GetResources(void)
		{
			return m_resources;
		}

		const gfx::ResourceBlock& SceneInstance::GetResources(void) const
		{
			return m_resources;
		}

		const script::Scripts::Block& SceneInstance::GetScripts(void) const
		{
			return m_scripts;
		}

		void SceneInstance::BeginLoad(void)
		{
			m_resources.Begin();
			m_materials.Begin();
			m_shapes.Begin();
			m_scriptGroup.Begin();
			m_events.Begin();
			m_scripts.Begin();
		}

		void SceneInstance::EndLoad(void)
		{
			m_resources.End();
			m_materials.End();
			m_shapes.End();
			m_scriptGroup.End();
			m_events.End();
			m_scripts.End();
		}

		void SceneInstance::Update(double dt)
		{
			if(m_pInstance)
				m_pInstance->CallMethod<void>("void OnUpdate(double dt)");
		}

	}
}

