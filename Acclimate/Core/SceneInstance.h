#pragma once
#include "Events.h"
#include "..\Gfx\ResourceBlock.h"
#include "..\Physics\Materials.h"
#include "..\Physics\Shapes.h"
#include "..\Script\ConfigGroup.h"
#include "..\Script\Resources.h"

namespace acl
{
	namespace gfx
	{
		struct ResourceContext;
	}

	namespace input
	{
		class Handler;
		class Module;
	}
	
	namespace physics
	{
		struct Context;
	}

	namespace script
	{
		class Instance;
	}

	namespace core
	{

		class Scene;

		class ACCLIMATE_API SceneInstance
		{
		public:
			typedef std::vector<input::Handler*> HandlerVector;

			SceneInstance(const std::wstring& stName, const std::wstring& stPath, const gfx::ResourceContext& resources, const physics::Context& physics, script::ConfigGroup&& group, script::Scripts& scripts, input::Module& input, const HandlerVector& vHandler, Events& events);
			~SceneInstance(void);

			void SetScriptInstance(script::Instance& instance);

			const std::wstring& GetName(void) const;
			gfx::ResourceBlock& GetResources(void);
			const gfx::ResourceBlock& GetResources(void) const;
			const script::Scripts::Block& GetScripts(void) const;

			void BeginLoad(void);
			void EndLoad(void);

			void Update(double dt);

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stName;

			gfx::ResourceBlock m_resources;
			physics::Materials::Block m_materials;
			physics::Shapes::Block m_shapes;
			script::ConfigGroup m_scriptGroup;
			script::Scripts::Block m_scripts;
			input::Module* m_pInput;
			Events::Block m_events;
			HandlerVector m_vHandler;

			script::Instance* m_pInstance;
#pragma warning( default: 4251 )
		};

	}
}

