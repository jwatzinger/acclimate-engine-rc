#include "SceneLoader.h"
#include "SceneManager.h"
#include "..\File\File.h"
#include "..\System\WorkingDirectory.h"
#include "..\System\Convert.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		SceneLoader::SceneLoader(SceneManager& scenes) : m_pScenes(&scenes)
		{
		}

		void SceneLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			if(auto pRoot = doc.Root(L"Scenes"))
			{
				sys::WorkingDirectory dir(stFilename, true);
				sys::WorkingDirectory dir2;
				if(auto pPath = pRoot->Attribute(L"path"))
					dir2 = sys::WorkingDirectory(pPath->GetValue());

				if(auto pScenes = pRoot->Nodes(L"Scene"))
				{
					for(auto pScene : *pScenes)
					{
						LoadScene(pScene->GetValue());
					}
				}

				dir2.Restore();
				dir.Restore();
			}
		}

		void SceneLoader::LoadScene(const std::wstring& stName) const
		{
			sys::WorkingDirectory dir(stName);

			xml::Doc doc;
			doc.LoadFile(L"Scene.axm");

			if(auto pRoot = doc.Root(L"Scene"))
			{
				Scene::ModuleMap mModules;

				if(auto pResources = pRoot->FirstNode(L"Resources"))
					mModules.emplace(Scene::ModuleType::RESOURCES, pResources->Attribute(L"file")->GetValue());

				if(auto pScripts = pRoot->FirstNode(L"Scripts"))
					mModules.emplace(Scene::ModuleType::SCRIPTS, pScripts->Attribute(L"file")->GetValue());

				if(auto pEntities = pRoot->FirstNode(L"Entities"))
					mModules.emplace(Scene::ModuleType::ENTITIES, pEntities->Attribute(L"file")->GetValue());

				if(auto pPhysics = pRoot->FirstNode(L"Physics"))
					mModules.emplace(Scene::ModuleType::PHYSICS, pPhysics->Attribute(L"file")->GetValue());

				if(auto pEvents = pRoot->FirstNode(L"Events"))
					mModules.emplace(Scene::ModuleType::EVENTS, pEvents->Attribute(L"file")->GetValue());

				Scene::InputVector vInput;
				if(auto pInput = pRoot->FirstNode(L"Input"))
				{
					sys::WorkingDirectory dir2;
					if(auto pPath = pInput->Attribute(L"path"))
						dir2 = sys::WorkingDirectory(pPath->GetValue());

					if(auto pHandler = pInput->Nodes(L"Handler"))
					{
						for(auto pHandlerNode : *pHandler)
						{
							vInput.push_back(file::FullPath(*pHandlerNode->Attribute(L"file")));
						}
					}
				}

				// load customs
				Scene::CustomMap mCustoms;
				if(auto pCustoms = pRoot->FirstNode(L"Customs"))
				{
					if(auto pResources = pCustoms->Nodes(L"Resources"))
					{
						for(auto pResource : *pResources)
						{
							mCustoms.emplace(pResource->GetValue(), pResource->Attribute(L"file")->GetValue());
						}
					}
				}

				// load extentions
				Scene::ExtentionVector vExtentions;
				if(auto pExtentions = pRoot->FirstNode(L"Extentions"))
				{
					if(auto pExtention = pExtentions->Nodes(L"Extention"))
					{
						for(auto pExt : *pExtention)
						{
							auto& stExtention = pExt->GetValue();
							auto itr = std::find(vExtentions.begin(), vExtentions.end(), stExtention);
							if(itr == vExtentions.end())
								vExtentions.push_back(stExtention);
						}
					}
				}

				std::string stScriptClass;
				if(auto pScriptClass = pRoot->FirstNode(L"ScriptClass"))
					stScriptClass = conv::ToA(pScriptClass->GetValue());
					
				auto& scene = *new Scene(stName, dir.GetDirectory(), mModules, vExtentions, stScriptClass, mCustoms, vInput);
				m_pScenes->AddScene(scene);
			}
		}

	}
}

