#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class SceneManager;

		class ACCLIMATE_API SceneLoader
		{
		public:
			SceneLoader(SceneManager& scenes);

			void Load(const std::wstring& stFilename) const;

		private:

			void LoadScene(const std::wstring& stName) const;

			SceneManager* m_pScenes;
		};

	}
}

