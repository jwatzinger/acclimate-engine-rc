#include "SceneManager.h"
#include "SceneInstance.h"
#include "ResourceManager.h"
#include "EventLoader.h"
#include "..\Entity\EntityManager.h"
#include "..\Entity\Loader.h"
#include "..\Gfx\IResourceLoader.h"
#include "..\Input\ILoader.h"
#include "..\Input\Context.h"
#include "..\Physics\Context.h"
#include "..\Physics\IPhysicsLoader.h"
#include "..\Script\Context.h"
#include "..\Script\Core.h"
#include "..\Script\Loader.h"
#include "..\System\Convert.h"
#include "..\System\Log.h"
#include "..\System\WorkingDirectory.h"

namespace acl
{
	namespace core
	{

		SceneManager::SceneManager(const gfx::ResourceContext& resources, const gfx::IResourceLoader& resourceLoader, const script::Context& scripts, ecs::EntityManager& entities, ResourceManager& resourceManager, Events& events, const physics::Context& physics, const input::Context& input) :
			m_pResources(&resources), m_pInstance(nullptr), m_pResourceLoader(&resourceLoader), m_pScripts(&scripts), m_pEntities(&entities), m_pResourceManager(&resourceManager),
			m_isLocked(false), m_pPhysics(&physics), m_pInput(&input), m_pEvents(&events)
		{
		}

		SceneManager::~SceneManager(void)
		{
			delete m_pInstance;
		}

		const Scenes& SceneManager::GetScenes(void) const
		{
			return m_scenes;
		}

		const Scene* SceneManager::GetActiveScene(void) const
		{
			if(m_pInstance)
				return m_scenes[m_pInstance->GetName()];
			else
				return nullptr;
		}

		SceneInstance* SceneManager::GetActiveSceneInstance(void)
		{
			return m_pInstance;
		}

		const SceneInstance* SceneManager::GetActiveSceneInstance(void) const
		{
			return m_pInstance;
		}

		const SceneManager::ExtentionMap& SceneManager::GetExtentions(void) const
		{
			return m_mExtentions;
		}

		void SceneManager::SetLocked(bool isLocked)
		{
			m_isLocked = isLocked;
		}

		void SceneManager::AddScene(Scene& scene)
		{
			m_scenes.Add(scene.GetName(), scene);
		}

		void SceneManager::ActivateScene(const std::wstring& stName)
		{
			if(m_isLocked)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "failed to activate scene", stName, "(scenes are locked)");
				return;
			}

			if(stName != m_stNextScene)
			{
				if(auto pScene = m_scenes[stName])
					m_stNextScene = stName;
				else
					sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Tried to activate missing scene", stName);
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Scene", stName, "is already active.");
		}

		void SceneManager::Update(double dt)
		{
			if(!m_stNextScene.empty())
			{
				if(auto pScene = m_scenes[m_stNextScene])
				{
					if(m_pInstance)
					{
						// unload extentions
						if(auto pOldScene = m_scenes[m_pInstance->GetName()])
						{
							for(auto& extention : pOldScene->GetExtentions())
							{
								if(auto pExtention = m_mExtentions[extention])
								{
									pExtention->OnUnload();
								}
							}
						}

						m_pResourceManager->UnloadSetResources(L"Scene");
						m_pEntities->Clear();
						delete m_pInstance;
					}

					// input
					SceneInstance::HandlerVector vHandler;
					auto& vInput = pScene->GetInput();
					for(auto& stInput : vInput)
					{
						vHandler.push_back(m_pInput->loader.Load(stInput));
					}

					const auto& stPath = pScene->GetPath();

					m_pInstance = new SceneInstance(m_stNextScene, stPath, *m_pResources, *m_pPhysics, m_pScripts->core.GetConfigGroup(conv::ToA(m_stNextScene).c_str()), m_pScripts->scripts, m_pInput->module, vHandler, *m_pEvents);

					sys::WorkingDirectory dir(stPath);

					m_pInstance->BeginLoad();

					// scripts
					for(auto& module : pScene->GetModules())
					{
						switch(module.first)
						{
						case Scene::ModuleType::ENTITIES:
						{
							ecs::Loader loader(*m_pEntities);
							loader.Load(module.second);
							break;
						}
						case Scene::ModuleType::RESOURCES:
							m_pResourceLoader->Load(module.second);
							break;
						case Scene::ModuleType::SCRIPTS:
							m_pScripts->loader.FromConfig(module.second);
							break;
						case Scene::ModuleType::PHYSICS:
							m_pPhysics->loader.Load(module.second);
							break;
						case Scene::ModuleType::EVENTS:
						{
							EventLoader loader(*m_pEvents);
							loader.Load(module.second);
							break;
						}
						}
					}
						
					for(auto& custom : pScene->GetCustoms())
					{
						m_pResourceManager->LoadSetResources(custom.first, L"Scene", custom.second);
					}

					if (auto pScriptInstance = m_pScripts->core.CreateInstanceFromInterface("IScene", pScene->GetScriptClass().c_str(), ""))
						m_pInstance->SetScriptInstance(*pScriptInstance);
					else
						sys::log->Out(sys::LogModule::SYSTEM, sys::LogType::ERR, "Failed to create script instance from class", pScene->GetScriptClass(), "for scene", m_stNextScene, "(class either doesn't exist; doesn't inherit from IScene or causes an exception in the constructor)");

					m_pInstance->EndLoad();
					m_stNextScene.clear();

					// load new extentions
					for(auto& extention : pScene->GetExtentions())
					{
						if(auto pExtention = m_mExtentions[extention])
						{
							pExtention->OnLoad();
						}
					}

					return;
				}
			}

			if(m_pInstance)
				m_pInstance->Update(dt);
		}

		void SceneManager::RemoveExtention(const std::wstring& stName)
		{
			auto itr = m_mExtentions.find(stName);
			if(itr != m_mExtentions.end())
				m_mExtentions.erase(itr);
		}

		void SceneManager::Clear(void)
		{
			if(m_pInstance)
			{
				m_pResourceManager->UnloadSetResources(L"Scene");
				delete m_pInstance;
				m_pInstance = nullptr;
			}
			m_scenes.Clear();
		}

	}
}
