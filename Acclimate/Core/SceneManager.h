#pragma once
#include "Scenes.h"
#include "Events.h"
#include "Dll.h"
#include "ISceneExtention.h"
#include "..\Script\ConfigGroup.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace gfx
	{
		struct ResourceContext;
		class IResourceLoader;
	}
	
	namespace input
	{
		struct Context;
	}

	namespace physics
	{
		struct Context;
	}

	namespace script
	{
		struct Context;
	}

	namespace core
	{

		class ResourceManager;
		class SceneInstance;

		class ACCLIMATE_API SceneManager
		{
		public:
			typedef std::unordered_map<std::wstring, ISceneExtention*> ExtentionMap;

			SceneManager(const gfx::ResourceContext& resources, const gfx::IResourceLoader& resourceLoader, const script::Context& scripts, ecs::EntityManager& entities, ResourceManager& resourceManager, Events& events, const physics::Context& physics, const input::Context& input);
			~SceneManager(void);

			void SetLocked(bool isLocked);

			const Scenes& GetScenes(void) const;
			const Scene* GetActiveScene(void) const;
			SceneInstance* GetActiveSceneInstance(void);
			const SceneInstance* GetActiveSceneInstance(void) const;
			const ExtentionMap& GetExtentions(void) const;

			void AddScene(Scene& scene);
			void ActivateScene(const std::wstring& stName);

			template<typename Extention, typename... Args>
			Extention& AddExtention(const std::wstring& stName, Args&&... args)
			{
				auto pExtention = new Extention(args...);
				auto itr = m_mExtentions.find(stName);
				if(itr != m_mExtentions.end())
				{
					delete itr->second;
					itr->second = pExtention;
				}
				else
					m_mExtentions[stName] = pExtention;

				return *pExtention;
			}

			void RemoveExtention(const std::wstring& stName);

			void Update(double dt);
			void Clear(void);

		private:
#pragma warning( disable: 4251 )
			Scenes m_scenes;
			SceneInstance* m_pInstance;
			ExtentionMap m_mExtentions;
			std::wstring m_stNextScene;
			bool m_isLocked;

			Events* m_pEvents;
			ResourceManager* m_pResourceManager;
			const gfx::ResourceContext* m_pResources;
			const gfx::IResourceLoader* m_pResourceLoader;

			const script::Context* m_pScripts;

			ecs::EntityManager* m_pEntities;
			const physics::Context* m_pPhysics;
			const input::Context* m_pInput;
#pragma warning( default: 4251 )
		};

	}
}


