#include "SceneSaver.h"
#include "SceneManager.h"
#include "..\System\Log.h"
#include "..\System\Convert.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		SceneSaver::SceneSaver(const SceneManager& scenes):
			m_pScenes(&scenes)
		{
		}

		void SceneSaver::Save(const std::wstring& stName) const
		{
			xml::Doc doc;

			auto& root = doc.InsertNode(L"Scenes");
			root.ModifyAttribute(L"path", L"../Scenes");

			for(auto& scene : m_pScenes->GetScenes().Map())
			{
				auto& sceneNode = root.InsertNode(L"Scene");
				sceneNode.SetValue(scene.second->GetName());
			}

			doc.SaveFile(stName);
		}

		std::wstring sceneModuleToString(Scene::ModuleType type)
		{
			switch(type)
			{
			case Scene::ModuleType::ENTITIES:
				return L"Entities";
			case Scene::ModuleType::EVENTS:
				return L"Events";
			case Scene::ModuleType::PHYSICS:
				return L"Physics";
			case Scene::ModuleType::RESOURCES:
				return L"Resources";
			case Scene::ModuleType::SCRIPTS:
				return L"Scripts";
			default:
				ACL_ASSERT(false);
			}

			return L"";
		}

		void SceneSaver::SaveScene(const std::wstring& stName, const std::wstring& stFile)
		{
			if(auto pScene = m_pScenes->GetScenes()[stName])
			{
				xml::Doc doc;

				auto& root = doc.InsertNode(L"Scene");

				for(auto& module : pScene->GetModules())
				{
					auto& moduleNode = root.InsertNode(sceneModuleToString(module.first));
					moduleNode.ModifyAttribute(L"file", module.second);
				}

				// script class
				auto& scriptClass = root.InsertNode(L"ScriptClass");
				scriptClass.SetValue(conv::ToW(pScene->GetScriptClass()));

				// customs
				auto& mCustoms = pScene->GetCustoms();
				if(!mCustoms.empty())
				{
					auto& customsNode = root.InsertNode(L"Customs");
					for(auto& custom : mCustoms)
					{
						auto& resources = customsNode.InsertNode(L"Resources");
						resources.SetValue(custom.first);
						resources.ModifyAttribute(L"file", custom.second);
					}
				}

				// extentions
				auto& vExtentions = pScene->GetExtentions();
				if(!vExtentions.empty())
				{
					auto& extentionsNode = root.InsertNode(L"Extentions");
					for(auto& extention : vExtentions)
					{
						auto& extentionNode = extentionsNode.InsertNode(L"Extention");
						extentionNode.SetValue(extention);
					}
				}

				doc.SaveFile(stFile);
			}
			else
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, L"Failed to save scene", stName, L". Scene was not found.");
		}

	}
}
