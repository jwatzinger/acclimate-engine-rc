#pragma once
#include <string>
#include "Dll.h"

namespace acl
{
	namespace core
	{

		class SceneManager;

		class ACCLIMATE_API SceneSaver
		{
		public:
			SceneSaver(const SceneManager& scenes);
		
			void Save(const std::wstring& stName) const;
			void SaveScene(const std::wstring& stName, const std::wstring& stFile);

		private:

			const SceneManager* m_pScenes;
		};

	}
}

