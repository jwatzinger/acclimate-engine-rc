#pragma once
#include <string>
#include "Scene.h"
#include "Dll.h"
#include "Resources.h"

namespace acl
{
	namespace core
	{

		typedef Resources<std::wstring, Scene> Scenes;

		EXPORT_TEMPLATE template class ACCLIMATE_API Resources<std::wstring, Scene>;

	}
}