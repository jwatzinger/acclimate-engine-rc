#include "Setting.h"

namespace acl
{
	namespace core
	{

		Setting::Setting(SettingType type, AccessType access) : m_type(type), m_access(access)
		{
			switch(type)
			{
			case SettingType::FLOAT:
				m_pData = new float;
				break;
			case SettingType::INT:
				m_pData = new int;
				break;
			case SettingType::UINT:
				m_pData = new unsigned int;
				break;
			case SettingType::BOOL:
				m_pData = new bool;
				break;
			case SettingType::STRING:
				m_pData = new std::wstring;
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		Setting::~Setting(void)
		{
			delete m_pData;
		}

		void Setting::SetAccessType(AccessType access)
		{
			m_access = access;
		}

		SettingType Setting::GetType(void) const
		{
			return m_type;
		}

		AccessType Setting::GetAccessType(void) const
		{
			return m_access;
		}

	}
}

