#pragma once
#include "..\System\Assert.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		enum class SettingType
		{
			FLOAT, INT, UINT, BOOL, STRING
		};

		enum class AccessType
		{
			PUBLIC, INTERN, STATIC
		};

		class Setting
		{
		public:
			Setting(SettingType type, AccessType access = AccessType::PUBLIC);
			~Setting(void);

			template<typename Type>
			void Set(const Type& data);
			template<>
			void Set<float>(const float& data);
			template<>
			void Set<int>(const int& data);
			template<>
			void Set<unsigned int>(const unsigned int& data);
			template<>
			void Set<bool>(const bool& data);
			template<>
			void Set<std::wstring>(const std::wstring& data);
			void SetAccessType(AccessType access);

			template<typename Type>
			const Type& GetData(void) const;
			template<>
			const float& GetData<float>(void) const;			
			template<>
			const int& GetData<int>(void) const;
			template<>
			const unsigned int& GetData<unsigned int>(void) const;
			template<>
			const bool& GetData<bool>(void) const;
			template<>
			const std::wstring& GetData<std::wstring>(void) const;
			SettingType GetType(void) const;
			AccessType GetAccessType(void) const;

		private:

			SettingType m_type;
			AccessType m_access;
			void* m_pData;
		};

		template<typename Type>
		void Setting::Set(const Type& data)
		{
			static_assert(false, "Unsupported data type specified for setting-setter.");
		}

		template<>
		void Setting::Set<float>(const float& data)
		{
			if(m_access == AccessType::STATIC)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Variable cannot be altered.");
				return;
			}

			ACL_ASSERT(m_type == SettingType::FLOAT);
			*(float*)m_pData = data;
		}
		
		template<>
		void Setting::Set<int>(const int& data)
		{
			if(m_access == AccessType::STATIC)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Variable cannot be altered.");
				return;
			}

			ACL_ASSERT(m_access != AccessType::STATIC);
			ACL_ASSERT(m_type == SettingType::INT);
			*(int*)m_pData = data;
		}

		template<>
		void Setting::Set<unsigned int>(const unsigned int& data)
		{
			if(m_access == AccessType::STATIC)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Variable cannot be altered.");
				return;
			}

			ACL_ASSERT(m_access != AccessType::STATIC);
			ACL_ASSERT(m_type == SettingType::UINT);
			*(unsigned int*)m_pData = data;
		}

		template<>
		void Setting::Set<bool>(const bool& data)
		{
			if(m_access == AccessType::STATIC)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Variable cannot be altered.");
				return;
			}

			ACL_ASSERT(m_access != AccessType::STATIC);
			ACL_ASSERT(m_type == SettingType::BOOL);
			*(bool*)m_pData = data;
		}

		template<>
		void Setting::Set<std::wstring>(const std::wstring& data)
		{
			if(m_access == AccessType::STATIC)
			{
				sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Variable cannot be altered.");
				return;
			}

			ACL_ASSERT(m_access != AccessType::STATIC);
			ACL_ASSERT(m_type == SettingType::STRING);
			*(std::wstring*)m_pData = data;
		}

		template<typename Type>
		const Type& Setting::GetData(void) const
		{
			static_assert(false, "Unsupported data type specified for setting-getter.");
		}

		template<>
		const float& Setting::GetData<float>(void) const
		{
			ACL_ASSERT(m_type == SettingType::FLOAT);
			return *(const float*)m_pData;
		}

		template<>
		const int& Setting::GetData<int>(void) const
		{
			ACL_ASSERT(m_type == SettingType::INT);
			return *(const int*)m_pData;
		}

		template<>
		const unsigned int& Setting::GetData<unsigned int>(void) const
		{
			ACL_ASSERT(m_type == SettingType::UINT);
			return *(const unsigned int*)m_pData;
		}

		template<>
		const bool& Setting::GetData<bool>(void) const
		{
			ACL_ASSERT(m_type == SettingType::BOOL);
			return *(const bool*)m_pData;
		}

		template<>
		const std::wstring& Setting::GetData<std::wstring>(void) const
		{
			ACL_ASSERT(m_type == SettingType::STRING);
			return *(const std::wstring*)m_pData;
		}

	}
}


