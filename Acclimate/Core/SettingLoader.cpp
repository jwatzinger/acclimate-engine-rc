#include "SettingLoader.h"
#include "SettingModule.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace core
	{

		SettingLoader::SettingLoader(SettingModule& module): m_pModule(&module)
		{
		}

		void SettingLoader::Load(const std::wstring& stFileName) const
		{
			xml::Doc doc;
			doc.LoadFile(stFileName);

			if(auto pRoot = doc.Root(L"Settings"))
			{
				if(auto pSettings = pRoot->Nodes(L"Setting"))
				{
					for(auto pSetting : *pSettings)
					{
						const std::wstring& stType = *pSetting->Attribute(L"type");
						const std::wstring& stName = pSetting->GetValue();

						AccessType access = AccessType::PUBLIC;
						if(auto pAccess = pSetting->Attribute(L"Access"))
							access = (AccessType)pAccess->AsInt();

						if(stType == L"float")
						{
							m_pModule->AddSetting<float>(stName, access);
							m_pModule->GetSetting(stName)->Set<float>(pSetting->Attribute(L"default")->AsFloat());
						}
						else if(stType == L"int")
						{
							m_pModule->AddSetting<int>(stName, access);
							m_pModule->GetSetting(stName)->Set<int>(pSetting->Attribute(L"default")->AsInt());
						}
						else if(stType == L"unsigned int")
						{
							m_pModule->AddSetting<unsigned int>(stName, access);
							m_pModule->GetSetting(stName)->Set<unsigned int>(pSetting->Attribute(L"default")->AsInt());
						}
						else if(stType == L"bool")
						{
							m_pModule->AddSetting<bool>(stName, access);
							m_pModule->GetSetting(stName)->Set<bool>(pSetting->Attribute(L"default")->AsBool());
						}
						else if(stType == L"string")
						{
							m_pModule->AddSetting<std::wstring>(stName, access);
							m_pModule->GetSetting(stName)->Set<std::wstring>(*pSetting->Attribute(L"default"));
						}
						else
							sys::log->Out(sys::LogModule::CORE, sys::LogType::ERR, "Invalid type specified for setting", stName, "in file", stFileName);
					}
					m_pModule->Refresh();
				}
			}
		}

	}
}

