#include "SettingModule.h"

namespace acl
{
	namespace core
	{
		
		void SettingModule::Refresh(void) const
		{
			SigUpdateSettings(*this);
		}

		Setting* SettingModule::GetSetting(const std::wstring& stName) const
		{
			if(m_mSettings.count(stName) != 0)
				return m_mSettings.at(stName);
			else
				return nullptr;
		}

		const SettingModule::SettingMap& SettingModule::GetSettings(void) const
		{
			return m_mSettings;
		}

	}
}
