#pragma once
#include <unordered_map>
#include "Signal.h"
#include "Setting.h"
#include "..\Core\Dll.h"
#include "..\System\Log.h"

namespace acl
{
	namespace core
	{

		class Setting;
		class SettingModule;

		EXPORT_TEMPLATE template class ACCLIMATE_API Signal<const SettingModule&>;

		class ACCLIMATE_API SettingModule
		{
		public:
			typedef std::unordered_map<std::wstring, Setting*> SettingMap;

			template<typename Type>
			void AddSetting(const std::wstring& stName, AccessType access);

			void Refresh(void) const;

			Setting* GetSetting(const std::wstring& stName) const;
			const SettingMap& GetSettings(void) const;

			Signal<const SettingModule&> SigUpdateSettings;

		private:
#pragma warning( disable: 4251 )

			template<typename Type>
			Setting& GenerateSetting(const std::wstring& stName);
			template<>
			Setting& GenerateSetting<float>(const std::wstring& stName);
			template<>
			Setting& GenerateSetting<int>(const std::wstring& stName);
			template<>
			Setting& GenerateSetting<unsigned int>(const std::wstring& stName);
			template<>
			Setting& GenerateSetting<bool>(const std::wstring& stName);
			template<>
			Setting& GenerateSetting<std::wstring>(const std::wstring& stName);

			SettingMap m_mSettings;
#pragma warning( default: 4251 )
		};

		template<typename Type>
		void SettingModule::AddSetting(const std::wstring& stName, AccessType access)
		{
			if(m_mSettings.count(stName))
				sys::log->Out(sys::LogModule::CORE, sys::LogType::WARNING, "Setting", stName, "already exists");
			else
			{
				Setting& setting = GenerateSetting<Type>(stName);
				setting.SetAccessType(access);
				m_mSettings[stName] = &setting;
			}
				
		}

		template<>
		Setting& SettingModule::GenerateSetting<float>(const std::wstring& stName)
		{
			return *new Setting(SettingType::FLOAT);
		}

		template<>
		Setting& SettingModule::GenerateSetting<int>(const std::wstring& stName)
		{
			return *new Setting(SettingType::INT);
		}

		template<>
		Setting& SettingModule::GenerateSetting<unsigned int>(const std::wstring& stName)
		{
			return *new Setting(SettingType::UINT);
		}

		template<>
		Setting& SettingModule::GenerateSetting<bool>(const std::wstring& stName)
		{
			return *new Setting(SettingType::BOOL);
		}

		template<>
		Setting& SettingModule::GenerateSetting<std::wstring>(const std::wstring& stName)
		{
			return *new Setting(SettingType::STRING);
		}

	}
}


