/*
* Acclimate Engine: Core
*
* A lightweight signals and slots implementation using fast delegates
*
*
* Created by Patrick Hogan on 5/18/09.
* Modified for the Acclimate Engine by Julian Watzinger 10/12/13
*
*/

#ifndef _Signal_H_
#define _Signal_H_

#include "Delegate.h"
#include <set>

namespace acl
{
    namespace core 
    {

		template<typename ... Args>
		class Signal
        {
        public:
            typedef Delegate< Args... > _Delegate;

        private:
#pragma warning( disable: 4251 )
            typedef std::set<_Delegate> DelegateList;
            typedef typename DelegateList::const_iterator DelegateIterator;
            DelegateList delegateList;
#pragma warning( default: 4251 )

        public:
            void Connect( _Delegate delegate )
            {
                delegateList.insert( delegate );
            }

            template< class X, class Y >
            void Connect( Y * obj, void (X::*func)( Args... args ) )
            {
                delegateList.insert( MakeDelegate( obj, func ) );
            }

            template< class X, class Y >
            void Connect( Y * obj, void (X::*func)( Args... args ) const )
            {
                delegateList.insert( MakeDelegate( obj, func ) );
            }

            void Disconnect( _Delegate delegate )
            {
                delegateList.erase( delegate );
            }

            template< class X, class Y >
            void Disconnect( Y * obj, void (X::*func)( Args... args ) )
            {
                delegateList.erase( MakeDelegate( obj, func ) );
            }

            template< class X, class Y >
            void Disconnect( Y * obj, void (X::*func)( Args... args ) const )
            {
                delegateList.erase( MakeDelegate( obj, func ) );
            }

            void Clear()
            {
                delegateList.clear();
            }

            void Emit( Args... args) const
            {
                for (auto& delegate : delegateList)
                {
                    delegate( args... );
                }
            }

            void operator() ( Args... args ) const
            {
                for (auto& delegate : delegateList)
                {
                    delegate( args... );
                }
            }
        };

    } //  
}

#endif //_SIGNAL_H_