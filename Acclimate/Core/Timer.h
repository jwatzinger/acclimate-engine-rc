#pragma once
#include "Dll.h"

namespace acl
{
	namespace core
	{

		/// High precision timer
		/** This class offers high precision timer functionality with little overhead, using the 
		*	performance counter. The duration can be calculated and output at any time,
		*	aside from this, no performance overhead occurs. The timer can also be reset.*/
		class ACCLIMATE_API Timer
		{
		public:
			Timer(void);

			/** 
			 *	Resets the timers duration to 0.
			 */
			void Reset(void);

			/** Aquires the duration since the last reset.
			 *	Computes this duration using the current processor performance counter,
			 *	so it is recommendet to cache the result for repeated calls in a frame.
			 *
			 *	@return Duration since last call of reset, in milliseconds.
			 */
			double Duration(void) const;

		private:

			long long m_lStartTime;   //start time
			long long m_lFrequency;   //frequency
		};

	}
}
