#include "TriggerDeclaration.h"
#include "EventAttribute.h"

namespace acl
{
	namespace core
	{

		TriggerDeclaration::TriggerDeclaration(const std::wstring& stName, const ReturnVector& vReturns) : m_stName(stName),
			m_vReturns(vReturns)
		{
		}

		const std::wstring& TriggerDeclaration::GetName(void) const
		{
			return m_stName;
		}

		const ReturnVector& TriggerDeclaration::GetReturns(void) const
		{
			return m_vReturns;
		}

	}
}

