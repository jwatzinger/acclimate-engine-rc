#pragma once
#include "EventDeclaration.h"

namespace acl
{
	namespace core
	{

		typedef std::vector<AttributeDeclaration> ReturnVector;

		class ACCLIMATE_API TriggerDeclaration
		{
		public:
			TriggerDeclaration(const std::wstring& stName, const ReturnVector& vReturns);

			const std::wstring& GetName(void) const;
			const ReturnVector& GetReturns(void) const;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stName;

			ReturnVector m_vReturns;
#pragma warning( default: 4251 )
		};

	}
}

