#include "WMessageHandler.h"
#include "..\Gui\WindowProcHook.h"
#include "..\Input\WindowProcHook.h"
#include "..\Math\Vector.h"

namespace acl
{
    namespace core
    {

        LRESULT CALLBACK WindowHandler(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
        {
            // test for message
            switch(msg)
            {
             // Quit program if window is closed
	        case WM_CLOSE:
		        if( !gui::WindowProcHook::PostCloseMessage() )
			        return 0;
		        break;
            case WM_DESTROY:
		        PostQuitMessage(0);
		        return 0;
	        case WM_MOUSEMOVE:
			{
				RECT r;
				GetWindowRect(hwnd, &r);
				const math::Vector2& vScreenSize = gui::WindowProcHook::GetSize();

				if(!gui::WindowProcHook::IsCursorVisible())
				{
					int nowX = (int)((vScreenSize.x / 2) * (vScreenSize.x / (float)(r.right - r.left)));
					int nowY = (int)((vScreenSize.y / 2) * (vScreenSize.y / (float)(r.bottom - r.top)));
					input::WindowProcHook::SetAxisValueForced(input::Axis::MOUSE_X, (float)nowX);
					input::WindowProcHook::SetAxisValueForced(input::Axis::MOUSE_Y, (float)nowY);
				}

				int nowX = (int)(LOWORD(lParam) * (vScreenSize.x / (float)(r.right - r.left)));
				int nowY = (int)(HIWORD(lParam) * (vScreenSize.y / (float)(r.bottom - r.top)));
		        input::WindowProcHook::SetRawAxisValue(input::Axis::MOUSE_X, (float)nowX);
				input::WindowProcHook::SetRawAxisValue(input::Axis::MOUSE_Y, (float)nowY);

				input::WindowProcHook::SetMousePos(math::Vector2(nowX, nowY));

		        break;
			}
	        case WM_LBUTTONDBLCLK:
				input::WindowProcHook::SetRawButtonState(input::mouseToKey(2), true, false);
		        break;
	        case WM_LBUTTONDOWN:
		        input::WindowProcHook::SetRawButtonState(input::mouseToKey(0), true, false);
				break;
	        case WM_LBUTTONUP:
		        input::WindowProcHook::SetRawButtonState(input::mouseToKey(0), false, true);
		        break;
	        case WM_RBUTTONDOWN:
				input::WindowProcHook::SetRawButtonState(input::mouseToKey(1), true, false);
		        break;
	        case WM_RBUTTONUP:
		        input::WindowProcHook::SetRawButtonState(input::mouseToKey(1), false, true);
		        break;
	        case WM_MBUTTONDOWN:
		        input::WindowProcHook::SetRawButtonState(input::mouseToKey(2), true, false);
		        break;
			case WM_MBUTTONUP:
		        input::WindowProcHook::SetRawButtonState(input::mouseToKey(2), false, true);
		        break;
	        case WM_MOUSEWHEEL:
		        input::WindowProcHook::SetRawAxisValue(input::Axis::MOUSE_WHEEL, -1*GET_WHEEL_DELTA_WPARAM(wParam)/4.0f, false);
		        break;
	        case WM_KEYDOWN:
			{
				bool bPreviouslyDown = ((lParam & (1 << 31)) != 0);
			    input::WindowProcHook::SetRawButtonState(input::winToKey(wParam), true, bPreviouslyDown);
				gui::WindowProcHook::OnShortcut(wParam);
		        break;
			}
	        case WM_KEYUP:
			    input::WindowProcHook::SetRawButtonState(input::winToKey(wParam), false, true);
				break;
	        case WM_CHAR:
		        gui::WindowProcHook::OnKeyStroke((WCHAR)wParam);
		        break;
			case WM_SYSKEYDOWN:
				if(wParam == 18)
					input::WindowProcHook::SetRawButtonState(input::winToKey(18), true, false);
				else
					gui::WindowProcHook::OnShortcut(wParam);
				break;
			case WM_SYSKEYUP:
				input::WindowProcHook::SetRawButtonState(input::winToKey(wParam), false, true);
				break;
			case WM_ACTIVATE:
				input::WindowProcHook::SetRawButtonState(input::Keys::ALT, false, false);
				break;
	        }

            return DefWindowProc(hwnd, msg, wParam, lParam);
        }

    }
}