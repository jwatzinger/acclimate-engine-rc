#pragma once
#include <Windows.h>

namespace acl
{
    namespace core
    {

        LRESULT CALLBACK WindowHandler(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam);

    }
}