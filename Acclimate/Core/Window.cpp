#include "Window.h"

namespace acl
{
    namespace core
    {

        Window::Window(HINSTANCE hInstance, LPCWSTR lpClassString, LPCWSTR lpTitle, int x, int y, int width, int height)
        {
	        m_hInstance = hInstance;
	        m_x = x, m_y = y, m_width = width, m_height = height;

            m_hWnd = CreateWindowEx(0,
                                  lpClassString,
                                  lpTitle,
                                  WS_VISIBLE | WS_EX_TOPMOST | WS_POPUP,
                                  m_x, m_y,
                                  m_width, m_height,
                                  nullptr,
                                  nullptr,
                                  m_hInstance,
                                  nullptr); 
        }

        Window::~Window(void)
        {
			DestroyWindow(m_hWnd);
        }

		void Window::Resize(int width, int height)
		{
			if(width != m_width || height != m_height)
			{
				m_width = width;
				m_height = height;

				SetWindowPos(m_hWnd, 0, m_x, m_y, m_width, m_height, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
			}
		}

        HWND Window::GethWnd(void) const
        {
	        return m_hWnd;
        }

    }
}