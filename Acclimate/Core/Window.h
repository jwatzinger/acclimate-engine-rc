#pragma once
#include <Windows.h>

namespace acl
{
    namespace core
    {

        class Window
        {
        public:
	        Window(HINSTANCE hInstance, LPCWSTR lpClassString, LPCWSTR lpTitle, int x, int y, int width, int height);
	        ~Window(void);

			void Resize(int width, int height);

	        HWND GethWnd(void) const; //getter: window handle

        protected:

	        HWND m_hWnd;                //window handle
	        HINSTANCE m_hInstance;      //program instance handle
	        int m_x, m_y;             //window position parameters
	        int m_width, m_height;    //window size parameters
        };
        
    }
}

