#include "WindowModule.h"
#include <algorithm>
#include "WMessageHandler.h"
#include "Window.h"
#include "..\ApiDef.h"

namespace acl
{
    namespace core
    {

        WindowModule::WindowModule(HINSTANCE hInstance): m_hInstance(hInstance)
        {
        }


        WindowModule::~WindowModule(void)
        {
            //delete all windows
	        for(auto pWindow : m_vWindows)
	        {
		        delete pWindow;
	        }

            //unregister classes
            for(const std::wstring& stClass : m_vWndClassStrings)
            {
                UnregisterClass(stClass.c_str(), m_hInstance);
            }
        }

        Window* WindowModule::NewMainWindow(LPCWSTR lpTitle)
        {
	        return NewWindow(L"MainClass", lpTitle, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
        }

        Window* WindowModule::NewWindow(LPCWSTR lpClassString, LPCWSTR lpTitle, int x, int y, int width, int height)
        {
	        //class doesn't exist?
	        if(!HasWindowClass(lpClassString))
	        {
		        WNDCLASSEX wndClass =
		        {
			        sizeof(WNDCLASSEX),                                 // size
#ifdef ACL_API_GL4
					CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW | CS_OWNDC,
#else
			        CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW,    // styles
#endif
			        WindowHandler,                                     // callback-function
			        0,                                                  // additional infos
			        0,                                                  // not needed
			        m_hInstance,                 
			        LoadIcon(nullptr, IDI_WINLOGO),                        // windows logo
			        nullptr,												// normal cursor
			        (HBRUSH)GetStockObject(WHITE_BRUSH),                // white brush
			        nullptr,                                               // no menue
			        lpClassString,                                     // class name
			        LoadIcon(nullptr, IDI_WINLOGO)                         // windows logo
		        };

		        RegisterWindowClass(wndClass);
	        }

	        Window* pWindow = new Window(m_hInstance, lpClassString, lpTitle, x, y, width, height);
			m_vWindows.push_back(pWindow);
			return pWindow;
        }

        int WindowModule::GetWindowNum(void) const
        {
	        return m_vWindows.size();
        }

        bool WindowModule::HasWindowClass(LPCWSTR lpClassName) const
        {
	        ClassVector::const_iterator it = find(m_vWndClassStrings.cbegin(), m_vWndClassStrings.cend(), lpClassName);
	        //class name was found?
	        return it != m_vWndClassStrings.end();
        }

        void WindowModule::RegisterWindowClass(WNDCLASSEX wndClass)
        {
	        RegisterClassEx(&wndClass);
	        //add class name to vector
	        m_vWndClassStrings.push_back(wndClass.lpszClassName);
        }

    }
}