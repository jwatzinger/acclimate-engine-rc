#pragma once
#include <Windows.h>
#include <vector>
#include <string>

namespace acl
{
    namespace core
    {
        class Window;
        class WindowModule
        {
            typedef std::vector<std::wstring> ClassVector;
            typedef std::vector<Window*> WindowVector;

        public:
	        WindowModule(HINSTANCE hInstance);
	        ~WindowModule(void);

	        Window* NewMainWindow(LPCWSTR lpTitle);

	        int GetWindowNum(void) const;

        private:

            HINSTANCE m_hInstance;

	        Window* NewWindow(LPCWSTR lpClassString, LPCWSTR lpTitle, int x, int y, int width, int height);
	        bool HasWindowClass(LPCWSTR lpClassName) const;
	        void RegisterWindowClass(WNDCLASSEX wndClass);

	        ClassVector m_vWndClassStrings;   //vector for window class names
	        WindowVector m_vWindows;
        };

    }
}
