#include "ConstantBuffer.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "..\Safe.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			ConstantBuffer::ConstantBuffer(const Device& device, size_t sizeInByte): m_pDevice(&device), m_size(sizeInByte)
			{
				D3D11_BUFFER_DESC bufferDesc = {};
				bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				bufferDesc.ByteWidth = m_size;
				bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

				m_pBuffer = device.CreateBuffer(bufferDesc, nullptr);
			}

			ConstantBuffer::ConstantBuffer(const ConstantBuffer& constantBuffer): m_pDevice(constantBuffer.m_pDevice), m_size(constantBuffer.m_size)
			{
				D3D11_BUFFER_DESC bufferDesc = {};
				bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				bufferDesc.ByteWidth = m_size;
				bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
				bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

				m_pBuffer = m_pDevice->CreateBuffer(bufferDesc, nullptr);
			}

			ConstantBuffer::~ConstantBuffer(void)
			{
				SAFE_RELEASE(m_pBuffer);
			}

			size_t ConstantBuffer::GetSize(void) const
			{
				return m_size;
			}

			void ConstantBuffer::Overwrite(void* pData, size_t range)
			{
				D3D11_MAPPED_SUBRESOURCE mappedResource;
				m_pDevice->Map(*m_pBuffer, D3D11_MAP_WRITE_DISCARD, mappedResource);

				range = min(m_size, range);

				memcpy(mappedResource.pData, pData, range);

				m_pDevice->Unmap(*m_pBuffer);
			}

		}
	}
}

#endif