#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class ConstantBuffer
			{
			public:
				ConstantBuffer(const Device& device, size_t sizeInByte);
				ConstantBuffer(const ConstantBuffer& constantBuffer);
				~ConstantBuffer(void);

				ID3D11Buffer* GetBuffer(void) const
				{
					return m_pBuffer;
				}

				size_t GetSize(void) const;

				void Overwrite(void* pData, size_t range);

			private:

				size_t m_size;

				ID3D11Buffer* m_pBuffer;

				const Device* m_pDevice;
			};

		}
	}
}

#endif