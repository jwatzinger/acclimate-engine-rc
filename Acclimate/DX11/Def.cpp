#include "Def.h"

#pragma comment(lib, "d3d11.lib")
#ifdef _DEBUG
#pragma comment(lib, "d3dx11d.lib")
#else
#pragma comment(lib, "d3dx11.lib")
#endif
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "D3Dcompiler.lib")
#pragma comment(lib, "dxgi.lib")
