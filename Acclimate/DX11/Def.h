#pragma once
#ifdef USE_API_DX11
#define ACL_API_DX11

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;
			class Sprite;
			class Texture;
			class VertexBuffer;
			class Effect;
			class Mesh;
			class Font;
			class DepthBuffer;
			class Line;
			class ConstantBuffer;
			class InputLayout;
		}
	}

	typedef dx11::d3d::Device AclDevice; 
	typedef dx11::d3d::Sprite AclSprite;
	typedef dx11::d3d::Texture AclTexture;
	typedef dx11::d3d::DepthBuffer AclDepth;
	typedef dx11::d3d::VertexBuffer AclVertexBuffer;
    typedef dx11::d3d::Effect AclEffect;
    typedef dx11::d3d::Mesh AclMesh;
    typedef dx11::d3d::Font AclFont;
	typedef dx11::d3d::Line AclLine;
	typedef dx11::d3d::ConstantBuffer AclCbuffer;
	typedef dx11::d3d::InputLayout AclGeometry;

}

#endif