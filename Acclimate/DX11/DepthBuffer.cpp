#include "DepthBuffer.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "..\Safe.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			DepthBuffer::DepthBuffer(const Device& device, int width, int height): m_pTexture(nullptr), m_pView(nullptr), 
				m_vSize(width, height), m_pDevice(&device)
			{
				m_pTexture = device.Create2DTexture(width, height, nullptr, 0, DXGI_FORMAT_D24_UNORM_S8_UINT, D3D11_USAGE_DEFAULT, D3D11_BIND_DEPTH_STENCIL, 0, 0);
				m_pView = device.CreateDepthView(*m_pTexture);
			}

			DepthBuffer::~DepthBuffer(void)
			{
				SAFE_RELEASE(m_pTexture);
				SAFE_RELEASE(m_pView);
			}

			void DepthBuffer::Recreate(int width, int height)
			{
				m_vSize = math::Vector2(width, height);

				m_pTexture->Release();
				m_pView->Release();

				m_pTexture = m_pDevice->Create2DTexture(width, height, nullptr, 0, DXGI_FORMAT_D24_UNORM_S8_UINT, D3D11_USAGE_DEFAULT, D3D11_BIND_DEPTH_STENCIL, 0, 0);
				m_pView = m_pDevice->CreateDepthView(*m_pTexture);
			}

			const math::Vector2& DepthBuffer::GetSize(void) const
			{
				return m_vSize;
			}

			ID3D11DepthStencilView* DepthBuffer::GetView(void) const
			{
				return m_pView;
			}

		}
	}
}

#endif