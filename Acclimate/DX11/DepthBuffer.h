#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>
#include "..\Math\Vector.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class DepthBuffer
			{
			public:
				DepthBuffer(const Device& device, int width, int height);
				~DepthBuffer(void);

				void Recreate(int width, int height);

				const math::Vector2& GetSize(void) const;
				ID3D11DepthStencilView* GetView(void) const;

			private:

				const Device* m_pDevice;

				ID3D11Texture2D* m_pTexture;
				ID3D11DepthStencilView* m_pView;

				math::Vector2 m_vSize;
			};

		}
	}
}

#endif