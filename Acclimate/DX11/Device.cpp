#include "Device.h"

#ifdef ACL_API_DX11

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "Effect.h"
#include "Texture.h"
#include "DepthBuffer.h"
#include "InputLayout.h"
#include "ConstantBuffer.h"
#include "Sampler.h"
#include "InputLayout.h"
#include "..\Safe.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			Device::Device(const DXGI_SWAP_CHAIN_DESC& description): m_pContext(nullptr), m_pDevice(nullptr), m_pSwapChain(nullptr),
				m_vSize(description.BufferDesc.Width, description.BufferDesc.Height), m_format(description.BufferDesc.Format),
				m_isFullscreen(!description.Windowed)
			{

				UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

				if(FAILED(D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, createDeviceFlags, nullptr, 0, D3D11_SDK_VERSION, &description, &m_pSwapChain, &m_pDevice, &m_featureLevel, &m_pContext)))
					throw apiException();

				ID3D11Texture2D* pBackBuffer = nullptr;
				if(FAILED(m_pSwapChain->GetBuffer(0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer)))
					throw apiException();

				m_pDevice->CreateRenderTargetView(pBackBuffer, nullptr, &m_pBackbufferView);
				pBackBuffer->Release();

				m_pContext->OMSetRenderTargets(1, &m_pBackbufferView, nullptr);

				D3D11_VIEWPORT vp;
				vp.Width = (float)m_vSize.x;
				vp.Height = (float)m_vSize.y;
				vp.MinDepth = 0.0f;
				vp.MaxDepth = 1.0f;
				vp.TopLeftX = 0;
				vp.TopLeftY = 0;
				m_pContext->RSSetViewports( 1, &vp );
			}

			Device::~Device(void)
			{
				SAFE_RELEASE(m_pContext);
				SAFE_RELEASE(m_pDevice);
				SAFE_RELEASE(m_pBackbufferView);
				SAFE_RELEASE(m_pSwapChain);
			}

			void Device::ResizeSwapChain(int width, int height)
			{
				m_pBackbufferView->Release();
				m_pSwapChain->ResizeBuffers(1, width, height, m_format, 0);

				ID3D11Texture2D* pBackBuffer = nullptr;
				if(FAILED(m_pSwapChain->GetBuffer(0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer)))
					throw apiException();

				m_pDevice->CreateRenderTargetView(pBackBuffer, nullptr, &m_pBackbufferView);
				pBackBuffer->Release();

				m_pContext->OMSetRenderTargets(1, &m_pBackbufferView, nullptr);

				m_vSize = math::Vector2(width, height);
			}

			void Device::SetFullscreen(bool isFullscreen)
			{
				m_pSwapChain->SetFullscreenState(isFullscreen, nullptr);
				m_isFullscreen = isFullscreen;
			}

			bool Device::IsFullscreen(void) const
			{
				return m_isFullscreen;
			}

			/************************************************
			* Creation methods
			************************************************/

			ID3D11Buffer* Device::CreateBuffer(const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA* pInitData) const
			{
				ID3D11Buffer* pBuffer = nullptr;

				if(FAILED(m_pDevice->CreateBuffer(&bufferDesc, pInitData, &pBuffer)))
					throw apiException();

				return pBuffer;
			}

			ID3D11VertexShader* Device::CreateVertexShader(ID3DBlob& vertexBlob) const
			{
				ID3D11VertexShader* pVertexShader = nullptr;

				if(FAILED(m_pDevice->CreateVertexShader(vertexBlob.GetBufferPointer(), vertexBlob.GetBufferSize(), nullptr, &pVertexShader)))
					throw apiException();

				return pVertexShader;
			}

			ID3D11PixelShader* Device::CreatePixelShader(ID3DBlob& pixelBlob, ID3D11ClassLinkage* pLinkage) const
			{
				ID3D11PixelShader* pPixelShader = nullptr;

				if(FAILED(m_pDevice->CreatePixelShader(pixelBlob.GetBufferPointer(), pixelBlob.GetBufferSize(), pLinkage, &pPixelShader)))
					throw apiException();

				return pPixelShader;
			}

			ID3D11GeometryShader* Device::CreateGeometryShader(ID3DBlob& geometryBlob) const
			{
				ID3D11GeometryShader* pGeometryShader = nullptr;

				if(FAILED(m_pDevice->CreateGeometryShader(geometryBlob.GetBufferPointer(), geometryBlob.GetBufferSize(), nullptr, &pGeometryShader)))
					throw apiException();

				return pGeometryShader;
			}

			ID3D11InputLayout* Device::CreateInputLayout(D3D11_INPUT_ELEMENT_DESC* pElementDesc, size_t numElements, ID3DBlob& vertexShaderBlob) const
			{
				ID3D11InputLayout* pLayout = nullptr;

				if(FAILED(m_pDevice->CreateInputLayout(pElementDesc, numElements, vertexShaderBlob.GetBufferPointer(), vertexShaderBlob.GetBufferSize(), &pLayout)))
					throw apiException();

				return pLayout;
			}

			ID3D11Texture2D* Device::Create2DTexture(int width, int height, const void* pData, int pitch, DXGI_FORMAT format, D3D11_USAGE usage, UINT bindFlags, UINT cpuFlags, UINT miscFlags) const
			{
				ID3D11Texture2D* pTexture = nullptr;

				D3D11_TEXTURE2D_DESC desc;
				desc.Width = width;
				desc.Height = height;
				desc.ArraySize = 1;
				if(miscFlags & D3D11_RESOURCE_MISC_GENERATE_MIPS)
					desc.MipLevels = 0;
				else
					desc.MipLevels = 1;
				desc.Format = format;
				desc.Usage = usage;
				desc.BindFlags = bindFlags;
				desc.CPUAccessFlags = cpuFlags;
				desc.MiscFlags = miscFlags;
				desc.SampleDesc.Count = 1;				
				desc.SampleDesc.Quality = 0;

				if(pData)
				{
					D3D11_SUBRESOURCE_DATA data;
					data.pSysMem = pData;
					data.SysMemPitch = pitch;
					data.SysMemSlicePitch = pitch;

					if(FAILED(m_pDevice->CreateTexture2D(&desc, &data, &pTexture)))
						throw apiException();
				}
				else if(FAILED(m_pDevice->CreateTexture2D(&desc, nullptr, &pTexture)))
					throw apiException();

				return pTexture;
			}

			ID3D11ShaderResourceView* Device::CreateShaderResourceView(ID3D11Resource& resource) const
			{
				ID3D11ShaderResourceView* pView = nullptr;

				if(FAILED(m_pDevice->CreateShaderResourceView(&resource, nullptr,  &pView)))
					throw apiException();

				return pView;
			}

			ID3D11DepthStencilView* Device::CreateDepthView(ID3D11Texture2D& texture) const
			{
				ID3D11DepthStencilView* pView = nullptr;

				if(FAILED(m_pDevice->CreateDepthStencilView((ID3D11Resource*)&texture, nullptr, &pView)))
					throw apiException();

				return pView;
			}

			ID3D11SamplerState* Device::CreateSampler(const D3D11_SAMPLER_DESC& desc) const
			{
				ID3D11SamplerState* pSampler = nullptr;

				if(FAILED(m_pDevice->CreateSamplerState(&desc, &pSampler)))
					throw apiException();

				return pSampler;
			}

			ID3D11RenderTargetView* Device::CreateRenderTargetView(ID3D11Resource& resource) const
			{
				ID3D11RenderTargetView* pView = nullptr;

				if(FAILED(m_pDevice->CreateRenderTargetView(&resource, nullptr, &pView)))
					throw apiException();

				return pView;
			}

			ID3D11DepthStencilState* Device::CreateDepthState(const D3D11_DEPTH_STENCIL_DESC& desc) const
			{
				ID3D11DepthStencilState* pDepthState = nullptr;

				if(FAILED(m_pDevice->CreateDepthStencilState(&desc, &pDepthState)))
					throw apiException();

				return pDepthState;
			}

			ID3D11BlendState* Device::CreateBlendState(const D3D11_BLEND_DESC& desc) const
			{
				ID3D11BlendState* pBlendState = nullptr;

				if(FAILED(m_pDevice->CreateBlendState(&desc, &pBlendState)))
					throw apiException();

				return pBlendState;
			}

			ID3D11RasterizerState* Device::CreateRasterState(const D3D11_RASTERIZER_DESC& desc) const
			{
				ID3D11RasterizerState* pRasterState = nullptr;

				if(FAILED(m_pDevice->CreateRasterizerState(&desc, &pRasterState)))
					throw apiException();

				return pRasterState;
			}

			ID3D11ClassLinkage* Device::CreateClassLinkage(void) const
			{
				ID3D11ClassLinkage* pLinkage = nullptr;

				if(FAILED(m_pDevice->CreateClassLinkage(&pLinkage)))
					throw apiException();

				return pLinkage;
			}

			/************************************************
			* Setter methods
			************************************************/

			void Device::SetVertexBuffer(unsigned int slot, const VertexBuffer& vertexBuffer, unsigned int offset) const
			{
				ID3D11Buffer* buffers[1] = { &vertexBuffer.GetBuffer()};
				const UINT strides[1] = { vertexBuffer.GetVertexSize() };
				const UINT offsets[1] = { offset };
				m_pContext->IASetVertexBuffers(slot, 1, buffers, strides, offsets);
			}

			void Device::SetVertexBuffer(unsigned int slot, const std::vector<VertexBuffer*>& vVertexBuffer) const
			{
				const size_t numBuffer = vVertexBuffer.size();

				ID3D11Buffer** pBuffers = new ID3D11Buffer*[numBuffer];
				UINT* pStrides = new UINT[numBuffer];
				UINT* pOffsets = new UINT[numBuffer]();

				unsigned int vertexBufferCounter = 0;
				for(auto pVertexBuffer : vVertexBuffer)
				{
					pBuffers[vertexBufferCounter] = &pVertexBuffer->GetBuffer();
					pStrides[vertexBufferCounter] = pVertexBuffer->GetVertexSize();
					vertexBufferCounter++;
				}

				m_pContext->IASetVertexBuffers(slot, numBuffer, pBuffers, pStrides, pOffsets);

				delete[] pBuffers;
				delete[] pStrides;
				delete[] pOffsets;
			}

			void Device::SetIndexBuffer(const IndexBuffer& indexBuffer) const
			{
				m_pContext->IASetIndexBuffer(&indexBuffer.GetBuffer(), DXGI_FORMAT_R32_UINT, 0);
			}

			void Device::SetEffect(const Effect& effect) const
			{
				m_pContext->VSSetShader(&effect.GetVertexShader(), nullptr, 0);
				m_pContext->PSSetShader(&effect.GetPixelShader(), effect.GetClassInstances(), effect.GetNumInterfaces());
				m_pContext->GSSetShader(effect.GetGeometryShader(), nullptr, 0);
			}

			void Device::SetTexture(size_t id, const Texture* pTexture) const
			{
				if(pTexture)
				{
					ID3D11ShaderResourceView* pViews[1] = {pTexture->GetResourceView()};
					m_pContext->PSSetShaderResources(id, 1, pViews);
				}
				else
				{
					ID3D11ShaderResourceView* pViews[1] = {nullptr};
					m_pContext->PSSetShaderResources(id, 1, pViews);
				}
			}

			void Device::SetVertexTexture(size_t id, const Texture* pTexture) const
			{
				if(pTexture)
				{
					ID3D11ShaderResourceView* pViews[1] = {pTexture->GetResourceView()};
					m_pContext->VSSetShaderResources(id, 1, pViews);
				}
				else
				{
					ID3D11ShaderResourceView* pViews[1] = {nullptr};
					m_pContext->VSSetShaderResources(id, 1, pViews);
				}
			}

			void Device::SetVertexShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const
			{
				ID3D11Buffer* buffers[1] = { cBuffer.GetBuffer() };
				m_pContext->VSSetConstantBuffers(slot, 1,  buffers);
			}

			void Device::SetPixelShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const
			{
				ID3D11Buffer* buffers[1] = { cBuffer.GetBuffer() };
				m_pContext->PSSetConstantBuffers(slot, 1,  buffers);
			}

			void Device::SetGeometryShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const
			{
				ID3D11Buffer* buffers[1] = { cBuffer.GetBuffer() };
				m_pContext->GSSetConstantBuffers(slot, 1, buffers);
			}

			void Device::SetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY topology) const
			{
				m_pContext->IASetPrimitiveTopology(topology);
			}

			void Device::SetPixelSampler(size_t slot, const Sampler& sampler) const
			{
				ID3D11SamplerState* pSampler[1] = { &sampler.GetSampler() };
				m_pContext->PSSetSamplers(slot, 1, pSampler);
			}

			void Device::SetVertexSampler(size_t slot, const Sampler& sampler) const
			{
				ID3D11SamplerState* pSampler[1] = { &sampler.GetSampler() };
				m_pContext->VSSetSamplers(slot, 1, pSampler);
			}

			void Device::SetRenderTarget(size_t numTargets, ID3D11RenderTargetView** pRenderTargetViews, ID3D11DepthStencilView* pDepthView) const
			{
				if(numTargets == 0)
					m_pContext->OMSetRenderTargets(1, &m_pBackbufferView, pDepthView); 
				else
					m_pContext->OMSetRenderTargets(numTargets, pRenderTargetViews, pDepthView); 
			}

			void Device::SetDepthState(ID3D11DepthStencilState* pDepthState) const
			{
				m_pContext->OMSetDepthStencilState(pDepthState, 0);
			}

			void Device::SetBlendState(ID3D11BlendState* pBlendState) const
			{
				float blendFactors[4] = {1.0f, 1.0f, 1.0f, 1.0f};
				m_pContext->OMSetBlendState(pBlendState, blendFactors, 0xffffffff);
			}

			void Device::SetRasterState(ID3D11RasterizerState* pRasterState) const
			{
				m_pContext->RSSetState(pRasterState);
			}

			void Device::SetViewport(const D3D11_VIEWPORT& viewport) const
			{
				if(viewport.Width == 0.0f && viewport.Height == 0.0f)
				{
					D3D11_VIEWPORT port = viewport;
					port.Width = (float)m_vSize.x;
					port.Height = (float)m_vSize.y;
					m_pContext->RSSetViewports(1, &port);
				}
				else
					m_pContext->RSSetViewports(1, &viewport);
			}

			void Device::SetScissorRect(void) const
			{
				D3D11_RECT rScreen = {0, 0, m_vSize.x, m_vSize.y};
				m_pContext->RSSetScissorRects(1, &rScreen);
			}

			void Device::SetScissorRect(const D3D11_RECT& rect) const
			{
				m_pContext->RSSetScissorRects(1, &rect);
			}

			void Device::SetInputLayout(const InputLayout& layout) const
			{
				m_pContext->IASetInputLayout(layout.GetLayout());
			}

			/************************************************
			* Function methods
			************************************************/
			
			void Device::Draw(size_t vertexCount, int vertexStart) const
			{
				m_pContext->Draw(vertexCount, vertexStart);
			}

			void Device::DrawIndexed(size_t indexCount, size_t indexStart, int vertexStart) const
			{
				m_pContext->DrawIndexed(indexCount, indexStart, vertexStart);
			}

			void Device::DrawIndexedInstanced(size_t indicesPerInst, size_t numInstances, size_t indexStart) const
			{
				m_pContext->DrawIndexedInstanced(indicesPerInst, numInstances, indexStart, 0, 0);
			}

			void Device::Clear(ID3D11RenderTargetView* pView, float* pColor) const
			{
				if(!pColor)
				{
					static float color[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
					pColor = color;
				}
					 
				if(pView)
					m_pContext->ClearRenderTargetView(pView, pColor);
				else
					m_pContext->ClearRenderTargetView(m_pBackbufferView, pColor);
			}

			void Device::ClearDepth(const DepthBuffer& depthBuffer) const
			{
				m_pContext->ClearDepthStencilView(depthBuffer.GetView(), D3D11_CLEAR_DEPTH, 1.0f, 0);
			}

			void Device::Present(void) const
			{
				m_pSwapChain->Present(0, 0);
			}

			void Device::Map(ID3D11Resource& resource, D3D11_MAP mapType, D3D11_MAPPED_SUBRESOURCE& mappedResource) const
			{
				m_pContext->Map(&resource, 0, mapType, 0, &mappedResource);
			}

			void Device::Unmap(ID3D11Resource& resource) const
			{
				m_pContext->Unmap(&resource, 0);
			}

			void Device::GenerateMips(ID3D11ShaderResourceView& resource) const
			{
				m_pContext->GenerateMips(&resource);
			}

			ID3D11Device* Device::GetDevice(void) const
			{
				return m_pDevice;
			}

			ID3D11DeviceContext* Device::GetContext(void) const
			{
				return m_pContext;
			}

			const math::Vector2& Device::GetBackbufferSize(void) const
			{
				return m_vSize;
			}

		}
	}
}

#endif
