#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>
#include <vector>
#include "..\Math\Vector.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class VertexBuffer;
			class IndexBuffer;
			class Effect;
			class Texture;
			class DepthBuffer;
			class ConstantBuffer;
			class Sampler;
			class InputLayout;

			class Device
			{
			public:
				Device(const DXGI_SWAP_CHAIN_DESC& description);
				~Device(void);

				void ResizeSwapChain(int x, int y);
				void SetFullscreen(bool isFullscreen);
				bool IsFullscreen(void) const;

				ID3D11Buffer* CreateBuffer(const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA* pInitData) const;
				ID3D11VertexShader* CreateVertexShader(ID3DBlob& vertexBlob) const;
				ID3D11PixelShader* CreatePixelShader(ID3DBlob& pixelBlob, ID3D11ClassLinkage* pLinkage = nullptr) const;
				ID3D11GeometryShader* CreateGeometryShader(ID3DBlob& geometryBlob) const;
				ID3D11InputLayout* CreateInputLayout(D3D11_INPUT_ELEMENT_DESC* pElementDesc, size_t numElements, ID3DBlob& vertexShaderBlob) const;
				ID3D11Texture2D* Create2DTexture(int width, int height, const void* pData, int pitch, DXGI_FORMAT format, D3D11_USAGE usage, UINT bindFlags, UINT cpuFlags, UINT miscFlags) const;
				ID3D11ShaderResourceView* CreateShaderResourceView(ID3D11Resource& resource) const;
				ID3D11DepthStencilView* CreateDepthView(ID3D11Texture2D& texture) const;
				ID3D11SamplerState* CreateSampler(const D3D11_SAMPLER_DESC& desc) const;
				ID3D11RenderTargetView* CreateRenderTargetView(ID3D11Resource& resource) const;
				ID3D11DepthStencilState* CreateDepthState(const D3D11_DEPTH_STENCIL_DESC& desc) const;
				ID3D11BlendState* CreateBlendState(const D3D11_BLEND_DESC& desc) const;
				ID3D11RasterizerState* CreateRasterState(const D3D11_RASTERIZER_DESC& desc) const;
				ID3D11ClassLinkage* CreateClassLinkage(void) const;

				void SetVertexBuffer(unsigned int slot, const VertexBuffer& vertexBuffer, unsigned int offset) const;
				void SetVertexBuffer(unsigned int slot, const std::vector<VertexBuffer*>& vVertexBuffer) const;
				void SetIndexBuffer(const IndexBuffer& indexBuffer) const;
				void SetEffect(const Effect& effect) const;
				void SetTexture(size_t id, const Texture* pTexture) const;
				void SetVertexTexture(size_t id, const Texture* pTexture) const;
				void SetVertexShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const;
				void SetPixelShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const;
				void SetGeometryShaderCBuffer(size_t slot, const ConstantBuffer& cBuffer) const;
				void SetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY topology) const;
				void SetPixelSampler(size_t slot, const Sampler& sampler) const;
				void SetVertexSampler(size_t slot, const Sampler& sampler) const;
				void SetRenderTarget(size_t numTargets, ID3D11RenderTargetView** pRenderTargetViews, ID3D11DepthStencilView* pDepthView) const;
				void SetDepthState(ID3D11DepthStencilState* pDepthState) const;
				void SetBlendState(ID3D11BlendState* pBlendState) const;
				void SetRasterState(ID3D11RasterizerState* pRasterState) const;
				void SetViewport(const D3D11_VIEWPORT& viewport) const;
				void SetScissorRect(void) const;
				void SetScissorRect(const D3D11_RECT& rect) const;
				void SetInputLayout(const InputLayout& layout) const;

				void Draw(size_t vertexCount, int vertexStart) const;
				void DrawIndexed(size_t indexCount, size_t indexStart, int vertexStart) const;
				void DrawIndexedInstanced(size_t indicesPerInst, size_t numInstances, size_t indexStart) const;
				void Clear(ID3D11RenderTargetView* pView = nullptr, float* pColor = nullptr) const;
				void ClearDepth(const DepthBuffer& depthBuffer) const;
				void Map(ID3D11Resource& resource, D3D11_MAP mapType, D3D11_MAPPED_SUBRESOURCE& mappedResource) const;
				void Unmap(ID3D11Resource& resource) const;
				void GenerateMips(ID3D11ShaderResourceView& resource) const;

				void Present(void) const;

				ID3D11Device* GetDevice(void) const;
				ID3D11DeviceContext* GetContext(void) const;
				const math::Vector2& GetBackbufferSize(void) const;

			private:

				Device(const Device& device) {}

				bool m_isFullscreen;
				math::Vector2 m_vSize;

				ID3D11Device* m_pDevice;
				ID3D11DeviceContext* m_pContext;
				ID3D11RenderTargetView* m_pBackbufferView;

				IDXGISwapChain* m_pSwapChain;
				DXGI_FORMAT m_format;

				D3D_FEATURE_LEVEL m_featureLevel;
			};

		}
	}
}

#endif
