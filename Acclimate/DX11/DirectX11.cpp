#include "DirectX11.h"

#ifdef ACL_API_DX11

#include <D3DX11.h>
#include "Device.h"
#include "Sprite.h"
#include "Line.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx11
	{

		DirectX11::DirectX11(HWND hWnd): m_pDevice(nullptr)
		{
			/**********************************************
			* Create DXGI factory for adapter
			**********************************************/
			IDXGIFactory * pFactory = nullptr;
			if(FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory) ,(void**)&pFactory)))
				throw apiException();

			IDXGIAdapter* pAdapter;
			if(FAILED(pFactory->EnumAdapters(0, &pAdapter)))
				throw apiException();
				
			pFactory->Release();

			/**********************************************
			* Enum display modes
			**********************************************/

			IDXGIOutput* pOutput = nullptr;

			if(FAILED(pAdapter->EnumOutputs(0, &pOutput)))
				throw apiException();

			UINT numModes = 0;
			DXGI_MODE_DESC* displayModes = nullptr;
			DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM;

			// get number of modes
			if(FAILED(pOutput->GetDisplayModeList(format, 0, &numModes, nullptr)))
				throw apiException();

			displayModes = new DXGI_MODE_DESC[numModes];

			if(FAILED(pOutput->GetDisplayModeList(format, 0, &numModes, displayModes)))
				throw apiException();

			for(unsigned int i = 0; i < numModes; i++)
			{
				auto& mode = displayModes[i];
				const float refreshRate = mode.RefreshRate.Numerator / (float)mode.RefreshRate.Denominator;
				auto& lastMode = m_vVideoModes.rbegin();
				
				if(!i || lastMode->vSize.x != mode.Width || lastMode->vSize.y != mode.Height || lastMode->refreshRate != refreshRate)
					m_vVideoModes.emplace_back(mode.Width, mode.Height, refreshRate);
			}

			DXGI_MODE_DESC displayMode = displayModes[numModes-1];

			delete[] displayModes;
			
			pAdapter->Release();

			/**********************************************
			* Setup swap chain params
			**********************************************/
			
			DXGI_SWAP_CHAIN_DESC swapDescription = {};

			swapDescription.BufferCount = 1;
			swapDescription.BufferDesc.Width = displayMode.Width;
			swapDescription.BufferDesc.Height = displayMode.Height;
			swapDescription.BufferDesc.Format = displayMode.Format;
			swapDescription.BufferDesc.RefreshRate = displayMode.RefreshRate;
			swapDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swapDescription.OutputWindow = hWnd;
			swapDescription.SampleDesc.Count = 1;
			swapDescription.SampleDesc.Quality = 0;
			swapDescription.Windowed = true;

			/**********************************************
			* Create device
			**********************************************/

			m_pDevice = new d3d::Device(swapDescription);

			/**********************************************
			* Create sprite
			**********************************************/

			m_pSprite = new d3d::Sprite(*m_pDevice);

			/**********************************************
			* Create line
			**********************************************/

			m_pLine = new d3d::Line(*m_pDevice);
		}

		DirectX11::~DirectX11(void)
		{
			delete m_pDevice;
			delete m_pSprite;
		}

		d3d::Device& DirectX11::GetDevice(void) const
		{
			return *m_pDevice;
		}
		
		d3d::Sprite& DirectX11::GetSprite(void) const
		{
			return *m_pSprite;
		}

		d3d::Line& DirectX11::GetLine(void) const
		{
			return *m_pLine;
		}

		const DirectX11::VideoModeVector& DirectX11::GetVideoModes(void) const
		{
			return m_vVideoModes;
		}

		void DirectX11::Present(void) const
		{
			m_pDevice->Present();
		}

		void DirectX11::Reset(void)
		{
			m_pSprite->Reset();
			m_pLine->Reset();
		}

	}
}

#endif
