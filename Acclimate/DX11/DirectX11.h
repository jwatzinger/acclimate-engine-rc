#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <vector>
#include <Windows.h>
#include "..\Gfx\VideoMode.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;
			class Sprite;
			class Line;
		}

		class DirectX11
		{
			typedef std::vector<gfx::VideoMode> VideoModeVector;
		public:
			DirectX11(HWND hWnd);
			~DirectX11(void);

			d3d::Device& GetDevice(void) const;
			d3d::Sprite& GetSprite(void) const;
			d3d::Line& GetLine(void) const;
			const VideoModeVector& GetVideoModes(void) const;

			void Present(void) const;
			void Reset(void);

		private:
		
			DirectX11(const DirectX11&) {}

			void Setup(void);

			d3d::Device* m_pDevice;
			d3d::Sprite* m_pSprite;
			d3d::Line* m_pLine;

			VideoModeVector m_vVideoModes;
		};
	}
}

#endif