#include "Effect.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "InputLayout.h"
#include "ShaderInclude.h"
#include "..\Safe.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"
#include "..\System\Exception.h"
#include "..\System\Convert.h"

//#define REPORT_WARNINGS

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

#pragma warning( disable: 4351 )
			Effect::Effect(const Device& device, const LPCWSTR& lpShaderName, LPCSTR pData, size_t dataLenght, const MacroVector& vPermutations, const ClassVector& vClasses, bool hasGeometryShader) : m_pVertexShader(nullptr), m_pPixelShader(nullptr),
				m_pDevice(&device), m_ppClasses(nullptr), m_numInterfaces(0), m_pLinkage(nullptr), m_pGeometryShader(nullptr), m_cVBuffer(), m_cPBuffer(), m_cGBuffer()
			{
				ID3DBlob* pErrorBlob = nullptr;
				ID3DBlob* pVertexBlob = nullptr;

				ShaderInclude include(lpShaderName);
						
#ifdef _DEBUG
				const unsigned int flags = D3DCOMPILE_DEBUG;
#else
				const unsigned int flags = D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

				/***********************************
				* VertexShader
				***********************************/

				if(FAILED(D3DCompile(pData, dataLenght, conv::ToA(lpShaderName).c_str(), (D3D10_SHADER_MACRO*)&vPermutations[0], &include, "mainVS", "vs_5_0", flags, 0, &pVertexBlob, &pErrorBlob)))
				{
					ACL_ASSERT(pErrorBlob);

					char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, compileErrors);
					pErrorBlob->Release();

					throw apiException();
				}
				else
				{
					m_pVertexShader = device.CreateVertexShader(*pVertexBlob);
					ReflectVertexShader(*pVertexBlob);
					pVertexBlob->Release();
				}

#ifdef REPORT_WARNINGS
				if(pErrorBlob)
				{
					char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
					sys::log->Out(sys::LogModule::API, sys::LogType::WARNING, compileErrors);
					pErrorBlob->Release();
				}
#endif

				/***********************************
				* PixelShader
				***********************************/

				ID3DBlob* pPixelBlob = nullptr;
				
				if(FAILED(D3DCompile(pData, dataLenght, conv::ToA(lpShaderName).c_str(), (D3D10_SHADER_MACRO*)&vPermutations[0], &include, "mainPS", "ps_5_0", flags, 0, &pPixelBlob, &pErrorBlob)))
				{
					ACL_ASSERT(pErrorBlob);

					char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, compileErrors);
					pErrorBlob->Release();

					throw apiException();
				}
				else
				{
					if(!vClasses.empty())
					{
						m_pLinkage = device.CreateClassLinkage();

						for(auto& stClass : vClasses)
						{
							ID3D11ClassInstance* pInst = nullptr;
							m_pLinkage->GetClassInstance(stClass.c_str(), 0, &pInst);

							m_mClassInstances[conv::ToW(stClass)] = pInst;
						}
					}

					m_pPixelShader = device.CreatePixelShader(*pPixelBlob, m_pLinkage);
					ReflectPixelShader(*pPixelBlob);
					pPixelBlob->Release();
				}

#ifdef REPORT_WARNINGS
				if(pErrorBlob)
				{
					char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
					sys::log->Out(sys::LogModule::API, sys::LogType::WARNING, compileErrors);
					pErrorBlob->Release();
				}
#endif

				/***********************************
				* GeometryShader
				***********************************/

				if(hasGeometryShader)
				{
					ID3DBlob* pGeometryBlob = nullptr;

					if(FAILED(D3DCompile(pData, dataLenght, conv::ToA(lpShaderName).c_str(), (D3D10_SHADER_MACRO*)&vPermutations[0], &include, "mainGS", "gs_5_0", flags, 0, &pGeometryBlob, &pErrorBlob)))
					{
						ACL_ASSERT(pErrorBlob);

						char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
						sys::log->Out(sys::LogModule::API, sys::LogType::ERR, compileErrors);
						pErrorBlob->Release();

						throw apiException();
					}
					else
					{
						m_pGeometryShader = device.CreateGeometryShader(*pGeometryBlob);
						ReflectGeometryShader(*pGeometryBlob);
						pGeometryBlob->Release();
					}

#ifdef REPORT_WARNINGS
					if(pErrorBlob)
					{
						char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
						sys::log->Out(sys::LogModule::API, sys::LogType::WARNING, compileErrors);
						pErrorBlob->Release();
					}
#endif
				}

				// select dynamic classes
				size_t slot = 0;
				for(auto& stClass : vClasses)
				{
					if(slot < m_numInterfaces)
						this->SetClassInstance(slot, conv::ToW((stClass).c_str()));
				}
			}

#pragma warning( default: 4351 )

			Effect::~Effect(void)
			{
				SAFE_RELEASE(m_pVertexShader);
				SAFE_RELEASE(m_pPixelShader);
				SAFE_RELEASE(m_pGeometryShader);

				for(size_t i = 0; i < gfx::MAX_CBUFFERS; i++)
				{
					delete m_cVBuffer[i];
					delete m_cPBuffer[i];
					delete m_cGBuffer[i];
				}

				delete[] m_ppClasses;

				for(auto Class : m_mClassInstances)
				{
					SAFE_RELEASE(Class.second);
				}
				SAFE_RELEASE(m_pLinkage);
			}

			void Effect::SetClassInstance(size_t slot, const std::wstring& stName)
			{
				ACL_ASSERT(slot < m_numInterfaces);

				const auto itr = m_mClassInstances.find(stName);
				if(itr != m_mClassInstances.end())
				{
					m_ppClasses[slot] = itr->second;
				}
			}

			ID3D11VertexShader& Effect::GetVertexShader(void) const
			{
				return *m_pVertexShader;
			}

			ID3D11PixelShader& Effect::GetPixelShader(void) const
			{
				return *m_pPixelShader;
			}

			ID3D11GeometryShader* Effect::GetGeometryShader(void) const
			{
				return m_pGeometryShader;
			}

			ID3D11ClassInstance* const* Effect::GetClassInstances(void) const
			{
				return m_ppClasses;
			}

			size_t Effect::GetNumInterfaces(void) const
			{
				return m_numInterfaces;
			}

			ConstantBuffer* Effect::CloneVCBuffer(size_t id) const
			{
				ACL_ASSERT(id < gfx::MAX_CBUFFERS);

				if(auto pBuffer = m_cVBuffer[id])
					return new ConstantBuffer(*pBuffer);
				else 
					return nullptr;
			}

			ConstantBuffer* Effect::ClonePCBuffer(size_t id) const
			{
				ACL_ASSERT(id < gfx::MAX_CBUFFERS);

				if(auto pBuffer= m_cPBuffer[id])
					return new ConstantBuffer(*pBuffer);
				else 
					return nullptr;
			}

			ConstantBuffer* Effect::CloneGCBuffer(size_t id) const
			{
				ACL_ASSERT(id < gfx::MAX_CBUFFERS);

				if(auto pBuffer = m_cGBuffer[id])
					return new ConstantBuffer(*pBuffer);
				else
					return nullptr;
			}

			void Effect::ReflectVertexShader(ID3DBlob& shaderBlob)
			{
				// Reflect shader info
				ID3D11ShaderReflection* pVertexShaderReflection = nullptr;
				if ( FAILED( D3DReflect( shaderBlob.GetBufferPointer(), shaderBlob.GetBufferSize(), IID_ID3D11ShaderReflection, (void**) &pVertexShaderReflection ) ) )
					throw apiException();

				CreateCBufferDummies(*pVertexShaderReflection, m_cVBuffer);

				//Free allocation shader reflection memory
				pVertexShaderReflection->Release();
			}

			void Effect::ReflectPixelShader(ID3DBlob& shaderBlob)
			{
				// Reflect shader info
				ID3D11ShaderReflection* pPixelShaderReflection = nullptr;
				if ( FAILED( D3DReflect( shaderBlob.GetBufferPointer(), shaderBlob.GetBufferSize(), IID_ID3D11ShaderReflection, (void**) &pPixelShaderReflection ) ) )
					throw apiException();

				m_numInterfaces = pPixelShaderReflection->GetNumInterfaceSlots();

				if(m_numInterfaces)
				{
					m_ppClasses = new ID3D11ClassInstance*[m_numInterfaces];
					memset(m_ppClasses, 0, sizeof(ID3D11ClassInstance*)*m_numInterfaces);
				}

				CreateCBufferDummies(*pPixelShaderReflection, m_cPBuffer);

				//Free allocation shader reflection memory
				pPixelShaderReflection->Release();
			}

			void Effect::ReflectGeometryShader(ID3DBlob& shaderBlob)
			{
				// Reflect shader info
				ID3D11ShaderReflection* pGeometryShaderReflection = nullptr;
				if(FAILED(D3DReflect(shaderBlob.GetBufferPointer(), shaderBlob.GetBufferSize(), IID_ID3D11ShaderReflection, (void**)&pGeometryShaderReflection)))
					throw apiException();

				m_numInterfaces = pGeometryShaderReflection->GetNumInterfaceSlots();

				if(m_numInterfaces)
				{
					m_ppClasses = new ID3D11ClassInstance*[m_numInterfaces];
					memset(m_ppClasses, 0, sizeof(ID3D11ClassInstance*)*m_numInterfaces);
				}

				CreateCBufferDummies(*pGeometryShaderReflection, m_cGBuffer);

				//Free allocation shader reflection memory
				pGeometryShaderReflection->Release();
			}

			void Effect::CreateCBufferDummies(ID3D11ShaderReflection& reflection, const d3d::ConstantBuffer** pBuffer)
			{
				// Get shader info
				D3D11_SHADER_DESC shaderDesc;
				reflection.GetDesc( &shaderDesc );

				for(size_t i = 0; i < shaderDesc.ConstantBuffers; i++)
				{
					ID3D11ShaderReflectionConstantBuffer* pConstantReflection = reflection.GetConstantBufferByIndex(i);
					
					D3D11_SHADER_BUFFER_DESC desc;
					pConstantReflection->GetDesc(&desc);

					D3D11_SHADER_INPUT_BIND_DESC bindDesc;
					reflection.GetResourceBindingDescByName(desc.Name, &bindDesc);

					if(bindDesc.BindPoint < gfx::MAX_CBUFFERS)
						pBuffer[bindDesc.BindPoint] = new d3d::ConstantBuffer(*m_pDevice, desc.Size);
				}
			}

		}
	}
}

#endif