#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <vector>
#include <unordered_map>
#include <D3DCompiler.h>
#include <string>
#include "ConstantBuffer.h"
#include "..\Gfx\Defines.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class Effect
			{
				typedef std::unordered_map<std::wstring, ID3D11ClassInstance*> ClassInstanceMap;
			public:
				typedef std::vector<D3D10_SHADER_MACRO> MacroVector;
				typedef std::vector<bool> InstancedVector;
				typedef std::vector<std::string> ClassVector;

				Effect(const Device& device, const LPCWSTR& lpShaderName, LPCSTR pData, size_t dataLenght, const MacroVector& vPermutations, const ClassVector& vClasses, bool hasGeometryShader);
				~Effect(void);

				void SetClassInstance(size_t slot, const std::wstring& stName);

				ID3D11VertexShader& GetVertexShader(void) const;
				ID3D11PixelShader& GetPixelShader(void) const;
				ID3D11GeometryShader* GetGeometryShader(void) const;
				ID3D11ClassInstance* const* GetClassInstances(void) const;
				size_t GetNumInterfaces(void) const;

				ConstantBuffer* CloneVCBuffer(size_t id) const;
				ConstantBuffer* ClonePCBuffer(size_t id) const;
				ConstantBuffer* CloneGCBuffer(size_t id) const;

			private:

				void ReflectVertexShader(ID3DBlob& shaderBlob);
				void ReflectPixelShader(ID3DBlob& shaderBlob);
				void ReflectGeometryShader(ID3DBlob& shaderBlob);
				void CreateCBufferDummies(ID3D11ShaderReflection& reflection, const d3d::ConstantBuffer** pBuffer);
				void CreateSamplerDummies(ID3D11ShaderReflection& reflection);

				const Device* m_pDevice;

				ID3D11VertexShader* m_pVertexShader;
				ID3D11PixelShader* m_pPixelShader;
				ID3D11GeometryShader* m_pGeometryShader;

				const ConstantBuffer* m_cVBuffer[gfx::MAX_CBUFFERS], *m_cPBuffer[gfx::MAX_CBUFFERS], *m_cGBuffer[gfx::MAX_CBUFFERS];

				size_t m_numInterfaces;
				ID3D11ClassLinkage* m_pLinkage;
				ID3D11ClassInstance** m_ppClasses;
				ClassInstanceMap m_mClassInstances;
			};

		}
	}
}

#endif
