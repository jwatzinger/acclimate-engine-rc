#include "Engine.h"

#ifdef ACL_API_DX11

#include "..\Core\Window.h"
#include "..\System\Exception.h"
#include "..\Math\Vector.h"

#include "DirectX11.h"
#include "Sprite.h"
#include "Line.h"
#include "Device.h"
#include "Gfx\RenderStateCache.h"
#include "Gfx\Effect.h"
#include "Gfx\CbufferLoader.h"
#include "Gfx\SpriteBatch.h"
#include "Gfx\Sprite.h"
#include "Gfx\Text.h"
#include "Gfx\Line.h"
#include "Gfx\EffectLoader.h"
#include "Gfx\TextureLoader.h"
#include "Gfx\MaterialLoader.h"
#include "Gfx\MeshLoader.h"
#include "Gfx\FontLoader.h"
#include "Gfx\ZBufferLoader.h"
#include "Gfx\GeometryCreator.h"
#include "Gfx\RenderStateHandler.h"
#include "Render\Renderer.h"
#include "Render\Queue.h"

namespace acl
{
	namespace dx11
	{

		Engine::Engine(HINSTANCE hInstance) : BaseEngine(hInstance), m_pStates(nullptr)
		{
			Queue::Init();
		}

		Engine::~Engine(void)
		{
			delete m_pDirectX11;

			delete m_pStates;
			Effect::pStates = nullptr;
		}

		BaseEngine::VideoModeVector Engine::OnSetupAPI(void)
		{
			try
			{
				m_pDirectX11 = new DirectX11(m_pWindow->GethWnd());
			}
			catch (apiException&)
			{
				return BaseEngine::VideoModeVector();
			}

			const gui::Context& guiContext = m_guiPackage.GetContext();

			AclDevice& device = m_pDirectX11->GetDevice();

			m_pRenderStateHandler = new RenderStateHandlerInstance;

			m_pCbufferLoader = new CbufferLoader(device);
			//setup renderer
			m_pRenderer = new Renderer(*m_pDirectX11, *m_pCbufferLoader);

			// create resource loader
			m_pGeometryCreator = new GeometryCreator(device);
			m_pEffectLoader = new EffectLoader(device, m_effects);
			m_pTextureLoader = new TextureLoader(device, m_textures);
			m_pMeshLoader = new MeshLoader(device, m_textures, m_meshes, *m_pGeometryCreator);
			m_pMaterialLoader = new MaterialLoader(m_effects, m_textures, m_materials);
			m_pFontLoader = new FontLoader(m_pDirectX11->GetSprite(), m_fonts, *m_pTextureLoader);
			m_pZbufferLoader = new ZBufferLoader(device, m_zbuffers);

			// sprite
			m_pSprite = new Sprite(m_pDirectX11->GetSprite(), nullptr, *m_pGeometryCreator);
			m_pSpriteBatch = new SpriteBatch(m_pDirectX11->GetSprite(), m_fonts, *m_pFontLoader, *m_pGeometryCreator);
			m_pText = new Text(m_fonts, nullptr);
			m_pLine = new Line(m_pDirectX11->GetLine(), *m_pGeometryCreator);

			// set state cache
			m_pStates = new RenderStateCache(device);
			Effect::pStates = m_pStates;

			return m_pDirectX11->GetVideoModes();
		}

		bool Engine::OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen)
		{
			auto& device = m_pDirectX11->GetDevice();
			vOldSize = device.GetBackbufferSize();

			if(isFullscreen != device.IsFullscreen())
				device.SetFullscreen(isFullscreen);

			if(vSize != vOldSize)
			{
				device.ResizeSwapChain(vSize.x, vSize.y);
				return true;
			}
			else
				return false;
		}

	}
}

#endif