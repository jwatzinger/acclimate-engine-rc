#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\BaseEngine.h"

namespace acl
{
	namespace dx11
	{
		class DirectX11;
		class RenderStateCache;

		class Engine :
			public BaseEngine
		{
		public:
			Engine(HINSTANCE hInstance);
			~Engine(void);

		private:

			VideoModeVector OnSetupAPI(void) override;
			bool OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen) override;

			DirectX11* m_pDirectX11;
			RenderStateCache* m_pStates;
		};
		
	}
}

#endif