#include "Font.h"

#ifdef ACL_API_DX11

#include "Sprite.h"
#include "..\Math\Rect.h"
#include "..\Gfx\FontFile.h"
#include "..\Gfx\Color.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			Font::Font(Sprite& sprite, const gfx::FontFile& file) : m_file(file), m_pSprite(&sprite)
			{
			}

			const VertexBuffer& Font::GetVertexBuffer(void) const
			{
				return m_pSprite->GetVertexBuffer();
			}

			const gfx::FontFile& Font::GetFile(void) const
			{
				return m_file;
			}

			const float recipColor = 1.0f / 255.0f;

			size_t Font::DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color)
			{
				if(!stText.size())
					return -1;

				const size_t textSize = stText.size();

				const unsigned int fontHeight = m_file.GetFontHeight();

				// horizontal center
				int leftOffset = 0;
				if(dFlags & DT_CENTER)
				{
					const unsigned int textWidth = m_file.CalculateWidth(stText);
					leftOffset = (int)((rect.width - (int)textWidth) * 0.5f);
				}
				else if(dFlags & DT_RIGHT)
				{
					const unsigned int textWidth = m_file.CalculateWidth(stText);
					leftOffset = rect.width - textWidth;
				}

				// vertical center
				int topOffset = 0;
				if(dFlags & DT_VCENTER)
					topOffset = (int)((rect.height - (int)(fontHeight-4)) * 0.5f);
				else if(dFlags & DT_BOTTOM)
					topOffset = rect.height - fontHeight;

				const math::Vector2f& vInvHalfScreenSize = m_pSprite->GetInvHalfScreenSize();

				// vertices
				float leftVertex = (rect.x + leftOffset)  * vInvHalfScreenSize.x - 1.0f;
				const float topVertex = -(rect.y + topOffset) * vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = topVertex - fontHeight * vInvHalfScreenSize.y;

				// color
				const float r = color.r * recipColor;
				const float g = color.g * recipColor;
				const float b = color.b * recipColor;
				const float a = color.a * recipColor;

				SpriteVertex Vertex = { leftVertex, leftVertex, topVertex, bottomVertex, 0.0f, 0.0f, 0.0f, 0.0f, z, r, g, b, a };

				size_t id = 0;
				for(auto glyph : stText)
				{
					const gfx::CharData& charData = m_file.GetCharData(glyph);

					Vertex.rightPos += charData.width * vInvHalfScreenSize.x;
					Vertex.leftCoord = charData.ul;
					Vertex.topCoord = charData.vt;
					Vertex.rightCoord = charData.ur;
					Vertex.bottomCoord = charData.vb;

					id = m_pSprite->Draw(&Vertex, 1);

					Vertex.leftPos = Vertex.rightPos;
				}

				return id-textSize+1;
			}

		}
	}
}

#endif