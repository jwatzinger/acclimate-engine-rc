#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <string>
#include "..\Gfx\FontFile.h"
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

	namespace math
	{
		struct Rect;
	}

	namespace dx11
	{
		namespace d3d
		{

			class Sprite;
			class VertexBuffer;

			class Font
			{
			public:
				Font(Sprite& sprite, const gfx::FontFile& file);

				const VertexBuffer& GetVertexBuffer(void) const;
				const gfx::FontFile& GetFile(void) const;

				size_t DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color);
				
			private:

				Sprite* m_pSprite;

				gfx::FontFile m_file;
			};

		}
	}
}

#endif