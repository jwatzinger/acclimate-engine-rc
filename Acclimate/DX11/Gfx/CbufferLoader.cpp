#include "CbufferLoader.h"

#ifdef ACL_API_DX11

#include "Cbuffer.h"
#include "..\ConstantBuffer.h"

namespace acl
{
	namespace dx11
	{

		CbufferLoader::CbufferLoader(const AclDevice& device): m_pDevice(&device)
		{
		}

		gfx::ICbuffer& CbufferLoader::OnCreateFromSize(size_t numBytes) const
		{
			auto* pDx11Buffer = new d3d::ConstantBuffer(*m_pDevice, numBytes);

			return *new Cbuffer(pDx11Buffer);
		}

		gfx::ICbuffer& CbufferLoader::OnCreateFromBuffer(AclCbuffer& buffer) const
		{
			return *new Cbuffer(&buffer);
		}

	}
}

#endif