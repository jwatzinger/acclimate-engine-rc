#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseCbufferLoader.h"

namespace acl
{
	namespace dx11
	{

		class CbufferLoader final :
			public gfx::BaseCbufferLoader
		{
		public:
			CbufferLoader(const AclDevice& device);

		private:

			gfx::ICbuffer& OnCreateFromSize(size_t numBytes) const override;
			gfx::ICbuffer& OnCreateFromBuffer(AclCbuffer& buffer) const override;

			const AclDevice* m_pDevice;
		};

	}
}

#endif