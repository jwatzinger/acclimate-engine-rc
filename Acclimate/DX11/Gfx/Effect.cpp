#include "Effect.h"

#ifdef ACL_API_DX11

#include "RenderStateCache.h"
#include "..\Effect.h"
#include "..\Render\States.h"
#include "..\..\System\Log.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx11
	{

		void deleteEffect(AclEffect* pEffect)
		{
			delete pEffect;
		}

		Effect::Effect(gfx::EffectFile& file, const gfx::IEffectLoader& loader) : BaseEffect(file, loader, deleteEffect)
		{
		}

		AclCbuffer* Effect::OnCreateVCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.CloneVCBuffer(id);
		}

		AclCbuffer* Effect::OnCreatePCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.ClonePCBuffer(id);
		}

		AclCbuffer* Effect::OnCreateGCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.CloneGCBuffer(id);
		}

		void Effect::SetAlphaState(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend)
		{
			ACL_ASSERT(pStates);

			m_states.Add<AlphaBlendState>(&pStates->GetBlend(bEnable, srcBlend, destBlend, blendFunc, alphaSrcBlend, alphaDestBlend));
		}

		void Effect::SetDepthState(bool bEnable, bool bWrite)
		{
			ACL_ASSERT(pStates);

			m_states.Add<DepthState>(pStates->GetDepth(bEnable, bWrite));
		}

		void Effect::SetRasterizerState(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable)
		{
			ACL_ASSERT(pStates);

			m_states.Add<RasterizerState>(&pStates->GetRasterizer(cull, fill, bScissorEnable));
		}

		void Effect::OnSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			ACL_ASSERT(pStates);

			auto& sampler = pStates->GetSampler(minFilter, magFilter, mipFilter, u, v);

			switch(index)
			{
			case 0:
				m_states.Add<SamplerState0>(sampler);
				break;
			case 1:
				m_states.Add<SamplerState1>(sampler);
				break;
			case 2:
				m_states.Add<SamplerState2>(sampler);
				break;
			case 3:
				m_states.Add<SamplerState3>(sampler);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void Effect::OnVertexSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			ACL_ASSERT(pStates);

			auto& sampler = pStates->GetSampler(minFilter, magFilter, mipFilter, u, v);

			switch(index)
			{
			case 0:
				m_states.Add<VertexSamplerState0>(sampler);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void Effect::OnBindShader(render::StateGroup& group, AclEffect& effect) const
		{
			group.Add<BindShader>(effect);
		}

		void Effect::OnDeleteEffect(AclEffect& effect)
		{
			delete &effect;
		}

		RenderStateCache* Effect::pStates = nullptr;

	}
}

#endif