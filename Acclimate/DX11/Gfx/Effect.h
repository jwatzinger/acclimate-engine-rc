#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseEffect.h"

namespace acl
{
	namespace dx11
	{
		class RenderStateCache;

		class Effect final :
			public gfx::BaseEffect
		{
		public:
			Effect(gfx::EffectFile& file, const gfx::IEffectLoader& loader);

			void SetAlphaState(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend) override;
			void SetDepthState(bool bEnable, bool bWrite) override;
			void SetRasterizerState(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable) override;

			static RenderStateCache* pStates;

		private:

			void OnSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v) override;
			void OnVertexSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v) override;
			void OnBindShader(render::StateGroup& group, AclEffect& effect) const override;
			void OnDeleteEffect(AclEffect& effect) override;
			AclCbuffer* OnCreateVCbuffer(AclEffect& effect, unsigned int id) const override;
			AclCbuffer* OnCreatePCbuffer(AclEffect& effect, unsigned int id) const override;
			AclCbuffer* OnCreateGCbuffer(AclEffect& effect, unsigned int id) const override;
		};

	}
}

#endif