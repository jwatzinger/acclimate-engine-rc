#include "EffectLoader.h"

#ifdef ACL_API_DX11

#include "Effect.h"
#include "..\Effect.h"
#include "..\..\Gfx\EffectFile.h"
#include "..\..\System\Convert.h"

namespace acl
{
	namespace dx11
	{

		EffectLoader::EffectLoader(AclDevice& device, gfx::Effects& effects) : 
			BaseEffectLoader(effects, m_parser), m_pDevice(&device)
		{
		}

		gfx::IEffect& EffectLoader::OnCreateEffect(gfx::EffectFile& file) const
		{
			return *new Effect(file, *this);
		}

		AclEffect& EffectLoader::OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const
		{
			// macro permutations
			const gfx::EffectFile::PermutationVector vPermutationNames = file.GetPermutations(permutation);

			std::vector<std::string> vValues;
			vValues.reserve(vPermutationNames.size());

			d3d::Effect::MacroVector vMacros;
			for(auto& perm : vPermutationNames)
			{
				vValues.push_back(conv::ToStringA(perm.value));
				vMacros.emplace_back(D3D10_SHADER_MACRO{ perm.stName.c_str(), (*vValues.rbegin()).c_str() });
			}

			vMacros.emplace_back(D3D10_SHADER_MACRO{ nullptr, nullptr });

			d3d::Effect::ClassVector vClasses;
			const auto& vLinkage = file.GetLinkage();
			for(auto& link : vLinkage)
			{
				vClasses.push_back("instance" + link.stClassName);
			}

			bool hasGeometryShader = false;
			if(file.GetShaderTypes() == gfx::ShaderTypes::VERTEX_PIXEL_GEOMETRY)
				hasGeometryShader = true;
	
			return *new AclEffect(*m_pDevice, file.GetFilename().c_str(), file.GetData(), file.GetDataLength(), vMacros, vClasses, hasGeometryShader);
		}

	}
}

#endif