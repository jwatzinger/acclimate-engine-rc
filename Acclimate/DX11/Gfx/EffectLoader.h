#pragma once
#include "..\..\Gfx\BaseEffectLoader.h"

#ifdef ACL_API_DX11

#include "HLSL11Parser.h"

namespace acl
{
	namespace dx11
	{

		class EffectLoader final :
			public gfx::BaseEffectLoader
		{
		public:
			EffectLoader(AclDevice& device, gfx::Effects& effects);

		private:

			gfx::IEffect& OnCreateEffect(gfx::EffectFile& file) const override;
			AclEffect& OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const override;

			HLSL11Parser m_parser;
			AclDevice* m_pDevice;
		};

	}
}

#endif