#include "Font.h"

#ifdef ACL_API_DX11

#include "..\Font.h"
#include "..\Render\States.h"
#include "..\..\Gfx\ITexture.h"

namespace acl
{
    namespace dx11
    {

		Font::Font(AclFont& font, const gfx::ITexture& texture) : m_pFont(&font), m_pTexture(&texture)
        {
			m_states.Add<BindVertexBuffer>(&m_pFont->GetVertexBuffer());

			texture.Bind(m_states, gfx::ITexture::BindTarget::PIXEL, 0);
        }

        Font::~Font(void)
        {
            delete m_pFont;
        }

		size_t Font::DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color) const
		{
			return m_pFont->DrawString(rect, z, stText, dFlags, color);
		}

		const render::StateGroup& Font::GetState(void) const
		{
			return m_states;
		}

		const gfx::ITexture& Font::GetTexture(void) const
		{
			return *m_pTexture;
		}

		const gfx::FontFile& Font::GetFontFile(void) const
		{
			return m_pFont->GetFile();
		}

    }
}

#endif