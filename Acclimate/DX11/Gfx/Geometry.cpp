#include "Geometry.h"

#ifdef ACL_API_DX11

#include <d3d11.h>
#include "..\InputLayout.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx11
	{

		LPCSTR convertToSemanticName(gfx::AttributeSemantic semantic);
		DXGI_FORMAT convertToFormat(gfx::AttributeType type, unsigned int numElements);
		D3D_PRIMITIVE_TOPOLOGY primitiveToTopology(gfx::PrimitiveType primitive);

		Geometry::Geometry(const AclDevice& device, gfx::PrimitiveType type, const AttributeVector& vAttributes) : 
			BaseGeometry(type, vAttributes), m_pDevice(&device), m_pLayout(nullptr)
		{
			std::vector<D3D11_INPUT_ELEMENT_DESC> vElements(vAttributes.size());
			unsigned int id = 0;
			for(auto& attribute : vAttributes)
			{
				D3D11_INPUT_ELEMENT_DESC& element = vElements[id];
				element.SemanticName = convertToSemanticName(attribute.semantic);
				element.SemanticIndex = attribute.slot;
				element.Format = convertToFormat(attribute.type, attribute.numElements);
				element.InstanceDataStepRate = attribute.instanceDivisor;
				if(attribute.instanceDivisor)
					element.InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
				else
					element.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				element.InputSlot = attribute.buffer;
				element.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;

				id++;
			}

			m_pLayout = new d3d::InputLayout(device, vElements);
		}

		Geometry::Geometry(const Geometry& geometry): 
			Geometry(*geometry.m_pDevice, geometry.GetPrimitiveType(), geometry.GetAttributes())
		{
		}

		Geometry::~Geometry(void)
		{
			delete m_pLayout;
		}

		gfx::IGeometry& Geometry::Clone(void) const
		{
			return *new Geometry(*this);
		}

		void Geometry::Bind(render::StateGroup& group) const
		{
			group.Add<BindInputLayout>(*m_pLayout);
			group.Add<BindTopology>(primitiveToTopology(GetPrimitiveType()));
		}

		LPCSTR convertToSemanticName(gfx::AttributeSemantic semantic)
		{
			switch(semantic)
			{
			case gfx::AttributeSemantic::POSITION:
				return "SV_POSITION";
			case gfx::AttributeSemantic::TEXCOORD:
				return "TEXCOORD";
			default:
				ACL_ASSERT(false);
				return "";
			}
		}

		DXGI_FORMAT convertToFormat(gfx::AttributeType type, unsigned int numElements)
		{
			switch(type)
			{
			case gfx::AttributeType::FLOAT:
				{
					switch(numElements)
					{
					case 1:
						return DXGI_FORMAT_R32_FLOAT;
					case 2:
						return DXGI_FORMAT_R32G32_FLOAT;
					case 3:
						return DXGI_FORMAT_R32G32B32_FLOAT;
					case 4:
						return DXGI_FORMAT_R32G32B32A32_FLOAT;
					default:
						ACL_ASSERT(false);
					}
				}
			case gfx::AttributeType::UBYTE4:
				{
					ACL_ASSERT(numElements == 4);
					return DXGI_FORMAT_R8G8B8A8_UINT;
				}
			case gfx::AttributeType::SHORT:
				{
					switch(numElements)
					{
					case 1:
						return DXGI_FORMAT_R16_SINT;
					case 2:
						return DXGI_FORMAT_R16G16_SINT;
					case 4:
						return DXGI_FORMAT_R32G32B32A32_SINT;
					default:
						ACL_ASSERT(false);
					}
				}
			default:
				ACL_ASSERT(false);
			}

			return DXGI_FORMAT_UNKNOWN;
		}

		D3D_PRIMITIVE_TOPOLOGY primitiveToTopology(gfx::PrimitiveType primitive)
		{
			switch(primitive)
			{
			case gfx::PrimitiveType::TRIANGLE:
				return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			case gfx::PrimitiveType::TRIANGLE_STRIP:
				return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
			case gfx::PrimitiveType::LINE:
				return D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
			case gfx::PrimitiveType::POINT:
				return D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
			default:
				ACL_ASSERT(false);
			}

			return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		}

	}
}



#endif