#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseGeometry.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class InputLayout;
		}

		class Geometry :
			public gfx::BaseGeometry
		{
		public:
			Geometry(const AclDevice& device, gfx::PrimitiveType type, const AttributeVector& vAttributes);
			Geometry(const Geometry& geometry);
			~Geometry(void);

			gfx::IGeometry& Clone(void) const override;

			void Bind(render::StateGroup& group) const override;

		private:

			d3d::InputLayout* m_pLayout;
			const d3d::Device* m_pDevice;
		};

	}
}

#endif