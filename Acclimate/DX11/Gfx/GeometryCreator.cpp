#include "GeometryCreator.h"

#ifdef ACL_API_DX11

#include "Geometry.h"

namespace acl
{
	namespace dx11
	{

		GeometryCreator::GeometryCreator(const AclDevice& device) : m_pDevice(&device)
		{
		}

		gfx::IGeometry& GeometryCreator::OnCreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes) const
		{
			return *new Geometry(*m_pDevice, type, vAttributes);
		}

	}
}

#endif