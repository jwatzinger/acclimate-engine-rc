#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseGeometryCreator.h"

namespace acl
{
	namespace dx11
	{

		class GeometryCreator :
			public gfx::BaseGeometryCreator
		{
		public:

			GeometryCreator(const AclDevice& device);

		private:

			gfx::IGeometry& OnCreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes) const override;

			const AclDevice* m_pDevice;
		};

	}
}

#endif