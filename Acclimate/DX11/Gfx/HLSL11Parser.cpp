#include "HLSL11Parser.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\Defines.h"
#include "..\..\System\Convert.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx11
	{

		void HLSL11Parser::OnVertexBegin(std::string& stOut) const
		{
			stOut += "#pragma pack_matrix( row_major )\n";
		}

		void HLSL11Parser::OnPixelBegin(std::string& stOut) const
		{
		}

		void HLSL11Parser::OnBeginInputBlock(std::string& stOut, gfx::InputRegister reg, gfx::ShaderType type) const
		{
			stOut += "cbuffer ";

			static const std::string stInstance = conv::ToStringA(gfx::CBUFFER_INSTANCE_ID);
			static const std::string stStage = conv::ToStringA(gfx::CBUFFER_STAGE_ID);

			switch(reg)
			{
			case gfx::InputRegister::INSTANCE:
				stOut += "Instance : register(b" + stInstance + ")\n";
				break;
			case gfx::InputRegister::STAGE:
				stOut += "Stage : register(b" + stStage + ")\n";
				break;
			default:
				ACL_ASSERT(false);
			}
			
			stOut += "{\n";
		}

		void HLSL11Parser::OnInputVariable(std::string& stOut, const std::string& stType, const std::string& stName, gfx::InputRegister reg, unsigned int id) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
		}

		void HLSL11Parser::OnEndInputBlock(std::string& stOut) const
		{
			stOut += "};\n";
		}

		void HLSL11Parser::OnBeginIO(std::string& stOut, gfx::InOut io) const
		{
			// add struct to shader
			if(io == gfx::InOut::I)
				stOut += "struct VERTEX_IN\n{\n";
			else
				stOut += "struct VERTEX_OUT\n{\n";
		}

		void HLSL11Parser::OnBeginPixelOut(std::string& stOut) const
		{
			stOut += "struct PIXEL_OUT\n{\n";
		}

		void HLSL11Parser::OnIOVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id, gfx::InOut io) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
			if(id == 0)
				stOut += ": SV_POSITION0;";
			else
				stOut += ": TEXCOORD" + conv::ToA(conv::ToString(id - 1)) + ';';
		}

		void HLSL11Parser::OnPixelOutVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
			stOut += ": SV_TARGET" + conv::ToA(conv::ToString(id)) + ';';
		}

		void HLSL11Parser::OnEndIO(std::string& stOut) const
		{
			stOut += "};\n\n";
		}

		void HLSL11Parser::OnTexture(std::string& stOut, gfx::TextureType& type, const std::string& stName, unsigned int id) const
		{
			switch(type)
			{
			case gfx::TextureType::_1D:
				stOut += "Texture1D ";
				break;
			case gfx::TextureType::_2D:
				stOut += "Texture2D ";
				break;
			case gfx::TextureType::_3D:
				stOut += "Texture3D ";
				break;
			}

			const std::string stId = conv::ToA(conv::ToString(id));

			stOut += stName;
			stOut += ": register(t" + stId + ")";
			stOut += ";\nsampler ";
			stOut += stName;
			stOut += "Sampler : register(s" + stId + ")";
		}

		std::string HLSL11Parser::OnFunctionCall(const std::string& stName, const ArgumentVector& vArguments) const
		{
			std::string stFunction;
			if(stName == "Sample")
			{
				ACL_ASSERT(vArguments.size() == 2);
				stFunction += vArguments[0];
				stFunction += ".Sample(";
				stFunction += vArguments[0] + "Sampler, " + vArguments[1] + ')';
				return stFunction;
			}
			else if(stName == "SampleLOD")
			{
				ACL_ASSERT(vArguments.size() == 3);
				stFunction += vArguments[0];
				stFunction += ".SampleLevel(";
				stFunction += vArguments[0] + "Sampler, " + vArguments[1] + ", " + vArguments[2] + ')';
				return stFunction;
			}	
			else if(stName == "clip")
			{
				ACL_ASSERT(vArguments.size() == 1);
				stFunction += "clip(" + vArguments[0] + "? -1 : 1)";
				return stFunction;
			}
			else if(stName == "Append")
			{
				ACL_ASSERT(vArguments.size() == 0);
				stFunction += "outputStream.Append(Out)";
				return stFunction;
			}
			else if(stName == "Restart")
			{
				ACL_ASSERT(vArguments.size() == 0);
				stFunction += "outputStream.RestartStrip()";
				return stFunction;
			}
			else
			{
				if(vArguments.size() == 1)
				{
					if(stName == "float2")
					{
						stFunction += "float2(" + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
					else if(stName == "float3")
					{
						stFunction += "float3(" + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
					else if(stName == "float4")
					{
						stFunction += "float4(" + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
				}

				auto pos = stName.find("extentions.");
				if(pos != std::string::npos)
				{
					const std::string stExtention = stName.substr(pos+11);
					stFunction = "extention" + stExtention + "." + stExtention + '(';
				}
				else
					stFunction = stName + "(";
			}
				

			for(auto& stArgument : vArguments)
			{
				stFunction += stArgument + ",";
			}

			if(auto size = vArguments.size())
				stFunction.pop_back();

			stFunction += ")";
			return stFunction;
		}

		std::string HLSL11Parser::OnConvertType(gfx::Type type) const
		{
			switch(type)
			{
			case gfx::Type::OUT_:
				return"Out";
			case gfx::Type::IN_:
				return "In";
			}

			return "";
		}

		void HLSL11Parser::OnExtention(std::string& stOut, const std::string& stFunction, const std::string& stName, unsigned int id) const
		{
			const std::string stInterface = "I" + stName;

			stOut += "interface " + stInterface + "\n{\n";
			const size_t pos = stFunction.find(")");
			stOut += stFunction.substr(0, pos+1) + ";\n};\n\n";

			const std::string stBaseClass = "Base" + stName;
			stOut += "class " + stBaseClass + " : " + stInterface + "\n{\n";
			stOut += stFunction + "\n};\n\n";

			stOut += stInterface + " extention" + stName + ";\n";
			stOut += stBaseClass + " base" + stName + ";\n";

			stOut += "// extention_entry\n"; 
		}

		void HLSL11Parser::OnExtentionBegin(std::string& stOut, const std::string& stName, const std::string& stParent) const
		{
			stOut += "class " + stName + " : I" + stParent + "\n{\nfloat test;";
		}

		void HLSL11Parser::OnExtentionEnd(std::string& stOut, const std::string& stName) const
		{
			stOut += "\n};\n" + stName + " instance" + stName + ";\n";
		}

		void HLSL11Parser::OnMainDeclaration(std::string& stOut) const
		{
			stOut += "VERTEX_OUT mainVS(VERTEX_IN In)";
		}

		void HLSL11Parser::OnMainTop(std::string& stOut) const
		{
			stOut += "VERTEX_OUT Out = (VERTEX_OUT)0;";
		}

		void HLSL11Parser::OnMainBottom(std::string& stOut) const
		{
			stOut += "return Out;\n";
		}

		void HLSL11Parser::OnPixelMainDeclaration(std::string& stOut) const
		{
			if(stOut.find("struct GEOMETRY_OUT\n{\n") != std::string::npos)
				stOut += "PIXEL_OUT mainPS(GEOMETRY_OUT In)";
			else
				stOut += "PIXEL_OUT mainPS(VERTEX_OUT In)";
		}

		void HLSL11Parser::OnPixelMainTop(std::string& stOut) const
		{
			stOut += "PIXEL_OUT Out = (PIXEL_OUT)0;";
		}

		void HLSL11Parser::OnPixelMainBottom(std::string& stOut) const
		{
			stOut += "return Out;\n";
		}

		void HLSL11Parser::OnPostProcess(std::string& stOut) const
		{
		}

		void HLSL11Parser::OnGeometryBegin(std::string& stOut) const
		{
		}

		void HLSL11Parser::OnGeometryPrimitiveIn(gfx::GeometryPrimitiveType type, unsigned int numPrimitives, std::string& stOut) const
		{
			std::string stType;
			switch(type)
			{
			case gfx::GeometryPrimitiveType::TRIANGLE:
				stType = "triangle";
				break;
			case gfx::GeometryPrimitiveType::LINE:
				stType = "line";
				break;
			case gfx::GeometryPrimitiveType::POINT:
				stType = "point";
				break;
			default:
				ACL_ASSERT(false);
			}

			const std::string stNumPrimitives = conv::ToStringA(numPrimitives);

			const std::string stInput = "//geomain" + stType + " VERTEX_OUT In[" + stNumPrimitives + "])";

			stOut += stInput;
		}

		void HLSL11Parser::OnGeometryPrimitiveOut(gfx::GeometryPrimitiveType type, const std::string& stNumPrimitives, std::string& stOut) const
		{
			const std::string stMaxVertexCount = "[maxvertexcount(" + stNumPrimitives + ")]\n";

			const size_t mainPos = stOut.find("//geomain");
			ACL_ASSERT(mainPos != std::string::npos);

			stOut.insert(mainPos, stMaxVertexCount);

			const size_t outPos = mainPos + stMaxVertexCount.size() + 9;

			std::string stOutStream = "inout ";
			switch(type)
			{
			case gfx::GeometryPrimitiveType::TRIANGLE:
				stOutStream += "TriangleStream";
				break;
			case gfx::GeometryPrimitiveType::LINE:
				stOutStream = "LineStream";
				break;
			case gfx::GeometryPrimitiveType::POINT:
				stOutStream = "PointStream";
				break;
			default:
				ACL_ASSERT(false);
			}

			if(stOut.find("struct GEOMETRY_OUT\n{\n") != std::string::npos)
				stOutStream += "<GEOMETRY_OUT> outputStream, ";
			else
				stOutStream += "<VERTEX_OUT> outputStream, ";

			stOut.insert(outPos, stOutStream);
		}

		void HLSL11Parser::OnGeometryMainDeclaration(std::string& stOut) const
		{
			const size_t mainPos = stOut.find("//geomain");
			ACL_ASSERT(mainPos != std::string::npos);

			stOut.erase(mainPos, 9);
			stOut.insert(mainPos, "void mainGS(");
		}

		void HLSL11Parser::OnGeometryMainTop(std::string& stOut) const
		{
			if(stOut.find("struct GEOMETRY_OUT\n{\n") != std::string::npos)
				stOut += "GEOMETRY_OUT Out = (GEOMETRY_OUT)0;";
			else
				stOut += "VERTEX_OUT Out = (VERTEX_OUT)0;";
		}

		void HLSL11Parser::OnBeginGeometryOut(std::string& stOut) const
		{
			stOut += "struct GEOMETRY_OUT\n{\n";
		}

	}
}

#endif
