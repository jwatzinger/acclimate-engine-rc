#include "InstancedMesh.h"

#ifdef ACL_API_DX11

#include "..\Render\States.h"

namespace acl
{
    namespace dx11
    {

		InstancedMesh::InstancedMesh(gfx::IVertexBuffer& buffer, const gfx::IGeometry& geometry, size_t numInstances, IMesh& mesh) : 
			BaseInstancedMesh(buffer, numInstances, mesh, geometry)
        {
			OnChangeInstanceCount(numInstances);
        }

		InstancedMesh::InstancedMesh(const InstancedMesh& mesh) : 
			BaseInstancedMesh(mesh)
		{
			OnChangeInstanceCount(GetInstanceCount());
		}

		gfx::IInstancedMesh& InstancedMesh::Clone(void) const
		{
			return *new InstancedMesh(*this);
		}

		void InstancedMesh::OnChangeInstanceCount(unsigned int count)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				unsigned int vertexCount = GetVertexCount(i);
				SetDrawCall(i, *new DrawIndexedInstancedPrimitives(vertexCount, count, indexOffset));
				indexOffset += vertexCount;
			}
		}

		void InstancedMesh::OnChangeInstanceBuffer(void)
		{

		}

    }
}

#endif