#pragma once
#include "..\..\Gfx\BaseInstancedMesh.h"

#ifdef ACL_API_DX11

namespace acl
{
	namespace dx11
	{

		class InstancedMesh final : 
			public gfx::BaseInstancedMesh
		{
		public:
			InstancedMesh(gfx::IVertexBuffer& buffer, const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh);
			InstancedMesh(const InstancedMesh& instancedMesh);

			gfx::IInstancedMesh& Clone(void) const override;

		private:

			void OnChangeInstanceCount(unsigned int count) override;
			void OnChangeInstanceBuffer(void) override;
		};
	}
}

#endif