#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseLine.h"

namespace acl
{
    namespace dx11
    {

        class Line final :
			public gfx::BaseLine
        {
        public:
            Line(AclLine& line, gfx::IGeometryCreator& creator);
            Line(const Line& line);

            Line& Clone(void) const;

        private:

			bool OnDrawLine(render::Instance& instance, const math::Vector3& vFrom, const math::Vector3& vTo, const gfx::Color& color) override;
			bool OnDrawLines(render::Instance& instance, const gfx::PointVector& vPoints, const gfx::Color& color) override;

            AclLine* m_pLine;
        };

    }
}

#endif