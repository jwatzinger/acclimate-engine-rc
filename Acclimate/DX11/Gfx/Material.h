#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseMaterial.h"

namespace acl
{
	namespace dx11
	{

		class Material final :
			public gfx::BaseMaterial
		{
		public:
			Material(gfx::IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets);

		private:
		};

	}
}

#endif