#include "MaterialLoader.h"

#ifdef ACL_API_DX11

#include "Material.h"

namespace acl
{
	namespace dx11
	{

        MaterialLoader::MaterialLoader(const gfx::Effects& effects, const gfx::Textures& textures, gfx::Materials& materials): 
			BaseMaterialLoader(effects, textures, materials)
        {
        }

		gfx::IMaterial& MaterialLoader::OnCreateMaterial(gfx::IEffect& effect, size_t permutation, size_t numSubsets, size_t id) const
		{
			return *new Material(effect, permutation, id, numSubsets);
		}

	}
}

#endif