#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseMaterialLoader.h"

namespace acl
{
	namespace dx11
	{

		class MaterialLoader final :
			public gfx::BaseMaterialLoader
		{
		public:

            MaterialLoader(const gfx::Effects& effects, const gfx::Textures& textures, gfx::Materials& materials);

		private:

			gfx::IMaterial& OnCreateMaterial(gfx::IEffect& effect, size_t permutation, size_t numSubsets, size_t id) const override;
		};

	}
}

#endif