#include "Mesh.h"

#ifdef ACL_API_DX11

#include "..\Render\States.h"

namespace acl
{
	namespace dx11
	{

		Mesh::Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, const gfx::IGeometry& geometry, gfx::MeshSkeleton* pSkeleton) :
			BaseMesh(vertexBuffer, indexBuffer, isSharedIndexBuffer, vSubsets, 1, geometry, pSkeleton)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				unsigned int vertexCount = GetVertexCount(i);
				SetDrawCall(i, *new const DrawIndexedPrimitives(vertexCount, 0, indexOffset));
				indexOffset += vertexCount;
			}
		}

		void Mesh::OnChangeVertexCount(unsigned int subset)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < subset; i++)
			{
				const unsigned int vertexCount = GetVertexCount(i);
				indexOffset += vertexCount;
			}

			const unsigned int vertexCount = GetVertexCount(subset);
			RemoveDrawCall(subset, true);
			SetDrawCall(subset, *new const DrawIndexedPrimitives(vertexCount, 0, indexOffset));
		}

	}
}

#endif