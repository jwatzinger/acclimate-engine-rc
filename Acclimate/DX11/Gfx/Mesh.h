#pragma once
#include "..\..\Gfx\BaseMesh.h"

#ifdef ACL_API_DX11

namespace acl
{
	namespace dx11
	{

		class Mesh final :
			public gfx::BaseMesh
		{
		public:
			Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, const gfx::IGeometry& geometry, gfx::MeshSkeleton* pSkeleton);

		private:

			void OnChangeVertexCount(unsigned int subset) override;

		};

	}
}

#endif
