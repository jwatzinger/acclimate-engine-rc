#include "MeshBuffer.h"

#ifdef ACL_API_DX11

#include "..\Device.h"
#include "..\VertexBuffer.h"
#include "..\IndexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace dx11
	{
		
		/*******************************
		* VertexBuffer
		********************************/

		VertexBuffer::VertexBuffer(const AclDevice& device, size_t numVertices, size_t stride, const void* pData) :
			BaseVertexBuffer(numVertices, stride), m_pBuffer(nullptr)
		{
			D3D11_BUFFER_DESC vertexBufferDesc;
			vertexBufferDesc.ByteWidth = stride*numVertices;
			vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDesc.MiscFlags = 0;

			if(pData)
			{
				vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
				vertexBufferDesc.CPUAccessFlags = 0;

				D3D11_SUBRESOURCE_DATA initData;
				initData.pSysMem = pData;
				initData.SysMemPitch = 0;
				initData.SysMemSlicePitch = 0;

				m_pBuffer = new d3d::VertexBuffer(device, vertexBufferDesc, initData, stride);
			}
			else
			{
				vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

				m_pBuffer = new d3d::VertexBuffer(device, vertexBufferDesc, stride);
			}
		}

		VertexBuffer::VertexBuffer(const VertexBuffer& buffer) :
			BaseVertexBuffer(buffer)
		{
			m_pBuffer = new d3d::VertexBuffer(*buffer.m_pBuffer);
		}

		VertexBuffer::~VertexBuffer(void)
		{
			delete m_pBuffer;
		}

		gfx::IVertexBuffer& VertexBuffer::Clone(void) const
		{
			return *new VertexBuffer(*this);
		}

		void* VertexBuffer::OnMap(gfx::MapFlags flags)
		{
			D3D11_MAPPED_SUBRESOURCE mapped;

			D3D11_MAP map;
			switch(flags)
			{
			case gfx::MapFlags::READ:
				map = D3D11_MAP_READ;
				break;
			case gfx::MapFlags::READ_WRITE:
				map = D3D11_MAP_READ_WRITE;
				break;
			case gfx::MapFlags::WRITE:
				map = D3D11_MAP_WRITE;
				break;
			case gfx::MapFlags::WRITE_DISCARD:
				map = D3D11_MAP_WRITE_DISCARD;
				break;
			}

			m_pBuffer->Map(mapped, map);
			return mapped.pData;
		}

		void VertexBuffer::OnUnmap(void)
		{
			m_pBuffer->Unmap();
		}

		void VertexBuffer::OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const
		{
			switch(target)
			{
			case gfx::BindTarget::VERTEX:
				group.Add<BindVertexBuffer>(m_pBuffer);
				break;
			case gfx::BindTarget::INSTANCE:
				group.Add<BindInstanceBuffer>(m_pBuffer, offset);
				break;
			}
		}

		/*******************************
		* IndexBuffer
		********************************/

		IndexBuffer::IndexBuffer(const AclDevice& device, size_t numIndices, const unsigned int* pData) :
			BaseIndexBuffer(numIndices), m_pBuffer(nullptr)
		{

			D3D11_BUFFER_DESC indexBufferDesc;
			indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDesc.ByteWidth = numIndices * sizeof(unsigned int);
			indexBufferDesc.MiscFlags = 0;

			if(pData)
			{
				indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
				indexBufferDesc.CPUAccessFlags = 0;

				D3D11_SUBRESOURCE_DATA indexInitData;
				indexInitData.pSysMem = pData;
				indexInitData.SysMemPitch = 0;
				indexInitData.SysMemSlicePitch = 0;

				m_pBuffer = new d3d::IndexBuffer(device, indexBufferDesc, indexInitData);
			}
			else
			{
				indexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
				indexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

				m_pBuffer = new d3d::IndexBuffer(device, indexBufferDesc);
			}
		}

		IndexBuffer::~IndexBuffer(void)
		{
			delete m_pBuffer;
		}

		void IndexBuffer::Bind(render::StateGroup& group) const
		{
			group.Add<BindIndexBuffer>(m_pBuffer);
		}

		unsigned int* IndexBuffer::OnMap(gfx::MapFlags flags)
		{
			D3D11_MAPPED_SUBRESOURCE mapped;

			D3D11_MAP map;
			switch(flags)
			{
			case gfx::MapFlags::READ:
				map = D3D11_MAP_READ;
				break;
			case gfx::MapFlags::READ_WRITE:
				map = D3D11_MAP_READ_WRITE;
				break;
			case gfx::MapFlags::WRITE:
				map = D3D11_MAP_WRITE;
				break;
			case gfx::MapFlags::WRITE_DISCARD:
				map = D3D11_MAP_WRITE_DISCARD;
				break;
			}

			m_pBuffer->Map(mapped, map);
			return (unsigned int*)mapped.pData;
		}

		void IndexBuffer::OnUnmap(void)
		{
			m_pBuffer->Unmap();
		}

	}
}

#endif