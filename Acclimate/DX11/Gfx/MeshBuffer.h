#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseMeshBuffer.h"

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace dx11
	{
		namespace d3d
		{
			class Device;
			class VertexBuffer;
			class IndexBuffer;
		}

		class VertexBuffer :
			public gfx::BaseVertexBuffer
		{
		public:

			VertexBuffer(const d3d::Device& device, size_t numVertices, size_t stride, const void* pData);
			VertexBuffer(const VertexBuffer& buffer);
			~VertexBuffer(void);
			gfx::IVertexBuffer& Clone(void) const override;

		private:

			void* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;
			void OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const override;

			d3d::VertexBuffer* m_pBuffer;
		};

		class IndexBuffer :
			public gfx::BaseIndexBuffer
		{
		public:

			IndexBuffer(const d3d::Device& device, size_t numVertices, const unsigned int* pData);
			~IndexBuffer(void);

			void Bind(render::StateGroup& group) const override;

		private:

			unsigned int* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;

			d3d::IndexBuffer* m_pBuffer;
		};

	}
}

#endif