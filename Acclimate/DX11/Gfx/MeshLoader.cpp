#include "MeshLoader.h"

#ifdef ACL_API_DX11

#include "Mesh.h"
#include "InstancedMesh.h"
#include "MeshBuffer.h"
#include "..\Device.h"
#include "..\VertexBuffer.h"

namespace acl
{
	namespace dx11
	{

		MeshLoader::MeshLoader(const AclDevice& device, const gfx::Textures& textures, gfx::Meshes& meshes, gfx::IGeometryCreator& creator) : 
			BaseMeshLoader(textures, meshes, creator), m_pDevice(&device)
        {
        }

		gfx::IMesh& MeshLoader::OnCreateMesh(const gfx::IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndicies, const unsigned int* pIndices, const SubsetVector& vSubsets, gfx::MeshSkeleton* pSkeleton) const
		{
			const size_t size = geometry.GetVertexSize();

			auto pVertexBuffer = new VertexBuffer(*m_pDevice, numVertices, geometry.GetVertexSize(), pVertices);
			auto pIndexBuffer = new IndexBuffer(*m_pDevice, numIndicies, pIndices);

			return *new Mesh(*pVertexBuffer, *pIndexBuffer, false, vSubsets, geometry, pSkeleton);
		}

		gfx::IMesh& MeshLoader::OnCreateMesh(const gfx::IGeometry& geometry, const VertexVector& vVertices, gfx::IIndexBuffer& indexBuffer) const
		{
			const size_t size = geometry.GetVertexSize();

			const auto stride = geometry.GetVertexSize();
			auto pVertexBuffer = new VertexBuffer(*m_pDevice, vVertices.size() / (stride / sizeof(float)), stride, &vVertices[0]);

			const SubsetVector vSubsets = { indexBuffer.GetSize() };

			return *new Mesh(*pVertexBuffer, indexBuffer, true, vSubsets, geometry, nullptr);
		}

		gfx::IInstancedMesh& MeshLoader::OnCreateInstancedMesh(const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh) const
		{
			auto pInstanceBuffer = new VertexBuffer(*m_pDevice, numInstances, geometry.CalculateVertexSize(1), nullptr);

			return *new InstancedMesh(*pInstanceBuffer, geometry, numInstances, mesh);
		}

		gfx::IIndexBuffer& MeshLoader::Indexbuffer(const IndexVector& vIndices) const
		{
			return *new IndexBuffer(*m_pDevice, vIndices.size(), &vIndices[0]);
		}

	}
}

#endif