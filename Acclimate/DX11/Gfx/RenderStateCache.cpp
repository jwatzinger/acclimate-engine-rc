#include "RenderStateCache.h"
#include "..\Device.h"
#include "..\..\Gfx\IEffect.h"
#include "..\..\System\Log.h"
#include "..\..\System\Assert.h"

#ifdef ACL_API_DX11

namespace acl
{
	namespace dx11
	{

		RenderStateCache::RenderStateCache(const AclDevice& device) : 
			m_pDevice(&device)
		{
		}

		RenderStateCache::~RenderStateCache(void)
		{
			for(auto& sampler : m_mSampler)
			{
				delete sampler.second;
			}

			for(auto& blend : m_mBlends)
			{
				blend.second->Release();
			}

			for(auto& depth : m_mDepths)
			{
				depth.second->Release();
			}

			for(auto& rasterizer : m_mRasterizer)
			{
				rasterizer.second->Release();
			}
		}

		/******************************************
		* Sampler
		******************************************/

		D3D11_FILTER FilterToDX11(gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter)
		{
			switch(minFilter)
			{
			case gfx::TextureFilter::POINT:
				switch(magFilter)
				{
				case gfx::TextureFilter::POINT:
					switch(mipFilter)
					{
					case gfx::MipFilter::POINT:
						return D3D11_FILTER_MIN_MAG_MIP_POINT;
					case gfx::MipFilter::LINEAR:
						return D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
					}
					break;
				case gfx::TextureFilter::LINEAR:
					switch(mipFilter)
					{
					case gfx::MipFilter::POINT:
						return D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT;
					case gfx::MipFilter::LINEAR:
						return D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR;
					}
					break;
				}
				break;
			case gfx::TextureFilter::LINEAR:
				switch(magFilter)
				{
				case gfx::TextureFilter::POINT:
					switch(mipFilter)
					{
					case gfx::MipFilter::POINT:
						return D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT;
					case gfx::MipFilter::LINEAR:
						return D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
					}
					break;
				case gfx::TextureFilter::LINEAR:
					switch(mipFilter)
					{
					case gfx::MipFilter::POINT:
						return D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
					case gfx::MipFilter::LINEAR:
						return D3D11_FILTER_MIN_MAG_MIP_LINEAR;
					}
					break;
				}
				break;
			case gfx::TextureFilter::ANISOTROPIC:
				if(magFilter == gfx::TextureFilter::ANISOTROPIC)
					return D3D11_FILTER_ANISOTROPIC;
				break;
			}

			sys::log->Out(sys::LogModule::API, sys::LogType::ERR, "specified invalid combination of sampler states : ", "min", (int)minFilter, "mag", (int)magFilter, "mip", (int)mipFilter, ".falling back to point - filter");

			return D3D11_FILTER_MIN_MAG_MIP_POINT;
		}

		D3D11_TEXTURE_ADDRESS_MODE AdressToDX11(gfx::AdressMode mode)
		{
			switch(mode)
			{
			case gfx::AdressMode::WRAP:
				return D3D11_TEXTURE_ADDRESS_WRAP;
			case gfx::AdressMode::MIRROR:
				return D3D11_TEXTURE_ADDRESS_MIRROR;
			case gfx::AdressMode::CLAMP:
				return D3D11_TEXTURE_ADDRESS_CLAMP;
			case gfx::AdressMode::BORDER:
				return D3D11_TEXTURE_ADDRESS_BORDER;
			default:
				return D3D11_TEXTURE_ADDRESS_WRAP;
			}
		}

		D3D11_SAMPLER_DESC generateDesc(gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			D3D11_SAMPLER_DESC samplerDesc = {};

			if(mipFilter == gfx::MipFilter::NONE)
			{
				samplerDesc.MaxLOD = 0;
				mipFilter = gfx::MipFilter::POINT;
			}
			else
				samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

			samplerDesc.Filter = FilterToDX11(minFilter, magFilter, mipFilter);
			samplerDesc.AddressU = AdressToDX11(u);
			samplerDesc.AddressV = AdressToDX11(v);
			samplerDesc.AddressW = AdressToDX11(gfx::AdressMode::WRAP);
			samplerDesc.MaxAnisotropy = 16;

			return samplerDesc;
		}

		const d3d::Sampler& RenderStateCache::GetSampler(gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			const unsigned short key = (unsigned short)minFilter + ((unsigned short)magFilter << 2) + ((unsigned short)mipFilter << 4) + ((unsigned short)u << 6) + ((unsigned short)v << 8);
			
			auto itr = m_mSampler.find(key);
			if(itr != m_mSampler.end())
				return *itr->second;
			else
			{
				auto pSampler = new d3d::Sampler(*m_pDevice, generateDesc(minFilter, magFilter, mipFilter, u, v));

				m_mSampler.emplace(key, pSampler);

				return *pSampler;
			}
		}

		/******************************************
		* Blending
		******************************************/

		D3D11_BLEND BlendModeToDX11(gfx::BlendMode mode)
		{
			switch(mode)
			{
			case gfx::BlendMode::ONE:
				return D3D11_BLEND_ONE;
			case gfx::BlendMode::SRCALPHA:
				return D3D11_BLEND_SRC_ALPHA;
			case gfx::BlendMode::INV_SCRALPHA:
				return D3D11_BLEND_INV_SRC_ALPHA;
			case gfx::BlendMode::COLOR:
				return D3D11_BLEND_SRC_COLOR;
			case gfx::BlendMode::INV_COLOR:
				return D3D11_BLEND_INV_SRC_COLOR;
			case gfx::BlendMode::ZERO:
				return D3D11_BLEND_ZERO;
			default:
				ACL_ASSERT(false);
			}

			return D3D11_BLEND_ZERO;
		}

		D3D11_BLEND_OP BlendFuncToDX11(gfx::BlendFunc func)
		{
			switch(func)
			{
			case gfx::BlendFunc::ADD:
				return D3D11_BLEND_OP_ADD;
			case gfx::BlendFunc::SUB:
				return D3D11_BLEND_OP_SUBTRACT;
			default:
				ACL_ASSERT(false);
			}

			return D3D11_BLEND_OP_ADD;
		}

		D3D11_BLEND_DESC generateBlendDesc(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend)
		{
			D3D11_BLEND_DESC blendDesc;
			blendDesc.AlphaToCoverageEnable = false;
			blendDesc.IndependentBlendEnable = false;

			D3D11_RENDER_TARGET_BLEND_DESC& targetDesc = blendDesc.RenderTarget[0];

			targetDesc.BlendEnable = bEnable;
			targetDesc.SrcBlend = BlendModeToDX11(srcBlend);
			targetDesc.SrcBlendAlpha = BlendModeToDX11(alphaSrcBlend);
			targetDesc.DestBlend = BlendModeToDX11(destBlend);
			targetDesc.DestBlendAlpha = BlendModeToDX11(alphaDestBlend);
			targetDesc.BlendOp = BlendFuncToDX11(blendFunc);
			targetDesc.BlendOpAlpha = D3D11_BLEND_OP_ADD;
			targetDesc.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

			return blendDesc;
		}

		ID3D11BlendState& RenderStateCache::GetBlend(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend)
		{
			const unsigned short key = bEnable + ((unsigned short)srcBlend << 1) + ((unsigned short)destBlend << 4) + ((unsigned short)blendFunc << 7) + ((unsigned short)alphaSrcBlend << 8) + ((unsigned short)alphaDestBlend << 11);

			auto itr = m_mBlends.find(key);
			if(itr != m_mBlends.end())
				return *itr->second;
			else
			{
				auto pBlend = m_pDevice->CreateBlendState(generateBlendDesc(bEnable, srcBlend, destBlend, blendFunc, alphaSrcBlend, alphaDestBlend));

				m_mBlends.emplace(key, pBlend);

				return *pBlend;
			}
		}

		/******************************************
		* Depth
		******************************************/

		D3D11_DEPTH_STENCIL_DESC generateDepthDesc(bool bEnable, bool bWrite)
		{
			D3D11_DEPTH_STENCIL_DESC depthDesc = {};

			depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
			depthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
			depthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;

			if(bWrite)
				depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

			depthDesc.DepthEnable = bEnable;

			return depthDesc;
		}

		ID3D11DepthStencilState& RenderStateCache::GetDepth(bool bEnable, bool bWrite)
		{
			const unsigned char key = bEnable + (bWrite << 1);

			auto itr = m_mDepths.find(key);
			if(itr != m_mDepths.end())
				return *itr->second;
			else
			{
				auto pDepth = m_pDevice->CreateDepthState(generateDepthDesc(bEnable, bWrite));

				m_mDepths.emplace(key, pDepth);

				return *pDepth;
			}
		}

		/******************************************
		* Depth
		******************************************/

		D3D11_CULL_MODE CullToDX11(gfx::CullMode cull)
		{
			switch(cull)
			{
			case gfx::CullMode::NONE:
				return D3D11_CULL_NONE;
			case gfx::CullMode::BACK:
				return D3D11_CULL_BACK;
			case gfx::CullMode::FRONT:
				return D3D11_CULL_FRONT;
			default:
				ACL_ASSERT(false);
			}

			return D3D11_CULL_NONE;
		}

		D3D11_FILL_MODE FillToDX11(gfx::FillMode fill)
		{
			switch(fill)
			{
			case gfx::FillMode::SOLID:
				return D3D11_FILL_SOLID;
			case gfx::FillMode::WIREFRAME:
				return D3D11_FILL_WIREFRAME;
			default:
				ACL_ASSERT(false);
			}

			return D3D11_FILL_SOLID;
		}

		D3D11_RASTERIZER_DESC generateRasterizerDesc(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable)
		{
			D3D11_RASTERIZER_DESC desc = {};

			desc.DepthClipEnable = true;
			desc.CullMode = CullToDX11(cull);
			desc.FillMode = FillToDX11(fill);
			desc.ScissorEnable = bScissorEnable;
			
			return desc;
		}

		ID3D11RasterizerState& RenderStateCache::GetRasterizer(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable)
		{
			const unsigned char key = (unsigned char)cull + ((unsigned char)fill << 2) + (bScissorEnable << 3);

			auto itr = m_mRasterizer.find(key);
			if(itr != m_mRasterizer.end())
				return *itr->second;
			else
			{
				auto pRaster = m_pDevice->CreateRasterState(generateRasterizerDesc(cull, fill, bScissorEnable));

				m_mRasterizer.emplace(key, pRaster);

				return *pRaster;
			}
		}

	}
}

#endif