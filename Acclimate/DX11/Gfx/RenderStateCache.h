#pragma once
#include "..\..\ApiDef.h"
#include "..\Sampler.h"
#include <unordered_map>

#ifdef ACL_API_DX11

namespace acl
{
	namespace gfx
	{
		enum class TextureFilter;
		enum class MipFilter;
		enum class AdressMode;
		enum class BlendMode;
		enum class BlendFunc;
		enum class CullMode;
		enum class FillMode;
	}

	namespace dx11
	{

		class RenderStateCache
		{
			typedef std::unordered_map<unsigned short, d3d::Sampler*> SamplerMap;
			typedef std::unordered_map<unsigned short, ID3D11BlendState*> BlendMap;
			typedef std::unordered_map<unsigned char, ID3D11DepthStencilState*> DepthMap;
			typedef std::unordered_map<unsigned char, ID3D11RasterizerState*> RasterizerMap;
		public:
			RenderStateCache(const AclDevice& device);
			~RenderStateCache(void);

			const d3d::Sampler& GetSampler(gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v);
			ID3D11BlendState& GetBlend(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend);
			ID3D11DepthStencilState& GetDepth(bool bEnable, bool bWrite);
			ID3D11RasterizerState& GetRasterizer(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable);

		private:

			const AclDevice* m_pDevice;

			SamplerMap m_mSampler;
			BlendMap m_mBlends;
			DepthMap m_mDepths;
			RasterizerMap m_mRasterizer;
		};

	}
}


#endif