#include "SpriteBatch.h"

#ifdef ACL_API_DX11

#include "..\Sprite.h"
#include "..\Render\States.h"
#include "..\..\Gfx\ITexture.h"
#include "..\..\Gfx\IEffect.h"
#include "..\..\Render\Instance.h"
#include "..\..\Render\States.h"
#include "..\..\Render\IStage.h"

namespace acl
{
	namespace dx11
	{

		SpriteBatch::SpriteBatch(d3d::Sprite& sprite, const gfx::Fonts& fonts, const gfx::IFontLoader& loader, gfx::IGeometryCreator& creator) : 
			BaseSpriteBatch(fonts, loader, creator), m_pSprite(&sprite)
		{
			// set geometry resource binds
			m_states.Add<BindVertexBuffer>(&m_pSprite->GetVertexBuffer());

			SetClipRect(nullptr);
		}

		void SpriteBatch::SetClipRect(const math::Rect* pClip)
		{
			if(pClip)
			{
				if(m_rClip != *pClip)
					Submit();

				D3D11_RECT r = { pClip->x, pClip->y, pClip->x + pClip->width, pClip->y + pClip->height };
				m_states.Add<SetScissorRect>(r);
			}
			else
				m_states.Add<SetScissorRect>();
		}

		void SpriteBatch::OnPrepareExecute(void)
		{
			m_pSprite->Map();
		}


		size_t SpriteBatch::OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle, const gfx::Color& color) const
		{
			return m_pSprite->Draw(vTextureSize, vPosition, vOrigin, rSrcRect, vScale, angle, color);
		}

		size_t SpriteBatch::OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle, const gfx::Color& color) const
		{
			return m_pSprite->Draw(vPosition, vOrigin, vScale, angle, color);
		}

		void SpriteBatch::OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance)
		{
			instance.CreateCall<DrawPrimitives>(numIndices, startIndex);
		}

		void SpriteBatch::OnFinishExecute(void)
		{
			m_pSprite->Unmap();
		}

	}
}

#endif