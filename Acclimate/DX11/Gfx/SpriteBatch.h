#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include <vector>
#include "..\..\Gfx\BaseSpriteBatch.h"

namespace acl
{
	namespace dx11
	{

		class SpriteBatch final :
			public gfx::BaseSpriteBatch
		{
		public:
			SpriteBatch(d3d::Sprite& sprite, const gfx::Fonts& fonts, const gfx::IFontLoader& loader, gfx::IGeometryCreator& creator);

			void SetClipRect(const math::Rect* pClip) override;

		protected:

			void OnPrepareExecute(void) override;
			size_t OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle, const gfx::Color& color) const override;
			size_t OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle, const gfx::Color& color) const override;
			void OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance) override;
			void OnFinishExecute(void) override;

		private:

			d3d::Sprite* m_pSprite;

			math::Rect m_rClip;
		};

	}
}

#endif
