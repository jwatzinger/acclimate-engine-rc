#include "Text.h"

#ifdef ACL_API_DX11

#include "..\Font.h"
#include "..\Render\States.h"
#include "..\..\Gfx\IFont.h"
#include "..\..\Gfx\IEffect.h"
#include "..\..\Math\Rect.h"
#include "..\..\Render\IStage.h"
#include "..\..\Render\Instance.h"
#include "..\..\System\Convert.h"

namespace acl
{
    namespace dx11
    {

        Text::Text(const gfx::Fonts& fonts, const gfx::IEffect* pEffect) : m_pFonts(&fonts), m_pEffect(pEffect), m_pActiveFont(nullptr)
        {
			SetClipRect(nullptr);
        }

		void Text::Draw(render::IStage& stage, const math::Rect& rect, const std::wstring& lpText, unsigned long dFlags, float f)
        {
			if(!m_pEffect)
				return;

			if(!m_pActiveFont)
				SetFontProperties(16, L"Arial");

			const std::wstring stText(lpText);

			if(!stText.size())
				return;
			// TODO: use z
			size_t id = m_pActiveFont->DrawString(rect, 0.0f, stText, dFlags, m_color);

			// create instance
			render::Key64 key;
            key.bits = 0;

			const render::StateGroup* groups[3] = { &m_states, &m_pEffect->GetState(), &m_pActiveFont->GetState() };

            render::Instance instance(key, groups, 3);
			instance.CreateCall<DrawIndexedPrimitives>(6 * stText.size(), id * 4, 0);
            stage.SubmitInstance(instance);
        }
        
        void Text::SetClipRect(const math::Rect* pClip)
        {
            if(pClip)
            {
				D3D11_RECT r = { pClip->x, pClip->y, pClip->x + pClip->width, pClip->y + pClip->height };
	            m_states.Add<SetScissorRect>(r);
            }
            else
            {
	            m_states.Add<SetScissorRect>();
            }
        }

		void Text::SetFontProperties(int fontSize, const std::wstring& stFontString)
        {
			m_pActiveFont = m_pFonts->Get(stFontString + conv::ToString(fontSize));
        }

        void Text::SetColor(const gfx::Color& color)
        {
	        m_color = color;
        }

		void Text::SetEffect(const gfx::IEffect& effect)
		{
			m_pEffect = &effect;
		}

    }
}

#endif