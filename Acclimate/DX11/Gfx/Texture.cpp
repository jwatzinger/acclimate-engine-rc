#include "Texture.h"

#ifdef ACL_API_DX11

#include "..\Texture.h"
#include "..\Device.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx11
	{

		gfx::TextureFormats ConvertFormat(DXGI_FORMAT format)
		{
		    switch(format)
		    {
			case DXGI_FORMAT_B8G8R8X8_UNORM:
				return gfx::TextureFormats::X32;
            case DXGI_FORMAT_R8G8B8A8_UNORM:
			    return gfx::TextureFormats::A32;
            case DXGI_FORMAT_R16G16B16A16_FLOAT:
			    return gfx::TextureFormats::A16F;
			case DXGI_FORMAT_R32G32B32A32_FLOAT:
				return gfx::TextureFormats::A32F;
			case DXGI_FORMAT_R8_UNORM:
				return gfx::TextureFormats::L8;
			case DXGI_FORMAT_R16_UNORM:
				return gfx::TextureFormats::L16;
            case DXGI_FORMAT_R32_FLOAT:
                return gfx::TextureFormats::R32;
			case DXGI_FORMAT_R16G16_FLOAT:
				return gfx::TextureFormats::GR16F;
			case DXGI_FORMAT_R16_FLOAT:
				return gfx::TextureFormats::R16F;
            case DXGI_FORMAT_UNKNOWN:
				return gfx::TextureFormats::UNKNOWN;
            default:
                return gfx::TextureFormats::A32;
		    }
		}

		void deleteTexture(AclTexture* pTexture)
		{
			delete pTexture;
		}

		Texture::Texture(AclTexture& texture) :
			BaseTexture(texture, texture.GetSize(), ConvertFormat(texture.GetFormat()), deleteTexture)
		{
		}

		void Texture::Bind(render::StateGroup& states, BindTarget target, unsigned int slot) const
		{
			switch(target)
			{
			case BindTarget::PIXEL:
				{
					switch(slot)
					{
					case 0:
						states.Add<BindTexture0>(m_pTexture);
						break;
					case 1:
						states.Add<BindTexture1>(m_pTexture);
						break;
					case 2:
						states.Add<BindTexture2>(m_pTexture);
						break;
					case 3:
						states.Add<BindTexture3>(m_pTexture);
						break;
					default:
						ACL_ASSERT(false);
					}
					break;
				}
			case BindTarget::VERTEX:
				{
					switch(slot)
					{
					case 0:
						states.Add<BindVertexTexture0>(m_pTexture);
						break;
					default:
						ACL_ASSERT(false);
					}
					break;
				}
			default:
				ACL_ASSERT(false);
			}
		}

		void Texture::OnResize(const math::Vector2& vSize)
		{
			m_pTexture->Reload(vSize.x, vSize.y);
		}

		void Texture::OnMap(gfx::MapFlags flags, gfx::Mapped& mapped) const
		{
			D3D11_MAP type;

			switch(flags)
			{
			case gfx::MapFlags::READ:
				type = D3D11_MAP_READ;
				break;
			case gfx::MapFlags::WRITE:
				type = D3D11_MAP_WRITE;
				break;
			case gfx::MapFlags::READ_WRITE:
				type = D3D11_MAP_READ_WRITE;
				break;
			case gfx::MapFlags::WRITE_DISCARD:
				type = D3D11_MAP_WRITE_DISCARD;
				break;
			}

			D3D11_MAPPED_SUBRESOURCE mappedSubresource;
			m_pTexture->Map(mappedSubresource, type);

			mapped.pData = mappedSubresource.pData;
			mapped.pitch = mappedSubresource.RowPitch;
		}

		void Texture::OnUnmap(void) const
		{
			m_pTexture->Unmap();
		}

	}
}

#endif