#include "TextureLoader.h"

#ifdef ACL_API_DX11

#include "Texture.h"
#include "..\Device.h"
#include "..\Texture.h"
#include "..\..\System\Exception.h"
#include "..\..\System\Log.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx11
	{

        DXGI_FORMAT GetFormat(gfx::TextureFormats format)
		{
		    switch(format)
		    {
			case gfx::TextureFormats::X32:
				return DXGI_FORMAT_B8G8R8X8_UNORM;
            case gfx::TextureFormats::A32:
			    return DXGI_FORMAT_R8G8B8A8_UNORM;
            case gfx::TextureFormats::A16F:
				return DXGI_FORMAT_R16G16B16A16_FLOAT;
			case gfx::TextureFormats::A32F:
				return DXGI_FORMAT_R32G32B32A32_FLOAT;
			case gfx::TextureFormats::L8:
				return DXGI_FORMAT_R8_UNORM;
			case gfx::TextureFormats::L16:
				return DXGI_FORMAT_R16_UNORM;
            case gfx::TextureFormats::R32:
                return DXGI_FORMAT_R32_FLOAT;
			case gfx::TextureFormats::GR16F:
				return DXGI_FORMAT_R16G16_FLOAT;
			case gfx::TextureFormats::R16F:
				return DXGI_FORMAT_R16_FLOAT;
            case gfx::TextureFormats::UNKNOWN:
				return DXGI_FORMAT_UNKNOWN;
            default:
                return DXGI_FORMAT_R8G8B8A8_UNORM;
		    }
		}

		int toSize(gfx::TextureFormats format)
		{
			switch(format)
			{
			case gfx::TextureFormats::X32:
				return 4;
			case gfx::TextureFormats::A32:
				return 4;
			case gfx::TextureFormats::A16F:
				return 8;
			case gfx::TextureFormats::A32F:
				return 16;
			case gfx::TextureFormats::L8:
				return 1;
			case gfx::TextureFormats::L16:
				return 2;
			case gfx::TextureFormats::R32:
				return 4;
			case gfx::TextureFormats::GR16F:
				return 4;
			case gfx::TextureFormats::R16F:
				return 2;
			case gfx::TextureFormats::UNKNOWN:
				return 0;
			default:
				return 0;
			}
		}

        TextureLoader::TextureLoader(const AclDevice& device, gfx::Textures& textures): BaseTextureLoader(textures), m_pDevice(&device)
        {
        }

		gfx::ITexture& TextureLoader::OnLoadTexture(const std::wstring& stFilename, bool bRead, gfx::TextureFormats format) const
		{
			d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, stFilename.c_str(), bRead, GetFormat(format));
			return *new Texture(d3dTexture);
		}

		gfx::ITexture& TextureLoader::OnCreateTexture(const math::Vector2& vSize, gfx::TextureFormats format, gfx::LoadFlags flags) const
		{
			bool bTarget = false, bWrite = false, bMips = false;
			switch(flags)
			{
			case gfx::LoadFlags::RENDER_TARGET:
				bTarget = true;
				break;
			case gfx::LoadFlags::CPU_WRITE:
				bWrite = true;
				break;
			case gfx::LoadFlags::RENDER_TARGET_MIPS:
				bTarget = true;
				bMips = true;
				break;
			}

			d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, vSize.x, vSize.y, GetFormat(format), bTarget, bWrite, bMips);
			return *new Texture(d3dTexture);
		}

		gfx::ITexture& TextureLoader::OnCreateTexture(const math::Vector2& vSize, const void* pData, gfx::TextureFormats format, gfx::LoadFlags flags) const
		{
			bool bTarget = false, bWrite = false, bMips = false;
			switch(flags)
			{
			case gfx::LoadFlags::RENDER_TARGET:
				bTarget = true;
				break;
			case gfx::LoadFlags::CPU_WRITE:
				bWrite = true;
				break;
			case gfx::LoadFlags::RENDER_TARGET_MIPS:
				bTarget = true;
				bMips = true;
				break;
			}

			int pitch = 0;
			if(pData)
				pitch = toSize(format) * vSize.x;

			d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, vSize.x, vSize.y, pData, pitch, GetFormat(format), bTarget, bWrite, bMips);
			return *new Texture(d3dTexture);
		}

		D3DX11_IMAGE_FILE_FORMAT formatToDX11(gfx::FileFormat format)
		{
			switch(format)
			{
			case gfx::FileFormat::DDS:
				return D3DX11_IFF_DDS;
			case gfx::FileFormat::PNG:
				return D3DX11_IFF_PNG;
			default:
				ACL_ASSERT(false);
			}	

			return D3DX11_IFF_DDS;
		}

		bool TextureLoader::OnSaveTexture(AclTexture& texture, const std::wstring& stFilename, gfx::FileFormat format) const
		{
			const auto fmt = formatToDX11(format);
			auto hr = D3DX11SaveTextureToFile(m_pDevice->GetContext(), texture.GetResource(), fmt, stFilename.c_str());
			
			return SUCCEEDED(hr);
		}

	}
}

#endif