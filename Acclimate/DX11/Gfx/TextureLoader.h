#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\..\Gfx\BaseTextureLoader.h"

namespace acl
{
	namespace dx11
	{

		class TextureLoader final :
			public gfx::BaseTextureLoader
		{
		public:
			TextureLoader(const AclDevice& device, gfx::Textures& textures);

		private:

			gfx::ITexture& OnLoadTexture(const std::wstring& stFilename, bool bRead, gfx::TextureFormats format) const override;
			gfx::ITexture& OnCreateTexture(const math::Vector2& vSize, gfx::TextureFormats format, gfx::LoadFlags flags) const override;
			gfx::ITexture& OnCreateTexture(const math::Vector2& vSize, const void* pData, gfx::TextureFormats format, gfx::LoadFlags flags) const override;

			bool OnSaveTexture(AclTexture& texture, const std::wstring& stFilename, gfx::FileFormat format) const override;

			const AclDevice* m_pDevice;
		};

	}
}

#endif