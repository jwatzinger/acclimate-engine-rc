#include "ZBufferLoader.h"

#ifdef ACL_API_DX11

#include "ZBuffer.h"
#include "..\DepthBuffer.h"
#include "..\..\Math\Vector.h"
#include "..\..\System\Log.h"

namespace acl
{
	namespace dx11
	{

		ZBufferLoader::ZBufferLoader(AclDevice& device, gfx::ZBuffers& zBuffers): BaseZBufferLoader(zBuffers),
			m_device(device)
		{
		}

		gfx::IZBuffer& ZBufferLoader::OnCreateZBuffer(const math::Vector2& vSize) const
		{
			AclDepth* pDx11DepthBuffer = new AclDepth(m_device, vSize.x, vSize.y);

			return *new ZBuffer(*pDx11DepthBuffer);
		}

	}
}

#endif