#include "IndexBuffer.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "..\Safe.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			IndexBuffer::IndexBuffer(const Device& device, ID3D11Buffer& indexBuffer): m_pIndexBuffer(&indexBuffer), m_pDevice(&device), m_bMapped(false)
			{
			}

			IndexBuffer::IndexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc): m_pIndexBuffer(nullptr), m_pDevice(&device), m_bMapped(false)
			{
				m_pIndexBuffer = device.CreateBuffer(bufferDesc, nullptr);
			}

			IndexBuffer::IndexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA& initData): m_pIndexBuffer(nullptr),
				m_pDevice(&device), m_bMapped(false)
			{
				m_pIndexBuffer = device.CreateBuffer(bufferDesc, &initData);
			}

			IndexBuffer::~IndexBuffer(void)
			{
				SAFE_RELEASE(m_pIndexBuffer);
			}

			ID3D11Buffer& IndexBuffer::GetBuffer(void) const
			{
				return *m_pIndexBuffer;
			}

			void IndexBuffer::Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP flags)
			{
				if(!m_bMapped)
				{
					m_pDevice->Map(*m_pIndexBuffer, flags, map);
					m_bMapped = true;
				}
			}

			void IndexBuffer::Unmap(void)
			{
				if(m_bMapped)
				{
					m_pDevice->Unmap(*m_pIndexBuffer);
					m_bMapped = false;
				}
			}

		}
	}
}

#endif