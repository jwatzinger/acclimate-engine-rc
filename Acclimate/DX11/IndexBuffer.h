#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class IndexBuffer
			{
			public:
				IndexBuffer(const Device& device, ID3D11Buffer& indexBuffer);
				IndexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc);
				IndexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA& initData);
				~IndexBuffer(void);

				ID3D11Buffer& GetBuffer(void) const;

				void Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP flags);
				void Unmap(void);

			private:

				bool m_bMapped;

				const Device* m_pDevice;

				ID3D11Buffer* m_pIndexBuffer;
			};

		}
	}
}

#endif