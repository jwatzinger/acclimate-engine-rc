#include "InputLayout.h"

#ifdef ACL_API_DX11

#include <D3DCompiler.h>
#include "Device.h"
#include "..\Safe.h"
#include "..\System\Convert.h"
#include "..\System\Log.h"
#include "..\System\Assert.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			ID3DBlob& generateDummyShader(const InputLayout::InputElementVector& vElements);

			InputLayout::InputLayout(const Device& device, InputElementVector& vElements) : m_pLayout(nullptr)
			{
				auto& blob = generateDummyShader(vElements);
				m_pLayout = device.CreateInputLayout(&vElements[0], vElements.size(), blob);
				blob.Release();
			}

			InputLayout::InputLayout(const Device& device, D3D11_INPUT_ELEMENT_DESC* pElements, size_t numElements) : InputLayout(device, InputElementVector(pElements, pElements+numElements))
			{
			}

			InputLayout::InputLayout(InputLayout&& layout) : m_pLayout(layout.m_pLayout)
			{
				layout.m_pLayout = nullptr;
			}

			void InputLayout::operator=(InputLayout&& layout)
			{
				if(&layout != this)
				{
					m_pLayout->Release();
					m_pLayout = layout.m_pLayout;
					layout.m_pLayout = nullptr;
				}
			}

			InputLayout::~InputLayout(void)
			{
				SAFE_RELEASE(m_pLayout);
			}

			ID3DBlob& generateDummyShader(const InputLayout::InputElementVector& vElements)
			{
				std::string stShader = "struct VERTEX_IN\n{\n";
				size_t numElement = 0;
				for(auto& element : vElements)
				{
					switch(element.Format)
					{
					case DXGI_FORMAT_R32G32B32A32_FLOAT:
						stShader += "float4 ";
						break;
					case DXGI_FORMAT_R32G32B32_FLOAT:
						stShader += "float3 ";
						break;
					case DXGI_FORMAT_R32G32_FLOAT:
						stShader += "float2 ";
						break;
					case DXGI_FORMAT_R32_FLOAT:
						stShader += "float ";
						break;
					case DXGI_FORMAT_R8G8B8A8_UINT:
						stShader += "uint4 ";
						break;
					case DXGI_FORMAT_R16G16B16A16_SINT:
						stShader += "float4 ";
						break;
					case DXGI_FORMAT_R16G16_SINT:
						stShader += "float2 ";
						break;
					case DXGI_FORMAT_R16_SINT:
						stShader += "float ";
						break;
					default:
						ACL_ASSERT(false);
					}

					const std::string stNum = conv::ToA(conv::ToString(numElement));

					stShader += "element" + stNum + ": ";
					stShader += element.SemanticName;

					const std::string stSlot = conv::ToA(conv::ToString(element.SemanticIndex));
					stShader += stSlot + ";\n";

					numElement++;
				}

				stShader += "};\nstruct VERTEX_OUT\n{\nfloat4 vPos : SV_POSITION0;\n};\nVERTEX_OUT mainTest(VERTEX_IN i)\n{\nVERTEX_OUT o = (VERTEX_OUT)0;\nreturn o;\n}";

				ID3DBlob* pBlob = nullptr, *pErrorBlob = nullptr;
				if(FAILED(D3DCompile(stShader.c_str(), stShader.size(), nullptr, nullptr, nullptr, "mainTest", "vs_5_0", 0, 0, &pBlob, &pErrorBlob)))
				{
					if(pErrorBlob)
					{
						char* compileErrors = (char*)(pErrorBlob->GetBufferPointer());
						sys::log->Out(sys::LogModule::API, sys::LogType::ERR, compileErrors);
 						pErrorBlob->Release();
					}
					else
						sys::log->Out(sys::LogModule::API, sys::LogType::ERR, "Effect file not found.");

					throw apiException();
				}

				return *pBlob;
			}


		}
	}
}

#endif