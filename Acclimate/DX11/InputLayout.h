#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>
#include <vector>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class InputLayout
			{
			public:

				typedef std::vector<D3D11_INPUT_ELEMENT_DESC> InputElementVector;
				
				InputLayout(const Device& device, InputElementVector& vElements);
				InputLayout(const Device& device, D3D11_INPUT_ELEMENT_DESC* pElements, size_t numElements);
				InputLayout(InputLayout&& layout);
				~InputLayout(void);

				void operator=(InputLayout&& layout);

				inline ID3D11InputLayout* GetLayout(void) const
				{
					return m_pLayout;
				}

			private:

				ID3D11InputLayout* m_pLayout;
			};

		}
	}
}

#endif