#include "Line.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "Texture.h"
#include "VertexBuffer.h"
#include "..\Gfx\Color.h"
#include "..\Math\Vector3.h"
#include "..\Math\Vector2f.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			struct LineVertex
			{
				float x, y, z;
				float r, g, b, a;
			};

			Line::Line(Device& device) : m_nextFreeId(0), m_pDevice(&device),
				m_numLines(10000)
			{
				D3D11_BUFFER_DESC vertexBufferDesc = {};

				vertexBufferDesc.Usage				= D3D11_USAGE_DYNAMIC;
				vertexBufferDesc.ByteWidth			= sizeof(LineVertex)* m_numLines * 2;
				vertexBufferDesc.BindFlags			= D3D11_BIND_VERTEX_BUFFER;
				vertexBufferDesc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
				vertexBufferDesc.MiscFlags			= 0;
				m_pVertexBuffer = new VertexBuffer(device, vertexBufferDesc, sizeof( LineVertex ));

				const math::Vector2& vBackbufferSize = device.GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			Line::Line(const Line& sprite) : m_nextFreeId(0), m_vInvHalfScreenSize(sprite.m_vInvHalfScreenSize), 
				m_pDevice(sprite.m_pDevice), m_numLines(sprite.m_numLines)
			{
				D3D11_BUFFER_DESC vertexBufferDesc = {};

				vertexBufferDesc.Usage				= D3D11_USAGE_DYNAMIC;
				vertexBufferDesc.ByteWidth			= sizeof(LineVertex)* m_numLines * 2;
				vertexBufferDesc.BindFlags			= D3D11_BIND_VERTEX_BUFFER;
				vertexBufferDesc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
				vertexBufferDesc.MiscFlags			= 0;
				m_pVertexBuffer = new VertexBuffer(*m_pDevice, vertexBufferDesc, sizeof( LineVertex ));
			}

			Line::~Line(void)
			{
				delete m_pVertexBuffer;
			}

			void Line::Reset(void)
			{
				D3D11_MAPPED_SUBRESOURCE mappedRes;
				m_pVertexBuffer->Map(mappedRes, D3D11_MAP_WRITE_DISCARD);
				m_pVertexBuffer->Unmap();

				m_nextFreeId = 0;

				const math::Vector2& vBackbufferSize = m_pDevice->GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			void Line::Map(void)
			{
				m_pVertexBuffer->Map(m_mapped, D3D11_MAP_WRITE_NO_OVERWRITE);
			}

			void Line::Unmap(void)
			{
				m_pVertexBuffer->Unmap();
			}

			size_t Line::Draw(const math::Vector3& vStart, const math::Vector3& vEnd, const gfx::Color& color)
			{
				const float leftVertex = vStart.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float rightVertex = vEnd.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float topVertex = -vStart.y * m_vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = -vEnd.y * m_vInvHalfScreenSize.y + 1.0f;

				const float invColor = 1.0f / 255.0f;

				const float r = color.r * invColor;
				const float g = color.g * invColor;
				const float b = color.b * invColor;
				const float a = color.a * invColor;

				LineVertex Vertices[] =
				{
						{ leftVertex, topVertex, vStart.z, r, g, b, a},
						{ rightVertex, bottomVertex, vEnd.z, r, g, b, a },
				};

				LineVertex* pBuffer = static_cast<LineVertex*>(m_mapped.pData);
				memcpy(pBuffer + m_nextFreeId * 2, Vertices, sizeof(LineVertex)* 2);

				return m_nextFreeId++;
			}

			const VertexBuffer& Line::GetVertexBuffer(void) const
			{
				return *m_pVertexBuffer;
			}

			const math::Vector2f& Line::GetInvHalfScreenSize(void) const
			{
				return m_vInvHalfScreenSize;
			}

		}
	}
}

#endif