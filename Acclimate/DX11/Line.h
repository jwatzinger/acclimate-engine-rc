#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <D3D11.h>
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

    namespace math
    {
        struct Vector3;
    }

	namespace dx11
	{
		namespace d3d
		{

			class Device;
			class VertexBuffer;

			class Line
			{
			public:
				Line(Device& device);
				Line(const Line& sprite);
				~Line(void);

				void Reset(void);

				void Map(void);
				void Unmap(void);

				size_t Draw(const math::Vector3& vStart, const math::Vector3& vEnd, const gfx::Color& color);

				const VertexBuffer& GetVertexBuffer(void) const;
				const math::Vector2f& GetInvHalfScreenSize(void) const;

			private:

				D3D11_MAPPED_SUBRESOURCE m_mapped;
			
				size_t m_nextFreeId, m_numLines;
				math::Vector2f m_vInvHalfScreenSize;

				VertexBuffer* m_pVertexBuffer;
				Device* m_pDevice;

			};

		}
	}
}

#endif