#include "Queue.h"

#ifdef ACL_API_DX11

#include <algorithm>
#include "StateGuards.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx11
    {

#define GUARDED
#ifdef GUARDED
#define EXECUTE_GUARDED(pCmds, type, guardType)	{ \
														const type& command = *reinterpret_cast<const type*>(pCmds); \
														static guardType guard; \
														if(guard.Compare(command)) \
															command.Execute(*m_pDevice); \
														break; \
													} 
#else
#define EXECUTE_GUARDED(pCmds, type, guardType)	{ \
														const type& command = *reinterpret_cast<const type*>(pCmds); \
														command.Execute(*m_pDevice); \
														break; \
													} 
#endif

#define EXECUTE(pCmds, type)	{ \
											const type& command = *reinterpret_cast<const type*>(pCmds); \
											command.Execute(*m_pDevice); \
											break; \
										}

        void Queue::Init(void)
        {
	        BindVertexBuffer::type();
			BindInstanceBuffer::type();
	        BindIndexBuffer::type();
			BindInputLayout::type();
			BindTopology::type();
	        BindTexture0::type();
	        BindTexture1::type();
	        BindTexture2::type();
	        BindTexture3::type();
			BindVertexTexture0::type();
	        BindShader::type();
			SamplerState0::type();
			SamplerState1::type();
			SamplerState2::type();
			SamplerState3::type();
			VertexSamplerState0::type();
			BindRenderTargets::type();
			DepthState::type();
			AlphaBlendState::type();
			RasterizerState::type();
			SetViewport::type();
			SetScissorRect::type();
			BindVCBuffer0::type();
			BindVCBuffer1::type();
			BindPCBuffer0::type();
			BindPCBuffer1::type();
			BindGCBuffer0::type();
			BindGCBuffer1::type();

			// draw calls
			DrawIndexedPrimitives::type();
			ClearTargets::type();
			DrawIndexedInstancedPrimitives::type();
			DrawPrimitives::type();	
			GenerateMips::type();	
        }

		Queue::Queue(d3d::Device& device) : m_pDevice(&device)
        {
        }

		enum class StateType { VertexBuffer, InstanceBuffer, IndexBuffer, InputLayout, Topology, Texture0, Texture1, Texture2, Texture3, VertexTexture0, Shader, Sampler0, Sampler1, Sampler2, Sampler3, VertexSampler0, RenderTargets, Depth, AlphaBlend, RasterState, Viewport, ScissorRect, VCbuffer0, VCbuffer1, PCbuffer0, PCbuffer1,GCbuffer0, GCbuffer1 };
		enum class CallType { Indexed, Clear, IndexedInstanced, Primitives, Mips };

        void Queue::OnExecuteSorted(const InstanceVector& vInstances) const
        {
	        //iteratre through render instances
			for(auto& instance : vInstances)
	        {
		        //store pointer to instance and state groups
				const char* pStream = (char*)instance.first->GetStateStream();

		        unsigned int previousApplied = 0;

				unsigned int offset = 0;
		        //iterate through instance state groups
				for(unsigned int i = 0; i < instance.first->GetStateCount(); i++)
		        {
					const unsigned int commandCount = *reinterpret_cast<const unsigned int*>(pStream+offset);

			        //store pointer to state group and state pointer
			        const char* pvCmds = pStream+offset+sizeof(unsigned int);
					offset += commandCount*render::STATE_SIZE+sizeof(unsigned int);

			        //iterate through command group
			        for(unsigned int j = 0; j < commandCount; j++)
			        {
				        //get state type
				        const render::BaseState::Type type = *reinterpret_cast<const render::BaseState::Type*>(pvCmds);
						pvCmds = pvCmds + sizeof(render::BaseState::Type);

						const auto shifted = 1U << type;
				        //ignore if same command was applied earlier on stack
						const bool bApplied = (previousApplied & shifted) == shifted;

						if(!bApplied)
						{
							//set applied status to skip any further commands of this type
							previousApplied |= shifted;

							//switch on type
							switch((StateType)type)
							{
							case StateType::VertexBuffer:
								EXECUTE_GUARDED(pvCmds, BindVertexBuffer, VertexBufferGuard);
							case StateType::InstanceBuffer:
								EXECUTE(pvCmds, BindInstanceBuffer);
							case StateType::IndexBuffer:
								EXECUTE_GUARDED(pvCmds, BindIndexBuffer, IndexBufferGuard);
							case StateType::InputLayout:
								EXECUTE_GUARDED(pvCmds, BindInputLayout, InputLayoutGuard);
							case StateType::Topology:
								EXECUTE_GUARDED(pvCmds, BindTopology, TopologyGuard);
							case StateType::Texture0:
								EXECUTE_GUARDED(pvCmds, BindTexture0, Texture0Guard);
							case StateType::Texture1:
								EXECUTE_GUARDED(pvCmds, BindTexture1, Texture1Guard);
							case StateType::Texture2:
								EXECUTE_GUARDED(pvCmds, BindTexture2, Texture2Guard);
							case StateType::Texture3:
								EXECUTE_GUARDED(pvCmds, BindTexture3, Texture3Guard);
							case StateType::VertexTexture0:
								EXECUTE_GUARDED(pvCmds, BindVertexTexture0, VertexTexture0Guard);
							case StateType::Shader:
								EXECUTE_GUARDED(pvCmds, BindShader, ShaderGuard);
							case StateType::Sampler0:
								EXECUTE_GUARDED(pvCmds, SamplerState0, Sampler0Guard);
							case StateType::Sampler1:
								EXECUTE_GUARDED(pvCmds, SamplerState1, Sampler1Guard);
							case StateType::Sampler2:
								EXECUTE_GUARDED(pvCmds, SamplerState2, Sampler2Guard);
							case StateType::Sampler3:
								EXECUTE_GUARDED(pvCmds, SamplerState3, Sampler3Guard);
							case StateType::VertexSampler0:
								EXECUTE_GUARDED(pvCmds, VertexSamplerState0, VertexSampler0Guard);
							case StateType::RenderTargets:
								EXECUTE_GUARDED(pvCmds, BindRenderTargets, RenderTargetGuard);
							case StateType::Depth:
								EXECUTE_GUARDED(pvCmds, DepthState, DepthGuard);
							case StateType::AlphaBlend:
								EXECUTE_GUARDED(pvCmds, AlphaBlendState, AlphaGuard);
							case StateType::RasterState:
								EXECUTE_GUARDED(pvCmds, RasterizerState, RasterizerGuard);
							case StateType::Viewport:
								EXECUTE_GUARDED(pvCmds, SetViewport, ViewportGuard);
							case StateType::ScissorRect:
								EXECUTE(pvCmds, SetScissorRect);
							case StateType::VCbuffer0:
								EXECUTE_GUARDED(pvCmds, BindVCBuffer0, VCGuard0);
							case StateType::VCbuffer1:
								EXECUTE_GUARDED(pvCmds, BindVCBuffer1, VCGuard1);
							case StateType::PCbuffer0:
								EXECUTE_GUARDED(pvCmds, BindPCBuffer0, PCGuard0);
							case StateType::PCbuffer1:
								EXECUTE_GUARDED(pvCmds, BindPCBuffer1, PCGuard1);
							case StateType::GCbuffer0:
								EXECUTE_GUARDED(pvCmds, BindGCBuffer0, GCGuard0);
							case StateType::GCbuffer1:
								EXECUTE_GUARDED(pvCmds, BindGCBuffer1, GCGuard1);
							default:
								ACL_ASSERT(false);
							}
						}

						pvCmds = pvCmds + render::STATE_SIZE-sizeof(unsigned int);
			        }
		        }

		        //get draw call pointer and type
		        const render::BaseDrawCall* pDrawCall = instance.first->GetDrawCall();

		        if(pDrawCall)
		        {
					const CallType type((CallType)pDrawCall->GetType());

			        //switch on call type
			        switch(type)
			        {
					case CallType::Indexed:
				        {
					        const DrawIndexedPrimitives& drawIndexed = *static_cast<const DrawIndexedPrimitives*>(pDrawCall);
					        drawIndexed.Execute(*m_pDevice);
					        break;
				        }
					case CallType::Clear:
				        {
					        const ClearTargets& clear = *static_cast<const ClearTargets*>(pDrawCall);
					        clear.Execute(*m_pDevice);
					        break;
				        }
					case CallType::IndexedInstanced:
				        {
					        const DrawIndexedInstancedPrimitives& drawInst = *static_cast<const DrawIndexedInstancedPrimitives*>(pDrawCall);
					        drawInst.Execute(*m_pDevice);
					        break;
				        }
					case CallType::Primitives:
				        {
					        const DrawPrimitives& draw = *static_cast<const DrawPrimitives*>(pDrawCall);
					        draw.Execute(*m_pDevice);
					        break;
				        }
					case CallType::Mips:
				        {
					        const GenerateMips& draw = *static_cast<const GenerateMips*>(pDrawCall);
					        draw.Execute(*m_pDevice);
					        break;
				        }
			        }
		        }
	        }
        }
    }
}

#undef EXECUTE_GUARDED
#endif