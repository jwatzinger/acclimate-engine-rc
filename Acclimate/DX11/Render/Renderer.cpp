#include "Renderer.h"

#ifdef ACL_API_DX11

#include "Queue.h"
#include "Stage.h"
#include "..\DirectX11.h"
#include "..\..\Render\RendererInternalAccessor.h"

namespace acl
{
    namespace dx11
    {

        Renderer::Renderer(DirectX11& direct3D, const gfx::ICbufferLoader& loader): m_pDirectX11(&direct3D), m_pCbufferLoader(&loader)
        {
        }

		void Renderer::GetDevice(render::RendererInternalAccessor& accessor) const
		{
			return accessor.SetDevice(&m_pDirectX11->GetDevice());
		}

		render::IStage& Renderer::OnCreateStage(unsigned int id, render::IQueue& queue, unsigned int flags) const
		{
			return *new Stage(id, queue, flags, *m_pCbufferLoader);
		}

		render::IQueue& Renderer::OnCreateQueue(void) const
		{
			return *new Queue(m_pDirectX11->GetDevice());
		}

        void Renderer::Prepare(void)
        {
        }

		void Renderer::Finish(void)
		{
			m_pDirectX11->Present();
		}

		void Renderer::OnExecute(void)
		{
			m_pDirectX11->Reset();
		}
        
    }
}

#endif