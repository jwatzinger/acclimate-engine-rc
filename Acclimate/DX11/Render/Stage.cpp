#include "Stage.h"

#ifdef ACL_API_DX11

#include "States.h"
#include "..\..\Math\Rect.h"
#include "..\..\Render\Instance.h"

#undef nullptr

namespace acl
{
    namespace dx11
    {

        Stage::Stage(unsigned int id, render::IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader): 
			BaseStage(id, queue, flags, loader)
        {
			m_stageStates.Add<BindRenderTargets>(nullptr, 0, nullptr);

        }

		void Stage::OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture)
        {
			UpdateClearCall();
        }

		void Stage::OnChangeDepthBuffer(const AclDepth* pDepth)
        {
			UpdateClearCall();
        }

		void Stage::OnChangeViewport(const math::Rect& rect)
		{
			D3D11_VIEWPORT viewport;
			viewport.TopLeftX = (float)rect.x;
			viewport.TopLeftY = (float)rect.y;
			viewport.Width = (float)rect.width;
			viewport.Height = (float)rect.height;
			viewport.MinDepth = 0.0f;
			viewport.MaxDepth = 1.0f;
			m_stageStates.Add<dx11::SetViewport>(viewport);
		}

		void Stage::UpdateClearCall(void)
		{
			const size_t flags = GetFlags();

			auto vTextures = GetTextures();
			auto pDepthBuffer = GetDepthbuffer();

			m_stageStates.Add<BindRenderTargets>(&vTextures[0], render::MAX_RENDERTARGETS, pDepthBuffer);

			if((flags & render::CLEAR) || (flags & render::CLEAR_Z))
			{
				unsigned int target = 0;
				if(flags & render::CLEAR)
				{
					for(auto pTexture : vTextures)
					{
						if(pTexture)
							target += 1;
					}
					if(!target)
						target = 1;
				}

				auto& clearColor = GetClearColor();
				const float color[4] = { clearColor.r, clearColor.g, clearColor.b, clearColor.a };
				this->SetDrawCall(*new ClearTargets(&vTextures[0], target, color, pDepthBuffer));
			}
		}

		void Stage::OnChangeClearColor(const gfx::FColor& color)
		{
			UpdateClearCall();
		}

        bool Stage::OnGenerateMips(render::Instance& instance) const
        {
			auto vTextures = GetTextures();
			if(vTextures[0])
			{
				instance.CreateCall<GenerateMips>(*vTextures[0]);
				return true;
			}

			return false;
        }

    }
}

#endif