#pragma once
#include "States.h"
#include "..\..\Render\StateGuard.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Texture;
		}

		/****************************************
		* Vertex
		****************************************/

		class VertexBufferGuard :
			public render::StateGuard
		{
		public: 

			VertexBufferGuard(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindVertexBuffer& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = buffer.pVertexBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::VertexBuffer* pBuffer;
		};

		/****************************************
		* Index
		****************************************/

		class IndexBufferGuard :
			public render::StateGuard
		{
		public: 

			IndexBufferGuard(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindIndexBuffer& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = buffer.pIndexBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::IndexBuffer* pBuffer;
		};

		/****************************************
		* InputLayout
		****************************************/

		class InputLayoutGuard :
			public render::StateGuard
		{
		public:

			InputLayoutGuard(void) : pLayout(nullptr)
			{
			}

			bool Compare(const BindInputLayout& layout)
			{
				auto pOld = pLayout;
				pLayout = layout.pLayout;
				return pLayout != pOld;
			}

		private:

			const d3d::InputLayout* pLayout;
		};

		/****************************************
		* Topology
		****************************************/

		class TopologyGuard :
			public render::StateGuard
		{
		public:

			TopologyGuard(void) : topology(D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED)
			{
			}

			bool Compare(const BindTopology& bindTopology)
			{
				const auto old = topology;
				topology = bindTopology.topology;
				return topology != old;
			}

		private:

			D3D11_PRIMITIVE_TOPOLOGY topology;
		};

		/****************************************
		* Texture0
		****************************************/

		class Texture0Guard :
			public render::StateGuard
		{
		public: 

			Texture0Guard(void): pTexture(nullptr) 
			{
			}

			bool Compare(const BindTexture0& texture)
			{
				auto pOld = pTexture;
				pTexture = texture.pTexture;
				return pTexture != pOld;
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture1
		****************************************/

		class Texture1Guard :
			public render::StateGuard
		{
		public: 

			Texture1Guard(void): pTexture(nullptr) 
			{
			}

			bool Compare(const BindTexture1& texture)
			{
				auto pOld = pTexture;
				pTexture = texture.pTexture;
				return pTexture != pOld;
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture2
		****************************************/

		class Texture2Guard :
			public render::StateGuard
		{
		public: 

			Texture2Guard(void): pTexture(nullptr) 
			{
			}

			bool Compare(const BindTexture2& texture)
			{
				auto pOld = pTexture;
				pTexture = texture.pTexture;
				return pTexture != pOld;
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture3
		****************************************/

		class Texture3Guard :
			public render::StateGuard
		{
		public: 

			Texture3Guard(void): pTexture(nullptr) 
			{
			}

			bool Compare(const BindTexture3& texture)
			{
				auto pOld = pTexture;
				pTexture = texture.pTexture;
				return pTexture != pOld;
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* VertexTexture0
		****************************************/

		class VertexTexture0Guard :
			public render::StateGuard
		{
		public:

			VertexTexture0Guard(void) : pTexture(nullptr)
			{
			}

			bool Compare(const BindVertexTexture0& texture)
			{
				auto pOld = pTexture;
				pTexture = texture.pTexture;
				return pTexture != pOld;
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Sampler0
		****************************************/

		class Sampler0Guard :
			public render::StateGuard
		{
		public: 

			Sampler0Guard(void): pSampler(nullptr) 
			{
			}

			bool Compare(const SamplerState0& state)
			{
				auto pOld = pSampler;
				pSampler = &state.sampler;
				return pSampler != pOld;
			}

		private:

			const d3d::Sampler* pSampler;
		};

		/****************************************
		* Sampler1
		****************************************/

		class Sampler1Guard :
			public render::StateGuard
		{
		public: 

			Sampler1Guard(void): pSampler(nullptr) 
			{
			}

			bool Compare(const SamplerState1& state)
			{
				auto pOld = pSampler;
				pSampler = &state.sampler;
				return pSampler != pOld;
			}

		private:

			const d3d::Sampler* pSampler;
		};

		/****************************************
		* Sampler2
		****************************************/

		class Sampler2Guard :
			public render::StateGuard
		{
		public:

			Sampler2Guard(void) : pSampler(nullptr)
			{
			}

			bool Compare(const SamplerState2& state)
			{
				auto pOld = pSampler;
				pSampler = &state.sampler;
				return pSampler != pOld;
			}

		private:

			const d3d::Sampler* pSampler;
		};

		/****************************************
		* Sampler3
		****************************************/

		class Sampler3Guard :
			public render::StateGuard
		{
		public:

			Sampler3Guard(void) : pSampler(nullptr)
			{
			}

			bool Compare(const SamplerState3& state)
			{
				auto pOld = pSampler;
				pSampler = &state.sampler;
				return pSampler != pOld;
			}

		private:

			const d3d::Sampler* pSampler;
		};

		/****************************************
		* VertexSampler0
		****************************************/

		class VertexSampler0Guard :
			public render::StateGuard
		{
		public:

			VertexSampler0Guard(void) : pSampler(nullptr)
			{
			}

			bool Compare(const VertexSamplerState0& state)
			{
				auto pOld = pSampler;
				pSampler = &state.sampler;
				return pSampler != pOld;
			}

		private:

			const d3d::Sampler* pSampler;
		};

		/****************************************
		* Depth
		****************************************/

		class DepthGuard :
			public render::StateGuard
		{
		public: 

			DepthGuard(void): pState(nullptr) 
			{
			}

			bool Compare(const DepthState& state)
			{
				auto pOld = pState;
				pState = state.pDepthState;
				return pState != pOld;
			}

		private:

			const ID3D11DepthStencilState* pState;
		};

		/****************************************
		* Alpha
		****************************************/

		class AlphaGuard :
			public render::StateGuard
		{
		public: 

			AlphaGuard(void): pState(nullptr) 
			{
			}

			bool Compare(const AlphaBlendState& state)
			{
				auto pOld = pState;
				pState = state.pBlendState;
				return pState != pOld;
			}

		private:

			const ID3D11BlendState* pState;
		};

		/****************************************
		* Rasterizer
		****************************************/

		class RasterizerGuard :
			public render::StateGuard
		{
		public: 

			RasterizerGuard(void): pState(nullptr) 
			{
			}

			bool Compare(const RasterizerState& state)
			{
				auto pOld = pState;
				pState = state.pRasterState;
				return pState != pOld;
			}

		private:

			const ID3D11RasterizerState* pState;
		};

		/****************************************
		* RenderTargetGuard
		****************************************/

		class RenderTargetGuard :
			public render::StateGuard
		{
		public: 

			bool Compare(const BindRenderTargets& state)
			{
				return true;
			}
		};

		/****************************************
		* Viewport
		****************************************/

		class ViewportGuard :
			public render::StateGuard
		{
		public: 

			bool Compare(const SetViewport& set)
			{
				auto old = port;
				port = set.viewport;
				return (old.Width == 0.0f || old.Height == 0.0f) || old != port;
			}

		private:

			D3D11_VIEWPORT port;
		};

		/****************************************
		* Shader
		****************************************/

		class ShaderGuard :
			public render::StateGuard
		{
		public: 

			ShaderGuard(void): pEffect(nullptr) 
			{
			}

			bool Compare(const BindShader& shader)
			{
				auto pOld = pEffect;
				pEffect = shader.pEffect;
				return pEffect != pOld;
			}

		private:

			const d3d::Effect* pEffect;
		};

		/****************************************
		* ScissorRect
		****************************************/

		class ScissorGuard :
			public render::StateGuard
		{
		public:

			bool Compare(const SetScissorRect& scissor)
			{
				auto old = rect;
				rect = scissor.scissorRect;
				auto bOld = bSet;
				bSet = scissor.bSet;
				return bSet != bOld || rect != old;
			}

		private:

			D3D11_RECT rect;
			bool bSet;
		};

		/****************************************
		* VB0
		****************************************/

		class VCGuard0 :
			public render::StateGuard
		{
		public: 

			VCGuard0(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindVCBuffer0& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* VB1
		****************************************/

		class VCGuard1 :
			public render::StateGuard
		{
		public: 

			VCGuard1(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindVCBuffer1& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* PC0
		****************************************/

		class PCGuard0 :
			public render::StateGuard
		{
		public: 

			PCGuard0(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindPCBuffer0& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* PC1
		****************************************/

		class PCGuard1 :
			public render::StateGuard
		{
		public: 

			PCGuard1(void): pBuffer(nullptr) 
			{
			}

			bool Compare(const BindPCBuffer1& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* GC0
		****************************************/

		class GCGuard0 :
			public render::StateGuard
		{
		public:

			GCGuard0(void) : pBuffer(nullptr)
			{
			}

			bool Compare(const BindGCBuffer0& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* GC1
		****************************************/

		class GCGuard1 :
			public render::StateGuard
		{
		public:

			GCGuard1(void) : pBuffer(nullptr)
			{
			}

			bool Compare(const BindGCBuffer1& buffer)
			{
				auto pOld = pBuffer;
				pBuffer = &buffer.cBuffer;
				return pBuffer != pOld;
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

	}
}