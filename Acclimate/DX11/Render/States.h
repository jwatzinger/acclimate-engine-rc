#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX11

#include "..\Device.h"
#include "..\Texture.h"
#include "..\DepthBuffer.h"
#include "..\..\Render\States.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class VertexBuffer;
			class VertexDeclaration;
			class IndexBuffer;
			class Texture;
		}

		/****************************************
		* Vertex
		****************************************/

		class BindVertexBuffer : 
			public render::State<BindVertexBuffer>
		{
		public:	

			BindVertexBuffer(const d3d::VertexBuffer* vertexBuffer): 
				pVertexBuffer(vertexBuffer)
			{}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetVertexBuffer(0, *pVertexBuffer, 0);
			};	
	
			const d3d::VertexBuffer* pVertexBuffer;
		};

		/****************************************
		* Instance
		****************************************/

		class BindInstanceBuffer : 
			public render::State<BindInstanceBuffer>
		{
		public:	

			BindInstanceBuffer(const d3d::VertexBuffer* vertexBuffer, unsigned int offset): 
				pVertexBuffer(vertexBuffer), offset(offset)
			{}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetVertexBuffer(1, *pVertexBuffer, offset);
			};	

		private:	
	
			const d3d::VertexBuffer* pVertexBuffer;
			unsigned int offset;
		};

		/****************************************
		* Index buffer
		****************************************/

		class BindIndexBuffer : 
			public render::State<BindIndexBuffer>
		{
		public:	

			BindIndexBuffer(const d3d::IndexBuffer* indexBuffer): pIndexBuffer(indexBuffer) {}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetIndexBuffer(*pIndexBuffer);
			};	

			const d3d::IndexBuffer* pIndexBuffer;

		};

		/****************************************
		* Input layout
		****************************************/

		class BindInputLayout :
			public render::State<BindInputLayout>
		{
		public:

			BindInputLayout(const d3d::InputLayout& layout) : pLayout(&layout) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetInputLayout(*pLayout);
			};

			const d3d::InputLayout* pLayout;
		};

		/****************************************
		* Topology
		****************************************/

		class BindTopology :
			public render::State<BindTopology>
		{
		public:

			BindTopology(D3D11_PRIMITIVE_TOPOLOGY topology) : topology(topology) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPrimitiveTopology(topology);
			};

			const D3D11_PRIMITIVE_TOPOLOGY topology;
		};

		/****************************************
		* Texture 0
		****************************************/

		class BindTexture0 : 
			public render::State<BindTexture0>
		{
		public:

			BindTexture0(const d3d::Texture* pTexture): pTexture(pTexture)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(0, pTexture);
			}

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 1
		****************************************/

		class BindTexture1 : 
			public render::State<BindTexture1>
		{
		public:

			BindTexture1(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(1, pTexture);
			}

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 2
		****************************************/

		class BindTexture2 : 
			public render::State<BindTexture2>
		{
		public:

			BindTexture2(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(2, pTexture);
			}

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 3
		****************************************/

		class BindTexture3 : 
			public render::State<BindTexture3>
		{
		public:

			BindTexture3(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(3, pTexture);
			}

			const d3d::Texture* pTexture;
		};

		/****************************************
		* VertexTexture0
		****************************************/

		class BindVertexTexture0 : 
			public render::State<BindVertexTexture0>
		{
		public:

			BindVertexTexture0(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexTexture(0, pTexture);
			}

			const d3d::Texture* pTexture;
		};


		/****************************************
		* Bind shader
		****************************************/

		class BindShader :
			public render::State<BindShader>
		{
		public:

			BindShader(const d3d::Effect& effect) : pEffect(&effect) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetEffect(*pEffect);
			}

			const d3d::Effect* pEffect;
		};

		
		/****************************************
		* SamplerState0
		****************************************/

		class SamplerState0 : 
			public render::State<SamplerState0>
		{
		public:

			SamplerState0(const d3d::Sampler& sampler): sampler(sampler)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelSampler(0, sampler);
			}

			const d3d::Sampler& sampler;
		};

		/****************************************
		* SamplerState1
		****************************************/

		class SamplerState1 : 
			public render::State<SamplerState1>
		{
		public:

			SamplerState1(const d3d::Sampler& sampler): sampler(sampler)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelSampler(1, sampler);
			}

			const d3d::Sampler& sampler;
		};

		/****************************************
		* SamplerState2
		****************************************/

		class SamplerState2 :
			public render::State<SamplerState2>
		{
		public:

			SamplerState2(const d3d::Sampler& sampler) : sampler(sampler)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelSampler(2, sampler);
			}

			const d3d::Sampler& sampler;
		};

		/****************************************
		* SamplerState3
		****************************************/

		class SamplerState3 :
			public render::State<SamplerState3>
		{
		public:

			SamplerState3(const d3d::Sampler& sampler) : sampler(sampler)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelSampler(3, sampler);
			}

			const d3d::Sampler& sampler;
		};

		/****************************************
		* VertexSamplerState0
		****************************************/

		class VertexSamplerState0 :
			public render::State<VertexSamplerState0>
		{
		public:

			VertexSamplerState0(const d3d::Sampler& sampler) : sampler(sampler)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexSampler(0, sampler);
			}

			const d3d::Sampler& sampler;
		};
				
		/****************************************
		* Render target 0
		****************************************/

		class BindRenderTargets : 
			public render::State<BindRenderTargets>
		{
		public:

			BindRenderTargets(const d3d::Texture** pTexture, size_t numTargets, const d3d::DepthBuffer* pDepthBuffer): numTargets(numTargets), pDepthBuffer(pDepthBuffer),
				pTextures(nullptr), pViews(nullptr)
			{
				if(numTargets)
				{
					pTextures = new const d3d::Texture*[numTargets];
					pViews = new ID3D11RenderTargetView*[numTargets];
					memcpy(pTextures, pTexture, sizeof(d3d::Texture*)*numTargets);
				}
			}

			BindRenderTargets(const BindRenderTargets& bind): numTargets(bind.numTargets), pDepthBuffer(bind.pDepthBuffer), pTextures(nullptr), pViews(nullptr)
			{
				if(numTargets)
				{
					pTextures = new const d3d::Texture*[numTargets];
					pViews = new ID3D11RenderTargetView*[numTargets];
					memcpy(pTextures, bind.pTextures, sizeof(d3d::Texture*)*numTargets);
				}
			}

			~BindRenderTargets(void)
			{
				delete[] pTextures;
				delete[] pViews;
			}

			void Execute(const d3d::Device& device) const
			{
				ID3D11DepthStencilView* pDepthView = nullptr;
				if(pDepthBuffer)
					pDepthView = pDepthBuffer->GetView();

				if(numTargets)
				{
					for(size_t i = 0; i < numTargets; i++)
					{
						if(pTextures[i])
							pViews[i] = pTextures[i]->GetRenderTargetView();
						else
							pViews[i] = nullptr;
					}
					device.SetRenderTarget(numTargets, pViews, pDepthView);
				}
				else
				{
					device.SetRenderTarget(0, nullptr, pDepthView);
				}
			}

		private:

			size_t numTargets;
			const d3d::Texture** pTextures;
			const d3d::DepthBuffer* pDepthBuffer;
			ID3D11RenderTargetView** pViews;
		};

		/****************************************
		* Depth state
		****************************************/

		class DepthState : 
			public render::State<DepthState>
		{
		public:

			DepthState(ID3D11DepthStencilState& depthState): pDepthState(&depthState)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetDepthState(pDepthState);
			}

			ID3D11DepthStencilState* pDepthState;
		};

		/****************************************
		* Alpha state
		****************************************/

		class AlphaBlendState : 
			public render::State<AlphaBlendState>
		{
		public:

			AlphaBlendState(ID3D11BlendState* pBlendState): pBlendState(pBlendState)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetBlendState(pBlendState);
			}

			ID3D11BlendState* pBlendState;
		};

		/****************************************
		* RasterState
		****************************************/

		class RasterizerState : 
			public render::State<RasterizerState>
		{
		public:

			RasterizerState(ID3D11RasterizerState* pRasterState): pRasterState(pRasterState)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetRasterState(pRasterState);
			}

			ID3D11RasterizerState* pRasterState;
		};

		/****************************************
		* Viewport
		****************************************/

		class SetViewport : 
			public render::State<SetViewport>
		{
		public:

			SetViewport(const D3D11_VIEWPORT& viewport): viewport(viewport)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetViewport(viewport);
			}

			D3D11_VIEWPORT viewport;
		};

		/****************************************
		* Viewport
		****************************************/

		class SetScissorRect : 
			public render::State<SetScissorRect>
		{
		public:

			SetScissorRect(const D3D11_RECT& rect): scissorRect(rect), bSet(true)
			{
			}

			SetScissorRect(void): bSet(false)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				if(bSet)
					device.SetScissorRect(scissorRect);
				else
					device.SetScissorRect();
			}

			bool bSet;
			D3D11_RECT scissorRect;
		};

/***********************************************
*      DRAW CALLS
***********************************************/
				
		/****************************************
		* Draw indexed
		****************************************/

		class DrawIndexedPrimitives : 
			public render::DrawCall<DrawIndexedPrimitives>
		{
		public:	

			DrawIndexedPrimitives(unsigned int vertexCount, unsigned int startVertex, unsigned int startIndex): numVertices(vertexCount), startVertex(startVertex), startIndex(startIndex)
			{  
			};

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawIndexedPrimitives(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				device.DrawIndexed(numVertices, startIndex, startVertex); 
			}

		private:	


			unsigned int numVertices, startIndex, startVertex;
		};

		/****************************************
		* Clear
		****************************************/

		class ClearTargets : 
			public render::DrawCall<ClearTargets>
		{
		public:	

			ClearTargets(const d3d::Texture** pRenderTargets, unsigned int numTargets, const float* pColor, const d3d::DepthBuffer* pDepthBuffer): numTargets(numTargets), pDepthBuffer(pDepthBuffer),
				pTargets(nullptr)
			{ 
				if(numTargets)
				{
					pTargets = new const d3d::Texture*[numTargets];
					memcpy(pTargets, pRenderTargets, sizeof(d3d::Texture*)*numTargets);
				}
				if(pColor)
				{
					this->pColor = new float[4];
					memcpy(this->pColor, pColor, sizeof(float)* 4);
				}
			};

			ClearTargets(const ClearTargets& clear): numTargets(clear.numTargets), pDepthBuffer(clear.pDepthBuffer),
				pTargets(nullptr), pColor(nullptr)
			{
				if(numTargets)
				{
					pTargets = new const d3d::Texture*[numTargets];
					memcpy(pTargets, clear.pTargets, sizeof(ID3D11RenderTargetView*)*numTargets);
				}
				if(clear.pColor)
				{
					pColor = new float[4];
					memcpy(pColor, clear.pColor, sizeof(float)*4);
				}
			}

			~ClearTargets(void)
			{
				delete[] pColor;
				delete pTargets;
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new ClearTargets(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				if(numTargets)
				{
					if(numTargets == 1 && !pTargets[0])
						device.Clear();
					else
					{
						for(unsigned int i = 0; i < numTargets; i++)
						{
							device.Clear(pTargets[i]->GetRenderTargetView(), pColor);
						}
					}
				}

				if(pDepthBuffer)
					device.ClearDepth(*pDepthBuffer);
			}

		private:	

			const d3d::Texture** pTargets;
			const d3d::DepthBuffer* pDepthBuffer;
			float* pColor;
			unsigned int numTargets;
		};

		/****************************************
		* Draw instanced
		****************************************/

		class DrawIndexedInstancedPrimitives : 
			public render::DrawCall<DrawIndexedInstancedPrimitives>
		{
		public:	

			DrawIndexedInstancedPrimitives(unsigned int indicesPerInst, unsigned int numInstances, size_t indexStart) : indicesPerInst(indicesPerInst), numInstances(numInstances),
				indexStart(indexStart)
			{ 
			};

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawIndexedInstancedPrimitives(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				device.DrawIndexedInstanced(indicesPerInst, numInstances, indexStart);
			}

		private:	

			unsigned int indicesPerInst, numInstances, indexStart;
		};

		/****************************************
		* Draw
		****************************************/

		class DrawPrimitives : 
			public render::DrawCall<DrawPrimitives>
		{
		public:	

			DrawPrimitives(unsigned int vertexCount, unsigned int startVertex): numVertices(vertexCount), startVertex(startVertex)
			{  
			};

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawPrimitives(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				device.Draw(numVertices, startVertex); 
			}

		private:	

			unsigned int numVertices, startVertex;
		};

		/****************************************
		* GenerateMips
		****************************************/

		class GenerateMips : 
			public render::DrawCall<GenerateMips>
		{
		public:	

			GenerateMips(const d3d::Texture& texture): texture(texture)
			{  
			};

			render::BaseDrawCall& Clone(void) const
			{
				return *new GenerateMips(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				device.GenerateMips(*texture.GetResourceView()); 
			}

		private:	

			const d3d::Texture& texture;
		};

/***********************************************
*      CBuffer
***********************************************/
		
		/****************************************
		* Vertex constant buffer 0
		****************************************/

		class BindVCBuffer0 : 
			public render::State<BindVCBuffer0>
		{
		public:

			BindVCBuffer0(const d3d::ConstantBuffer& cBuffer): cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexShaderCBuffer(0, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

		/****************************************
		* Vertex constant buffer 1
		****************************************/

		class BindVCBuffer1 : 
			public render::State<BindVCBuffer1>
		{
		public:

			BindVCBuffer1(const d3d::ConstantBuffer& cBuffer): cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexShaderCBuffer(1, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

		/****************************************
		* Pixel constant buffer 0
		****************************************/

		class BindPCBuffer0 : 
			public render::State<BindPCBuffer0>
		{
		public:

			BindPCBuffer0(const d3d::ConstantBuffer& cBuffer): cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelShaderCBuffer(0, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

		/****************************************
		* Pixel constant buffer 1
		****************************************/

		class BindPCBuffer1 : 
			public render::State<BindPCBuffer1>
		{
		public:

			BindPCBuffer1(const d3d::ConstantBuffer& cBuffer): cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelShaderCBuffer(1, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

		/****************************************
		* Geometry constant buffer 0
		****************************************/

		class BindGCBuffer0 :
			public render::State<BindGCBuffer0>
		{
		public:

			BindGCBuffer0(const d3d::ConstantBuffer& cBuffer) : cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetGeometryShaderCBuffer(0, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

		/****************************************
		* Geometry constant buffer 1
		****************************************/

		class BindGCBuffer1 :
			public render::State<BindGCBuffer1>
		{
		public:

			BindGCBuffer1(const d3d::ConstantBuffer& cBuffer) : cBuffer(cBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetGeometryShaderCBuffer(1, cBuffer);
			}

			const d3d::ConstantBuffer& cBuffer;
		};

	}
}

#endif