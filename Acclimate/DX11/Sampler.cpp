#include "Sampler.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "..\Safe.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			Sampler::Sampler(const Device& device, const D3D11_SAMPLER_DESC& samplerDesc): m_pDevice(&device)
			{
				m_pSampler = m_pDevice->CreateSampler(samplerDesc);
			}

			Sampler::~Sampler(void)
			{
				SAFE_RELEASE(m_pSampler);
			}

			ID3D11SamplerState& Sampler::GetSampler(void) const
			{
				return *m_pSampler;
			}

			void Sampler::Set(const D3D11_SAMPLER_DESC& desc)
			{
				SAFE_RELEASE(m_pSampler);
				m_pSampler = m_pDevice->CreateSampler(desc);
			}

		}
	}
}

#endif