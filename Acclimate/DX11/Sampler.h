#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class Sampler
			{
			public:
				Sampler(const Device& device, const D3D11_SAMPLER_DESC& desc);
				~Sampler(void);

				void Set(const D3D11_SAMPLER_DESC& desc);

				ID3D11SamplerState& GetSampler(void) const;

			private:

				const Device* m_pDevice;

				ID3D11SamplerState* m_pSampler;
			};

		}
	}
}

#endif