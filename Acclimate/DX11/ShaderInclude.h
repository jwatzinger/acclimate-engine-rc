#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <string>
#include <list>
#include <memory>
#include <D3D11.h>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			class ShaderInclude :
				public ID3DInclude
			{
			public:
				ShaderInclude(const std::wstring& stBasefile);

				HRESULT __stdcall Open(D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) override;

				HRESULT __stdcall Close(LPCVOID pData) override;

			private:

				std::string m_stBaseDir;
				std::list<std::unique_ptr<char[]>> m_data;
			};

		}
	}
}

#endif