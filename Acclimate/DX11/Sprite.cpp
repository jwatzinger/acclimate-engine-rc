#include "Sprite.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "VertexBuffer.h"
#include "..\Gfx\Color.h"
#include "..\Math\Vector3.h"
#include "..\Math\Vector2f.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			Sprite::Sprite(Device& device): m_nextFreeId(0), m_pDevice(&device),
				m_numSprites(20000)
			{
				D3D11_BUFFER_DESC vertexBufferDesc = {};

				vertexBufferDesc.Usage			  = D3D11_USAGE_DYNAMIC;
				vertexBufferDesc.ByteWidth        = sizeof( SpriteVertex ) * m_numSprites;
				vertexBufferDesc.BindFlags        = D3D11_BIND_VERTEX_BUFFER;
				vertexBufferDesc.CPUAccessFlags   = D3D11_CPU_ACCESS_WRITE;
				vertexBufferDesc.MiscFlags        = 0;
				m_pVertexBuffer = new VertexBuffer(device, vertexBufferDesc, sizeof( SpriteVertex ));

				const math::Vector2& vBackbufferSize = device.GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			Sprite::Sprite(const Sprite& sprite) : 
				Sprite(*sprite.m_pDevice)
			{
			}

			Sprite::~Sprite(void)
			{
				delete m_pVertexBuffer;
			}

			void Sprite::Reset(void)
			{
				D3D11_MAPPED_SUBRESOURCE mappedRes;
				m_pVertexBuffer->Map(mappedRes, D3D11_MAP_WRITE_DISCARD);
				m_pVertexBuffer->Unmap();

				m_nextFreeId = 0;

				const math::Vector2& vBackbufferSize = m_pDevice->GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			void Sprite::Map(void)
			{
				m_pVertexBuffer->Map(m_mapped, D3D11_MAP_WRITE_NO_OVERWRITE);
			}

			void Sprite::Unmap(void)
			{
				m_pVertexBuffer->Unmap();
			}

			const float recipColor = 1.0f / 255.0f;

			size_t Sprite::Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const RECT& rSrcRect, const math::Vector2f& vScale, float /*fAngle*/, const gfx::Color& color)
			{
				const math::Vector2 vSrcSize(rSrcRect.right - rSrcRect.left, rSrcRect.bottom - rSrcRect.top);
				const math::Vector2f vSize(vSrcSize.x*vScale.x, vSrcSize.y*vScale.y);

				SpriteVertex* pBuffer = static_cast<SpriteVertex*>(m_mapped.pData) + m_nextFreeId;

				pBuffer->leftPos = vPosition.x  * m_vInvHalfScreenSize.x - 1.0f;
				pBuffer->rightPos = pBuffer->leftPos + vSize.x * m_vInvHalfScreenSize.x;
				pBuffer->topPos = -vPosition.y * m_vInvHalfScreenSize.y + 1.0f;
				pBuffer->bottomPos = pBuffer->topPos - vSize.y * m_vInvHalfScreenSize.y;

				pBuffer->leftCoord = rSrcRect.left / (float)vTextureSize.x;
				pBuffer->rightCoord = rSrcRect.right / (float)vTextureSize.x;
				pBuffer->topCoord = rSrcRect.top / (float)vTextureSize.y;
				pBuffer->bottomCoord = rSrcRect.bottom / (float)vTextureSize.y;

				pBuffer->z = vPosition.z;

				// color
				pBuffer->r = color.r * recipColor;
				pBuffer->g = color.g * recipColor;
				pBuffer->b = color.b * recipColor;
				pBuffer->a = color.a * recipColor;

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const math::Vector2f& vScale, float /*angle*/, const gfx::Color& color)
			{
				const float leftVertex = vPosition.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float rightVertex = leftVertex + vScale.x * m_vInvHalfScreenSize.x;
				const float topVertex = -vPosition.y * m_vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = topVertex - vScale.y * m_vInvHalfScreenSize.y;

				const SpriteVertex Vertex = { leftVertex, rightVertex, topVertex, bottomVertex, 0.0f, 0.0f, 0.0f, 0.0f, vPosition.z, 1.0f, 1.0f, 1.0f, 1.0f };
				
				SpriteVertex* pBuffer = static_cast<SpriteVertex*>(m_mapped.pData);
				memcpy(pBuffer + m_nextFreeId, &Vertex, sizeof(SpriteVertex));

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(SpriteVertex* pVertices, size_t numQuads)
			{
				SpriteVertex* pBuffer = static_cast<SpriteVertex*>(m_mapped.pData);

				memcpy(pBuffer+m_nextFreeId, pVertices, numQuads*sizeof(SpriteVertex));

				m_nextFreeId += numQuads;
				return m_nextFreeId - numQuads;
			}

			const VertexBuffer& Sprite::GetVertexBuffer(void) const
			{
				return *m_pVertexBuffer;
			}

			const math::Vector2f& Sprite::GetInvHalfScreenSize(void) const
			{
				return m_vInvHalfScreenSize;
			}

		}
	}
}

#endif