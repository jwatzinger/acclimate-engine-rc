#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <D3D11.h>
#include <memory>
#include "..\Math\Rect.h"
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

    namespace math
    {
        struct Vector3;
    }

	namespace dx11
	{
		namespace d3d
		{

			struct SpriteVertex
			{
				float leftPos, rightPos, topPos, bottomPos;
				float leftCoord, rightCoord, topCoord, bottomCoord;
				float z;
				float r, g, b, a;
			};

			class Device;
			class VertexBuffer;

			class Sprite
			{
			public:
				Sprite(Device& device);
				Sprite(const Sprite& sprite);
				~Sprite(void);

				void Reset(void);

				void Map(void);
				void Unmap(void);

				size_t Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle, const gfx::Color& color);
				size_t Draw(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle, const gfx::Color& color);
				size_t Draw(SpriteVertex* pVertices, size_t numVertices);

				const VertexBuffer& GetVertexBuffer(void) const;
				const math::Vector2f& GetInvHalfScreenSize(void) const;

			private:

				size_t m_numSprites;

				D3D11_MAPPED_SUBRESOURCE m_mapped;
			
				size_t m_nextFreeId;

				math::Vector2f m_vInvHalfScreenSize;

				VertexBuffer* m_pVertexBuffer;
				Device* m_pDevice;
			};

		}
	}
}

#endif