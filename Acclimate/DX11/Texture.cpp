#include "Texture.h"

#ifdef ACL_API_DX11

#include <D3DX11tex.h>
#include "Device.h"
#include "..\Safe.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			Texture::Texture(const Device& device, LPCWSTR lpFileName, bool bRead, DXGI_FORMAT format): m_pView(nullptr), m_pDevice(&device),
				m_pRenderView(nullptr), m_bMapped(false), m_bGenerateMips(false)
			{
				if(FAILED(D3DX11GetImageInfoFromFile(lpFileName, nullptr, &m_info, nullptr)))
					throw apiException();

				if(format != DXGI_FORMAT_UNKNOWN)
					m_info.Format = format;

				m_vSize.x = m_info.Width;
				m_vSize.y = m_info.Height;

				D3DX11_IMAGE_LOAD_INFO loadInfo;
				ZeroMemory(&loadInfo, sizeof(loadInfo));
				loadInfo.MipLevels = m_info.MipLevels;
				if(bRead)
				{
					loadInfo.Usage = D3D11_USAGE_STAGING;
					loadInfo.CpuAccessFlags = D3D11_CPU_ACCESS_READ;
				}
				else
				{
					loadInfo.Usage = D3D11_USAGE_IMMUTABLE;
					loadInfo.BindFlags = D3D11_BIND_SHADER_RESOURCE;
				}

				loadInfo.Width = m_info.Width;
				loadInfo.Height = m_info.Height;
				loadInfo.Format = m_info.Format;
				if(FAILED(D3DX11CreateTextureFromFile(device.GetDevice(), lpFileName, &loadInfo, nullptr, (ID3D11Resource**)&m_pTexture, nullptr)))
					throw apiException();

				if(!bRead)
					m_pView = device.CreateShaderResourceView(*m_pTexture);
			}

			Texture::Texture(const Device& device, int width, int height, DXGI_FORMAT format, bool bRenderTarget, bool bWrite, bool bGenerateMips): 
				Texture(device, width, height, nullptr, 0, format, bRenderTarget, bWrite, bGenerateMips)
			{
			}

			Texture::Texture(const Device& device, int width, int height, const void* pData, int pitch, DXGI_FORMAT format, bool bRenderTarget, bool bWrite, bool bGenerateMips) : m_vSize(width, height),
				m_pView(nullptr), m_pDevice(&device), m_pRenderView(nullptr), m_bMapped(false), m_bGenerateMips(bGenerateMips)
			{
				if(bRenderTarget)
				{
					UINT miscFlags = 0;
					if(bGenerateMips)
						miscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
					m_pTexture = device.Create2DTexture(width, height, pData, pitch, format, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, miscFlags);
				}
				else
				{
					UINT cpuFlags = 0;
					D3D11_USAGE usage = D3D11_USAGE_DEFAULT;
					if(bWrite)
					{
						cpuFlags |= D3D11_CPU_ACCESS_WRITE;
						usage = D3D11_USAGE_DYNAMIC;
					}
					else if(pData)
						usage = D3D11_USAGE_IMMUTABLE;
					
					m_pTexture = device.Create2DTexture(width, height, pData, pitch, format, usage, D3D11_BIND_SHADER_RESOURCE, cpuFlags, 0);
				}


				m_info.Width = width;
				m_info.Height = height;
				m_info.Format = format;
				m_info.MipLevels = 1;

				m_pView = device.CreateShaderResourceView(*m_pTexture);

				if(bRenderTarget)
					m_pRenderView = device.CreateRenderTargetView(*m_pTexture);
			}

			Texture::~Texture(void)
			{
				SAFE_RELEASE(m_pTexture);
				SAFE_RELEASE(m_pView);
				SAFE_RELEASE(m_pRenderView);
			}

			void Texture::Reload(int width, int height)
			{
				if(m_pRenderView)
				{
					m_info.Width = width;
					m_info.Height = height;
					m_vSize.x = width;
					m_vSize.y = height;

					m_pRenderView->Release();
					m_pView->Release();
					m_pTexture->Release();

					UINT miscFlags = 0;
					if(m_bGenerateMips)
						miscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

					m_pTexture = m_pDevice->Create2DTexture(width, height, nullptr, 0, m_info.Format, D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, miscFlags);
					m_pView = m_pDevice->CreateShaderResourceView(*m_pTexture);
					m_pRenderView = m_pDevice->CreateRenderTargetView(*m_pTexture);
				}
			}

			void Texture::SaveToFile(LPCWSTR stFile)
			{
				D3DX11SaveTextureToFile(m_pDevice->GetContext(), m_pTexture, D3DX11_IMAGE_FILE_FORMAT::D3DX11_IFF_PNG, stFile);
			}

			const math::Vector2& Texture::GetSize(void) const
			{
				return m_vSize;
			}

			ID3D11Resource* Texture::GetResource(void) const
			{
				return m_pTexture;
			}

			ID3D11ShaderResourceView* Texture::GetResourceView(void) const
			{
				return m_pView;
			}

			ID3D11RenderTargetView* Texture::GetRenderTargetView(void) const
			{
				return m_pRenderView;
			}

			DXGI_FORMAT Texture::GetFormat(void) const
			{
				return m_info.Format;
			}

			void Texture::Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP type)
			{
				if(!m_bMapped)
				{
					m_pDevice->Map(*m_pTexture, type, map);
					m_bMapped = true;
				}
			}

			void Texture::Unmap(void)
			{
				if(m_bMapped)
				{
					m_pDevice->Unmap(*m_pTexture);
					m_bMapped = false;
				}
			}

		}
	}
}

#endif