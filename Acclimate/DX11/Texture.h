#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3dx11.h>
#include "..\Math\Vector.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class Texture
			{
			public:
				Texture(const Device& device, LPCWSTR lpFileName, bool bRead, DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
				Texture(const Device& device, int width, int height, DXGI_FORMAT format, bool bRenderTarget, bool bRead, bool bGenerateMips);
				Texture(const Device& device, int width, int height, const void* pData, int pitch, DXGI_FORMAT format, bool bRenderTarget, bool bRead, bool bGenerateMips);
				~Texture(void);

				void Reload(int width, int height);
				void SaveToFile(LPCWSTR stFile);

				const math::Vector2& GetSize(void) const;
				ID3D11Resource* GetResource(void) const;
				ID3D11ShaderResourceView* GetResourceView(void) const;
				ID3D11RenderTargetView* GetRenderTargetView(void) const;
				DXGI_FORMAT GetFormat(void) const;

				void Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP type);
				void Unmap(void);

			private:

				bool m_bMapped, m_bGenerateMips;

				const Device* m_pDevice;

				ID3D11Texture2D* m_pTexture;
				ID3D11ShaderResourceView* m_pView;
				ID3D11RenderTargetView* m_pRenderView;

				D3DX11_IMAGE_INFO m_info;

				math::Vector2 m_vSize;
			};

		}
	}
}

#endif