#include "VertexBuffer.h"

#ifdef ACL_API_DX11

#include "Device.h"
#include "..\Safe.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{

			VertexBuffer::VertexBuffer(const Device& device, ID3D11Buffer& vertexBuffer, size_t vertexSize): m_pVertexBuffer(&vertexBuffer), m_pDevice(&device), 
				m_vertexSize(vertexSize), m_bMapped(false)
			{
			}

			VertexBuffer::VertexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, size_t vertexSize): m_pVertexBuffer(nullptr),
				m_pDevice(&device), m_vertexSize(vertexSize), m_desc(bufferDesc), m_bMapped(false)
			{
				m_pVertexBuffer = device.CreateBuffer(bufferDesc, nullptr);
			}

			VertexBuffer::VertexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA& initData, size_t vertexSize): m_pVertexBuffer(nullptr),
				m_pDevice(&device), m_vertexSize(vertexSize), m_desc(bufferDesc), m_bMapped(false)
			{
				m_pVertexBuffer = device.CreateBuffer(bufferDesc, &initData);
			}

			VertexBuffer::VertexBuffer(const VertexBuffer& vertexBuffer): m_vertexSize(vertexBuffer.m_vertexSize), m_pDevice(vertexBuffer.m_pDevice), 
				m_desc(vertexBuffer.m_desc), m_bMapped(false)
			{
				ACL_ASSERT(m_desc.Usage != D3D11_USAGE_IMMUTABLE);
				m_pVertexBuffer = m_pDevice->CreateBuffer(m_desc, nullptr);
			}

			VertexBuffer::~VertexBuffer(void)
			{
				SAFE_RELEASE(m_pVertexBuffer);
			}

			size_t VertexBuffer::GetVertexSize(void) const
			{
				return m_vertexSize;
			}

			ID3D11Buffer& VertexBuffer::GetBuffer(void) const
			{
				return *m_pVertexBuffer;
			}

			void VertexBuffer::Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP flags)
			{
				if(!m_bMapped)
				{
					m_pDevice->Map(*m_pVertexBuffer, flags, map);
					m_bMapped = true;
				}
			}

			void VertexBuffer::Unmap(void)
			{
				if(m_bMapped)
				{
					m_pDevice->Unmap(*m_pVertexBuffer);
					m_bMapped = false;
				}
			}

		}
	}
}

#endif