#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX11

#include <d3d11.h>

namespace acl
{
	namespace dx11
	{
		namespace d3d
		{
			class Device;

			class VertexBuffer
			{
			public:
				VertexBuffer(const Device& device, ID3D11Buffer& vertexBuffer, size_t vertexSize);
				VertexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, size_t vertexSize);
				VertexBuffer(const Device& device, const D3D11_BUFFER_DESC& bufferDesc, const D3D11_SUBRESOURCE_DATA& initData, size_t vertexSize);
				VertexBuffer(const VertexBuffer& vertexBuffer);
				~VertexBuffer(void);

				size_t GetVertexSize(void) const;
				ID3D11Buffer& GetBuffer(void) const;

				void Map(D3D11_MAPPED_SUBRESOURCE& map, D3D11_MAP flags);
				void Unmap(void);

			private:

				bool m_bMapped;
				size_t m_vertexSize;

				const Device* m_pDevice;

				D3D11_BUFFER_DESC m_desc;
				ID3D11Buffer* m_pVertexBuffer;
			};

		}
	}
}

#endif