#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			enum class BufferLock
			{
				NONE = 0x0, DISCARD = D3DLOCK_DISCARD, NOOVER = D3DLOCK_NOOVERWRITE, DISCARD_NOOVER = D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE
			};

		}
	}
}

#endif