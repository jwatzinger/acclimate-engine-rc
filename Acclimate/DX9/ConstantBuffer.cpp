#include "ConstantBuffer.h"

#ifdef ACL_API_DX9

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			ConstantBuffer::ConstantBuffer(void) : m_pBuffer(nullptr), m_size(0)
			{
			}

			ConstantBuffer::ConstantBuffer(const float* pBuffer, size_t sizeInByte) : m_pBuffer(pBuffer),
				m_size(sizeInByte)
			{
			}

			void ConstantBuffer::SetBuffer(const float* pBuffer, size_t size)
			{
				m_pBuffer = pBuffer;
				m_size = size;
			}

			size_t ConstantBuffer::GetSize(void) const
			{
				return m_size;
			}

			size_t ConstantBuffer::GetNumConsts(void) const
			{
				return m_size / 16;
			}

		}
	}
}

#endif