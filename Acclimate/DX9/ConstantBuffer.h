#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class ConstantBuffer
			{
			public:
				ConstantBuffer(void);
				ConstantBuffer(const float* pBuffer, size_t sizeInByte);

				void SetBuffer(const float* pBuffer, size_t size);

				const float* GetBuffer(void) const
				{
					return m_pBuffer;
				}

				size_t GetSize(void) const;
				size_t GetNumConsts(void) const;

			private:

				ConstantBuffer(const ConstantBuffer& constantBuffer) {}

				size_t m_size;

				const float* m_pBuffer;
			};

		}
	}
}

#endif