#pragma once
#ifdef USE_API_DX9
#define ACL_API_DX9

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{
			class Device;
			class Sprite;
			class Texture;
			class VertexBuffer;
			class IndexBuffer;
			class Effect;
			class Mesh;
			class Font;
			class DepthBuffer;
			class Line;
			class VertexDeclaration;
			class ConstantBuffer;
		}
	}

	typedef dx9::d3d::Device AclDevice; 
	typedef dx9::d3d::Sprite AclSprite;
	typedef dx9::d3d::Texture AclTexture;
	typedef dx9::d3d::DepthBuffer AclDepth;
	typedef dx9::d3d::VertexBuffer AclVertexBuffer;
	typedef dx9::d3d::IndexBuffer AclIndexBuffer;
	typedef dx9::d3d::Effect AclEffect;
    typedef dx9::d3d::Mesh AclMesh;
    typedef dx9::d3d::Font AclFont;
	typedef dx9::d3d::Line AclLine;
	typedef dx9::d3d::ConstantBuffer AclCbuffer;
	typedef dx9::d3d::VertexDeclaration AclGeometry;

}

#endif