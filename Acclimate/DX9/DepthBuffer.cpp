#include "DepthBuffer.h"

#ifdef ACL_API_DX9

#include "Device.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			DepthBuffer::DepthBuffer(const Device& device, int width, int height, D3DFORMAT format): m_pDevice(&device), m_vSize(width, height), 
				m_fmt(format)
			{
				m_lpSurface = device.CreateDepthSurface(width, height, format);
			}

			DepthBuffer::~DepthBuffer(void)
			{
				m_lpSurface->Release();
			}

			LPDIRECT3DSURFACE9 DepthBuffer::GetSurface(void) const
			{
				return m_lpSurface;
			}
		
			const math::Vector2& DepthBuffer::GetSize(void) const
			{
				return m_vSize;
			}

			void DepthBuffer::Reload(const math::Vector2& vSize)
			{
				if(m_vSize != vSize)
				{
					m_vSize = vSize;
					m_lpSurface->Release();
					m_lpSurface = m_pDevice->CreateDepthSurface(m_vSize.x, m_vSize.y, m_fmt);
				}
			}

		}
	}
}

#endif