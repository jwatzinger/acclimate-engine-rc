#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>
#include "..\Math\Vector.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Device;

			class DepthBuffer
			{
			public:
				DepthBuffer(const Device& device, int width, int height, D3DFORMAT format);
				~DepthBuffer(void);

				LPDIRECT3DSURFACE9 GetSurface(void) const;
				const math::Vector2& GetSize(void) const;

				void Reload(const math::Vector2& vSize);

			private:

				const Device* m_pDevice;

				math::Vector2 m_vSize;
				D3DFORMAT m_fmt;

				LPDIRECT3DSURFACE9 m_lpSurface;
			};

		}
	}
}

#endif