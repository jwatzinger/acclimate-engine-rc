#include "Device.h"

#ifdef ACL_API_DX9

#include "Texture.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexDeclaration.h"
#include "DepthBuffer.h"
#include "..\Safe.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			Device::Device(LPDIRECT3DDEVICE9 lpDevice, const D3DDISPLAYMODE& displayMode) : m_lpDevice(lpDevice), m_displayMode(displayMode)
			{
				m_lpDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_lpBackBuffer);
				m_lpDevice->GetDepthStencilSurface(&m_lpDepthBuffer);
	
				const int anis = 16;
				m_lpDevice->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, anis);
				m_lpDevice->SetSamplerState(1, D3DSAMP_MAXANISOTROPY, anis);
				m_lpDevice->SetSamplerState(2, D3DSAMP_MAXANISOTROPY, anis);
				m_lpDevice->SetSamplerState(3, D3DSAMP_MAXANISOTROPY, anis);

			}

			Device::~Device(void)
			{
				for(unsigned int i = 0; i < 4; i++)
				{
					SetRenderTarget(i, nullptr);
					SetTexture(i, nullptr);
				}
				m_lpDevice->SetStreamSource(0, nullptr, 0, 0);
				m_lpDevice->SetVertexDeclaration(nullptr);
				m_lpDevice->SetVertexShader(nullptr);
				m_lpDevice->SetPixelShader(nullptr);
				SAFE_RELEASE(m_lpBackBuffer);
				SAFE_RELEASE(m_lpDepthBuffer);
				SAFE_RELEASE(m_lpDevice);
			}

			void Device::Present(void) const
			{
				m_lpDevice->Present(0, 0, 0, 0);
			}

			void Device::BeginScene(void) const
			{
				m_lpDevice->BeginScene();
			}

			void Device::EndScene(void) const
			{
				m_lpDevice->EndScene();
			}

			void Device::Clear(DWORD dwFlags, float* pColor) const
			{
				D3DCOLOR color;
				if(pColor)
					color = D3DCOLOR_ARGB((int)(pColor[3] * 255), (int)(pColor[0] * 255), (int)(pColor[1] * 255), (int)(pColor[2] * 255));
				else
					color = D3DCOLOR_ARGB(255, 0, 0, 0);

				m_lpDevice->Clear(0, nullptr, dwFlags, color, 1.0f, 0);
			}

			void Device::Resize(D3DPRESENT_PARAMETERS& params)
			{
				m_lpBackBuffer->Release();
				m_lpDepthBuffer->Release();

				m_displayMode.Width = params.BackBufferWidth;
				m_displayMode.Height = params.BackBufferHeight;

				HRESULT hr;
				if(FAILED(hr = m_lpDevice->Reset(&params)))
				{
					int i = 0;
					switch(hr)
					{
					case D3DERR_DEVICELOST:
						i++;
						break;
					case D3DERR_DEVICEREMOVED:
						i++;
						break;
					case D3DERR_DRIVERINTERNALERROR:
						i++;
						break;
					case D3DERR_OUTOFVIDEOMEMORY:
						i++;
						break;
					case D3DERR_INVALIDCALL:
						i++;
						break;
					}
					throw apiException();
				}
					

				m_lpDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &m_lpBackBuffer);
				m_lpDevice->GetDepthStencilSurface(&m_lpDepthBuffer);
			}

			LPDIRECT3DTEXTURE9 Device::CreateTexture(int width, int height, DWORD dwUsage, D3DFORMAT d3dFormat, D3DPOOL d3dPool) const
			{
				//create texture
				LPDIRECT3DTEXTURE9 lpTexture;
				HRESULT hr = m_lpDevice->CreateTexture(width, height, 1, dwUsage, d3dFormat, d3dPool, &lpTexture, nullptr);

				if(FAILED(hr))
					throw apiException();

  				return lpTexture;
			}

			LPDIRECT3DVERTEXBUFFER9 Device::CreateVertexBuffer(unsigned int length, DWORD dwUsage, D3DPOOL pool) const
			{
				LPDIRECT3DVERTEXBUFFER9 lpVertexBuffer;
				HRESULT hr = m_lpDevice->CreateVertexBuffer(length, dwUsage, 0, pool, &lpVertexBuffer, nullptr);
				if(FAILED(hr))
					throw apiException();

				return lpVertexBuffer;
			}

			LPDIRECT3DVERTEXDECLARATION9 Device::CreateVertexDeclaration(const D3DVERTEXELEMENT9* pVertexElements) const
			{
				LPDIRECT3DVERTEXDECLARATION9 lpDeclaration;
				HRESULT hr = m_lpDevice->CreateVertexDeclaration(pVertexElements, &lpDeclaration);
				if(FAILED(hr))
					throw apiException();

				return lpDeclaration;
			}

			LPDIRECT3DINDEXBUFFER9 Device::CreateIndexBuffer(unsigned int length, DWORD dwUsage, D3DFORMAT fmt, D3DPOOL pool) const
			{
				LPDIRECT3DINDEXBUFFER9 lpIndexBuffer;
				HRESULT hr = m_lpDevice->CreateIndexBuffer(length, dwUsage, fmt, pool, &lpIndexBuffer, nullptr);
				if(FAILED(hr))
					throw apiException();

				return lpIndexBuffer;
			}

			LPDIRECT3DSURFACE9 Device::CreateDepthSurface(int width, int height, D3DFORMAT d3dFormat) const
			{
				LPDIRECT3DSURFACE9 lpDepthSurface;
				HRESULT hr = m_lpDevice->CreateDepthStencilSurface(width, height, d3dFormat, D3DMULTISAMPLE_NONE, 0, true, &lpDepthSurface, nullptr);
				if(FAILED(hr))
					throw apiException();

				return lpDepthSurface;
			}

			void Device::SetCursor(int offsetX, int offsetY, const Texture* pTexture) const
			{
				m_lpDevice->SetCursorProperties(offsetX, offsetY, pTexture->GetSurface());
				m_lpDevice->ShowCursor(true);
			}

			void Device::SetRenderTarget(unsigned int id, const Texture* pTexture) const
			{
				HRESULT hr;
				if(pTexture)
					hr = m_lpDevice->SetRenderTarget(id, pTexture->GetSurface());
				else
				{
					if(id == 0)
						hr = m_lpDevice->SetRenderTarget(id, m_lpBackBuffer);
					else
						hr = m_lpDevice->SetRenderTarget(id, 0);
				}
				if(FAILED(hr))
					throw apiException();
			}

			LPDIRECT3DDEVICE9 Device::GetDevice(void) const
			{
				return m_lpDevice;
			}

			D3DDISPLAYMODE Device::GetDisplayMode(void) const
			{
				return m_displayMode;
			}

			void Device::SetVertexBuffer(unsigned int id, unsigned int offset, const VertexBuffer* pVertexBuffer) const
			{
				m_lpDevice->SetStreamSource(id, pVertexBuffer->GetVertexBuffer(), offset, pVertexBuffer->GetVertexSize());
			}

			void Device::SetVertexDeclaration(const VertexDeclaration* pVertexDeclaration) const
			{
				m_lpDevice->SetVertexDeclaration(pVertexDeclaration->GetD3DDeclaration());
			}

			void Device::SetVertexFreq(unsigned int id, unsigned int frequency) const
			{
				m_lpDevice->SetStreamSourceFreq(id, frequency);
			}

			void Device::SetIndexBuffer(const IndexBuffer* pIndexBuffer) const
			{
				m_lpDevice->SetIndices(pIndexBuffer->GetIndexBuffer());
			}

			void Device::SetViewport(const D3DVIEWPORT9& d3dViewport) const
			{	
				if(d3dViewport.Width == 0.0f && d3dViewport.Height == 0.0f)
				{
					D3DVIEWPORT9 port = d3dViewport;
					port.Width = m_displayMode.Width;
					port.Height = m_displayMode.Height;
					m_lpDevice->SetViewport(&d3dViewport);
				}
				else
					m_lpDevice->SetViewport(&d3dViewport);
			}

			void Device::SetScissorRect(const RECT* pScissorRect) const
			{
				HRESULT hr;
				if(pScissorRect)
					hr = m_lpDevice->SetScissorRect(pScissorRect);
				else
				{
					RECT r = { 0, 0, m_displayMode.Width, m_displayMode.Height };
					hr = m_lpDevice->SetScissorRect(&r);
				}

				if(FAILED(hr))
					throw apiException();
			}

			void Device::SetTexture(unsigned int index, const Texture* texture) const
			{
				LPDIRECT3DTEXTURE9 lpTexture = nullptr;
				if(texture)
					lpTexture = texture->GetTexture();
	
				HRESULT hr = m_lpDevice->SetTexture(index, lpTexture);
				if(FAILED(hr))
					throw apiException();
			}

			void Device::SetSamplerState(unsigned int index, DWORD minFilter, DWORD magFilter, DWORD mipFilter, DWORD adressU, DWORD adressV) const
			{
				m_lpDevice->SetSamplerState(index, D3DSAMP_MINFILTER, minFilter);
				m_lpDevice->SetSamplerState(index, D3DSAMP_MAGFILTER, magFilter);
				m_lpDevice->SetSamplerState(index, D3DSAMP_MIPFILTER, mipFilter);
				m_lpDevice->SetSamplerState(index, D3DSAMP_ADDRESSU, adressU);
				m_lpDevice->SetSamplerState(index, D3DSAMP_ADDRESSV, adressV);
			}

			void Device::SetVertexConstants(unsigned int index, const float* pConstants, unsigned int cConstants) const
			{
				HRESULT hr = m_lpDevice->SetVertexShaderConstantF(index, pConstants, cConstants);
				if(FAILED(hr))
					throw apiException();
			}

			void Device::SetPixelConstants(unsigned int index, const float* pConstants, unsigned int cConstants) const
			{
				HRESULT hr = m_lpDevice->SetPixelShaderConstantF(index, pConstants, cConstants);
				if(FAILED(hr))
					throw apiException();
			}

			void Device::SetDepthBuffer(const DepthBuffer* pDepthBuffer) const
			{
				if(pDepthBuffer)
					m_lpDevice->SetDepthStencilSurface(pDepthBuffer->GetSurface());
				else
					m_lpDevice->SetDepthStencilSurface(m_lpDepthBuffer);
			}

			void Device::DrawIndexed(RenderTypes renderType, unsigned int startVertex, unsigned int startIndex, unsigned int count) const
			{
				D3DPRIMITIVETYPE pt = D3DPT_TRIANGLELIST;
				int faces = count;
				switch(renderType)
				{
				case RenderTypes::TRI:
					pt = D3DPT_TRIANGLELIST;
					faces /= 3;
					break;
				case RenderTypes::LINE:
					pt = D3DPT_LINELIST;
					faces /= 2;
					break;
				case RenderTypes::TRI_STRIP:
					pt = D3DPT_TRIANGLESTRIP;
					faces -= 2;
					break;
				case RenderTypes::POINT:
					pt = D3DPT_POINTLIST;
					break;
				}
				HRESULT hr = m_lpDevice->DrawIndexedPrimitive(pt, startVertex, 0, count, startIndex, faces);
				if(FAILED(hr))
					throw apiException();
			}

			void Device::Draw(RenderTypes renderType, unsigned int startVertex, unsigned int count) const
			{
				D3DPRIMITIVETYPE pt = D3DPT_TRIANGLELIST;
				int faces = count;
				switch(renderType)
				{
				case RenderTypes::TRI:
					pt = D3DPT_TRIANGLELIST;
					faces /= 3;
					break;
				case RenderTypes::LINE:
					pt = D3DPT_LINELIST;
					faces /= 2;
					break;
				case RenderTypes::TRI_STRIP:
					pt = D3DPT_TRIANGLESTRIP;
					faces -= 2;
					break;
				case RenderTypes::POINT:
					pt = D3DPT_POINTLIST;
					break;
				}
				HRESULT hr = m_lpDevice->DrawPrimitive(pt, startVertex, faces);
				if(FAILED(hr))
					throw apiException();
			}

			void Device::SetRenderState(D3DRENDERSTATETYPE renderState, DWORD value) const
			{
				m_lpDevice->SetRenderState(renderState, value);
			}

		}
	}
}

#endif