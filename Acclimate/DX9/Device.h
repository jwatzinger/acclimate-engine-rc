#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#ifdef _DEBUG
#define D3D_DEBUG_INFO
#endif

#include <d3d9.h>
#include "RenderTypes.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Texture;
			class VertexBuffer;
			class IndexBuffer;
			class VertexDeclaration;
			class DepthBuffer;


			class ACCLIMATE_API Device
			{
			public:
				Device(LPDIRECT3DDEVICE9 lpDevice, const D3DDISPLAYMODE& displayMode);
				~Device(void);

				LPDIRECT3DTEXTURE9 CreateTexture(int width, int height, DWORD dwUsage, D3DFORMAT d3dFormat, D3DPOOL d3dPool) const;
				LPDIRECT3DVERTEXBUFFER9 CreateVertexBuffer(unsigned int length, DWORD dwUsage, D3DPOOL pool) const;
				LPDIRECT3DVERTEXDECLARATION9 CreateVertexDeclaration(const D3DVERTEXELEMENT9* pVertexElements) const;
				LPDIRECT3DINDEXBUFFER9 CreateIndexBuffer(unsigned int length, DWORD dwUsage, D3DFORMAT fmt, D3DPOOL pool) const;
				LPDIRECT3DSURFACE9 CreateDepthSurface(int width, int height, D3DFORMAT fmt) const;

				void SetCursor(int offsetX, int offsetY, const Texture* pCursor) const;
				void SetRenderTarget(unsigned int id, const Texture* pTexture) const;
				void SetVertexBuffer(unsigned int id, unsigned int offset, const VertexBuffer* pVertexBuffer) const;
				void SetVertexDeclaration(const VertexDeclaration* pVertexDeclaration) const;
				void SetVertexFreq(unsigned int id, unsigned int frequency) const;
				void SetIndexBuffer(const IndexBuffer* pIndexBuffer) const;
				void SetViewport(const D3DVIEWPORT9& d3dViewport) const;
				void SetScissorRect(const RECT* pScissorRect) const;
				void SetTexture(unsigned int index, const Texture* texture) const;
				void SetSamplerState(unsigned int index, DWORD minFilter, DWORD magFilter, DWORD mipFilter, DWORD adressU, DWORD adressV) const;
				void SetVertexConstants(unsigned int index, const float* pConstants, unsigned int cConstants) const;
				void SetPixelConstants(unsigned int index, const float* pConstants, unsigned int cConstants) const;
				void SetRenderState(D3DRENDERSTATETYPE renderState, DWORD value) const;
				void SetDepthBuffer(const DepthBuffer* pDepthBuffer) const;

				D3DDISPLAYMODE GetDisplayMode(void) const;
				LPDIRECT3DDEVICE9 GetDevice(void) const;

				void Present(void) const;
				void BeginScene(void) const;
				void EndScene(void) const;
				void Clear(DWORD dwFlags, float* pColor = nullptr) const;
				void Resize(D3DPRESENT_PARAMETERS& params);

				void DrawIndexed(RenderTypes renderType, unsigned int startVertex, unsigned int startIndex, unsigned int count) const;
				void Draw(RenderTypes renderType, unsigned int startVertex, unsigned int count) const;

			private:

				D3DDISPLAYMODE m_displayMode;

				LPDIRECT3DDEVICE9 m_lpDevice;
				LPDIRECT3DSURFACE9 m_lpBackBuffer;
				LPDIRECT3DSURFACE9 m_lpDepthBuffer;

			};

		}
	}
}

#endif