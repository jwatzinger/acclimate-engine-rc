#include "DirectX9.h"

#ifdef ACL_API_DX9

#include <dxerr.h>
#include "Device.h"
#include "Sprite.h"
#include "Line.h"
#include "..\Safe.h"
#include "..\System\Log.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx9
	{

		DirectX9::DirectX9(HWND hWnd): m_d3dDeviceType(D3DDEVTYPE_HAL), m_d3dClearColor(D3DCOLOR_XRGB(0, 0, 0)), m_AdapterToUse(D3DADAPTER_DEFAULT), m_pDevice(nullptr),
			m_pSprite(nullptr), m_PParams()
		{    
			//create D3D object
			m_lpD3D = Direct3DCreate9(D3D_SDK_VERSION);

			//if creation failed
			if(!m_lpD3D)
				throw apiException();

			sys::log->Out(sys::LogModule::API, sys::LogType::INFO, "Successfully created D3D.");

			//set present params
			m_PParams.SwapEffect       = D3DSWAPEFFECT_DISCARD;
			m_PParams.hDeviceWindow    = hWnd;
			m_PParams.Windowed         = true;
			m_PParams.Flags = D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;
			m_PParams.BackBufferFormat = D3DFMT_X8R8G8B8;
			m_PParams.BackBufferCount = 1;
			//m_PParams.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
			m_PParams.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
			m_PParams.AutoDepthStencilFormat = D3DFMT_D24S8;
			m_PParams.EnableAutoDepthStencil = TRUE;
			//m_PParams.FullScreen_RefreshRateInHz = 60;

			//check all adapters
			for (unsigned int Adapter=0; Adapter<m_lpD3D->GetAdapterCount(); Adapter++)
			{
				D3DADAPTER_IDENTIFIER9 identifier;
				m_lpD3D->GetAdapterIdentifier(Adapter, 0, &identifier);
				//if adapter is from PerfHUD
				if (strstr(identifier.Description, "PerfHUD") != 0)
				{
					sys::log->Out(sys::LogModule::API, sys::LogType::INFO, "Using PerfHud adapter");
					m_AdapterToUse = Adapter;
					m_d3dDeviceType = D3DDEVTYPE_REF;
					break;
				}
			}
    
			//get number of display modes with XRGB format
			D3DFORMAT format = D3DFMT_X8R8G8B8;
			unsigned int NumDisplayModes = m_lpD3D->GetAdapterModeCount(m_AdapterToUse, format);

			D3DDISPLAYMODE displayMode;
			//enum display modes
			for(unsigned int i = 0; i<NumDisplayModes; i++)
			{
				//get this adapter mode
				m_lpD3D->EnumAdapterModes(m_AdapterToUse, format, i, &displayMode);
				//add display mode to list
				m_vDisplayModes.push_back(displayMode);

				sys::log->Out(sys::LogModule::API, sys::LogType::INFO, "Display mode found: ", displayMode.Width, "x", displayMode.Height, displayMode.RefreshRate, " hz");
			}

			//set back buffer to largest possible display mode
			m_PParams.BackBufferWidth = (m_vDisplayModes.end()-1)->Width;
			m_PParams.BackBufferHeight = (m_vDisplayModes.end()-1)->Height;

			m_lpD3D->GetDeviceCaps(m_AdapterToUse, m_d3dDeviceType, &m_d3dCaps);

			CreateDevice();
			CreateSprite();
			CreateLine();
		}

		DirectX9::~DirectX9(void)
		{
			//delete device object
			delete m_pDevice;
			//release d3d object
			SAFE_RELEASE(m_lpD3D);
		}

		void DirectX9::CreateDevice(void)
		{
			LPDIRECT3DDEVICE9 lpDevice;
			//create device
			HRESULT hr = m_lpD3D->CreateDevice(m_AdapterToUse, m_d3dDeviceType, m_PParams.hDeviceWindow, D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_PParams, &lpDevice);
			//if device creation failed
			if(FAILED(hr))
				throw apiException();

			sys::log->Out(sys::LogModule::API, sys::LogType::INFO, "Successfully created D3D-device!");

			//new device object with device and largest display mode
			m_pDevice = new d3d::Device(lpDevice, *(m_vDisplayModes.end()-1));
		}

		void DirectX9::CreateSprite(void)
		{
			m_pSprite = new d3d::Sprite(*m_pDevice);
		}

		void DirectX9::CreateLine(void)
		{
			m_pLine= new d3d::Line(*m_pDevice);
		}

		void DirectX9::Present(void) const
		{
			//present scene content to front buffer
			m_pDevice->Present();
			//clear back and z buffer
			m_pDevice->SetScissorRect(nullptr);
		}

		void DirectX9::Reset(void)
		{
			m_pSprite->Reset();
			m_pLine->Reset();
		}

		d3d::Device& DirectX9::GetDevice(void) const
		{
			return *m_pDevice;
		}

		d3d::Sprite* DirectX9::GetSprite(void) const
		{
			return m_pSprite;
		}

		d3d::Line* DirectX9::GetLine(void) const
		{
			return m_pLine;
		}

		const DirectX9::DisplayModeVector& DirectX9::GetDisplayModes(void) const
		{
			return m_vDisplayModes;
		}

		bool DirectX9::IsFullscreen(void) const
		{
			return !m_PParams.Windowed;
		}

		void DirectX9::Begin(void) const
		{
			//begin scene
			m_pDevice->BeginScene();
		}

		void DirectX9::End(void) const
		{
			//end scene
			m_pDevice->EndScene();
		}

		void DirectX9::OnResize(unsigned int width, unsigned int height)
		{
			m_PParams.BackBufferWidth = width;
			m_PParams.BackBufferHeight = height;

			m_pSprite->OnReset();
			m_pLine->OnReset();
			m_pDevice->Resize(m_PParams);
			m_pSprite->OnRecreate();
			m_pLine->OnRecreate();
		}

	}
}

#endif