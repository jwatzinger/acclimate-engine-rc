#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>
#include <vector>
#include <Windows.h>

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{
			class Device;
			class Sprite;
			class Line;
		}

		class DirectX9
		{
			typedef std::vector<D3DDISPLAYMODE> DisplayModeVector;
		public:
			DirectX9(HWND hWnd);
			~DirectX9(void);

			d3d::Device& GetDevice(void) const;
			d3d::Sprite* GetSprite(void) const;
			d3d::Line* GetLine(void) const;
			const DisplayModeVector& GetDisplayModes(void) const;
			bool IsFullscreen(void) const;

			void Begin(void) const;
			void End(void) const;
			void Present(void) const;
			void Reset(void);
			void OnResize(unsigned int width, unsigned int height);

		private:

			void CreateDevice(void);
			void CreateSprite(void);
			void CreateLine(void);

			unsigned int m_AdapterToUse;    //adapter id

			DisplayModeVector m_vDisplayModes;  //display mode vector
	
			LPDIRECT3D9 m_lpD3D;               //d3d object

			D3DCAPS9 m_d3dCaps;                //d3d caps 
			D3DPRESENT_PARAMETERS m_PParams;   //present parameters
			D3DDEVTYPE m_d3dDeviceType;        //d3d device type
			D3DCOLOR m_d3dClearColor;          //backbuffer clear color

			d3d::Device* m_pDevice;
			d3d::Sprite* m_pSprite;
			d3d::Line* m_pLine;
		};

	}
}

#endif