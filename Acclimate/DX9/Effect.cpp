#include "Effect.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "Texture.h"
#include "ShaderInclude.h"
#include "..\Safe.h"
#include "..\System\Log.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			Effect::Effect(const Device& device, const LPCWSTR& lpShaderName, LPCSTR pData, size_t dataLenght, const MacroVector& vPermutations, const BufferSizeVector& vBufferSizes):
				m_vBufferSizes(vBufferSizes)
			{
				HRESULT hr;
				LPD3DXBUFFER lpVertexBuffer = nullptr;
				LPD3DXBUFFER lpErrors = nullptr;

				ShaderInclude include(lpShaderName);

				//compile shader from file
				hr = D3DXCompileShader(pData, dataLenght, (D3DXMACRO*)&vPermutations[0], &include, "mainVS", "vs_3_0", 0, &lpVertexBuffer, &lpErrors, nullptr);
				//check if compilation failed
				if(FAILED(hr))
				{
					//ouptut error messages for debugging
					char *ErrorMessage = (char *)lpErrors->GetBufferPointer();
					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, ErrorMessage);
					//release error buffer
					lpErrors->Release();
					//throw exception
					throw apiException();
				}

				//create vertex shader from buffer
				hr = device.GetDevice()->CreateVertexShader((DWORD*)lpVertexBuffer->GetBufferPointer(), &m_lpVertexShader);
				//throw exception if creation failed
				if(FAILED(hr))
					throw apiException();

				//release vertex shader buffer
				lpVertexBuffer->Release();

				LPD3DXBUFFER lpPixelBuffer;
				//compile pixel shader from file
				hr = D3DXCompileShader(pData, dataLenght, (D3DXMACRO*)&vPermutations[0], &include, "mainPS", "ps_3_0", 0, &lpPixelBuffer, &lpErrors, nullptr);
				//check if compilation has failed
				if(FAILED(hr))
				{
					//ouput error message for debugging
					char *ErrorMessage = (char *)lpErrors->GetBufferPointer();
					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, ErrorMessage);
					//release error buffer
					lpErrors->Release();
					//throw exeception
					throw apiException();
				}

				//create pixel shader from buffer
				hr = device.GetDevice()->CreatePixelShader((DWORD*)lpPixelBuffer->GetBufferPointer(), &m_lpPixelShader);
				//throw exception if creation failed
				if(FAILED(hr))
					throw apiException();

				//release pixel shader buffer
				lpPixelBuffer->Release();
	
			}


			Effect::~Effect(void)
			{
				//release shader
				SAFE_RELEASE(m_lpVertexShader);
				SAFE_RELEASE(m_lpPixelShader);
			}

			LPDIRECT3DVERTEXSHADER9 Effect::GetVertexShader(void) const
			{
				//return vertex shader
				return m_lpVertexShader;
			}

			LPDIRECT3DPIXELSHADER9 Effect::GetPixelShader(void) const
			{
				//return pixel shader
				return m_lpPixelShader;
			}


			size_t Effect::GetVBufferSize(unsigned int id) const
			{
				return m_vBufferSizes[0][id];
			}

			size_t Effect::GetPBufferSize(unsigned int id) const
			{
				return m_vBufferSizes[1][id];
			}

		}
	}
}

#endif