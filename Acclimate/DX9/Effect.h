#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include <vector>

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Texture;
			class Device;

			class Effect
			{
			public:
				typedef std::vector<D3DXMACRO> MacroVector;
				typedef std::vector<std::vector<unsigned int>> BufferSizeVector;

				Effect(const Device& device, const LPCWSTR& lpShaderName, LPCSTR pData, size_t dataLenght, const MacroVector& vPermutations, const BufferSizeVector& vBufferSizes);
				~Effect(void);

				LPDIRECT3DVERTEXSHADER9 GetVertexShader(void) const;
				LPDIRECT3DPIXELSHADER9 GetPixelShader(void) const;
				size_t GetVBufferSize(unsigned int id) const;
				size_t GetPBufferSize(unsigned int id) const;

			private:
	
				LPDIRECT3DVERTEXSHADER9 m_lpVertexShader;
				LPDIRECT3DPIXELSHADER9 m_lpPixelShader;

				BufferSizeVector m_vBufferSizes;
	
			};

		}
	}
}

#endif

