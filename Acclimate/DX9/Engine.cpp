#include "Engine.h"

#ifdef ACL_API_DX9

#include "..\Core\Window.h"
#include "..\System\Exception.h"
#include "..\Math\Vector.h"

#include "DirectX9.h"
#include "Sprite.h"
#include "Line.h"
#include "Device.h"
#include "Texture.h"
#include "Gfx\Line.h"
#include "Gfx\Text.h"
#include "Gfx\Sprite.h"
#include "Gfx\SpriteBatch.h"
#include "Gfx\EffectLoader.h"
#include "Gfx\TextureLoader.h"
#include "Gfx\MaterialLoader.h"
#include "Gfx\MeshLoader.h"
#include "Gfx\FontLoader.h"
#include "Gfx\ZBufferLoader.h"
#include "Gfx\GeometryCreator.h"
#include "Gfx\RenderStateHandler.h"
#include "Gfx\CbufferLoader.h"
#include "Render\Queue.h"
#include "Render\Renderer.h"
#include "..\Gfx\TextureInternalAccessor.h"

namespace acl
{
	namespace dx9
	{

		Engine::Engine(HINSTANCE hInstance) : BaseEngine(hInstance)
		{
			Queue::Init();
		}

		Engine::~Engine(void)
		{
			delete m_pDirectX;
		}

		BaseEngine::VideoModeVector Engine::OnSetupAPI(void)
		{
			try
			{
				m_pDirectX = new DirectX9(m_pWindow->GethWnd());
			}
			catch (apiException&)
			{
				return BaseEngine::VideoModeVector();
			}

			const gui::Context& guiContext = m_guiPackage.GetContext();

			AclDevice& device = m_pDirectX->GetDevice();

			m_pCbufferLoader = new CbufferLoader();

			//setup renderer
			m_pRenderer = new Renderer(*m_pDirectX, *m_pCbufferLoader);

			/*****************************************************
			* GFX
			*****************************************************/

			m_pRenderStateHandler = new RenderStateHandlerInstance;

			// create resource loader
			m_pGeometryCreator = new GeometryCreator(device);
			m_pEffectLoader = new EffectLoader(device, m_effects);
			m_pTextureLoader = new TextureLoader(device, m_textures);
			m_pMeshLoader = new MeshLoader(device, m_textures, m_meshes, *m_pGeometryCreator);
			m_pMaterialLoader = new MaterialLoader(m_effects, m_textures, m_materials);
			m_pFontLoader = new FontLoader(*m_pDirectX->GetSprite(), m_fonts, *m_pTextureLoader);
			m_pZbufferLoader = new ZBufferLoader(device, m_zbuffers);

			// sprite
			m_pSprite = new Sprite(*m_pDirectX->GetSprite(), nullptr, *m_pGeometryCreator);
			m_pSpriteBatch = new SpriteBatch(*m_pDirectX->GetSprite(), m_fonts, *m_pFontLoader, *m_pGeometryCreator);
			m_pText = new Text(m_fonts);
			m_pLine = new Line(*m_pDirectX->GetLine(), *m_pGeometryCreator);

			VideoModeVector vVideoModes;
			auto vDisplayModes = m_pDirectX->GetDisplayModes();
			for(auto& mode : vDisplayModes)
			{
				vVideoModes.emplace_back(mode.Width, mode.Height, (float)mode.RefreshRate);
			}
			return vVideoModes;
		}

		bool Engine::OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen)
		{
			auto& device = m_pDirectX->GetDevice();
			auto mode = device.GetDisplayMode();
			vOldSize = math::Vector2(mode.Width, mode.Height);
			if(vSize != vOldSize || isFullscreen != m_pDirectX->IsFullscreen())
			{
				for(auto& texture : m_textures.Map())
				{
					d3d::Texture& d3dTexture = gfx::getTexture(*texture.second);
					d3dTexture.OnReset();
				}

				m_pDirectX->OnResize(vSize.x, vSize.y);
				return true;
			}
			else
				return false;
		}

	}
}

#endif