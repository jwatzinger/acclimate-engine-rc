#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\BaseEngine.h"

namespace acl
{
	namespace dx9
	{
		class DirectX9;

		class Engine :
			public BaseEngine
		{
		public:
			Engine(HINSTANCE hInstance);
			~Engine(void);

		private:
			
			VideoModeVector OnSetupAPI(void) override;
			bool OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen) override;

			DirectX9* m_pDirectX;
		};

	}
}

#endif