#include "Font.h"

#ifdef ACL_API_DX9

#include "Sprite.h"
#include "..\Math\Rect.h"
#include "..\Gfx\FontFile.h"
#include "..\Gfx\Color.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			Font::Font(Sprite& sprite, const gfx::FontFile& file) : m_file(file), m_pSprite(&sprite)
			{
			}

			const VertexBuffer& Font::GetVertexBuffer(void) const
			{
				return m_pSprite->GetVertexBuffer();
			}

			const IndexBuffer& Font::GetIndexBuffer(void) const
			{
				return m_pSprite->GetIndexBuffer();
			}

			const gfx::FontFile& Font::GetFontFile(void) const
			{
				return m_file;
			}

			const float recipColor = 1.0f / 255.0f;

			size_t Font::DrawString(const math::Rect& rect, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color)
			{
				if(!stText.size())
					return -1;

				const size_t textSize = stText.size();

				const unsigned int fontHeight = m_file.GetFontHeight();

				// horizontal center
				int leftOffset = 0;
				if(dFlags & DT_CENTER)
				{
					const unsigned int textWidth = m_file.CalculateWidth(stText);
					leftOffset = (int)((rect.width - (int)textWidth) * 0.5f);
				}
				else if(dFlags & DT_RIGHT)
				{
					const unsigned int textWidth = m_file.CalculateWidth(stText);
					leftOffset = rect.width - textWidth;
				}

				// vertical center
				int topOffset = 0;
				if(dFlags & DT_VCENTER)
					topOffset = (int)((rect.height - (int)(fontHeight - 4)) * 0.5f);
				else if(dFlags & DT_BOTTOM)
					topOffset = rect.height - fontHeight;

				const math::Vector2f& vInvHalfScreenSize = m_pSprite->GetInvHalfScreenSize();
				const math::Vector2f& vInvScreenSize = m_pSprite->GetInvScreenSize();

				// vertices
				float leftVertex = (rect.x + leftOffset)  * vInvHalfScreenSize.x - 1.0f - vInvScreenSize.x;
				const float topVertex = -(rect.y + topOffset) * vInvHalfScreenSize.y + 1.0f - vInvScreenSize.y;
				const float bottomVertex = topVertex - fontHeight * vInvHalfScreenSize.y;

				// color
				const float r = color.r * recipColor;
				const float g = color.g * recipColor;
				const float b = color.b * recipColor;
				const float a = color.a * recipColor;

				size_t id = 0;
				for(auto glyph : stText)
				{
					const gfx::CharData& charData = m_file.GetCharData(glyph);

					float rightVertex = leftVertex + charData.width * vInvHalfScreenSize.x;

					SpriteVertex Vertices[] =
					{
						{ leftVertex, topVertex, 0.5f, charData.ul, charData.vt, r, g, b, a },
						{ rightVertex, topVertex, 0.5f, charData.ur, charData.vt, r, g, b, a },
						{ rightVertex, bottomVertex, 0.5f, charData.ur, charData.vb, r, g, b, a },
						{ leftVertex, bottomVertex, 0.5f, charData.ul, charData.vb, r, g, b, a },
					};

					id = m_pSprite->Draw(Vertices, 1);

					leftVertex = rightVertex;
				}

				return id - textSize + 1;
			}

		}
	}
}

#endif