#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <string>
#include "..\Gfx\FontFile.h"
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

	namespace math
	{
		struct Rect;
	}

	namespace dx9
	{
		namespace d3d
		{

			class Device;
			class Sprite;
			class VertexBuffer;
			class IndexBuffer;

			class Font
			{
			public:
				Font(Sprite& sprite, const gfx::FontFile& file);

				const VertexBuffer& GetVertexBuffer(void) const;
				const IndexBuffer& GetIndexBuffer(void) const;
				const gfx::FontFile& GetFontFile(void) const;

				size_t DrawString(const math::Rect& rect, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color);

			private:

				Sprite* m_pSprite;

				gfx::FontFile m_file;
			};

		}
	}
}

#endif