#include "Cbuffer.h"

#ifdef ACL_API_DX9

#include "..\ConstantBuffer.h"
#include "..\Render\States.h"
#include "..\..\Gfx\Defines.h"
#include "..\..\System\Assert.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace dx9
	{

		Cbuffer::Cbuffer(void): BaseCbuffer()
		{
			m_pBuffer = new AclCbuffer(GetData(), GetSize());
		}

		Cbuffer::Cbuffer(AclCbuffer& cbuffer) : BaseCbuffer(&cbuffer, cbuffer.GetSize())
		{
		}

		Cbuffer::Cbuffer(const Cbuffer& buffer) : BaseCbuffer(buffer)
		{
			if(buffer.m_pBuffer)
				m_pBuffer = new AclCbuffer(GetData(), GetSize());
		}

		Cbuffer::~Cbuffer(void)
		{
			delete m_pBuffer;
		}

		gfx::ICbuffer& Cbuffer::Clone(void) const
		{
			return *new Cbuffer(*this);
		}

		bool Cbuffer::OnNewBufferIsValid(const AclCbuffer* pNewBuffer) const
		{
			return m_pBuffer->GetSize() <= pNewBuffer->GetSize();
		}

		void Cbuffer::OnOverwrite(float* pData, size_t size)
		{
			m_pBuffer->SetBuffer(pData, size);
		}

		void Cbuffer::OnDeleteBuffer(const AclCbuffer* pBuffer) const
		{
			delete pBuffer;
		}

		void Cbuffer::Bind(render::StateGroup& states, gfx::CbufferType type, gfx::CbufferSlot slot) const
		{
			static_assert(gfx::CBUFFER_INSTANCE_ID == 0, "Instance cbuffer ID does not conform.");
			static_assert(gfx::CBUFFER_STAGE_ID == 1, "Stage cbuffer ID does not conform.");

			switch(type)
			{
			case gfx::CbufferType::VERTEX:
				switch(slot)
				{
				case gfx::CbufferSlot::INSTANCE:
					states.Add<CBufferV0>(*m_pBuffer);
					break;
				case gfx::CbufferSlot::STAGE:
					states.Add<CBufferV1>(*m_pBuffer);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			case gfx::CbufferType::PIXEL:
				switch(slot)
				{
				case gfx::CbufferSlot::INSTANCE:
					states.Add<CBufferP0>(*m_pBuffer);
					break;
				case gfx::CbufferSlot::STAGE:
					states.Add<CBufferP1>(*m_pBuffer);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		size_t Cbuffer::OnGetBufferSize(const AclCbuffer& buffer) const
		{
			return buffer.GetSize();
		}

	}
}

#endif