#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseCbuffer.h"

namespace acl
{
	namespace dx9
	{

		class Cbuffer final : 
			public gfx::BaseCbuffer
		{
		public:
			Cbuffer(void);
			Cbuffer(AclCbuffer& cbuffer);
			Cbuffer(const Cbuffer& buffer);
			~Cbuffer(void);

			gfx::ICbuffer& Clone(void) const override;

			void Bind(render::StateGroup& states, gfx::CbufferType type, gfx::CbufferSlot slot) const override;

		private:

			bool OnNewBufferIsValid(const AclCbuffer* pNewBuffer) const override;
			void OnOverwrite(float* pData, size_t size) override;
			void OnDeleteBuffer(const AclCbuffer* pBuffer) const override;
			size_t OnGetBufferSize(const AclCbuffer& buffer) const override;
		};

	}
}

#endif