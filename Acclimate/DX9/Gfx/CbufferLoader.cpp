#include "CbufferLoader.h"

#ifdef ACL_API_DX9

#include "Cbuffer.h"
#include "..\ConstantBuffer.h"

namespace acl
{
	namespace dx9
	{

		CbufferLoader::CbufferLoader(void)
		{
		}

		gfx::ICbuffer& CbufferLoader::OnCreateFromSize(size_t numBytes) const
		{
			float* pBuffer = new float[numBytes / sizeof(float)];
			d3d::ConstantBuffer* pDx11Buffer = new d3d::ConstantBuffer(pBuffer, numBytes);

			return *new Cbuffer(*pDx11Buffer);
		}

		gfx::ICbuffer& CbufferLoader::OnCreateFromBuffer(AclCbuffer& buffer) const
		{
			return *new Cbuffer(buffer);
		}

	}
}

#endif