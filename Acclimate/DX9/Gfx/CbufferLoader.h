#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseCbufferLoader.h"

namespace acl
{
	namespace dx9
	{

		class CbufferLoader final :
			public gfx::BaseCbufferLoader
		{
		public:
			CbufferLoader(void);

		private:

			gfx::ICbuffer& OnCreateFromSize(size_t numBytes) const override;
			gfx::ICbuffer& OnCreateFromBuffer(AclCbuffer& buffer) const override;

			const AclDevice* m_pDevice;
		};

	}
}

#endif