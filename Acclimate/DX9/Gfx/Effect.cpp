#include "Effect.h"

#ifdef ACL_API_DX9

#include "..\Render\States.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx9
    {

        DWORD FilterToDX9(gfx::TextureFilter filter)
		{
			switch(filter)
			{
			case gfx::TextureFilter::POINT:
				return D3DTEXF_POINT;
			case gfx::TextureFilter::LINEAR:
				return D3DTEXF_LINEAR;
			case gfx::TextureFilter::ANISOTROPIC:
				return D3DTEXF_ANISOTROPIC;
			default:
				return D3DTEXF_POINT;
			}
		}

		DWORD MipToDX9(gfx::MipFilter filter)
		{
			switch(filter)
			{
			case gfx::MipFilter::NONE:
				return D3DTEXF_NONE;
			case gfx::MipFilter::POINT:
				return D3DTEXF_POINT;
			case gfx::MipFilter::LINEAR:
				return D3DTEXF_LINEAR;
			default:
				return D3DTEXF_NONE;
			}
		}

		DWORD AdressToDX9(gfx::AdressMode mode)
		{
			switch(mode)
			{
			case gfx::AdressMode::WRAP:
				return D3DTADDRESS_WRAP;
			case gfx::AdressMode::MIRROR:
				return D3DTADDRESS_MIRROR;
			case gfx::AdressMode::CLAMP:
				return D3DTADDRESS_CLAMP;
			case gfx::AdressMode::BORDER:
				return D3DTADDRESS_BORDER;
			default:
				return D3DTADDRESS_WRAP;
			}
		}

		D3DCULL ToCull(gfx::CullMode cull)
		{
			switch(cull)
			{
			case gfx::CullMode::NONE:
				return D3DCULL_NONE;
			case gfx::CullMode::BACK:
				return D3DCULL_CCW;
			case gfx::CullMode::FRONT:
				return D3DCULL_CW;
			default:
				return D3DCULL_NONE;
			}
		}

		D3DFILLMODE ToFill(gfx::FillMode fill)
		{
			switch(fill)
			{
			case gfx::FillMode::SOLID:
				return D3DFILL_SOLID;
			case gfx::FillMode::WIREFRAME:
				return D3DFILL_WIREFRAME;
			default:
				return D3DFILL_SOLID;
			}
		}

		void deleteEffect(AclEffect* pEffect)
		{
			delete pEffect;
		}

		Effect::Effect(gfx::EffectFile& file, const gfx::IEffectLoader& loader) : BaseEffect(file, loader, deleteEffect)
        {
        }

		void Effect::OnSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			DWORD min = FilterToDX9(minFilter);
			DWORD mag = FilterToDX9(magFilter);
			DWORD mip = MipToDX9(mipFilter);
			DWORD adressU = AdressToDX9(u);
			DWORD adressV = AdressToDX9(v);

			switch(index)
			{
			case 0:
				m_states.Add<SamplerState0>(min, mag, mip, adressU, adressV);
				break;
			case 1:
				m_states.Add<SamplerState1>(min, mag, mip, adressU, adressV);
				break;
			case 2:
				m_states.Add<SamplerState2>(min, mag, mip, adressU, adressV);
				break;
			case 3:
				m_states.Add<SamplerState3>(min, mag, mip, adressU, adressV);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void Effect::OnVertexSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			DWORD min = FilterToDX9(minFilter);
			DWORD mag = FilterToDX9(magFilter);
			DWORD mip = MipToDX9(mipFilter);
			DWORD adressU = AdressToDX9(u);
			DWORD adressV = AdressToDX9(v);

			switch(index)
			{
			case 0:
				m_states.Add<VertexSamplerState0>(min, mag, mip, adressU, adressV);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

        void Effect::SetAlphaState(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend)
        {
	        m_states.Add<AlphaBlendState>(bEnable, static_cast<int>(srcBlend), static_cast<int>(destBlend), static_cast<int>(blendFunc), static_cast<int>(alphaSrcBlend), static_cast<int>(alphaDestBlend));
        }

        void Effect::SetDepthState(bool bEnable, bool bWrite)
        {
	        m_states.Add<DepthState>(bEnable, bWrite);
        }

		void Effect::SetRasterizerState(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable)
		{
			m_states.Add<CullState>(ToCull(cull));
			m_states.Add<FillMode>(ToFill(fill));
			m_states.Add<ScissorTest>(bScissorEnable);
		}

		AclCbuffer* Effect::OnCreateVCbuffer(AclEffect& effect, unsigned int id) const
		{ 
			if(auto bufferSize = effect.GetVBufferSize(id))
				return new AclCbuffer(nullptr, bufferSize);
			else
				return nullptr;
		}

		AclCbuffer* Effect::OnCreatePCbuffer(AclEffect& effect, unsigned int id) const
		{ 
			if(auto bufferSize = effect.GetPBufferSize(id))
				return new AclCbuffer(nullptr, bufferSize);
			else
				return nullptr;
		}

		void Effect::OnBindShader(render::StateGroup& group, AclEffect& effect) const
		{
			group.Add<BindShader>(effect);
		}

		void Effect::OnDeleteEffect(AclEffect& effect)
		{
			delete &effect;
		}

    }
}

#endif