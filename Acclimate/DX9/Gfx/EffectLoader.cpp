#include "EffectLoader.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include "Effect.h"
#include "..\Effect.h"
#include "..\..\Gfx\EffectReflector.h"
#include "..\..\Gfx\EffectFile.h"
#include "..\..\System\Convert.h"

namespace acl
{
    namespace dx9
    {

        EffectLoader::EffectLoader(const AclDevice& device, gfx::Effects& effects): BaseEffectLoader(effects, m_parser),
			m_pDevice(&device)
        {
        }

		gfx::IEffect& EffectLoader::OnCreateEffect(gfx::EffectFile& file) const
		{
			return *new Effect(file, *this);
		}

		AclEffect& EffectLoader::OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const
		{
			// macro permutations
			const gfx::EffectFile::PermutationVector vPermutationNames = file.GetPermutations(permutation);

			std::vector<std::string> vValues;
			vValues.reserve(vPermutationNames.size());

			d3d::Effect::MacroVector vMacros;	
			for(auto& macro : vPermutationNames)
			{
				vValues.push_back(conv::ToStringA(macro.value));
				vMacros.emplace_back(D3DXMACRO{ macro.stName.c_str(), (*vValues.rbegin()).c_str() });
			}
			vMacros.emplace_back(D3DXMACRO{ nullptr, nullptr });

			// get buffer size through reflection
			gfx::EffectReflector reflector(file);
			const auto reflectionData = reflector.Reflect(vPermutationNames);

			d3d::Effect::BufferSizeVector vBufferSizes(2);
			unsigned int shader = 0;
			for(auto& vSizes : vBufferSizes)
			{
				vSizes.resize(5);
				vSizes[2] = reflectionData.bufferSize[shader][0];
				vSizes[3] = reflectionData.bufferSize[shader][1];
				shader++;
			}

			return *new d3d::Effect(*m_pDevice, file.GetFilename().c_str(), file.GetData(), file.GetDataLength(), vMacros, vBufferSizes);
		}

    }
}

#endif