#pragma once
#include "..\..\Gfx\BaseEffectLoader.h"

#ifdef ACL_API_DX9

#include "HLSL9Parser.h"

namespace acl
{
    namespace dx9
    {

        class EffectLoader final :
			public gfx::BaseEffectLoader
        {
        public:
            EffectLoader(const AclDevice& device, gfx::Effects& effects);

        private:

			gfx::IEffect& OnCreateEffect(gfx::EffectFile& file) const override;
			AclEffect& OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const override;

			HLSL9Parser m_parser;
            const AclDevice* m_pDevice;
        };
        
    }
}

#endif

