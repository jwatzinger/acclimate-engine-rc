#include "Font.h"

#ifdef ACL_API_DX9

#include "..\Font.h"

namespace acl
{
    namespace dx9
    {

		Font::Font(AclFont& font, const gfx::ITexture& texture) : m_pFont(&font),
			m_pTexture(&texture)
        {
        }

        Font::~Font(void)
        {
            delete m_pFont;
        }

		size_t Font::DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color) const
		{
			// TODO: use z
			return m_pFont->DrawString(rect, stText, dFlags, color);
		}

		const render::StateGroup& Font::GetState(void) const
		{
			return m_states;
		}

		const gfx::ITexture& Font::GetTexture(void) const
		{
			return *m_pTexture;
		}

		const gfx::FontFile& Font::GetFontFile(void) const
		{
			return m_pFont->GetFontFile();
		}

    }
}

#endif