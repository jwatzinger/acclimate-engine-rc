#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\IFont.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
    namespace dx9
    {

        class Font :
			public gfx::IFont
        {
        public:
            Font(AclFont& font, const gfx::ITexture& texture);
            ~Font(void);

			size_t DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color) const override;
			const render::StateGroup& GetState(void) const override;
			const gfx::ITexture& GetTexture(void) const override;
			const gfx::FontFile& GetFontFile(void) const override;

        private:

            AclFont* m_pFont;

			const gfx::ITexture* m_pTexture;

			render::StateGroup m_states;
        };

    }
}

#endif