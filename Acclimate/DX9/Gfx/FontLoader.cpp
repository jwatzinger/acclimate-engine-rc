#include "FontLoader.h"

#ifdef ACL_API_DX9

#include "Font.h"
#include "..\Font.h"

namespace acl
{
	namespace dx9
	{

		FontLoader::FontLoader(AclSprite& sprite, gfx::Fonts& fonts, gfx::ITextureLoader& textureLoader) : BaseFontLoader(fonts, textureLoader), m_pSprite(&sprite)
		{
		}

		gfx::IFont& FontLoader::OnCreateFont(gfx::FontFile& file, gfx::ITexture& texture) const
		{
			AclFont& dx9Font = *new AclFont(*m_pSprite, file);
			return *new Font(dx9Font, texture);
		}

	}
}

#endif

