#include "Geometry.h"

#ifdef ACL_API_DX9

#include <d3d9.h>
#include "..\Render\States.h"
#include "..\VertexDeclaration.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx9
	{

		BYTE convertToType(gfx::AttributeType type, unsigned int numElements);
		BYTE convertToUsage(gfx::AttributeSemantic semantic);

		Geometry::Geometry(const AclDevice& device, gfx::PrimitiveType type, const AttributeVector& vAttributes) : BaseGeometry(type, vAttributes),
			m_pDeclaration(nullptr), m_pDevice(&device)
		{
			std::vector<D3DVERTEXELEMENT9> vElements(vAttributes.size());
			unsigned int offset = 0;
			unsigned int id = 0;
			unsigned int lastStream = 0;
			for(auto& attribute : vAttributes)
			{
				if(lastStream != attribute.buffer)
				{
					lastStream = attribute.buffer;
					offset = 0;
				}

				D3DVERTEXELEMENT9& element = vElements[id];
				element.Stream = attribute.buffer;
				element.Offset = offset;
				element.Type = convertToType(attribute.type, attribute.numElements);
				element.Method = D3DDECLMETHOD_DEFAULT;
				element.Usage = convertToUsage(attribute.semantic);
				element.UsageIndex = attribute.slot;

				offset += CalculateAttributeSize(attribute);
				id++;
			}

			vElements.push_back(D3DDECL_END());

			m_pDeclaration = new d3d::VertexDeclaration(device, &vElements[0]);
		}

		Geometry::Geometry(const Geometry& geometry):
			Geometry(*geometry.m_pDevice, geometry.GetPrimitiveType(), geometry.GetAttributes())
		{
		}

		Geometry::~Geometry(void)
		{
			delete m_pDeclaration;
		}

		gfx::IGeometry& Geometry::Clone(void) const
		{
			return *new Geometry(*this);
		}

		void Geometry::Bind(render::StateGroup& states) const
		{
			states.Add<SetVertexDeclaration>(*m_pDeclaration);
		}

		BYTE convertToType(gfx::AttributeType type, unsigned int numElements)
		{
			switch(type)
			{
			case gfx::AttributeType::FLOAT:
				return D3DDECLTYPE_FLOAT1 + numElements-1;
			case gfx::AttributeType::UBYTE4:
				ACL_ASSERT(numElements == 4);
				return D3DDECLTYPE_UBYTE4;
			default:
				ACL_ASSERT(false);
			}

			return D3DDECLTYPE_FLOAT1;
		}

		BYTE convertToUsage(gfx::AttributeSemantic semantic)
		{
			switch(semantic)
			{
			case gfx::AttributeSemantic::POSITION:
				return D3DDECLUSAGE_POSITION;
			case gfx::AttributeSemantic::TEXCOORD:
				return D3DDECLUSAGE_TEXCOORD;
			default:
				ACL_ASSERT(false);
			}

			return D3DDECLUSAGE_POSITION;
		}

	}
}



#endif