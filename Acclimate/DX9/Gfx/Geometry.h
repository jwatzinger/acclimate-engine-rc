#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseGeometry.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{
			class VertexDeclaration;
		}

		class Geometry :
			public gfx::BaseGeometry
		{
		public:
			Geometry(const AclDevice& device, gfx::PrimitiveType type, const AttributeVector& vAttributes);
			Geometry(const Geometry& geometry);
			~Geometry(void);

			gfx::IGeometry& Clone(void) const override;

			void Bind(render::StateGroup& states) const override;

		private:

			d3d::VertexDeclaration* m_pDeclaration;
			const d3d::Device* m_pDevice;
		};

	}
}

#endif