#include "GeometryCreator.h"

#ifdef ACL_API_DX9

#include "Geometry.h"

namespace acl
{
	namespace dx9
	{

		GeometryCreator::GeometryCreator(const AclDevice& device) : m_pDevice(&device)
		{
		}

		gfx::IGeometry& GeometryCreator::CreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes)
		{
			return *new Geometry(*m_pDevice, type, vAttributes);
		}

	}
}

#endif