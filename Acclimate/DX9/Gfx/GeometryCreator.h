#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\IGeometryCreator.h"

namespace acl
{
	namespace dx9
	{

		class GeometryCreator :
			public gfx::IGeometryCreator
		{
		public:

			GeometryCreator(const AclDevice& device);

			gfx::IGeometry& CreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes) override;

		private:

			const AclDevice* m_pDevice;
		};

	}
}

#endif