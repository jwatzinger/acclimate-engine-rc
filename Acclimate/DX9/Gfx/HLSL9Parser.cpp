#include "HLSL9Parser.h"

#ifdef ACL_API_DX9

#include "..\..\System\Convert.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx9
	{

		void HLSL9Parser::OnVertexBegin(std::string& stOut) const
		{
			stOut += "#pragma pack_matrix( row_major )\n";
		}

		void HLSL9Parser::OnPixelBegin(std::string& stOut) const
		{
		}

		void HLSL9Parser::OnBeginInputBlock(std::string& stOut, gfx::InputRegister reg, gfx::ShaderType type) const
		{
		}

		void HLSL9Parser::OnEndInputBlock(std::string& stOut) const
		{
		}

		void HLSL9Parser::OnInputVariable(std::string& stOut, const std::string& stType, const std::string& stName, gfx::InputRegister reg, unsigned int id) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;

			size_t offset;
			switch(reg)
			{
			case gfx::InputRegister::INSTANCE:
				offset = 0;
				break;
			case gfx::InputRegister::STAGE:
				offset = 160;
				break;
			default:
				ACL_ASSERT(false);
			}

			stOut += ": register(c" + conv::ToA(conv::ToString(offset+id)) + ')';
		}

		void HLSL9Parser::OnBeginIO(std::string& stOut, gfx::InOut io) const
		{
			// add struct to shader
			if(io == gfx::InOut::I)
				stOut += "struct VERTEX_IN\n{\n";
			else
				stOut += "struct VERTEX_OUT\n{\n";
		}

		void HLSL9Parser::OnBeginPixelOut(std::string& stOut) const
		{
			stOut += "struct PIXEL_OUT\n{\n";
		}

		void HLSL9Parser::OnIOVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id, gfx::InOut io) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
			if(id == 0)
				stOut += ": POSITION0;";
			else
				stOut += ": TEXCOORD" + conv::ToA(conv::ToString(id - 1)) + ';';
		}

		void HLSL9Parser::OnPixelOutVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
			stOut += ": COLOR" + conv::ToA(conv::ToString(id))  + ';';
		}

		void HLSL9Parser::OnEndIO(std::string& stOut) const
		{
			stOut += "};\n\n";
		}

		void HLSL9Parser::OnTexture(std::string& stOut, gfx::TextureType& type, const std::string& stName, unsigned int id) const
		{
			const std::string stId = conv::ToA(conv::ToString(id));

			stOut += "sampler ";
			stOut += stName;
			stOut += ": register(s" + stId + ")";
		}

		std::string HLSL9Parser::OnConvertType(gfx::Type type) const
		{
			switch(type)
			{
			case gfx::Type::OUT_:
				return"Out";
			case gfx::Type::IN_:
				return "In";
			}

			return "";
		}

		void HLSL9Parser::OnExtention(std::string& stOut, const std::string& stFunction, const std::string& stName, unsigned int id) const
		{
			stOut += "// extention_entry\n";

			stOut += "#ifndef OVERWRITE_" + stName + "_EXT\n";

			stOut += stFunction + '\n';

			stOut += "#endif";
		}

		void HLSL9Parser::OnExtentionBegin(std::string& stOut, const std::string& stName, const std::string& stParent) const
		{
			stOut += "#define OVERWRITE_" + stParent + "_EXT\n";
		}

		void HLSL9Parser::OnExtentionEnd(std::string& stOut, const std::string& stName) const
		{
		}

		std::string HLSL9Parser::OnFunctionCall(const std::string& stName, const ArgumentVector& vArguments) const
		{
			std::string stFunction;
			if(stName == "Sample")
			{
				ACL_ASSERT(vArguments.size() == 2);
				stFunction = "tex2D(";
			}
			else if(stName == "SampleLOD")
			{
				ACL_ASSERT(vArguments.size() == 3);
				stFunction = "tex2Dlod(" + vArguments[0] + ", float4(" + vArguments[1] + "," + vArguments[2] + ", 0.0f))";
				return stFunction;
			}
			else if(stName == "clip")
			{
				ACL_ASSERT(vArguments.size() == 1);
				stFunction += "clip(" + vArguments[0] + "? -1 : 1)";
				return stFunction;
			}
			else
			{
				if(vArguments.size() == 1)
				{
					if(stName == "float2")
					{
						stFunction += "float2(" + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
					else if(stName == "float3")
					{
						stFunction += "float3(" + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
					else if(stName == "float4")
					{
						stFunction += "float4(" + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ", " + vArguments[0] + ")";
						return stFunction;
					}
				}

				auto pos = stName.find("extentions.");
				if(pos != std::string::npos)
				{
					const std::string stExtention = stName.substr(pos + 11);
					stFunction = stExtention + '(';
				}
				else
					stFunction = stName + '(';
			}


			for(auto& stArgument : vArguments)
			{
				stFunction += stArgument + ",";
			}

			if(auto size = vArguments.size())
				stFunction.pop_back();

			stFunction += ")";
			return stFunction;
		}

		void HLSL9Parser::OnMainDeclaration(std::string& stOut) const
		{
			stOut += "VERTEX_OUT mainVS(VERTEX_IN In)";
		}

		void HLSL9Parser::OnMainTop(std::string& stOut) const
		{
			stOut += "VERTEX_OUT Out = (VERTEX_OUT)0;";
		}

		void HLSL9Parser::OnMainBottom(std::string& stOut) const
		{
			stOut += "return Out;";
		}

		void HLSL9Parser::OnPixelMainDeclaration(std::string& stOut) const
		{
			stOut += "PIXEL_OUT mainPS(VERTEX_OUT In)";
		}

		void HLSL9Parser::OnPixelMainTop(std::string& stOut) const
		{
			stOut += "PIXEL_OUT Out = (PIXEL_OUT)0;";
		}

		void HLSL9Parser::OnPixelMainBottom(std::string& stOut) const
		{
			stOut += "return Out;";
		}

		void HLSL9Parser::OnPostProcess(std::string& stOut) const
		{
		}

	}
}

#endif
