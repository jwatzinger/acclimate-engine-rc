#include "InstancedMesh.h"

#ifdef ACL_API_DX9

#include "..\VertexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
    namespace dx9
    {

        InstancedMesh::InstancedMesh(gfx::IVertexBuffer& buffer, const gfx::IGeometry& geometry, size_t numInstances, IMesh& mesh): 
			BaseInstancedMesh(buffer, numInstances, mesh, geometry)
        {
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				SetDrawCall(i, mesh.GetDraw(i));
			}
        }

		InstancedMesh::InstancedMesh(const InstancedMesh& mesh) : BaseInstancedMesh(mesh)
		{
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				SetDrawCall(i, mesh.GetDraw(i));
			}
		}

		InstancedMesh::~InstancedMesh(void)
		{
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				RemoveDrawCall(i);
			}
		}

		gfx::IInstancedMesh& InstancedMesh::Clone(void) const
		{
			return *new InstancedMesh(*this);
		}

		void InstancedMesh::OnChangeInstanceCount(unsigned int count)
		{
			GetOwnStateGroup().Add<dx9::SetInstanceCount>(count);
		}

		void InstancedMesh::OnChangeInstanceBuffer(void)
		{
		}

    }
}

#endif