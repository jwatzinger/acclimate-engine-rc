#include "Line.h"

#ifdef ACL_API_DX9

#include "..\Line.h"
#include "..\Render\States.h"
#include "..\..\Gfx\IEffect.h"
#include "..\..\Render\Instance.h"

namespace acl
{
    namespace dx9
    {

		Line::Line(AclLine& line, gfx::IGeometryCreator& creator) : BaseLine(creator), 
			m_pLine(&line)
        {
            // set geometry resource binds
	        m_states.Add<BindVertexBuffer>(&m_pLine->GetVertexBuffer());
	        m_states.Add<BindIndexBuffer>(&m_pLine->GetIndexBuffer());
        }

        Line::Line(const Line& line): BaseLine(line), m_pLine(line.m_pLine)
        {
            // set geometry resource binds
	        m_states.Add<BindVertexBuffer>(&m_pLine->GetVertexBuffer());
	        m_states.Add<BindIndexBuffer>(&m_pLine->GetIndexBuffer());
        }

        Line& Line::Clone(void) const
        {
            // clones a new line object fromt his
            return *new Line(*this);
        }

		bool Line::OnDrawLine(render::Instance& instance, const math::Vector3& vFrom, const math::Vector3& vTo, const gfx::Color& color)
        {
			const size_t id = m_pLine->Draw(vFrom, vTo, color);

            // create and set draw call
			instance.CreateCall<DrawIndexedPrimitives>(RenderTypes::LINE, 2, id * 2, 0);

			return true;
        }

		bool Line::OnDrawLines(render::Instance& instance, const gfx::PointVector& vPoints, const gfx::Color& color)
		{
			if(vPoints.size() <= 1)
				return false;

			const size_t numElements = vPoints.size() - 1;
			size_t firstId = -1;

			for(unsigned int i = 0; i < numElements; i++)
			{
				const auto& vPoint = vPoints[i];
				const auto& vNextPoint = vPoints[i + 1];
				const size_t id = m_pLine->Draw(vPoint, vNextPoint, color);

				if(firstId == -1)
					firstId = id;
			}

			// create and set draw call
			instance.CreateCall<DrawPrimitives>(RenderTypes::LINE, 2 * numElements, firstId * 2);

			return true;
		}

    }
}

#endif