#include "Material.h"

#ifdef ACL_API_DX9

namespace acl
{
    namespace dx9
	{

		Material::Material(gfx::IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets) : BaseMaterial(effect, permutation, id, numSubsets)
        {
        }

    }
}

#endif