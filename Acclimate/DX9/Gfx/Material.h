#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseMaterial.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
		class IEffect;
	}

	namespace dx9
	{


		class Material final :
			public gfx::BaseMaterial
		{
		public:
			Material(gfx::IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets);

		};

	}
}

#endif