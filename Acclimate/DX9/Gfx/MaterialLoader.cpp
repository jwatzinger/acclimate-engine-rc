#include "MaterialLoader.h"

#ifdef ACL_API_DX9

#include "Material.h"

namespace acl
{
	namespace dx9
	{

		MaterialLoader::MaterialLoader(const gfx::Effects& effects, const gfx::Textures& textures, gfx::Materials& materials) :
			BaseMaterialLoader(effects, textures, materials)
		{
		}

		gfx::IMaterial& MaterialLoader::OnCreateMaterial(gfx::IEffect& effect, unsigned int permutation, size_t numSubsets, size_t id) const
		{
			return *new Material(effect, permutation, id, numSubsets);
		}

	}
}

#endif