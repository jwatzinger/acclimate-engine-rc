#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseMaterialLoader.h"

namespace acl
{
	namespace dx9
	{

		class MaterialLoader final :
			public gfx::BaseMaterialLoader
		{
		public:

			MaterialLoader(const gfx::Effects& effects, const gfx::Textures& textures, gfx::Materials& materials);

		private:

			gfx::IMaterial& OnCreateMaterial(gfx::IEffect& effect, unsigned int permutation, size_t numSubsets, size_t id) const override;
		};

	}
}

#endif