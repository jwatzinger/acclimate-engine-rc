#include "Mesh.h"

#ifdef ACL_API_DX9

#include "..\VertexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Gfx\IGeometry.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx9
	{

		RenderTypes primitiveToRenderType(gfx::PrimitiveType type)
		{
			switch(type)
			{
			case gfx::PrimitiveType::TRIANGLE:
				return RenderTypes::TRI;
			case gfx::PrimitiveType::LINE:
				return RenderTypes::LINE;
			case gfx::PrimitiveType::TRIANGLE_STRIP:
				return RenderTypes::TRI_STRIP;
			case gfx::PrimitiveType::POINT:
				return RenderTypes::POINT;
			default:
				ACL_ASSERT(false);
			}

			return RenderTypes::TRI;
		}

		Mesh::Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, const gfx::IGeometry& geometry, gfx::MeshSkeleton* pSkeleton) :
			BaseMesh(vertexBuffer, indexBuffer, isSharedIndexBuffer, vSubsets, 1, geometry, pSkeleton)
        {
			GetOwnStates().Add<SetInstanceCount>(1);

			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				unsigned int vertexCount = GetVertexCount(i);
				SetDrawCall(i, *new const DrawIndexedPrimitives(primitiveToRenderType(geometry.GetPrimitiveType()), vertexCount, 0, indexOffset));
				indexOffset += vertexCount;
			}
        }

		void Mesh::OnChangeVertexCount(unsigned int subset)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < subset; i++)
			{
				const unsigned int vertexCount = GetVertexCount(i);
				indexOffset += vertexCount;
			}

			const unsigned int vertexCount = GetVertexCount(subset);
			RemoveDrawCall(subset, true);
			SetDrawCall(subset, *new const DrawIndexedPrimitives(primitiveToRenderType(GetGeometry().GetPrimitiveType()), vertexCount, 0, indexOffset));
		}

    }
}

#endif