#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseMesh.h"

namespace acl
{
	namespace gfx
	{
		class IGeometry;
	}

	namespace dx9
	{

		class Mesh : 
			public gfx::BaseMesh
		{
		public:
			Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, const gfx::IGeometry& geometry, gfx::MeshSkeleton* pSkeleton);

		private:

			void OnChangeVertexCount(unsigned int subset) override;
		};
	}
}

#endif