#include "MeshBuffer.h"

#ifdef ACL_API_DX9

#include "..\Device.h"
#include "..\VertexBuffer.h"
#include "..\IndexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace dx9
	{
		
		/*******************************
		* VertexBuffer
		********************************/

		VertexBuffer::VertexBuffer(const AclDevice& device, size_t numVertices, size_t stride, RenderTypes type, const void* pData) :
			BaseVertexBuffer(numVertices, stride), m_pBuffer(nullptr)
		{
			d3d::VertexBufferUsage usage;
			if(pData)
				usage = d3d::VertexBufferUsage::WRITEONLY;
			else
				usage = d3d::VertexBufferUsage::DYNAMIC_WRITEONLY;

			m_pBuffer = new d3d::VertexBuffer(device, type, stride, numVertices, usage);

			if(pData)
			{
				void* pBufferData = OnMap(gfx::MapFlags::WRITE_DISCARD);
				memcpy(pBufferData, pData, stride*numVertices);
				OnUnmap();
			}
		}

		VertexBuffer::VertexBuffer(const VertexBuffer& buffer) :
			BaseVertexBuffer(buffer)
		{
			m_pBuffer = new d3d::VertexBuffer(*buffer.m_pBuffer);
		}

		VertexBuffer::~VertexBuffer(void)
		{
			delete m_pBuffer;
		}

		gfx::IVertexBuffer& VertexBuffer::Clone(void) const
		{
			return *new VertexBuffer(*this);
		}

		void VertexBuffer::OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const
		{
			switch(target)
			{
			case gfx::BindTarget::VERTEX:
				group.Add<BindVertexBuffer>(m_pBuffer);
				break;
			case gfx::BindTarget::INSTANCE:
				group.Add<BindInstanceBuffer>(m_pBuffer, offset);
				break;
			}
		}

		void* VertexBuffer::OnMap(gfx::MapFlags flags)
		{
			d3d::BufferLock lock;
			switch(flags)
			{
			case gfx::MapFlags::READ:
				lock = d3d::BufferLock::NOOVER;
				break;
			case gfx::MapFlags::READ_WRITE:
				lock = d3d::BufferLock::NONE;
				break;
			case gfx::MapFlags::WRITE:
				lock = d3d::BufferLock::NONE;
				break;
			case gfx::MapFlags::WRITE_DISCARD:
				lock = d3d::BufferLock::DISCARD_NOOVER;
				break;
			}

			void* pBuffer = nullptr;
			m_pBuffer->Lock(&pBuffer, lock);

			return pBuffer;
		}

		void VertexBuffer::OnUnmap(void)
		{
			m_pBuffer->Unlock();
		}

		/*******************************
		* IndexBuffer
		********************************/

		IndexBuffer::IndexBuffer(const AclDevice& device, size_t numIndices, const unsigned int* pData) :
			BaseIndexBuffer(numIndices), m_pBuffer(nullptr)
		{
			m_pBuffer = new d3d::IndexBuffer(device, numIndices * sizeof(unsigned int));

			if(pData)
			{
				unsigned int* pIndexData = OnMap(gfx::MapFlags::WRITE_DISCARD);
				memcpy(pIndexData, pData, numIndices * sizeof(unsigned int));
				OnUnmap();
			}
		}

		IndexBuffer::~IndexBuffer(void)
		{
			delete m_pBuffer;
		}

		void IndexBuffer::Bind(render::StateGroup& group) const
		{
			group.Add<BindIndexBuffer>(m_pBuffer);
		}

		unsigned int* IndexBuffer::OnMap(gfx::MapFlags flags)
		{
			unsigned int* pData;

			d3d::BufferLock lock;
			switch(flags)
			{
			case gfx::MapFlags::READ:
				lock = d3d::BufferLock::NOOVER;
				break;
			case gfx::MapFlags::READ_WRITE:
				lock = d3d::BufferLock::NONE;
				break;
			case gfx::MapFlags::WRITE:
				lock = d3d::BufferLock::NONE;
				break;
			case gfx::MapFlags::WRITE_DISCARD:
				lock = d3d::BufferLock::DISCARD_NOOVER;
				break;
			}

			m_pBuffer->Lock((void**)&pData, lock);
			return pData;
		}

		void IndexBuffer::OnUnmap(void)
		{
			m_pBuffer->Unlock();
		}

	}
}

#endif