#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\VertexBuffer.h"
#include "..\..\Gfx\BaseMeshBuffer.h"

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace dx9
	{
		enum class PrimitiveType
		{
			TRIANGLE, LINE
		};

		class VertexBuffer :
			public gfx::BaseVertexBuffer
		{
		public:

			VertexBuffer(const AclDevice& device, size_t numVertices, size_t stride, RenderTypes type, const void* pData);
			VertexBuffer(const VertexBuffer& buffer);
			~VertexBuffer(void);
			gfx::IVertexBuffer& Clone(void) const override;

		private:

			void* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;
			void OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const override;

			AclVertexBuffer* m_pBuffer;
		};

		class IndexBuffer :
			public gfx::BaseIndexBuffer
		{
		public:

			IndexBuffer(const AclDevice& device, size_t numVertices, const unsigned int* pData);
			~IndexBuffer(void);

			void Bind(render::StateGroup& group) const override;

		private:

			unsigned int* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;

			AclIndexBuffer* m_pBuffer;
		};

	}
}

#endif