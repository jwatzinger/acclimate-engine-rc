#include "MeshLoader.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include "MeshBuffer.h"
#include "InstancedMesh.h"
#include "Mesh.h"
#include "..\Device.h"

#include "..\..\Gfx\Defines.h"
#include "..\..\Gfx\VertexBufferAccessor.h"
#include "..\..\Gfx\IndexBufferAccessor.h"
#include "..\..\Math\Vector2f.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx9
    {

		RenderTypes toRenderType(gfx::PrimitiveType type)
		{
			switch(type)
			{
			case gfx::PrimitiveType::TRIANGLE:
				return RenderTypes::TRI;
			case gfx::PrimitiveType::LINE:
				return RenderTypes::LINE;
			case gfx::PrimitiveType::TRIANGLE_STRIP:
				return RenderTypes::TRI_STRIP;
			case gfx::PrimitiveType::POINT:
				return RenderTypes::POINT;
			default:
				ACL_ASSERT(false);
			}

			return RenderTypes::DATA;
		}

		MeshLoader::MeshLoader(const AclDevice& device, const gfx::Textures& textures, gfx::Meshes& meshes, gfx::IGeometryCreator& creator) : 
			BaseMeshLoader(textures, meshes, creator), m_pDevice(&device)
        {
        }

		gfx::IMesh& MeshLoader::OnCreateMesh(const gfx::IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndicies, const unsigned int* pIndices, const SubsetVector& vSubsets, gfx::MeshSkeleton* pSkeleton) const
		{
			const RenderTypes types = toRenderType(geometry.GetPrimitiveType());

			auto pVertexBuffer = new VertexBuffer(*m_pDevice, numVertices, geometry.GetVertexSize(), types, pVertices);
			auto pIndexBuffer = new IndexBuffer(*m_pDevice, numIndicies, pIndices);

			auto& mesh = *new Mesh(*pVertexBuffer, *pIndexBuffer, false, vSubsets, geometry, pSkeleton);

			return mesh;
		}

		gfx::IMesh& MeshLoader::OnCreateMesh(const gfx::IGeometry& geometry, const VertexVector& vVertices, gfx::IIndexBuffer& indexBuffer) const
		{
			const RenderTypes types = toRenderType(geometry.GetPrimitiveType());
			const size_t size = geometry.GetVertexSize();

			auto pVertexBuffer = new VertexBuffer(*m_pDevice, vVertices.size(), geometry.GetVertexSize(), types, vVertices.data());

			const SubsetVector vSubsets = { indexBuffer.GetSize() };

			return *new Mesh(*pVertexBuffer, indexBuffer, true, vSubsets, geometry, nullptr);
		}

		gfx::IInstancedMesh& MeshLoader::OnCreateInstancedMesh(const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh) const
		{
			auto& instanceBuffer = *new VertexBuffer(*m_pDevice, numInstances, geometry.CalculateVertexSize(gfx::INSTANCE_BUFFER_POS), RenderTypes::DATA, nullptr);

			InstancedMesh* pInstanced = new InstancedMesh(instanceBuffer, geometry, numInstances, mesh );

			return *pInstanced;
		}

		gfx::IIndexBuffer& MeshLoader::Indexbuffer(const IndexVector& vIndices) const
		{
			return *new IndexBuffer(*m_pDevice, vIndices.size(), vIndices.data());
		}

    }
}

#endif
