#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseMeshLoader.h"

namespace acl
{
    namespace dx9
    {

        class MeshLoader final :
			public gfx::BaseMeshLoader
        {
        public:
            MeshLoader(const AclDevice& device, const gfx::Textures& textures, gfx::Meshes& meshes, gfx::IGeometryCreator& creator);

        private:

			gfx::IMesh& OnCreateMesh(const gfx::IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndicies, const unsigned int* pIndicies, const SubsetVector& vSubsets, gfx::MeshSkeleton* pSkeleton = nullptr) const override;
			gfx::IMesh& OnCreateMesh(const gfx::IGeometry& geometry, const VertexVector& vVertices, gfx::IIndexBuffer& indexBuffer) const override;
			gfx::IInstancedMesh& OnCreateInstancedMesh(const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh) const override;
			gfx::IIndexBuffer& Indexbuffer(const IndexVector& vIndices) const override;

            const AclDevice* m_pDevice;
        };

    }
}

#endif