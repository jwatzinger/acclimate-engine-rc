#include "Model.h"

#ifdef ACL_API_DX9

#include "Cbuffer.h"
#include "..\Render\States.h"

namespace acl
{
    namespace dx9
    {

		Model::Model(void) : BaseModel(*new Cbuffer, *new Cbuffer)
        {
			m_states.Add<ScissorTest>(false);
        }

		Model::Model(gfx::IMesh& mesh) : BaseModel(*new Cbuffer, *new Cbuffer, mesh)
        {
			m_states.Add<ScissorTest>(false);
        }

		Model::Model(const Model& model) : BaseModel(model)
		{
			m_states.Add<ScissorTest>(false);

			UpdateMaterials();
		}

		gfx::IModel& Model::Clone(void) const
		{
			return *new Model(*this);
		}

    }
}

#endif