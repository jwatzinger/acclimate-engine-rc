#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseModel.h"

namespace acl
{
	namespace dx9
	{

		class Model final :
			public gfx::BaseModel
		{
		public:
			Model(void);
            Model(gfx::IMesh& mesh);
			Model(const Model& model);

			gfx::IModel& Clone(void) const override;
		};

	}
}

#endif