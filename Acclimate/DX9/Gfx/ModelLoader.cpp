#include "ModelLoader.h"

#ifdef ACL_API_DX9

#include "Model.h"

namespace acl
{
	namespace dx9
	{

		ModelLoader::ModelLoader(const gfx::Materials& materials, const gfx::Meshes& meshes, gfx::Models& models) : BaseModelLoader(materials, meshes, models)
		{
		}

		gfx::IModel& ModelLoader::OnCreateModel(gfx::IMesh* pMesh) const
		{
			if(pMesh)
				return *new Model(*pMesh);
			else
				return *new Model;
		}

	}
}

#endif
