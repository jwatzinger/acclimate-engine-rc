#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseModelLoader.h"

namespace acl
{
	namespace dx9
	{

		class ModelLoader final:
			public gfx::BaseModelLoader
		{
		public:
			ModelLoader(const gfx::Materials& materials, const gfx::Meshes& meshes, gfx::Models& models);

		private:

			gfx::IModel& OnCreateModel(gfx::IMesh* pMesh) const override;
		};

	}
}

#endif
