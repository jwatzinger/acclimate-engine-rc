#include "RenderStateHandler.h"

#ifdef ACL_API_DX9

#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx9
	{

		void RenderStateHandlerInstance::OnUnbindTexture(render::StateGroup& states, gfx::TextureBindTarget target, unsigned int slot) const
		{
			switch(target)
			{
			case gfx::TextureBindTarget::PIXEL:
			{
				switch(slot)
				{
				case 0:
					states.Add<BindTexture0>(nullptr);
					break;
				case 1:
					states.Add<BindTexture1>(nullptr);
					break;
				case 2:
					states.Add<BindTexture2>(nullptr);
					break;
				case 3:
					states.Add<BindTexture3>(nullptr);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			}
			case gfx::TextureBindTarget::VERTEX:
			{
				switch(slot)
				{
				case 0:
					states.Add<BindVertexTexture0>(nullptr);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			}
			default:
				ACL_ASSERT(false);
			}
		}

	}
}

#endif