#include "Sprite.h"

#ifdef ACL_API_DX9

#include "Effect.h"
#include "..\VertexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Gfx\ITexture.h"
#include "..\..\Gfx\IGeometryCreator.h"
#include "..\..\Render\Instance.h"
#include "..\..\Render\States.h"
#include "..\..\Render\IStage.h"
#include "..\..\Math\Utility.h"
#include "..\..\Math\Vector.h"

namespace acl
{
    namespace dx9
    {

		Sprite::Sprite(d3d::Sprite& sprite, const gfx::IEffect* pEffect, gfx::IGeometryCreator& creator, const math::Rect* pClip) : 
			m_pTexture(nullptr), m_vScale(1.0f, 1.0f), m_vPosition(0.0f, 0.0f, 0.0f), m_vOrigin(0.0f, 0.0f, 0.0f), m_width(0),
			m_height(0), m_angle(0.0f), m_pSprite(&sprite), m_pEffect(pEffect), m_bVisible(true)
        {
			gfx::IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 3);
			vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 0, gfx::AttributeType::FLOAT, 2);
			vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 4);

			m_pGeometry = &creator.CreateGeometry(gfx::PrimitiveType::TRIANGLE, vAttributes);

            // intialize sort key
	        m_sortKey.bits = 0;

            // set initial clip rect
	        SetClipRect(pClip);

            // set geometry resource binds
	        m_states.Add<BindVertexBuffer>(&m_pSprite->GetVertexBuffer());
	        m_states.Add<BindIndexBuffer>(&m_pSprite->GetIndexBuffer());
			m_pGeometry->Bind(m_states);
        }

        Sprite::Sprite(const Sprite& sprite): m_sortKey(sprite.m_sortKey), m_pTexture(sprite.m_pTexture), m_vScale(sprite.m_vScale), 
			m_vPosition(sprite.m_vPosition), m_vOrigin(sprite.m_vOrigin), m_width(sprite.m_width), m_height(sprite.m_height), 
			m_angle(sprite.m_angle), m_pSprite(sprite.m_pSprite), m_pEffect(sprite.m_pEffect), m_bVisible(sprite.m_bVisible),
			m_pGeometry(sprite.m_pGeometry)
        {
            // set geometry resource binds 
            m_states.Add<BindVertexBuffer>(&m_pSprite->GetVertexBuffer());
	        m_states.Add<BindIndexBuffer>(&m_pSprite->GetIndexBuffer());
			m_pGeometry->Bind(m_states);

			SetClipRect(nullptr);
        }

		Sprite::~Sprite(void)
		{
			delete m_pGeometry;
		}

        gfx::ISprite& Sprite::Clone(void) const
        {
            // clones a new sprite object fromt his
            return *new Sprite(*this);
        }

        void Sprite::SetTexture(gfx::ITexture* pTexture)
        {
			if(pTexture != m_pTexture)
			{
				// set texture
				m_pTexture = pTexture;

				// return if texture is set to nullptr
				if(!m_pTexture)
					return;

				// aquire textures size
				math::Vector2 vSize = m_pTexture->GetSize();
				m_width = vSize.x;
				m_height = vSize.y;

				// adjust sourc rect if it is set to a null area
				if(m_rSrcRect.width - m_rSrcRect.x == 0)
					m_rSrcRect.width = vSize.x;
				if(m_rSrcRect.height - m_rSrcRect.y == 0)
					m_rSrcRect.height = vSize.x;

				// add bind texture command
				m_pTexture->Bind(m_states, gfx::ITexture::BindTarget::PIXEL, 0);
			}
        }

        void Sprite::SetPosition(int x, int y)
        {
	        m_vPosition.x = (float)x;
	        m_vPosition.y = (float)y;
        }

        void Sprite::SetZ(float z)
        {
	        m_vPosition.z = z;
        }

        void Sprite::SetSize(int width, int height)
        {
	        m_width = width; 
	        m_height = height;
        }

        void Sprite::SetScale(float scaleX, float scaleY)
        {
	        m_vScale.x = scaleX;
	        m_vScale.y = scaleY;
        }

        void Sprite::SetOrigin(int originX, int originY)
        {
	        m_vOrigin.x = (float)originX;
	        m_vOrigin.y = (float)originY;
        }

        void Sprite::SetAngle(float angle)
        {
	        m_angle = angle;
        }

        void Sprite::SetSrcRect(int x, int y, int width, int height)
        {
	        m_rSrcRect.Set(x, y, x+width, y+height);
        }

        void Sprite::SetSrcRect(const math::Rect& rSrcRect)
        {
	        m_rSrcRect = rSrcRect;
            // adjust source rects parameters to fit expected rect format for dx9 sprite renderer
	        m_rSrcRect.width += m_rSrcRect.x;
	        m_rSrcRect.height += m_rSrcRect.y;
        }

        void Sprite::SetClipRect(const math::Rect* pClip)
        {
            if(pClip)
            {
                RECT r = { pClip->x, pClip->y, pClip->x + pClip->width, pClip->y + pClip->height };
	            m_states.Add<ScissorTest>(true, r);
            }
            else
            {
                m_states.Add<ScissorTest>(false);
            }
        }

		void Sprite::SetPixelConstant(unsigned int index, const float constArr[4])
        {
	        //switch(index)
	        //{
	        //case 0:
		       // m_states.Add<CBufferP0>(constArr);
		       // break;
	        //case 1:
		       // m_states.Add<CBufferP1>(constArr);
		       // break;
	        //case 2:
		       // m_states.Add<CBufferP2>(constArr);
		       // break;
	        //case 3:
		       // m_states.Add<CBufferP3>(constArr);
		       // break;
	        //case 4:
		       // m_states.Add<CBufferP4>(constArr);
		       // break;
	        //}
        }

		void Sprite::SetEffect(const gfx::IEffect& effect)
		{
			m_pEffect = &effect;
		}

		void Sprite::SetVisible(bool bVisible)
		{
			m_bVisible = bVisible;
		}

        gfx::ITexture* Sprite::GetTexture(void) const
        {
	        return m_pTexture;
        }

        void Sprite::Draw(const render::IStage& stage)
        {
			if(!m_bVisible || !m_pEffect)
				return;

	        int width = max(1, m_rSrcRect.width - m_rSrcRect.x);

	        math::Vector2f vScale = m_vScale;
	        if(m_width != width)
	        {
		        vScale.x *= m_width;
		        vScale.x /= width;
	        }

	        int height =  max(1, m_rSrcRect.height - m_rSrcRect.y);
	        if(m_height != height)
	        {
		        vScale.y *= m_height;
		        vScale.y /= height;
	        }

			size_t id = 0;
			if(m_pTexture)
				id = m_pSprite->Draw(m_pTexture->GetSize(), m_vPosition, m_vOrigin, m_rSrcRect.GetWinRect(), vScale, m_angle);
			else
				id = m_pSprite->Draw(m_vPosition, m_vOrigin, vScale, m_angle);

			const render::StateGroup* pGroups[2] = { &m_states, &m_pEffect->GetState() };
			render::Instance instance(m_sortKey, pGroups, 2);

			instance.CreateCall<DrawIndexedPrimitives>(RenderTypes::TRI, 6, id * 4, 0);

	        stage.SubmitTempInstance(std::move(instance));
        }

    }
}

#endif