#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\ISprite.h"
#include "..\..\Math\Vector3.h"
#include "..\..\Math\Vector2f.h"
#include "..\..\Math\Rect.h"
#include "..\..\Render\Key.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{
        class IEffect;
		class IGeometry;
		class IGeometryCreator;
	}

    namespace render
    {
        class IStage;
    }

	namespace dx9
    {

		class Sprite : 
			public gfx::ISprite
		{
		public:
			Sprite(d3d::Sprite& sprite, const gfx::IEffect* pEffect, gfx::IGeometryCreator& creator, const math::Rect* rClip = nullptr);
            Sprite(const Sprite& sprite);
			~Sprite(void);

            ISprite& Clone(void) const;

			void SetPosition(int x, int y);
			void SetZ(float z);
			void SetSize(int width, int heigh);
			void SetScale(float scaleX, float scaleY);
			void SetOrigin(int originX, int originY);
			void SetAngle(float angle);
			void SetTexture(gfx::ITexture* pTexture);
			void SetClipRect(const math::Rect* pClip);
			void SetSrcRect(int x, int y, int width, int height);
			void SetSrcRect(const math::Rect& rSrcRect);
			void SetPixelConstant(unsigned int index, const float constArr[4]);
			void SetEffect(const gfx::IEffect& effect);
			void SetVisible(bool bVisible);

			gfx::ITexture* GetTexture(void) const;

			void Draw(const render::IStage& stage);

		private:

            d3d::Sprite* m_pSprite;

			gfx::ITexture* m_pTexture;
			const gfx::IEffect* m_pEffect;
			const gfx::IGeometry* m_pGeometry;

			int m_width, m_height;
			float m_angle;
			bool m_bVisible;
			math::Rect m_rSrcRect;
			math::Vector3 m_vOrigin;
			math::Vector3 m_vPosition;
			math::Vector2f m_vScale;

			render::StateGroup m_states;
			render::Key64 m_sortKey;
		};

	}
}

#endif

