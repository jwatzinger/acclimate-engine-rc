#include "SpriteBatch.h"

#ifdef ACL_API_DX9

#include "..\Sprite.h"
#include "..\VertexBuffer.h"
#include "..\Render\States.h"
#include "..\..\Render\Instance.h"
#include "..\..\Render\States.h"

namespace acl
{
	namespace dx9
	{

		SpriteBatch::SpriteBatch(d3d::Sprite& sprite, const gfx::Fonts& fonts, const gfx::IFontLoader& fontLoader, gfx::IGeometryCreator& creator) : 
			BaseSpriteBatch(fonts, fontLoader, creator), m_pSprite(&sprite)
		{
			// set geometry resource binds
			m_states.Add<BindVertexBuffer>(&m_pSprite->GetVertexBuffer());
			m_states.Add<BindIndexBuffer>(&m_pSprite->GetIndexBuffer());

			SetClipRect(nullptr);
		}

		void SpriteBatch::SetClipRect(const math::Rect* pClip)
		{
			if(pClip)
			{
				Submit();

				RECT r = { pClip->x, pClip->y, pClip->x + pClip->width, pClip->y + pClip->height };
				m_states.Add<ScissorTest>(true, r);
			}
			else
				m_states.Add<ScissorTest>(false);
		}

		void SpriteBatch::OnPrepareExecute(void)
		{
			m_pSprite->Lock();
		}

		size_t SpriteBatch::OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle, const gfx::Color& color) const
		{
			return m_pSprite->Draw(vTextureSize, vPosition, vOrigin, rSrcRect, vScale, angle);
		}

		size_t SpriteBatch::OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle, const gfx::Color& color) const
		{
			return m_pSprite->Draw(vPosition, vOrigin, vScale, angle);
		}

		void SpriteBatch::OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance)
		{
			// TODO: figure out why this hack (startIndex/6)*4 is necessary so that sprites are getting rendered correctly,
			// and find a proper way of doing this
			instance.CreateCall<DrawIndexedPrimitives>(RenderTypes::TRI, numIndices, (startIndex/6)*4, 0);
		}

		void SpriteBatch::OnFinishExecute(void)
		{
			m_pSprite->Unlock();
		}

	}
}

#endif