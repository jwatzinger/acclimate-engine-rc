#include "Text.h"

#ifdef ACL_API_DX9

#include "..\Font.h"
#include "..\Render\States.h"
#include "..\..\Gfx\IFont.h"
#include "..\..\Gfx\IEffect.h"
#include "..\..\Math\Rect.h"
#include "..\..\Render\IStage.h"
#include "..\..\Render\Instance.h"
#include "..\..\System\Convert.h"

namespace acl
{
    namespace dx9
    {

        Text::Text(const gfx::Fonts& fonts) : m_pFonts(&fonts), m_pEffect(nullptr), m_pActiveFont(nullptr)
		{
			SetClipRect(nullptr);
        }

		Text::Text(const Text& text): m_color(text.m_color), m_pFonts(text.m_pFonts),  m_pActiveFont(text.m_pActiveFont),
			m_pEffect(nullptr)
		{
			SetClipRect(nullptr);
		}

		void Text::Draw(render::IStage& stage, const math::Rect& rect, const std::wstring& stText, DWORD dFlags, float f)
        {
			if(!m_pEffect)
				return;

			if(!m_pActiveFont)
				SetFontProperties(16, L"Arial");

			if(stText.empty())
				return;

			size_t id = m_pActiveFont->DrawString(rect, 0.5f, stText, dFlags, m_color);

			render::Key64 key;
            key.bits = 0;

			const render::StateGroup* pGroups[3] = { &m_states, &m_pEffect->GetState(), &m_pActiveFont->GetState() };
			render::Instance instance(key, pGroups, 3);

			instance.CreateCall<dx9::DrawIndexedPrimitives>(dx9::RenderTypes::TRI, stText.size()*6, id*4, 0);

            stage.SubmitTempInstance(std::move(instance));
        }

        
        void Text::SetClipRect(const math::Rect* pClip)
        {
            if(pClip)
            {
                RECT r = { pClip->x, pClip->y, pClip->x + pClip->width, pClip->y + pClip->height };
	            m_states.Add<ScissorTest>(true, r);
            }
            else
            {
                m_states.Add<ScissorTest>(false);
            }
        }

        void Text::SetFontProperties(int fontSize, const std::wstring& stFontString)
        {
			m_pActiveFont = m_pFonts->Get(stFontString + conv::ToString(fontSize));
        }

        void Text::SetColor(const gfx::Color& color)
        {
	        m_color = color;
        }

		void Text::SetEffect(const gfx::IEffect& effect)
		{
			m_pEffect = &effect;
		}


    }
}

#endif