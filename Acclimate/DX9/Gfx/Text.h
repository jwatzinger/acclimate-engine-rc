#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include <map>
#include <string>
#include "..\..\Gfx\IText.h"
#include "..\..\Gfx\Color.h"
#include "..\..\Gfx\Fonts.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
    namespace math
    {
        struct Rect;
    }

    namespace dx9
    {

        class Text: 
			public gfx::IText
        {
        public:
	        Text(const gfx::Fonts& fonts);
			Text(const Text& text);
            
            void SetClipRect(const math::Rect* pClip) override;
			void SetColor(const gfx::Color& color) override;
			void SetFontProperties(int fontSize, const std::wstring& stFontName) override;
			void SetEffect(const gfx::IEffect& effect) override;

			void Draw(render::IStage& stage, const math::Rect& rect, const std::wstring& lpText, unsigned long dFlags, float f = 0.0f) override;

        private:
		
			const gfx::IEffect* m_pEffect;

	        gfx::Color m_color;
	        gfx::IFont* m_pActiveFont;

            const gfx::Fonts* m_pFonts;

            render::StateGroup m_states;
        };

    }
}

#endif