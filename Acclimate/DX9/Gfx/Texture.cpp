#include "Texture.h"

#ifdef ACL_API_DX9

#include "..\Texture.h"
#include "..\Render\States.h"
#include "..\..\System\Assert.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace dx9
	{

		gfx::TextureFormats ConvertFormat(D3DFORMAT format)
		{
		    switch(format)
		    {
			case D3DFMT_X8R8G8B8:
				return gfx::TextureFormats::X32;
			case D3DFMT_A8R8G8B8:
			    return gfx::TextureFormats::A32;
			case D3DFMT_A16B16G16R16F:
				return gfx::TextureFormats::A16F;
			case D3DFMT_L8:
				return gfx::TextureFormats::L8;
			case D3DFMT_L16:
				return gfx::TextureFormats::L16;
            case D3DFMT_R32F:
                return gfx::TextureFormats::R32;
			case D3DFMT_G16R16F:
				return gfx::TextureFormats::GR16F;
			case D3DFMT_R16F:
				return gfx::TextureFormats::R16F;
			case D3DFMT_UNKNOWN:
				return gfx::TextureFormats::UNKNOWN;
            default:
                return gfx::TextureFormats::A32;
		    }
		}

		void deleteTexture(AclTexture* pTexture)
		{
			delete pTexture;
		}

		Texture::Texture(AclTexture& texture) : 
			BaseTexture(texture, texture.GetSize(), ConvertFormat(texture.GetFormat()), deleteTexture)
		{
		}

		void Texture::Bind(render::StateGroup& states, BindTarget target, unsigned int slot) const
		{
			switch(target)
			{
			case BindTarget::PIXEL:
				{
					switch(slot)
					{
					case 0:
						states.Add<BindTexture0>(m_pTexture);
						break;
					case 1:
						states.Add<BindTexture1>(m_pTexture);
						break;
					case 2:
						states.Add<BindTexture2>(m_pTexture);
						break;
					case 3:
						states.Add<BindTexture3>(m_pTexture);
						break;
					default:
						ACL_ASSERT(false);
					}
					break;
				}
			case BindTarget::VERTEX:
				{
					switch(slot)
					{
					case 0:
						states.Add<BindVertexTexture0>(m_pTexture);
						break;
					default:
						ACL_ASSERT(false);
					}
				}
			}
		}

		void Texture::OnResize(const math::Vector2& vSize)
		{
			m_pTexture->Reload(vSize);
		}

		void Texture::OnMap(gfx::MapFlags flags, gfx::Mapped& map) const
		{
			D3DLOCKED_RECT rect;
			m_pTexture->Lock(rect);

			map.pData = rect.pBits;
			map.pitch = rect.Pitch;
		} 

		void Texture::OnUnmap(void) const
		{
			m_pTexture->Unlock();
		}

	}
}

#endif