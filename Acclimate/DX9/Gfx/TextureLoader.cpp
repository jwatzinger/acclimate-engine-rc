#include "TextureLoader.h"

#ifdef ACL_API_DX9

#include "Texture.h"
#include "..\Texture.h"
#include "..\..\Gfx\TextureAccessors.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx9
    {

        D3DFORMAT GetFormat(gfx::TextureFormats format)
		{
		    switch(format)
		    {
            case gfx::TextureFormats::A32:
			    return D3DFMT_A8R8G8B8;
            case gfx::TextureFormats::A16F:
			    return D3DFMT_A16B16G16R16F;
			case gfx::TextureFormats::L8:
				return D3DFMT_L8;
			case gfx::TextureFormats::L16:
				return D3DFMT_L16;
            case gfx::TextureFormats::R32:
                return D3DFMT_R32F;
			case gfx::TextureFormats::GR16F:
				return D3DFMT_G16R16F;
			case gfx::TextureFormats::R16F:
				return D3DFMT_R16F;
            case gfx::TextureFormats::UNKNOWN:
				return D3DFMT_UNKNOWN;
            default:
                return D3DFMT_X8R8G8B8;
		    }
		}

        TextureLoader::TextureLoader(const AclDevice& device, gfx::Textures& textures): BaseTextureLoader(textures), m_pDevice(&device)
        {
        }

        gfx::ITexture& TextureLoader::OnLoadTexture(const std::wstring& stFilename, bool bRead, gfx::TextureFormats format) const
        {
            d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, stFilename.c_str(), GetFormat(format));
            return *new Texture(d3dTexture);
        }

        gfx::ITexture& TextureLoader::OnCreateTexture(const math::Vector2& vSize, gfx::TextureFormats format, gfx::LoadFlags flags) const
        {
			bool bIsRenderTarget = flags == gfx::LoadFlags::RENDER_TARGET || flags == gfx::LoadFlags::RENDER_TARGET_MIPS;

            d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, vSize.x, vSize.y, GetFormat(format), bIsRenderTarget);
            return *new Texture(d3dTexture);
        }

		gfx::ITexture& TextureLoader::OnCreateTexture(const math::Vector2& vSize, const void* pData, gfx::TextureFormats format, gfx::LoadFlags flags) const
		{
			bool bIsRenderTarget = flags == gfx::LoadFlags::RENDER_TARGET || flags == gfx::LoadFlags::RENDER_TARGET_MIPS;

			d3d::Texture& d3dTexture = *new d3d::Texture(*m_pDevice, vSize.x, vSize.y, GetFormat(format), bIsRenderTarget);

			auto pTexture = new Texture(d3dTexture);

			// TODO: fix for other texture formats
			switch(format)
			{
			case gfx::TextureFormats::R32:
			{
				const float* pRealData = (float*)pData;
				gfx::TextureAccessorR32 accessor(*pTexture, false);
				for(int i = 0; i < vSize.y; i++)
				{
					accessor.SetLine(0, i, *pRealData, vSize.x);
				}
			}
			default:
				ACL_ASSERT(false);
			}

			return *pTexture;
		}

		bool TextureLoader::OnSaveTexture(AclTexture& texture, const std::wstring& stFilename, gfx::FileFormat format) const
		{
			return false;
		}

    }
}

#endif