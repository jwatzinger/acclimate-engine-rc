#include "ZBuffer.h"

#ifdef ACL_API_DX9

#include "..\DepthBuffer.h"

namespace acl
{
	namespace dx9
	{

		void deleteZBuffer(AclDepth* pDepthBuffer)
		{
			delete pDepthBuffer;
		}

		ZBuffer::ZBuffer(AclDepth& depthBuffer): 
			BaseZBuffer(depthBuffer, depthBuffer.GetSize(), deleteZBuffer)
		{
		}

		void ZBuffer::OnResize(const math::Vector2& vSize)
		{
			m_pDepthBuffer->Reload(vSize);
		}

	}
}

#endif