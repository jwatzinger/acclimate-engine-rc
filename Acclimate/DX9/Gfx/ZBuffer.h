#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseZBuffer.h"

namespace acl
{
	namespace dx9
	{

		class ZBuffer final :
			public gfx::BaseZBuffer
		{
		public:
			ZBuffer(AclDepth& depthBuffer);

			void OnResize(const math::Vector2& vSize) override;
		};

	}
}

#endif
