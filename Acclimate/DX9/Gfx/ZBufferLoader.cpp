#include "ZBufferLoader.h"

#ifdef ACL_API_DX9

#include "ZBuffer.h"
#include "..\DepthBuffer.h"
#include "..\..\Math\Vector.h"

namespace acl
{
	namespace dx9
	{

		ZBufferLoader::ZBufferLoader(AclDevice& device, gfx::ZBuffers& zBuffers): BaseZBufferLoader(zBuffers), 
			m_device(device)
		{
		}

		gfx::IZBuffer& ZBufferLoader::OnCreateZBuffer(const math::Vector2& vSize) const
		{
			AclDepth* pDX9DepthBuffer = new AclDepth(m_device, vSize.x, vSize.y, D3DFMT_D24X8);

			return *new ZBuffer(*pDX9DepthBuffer);
		}

	}
}

#endif