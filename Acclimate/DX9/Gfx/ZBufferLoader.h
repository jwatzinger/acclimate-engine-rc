#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Gfx\BaseZBufferLoader.h"

namespace acl
{
	namespace dx9
	{

		class ZBufferLoader final :
			public gfx::BaseZBufferLoader
		{
		public:
			ZBufferLoader(AclDevice& device, gfx::ZBuffers& zBuffers);

		private:

			gfx::IZBuffer& OnCreateZBuffer(const math::Vector2& vSize) const override;
			
			AclDevice& m_device;
		};

	}
}

#endif