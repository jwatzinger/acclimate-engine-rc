#include "IndexBuffer.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "..\Safe.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			IndexBuffer::IndexBuffer(LPDIRECT3DINDEXBUFFER9 lpIndexBuffer): m_lpIndexBuffer(lpIndexBuffer), m_bLocked(false)
			{
			}

			IndexBuffer::IndexBuffer(const Device& device, unsigned int length): m_lpIndexBuffer(nullptr), m_bLocked(false)
			{
				m_lpIndexBuffer = device.CreateIndexBuffer(length*sizeof(unsigned int), D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED);
			}

			IndexBuffer::IndexBuffer(const IndexBuffer& indexBuffer): m_lpIndexBuffer(nullptr), m_bLocked(indexBuffer.m_bLocked)
			{
				m_lpIndexBuffer = indexBuffer.m_lpIndexBuffer;
				m_lpIndexBuffer->AddRef();
			}

			IndexBuffer::IndexBuffer(IndexBuffer&& indexBuffer): m_lpIndexBuffer(indexBuffer.m_lpIndexBuffer), m_bLocked(indexBuffer.m_bLocked)
			{
				indexBuffer.m_lpIndexBuffer = nullptr;
				indexBuffer.m_bLocked = false;
			}

			IndexBuffer::~IndexBuffer(void)
			{
				SAFE_RELEASE(m_lpIndexBuffer);
			}

			IndexBuffer& IndexBuffer::operator=(const IndexBuffer& indexBuffer)
			{
				indexBuffer.m_lpIndexBuffer->AddRef();
				SAFE_RELEASE(m_lpIndexBuffer);
				m_lpIndexBuffer = indexBuffer.m_lpIndexBuffer;
				m_bLocked = indexBuffer.m_bLocked;

				return *this;
			}

			LPDIRECT3DINDEXBUFFER9 IndexBuffer::GetIndexBuffer(void) const
			{
				return m_lpIndexBuffer;
			}

			void IndexBuffer::Lock(void** pBuffer, BufferLock lock)
			{
				if(m_bLocked)
					return;

				m_lpIndexBuffer->Lock(0, 0, (void**)pBuffer, 0);
				m_bLocked = true;
			}

			void IndexBuffer::Unlock(void)
			{
				if(!m_bLocked)
					return;

				m_lpIndexBuffer->Unlock();
				m_bLocked = false;
			}

		}
	}
}

#endif