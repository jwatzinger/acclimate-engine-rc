#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>
#include "BufferLock.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Device;

			class IndexBuffer
			{
			public:
				IndexBuffer(LPDIRECT3DINDEXBUFFER9 lpIndexBuffer);
				IndexBuffer(const Device& device, unsigned int length);
				IndexBuffer(const IndexBuffer& indexBuffer);
				IndexBuffer(IndexBuffer&& indexBuffer);
				~IndexBuffer(void);

				IndexBuffer& operator=(const IndexBuffer& indexBuffer);

				void Lock(void** pBuffer, BufferLock lock);
				void Unlock(void);

				LPDIRECT3DINDEXBUFFER9 GetIndexBuffer(void) const;

			private:

				bool m_bLocked;

				LPDIRECT3DINDEXBUFFER9 m_lpIndexBuffer;
			};

		}
	}
}

#endif