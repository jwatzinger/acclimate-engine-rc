#include "Line.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "Texture.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexDeclaration.h"
#include "..\Safe.h"
#include "..\Gfx\Color.h"
#include "..\Math\Vector3.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			struct LineVertex
			{
				float x,y,z;
				float r,g,b,a;
			};

			const size_t TEMP_NUM_LINES = 5000;

			Line::Line(const Device& device) : m_nextFreeId(0), m_pDevice(&device)
			{
				m_pVertexBuffer = new VertexBuffer(device, RenderTypes::LINE, sizeof(LineVertex), TEMP_NUM_LINES*2, VertexBufferUsage::DYNAMIC_WRITEONLY);

				m_pIndexBuffer = new IndexBuffer(device, 2);

				//index buffer 

				WORD* pData = nullptr;
				m_pIndexBuffer->Lock((void**)&pData, BufferLock::DISCARD_NOOVER);

				pData[0] = 0;
				pData[1] = 1;

				m_pIndexBuffer->Unlock();
			}

			Line::~Line(void)
			{
				delete m_pVertexBuffer;
				delete m_pIndexBuffer;
			}

			void Line::Reset(void)
			{
				LineVertex* pBuffer = nullptr;
				m_pVertexBuffer->Lock((void**)&pBuffer, BufferLock::DISCARD);
				m_pVertexBuffer->Unlock();

				m_nextFreeId = 0;
				const auto mode = m_pDevice->GetDisplayMode();
				math::Vector2f m_vInvHalfScreenSize = math::Vector2f(1.0f / (mode.Width * 0.5f), 1.0f / (mode.Height * 0.5f));
			}

			void Line::OnReset(void)
			{
				m_pVertexBuffer->OnReset();
			}

			void Line::OnRecreate(void)
			{
				m_pVertexBuffer->OnRecreate();
			}

			size_t Line::Draw(const math::Vector3& vStart, const math::Vector3& vEnd, const gfx::Color& color)
			{
				LineVertex* pBuffer = nullptr;

				const float leftVertex = vStart.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float rightVertex = vEnd.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float topVertex = -vStart.y * m_vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = -vEnd.y * m_vInvHalfScreenSize.y + 1.0f;

				const float invColor = 1.0f / 255.0f;

				const float r = color.r * invColor;
				const float g = color.g * invColor;
				const float b = color.b * invColor;
				const float a = color.a * invColor;

				LineVertex vertices[] =
				{
					{ leftVertex, topVertex, vStart.z, r, g, b, a },
					{ rightVertex, bottomVertex, vEnd.z, r, g, b, a },
				};

				m_pVertexBuffer->Lock(m_nextFreeId*2, 2, (void**)&pBuffer, BufferLock::NOOVER);
				memcpy(pBuffer, vertices, sizeof(LineVertex)* 2);
				m_pVertexBuffer->Unlock();

				return m_nextFreeId++;
			}

			const VertexBuffer& Line::GetVertexBuffer(void) const
			{
				return *m_pVertexBuffer;
			}

			const IndexBuffer& Line::GetIndexBuffer(void) const
			{
				return *m_pIndexBuffer;
			}

		}
	}
}

#endif