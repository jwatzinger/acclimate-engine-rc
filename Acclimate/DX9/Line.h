#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <vector>
#include <d3d9.h>
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

    namespace math
    {
        struct Vector3;
    }

	namespace dx9
	{
		namespace d3d
		{

			class Device;
			class Texture;
			class VertexBuffer;
			class IndexBuffer;

			class Line
			{
				typedef std::vector<bool> FreeIdVector;

			public:
				Line(const Device& device);
				~Line(void);

				void Reset(void);
				void OnReset(void);
				void OnRecreate(void);

				size_t Draw(const math::Vector3& vStart, const math::Vector3& vEnd, const gfx::Color& color); 

				const VertexBuffer& GetVertexBuffer(void) const;
				const IndexBuffer& GetIndexBuffer(void) const;

			private:
			
				Line(const Line& line) {};

				const Device* m_pDevice;

				size_t m_nextFreeId;
				math::Vector2f m_vInvHalfScreenSize;

				VertexBuffer* m_pVertexBuffer;
				IndexBuffer* m_pIndexBuffer;

			};

		}
	}
}

#endif