#include "Queue.h"

#ifdef ACL_API_DX9

#include <algorithm>
#include "States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
    namespace dx9
    {

        void Queue::Init(void)
        {
	        BindVertexBuffer::type();
	        BindIndexBuffer::type();
	        BindInstanceBuffer::type();
	        SetVertexDeclaration::type();
	        SetInstanceCount::type();
	        BindTexture0::type();
	        BindTexture1::type();
	        BindTexture2::type();
	        BindTexture3::type();
            SamplerState0::type();
            SamplerState1::type();
            SamplerState2::type();
            SamplerState3::type();
			VertexSamplerState0::type();
            BindVertexTexture0::type();
	        BindShader::type();
	        BindRenderTarget0::type();
	        BindRenderTarget1::type();
	        BindRenderTarget2::type();
	        BindRenderTarget3::type();
			BindDepthBuffer::type();
	        AlphaBlendState::type();
	        DepthState::type();
	        CullState::type();
            ScissorTest::type();
            FillMode::type();
			SetViewport::type();
	        CBufferV0::type();
	        CBufferV1::type();
	        CBufferP0::type();
	        CBufferP1::type();

			// draw calls
			DrawIndexedPrimitives::type();
			ClearTargets::type();
			DrawPrimitives::type();
        }

        Queue::Queue(d3d::Device& device) : m_pDevice(&device)
        {
        }

		enum StateType	{	VertexBuffer, IndexBuffer, InstanceBuffer, VertexDeclaration, InstanceCount, Texture0, Texture1, Texture2, Texture3, Sampler0, Sampler1, Sampler2, Sampler3, VertexSampler0, VertexTexture0, Shader, RenderTarget0, RenderTarget1, RenderTarget2, RenderTarget3, DepthBuffer, AlphaState, Depth, CullMode, Scissor, Fill, Port, 
			CV0, CV1, CP0, CP1 };
		enum CallType { Indexed, Clear, Draw };

        void Queue::OnExecuteSorted(const InstanceVector& vInstances) const
        {
	        //iteratre through render instances
	        for(auto& instance : vInstances)
	        {
		        //store pointer to instance and state groups
				const void* pStream = instance.first->GetStateStream();

		        unsigned int bPreviousApplied = 0;
				unsigned int offset = 0;
		        //iterate through instance state groups
				for(unsigned int i = 0; i < instance.first->GetStateCount(); i++)
		        {
					const unsigned int commandCount = *reinterpret_cast<const unsigned int*>(reinterpret_cast<const char*>(pStream) +offset);

					//store pointer to state group and state pointer
					const void* pvCmds = reinterpret_cast<const char*>(pStream) +offset + sizeof(unsigned int);
					offset += commandCount*render::STATE_SIZE + sizeof(unsigned int);

			        //iterate through command group
			        for(unsigned int j = 0; j < commandCount; j++)
			        {
						//get state type
						const render::BaseState::Type type = *reinterpret_cast<const render::BaseState::Type*>(pvCmds);
						pvCmds = reinterpret_cast<const char*>(pvCmds) +sizeof(render::BaseState::Type);

						const auto shifted = 1U << type;
				        //ignore if same command was applied earlier on stack
						if((bPreviousApplied & shifted) == shifted)
                        {
							pvCmds = static_cast<const void*>(static_cast<const char*>(pvCmds) +render::STATE_SIZE - sizeof(unsigned int));
					        continue;
                        }

				        //set applied status to skip any further commands of this type
						bPreviousApplied |=  shifted;

				        //switch on type
				        switch(type)
				        {
				        case StateType::VertexBuffer:
					        {
								const BindVertexBuffer& bindVB = *static_cast<const BindVertexBuffer*>(pvCmds);
						        bindVB.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::IndexBuffer:
					        {
						        const BindIndexBuffer& bindIB = *static_cast<const BindIndexBuffer*>(pvCmds);
						        bindIB.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::InstanceBuffer:
					        {
						        const BindInstanceBuffer& bindIB = *static_cast<const BindInstanceBuffer*>(pvCmds);
						        bindIB.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::VertexDeclaration:
					        {
						        const SetVertexDeclaration& setVD = *static_cast<const SetVertexDeclaration*>(pvCmds);
						        setVD.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::InstanceCount:
					        {
						        const SetInstanceCount& setInstCount = *static_cast<const SetInstanceCount*>(pvCmds);
						        setInstCount.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Texture0:
					        {
						        const BindTexture0& bindTex = *static_cast<const BindTexture0*>(pvCmds);
						        bindTex.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Texture1:
					        {
						        const BindTexture1& bindTex = *static_cast<const BindTexture1*>(pvCmds);
						        bindTex.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Texture2:
					        {
						        const BindTexture2& bindTex = *static_cast<const BindTexture2*>(pvCmds);
						        bindTex.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Texture3:
					        {
						        const BindTexture3& bindTex = *static_cast<const BindTexture3*>(pvCmds);
						        bindTex.Execute(*m_pDevice);
						        break;
					        }
                        case StateType::Sampler0:
					        {
						        const SamplerState0& sampler = *static_cast<const SamplerState0*>(pvCmds);
						        sampler.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Sampler1:
					        {
						        const SamplerState1& sampler = *static_cast<const SamplerState1*>(pvCmds);
						        sampler.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Sampler2:
					        {
						        const SamplerState2& sampler = *static_cast<const SamplerState2*>(pvCmds);
						        sampler.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Sampler3:
					        {
						        const SamplerState3& sampler = *static_cast<const SamplerState3*>(pvCmds);
						        sampler.Execute(*m_pDevice);
						        break;
					        }
						case StateType::VertexSampler0:
							{
								const VertexSamplerState0& sampler = *static_cast<const VertexSamplerState0*>(pvCmds);
								sampler.Execute(*m_pDevice);
								break;
							}
                        case StateType::VertexTexture0:
					        {
						        const BindVertexTexture0& bindTex = *static_cast<const BindVertexTexture0*>(pvCmds);
						        bindTex.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Shader:
					        {
						        const BindShader& bindShader = *static_cast<const BindShader*>(pvCmds);
						        bindShader.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::RenderTarget0:
					        {
						        const BindRenderTarget0& bindRT = *static_cast<const BindRenderTarget0*>(pvCmds);
						        bindRT.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::RenderTarget1:
					        {
						        const BindRenderTarget1& bindRT = *static_cast<const BindRenderTarget1*>(pvCmds);
						        bindRT.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::RenderTarget2:
					        {
						        const BindRenderTarget2& bindRT = *static_cast<const BindRenderTarget2*>(pvCmds);
						        bindRT.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::RenderTarget3:
					        {
						        const BindRenderTarget3& bindRT = *static_cast<const BindRenderTarget3*>(pvCmds);
						        bindRT.Execute(*m_pDevice);
						        break;
					        }
						case StateType::DepthBuffer:
					        {
						        const BindDepthBuffer& bindDB = *static_cast<const BindDepthBuffer*>(pvCmds);
						        bindDB.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::AlphaState:
					        {
						        const AlphaBlendState& blendState = *static_cast<const AlphaBlendState*>(pvCmds);
						        blendState.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::Depth:
					        {
						        const DepthState& depth = *static_cast<const DepthState*>(pvCmds);
						        depth.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::CullMode:
					        {
						        const CullState& cull = *static_cast<const CullState*>(pvCmds);
						        cull.Execute(*m_pDevice);
						        break;
					        }
                        case StateType::Scissor:
					        {
						        const ScissorTest& scissor = *static_cast<const ScissorTest*>(pvCmds);
						        scissor.Execute(*m_pDevice);
						        break;
					        }
                        case StateType::Fill:
					        {
                                const FillMode& fill = *static_cast<const FillMode*>(pvCmds);
						        fill.Execute(*m_pDevice);
						        break;
					        }
						case StateType::Port:
					        {
                                const SetViewport& viewport = *static_cast<const SetViewport*>(pvCmds);
						        viewport.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::CV0:
					        {
						        const CBufferV0& cBufferV0 = *static_cast<const CBufferV0*>(pvCmds);
						        cBufferV0.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::CV1:
					        {
						        const CBufferV1& cBufferV1 = *static_cast<const CBufferV1*>(pvCmds);
						        cBufferV1.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::CP0:
					        {
						        const CBufferP0& cBufferP = *static_cast<const CBufferP0*>(pvCmds);
						        cBufferP.Execute(*m_pDevice);
						        break;
					        }
				        case StateType::CP1:
					        {
						        const CBufferP1& cBufferP = *static_cast<const CBufferP1*>(pvCmds);
						        cBufferP.Execute(*m_pDevice);
						        break;
					        }
				        }
						pvCmds = static_cast<const void*>(static_cast<const char*>(pvCmds) +render::STATE_SIZE - sizeof(unsigned int));
			        }
		        }

		        //get draw call pointer and type
				const render::BaseDrawCall* pDrawCall = instance.first->GetDrawCall();

		        if(pDrawCall)
		        {
			        const render::BaseDrawCall::Type type(pDrawCall->GetType());

			        //switch on call type
			        switch(type)
			        {
			        case CallType::Indexed:
				        {
					        const DrawIndexedPrimitives& drawIndexed = *static_cast<const DrawIndexedPrimitives*>(pDrawCall);
					        drawIndexed.Execute(*m_pDevice);
					        break;
				        }
			        case CallType::Clear:
				        {
					        const ClearTargets& clear = *static_cast<const ClearTargets*>(pDrawCall);
					        clear.Execute(*m_pDevice);
					        break;
				        }
					case CallType::Draw:
						{
							const DrawPrimitives& draw = *static_cast<const DrawPrimitives*>(pDrawCall);
							draw.Execute(*m_pDevice);
							break;
						}
			        }
		        }
	        }
        }

    }
}

#endif