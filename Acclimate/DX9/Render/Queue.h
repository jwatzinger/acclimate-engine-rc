#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include <vector>
#include "..\..\Render\BaseQueue.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{
			class Device;
		}

		class Queue : 
			public render::BaseQueue
		{
		public:
			Queue(d3d::Device& device);

			static void Init(void);

		private:

			void OnExecuteSorted(const InstanceVector& vInstances) const override;

			d3d::Device* m_pDevice;
		};

	}
}

#endif