#include "Renderer.h"

#ifdef ACL_API_DX9

#include "Queue.h"
#include "Stage.h"
#include "..\DirectX9.h"
#include "..\..\Render\RendererInternalAccessor.h"

namespace acl
{
    namespace dx9
    {

        Renderer::Renderer(DirectX9& direct3D, const gfx::ICbufferLoader& loader): m_pDirectX9(&direct3D),
			m_pLoader(&loader)
        {
        }

		void Renderer::GetDevice(render::RendererInternalAccessor& accessor) const
		{
			accessor.SetDevice(&m_pDirectX9->GetDevice());
		}

        void Renderer::Prepare(void)
        {
	        m_pDirectX9->Begin();
        }

		void Renderer::Finish(void)
		{
			m_pDirectX9->Present();
		}

		render::IStage& Renderer::OnCreateStage(unsigned int id, render::IQueue& queue, unsigned int flags) const
		{
			return *new Stage(id, queue, flags, *m_pLoader);
		}

		render::IQueue& Renderer::OnCreateQueue(void) const
		{
			return *new Queue(m_pDirectX9->GetDevice());
		}

		void Renderer::OnExecute(void)
		{
			m_pDirectX9->Reset();
			m_pDirectX9->End();
		}
        
    }
}

#endif