#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Render\BaseRenderer.h"

namespace acl
{
	namespace gfx
	{
		class ICbufferLoader;
	}

	namespace dx9
	{

		class DirectX9;

		class Renderer :
			public render::BaseRenderer
		{
		public:
			Renderer(DirectX9& direct3D, const gfx::ICbufferLoader& loader);

			void GetDevice(render::RendererInternalAccessor& accessor) const override;

			void Prepare(void) override;
			void Finish(void) override;

		private:

			render::IStage& OnCreateStage(unsigned int id, render::IQueue& queue, unsigned int flags) const override;
			render::IQueue& OnCreateQueue(void) const override;

			void OnExecute(void) override;

			DirectX9* m_pDirectX9;

			const gfx::ICbufferLoader* m_pLoader;

		};

	}
}

#endif