#include "Stage.h"

#ifdef ACL_API_DX9

#include "States.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace dx9
    {

		Stage::Stage(unsigned int id, render::IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader) : BaseStage(id, queue, flags, loader)
        {
            m_stageStates.Add<ScissorTest>(false);

			for(unsigned int i = 0; i < render::MAX_RENDERTARGETS; i++)
			{
				OnChangeRenderTarget(i, nullptr);
			}

			OnChangeDepthBuffer(nullptr);

			SetClearColor(gfx::FColor(0.0f, 0.0f, 0.0, 0.0f));
        }

		void Stage::OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture)
        {
			switch(slot)
	        {
	        case 0:
				m_stageStates.Add<BindRenderTarget0>(*pTexture);
		        break;
	        case 1:
				m_stageStates.Add<BindRenderTarget1>(*pTexture);
		        break;
	        case 2:
				m_stageStates.Add<BindRenderTarget2>(*pTexture);
		        break;
	        case 3:
				m_stageStates.Add<BindRenderTarget3>(*pTexture);
		        break;
			default:
				ACL_ASSERT(false);
	        }
        }

		void Stage::OnChangeDepthBuffer(const AclDepth* pDepth)
		{
			m_stageStates.Add<BindDepthBuffer>(pDepth);
		}

		void Stage::OnChangeClearColor(const gfx::FColor& color)
		{
			const float col[4] = { color.r, color.g, color.b, color.a };
			const auto flags = GetFlags();
			SetDrawCall(*new ClearTargets((flags & render::CLEAR) == render::CLEAR, (flags & render::CLEAR_Z) == render::CLEAR_Z, col));
		}

		bool Stage::OnGenerateMips(render::Instance& instance) const
		{
			return false;
		}

		void Stage::OnChangeViewport(const math::Rect& rect)
		{
			D3DVIEWPORT9 viewport;
			viewport.X = rect.x;
			viewport.Y = rect.y;
			viewport.Width = rect.width;
			viewport.Height = rect.height;
			viewport.MinZ = 0.0f;
			viewport.MaxZ = 1.0f;
			m_stageStates.Add<dx9::SetViewport>(viewport);
		}

    }
}

#endif