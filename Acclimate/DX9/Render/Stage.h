#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\..\Render\BaseStage.h"

namespace acl
{
	namespace gfx
	{
		class ICbufferLoader;
	}

	namespace dx9
	{

		class Stage final :
			public render::BaseStage
		{
		public:
			Stage(unsigned int id, render::IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader);

		private:

			void OnChangeDepthBuffer(const AclDepth* pDepth) override;
			void OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture) override;
			void OnChangeClearColor(const gfx::FColor& color) override;
			bool OnGenerateMips(render::Instance& instance) const override;
			void OnChangeViewport(const math::Rect& rect) override;
		};

	}
}

#endif