#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include "..\Device.h"
#include "..\Effect.h"
#include "..\Sprite.h"
#include "..\ConstantBuffer.h"
#include "..\..\Render\States.h"

#include "..\..\System\Assert.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{
			class VertexBuffer;
			class VertexDeclaration;
			class IndexBuffer;
			class Texture;
		}

		/****************************************
		* Vertex
		****************************************/

		class BindVertexBuffer : 
			public render::State<BindVertexBuffer>
		{
		public:	

			BindVertexBuffer(const d3d::VertexBuffer* vertexBuffer): pVertexBuffer(vertexBuffer) {}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetVertexBuffer(0, 0, pVertexBuffer);
			};	

		private:	
	
			const d3d::VertexBuffer* pVertexBuffer;

		};

		/****************************************
		* Index buffer
		****************************************/

		class BindIndexBuffer : 
			public render::State<BindIndexBuffer>
		{
		public:	

			BindIndexBuffer(const d3d::IndexBuffer* indexBuffer): pIndexBuffer(indexBuffer) {}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetIndexBuffer(pIndexBuffer);
			};	

		private:	
	
			const d3d::IndexBuffer* pIndexBuffer;

		};

		/****************************************
		* Instance buffer
		****************************************/

		class BindInstanceBuffer : 
			public render::State<BindInstanceBuffer>
		{
		public:	

			BindInstanceBuffer(const d3d::VertexBuffer* vertexBuffer, unsigned int offset): 
				pVertexBuffer(vertexBuffer), offset(offset)
			{
			}
	
			void Execute(const d3d::Device& device) const
			{
				device.SetVertexFreq(1, D3DSTREAMSOURCE_INSTANCEDATA | 1);
				device.SetVertexBuffer(1, offset, pVertexBuffer);
			};	

		private:
	
			const d3d::VertexBuffer* pVertexBuffer;
			unsigned int offset;
		};

		/****************************************
		* Vertex declaration
		****************************************/

		class SetVertexDeclaration : 
			public render::State<SetVertexDeclaration>
		{
		public:

			SetVertexDeclaration(const d3d::VertexDeclaration& declaration): pDeclaration(&declaration) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexDeclaration(pDeclaration);
			}

		private:

			const d3d::VertexDeclaration* pDeclaration;
		};

		/****************************************
		* Instance count
		****************************************/

		class SetInstanceCount : 
			public render::State<SetInstanceCount>
		{
		public:

			SetInstanceCount(unsigned int count): count(D3DSTREAMSOURCE_INDEXEDDATA | count) 
			{ 
				if(count == 1)
					this->count = 1;
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexFreq(0, count);
			}

		private:

			unsigned int count;
		};

		/****************************************
		* Texture 0
		****************************************/

		class BindTexture0 : 
			public render::State<BindTexture0>
		{
		public:

			BindTexture0(const d3d::Texture* pTexture): pTexture(pTexture)  {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(0, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 1
		****************************************/

		class BindTexture1 : 
			public render::State<BindTexture1>
		{
		public:

			BindTexture1(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(1, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 2
		****************************************/

		class BindTexture2 : 
			public render::State<BindTexture2>
		{
		public:

			BindTexture2(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(2, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Texture 3
		****************************************/

		class BindTexture3 : 
			public render::State<BindTexture3>
		{
		public:

			BindTexture3(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(3, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* SamplerState 0
		****************************************/

		class SamplerState0 : 
			public render::State<SamplerState0>
		{
		public:

			SamplerState0(int minFilter, int magFilter, int mipFilter, int adressU, int adressV): minFilter(minFilter), 
				magFilter(magFilter), mipFilter(mipFilter), adressU(adressU), adressV(adressV) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetSamplerState(0, minFilter, magFilter, mipFilter, adressU, adressV);
			}

		private:

			int minFilter, magFilter, mipFilter, adressU, adressV;
		};

		/****************************************
		* SamplerState 1
		****************************************/

		class SamplerState1 : 
			public render::State<SamplerState1>
		{
		public:

			SamplerState1(int minFilter, int magFilter, int mipFilter, int adressU, int adressV): minFilter(minFilter), magFilter(magFilter), 
				mipFilter(mipFilter), adressU(adressU), adressV(adressV) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetSamplerState(1, minFilter, magFilter, mipFilter, adressU, adressV);
			}

		private:

			int minFilter, magFilter, mipFilter, adressU, adressV;
		};

		/****************************************
		* SamplerState 2
		****************************************/

		class SamplerState2 : 
			public render::State<SamplerState2>
		{
		public:

			SamplerState2(int minFilter, int magFilter, int mipFilter, int adressU, int adressV): minFilter(minFilter), magFilter(magFilter), 
				mipFilter(mipFilter), adressU(adressU), adressV(adressV) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetSamplerState(2, minFilter, magFilter, mipFilter, adressU, adressV);
			}

		private:

			int minFilter, magFilter, mipFilter, adressU, adressV;
		};

		/****************************************
		* SamplerState 3
		****************************************/

		class SamplerState3 : 
			public render::State<SamplerState3>
		{
		public:

			SamplerState3(int minFilter, int magFilter, int mipFilter, int adressU, int adressV): minFilter(minFilter), magFilter(magFilter), 
				mipFilter(mipFilter), adressU(adressU), adressV(adressV) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetSamplerState(3, minFilter, magFilter, mipFilter, adressU, adressV);
			}

		private:

			int minFilter, magFilter, mipFilter, adressU, adressV;
		};

		/****************************************
		* VertexSamplerState0
		****************************************/

		class VertexSamplerState0 :
			public render::State<VertexSamplerState0>
		{
		public:

			VertexSamplerState0(int minFilter, int magFilter, int mipFilter, int adressU, int adressV) : minFilter(minFilter), magFilter(magFilter),
				mipFilter(mipFilter), adressU(adressU), adressV(adressV) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetSamplerState(D3DVERTEXTEXTURESAMPLER0, minFilter, magFilter, mipFilter, adressU, adressV);
			}

		private:

			int minFilter, magFilter, mipFilter, adressU, adressV;
		};

		/****************************************
		* Vertex Texture 0
		****************************************/

		class BindVertexTexture0 : 
			public render::State<BindVertexTexture0>
		{
		public:

			BindVertexTexture0(const d3d::Texture* pTexture): pTexture(pTexture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetTexture(D3DVERTEXTEXTURESAMPLER0, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* Bind shader
		****************************************/

		class BindShader : 
			public render::State<BindShader>
		{
		public:

			BindShader(const d3d::Effect& effect): pEffect(&effect) {}

			void Execute(const d3d::Device& device) const
			{
				device.GetDevice()->SetVertexShader(pEffect->GetVertexShader());
				device.GetDevice()->SetPixelShader(pEffect->GetPixelShader());
			}


		private:

			const d3d::Effect* pEffect;
		};

		/****************************************
		* BindRenderTarget 0
		****************************************/

		class BindRenderTarget0 : 
			public render::State<BindRenderTarget0>
		{
		public:

			BindRenderTarget0(const d3d::Texture& texture): pTexture(&texture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderTarget(0, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* BindRenderTarget 1
		****************************************/

		class BindRenderTarget1 : 
			public render::State<BindRenderTarget1>
		{
		public:

			BindRenderTarget1(const d3d::Texture& texture): pTexture(&texture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderTarget(1, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* BindRenderTarget 2
		****************************************/

		class BindRenderTarget2 : 
			public render::State<BindRenderTarget2>
		{
		public:

			BindRenderTarget2(const d3d::Texture& texture): pTexture(&texture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderTarget(2, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* BindRenderTarget 3
		****************************************/

		class BindRenderTarget3 : 
			public render::State<BindRenderTarget3>
		{
		public:

			BindRenderTarget3(const d3d::Texture& texture): pTexture(&texture) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderTarget(3, pTexture);
			}

		private:

			const d3d::Texture* pTexture;
		};

		/****************************************
		* DepthBuffer
		****************************************/

		class BindDepthBuffer : 
			public render::State<BindDepthBuffer>
		{
		public:

			BindDepthBuffer(const d3d::DepthBuffer* pDepthBuffer): pDepthBuffer(pDepthBuffer) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetDepthBuffer(pDepthBuffer);
			}

		private:

			const d3d::DepthBuffer* pDepthBuffer;
		};

		/****************************************
		* Alpha blend render::State
		****************************************/

		class AlphaBlendState : 
			public render::State<AlphaBlendState>
		{
		public:

			AlphaBlendState(bool bEnableAlpha, unsigned int srcBlend, unsigned int destBlend, unsigned int blendFunc, unsigned int alphaSrcBlend, unsigned int alphaDestBlend): bEnableAlpha(bEnableAlpha) 
			{
				this->srcBlend = ToBlend(srcBlend);
				this->destBlend = ToBlend(destBlend);
				this->blendFunc = ToFunc(blendFunc);
				this->alphaSrcBlend = ToBlend(alphaSrcBlend);
				this->alphaDestBlend = ToBlend(alphaDestBlend);
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderState(D3DRS_ALPHABLENDENABLE, bEnableAlpha);
				device.SetRenderState(D3DRS_SRCBLEND, srcBlend);
				device.SetRenderState(D3DRS_DESTBLEND, destBlend);
				device.SetRenderState(D3DRS_BLENDOP, blendFunc);
				device.SetRenderState(D3DRS_SRCBLENDALPHA, alphaSrcBlend);
				device.SetRenderState(D3DRS_DESTBLENDALPHA, alphaDestBlend);
			}

		private:

			static DWORD ToBlend(unsigned int blend)
			{
				switch(blend)
				{
				case 0: //one
					return D3DBLEND_ONE;
				case 1: //src alpha
					return D3DBLEND_SRCALPHA;
				case 2: //inv src alpha
					return D3DBLEND_INVSRCALPHA;
				case 3: //src alpha
					return D3DBLEND_SRCCOLOR;
				case 4: //inv src alpha
					return D3DBLEND_INVSRCCOLOR;
				default:
					return D3DBLEND_ZERO;
				} 
			}

			static DWORD ToFunc(unsigned int func)
			{
				switch(func)
				{
				case 0:
					return D3DBLENDOP_ADD;
				case 1:
					return D3DBLENDOP_SUBTRACT;
				default:
					return D3DBLENDOP_ADD;
				}
			}

			bool bEnableAlpha;
			unsigned int srcBlend, destBlend, blendFunc, alphaSrcBlend, alphaDestBlend;
		};

		/****************************************
		* Depth
		****************************************/

		class DepthState : 
			public render::State<DepthState>
		{
		public:

			DepthState(bool bEnable, bool bWrite): bEnable(bEnable), bWrite(bWrite) {}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderState(D3DRS_ZENABLE, bEnable);
				device.SetRenderState(D3DRS_ZWRITEENABLE, bWrite);
			}

		private:

			bool bEnable;
			bool bWrite;
		};

		/****************************************
		* Cull mode
		****************************************/

		class CullState : 
			public render::State<CullState>
		{
		public:

			CullState(D3DCULL mode) : model(mode)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderState(D3DRS_CULLMODE, model);
			}

		private:

			D3DCULL model;
		};

		/****************************************
		* Cull mode
		****************************************/

		class ScissorTest : 
			public render::State<ScissorTest>
		{
		public:

			ScissorTest(bool bTest): bTest(bTest), bRect(false)
			{
			}

			ScissorTest(bool bTest, const RECT& r): bTest(bTest), r(r), bRect(true)
			{
				ACL_ASSERT(r.left >= 0 && r.top >= 0);
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderState(D3DRS_SCISSORTESTENABLE, bTest);
				if(bRect)
					device.SetScissorRect(&r);
				else
					device.SetScissorRect(nullptr);
			}

		private:

			bool bTest;
			bool bRect;
			RECT r;
		};

		/****************************************
		* FillMode
		****************************************/

		class FillMode : 
			public render::State<FillMode>
		{
		public:

			FillMode(D3DFILLMODE mode): mode(mode) 
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetRenderState(D3DRS_FILLMODE, mode);
			}

		private:

			D3DFILLMODE mode;
		};

		/****************************************
		* Viewport
		****************************************/

		class SetViewport : 
			public render::State<SetViewport>
		{
		public:

			SetViewport(const D3DVIEWPORT9& viewport): viewport(viewport) {
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetViewport(viewport);
			}

		private:

			D3DVIEWPORT9 viewport;
		};

/***********************************************
*      DRAW CALLS
***********************************************/


		/****************************************
		* Draw indexed
		****************************************/

		class DrawIndexedPrimitives : 
			public render::DrawCall<DrawIndexedPrimitives>
		{
		public:	

			DrawIndexedPrimitives(RenderTypes primType, unsigned int vertexCount, unsigned int startVertex, unsigned int startIndex): primType(primType), numVertices(vertexCount), startVertex(startVertex), startIndex(startIndex)
			{ 
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawIndexedPrimitives(*this);
			}
	
			void Execute(const d3d::Device& device) const 
			{ 
				device.DrawIndexed(primType, startVertex, startIndex, numVertices); 
			}

		private:	


			RenderTypes primType;	
			unsigned int numVertices;
			unsigned int startIndex;
			unsigned int startVertex;
		};

		/****************************************
		* Clear
		****************************************/

		class ClearTargets : 
			public render::DrawCall<ClearTargets>
		{
		public:

			ClearTargets(bool bClear, bool bClearDepth, const float* pColor) : dClear(0), pColor(nullptr)
			{ 
				if(bClear)
				{
					dClear |= D3DCLEAR_TARGET;
					if(pColor)
					{
						this->pColor = new float[4];
						memcpy(this->pColor, pColor, sizeof(float)* 4);
					}
				}
					
				if(bClearDepth)
					dClear |= D3DCLEAR_ZBUFFER;
			}

			ClearTargets(const ClearTargets& clear) : dClear(clear.dClear), pColor(nullptr)
			{
				if(clear.pColor)
				{
					pColor = new float[4];
					memcpy(this->pColor, clear.pColor, sizeof(float)* 4);
				}
			}

			~ClearTargets(void)
			{
				delete[] pColor;
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new ClearTargets(*this);
			}

			void Execute(const d3d::Device& device) const
			{
				if(dClear != 0)
					device.Clear(dClear, pColor);
			}

		private:

			DWORD dClear;
			float* pColor;
		};

		/****************************************
		* Draw
		****************************************/

		class DrawPrimitives :
			public render::DrawCall<DrawPrimitives>
		{
		public:

			DrawPrimitives(RenderTypes primType, unsigned int vertexCount, unsigned int startVertex) : primType(primType), numVertices(vertexCount), startVertex(startVertex)
			{
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawPrimitives(*this);
			}

			void Execute(const d3d::Device& device) const
			{
				device.Draw(primType, startVertex, numVertices);
			}

		private:


			RenderTypes primType;
			unsigned int numVertices;
			unsigned int startVertex;
		};

/***************************************************
* C-Buffer
***************************************************/


		/****************************************
		* C-buffer Vertex 0
		****************************************/

		class CBufferV0 : 
			public render::State<CBufferV0>
		{
		public:

			CBufferV0(const d3d::ConstantBuffer& buffer): pBuffer(&buffer)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexConstants(0, pBuffer->GetBuffer(), pBuffer->GetNumConsts());
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* C-buffer Vertex 1
		****************************************/

		class CBufferV1 : 
			public render::State<CBufferV1>
		{
		public:

			CBufferV1(const d3d::ConstantBuffer& buffer) : pBuffer(&buffer)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetVertexConstants(16, pBuffer->GetBuffer(), pBuffer->GetNumConsts());
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* C-buffer Pixel 0
		****************************************/

		class CBufferP0 : 
			public render::State<CBufferP0>
		{
		public:

			CBufferP0(const d3d::ConstantBuffer& buffer) : pBuffer(&buffer)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelConstants(0, pBuffer->GetBuffer(), pBuffer->GetNumConsts());
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

		/****************************************
		* C-buffer Pixel 1
		****************************************/

		class CBufferP1 : 
			public render::State<CBufferP1>
		{
		public:

			CBufferP1(const d3d::ConstantBuffer& buffer) : pBuffer(&buffer)
			{
			}

			void Execute(const d3d::Device& device) const
			{
				device.SetPixelConstants(16, pBuffer->GetBuffer(), pBuffer->GetNumConsts());
			}

		private:

			const d3d::ConstantBuffer* pBuffer;
		};

	}
}

#endif