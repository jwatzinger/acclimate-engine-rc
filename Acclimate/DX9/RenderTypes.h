#pragma once

namespace acl
{
	namespace dx9
	{

		enum class RenderTypes {
			TRI = 0, LINE, DATA, TRI_STRIP, POINT
		};

	}
}