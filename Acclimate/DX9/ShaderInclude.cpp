#include "ShaderInclude.h"

#ifdef ACL_API_DX9

#include <fstream>
#include "..\File\File.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			ShaderInclude::ShaderInclude(const std::wstring& stBasefile): m_stBaseDir(conv::ToA(file::FilePath(stBasefile, L"")) + "\\")
			{
			}

			HRESULT ShaderInclude::Open(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes)
			{
				// Open file
				std::ifstream file(m_stBaseDir + pFileName, std::ios::binary);
				if(!file.is_open())
				{
					*ppData = NULL;
					*pBytes = 0;
					return S_OK;
				}

				// Get filesize
				file.seekg(0, std::ios::end);
				UINT size = static_cast<UINT>(file.tellg());
				file.seekg(0, std::ios::beg);

				// Create buffer and read file
				std::unique_ptr<char[]> data(new char[size]);
				file.read(data.get(), size);
				*ppData = data.get();
				*pBytes = size;
				m_data.push_back(std::move(data));
				return S_OK;
			}

			HRESULT ShaderInclude::Close(LPCVOID pData)
			{
				m_data.begin()->release();
				return S_OK;
			}		
		}
	}
}

#endif