#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <string>
#include <list>
#include <memory>
#include <d3dx9.h>

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class ShaderInclude :
				public ID3DXInclude
			{
			public:
				ShaderInclude(const std::wstring& stBasefile);

				HRESULT __stdcall Open(D3DXINCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID *ppData, UINT *pBytes) override;

				HRESULT __stdcall Close(LPCVOID pData) override;

			private:

				std::string m_stBaseDir;
				std::list<std::unique_ptr<char[]>> m_data;
			};

		}
	}
}

#endif