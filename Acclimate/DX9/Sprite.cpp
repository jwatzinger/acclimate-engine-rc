#include "Sprite.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexDeclaration.h"
#include "..\Safe.h"
#include "..\Math\Vector.h"
#include "..\Math\Vector3.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			const size_t TEMP_NUM_SPRITES = 10000;

			Sprite::Sprite(const Device& device) : m_nextFreeId(0), m_pDevice(&device),
				m_pBuffer(nullptr)
			{
				m_pVertexBuffer = new VertexBuffer(device, RenderTypes::TRI, sizeof(SpriteVertex), TEMP_NUM_SPRITES*4, VertexBufferUsage::DYNAMIC_WRITEONLY);

				m_pIndexBuffer = new IndexBuffer(device, TEMP_NUM_SPRITES*6);

				//index buffer 

				unsigned int* pData = nullptr;
				m_pIndexBuffer->Lock((void**)&pData, BufferLock::DISCARD_NOOVER);

				unsigned int id = 0;
				for(unsigned int i = 0; i < TEMP_NUM_SPRITES * 6;)
				{
					pData[i++] = id;
					pData[i++] = id + 1;
					pData[i++] = id + 3;
					pData[i++] = id + 3;
					pData[i++] = id + 1;
					pData[i++] = id + 2;
					id += 4;
				}

				m_pIndexBuffer->Unlock();

				auto mode = m_pDevice->GetDisplayMode();
				m_vInvScreenSize = math::Vector2f(1.0f / mode.Width, 1.0f / mode.Height);
				m_vInvHalfScreenSize = m_vInvScreenSize * 2.0f;
			}

			Sprite::~Sprite(void)
			{
				delete m_pVertexBuffer;
				delete m_pIndexBuffer;
			}

			void Sprite::Reset(void)
			{
				SpriteVertex* pBuffer = nullptr;
				m_pVertexBuffer->Lock((void**)&pBuffer, BufferLock::DISCARD);
				m_pVertexBuffer->Unlock();
				m_nextFreeId = 0;

				auto mode = m_pDevice->GetDisplayMode();
				m_vInvScreenSize = math::Vector2f(1.0f / mode.Width, 1.0f / mode.Height);
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (mode.Width * 0.5f), 1.0f / (mode.Height * 0.5f));
			}

			void Sprite::OnReset(void)
			{
				m_pVertexBuffer->OnReset();
			}

			void Sprite::OnRecreate(void)
			{
				m_pVertexBuffer->OnRecreate();
			}

			void Sprite::Lock(void)
			{
				m_pVertexBuffer->Lock(0, 0, (void**)&m_pBuffer, BufferLock::NOOVER);
			}

			void Sprite::Unlock(void)
			{
				m_pVertexBuffer->Unlock();
			}

			size_t Sprite::Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const RECT& rSrcRect, const math::Vector2f& vScale, float /*fAngle*/)
			{
				const math::Vector2 vSrcSize(rSrcRect.right - rSrcRect.left, rSrcRect.bottom - rSrcRect.top);
				const math::Vector2f vSize(vSrcSize.x*vScale.x, vSrcSize.y*vScale.y);

				const float leftVertex = vPosition.x  * m_vInvHalfScreenSize.x - 1.0f - m_vInvScreenSize.x;
				const float rightVertex = leftVertex + vSize.x * m_vInvHalfScreenSize.x;
				const float topVertex = -vPosition.y * m_vInvHalfScreenSize.y + 1.0f - m_vInvScreenSize.y;
				const float bottomVertex = topVertex - vSize.y * m_vInvHalfScreenSize.y;

				const float leftCoord = rSrcRect.left / (float)vTextureSize.x;
				const float rightCoord = rSrcRect.right / (float)vTextureSize.x;
				const float topCoord = rSrcRect.top / (float)vTextureSize.y;
				const float bottomCoord = rSrcRect.bottom / (float)vTextureSize.y;

				SpriteVertex Vertices[] =
				{
					{ leftVertex, topVertex, vPosition.z, leftCoord, topCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, topVertex, vPosition.z, rightCoord, topCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, bottomVertex, vPosition.z, rightCoord, bottomCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ leftVertex, bottomVertex, vPosition.z, leftCoord, bottomCoord, 1.0f, 1.0f, 1.0f, 1.0f }
				};

				memcpy(&m_pBuffer[m_nextFreeId * 4], Vertices, sizeof(SpriteVertex)* 4);

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const math::Vector2f& vScale, float /*angle*/)
			{
				const float leftVertex = vPosition.x - m_vInvScreenSize.x;
				const float rightVertex(leftVertex + vScale.x);
				const float topVertex = vPosition.y - m_vInvScreenSize.y;
				const float bottomVertex(topVertex + vScale.y);

				SpriteVertex Vertices[] =
				{
					{ leftVertex, topVertex, vPosition.z, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, topVertex, vPosition.z, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, bottomVertex, vPosition.z, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ leftVertex, bottomVertex, vPosition.z, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }
				};

				memcpy(&m_pBuffer[m_nextFreeId * 4], Vertices, sizeof(SpriteVertex)* 4);

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(SpriteVertex* pVertices, size_t numQuads)
			{
				memcpy(&m_pBuffer[m_nextFreeId*4], pVertices, numQuads * 4 * sizeof(SpriteVertex));

				m_nextFreeId += numQuads;
				return m_nextFreeId - numQuads;
			}

			const Device& Sprite::GetDevice(void) const
			{
				return *m_pDevice;
			}

			const VertexBuffer& Sprite::GetVertexBuffer(void) const
			{
				return *m_pVertexBuffer;
			}

			const IndexBuffer& Sprite::GetIndexBuffer(void) const
			{
				return *m_pIndexBuffer;
			}

			const math::Vector2f& Sprite::GetInvHalfScreenSize(void) const
			{
				return m_vInvHalfScreenSize;
			}

			const math::Vector2f& Sprite::GetInvScreenSize(void) const
			{
				return m_vInvScreenSize;
			}

		}
	}
}

#endif