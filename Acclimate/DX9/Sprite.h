#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include "..\Math\Rect.h"
#include "..\Math\Vector2f.h"

namespace acl
{
    namespace math
    {
		struct Vector2f;
        struct Vector3;
    }

	namespace dx9
	{
		namespace d3d
		{

			struct SpriteVertex
			{
				float x, y, z;
				float u, v;
				float r, g, b, a;
			};

			class Device;
			class VertexBuffer;
			class IndexBuffer;

			class Sprite
			{
			public:
				Sprite(const Device& device);
				~Sprite(void);

				void Reset(void);
				void OnReset(void);
				void OnRecreate(void);

				void Lock(void);
				void Unlock(void);

				size_t Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle); 
				size_t Draw(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle); 
				size_t Draw(SpriteVertex* pVertices, size_t numQuads);

				const Device& GetDevice(void) const;
				const VertexBuffer& GetVertexBuffer(void) const;
				const IndexBuffer& GetIndexBuffer(void) const;
				const math::Vector2f& GetInvScreenSize(void) const;
				const math::Vector2f& GetInvHalfScreenSize(void) const;

			private:
			
				Sprite(const Sprite& sprite) {};

				size_t m_nextFreeId;

				VertexBuffer* m_pVertexBuffer;
				IndexBuffer* m_pIndexBuffer;
				const Device* m_pDevice;

				SpriteVertex* m_pBuffer;

				math::Vector2f m_vInvScreenSize, m_vInvHalfScreenSize;

			};

		}
	}
}

#endif