#include "Texture.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "..\Safe.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			Texture::Texture(const Device& device, LPCWSTR lpFileName, D3DFORMAT format): m_bRenderTarget(false), m_pDevice(&device)
			{
				//get image info
				D3DXGetImageInfoFromFile(lpFileName, &m_d3dImageInfo);
				if(format == D3DFMT_UNKNOWN)
					format = m_d3dImageInfo.Format;
				else
					m_d3dImageInfo.Format = format;
				//create texture
				HRESULT hr = D3DXCreateTextureFromFileEx(device.GetDevice(), lpFileName, m_d3dImageInfo.Width, m_d3dImageInfo.Height, m_d3dImageInfo.MipLevels, 0, format, D3DPOOL_MANAGED, D3DX_FILTER_NONE, D3DX_FILTER_NONE, 0, 0, 0, &m_lpTexture);
				//check for failure
				if(FAILED(hr))
					throw resourceLoadException(lpFileName);

				m_vSize = math::Vector2(m_d3dImageInfo.Width, m_d3dImageInfo.Height);
				//aquire surface
				m_lpTexture->GetSurfaceLevel(0, &m_lpD3DSurface);
			}

			Texture::Texture(const Device& device, int width, int height, D3DFORMAT format, bool bRenderTarget): m_vSize(width, height),
				m_bRenderTarget(bRenderTarget), m_pDevice(&device)
			{
				if(bRenderTarget)
					m_lpTexture = device.CreateTexture(width, height, D3DUSAGE_RENDERTARGET, format, D3DPOOL_DEFAULT);
				else
					m_lpTexture = device.CreateTexture(width, height, 0, format, D3DPOOL_MANAGED);

				m_d3dImageInfo.Width = width;
				m_d3dImageInfo.Height = height;
				m_d3dImageInfo.Format = format;
				m_d3dImageInfo.MipLevels = 1;

				//aquire surface
				m_lpTexture->GetSurfaceLevel(0, &m_lpD3DSurface);
			}

			Texture::~Texture(void)
			{
				SAFE_RELEASE(m_lpD3DSurface);
				SAFE_RELEASE(m_lpTexture);
			}

			const math::Vector2& Texture::GetSize(void) const
			{
				return m_vSize;
			}

			D3DFORMAT Texture::GetFormat(void) const
			{
				return m_d3dImageInfo.Format;
			}


			LPDIRECT3DSURFACE9 Texture::GetSurface(void) const
			{
				return m_lpD3DSurface;
			}

			LPDIRECT3DTEXTURE9 Texture::GetTexture(void) const
			{
				return m_lpTexture;
			}

			void Texture::Lock(D3DLOCKED_RECT& rect) const
			{
				if( FAILED( m_lpD3DSurface->LockRect(&rect, nullptr, 0) ) )
				{
					throw apiException();
				}
			}

			void Texture::Unlock(void) const
			{
				m_lpD3DSurface->UnlockRect();
			}

			void Texture::Reload(const math::Vector2& vSize)
			{
				if(m_bRenderTarget)
				{
					if(m_lpD3DSurface)
						m_lpD3DSurface->Release();
					if(m_lpTexture)
						m_lpTexture->Release();

					m_d3dImageInfo.Width = vSize.x;
					m_d3dImageInfo.Height = vSize.y;

					m_lpTexture = m_pDevice->CreateTexture(m_d3dImageInfo.Width, m_d3dImageInfo.Height, D3DUSAGE_RENDERTARGET, m_d3dImageInfo.Format, D3DPOOL_DEFAULT);
					m_lpTexture->GetSurfaceLevel(0, &m_lpD3DSurface);
				}
			}

			void Texture::OnReset(void)
			{
				if(m_bRenderTarget)
				{
					m_lpD3DSurface->Release();
					m_lpTexture->Release();
					m_lpD3DSurface = nullptr;
					m_lpTexture = nullptr;
				}
			}

		}
	}
}

#endif