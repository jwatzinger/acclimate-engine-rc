#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3dx9.h>
#include "..\Math\Vector.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Device;

			class Texture
			{
			public:
				Texture(const Device& device, LPCWSTR lpFileName, D3DFORMAT format = D3DFMT_A8R8G8B8);
				Texture(const Device& device, int width, int height, D3DFORMAT format, bool bRenderTarget = true);
				~Texture(void);

				void Lock(D3DLOCKED_RECT& rect) const;
				void Unlock(void) const;
				void Reload(const math::Vector2& vSize);
				void OnReset(void);

				const math::Vector2& GetSize(void) const;
				D3DFORMAT GetFormat(void) const;

				LPDIRECT3DSURFACE9 GetSurface(void) const;
				LPDIRECT3DTEXTURE9 GetTexture(void) const;

			private:

				bool m_bRenderTarget;

				const Device* m_pDevice;
				LPDIRECT3DTEXTURE9 m_lpTexture; 
				LPDIRECT3DSURFACE9 m_lpD3DSurface;
				D3DXIMAGE_INFO m_d3dImageInfo;
				math::Vector2 m_vSize;
			};

		}
	}
}

#endif