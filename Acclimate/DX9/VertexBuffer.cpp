#include "VertexBuffer.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "..\Safe.h"
#include "..\System\Exception.h"
#include "..\System\Bit.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			VertexBuffer::VertexBuffer(const Device& device, LPDIRECT3DVERTEXBUFFER9 lpVertexBuffer, RenderTypes type, unsigned int vertexSize, VertexBufferUsage usage, unsigned int numVertices): 
				m_lpVertexBuffer(lpVertexBuffer), m_vertexSize(vertexSize), m_type(type), m_bLocked(false), m_pDevice(&device), m_usage(usage), m_numVertices(numVertices)
			{
			}

			VertexBuffer::VertexBuffer(const Device& device, RenderTypes type, unsigned int length, unsigned int numVertices, VertexBufferUsage usage): 
				m_lpVertexBuffer(nullptr), m_vertexSize(length), m_type(type), m_bLocked(false), m_pDevice(&device), m_usage(usage), m_numVertices(numVertices)
			{
				DWORD d3dusage = static_cast<DWORD>(usage);

				D3DPOOL pool = D3DPOOL_MANAGED;
				if( bit::contains( d3dusage, static_cast<DWORD>(D3DUSAGE_DYNAMIC) ) )
					pool = D3DPOOL_DEFAULT;

				m_lpVertexBuffer = device.CreateVertexBuffer(length*numVertices, d3dusage, pool);
			}

			VertexBuffer::VertexBuffer(const VertexBuffer& vertexBuffer): m_lpVertexBuffer(nullptr), m_bLocked(vertexBuffer.m_bLocked),
				m_vertexSize(vertexBuffer.m_vertexSize), m_type(vertexBuffer.m_type), m_pDevice(vertexBuffer.m_pDevice), m_numVertices(vertexBuffer.m_numVertices), m_usage(vertexBuffer.m_usage)
			{
				DWORD d3dusage = static_cast<DWORD>(m_usage);

				D3DPOOL pool = D3DPOOL_MANAGED;
				if( bit::contains( d3dusage, static_cast<DWORD>(D3DUSAGE_DYNAMIC) ) )
					pool = D3DPOOL_DEFAULT;

				m_lpVertexBuffer = m_pDevice->CreateVertexBuffer(m_vertexSize*m_numVertices, d3dusage, pool);
			}

			VertexBuffer::VertexBuffer(VertexBuffer&& vertexBuffer): m_lpVertexBuffer(vertexBuffer.m_lpVertexBuffer), m_bLocked(vertexBuffer.m_bLocked),
				m_vertexSize(vertexBuffer.m_vertexSize), m_type(vertexBuffer.m_type), m_pDevice(vertexBuffer.m_pDevice), m_numVertices(vertexBuffer.m_numVertices), m_usage(vertexBuffer.m_usage)
			{
				vertexBuffer.m_lpVertexBuffer = nullptr;
				vertexBuffer.m_vertexSize = 0;
				vertexBuffer.m_numVertices = 0;
				vertexBuffer.m_bLocked = false;
			}

			VertexBuffer::~VertexBuffer(void)
			{
				if(m_bLocked)
					m_lpVertexBuffer->Unlock();
				SAFE_RELEASE(m_lpVertexBuffer);
			}

			void VertexBuffer::OnReset(void)
			{
				if(m_usage == VertexBufferUsage::DYNAMIC || m_usage == VertexBufferUsage::DYNAMIC_WRITEONLY)
				{
					m_lpVertexBuffer->Release();
					m_lpVertexBuffer = nullptr;
				}
			}

			void VertexBuffer::OnRecreate(void)
			{
				if(m_usage == VertexBufferUsage::DYNAMIC || m_usage == VertexBufferUsage::DYNAMIC_WRITEONLY)
				{
					DWORD d3dusage = static_cast<DWORD>(m_usage);

					D3DPOOL pool = D3DPOOL_MANAGED;
					if(bit::contains(d3dusage, static_cast<DWORD>(D3DUSAGE_DYNAMIC)))
						pool = D3DPOOL_DEFAULT;

					m_lpVertexBuffer = m_pDevice->CreateVertexBuffer(m_vertexSize*m_numVertices, d3dusage, pool);
				}
			}

			VertexBuffer& VertexBuffer::operator=(const VertexBuffer& vertexBuffer)
			{
				vertexBuffer.m_lpVertexBuffer->AddRef();
				SAFE_RELEASE(m_lpVertexBuffer);
				m_lpVertexBuffer = vertexBuffer.m_lpVertexBuffer;
				m_vertexSize = vertexBuffer.m_vertexSize;
				m_bLocked = vertexBuffer.m_bLocked;
				m_numVertices = vertexBuffer.m_numVertices;
				m_type = vertexBuffer.m_type;
				m_usage = vertexBuffer.m_usage;
				m_pDevice = vertexBuffer.m_pDevice;
	
				return *this;
			}

			LPDIRECT3DVERTEXBUFFER9 VertexBuffer::GetVertexBuffer(void) const
			{
				return m_lpVertexBuffer;
			}

			unsigned int VertexBuffer::GetVertexSize(void) const
			{
				return m_vertexSize;
			}

			void VertexBuffer::Lock(void** pBuffer, BufferLock lock)
			{
				if(m_bLocked)
					return;

				HRESULT hr = m_lpVertexBuffer->Lock(0, 0, pBuffer, static_cast<DWORD>(lock));
				if(FAILED(hr))
					throw apiException();
				m_bLocked = true;
			}

			void VertexBuffer::Lock(unsigned int start, unsigned int size, void** pBuffer, BufferLock lock)
			{
				if(m_bLocked)
					return;

				HRESULT hr = m_lpVertexBuffer->Lock(start*m_vertexSize, size*m_vertexSize, pBuffer, static_cast<DWORD>(lock));
				if(FAILED(hr))
					throw apiException();
				m_bLocked = true;
			}

			void VertexBuffer::Unlock(void)
			{
				if(!m_bLocked)
					return;

				m_lpVertexBuffer->Unlock();
				m_bLocked = false;
			}

			RenderTypes VertexBuffer::GetType(void) const
			{
				return m_type;
			}

		}
	}
}

#endif