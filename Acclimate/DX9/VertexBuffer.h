#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>
#include "BufferLock.h"
#include "RenderTypes.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			enum class VertexBufferUsage
			{
				NONE = 0x000, 
				WRITEONLY = D3DUSAGE_WRITEONLY, 
				DYNAMIC = D3DUSAGE_DYNAMIC, 
				DYNAMIC_WRITEONLY = D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC
			};

			class Device;

			class VertexBuffer
			{
			public:
				VertexBuffer(const Device& device, LPDIRECT3DVERTEXBUFFER9 lpVertexBuffer, RenderTypes type, unsigned int vertexSize, VertexBufferUsage usage, unsigned int numVertices);
				VertexBuffer(const Device& device, RenderTypes type, unsigned int length, unsigned int numVertices, VertexBufferUsage usage);
				VertexBuffer(const VertexBuffer& vertexBuffer);
				VertexBuffer(VertexBuffer&& vertexBuffer);
				~VertexBuffer(void);

				void OnReset(void);
				void OnRecreate(void);

				VertexBuffer& operator=(const VertexBuffer& vertexBuffer);

				void Lock(void** pBuffer, BufferLock lock);
				void Lock(unsigned int start, unsigned int size, void** pBuffer, BufferLock lock);
				void Unlock(void);

				LPDIRECT3DVERTEXBUFFER9 GetVertexBuffer(void) const;
				unsigned int GetVertexSize(void) const;
				RenderTypes GetType(void) const;

			private:

				unsigned int m_vertexSize;
				unsigned int m_numVertices;
				bool m_bLocked;

				RenderTypes m_type;
				VertexBufferUsage m_usage;

				const Device* m_pDevice;

				LPDIRECT3DVERTEXBUFFER9 m_lpVertexBuffer;
			};

		}
	}
}

#endif