#include "VertexDeclaration.h"

#ifdef ACL_API_DX9

#include "Device.h"
#include "..\Safe.h"

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			VertexDeclaration::VertexDeclaration(const Device& device, const D3DVERTEXELEMENT9* pElements)
			{
				m_lpDeclaration = device.CreateVertexDeclaration(pElements);
			}


			VertexDeclaration::~VertexDeclaration(void)
			{
				SAFE_RELEASE(m_lpDeclaration);
			}

			LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration::GetD3DDeclaration(void) const
			{
				return m_lpDeclaration;
			}

		}
	}
}

#endif