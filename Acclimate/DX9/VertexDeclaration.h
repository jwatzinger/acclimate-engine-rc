#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_DX9

#include <d3d9.h>

namespace acl
{
	namespace dx9
	{
		namespace d3d
		{

			class Device;

			class VertexDeclaration
			{
			public:
				VertexDeclaration(const Device& device, const D3DVERTEXELEMENT9* pElements);
				~VertexDeclaration(void);

				LPDIRECT3DVERTEXDECLARATION9 GetD3DDeclaration(void) const;

			private:

				LPDIRECT3DVERTEXDECLARATION9 m_lpDeclaration;
			};
		}
	}
}

#endif