#include "Engine.h"
#include "ApiDef.h"

#ifdef ACL_API_DX9
#include "DX9\Engine.h"
#elif defined(ACL_API_DX11)
#include "DX11\Engine.h"
#elif defined(ACL_API_GL4)
#include "GL4\Engine.h"
#endif


namespace acl
{

	BaseEngine& createEngine(HINSTANCE hInstance)
	{
#ifdef ACL_API_DX9
		return *new dx9::Engine(hInstance);
#elif defined(ACL_API_DX11)
		return *new dx11::Engine(hInstance);
#elif defined(ACL_API_GL4)
		return *new gl4::Engine(hInstance);
#endif
	}
}