#pragma once
#include "BaseEngine.h"
#include "Core\Dll.h"

namespace acl
{

	ACCLIMATE_API BaseEngine& createEngine(HINSTANCE hInstance);
	
}
