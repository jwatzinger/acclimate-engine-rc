#include "Component.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace ecs
	{

		BaseComponent::BaseComponent(void) : m_pParent(nullptr)
		{
		}

		BaseComponent::BaseComponent(const BaseComponent& component) : m_pParent(nullptr)
		{
		}

		void BaseComponent::SetParent(Entity& entity)
		{
			ACL_ASSERT(!m_pParent); // component ownership should never switch
			m_pParent = &entity;
		}

		Entity& BaseComponent::GetParent(void)
		{
			ACL_ASSERT(m_pParent);
			return *m_pParent;
		}

		const Entity& BaseComponent::GetParent(void) const
		{
			ACL_ASSERT(m_pParent);
			return *m_pParent;
		}

		BaseComponent::Family BaseComponent::family_count = 0;

		void BaseComponent::ResetCount(void)
		{
			family_count = 0;
		}

		BaseComponent::Family BaseComponent::GenerateFamilyId(void)
		{
			return family_count++;
		}

	}
}