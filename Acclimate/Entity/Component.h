/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		/*********************************
		* Base component struct
		*********************************/

		class Entity;

        /// Used for implementing the CRTP for components
		/** This struct is used as a base from which the actual component derives.
		*   It offers an implementation of the Curiously recurring template pattern,
		*   by storing a static component counter. Each different component struct
        *   will increase this counter and aquire its unique id from it. */
		struct ACCLIMATE_API BaseComponent
		{
			typedef unsigned int Family; ///< Component struct type

			BaseComponent(void);
			BaseComponent(const BaseComponent& component);
			virtual ~BaseComponent(void) = 0 {}

			virtual BaseComponent& Clone(void) const = 0;

			void SetParent(Entity& entity);

			template<typename Type>
			Type* GetAttribute(const std::wstring& stName);

			Entity& GetParent(void);
			const Entity& GetParent(void) const;
			virtual Family GetFamily(void) const = 0;

			static void ResetCount(void); // todo: make safer, now it could be called everywhere and screw up the ids,
										  // but it is needed to allow projects to be closed

		protected:
			static Family GenerateFamilyId(void);

			virtual void* GetAttribute(const std::wstring& stName, const std::type_info** type) { return nullptr; }

		private:
			static Family family_count; /**<    Counter of instanciated components.
                *   Each component of a different type that is created, or whose 
                *   family function is called the first time will increase this 
                *   counter. The counter is used as the ID of the next component. */

			Entity* m_pParent;
		};

		template<typename Type>
		Type* BaseComponent::GetAttribute(const std::wstring& stName)
		{
			const std::type_info* pInType = nullptr;
			if(auto pAttribute = GetAttribute(stName, &pInType))
			{
#ifdef _DEBUG
				if(*pInType == typeid(Type))
#endif
					return (Type*)pAttribute;
			}

			return nullptr;
		}

		/*********************************
		* Component struct
		**********************************/

        /// The component parent struct
		/** Each component must derive from this struct. It automatically sets its
        *   unique family id on creation, which can then be accessed via a static
        *   method, so you can compare the type of a component object with the type
        *   of a component struct. */
		template <typename Derived> //template to pick implementation
		struct Component : 
			public BaseComponent 
		{
            /** Returns the "family" type of the component.
			 *	This method sets the type id for the specific component struct
             *  on the first call, and returns it from there on. The id
             *  is consistend between the running time of a program and cannot
             *  be altered, but it may alter between different runs, depending
             *  on the first creation order of the components. 
             *
             *  @return The unique id of this component instance
             */
			static Family family(void) 
            {
			    //increase base family count on first access
			    static const BaseComponent::Family Family = GenerateFamilyId(); 
			    return Family;
		    }

			BaseComponent& Clone(void) const override final
			{
				return *new Derived(*(Derived*)this);
			}

			Family GetFamily(void) const override final
			{
				return family();
			}

			virtual ~Component(void) = 0 {};
		};

	}
}
