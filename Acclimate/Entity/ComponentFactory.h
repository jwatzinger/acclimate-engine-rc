#pragma once
#include <string>
#include <unordered_map>
#include "ComponentRegistry.h"
#include "..\Core\Dll.h"
#include "..\System\Log.h"

namespace acl
{
	namespace ecs
	{

		class Entity;

		class ACCLIMATE_API ComponentFactory
		{
		public:
			typedef void (*FunctionPtr)(void);

		private:
			typedef std::unordered_map<size_t, FunctionPtr> FactoryMethodMap;

		public:

			template<typename Component, typename... Args>
			void Register(void);
			template<typename Component>
			void Unregister(void);

			template<typename ... Args>
			void AttachComponent(size_t id, Entity& entity, Args&&... args) const;
			template<typename ... Args>
			void AttachComponent(const std::wstring& stName, Entity& entity, Args&&... args) const;

		private:
#pragma warning( disable: 4251 )

			template<typename Type, typename... Args>
			static void AttachComponentTemplate(Entity& entity, Args... args);

			FactoryMethodMap m_mMethods;
#pragma warning( default: 4251 )
		};

		template<typename Component, typename... Args>
		void ComponentFactory::Register(void)
		{
			m_mMethods[Component::family()] = (ecs::ComponentFactory::FunctionPtr)&ComponentFactory::AttachComponentTemplate<Component, Args...>;
		}

		template<typename Component>
		void ComponentFactory::Unregister(void)
		{
			auto itr = m_mMethods.find(Component::family());
			if(itr != m_mMethods.end())
				m_mMethods.erase(itr);
		}

		template<typename ... Args>
		void ComponentFactory::AttachComponent(size_t id, Entity& entity, Args&&... args) const
		{
			void (*pPtr)(Entity&, Args...) = (void (*)(Entity&, Args...))m_mMethods.at(id);
			pPtr(entity, args...);
		}

		template<typename ... Args>
		void ComponentFactory::AttachComponent(const std::wstring& stName, Entity& entity, Args&&... args) const
		{
			const int id = ComponentRegistry::GetComponentId(stName);
			if(id != -1)
			{
				void (*pPtr)(Entity&, Args...) = (void (*)(Entity&, Args...))m_mMethods.at(id);
				if(pPtr)
					pPtr(entity, args...);
			}
			else
			{
				sys::log->Out(sys::LogModule::ENTITY, sys::LogType::WARNING, "Cannot attach component", stName, "(component unknown)");
			}
		}

		template<typename Type, typename... Args>
		void ComponentFactory::AttachComponentTemplate(Entity& entity, Args... args)
		{
			entity.AttachComponent<Type>(args...);
		}

	}
}