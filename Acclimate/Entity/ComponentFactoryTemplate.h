#pragma once
#include "Entity.h"

namespace acl
{
	namespace ecs
	{

		template<typename Type, typename... Args>
		void AttachComponent(Entity& entity, Args... args)
		{
			entity.AttachComponent<Type>(args...);
		}

	}
}