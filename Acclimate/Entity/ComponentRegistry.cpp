#include "ComponentRegistry.h"

namespace acl
{
	namespace ecs
	{

		int ComponentRegistry::GetComponentId(const std::wstring& stName)
		{
			if(m_mIds.count(stName))
				return m_mIds[stName];
			else
				return -1;
		}

		void ComponentRegistry::UnregisterComponent(const std::wstring& stName)
		{
			auto itr = m_mIds.find(stName);
			if(itr != m_mIds.end())
				m_mIds.erase(itr);
		}

		const std::wstring& ComponentRegistry::GetComponentName(size_t id)
		{
			for(auto& ids : m_mIds)
			{
				if(ids.second == id)
					return ids.first;
			}

			static const std::wstring stTemp = L"";
			return stTemp;
		}

		const ComponentRegistry::ComponentIdMap& ComponentRegistry::GetIdMap(void)
		{
			return GetMap();
		}

		ComponentRegistry::ComponentIdMap& ComponentRegistry::GetMap(void)
		{
			return m_mIds;
		}

		ComponentRegistry::ComponentIdMap ComponentRegistry::m_mIds(0);

	}
}
