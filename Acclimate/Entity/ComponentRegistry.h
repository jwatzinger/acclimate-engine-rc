#pragma once
#include <unordered_map>
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class ACCLIMATE_API ComponentRegistry
		{
		public:
			typedef std::unordered_map<std::wstring, int> ComponentIdMap;

			template<typename Component>
			static void RegisterComponent(const std::wstring& stName);
			static void UnregisterComponent(const std::wstring& stName);

			static int GetComponentId(const std::wstring& stName);
			static const std::wstring& GetComponentName(size_t id);
			static const ComponentIdMap& GetIdMap(void);

		private:
#pragma warning( disable: 4251 )
			static ComponentIdMap m_mIds;
			static ComponentIdMap& GetMap(void);
#pragma warning( default: 4251 )
		};
		
		template<typename Component>
		void ComponentRegistry::RegisterComponent(const std::wstring& stName)
		{
			GetMap()[stName] = Component::family();
		}
	}
}

