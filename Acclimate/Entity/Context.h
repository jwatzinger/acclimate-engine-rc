#pragma once

namespace acl
{
    namespace ecs
    {

        class EntityManager;
        class SystemManager;
        class MessageManager;
		class MessageFactory;
		class ComponentFactory;
		class QueryFactory;
		class Prefabs;

        /// Packs a set of ECS managers
		/** The manager context implements the context pattern for the  
		*   entity/component-system by storing one of each manager. */
        struct Context
        {
            Context(EntityManager& entities, SystemManager& systems, MessageManager& messages, MessageFactory& messageFactory, ComponentFactory& componentFactory, QueryFactory& queryFactory, Prefabs& prefabs): 
				entities(entities), systems(systems), messages(messages), messageFactory(messageFactory), componentFactory(componentFactory), queryFactory(queryFactory), prefabs(prefabs) {}

        	EntityManager& entities;
			SystemManager& systems;
			MessageManager& messages;
			MessageFactory& messageFactory;
			ComponentFactory& componentFactory;
			QueryFactory& queryFactory;
			Prefabs& prefabs;
        };

    }
}