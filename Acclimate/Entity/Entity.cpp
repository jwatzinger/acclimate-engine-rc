#include "Entity.h"
#include "Component.h"

namespace acl
{
	namespace ecs
	{

		Entity::Entity(const std::wstring& stName, const std::wstring& stPrefab) : m_mask(0), m_vComponents(32),
			m_stName(stName), m_stPrefab(stPrefab)
		{
		}

		Entity::Entity(Entity&& entity) : m_mask(std::move(entity.m_mask)), m_vComponents(std::move(entity.m_vComponents)),
			m_stName(std::move(entity.m_stName)), m_stPrefab(std::move(entity.m_stPrefab))
		{
		}

		Entity::Entity(const Entity& entity) : m_mask(entity.m_mask), m_stName(entity.m_stName), m_stPrefab(entity.m_stPrefab)
		{
			m_vComponents.reserve(entity.m_vComponents.size());
			for(auto pComponent : entity.m_vComponents)
			{
				if(pComponent)
				{
					auto& component = pComponent->Clone();
					component.SetParent(*this);
					m_vComponents.push_back(&component);
				}
				else
					m_vComponents.push_back(nullptr);
			}
		}

        Entity::~Entity(void)
        {
			for(auto pComponent : m_vComponents)
			{
				delete pComponent;
			}
        }

		void Entity::SetName(const std::wstring& stName)
		{
			m_stName = stName;
		}

		BaseComponent& Entity::AttachComponent(const BaseComponent& component)
		{
			const auto family = component.GetFamily();
			ACL_ASSERT(m_vComponents.size() > family);

			if(auto pComponent = m_vComponents[family])
			{
				if(pComponent == &component)
					return *pComponent;
				else
					delete pComponent;
			}
			else
			{
				ACL_ASSERT(m_mask.at(family) == false);
				m_mask.set(family, true);
			}

			auto& newComponent = component.Clone();
			m_vComponents[family] = &newComponent;

			return newComponent;
		}

		void Entity::DetachComponent(const BaseComponent& component)
		{
			const auto family = component.GetFamily();
			ACL_ASSERT(m_vComponents.size() > family);
			
			auto pComponent = m_vComponents[family];

			// does the entity has this component?
			if(pComponent == &component)
			{
				ACL_ASSERT(m_mask.at(family));

				// unset family in mask
				m_mask.set(family, false);

				// remove it
				delete pComponent;
				m_vComponents[family] = nullptr;
			}
		}

		const std::bitset<32>& Entity::GetMask(void) const
		{
			return m_mask;
		}

		const std::wstring& Entity::GetName(void) const
		{
			return m_stName;
		}

		const std::wstring& Entity::GetPrefab(void) const
		{
			return m_stPrefab;
		}
 
		BaseComponent* Entity::GetComponent(size_t id) const
		{
			return m_vComponents[id];
		}

		Entity& Entity::operator=(const Entity& entity)
		{
			m_mask = entity.m_mask;
			m_stName = entity.m_stName;
			m_stPrefab = entity.m_stPrefab;

			for(auto pComponent : m_vComponents)
			{
				delete pComponent;
			}
			m_vComponents.clear();

			for(auto pComponent : entity.m_vComponents)
			{
				if(pComponent)
				{
					auto& component = pComponent->Clone();
					component.SetParent(*this);
					m_vComponents.push_back(&component);
				}
				else
					m_vComponents.push_back(nullptr);
			}

			return *this;
		}

	}
}