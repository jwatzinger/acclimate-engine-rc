/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <vector>
#include <bitset>
#include "..\Core\Handle.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

        struct BaseComponent;

		/// Represents an entity in the entity/component-system
		/** The entity class is responsible to store a set of 
		*   components, forming a game object. It has methods
		*   for factory-creating as well as accessing components.
		*   It holds a bitmask, describing which components
		*   are attached to it. */
		class ACCLIMATE_API Entity final
		{
			typedef std::vector<BaseComponent*> ComponentVector;
		public:

			Entity(const std::wstring& stName, const std::wstring& stPrefab);
			Entity(Entity&& entity);
			Entity(const Entity& entity);
			~Entity(void);

			/** Creates and attaches a component to the entity. This method
			 *	takes a variable number of arguments, and uses them to create
			 *	a Component of the templated type. The component is stored in 
             *  the entity.
             *
             *  @param[in] args The constructor arguments for the component being created.
             *
             *  @tparam Comp Type of the component to attach.
             *
             *  @return Pointer to the created component.
			 */
			template<class Comp, typename... Args>
			Comp* AttachComponent(Args&& ... args);

			/** Attaches a copy of the passed component to the entity. Does nothing
			*	in case the same component is already attached.
			*
			*  @param[in] component The component to clone and attach.
			*
			*  @return Pointer to the attached component.
			*/
			BaseComponent& AttachComponent(const BaseComponent& component);

			/**
			*  @param Name
			*/
			void SetName(const std::wstring& stName);

			/** Detaches a component from the entity. This method removes the
			 *	specified component type from the entitiy.
             *
             *  @tparam Comp Type of component to remove
			 */
			template<class Comp>
			void DetachComponent(void); 

			/** Detaches a component from the entity. This method removes the
			*	passed component from the entity.
			*
			*  @param component Component to remove
			*/
			void DetachComponent(const BaseComponent& component);

			/** Used to access a Component of templated type from the entity.
			 *	This method checks the entities bitmask to see if it has a
			 *	component of the specified type "Component", and returns \c nullptr 
             *  if it hasn't.
             *
             *  @tparam Comp Type of the component to attach.
             *
             *  @return Pointer to the component.
			 */
			template<class Comp>
			Comp* GetComponent(void) const;

			/** Used to access a Component of a certain type id.
			*	This method checks the entities bitmask to see if it has a
			*	component of the specified id, and returns \c nullptr
			*  if it hasn't.
			*
			*  @param id Type-id of the component.
			*
			*  @return Pointer to the component.
			*/
			BaseComponent* GetComponent(size_t id) const;

			/** 
			*  @return Name
			*/
			const std::wstring& GetName(void) const;

			/**
			*  Returns the prefab that was used to initialize this entity.
			*  @return Prefab, empty string if no prefab was used.
			*/
			const std::wstring& GetPrefab(void) const;

			/** Used to access the bitmask of the entity.
			 *	This method is an read-only accessor for the entities bitmask.
             *
             *  @return Const reference to the entities bitmask.
			 */
			const std::bitset<32>& GetMask(void) const;

			Entity& operator=(const Entity& entity);

		private:
#pragma warning( disable: 4251 )
			ComponentVector m_vComponents; ///< The component storage vector of the entity

			std::bitset<32> m_mask; /**< The entities component bitmask.
									*  Each bit represents a component type. If the bit is set,
									*	the entity posesses a component of this type. */
			std::wstring m_stName,		///< The entities name
						 m_stPrefab;	///< The entities prefab
#pragma warning( default: 4251 )
		};

		template<class Comp, typename ... Args>
		Comp* Entity::AttachComponent(Args&& ... args)
		{
			// get component family by type
			const auto family = Comp::family();
			// create new component with args
			Comp* pComponent = new Comp(args...);
			pComponent->SetParent(*this);
			// store info that component of this type is attached to this entity
			m_mask.set(family);

			// delete old component to avoid memory leak
			delete m_vComponents[family];

			// set new component
			m_vComponents[family] = pComponent;

			// return created component for being able to work on with it
			return pComponent;
		}

		template<class Comp>
		void Entity::DetachComponent(void)
		{
			// get component family by type
			const auto family = Comp::family();

			// does the entity has this component?
			if(m_mask.at(family))
			{
				// unset family in mask
				m_mask.set(family, false);

				// remove it
				delete m_vComponents[family];
				m_vComponents[family] = nullptr;
			}
		}

		template<class Comp>
		Comp* Entity::GetComponent(void) const
		{
			// return pointer to attached component with type Component
			return static_cast<Comp*>(m_vComponents[Comp::family()]);
		}

		typedef core::Handle<Entity> EntityHandle;

	}
}