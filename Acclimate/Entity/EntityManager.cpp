#include "EntityManager.h"
#include "Entity.h"
#include "Prefabs.h"

namespace acl
{
	namespace ecs
	{

		EntityManager::EntityManager(Prefabs& prefabs) : m_pPrefabs(&prefabs), m_pActiveState(&m_state)
		{
			prefabs.SetEntities(this);
		}

		EntityManager::~EntityManager(void)
		{
			ACL_ASSERT(m_pActiveState == &m_state);
			m_pPrefabs->SetEntities(nullptr);

			// call clear to delete all entities
			Clear();
		}

		const Prefabs& EntityManager::GetPrefabs(void) const
		{
			return *m_pPrefabs;
		}

		EntityHandle& EntityManager::CreateEntity(const std::wstring& stName)
		{
			ACL_ASSERT(m_pActiveState);

			m_pActiveState->m_vEntityOwners.push_back(new EntityState::EntityOwner(stName, L""));

			m_pActiveState->m_vEntities.emplace_back(m_pActiveState->m_vEntityOwners[m_pActiveState->m_vEntityOwners.size() - 1]->CreateHandle());

			auto& entity = m_pActiveState->m_vEntities.back();
			auto pHandle = new EntityHandle(entity);
			m_pActiveState->m_vSafeHandles.push_back(pHandle);

			return *pHandle;
		}

		EntityHandle& EntityManager::CreateEntityFromPrefab(const std::wstring& stName, const std::wstring& stPrefab)
		{
			m_pActiveState->m_vEntityOwners.push_back(new EntityState::EntityOwner(stName, L""));

			m_pActiveState->m_vEntities.emplace_back(m_pActiveState->m_vEntityOwners[m_pActiveState->m_vEntityOwners.size() - 1]->CreateHandle());

			auto& entity = m_pActiveState->m_vEntities.back();
			auto pHandle = new EntityHandle(entity);
			m_pActiveState->m_vSafeHandles.push_back(pHandle);
			m_pPrefabs->Init(entity, stPrefab);

			return *pHandle;
		}

		EntityHandle& EntityManager::GetHandle(const ecs::Entity& entity) const
		{
			ACL_ASSERT(m_pActiveState);

			auto itr = std::find(m_pActiveState->m_vEntities.begin(), m_pActiveState->m_vEntities.end(), &entity);
			ACL_ASSERT(itr != m_pActiveState->m_vEntities.end());

			return *itr;
		}

		void EntityManager::RemoveEntity(const EntityHandle& entity)
		{
			m_vToDelete.push_back(entity);
		}

		EntityManager::EntityVector& EntityManager::AllEntities(void)
		{
			// return all entities
			return m_pActiveState->m_vEntities;
		}

		const EntityManager::EntityVector& EntityManager::AllEntities(void) const
		{
			// return all entities
			return m_pActiveState->m_vEntities;
		}

		EntityManager::ComponentMask EntityManager::componentMask(unsigned int family) const
		{
			return ComponentMask().set(family);
		}

		void EntityManager::Update(void)
		{
			for(auto& handle : m_vToDelete)
			{
				auto itr = std::find(m_pActiveState->m_vEntities.begin(), m_pActiveState->m_vEntities.end(), handle);

				// if so, remove it
				if(itr != m_pActiveState->m_vEntities.end())
				{
					size_t dist = itr - m_pActiveState->m_vEntities.begin();
					m_pActiveState->m_vEntities.erase(itr);

					auto ownerItr = m_pActiveState->m_vEntityOwners.begin() + dist;
					delete *ownerItr;
					m_pActiveState->m_vEntityOwners.erase(ownerItr);

					auto safeItr = m_pActiveState->m_vSafeHandles.begin() + dist;
					delete *safeItr;
					m_pActiveState->m_vSafeHandles.erase(safeItr);
				}
			}

			m_vToDelete.clear();
		}

		void EntityManager::Clear(void)
		{
			// clear entity list
			m_state.Clear();
		}


		EntityState& EntityManager::ChangeState(void)
		{
			ACL_ASSERT(m_pActiveState == &m_state);

			m_pActiveState = new EntityState(m_state);
			return *m_pActiveState;
		}

		void EntityManager::RevertState(EntityState& state)
		{
			ACL_ASSERT(m_pActiveState != &m_state);
			ACL_ASSERT(m_pActiveState == &state);

			delete &state;
			m_pActiveState = &m_state;
		}
		
	}
}