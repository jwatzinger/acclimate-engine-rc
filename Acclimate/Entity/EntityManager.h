/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <bitset>
#include "EntityState.h"
#include "..\Core\Dll.h"
#include "..\System\Bit.h"

namespace acl
{
	namespace ecs
	{

		class Prefabs;
		/// Handles the creation and storage of entites 
		/** The EntityManager is responsible for creating and storing 
		*   Entity objects. It grants direct access as well as selective 
		*	based on component possession. */

		class ACCLIMATE_API EntityManager
		{
			static const int MAX_COMPONENTS = 32;

			typedef std::bitset<MAX_COMPONENTS> ComponentMask;
		public:

			EntityManager(Prefabs& prefabs);
			~EntityManager(void);

			typedef std::vector<EntityHandle> EntityVector;

			const Prefabs& GetPrefabs(void) const;

			/**	Creates an entity. 
			 *  This method is used to create and store a new Entity.
			 *	
             *  @return A reference to the created entity.
             */
			EntityHandle& CreateEntity(const std::wstring& stName);

			EntityHandle& CreateEntityFromPrefab(const std::wstring& stName, const std::wstring& stPrefab);

			EntityHandle& GetHandle(const ecs::Entity& entity) const;

			/** Removes an entity.
			 *	This method removes an Entity from storage
			 *  and deletes it afterwards. The pointer to the passed
			 *  entity is therefore invalid afterwards. 
             *
             *  @param[in] entity Reference to the entity that should be removed.
             */
			void RemoveEntity(const EntityHandle& entity);

			/** Fills a vector with all matching entities.
			 *	This method can recieve a variable number of template
			 *  arguments. Each argument must be a valid Component class.
			 *  All entities are then checked, and only those that posses
			 *	the specified components are filled in the output vector.
             *
             *  @tparam C Type of at least one component the entity has to possess.
             *  @tparam Components Types of a variable number of additional components.
			 *
			 *	@return Container of all found entities.
             */
			template<typename C, typename... Components>
			EntityVector EntitiesWithComponents(void) const;

			template<typename... Families>
			EntityVector EntitiesWithComponents(unsigned int family, Families&&... families) const;

			/** Returns the entity storage.
			 *	This method grants access to the entire entity storage. 
             *
             *  @return Reference to the entity storage vector.
             */
			EntityVector& AllEntities(void);

			/** Returns a const reference to the entity storage.
			 *	This method grants read-only access to the entity storage.
             *
             *  @return Const reference to the entity storage vector.
             */
			const EntityVector& AllEntities(void) const;

			void Update(void);

			/** Clears the entity storage.
			 *	This method deletes all stored entities and clears the storage. */
			void Clear(void);

			EntityState& ChangeState(void);
			void RevertState(EntityState& state);

		private:
#pragma warning( disable: 4251 )
			/** Recursively creates a component mask.
			 *	This method is used to create a component bit mask from the
			 *  classtypes passed in to the EntitiesWithComponents-method.
             *
             *  @tparam C Type of the first Component that will be used for the bitmask
             *  @tparam C2 Type of the second Component that will be used for the bitmask
             *  @tparam Components Types of a variable number of additional components.
             *
             *  @return Bitmask with the bits of the specified component types set.
             */
			template<typename C, typename C2, typename... Components>
			ComponentMask componentMask(void) const;
			
			template<typename... Families>
			ComponentMask componentMask(unsigned int family, unsigned int family2, Families&&... families) const;

			/** Creates a bitmask for the templated component type.
			 *	This method creates a bitmask using the templated Component
			 *  class type.
             *
             *  @tparam C Type of the component whose id will be set in the bitmask.
             *
             *  @return Bitmask with the bit of the specified component set.
             */
			template<typename C>
			ComponentMask componentMask(void) const;

			ComponentMask componentMask(unsigned int family) const;
			
			EntityState m_state;
			EntityState* m_pActiveState;
			EntityVector m_vToDelete;

			Prefabs* m_pPrefabs;
#pragma warning( default: 4251 )
		};

		template<typename C, typename... Components>
		EntityManager::EntityVector EntityManager::EntitiesWithComponents(void) const
		{
			EntityVector vOut;

			//create bitmask to determine which entities fit component types
			ComponentMask mask = componentMask<C, Components...>();
			//iterate through all components // todo: performance optimize
			for(auto& entity : m_pActiveState->m_vEntities)
			{
				//add entity to list if component mask fits
				if(bit::contains(entity->GetMask(), mask))
					vOut.emplace_back(entity);
			}

			return vOut;
		}

		template<typename... Families>
		EntityManager::EntityVector EntityManager::EntitiesWithComponents(unsigned int family, Families&&... families) const
		{
			EntityVector vOut;

			//create bitmask to determine which entities fit component types
			ComponentMask mask = componentMask(family, families...);
			//iterate through all components // todo: performance optimize
			for(auto& entity : m_pActiveState->m_vEntities)
			{
				//add entity to list if component mask fits
				if(bit::contains(entity->GetMask(), mask))
					vOut.emplace_back(entity);
			}

			return vOut;
		}

		template<typename C, typename C2, typename... Components>
		EntityManager::ComponentMask EntityManager::componentMask(void) const
        {
			//recurisvely create the component mask
			return componentMask<C>() | componentMask<C2, Components...>();
        }

		template<typename... Families>
		EntityManager::ComponentMask EntityManager::componentMask(unsigned int family, unsigned int family2, Families&&... families) const
		{
			//recurisvely create the component mask
			return componentMask(family) | componentMask(family2, families...);
		}

		template<typename C>
		EntityManager::ComponentMask EntityManager::componentMask(void) const
        {
			//create component mask with component type C bit set
			return ComponentMask().set(C::family());
        }

	}
}