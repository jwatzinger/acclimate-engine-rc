#include "EntityState.h"

namespace acl
{
	namespace ecs
	{

		EntityState::EntityState(void)
		{
		}

		EntityState::EntityState(const EntityState& state)
		{
			const auto size = state.m_vEntityOwners.size();
			m_vEntityOwners.reserve(size);
			m_vEntities.reserve(size);
			m_vSafeHandles.reserve(size);

			for(auto pOwner : state.m_vEntityOwners)
			{
				auto& newOwner = pOwner->Clone();

				auto handle = newOwner.CreateHandle();
				auto pNewHandle = new EntityHandle(handle);

				m_vEntityOwners.push_back(&newOwner);
				m_vEntities.emplace_back(*pNewHandle);
				m_vSafeHandles.emplace_back(pNewHandle);
			}
		}

		EntityState::~EntityState(void)
		{
			for(auto pOwner : m_vEntityOwners)
			{
				delete pOwner;
			}

			for(auto pHandle : m_vSafeHandles)
			{
				delete pHandle;
			}
		}

		void EntityState::Clear(void)
		{
			for(auto pHandle : m_vSafeHandles)
			{
				delete pHandle;
			}
			m_vSafeHandles.clear();

			m_vEntities.clear();

			for(auto pOwner : m_vEntityOwners)
			{
				delete pOwner;
			}
			m_vEntityOwners.clear();
		}

	}
}

