#pragma once
#include <vector>
#include "Entity.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class EntityManager;

		class ACCLIMATE_API EntityState
		{
			typedef core::Owner<Entity> EntityOwner;
			typedef std::vector<EntityOwner*> EntityOwnerVector;
			typedef std::vector<EntityHandle*> SafeHandleVector;
			typedef std::vector<EntityHandle> EntityVector;
		public:
			EntityState(void);
			~EntityState(void);

			void Clear(void);

		private:
#pragma warning( disable: 4251 )
			friend class EntityManager;

			EntityState(const EntityState& state);

			EntityOwnerVector m_vEntityOwners; ///< The entity storage vector of the manager
			EntityVector m_vEntities;
			SafeHandleVector m_vSafeHandles;
#pragma warning( default: 4251 )
		};

	}
}


