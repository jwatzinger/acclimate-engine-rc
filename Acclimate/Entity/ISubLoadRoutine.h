/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once

namespace acl
{
	namespace xml
	{
		class Node;
	}

	namespace ecs
	{
		
		class Entity;

		class ISubLoadRoutine
		{
		public:
			virtual ~ISubLoadRoutine(void) = 0 {};

			virtual void Execute(Entity& entity, const xml::Node& entityNode) const = 0;
		};
	}
}