/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once

namespace acl
{
	namespace xml
	{
		class Node;
	}

	namespace ecs
	{
		
		class Entity;

		class ISubSaveRoutine
		{
		public:
			virtual ~ISubSaveRoutine(void) = 0 {};

			virtual void Execute(const Entity& entity, xml::Node& entityNode) const = 0;
		};
	}
}