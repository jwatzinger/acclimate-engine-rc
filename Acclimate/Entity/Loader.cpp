#include "Loader.h"
#include "Entity.h"
#include "EntityManager.h"
#include "ComponentRegistry.h"
#include "..\XML\Exception.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace ecs
	{

		Loader::Loader(EntityManager& entities) : m_pEntities(&entities)
		{
		}

		void Loader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;

			try 
			{
				doc.LoadFile(stFilename.c_str());
			}
			catch(xml::Exception&)
			{
				throw fileException();
			}

			if(const xml::Node* world = doc.Root(L"World"))
			{
				if( const xml::Node::NodeVector* pEntities = world->Nodes(L"Entity") )
				{
					for(auto pEntityNode : *pEntities)
					{
						std::wstring stName;
						if(auto pName = pEntityNode->Attribute(L"name"))
							stName = *pName;

						EntityHandle* pEntity;
						if(auto pPrefab = pEntityNode->Attribute(L"prefab"))
						{
							pEntity = &m_pEntities->CreateEntityFromPrefab(stName, *pPrefab);

							// handle erasure of prefab components
							if(auto pEraseVector = pEntityNode->Nodes(L"Erase"))
							{
								for(auto pErase : *pEraseVector)
								{
									const int id = ComponentRegistry::GetComponentId(pErase->GetValue());
									if(id != -1)
									{
										auto pComponent = (*pEntity)->GetComponent(id);
										if(pComponent)
											(*pEntity)->DetachComponent(*pComponent);
									}
								}
							}	
						}
						else
							pEntity = &m_pEntities->CreateEntity(stName);

						LoadFromNode(*pEntity, *pEntityNode);
					}
				}
			}
		}

		void Loader::LoadFromNode(EntityHandle& entity, const xml::Node& node) const
		{
			for(auto& routine : m_mSubRoutines)
			{
				routine.second->Execute(entity, node);
			}
		}

		void Loader::RegisterSubRoutine(const std::wstring& stName, const ISubLoadRoutine& routine)
		{
			m_mSubRoutines[stName] = &routine;
		}

		void Loader::UnregisterSubRoutine(const std::wstring& stName)
		{
			if(m_mSubRoutines.count(stName))
			{
				delete m_mSubRoutines.at(stName);
				m_mSubRoutines.erase(stName);
			}
		}

		Loader::SubRoutineMap Loader::m_mSubRoutines = Loader::SubRoutineMap();

	}
}