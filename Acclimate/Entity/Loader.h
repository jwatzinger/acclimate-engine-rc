/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>
#include <map>
#include "Entity.h"
#include "ISubLoadRoutine.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace ecs
    {

        class EntityManager;

        /// Loads entities from a serialized XML-file
		/** This class is used to read in entities and components to a certain
		*   entity manager from a file in XML. */
		class ACCLIMATE_API Loader
		{
			typedef std::map<std::wstring, const ISubLoadRoutine*> SubRoutineMap;
		public:
			Loader(EntityManager& entities);

            /** Loads the entities from a file.
             *  This method loads a set of entities from a specific XML-file.
             *  Any previously stored entities are being cleared from the manager
             *  passed before, and the read entities are put into that same manager.
             *  
             *  @param[in] stFilename The path and name of the file that should be read from.
             */
			void Load(const std::wstring& stFilename) const;

			void LoadFromNode(EntityHandle& entity, const xml::Node& node) const;

			/** Registeres a load routine.
             *  This method is used to statically register a sub-load routine to the axm
			 *	component loader. A new instance will be created as specified. No checking for 
			 *	double registration (yet).
             *  
             *  @tparam Routine Classname of the routine to register.
             */
			template<typename Routine>
			static void RegisterSubRoutine(const std::wstring& stName);

			static void RegisterSubRoutine(const std::wstring& stName, const ISubLoadRoutine& routine);

			static void UnregisterSubRoutine(const std::wstring& stName);

		private:
#pragma warning( disable: 4251 )
			EntityManager* m_pEntities; ///< Pointer to the entity manager the entities should be stored to.

			static SubRoutineMap m_mSubRoutines; ///< Map of previously registered load routines.
#pragma warning( default: 4251 )
		};

		template<typename Routine>
		void Loader::RegisterSubRoutine(const std::wstring& stName)
		{
			UnregisterSubRoutine(stName);
			RegisterSubRoutine(stName, *new Routine);
		}

    }
}
