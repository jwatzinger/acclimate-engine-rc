#include "Message.h"

namespace acl
{
	namespace ecs
	{

		BaseMessage::Family BaseMessage::family_count = 0;

		void BaseMessage::ResetCount(void)
		{
			family_count = 0;
		}

		BaseMessage::Family BaseMessage::GenerateFamilyId(void)
		{
			return family_count++;
		}

		const BaseMessage* BaseMessage::Check(Family family) const
		{
			if(m_family == family)
                return this;
            else 
                return nullptr;
		}
	
	}
}