/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
        /*******************************
        * BaseMessage
        *******************************/

        /// Used for implementing the CRTP for messages
		/** This class is used as a base from which the actual message derives.
		*   It offers an implementation of the Curiously recurring template pattern,
		*   by storing a static message counter. Each different message struct
        *   will increase this counter and aquire its unique id from it. It also offers
        *   a method to quickly convert a derived message to its underlying type.*/
		struct ACCLIMATE_API BaseMessage
		{
		protected:
			typedef unsigned int Family;
		public:

            /** Converts message to an underlying type.
             *  This method trys to convert the message to the
             *  templated type, using the internal type id. If the
             *  types doesn't fit, a \c nullptr is returned.
             *  
             *  @tparam M The target messages type.
             *
             *  @return Pointer to the message, now with its converted type.
             */
            template<typename M>
            const M* Convert(void) const;

            const BaseMessage* Check(Family family) const;

			template<typename Type>
			const Type* GetParam(const std::wstring& stName) const;

			static void ResetCount(void); // todo: make safer, now it could be called everywhere and screw up the ids!

		protected:
			Family m_family; ///< The messages current family id

			static Family GenerateFamilyId(void);

			virtual const void* GetParam(const std::wstring& stName, const std::type_info** type) const { return nullptr; }

		private:
			static Family family_count; /**< Counter of instanciated messages.
                *   Each message of a different type that is created, or whose 
                *   family function is called the first time will increase this 
                *   counter. The counter is used as the ID of the next message. */
		};

        template<typename M>
        const M* BaseMessage::Convert(void) const
        {
            if(m_family == M::family())
                return static_cast<const M*>(this);
            else 
                return nullptr;
        }

		template<typename Type>
		const Type* BaseMessage::GetParam(const std::wstring& stName) const
		{
			const std::type_info* pInType = nullptr;
			if(auto pAttribute = GetParam(stName, &pInType))
			{
#ifdef _DEBUG
				if(*pInType == typeid(Type))
#endif
					return (Type*)pAttribute;
			}

			return nullptr;
		}

        /*******************************
        * Message
        *******************************/

        /// The message parent struct
		/** Each message must derive from this struct. It automatically sets its
        *   unique family id on creation, which can then be accessed via a static
        *   method, so you can compare the type of a message object with the type
        *   of a message struct. */
		template <typename Derived>
		struct Message : 
            public BaseMessage
		{
		public: 

			Message(void) 
            {
                m_family = family(); 
            };

            /** Returns the "family" type of the message.
			 *	This method sets the type id for the specific message struct on
             *  the first call, and returns it from there on. The id is consistent 
             *  between the running time of a program and cannot be altered, but
             *  it may alter between different runs, depending on the first creation 
             *  order of the messages. 
             *
             *  @return The unique id of this message instance
             */
			static Family family(void)
            {
			    static const BaseMessage::Family Family = BaseMessage::GenerateFamilyId();
			    return Family;
		    }
		};

	}
}
