#pragma once
#include <string>
#include <unordered_map>
#include "MessageRegistry.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class MessageManager;

		class ACCLIMATE_API MessageFactory
		{
			typedef void (*FunctionPtr)(void);
			typedef std::unordered_map<size_t, FunctionPtr> FactoryMethodMap;
		public:

			MessageFactory(const MessageManager& manager);

			template<typename Message, typename... Args>
			void Register(void);
			template<typename Message>
			void Unregister(void);

			template<typename ... Args>
			void DeliverMessage(size_t id, Args&&... args) const;
			template<typename ... Args>
			void DeliverMessage(const std::wstring& stName, Args&&... args) const;

		private:
#pragma warning( disable: 4251 )

			template<typename Type, typename ... Args>
			static void DeliverMessageTemplate(const MessageManager& manager, Args... args);

			FactoryMethodMap m_mMethods;

			const MessageManager* m_pManager;
#pragma warning( default: 4251 )
		};

		template<typename Message, typename... Args>
		void MessageFactory::Register(void)
		{
			m_mMethods[Message::family()] = (ecs::MessageFactory::FunctionPtr)&MessageFactory::DeliverMessageTemplate<Message, Args...>;
		}

		template<typename Message>
		void MessageFactory::Unregister(void)
		{
			auto itr = m_mMethods.find(Message::family());
			if(itr != m_mMethods.end())
				m_mMethods.erase(itr);
		}

		template<typename ... Args>
		void MessageFactory::DeliverMessage(size_t id, Args&&... args) const
		{
			void (*pPtr)(const MessageManager&, Args...) = (void (*)(const MessageManager&, Args...))m_mMethods.at(id);
			pPtr(*m_pManager, args...);
		}

		template<typename ... Args>
		void MessageFactory::DeliverMessage(const std::wstring& stName, Args&&... args) const
		{
			const int id = MessageRegistry::GetMessageId(stName);
			if(id != -1)
			{
				void (*pPtr)(const MessageManager&, Args...) = (void (*)(const MessageManager&, Args...))m_mMethods.at(id);
				if(pPtr)
					pPtr(*m_pManager, args...);
			}
		}

		template<typename Type, typename ... Args>
		void MessageFactory::DeliverMessageTemplate(const MessageManager& manager, Args... args)
		{
			manager.DeliverMessage<Type>(args...);
		}

	}
}