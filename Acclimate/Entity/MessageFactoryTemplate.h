#pragma once
#include "MessageManager.h"

namespace acl
{
	namespace ecs
	{
		
		template<typename Type, typename ... Args>
		void DeliverMessage(const MessageManager& manager, Args... args)
		{
			manager.DeliverMessage<Type>(args...);
		}

	}
}