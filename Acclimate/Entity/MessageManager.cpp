#include "MessageManager.h"

namespace acl
{
	namespace ecs
	{

		MessageManager::MessageManager(void): m_vSubscript(32), m_vQueries(32)
		{
        }

		void MessageManager::Subscribe(BaseSystem::Family family, BaseSystem& system)
		{
			//resize subscript vector if no entry for this family exists
			if(m_vSubscript.size() <= family)
				m_vSubscript.resize(family+1);

			//add system to family subscript list to inform later
			auto& vSubscript = m_vSubscript[family];
			auto itr = std::find(vSubscript.begin(), vSubscript.end(), &system);
			if(itr == vSubscript.end())
				vSubscript.push_back(&system);
		}

		void MessageManager::Unsubscribe(BaseSystem::Family family, BaseSystem& system)
		{
			if(m_vSubscript.size() > family)
			{
				auto& vSubscript = m_vSubscript[family];
				auto itr = std::find(vSubscript.begin(), vSubscript.end(), &system);
				if(itr != vSubscript.end())
					vSubscript.erase(itr);
			}
		}

		void MessageManager::UnsubscribeAll(BaseSystem& system)
		{
			for(auto& vSubscript : m_vSubscript)
			{
				auto itr = std::find(vSubscript.begin(), vSubscript.end(), &system);
				if(itr != vSubscript.end())
					vSubscript.erase(itr);
			}
		}

		void MessageManager::UnregisterAll(const BaseSystem& system)
		{
			for(unsigned int i = 0; i < m_vQueries.size(); i++)
			{
				auto& query = m_vQueries[i];
				if(query == &system)
					query = nullptr;
			}
		}

        void MessageManager::Clear(void)
        {
            m_vSubscript.clear();
            m_vSubscript.resize(32);
            m_vQueries.clear();
            m_vQueries.resize(32);
        }

	}
}