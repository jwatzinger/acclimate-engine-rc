/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <vector>
#include "System.h"   
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
        /// Handles registration to and dispatching of messages and queries.
		/** The MessageManager is responsible for allowing systems to
        *   subscribe to messages and queries, as well as recieve any 
        *   such unit and dispatch it to the previously subscribed systems. */
		class ACCLIMATE_API MessageManager
		{
            typedef std::vector<const BaseSystem*> QueryVector; ///< Typedef for vector of query registrations
			typedef std::vector<std::vector<BaseSystem*>> SubscriptVector; ///< Typedef for vector of message subscripts

		public:
			MessageManager(void);
            
            /** Subscribe system to message.
             *  This method allows systems to subscribe themselfs to the templated message 
             *  type. This is necessary for them to recieve any message via the manager.
             *  
             *  @param[in] system Reference to the system that wants to subscribe to the message.
             *
             *  @tparam M The type of the message that should be subscribed to.
             */
			template<typename M>
			void Subscribe(BaseSystem& system);
			void Subscribe(BaseSystem::Family family, BaseSystem& system);
			template<typename M>
			void Unsubscribe(BaseSystem& system);
			void Unsubscribe(BaseSystem::Family family, BaseSystem& system);
			void UnsubscribeAll(BaseSystem& system);

            /** Register system to query.
             *  This method allows systems to register themselves to the templated query 
             *  type. This is necessary for them  to recieve any query via the manager. 
             *  Only one system can be registered to a query at a time. Latter registrations 
             *  will override earlier ones.
             *  
             *  @param[in] system Reference to the system that wants to register to the query.
             *
             *  @tparam QueryType The type of the query that should be registered to.
             */
            template<typename QueryType>
            void RegisterQuery(const BaseSystem& system);
			template<typename QueryType>
			void UnregisterQuery(const BaseSystem& system);
			void UnregisterAll(const BaseSystem& system);

            /** Delivers a message to all subscribed systems.
             *  This method constructs a message and delivers it to all previously subscribed 
             *  systems.
             *  
             *  @param[in] args Constructor arguments for the message to being created.
             *
             *  @tparam M The type of message that should be delivered.
             */
			template<typename M, typename ... Args>
			void DeliverMessage(Args&& ... args) const;

            /** Performs a query.
             *  This method looks whether a system is registered to the query
             *  passed, and if so, it passes the query on for the system to handle.
             *  Returns false if no system is registered to the query. Otherwise,
             *  it returns the systems response to the query, which can indeed also
             *  be false, if the queries condition wasn't met by the system.
             *  
             *  @param[in,out] query Reference to the query that should be delivered
             *
             *  @tparam M The type of message that should be delivered.
             *
             *  @return Indicates whether the queries condition was fullfilled or not.
             */
            template<typename Q>
            bool Query(Q& query) const;

            /** Clears all registrations.
             *  This method removes all subscriptions and registrations of any
             *  system to any message or query. */
            void Clear(void);

		private:
#pragma warning( disable: 4251 )
            QueryVector m_vQueries; ///< Vector for query registrations
			SubscriptVector m_vSubscript; /**< Vector for message subscriptions. */
#pragma warning( default: 4251 )
		};

        template<typename Q>
        void MessageManager::RegisterQuery(const BaseSystem& system)
        {
			//get message family
			BaseQuery::Family family = Q::family();

			//resize subscript vector if no entry for this family exists
			if(m_vQueries.size() <= family)
				m_vQueries.resize(family+1);

			// set system for query
			m_vQueries[family] = &system;
        }

		template<typename QueryType>
		void MessageManager::UnregisterQuery(const BaseSystem& system)
		{
			const BaseQuery::Family family = Q::family();
			if(m_vQueries.size() > family)
				m_vQueries[family] = nullptr;

		}

		template<typename M, typename ... Args>
		void MessageManager::DeliverMessage(Args&& ... args) const
		{
			//get message family
			BaseSystem::Family family = M::family();

			//return if no subscript for this message family exists
			if(m_vSubscript.size() <= family)
				return;

			const M message(args...);

			//loop through family subscripts
			for(auto pSubscripts: m_vSubscript[family])
			{
				//notify subscripts of message
				pSubscripts->ReceiveMessage(message);
			}
		}

		template<typename M>
		void MessageManager::Subscribe(BaseSystem& system)
		{
			Subscribe(M::family(), system);
		}

		template<typename M>
		void MessageManager::Unsubscribe(BaseSystem& system)
		{
			Unsubscribe(M::family(), system);
		}

        template<typename QueryType>
        bool MessageManager::Query(QueryType& query) const
        {
            const BaseQuery::Family family = QueryType::family();

            if( m_vQueries.at(family) )
                return m_vQueries[family]->HandleQuery(query);
            else
                return false;
        }

	}
}