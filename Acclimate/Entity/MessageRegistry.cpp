#include "MessageRegistry.h"

namespace acl
{
	namespace ecs
	{

		int MessageRegistry::GetMessageId(const std::wstring& stName)
		{
			MessageIdMap& map = GetMap();
			if(map.count(stName))
				return map[stName];
			else
				return -1;
		}

		void MessageRegistry::UnregisterMessage(const std::wstring& stName)
		{
			auto itr = m_mIds.find(stName);
			if(itr != m_mIds.end())
				m_mIds.erase(itr);
		}

		MessageRegistry::MessageIdMap& MessageRegistry::GetMap(void)
		{
			return m_mIds;
		}

		MessageRegistry::MessageIdMap MessageRegistry::m_mIds(0);

	}
}
