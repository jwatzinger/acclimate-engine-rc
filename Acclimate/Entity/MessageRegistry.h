#pragma once
#include <unordered_map>
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class ACCLIMATE_API MessageRegistry
		{
			typedef std::unordered_map<std::wstring, int> MessageIdMap;
		public:
			template<typename Message>
			static void RegisterMessage(const std::wstring& stName);
			static void UnregisterMessage(const std::wstring& stName);

			static int GetMessageId(const std::wstring& stName);

		private:
#pragma warning( disable: 4251 )
			static MessageIdMap m_mIds;
			static MessageIdMap& GetMap(void);
#pragma warning( default: 4251 )
		};
		
		template<typename Message>
		void MessageRegistry::RegisterMessage(const std::wstring& stName)
		{
			GetMap()[stName] = Message::family();
		}
	}
}

