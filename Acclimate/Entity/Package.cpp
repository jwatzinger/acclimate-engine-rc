#include "Package.h"

namespace acl
{
	namespace ecs
	{

		Package::Package(void) : m_entityManager(m_prefabs), m_messageFactory(m_messageManager), m_queryFactory(m_messageManager),
			m_systemManager(m_entityManager, m_messageManager, m_messageFactory, m_componentFactory, m_queryFactory),
			m_context(m_entityManager, m_systemManager, m_messageManager, m_messageFactory, m_componentFactory, m_queryFactory, m_prefabs)
		{
		}

		Package::~Package(void)
		{
			Clear();
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

		void Package::Update(void)
		{
			m_entityManager.Update();
		}

		void Package::Clear(void)
		{
			m_entityManager.Clear();
			m_systemManager.Clear();
		}

	}
}