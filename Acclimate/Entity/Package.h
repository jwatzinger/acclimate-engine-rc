#pragma once
#include "EntityManager.h"
#include "MessageManager.h"
#include "MessageFactory.h"
#include "SystemManager.h"
#include "ComponentFactory.h"
#include "QueryFactory.h"
#include "Prefabs.h"
#include "Context.h"

namespace acl
{
	namespace ecs
	{

		class Package
		{
		public:
			Package(void);
			~Package(void);

			const Context& GetContext(void) const;

			void Update(void);
			void Clear(void);

		private:

			Prefabs m_prefabs;
			EntityManager m_entityManager;
			MessageManager m_messageManager;
			MessageFactory m_messageFactory;
			SystemManager m_systemManager;
			ComponentFactory m_componentFactory;
			QueryFactory m_queryFactory;
			Context m_context;
		};

	}
}

