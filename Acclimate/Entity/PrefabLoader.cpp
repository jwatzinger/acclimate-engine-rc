#include "PrefabLoader.h"
#include "Prefabs.h"
#include "..\XML\Doc.h"
#include "..\XML\Exception.h"

namespace acl
{
	namespace ecs
	{

		PrefabLoader::PrefabLoader(Prefabs& prefabs) : m_pPrefabs(&prefabs)
		{
		}

		PrefabLoader::~PrefabLoader()
		{
		}

		void PrefabLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;

			try
			{
				doc.LoadFile(stFilename.c_str());
			}
			catch(xml::Exception&)
			{
				throw fileException();
			}

			if(const xml::Node* pWorld = doc.Root(L"Prefabs"))
			{
				if(const xml::Node::NodeVector* pEntities = pWorld->Nodes(L"Entity"))
				{
					for(auto pEntityNode : *pEntities)
					{
						m_pPrefabs->AddPrefab(*pEntityNode->Attribute(L"name"), *pEntityNode);
					}
				}
			}

		}

	}
}



