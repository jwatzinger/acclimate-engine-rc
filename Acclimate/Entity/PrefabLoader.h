#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class Prefabs;

		class ACCLIMATE_API PrefabLoader
		{
		public:
			PrefabLoader(Prefabs& prefabs);
			~PrefabLoader();

			void Load(const std::wstring& stFilename) const;

		private:

			Prefabs* m_pPrefabs;
		};

	}
}

