#include "Prefabs.h"
#include "Loader.h"
#include "..\System\Log.h"

namespace acl
{
	namespace ecs
	{

		Prefabs::Prefabs(void): m_pLoader(nullptr)
		{
		}

		void Prefabs::SetEntities(EntityManager* pEntities)
		{
			delete m_pLoader;
			if(pEntities)
				m_pLoader = new Loader(*pEntities);
			else
				m_pLoader = nullptr;
		}

		const xml::Node* Prefabs::GetPrefab(const std::wstring&  stPrefab) const
		{
			auto itr = m_mPrefabs.find(stPrefab);
			if(itr == m_mPrefabs.end())
				return nullptr;
			else
				return &itr->second;
		}
		
		void Prefabs::AddPrefab(const std::wstring& stPrefab, const xml::Node& node)
		{
			if(m_mPrefabs.count(stPrefab))
				sys::log->Out(sys::LogModule::ENTITY, sys::LogType::WARNING, "Failed to add prefab: Prefab with name", stPrefab, "already stored.");
			else
				m_mPrefabs.emplace(stPrefab, node);
		}

		void Prefabs::RemovePrefab(const std::wstring& stPrefab)
		{
			if(!m_mPrefabs.count(stPrefab))
				sys::log->Out(sys::LogModule::ENTITY, sys::LogType::WARNING, "Failed to remove prefab: No prefab with name", stPrefab, "is stored.");
			else
				m_mPrefabs.erase(stPrefab);
		}

		void Prefabs::Clear(void)
		{
			m_mPrefabs.clear();
		}

		void Prefabs::Init(EntityHandle& entity, const std::wstring& stPrefab) const
		{
			auto& itr = m_mPrefabs.find(stPrefab);
			if(itr == m_mPrefabs.end())
				sys::log->Out(sys::LogModule::ENTITY, sys::LogType::WARNING, "Failed to init entity from prefab: No prefab with name", stPrefab, "is stored.");
			else
			{
				ACL_ASSERT(m_pLoader);
				m_pLoader->LoadFromNode(entity, itr->second);
			}	
		}

	}
}

