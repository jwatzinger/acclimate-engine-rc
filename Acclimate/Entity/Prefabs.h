#pragma once
#include <unordered_map>
#include <string>
#include "Entity.h"
#include "..\XML\Node.h"

namespace acl
{
	namespace ecs
	{

		class Loader;
		class EntityManager;

		class ACCLIMATE_API Prefabs
		{
			typedef std::unordered_map<std::wstring, xml::Node> PrefabMap;

		public:
			Prefabs(void);

			void SetEntities(EntityManager* pEntities);

			const xml::Node* GetPrefab(const std::wstring&  stPrefab) const;

			void AddPrefab(const std::wstring& stPrefab, const xml::Node& node);
			void RemovePrefab(const std::wstring& stPrefab);
			void Clear(void);

			void Init(EntityHandle& entity, const std::wstring& stPrefab) const;

		private:
#pragma warning( disable: 4251 )
			const Loader* m_pLoader;

			PrefabMap m_mPrefabs;
#pragma warning( default: 4251 )
		};

	}
}


