#include "Query.h"

namespace acl
{
	namespace ecs
	{

		BaseQuery::Family BaseQuery::family_count = 0;

		void BaseQuery::ResetCount(void)
		{
			family_count = 0;
		}

		BaseQuery::Family BaseQuery::GenerateFamilyId(void)
		{
			return family_count++;
		}
	
	}
}