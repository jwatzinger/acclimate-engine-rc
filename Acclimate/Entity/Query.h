/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
        /*******************************
        * BaseQuery
        *******************************/

        /// Used for implementing the CRTP for queries
		/** This class is used as a base from which the actual query derives.
		*   It offers an implementation of the Curiously recurring template pattern,
		*   by storing a static query counter. Each different query struct
        *   will increase this counter and aquire its unique id from it. It also offers
        *   a method to quickly convert a derived query to its underlying type.*/
		struct ACCLIMATE_API BaseQuery
		{
			typedef unsigned int Family;

			Family m_family;

            /** Converts query to an underlying type.
             *  This method trys to convert the query to the
             *  templated type, using the internal type id. If the
             *  types doesn't fit, a \c nullptr is returned.
             *  
             *  @tparam Q The target query type.
             *
             *  @return Pointer to the query, now with its converted type.
             */
            template<typename Q>
            Q* Convert(void);

			template<typename Type>
			const Type* GetResult(const std::wstring& stName) const;

			static void ResetCount(void); // todo: make safer, now it could be called everywhere and screw up the ids!

		protected:
			static Family GenerateFamilyId(void);

			virtual const void* GetResult(const std::wstring& stName, const std::type_info** type) const { return nullptr; }

		private:
			static Family family_count; /**< Counter of instanciated queries.
                *   Each query of a different type that is created, or whose 
                *   family function is called the first time will increase this 
                *   counter. The counter is used as the ID of the next query. */
		};

        template<typename Q>
        Q* BaseQuery::Convert(void)
        {
            if(m_family == Q::family())
                return static_cast<Q*>(this);
            else 
                return nullptr;
        }

		template<typename Type>
		const Type* BaseQuery::GetResult(const std::wstring& stName) const
		{
			const std::type_info* pInType = nullptr;
			if(auto pAttribute = GetResult(stName, &pInType))
			{
#ifdef _DEBUG
				if(*pInType == typeid(Type))
#endif
					return (Type*)pAttribute;
			}

			return nullptr;
		}

        /*******************************
        * Query
        *******************************/

        /// The query parent struct
		/** Each query must derive from this struct. It automatically sets its
        *   unique family id on creation, which can then be accessed via a static
        *   method, so you can compare the type of a query object with the type
        *   of a query struct. */
		template <typename Derived>
		struct Query : 
            public BaseQuery
		{
		public: 

			Query(void)
            { 
                m_family = family(); 
            }

            /** Returns the "family" type of the query.
			 *	This method sets the type id for the specific query struct on
             *  the first call, and returns it from there on. The id is consistent 
             *  between the running time of a program and cannot be altered, but
             *  it may alter between different runs, depending on the first creation 
             *  order of the queries. 
             *
             *  @return The unique id of this query instance
             */
			static Family family(void) 
            {
			    static const BaseQuery::Family Family = GenerateFamilyId();
			    return Family;
		    }

		};

	}
}
