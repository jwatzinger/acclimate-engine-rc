#pragma once
#include <string>
#include <unordered_map>
#include "QueryRegistry.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
		
		struct BaseQuery;
		class MessageManager;

		class ACCLIMATE_API QueryFactory
		{
			typedef void (*FunctionPtr)(void);
			typedef std::unordered_map<size_t, FunctionPtr> FactoryMethodMap;

		public:

			QueryFactory(const MessageManager& manager);

			template<typename Query, typename ... Args>
			void Register(void);
			template<typename Query>
			void Unregister(void);

			template<typename ... Args>
			BaseQuery* PerformQuery(size_t id, Args&&... args) const;
			template<typename ... Args>
			BaseQuery* PerformQuery(const std::wstring& stName, Args&&... args) const;

		private:
#pragma warning( disable: 4251 )

			template<typename Type, typename ... Args>
			static BaseQuery* PerformQueryTemplate(const MessageManager& manager, Args... args);

			FactoryMethodMap m_mMethods;

			const MessageManager* m_pManager;
#pragma warning( default: 4251 )
		};

		template<typename Query, typename ... Args>
		void QueryFactory::Register(void)
		{
			m_mMethods[Query::family()] = (ecs::QueryFactory::FunctionPtr)&QueryFactory::PerformQueryTemplate<Query, Args...>;
		}

		template<typename Query>
		void QueryFactory::Unregister(void)
		{
			auto itr = m_mMethods.find(Query::family());
			if(itr != m_mMethods.end())
				m_mMethods.erase(itr);
		}

		template<typename ... Args>
		BaseQuery* QueryFactory::PerformQuery(size_t id, Args&&... args) const
		{
			BaseQuery* (*pPtr)(const MessageManager&, Args...) = (BaseQuery* (*)(const MessageManager&, Args...))m_mMethods.at(id);
			return pPtr(*m_pManager, args...);
		}

		template<typename ... Args>
		BaseQuery* QueryFactory::PerformQuery(const std::wstring& stName, Args&&... args) const
		{
			const int id = QueryRegistry::GetQueryId(stName);
			if(id != -1)
			{
				BaseQuery* (*pPtr)(const MessageManager&, Args...) = (BaseQuery* (*)(const MessageManager&, Args...))m_mMethods.at(id);
				if(pPtr)
					return pPtr(*m_pManager, args...);
			}
			
			return nullptr;
		}

		template<typename Type, typename ... Args>
		BaseQuery* QueryFactory::PerformQueryTemplate(const MessageManager& manager, Args... args)
		{
			Type* pQuery = new Type(args...);
			if(manager.Query(*pQuery))
				return pQuery;
			else
				return nullptr;
		}

	}
}