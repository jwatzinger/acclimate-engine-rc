#pragma once
#include "MessageManager.h"

namespace acl
{
	namespace ecs
	{
		
		template<typename Type, typename ... Args>
		BaseQuery* PerformQuery(const MessageManager& manager, Args... args)
		{
			Type* pQuery = new Type(args...);
			if(manager.Query(*pQuery))
				return pQuery;
			else
				return nullptr;
		}

	}
}