#include "QueryRegistry.h"

namespace acl
{
	namespace ecs
	{

		int QueryRegistry::GetQueryId(const std::wstring& stName)
		{
			if(m_mIds.count(stName))
				return m_mIds[stName];
			else
				return -1;
		}

		void QueryRegistry::UnregisterQuery(const std::wstring& stName)
		{
			auto itr = m_mIds.find(stName);
			if(itr != m_mIds.end())
				m_mIds.erase(itr);
		}

		QueryRegistry::QueryIdMap& QueryRegistry::GetMap(void)
		{
			return m_mIds;
		}

		QueryRegistry::QueryIdMap QueryRegistry::m_mIds(0);

	}
}
