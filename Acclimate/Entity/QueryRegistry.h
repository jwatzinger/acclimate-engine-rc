#pragma once
#include <unordered_map>
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{

		class ACCLIMATE_API QueryRegistry
		{
			typedef std::unordered_map<std::wstring, int> QueryIdMap;
		public:
			template<typename Message>
			static void RegisterQuery(const std::wstring& stName);
			static void UnregisterQuery(const std::wstring& stName);

			static int GetQueryId(const std::wstring& stName);

		private:
#pragma warning( disable: 4251 )
			static QueryIdMap m_mIds;
			static QueryIdMap& GetMap(void);
#pragma warning( default: 4251 )
		};
		
		template<typename Message>
		void QueryRegistry::RegisterQuery(const std::wstring& stName)
		{
			GetMap()[stName] = Message::family();
		}
	}
}

