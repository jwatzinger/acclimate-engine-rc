#include "RegisterEvents.h"
#include "EntityManager.h"
#include "Context.h"
#include "..\Core\EventLoader.h"

namespace acl
{
	namespace ecs
	{

		namespace detail
		{
			EntityManager* _pEntities = nullptr;
		}

		void registerEvents(const Context& entities)
		{
			detail::_pEntities = &entities.entities;

			// events
			core::EventLoader::RegisterEvent<CreateEntity>();
			core::EventLoader::RegisterEvent<RemoveEntity>();
			core::EventLoader::RegisterEvent<RemoveThisEntity>();

			// functions
			core::EventLoader::RegisterFunction<GetEventEntity>();

			// objects
			core::EventLoader::RegisterObject<EntityHandle>(L"Entity");
		}

		/*****************************************
		* CreateEntity
		******************************************/

		void CreateEntity::Reset(void)
		{
		}

		void CreateEntity::OnExecute(double dt)
		{
			auto& entity = detail::_pEntities->CreateEntity(GetAttribute<std::wstring>(0));

			ReturnValue<EntityHandle>(0, entity);

			CallOutput(0);
		}

		const core::EventDeclaration& CreateEntity::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"name", core::AttributeType::STRING, false }
			};

			const core::AttributeVector vReturns =
			{
				{ L"entity", core::getObjectType<EntityHandle>(), false }
			};

			static const core::EventDeclaration decl(L"CreateEntity", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* RemoveEntity
		******************************************/

		void RemoveEntity::Reset(void)
		{
		}

		void RemoveEntity::OnExecute(double dt)
		{
			auto& handle = GetAttribute<EntityHandle>(0);
			detail::_pEntities->RemoveEntity(handle);

			Invalidate(0);

			CallOutput(0);
		}

		const core::EventDeclaration& RemoveEntity::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"entity", core::getObjectType<EntityHandle>(), false }
			};

			static const core::EventDeclaration decl(L"RemoveEntity", vAttributes, core::AttributeVector());

			return decl;
		}

		/*****************************************
		* RemoveThisEntity
		******************************************/

		void RemoveThisEntity::SetEntity(const EntityHandle& entity)
		{
			m_entity = entity;
		}

		void RemoveThisEntity::Reset(void)
		{
		}

		void RemoveThisEntity::OnExecute(double dt)
		{
			detail::_pEntities->RemoveEntity(m_entity);

			CallOutput(0);
		}

		const core::EventDeclaration& RemoveThisEntity::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
			};

			static const core::EventDeclaration decl(L"RemoveThisEntity", vAttributes, core::AttributeVector());

			return decl;
		}

		/*****************************************
		* GetEventEntity
		******************************************/

		void GetEventEntity::SetEntity(const EntityHandle& entity)
		{
			m_entity = entity;
		}

		void GetEventEntity::OnCalculate(void)
		{
			if(m_entity.IsValid())
				ReturnValue<EntityHandle>(0, m_entity);
		}

		const core::FunctionDeclaration& GetEventEntity::GenerateDeclaration(void)
		{
			const core::AttributeVector vReturns =
			{
				{ L"entity", core::getObjectType<EntityHandle>(), false }
			};

			static const core::FunctionDeclaration decl(L"GetEventEntity", core::AttributeVector(), vReturns);

			return decl;
		}

	}
}