#pragma once
#include "..\Core\Event.h"
#include "..\Core\EventFunction.h"
#include "Entity.h"

namespace acl
{
	namespace ecs
	{

		EXPORT_OBJECT(EntityHandle);

		struct Context;

		void registerEvents(const Context& entities);

		/*****************************************
		* CreateEntity
		******************************************/

		class CreateEntity :
			public core::BaseEvent<CreateEntity>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* RemoveEntity
		******************************************/

		class RemoveEntity :
			public core::BaseEvent<RemoveEntity>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* RemoveThisEntity
		******************************************/

		class RemoveThisEntity :
			public core::BaseEvent<RemoveThisEntity>
		{
		public:

			void SetEntity(const EntityHandle& entity);

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);

		private:

			EntityHandle m_entity;
		};

		/*****************************************
		* GetEventEntity
		******************************************/

		class GetEventEntity :
			public core::BaseEventFunction<GetEventEntity>
		{
		public:

			void SetEntity(const EntityHandle& entity);

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);

		private:

			EntityHandle m_entity;
		};

	}
}