#include "RegisterScript.h"
#include "Entity.h"
#include "Context.h"
#include "EntityManager.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace ecs
	{
		namespace
		{
			const Context* _pContext;
		}

		void EntityConstruct(void* memory)
		{
			new(memory)EntityHandle();
		}

		void EntityConstruct(const EntityHandle& entity, void* memory)
		{
			new(memory) EntityHandle(entity);
		}

		void EntityDestruct(void* memory)
		{
			((EntityHandle*)memory)->~EntityHandle();
		}

		EntityHandle& createEntity(void)
		{
			return _pContext->entities.CreateEntity(L"Scripted");
		}

		EntityHandle& createEntity(const std::wstring& stName)
		{
			return _pContext->entities.CreateEntity(stName);
		}

		EntityHandle createEntityPrefab(const std::wstring& stName, const std::wstring& stPrefab)
		{
			return _pContext->entities.CreateEntityFromPrefab(stName, stPrefab);
		}

		void removeEntity(const EntityHandle& entity)
		{
			return _pContext->entities.RemoveEntity(entity);
		}

		const std::wstring& getName(const EntityHandle& entity)
		{
			return entity->GetName();
		}

		void RegisterScript(const Context& context, script::Core& script)
		{
			_pContext = &context;

			auto entity = script.RegisterValueType<EntityHandle>("Entity", script::VALUE_CLASS | script::VALUE_CLASS_COPY_CONSTRUCTOR | script::VALUE_CLASS_DESTRUCTOR);
			entity.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(EntityConstruct, (void*), void), asCALL_CDECL_OBJLAST);
			entity.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(const Entity& in)", asFUNCTIONPR(EntityConstruct, (const EntityHandle& entity, void*), void), asCALL_CDECL_OBJLAST);
			entity.RegisterBehaviour(asBEHAVE_DESTRUCT, "void f()", asFUNCTION(EntityDestruct), asCALL_CDECL_OBJLAST);
			entity.RegisterMethod("Entity& opAssign(const Entity& in)", asMETHODPR(EntityHandle, operator=, (const EntityHandle& entity), EntityHandle&));
			entity.RegisterMethod("bool IsValid() const", asMETHOD(EntityHandle, IsValid));
			entity.RegisterMethod("const string& GetName() const", asFUNCTION(getName), asCALL_CDECL_OBJFIRST);

			script.RegisterGlobalFunction("Entity& createEntity()", asFUNCTIONPR(createEntity, (void), EntityHandle&));
			script.RegisterGlobalFunction("Entity& createEntity(const string& in)", asFUNCTIONPR(createEntity, (const std::wstring&), EntityHandle&));
			script.RegisterGlobalFunction("Entity createEntityPrefab(const string& in, const string& in)", asFUNCTION(createEntityPrefab));
			script.RegisterGlobalFunction("void removeEntity(const Entity& in)", asFUNCTION(removeEntity));
		}

	}
}
