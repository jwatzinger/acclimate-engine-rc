#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace ecs
	{

		struct Context;

		void RegisterScript(const Context& context, script::Core& core);
	}
}