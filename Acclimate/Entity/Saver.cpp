#include "Saver.h"
#include "Entity.h"
#include "EntityManager.h"
#include "Prefabs.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace ecs
	{

		Saver::Saver(const EntityManager& entities): m_pEntities(&entities)
		{
		}

		void Saver::Save(const std::wstring& stFilename) const
		{
			auto& prefabs = m_pEntities->GetPrefabs();

			xml::Doc doc;
			xml::Node& world = doc.InsertNode(L"World");
			for(auto& entity : m_pEntities->AllEntities())
			{
				xml::Node& entityNode = world.InsertNode(L"Entity");
				entityNode.ModifyAttribute(L"name", entity->GetName());

				for(auto& routine : m_mSubRoutines)
				{
					routine.second->Execute(entity, entityNode);
				}

				auto& stPrefab = entity->GetPrefab();
				if(!stPrefab.empty())
				{
					entityNode.ModifyAttribute(L"prefab", stPrefab);

					auto pPrefab = prefabs.GetPrefab(stPrefab);

					for(auto pComponent : pPrefab->GetNodes())
					{
						auto stName = pComponent->GetName();
						auto pNode = entityNode.FirstNode(stName);
						if(!pNode)
						{
							auto& erase = entityNode.InsertNode(L"Erase");
							erase.SetValue(stName);
						}
						else if(pNode == pComponent)
						{
							entityNode.RemoveNode(*pNode);
						}
					}
				}
			}

			doc.SaveFile(stFilename.c_str());
		}

		void Saver::RegisterSubRoutine(const std::wstring& stName, const ISubSaveRoutine& routine)
		{
			m_mSubRoutines[stName] = &routine;
		}

		void Saver::UnregisterSubRoutine(const std::wstring& stName)
		{
			if(m_mSubRoutines.count(stName))
			{
				delete m_mSubRoutines.at(stName);
				m_mSubRoutines.erase(stName);
			}
		}

		Saver::SubRoutineMap Saver::m_mSubRoutines = Saver::SubRoutineMap();

	}
}