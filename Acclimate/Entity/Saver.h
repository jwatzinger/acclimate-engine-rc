/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>
#include <map>
#include "ISubSaveRoutine.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;

        /// Serializes entities and components to XML
		/** This class is used to write out all entities and components of a certain
		*   entity manager in XMl to a file. Only base components are being written, 
        *   all custom game declared components are being ignored. */
		class ACCLIMATE_API Saver
		{
			typedef std::map<std::wstring, const ISubSaveRoutine*> SubRoutineMap;

		public:
			Saver(const EntityManager& entities);

            /** Saves the entities to a file.
             *  This method saves all entities of the previously stored manager
             *  to a certain XML-file. Any other content in the file will be
             *  overwritten.
             *  
             *  @param[in] stFilename The path and name of the file that should be written to.
             */
			void Save(const std::wstring& stFilename) const;

			/** Registeres a save routine.
             *  This method is used to statically register a sub-save routine to the axm
			 *	component saver. A new instance will be created as specified. No checking for 
			 *	double registration (yet).
             *  
             *  @tparam Routine Classname of the routine to register.
             */
			template<typename Routine>
			static void RegisterSubRoutine(const std::wstring& stName);
			
			static void RegisterSubRoutine(const std::wstring& stName, const ISubSaveRoutine& routine);

			static void UnregisterSubRoutine(const std::wstring& stName);

		private:
#pragma warning( disable: 4251 )
			const EntityManager* m_pEntities; ///< Pointer to the entity manager being written out.

			static SubRoutineMap m_mSubRoutines; ///< Map of previously registered save routines.
#pragma warning( default: 4251 )
		};

		template<typename Routine>
		void Saver::RegisterSubRoutine(const std::wstring& stName)
		{
			UnregisterSubRoutine(stName);
			RegisterSubRoutine(stName, *new Routine);
		}

	}
}
