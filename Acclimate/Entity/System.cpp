#include "System.h"

namespace acl
{
	namespace ecs
	{

		BaseSystem::Family BaseSystem::family_count = 0;

		void BaseSystem::ResetCount(void)
		{
			family_count = 0;
		}

		BaseSystem::Family BaseSystem::GenerateFamilyId(void)
		{
			return family_count++;
		}

	}
}