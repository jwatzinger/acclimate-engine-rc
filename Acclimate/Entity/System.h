/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
        class MessageManager;
		class MessageFactory;
		class ComponentFactory;
		class QueryFactory;
        struct BaseMessage;
        struct BaseQuery;

		/*********************************
		* Base system struct
		*********************************/

        /// Used for implementing the CRTP for systems
		/** This class is used as a base from which the actual system derives.
		*   It offers an implementation of the Curiously recurring template pattern,
		*   by storing a static component counter. Each different system struct
        *   will increase this counter and aquire its unique id from it. It also
        *   acts as an interface for the actual systems by providing declaraions for
        *   the base methods of a system Update, ReceiveMessage and HandleQuery. */
		struct ACCLIMATE_API BaseSystem
		{
			typedef unsigned int Family;

            virtual ~BaseSystem(void) {}

            /** Executes the systems logic.
             *  This method is called when an an per-framet action is requested
             *  for the system. Each child system must override this.
             *
             *  @param[in] dt The delta timestep.
             */
			virtual void Update(double dt) = 0; 

            /** Handles any recieved messages.
             *  This method is used for handling messages the system might recieve.
             *  A child system does not have to override this method unless it should
             *  interact on a certain message. The message type is unclear at this
             *  point, and the translation process has to be done in the methods
             *  implementation (see Message, it offers a method for this) 
             *
             *  @param[in] message The recieved message. 
             */
			virtual void ReceiveMessage(const BaseMessage& message) {} 

            /** Handles any incoming queries.
             *  This method is used for handling querys the system might recieve.
             *  A child system does not have to override this method unless it should
             *  respond to a certain query. The query type is unclear at this
             *  point, and the translation process has to be done in the methods
             *  implementation (see Query, it offers a method for this).
             *
             *  @param[in] query The recieved query, that the system should responds to. 
             *
             *  @return Indicates whether the query was successful or not. 
             */
            virtual bool HandleQuery(BaseQuery& query) const { return false; } 

			static void ResetCount(void); // todo: make safer, now it could be called everywhere and screw up the ids!

		protected:

			static Family GenerateFamilyId(void);

		private:
			static Family family_count; /**< Counter of instanciated systems.
                *   Each system of a different type that is created, or whose family 
                *   function is called the first time will increase this counter. The 
                *   counter is used as the ID of the next system. */
		};

        /*********************************
		* System
		*********************************/

        /// The system parent struct
		/** Each system must derive from this struct. It automatically sets its
        *   unique family id on creation, which can then be accessed via a static
        *   method, so you can compare the type of a component object with the type
        *   of a component struct. It also offers initialisation and registration
        *   methods for setting up the system. */
		template <typename Derived>
		struct System : 
            public BaseSystem 
        {

            virtual ~System(void) override = 0 {}
            
            /** Called upon system registration.
             *  This method sets the managers a system might need for interaction.
             *
             *  @param[in] entities Handle to the entity manager this system is corresponding with. 
             *  @param[in] messages Const handle to the message manager this system can speak to. 
             */
            void Register(EntityManager& entities, const MessageManager& messages, const MessageFactory& messageFactory, const ComponentFactory& componentFactory, const QueryFactory& queryFactory)
            {
                m_pEntities = &entities;
                m_pMessages = &messages;
				m_pMessageFactory = &messageFactory;
				m_pComponentFactory = &componentFactory;
				m_pQueryFactory = &queryFactory;
            }

        public:

            /** Should be overridden to subscribe to messages and queries.
             *  This method is called upon system initialization. It might be
             *  overwritten by each system that needs to recieve certain messages
             *  and/or queries, and should perform the registration there.
             *
             *  @param[in] messageManager Handle to the message manager the system can subscribe to. 
             */
			virtual void Init(MessageManager& messages) {}

            /** Returns the "family" type of the system.
			 *	This method sets the type id for the specific system struct on
             *  the first call, and returns it from there on. The id is consistent 
             *  between the running time of a program and cannot be altered, but
             *  it may alter between different runs, depending on the first creation 
             *  order of the systems. 
             *
             *  @return The unique id of this system instance
             */
			static Family family(void)
            {
			    static BaseSystem::Family Family = BaseSystem::GenerateFamilyId();
			    return Family;
		    }

        protected:

            EntityManager* m_pEntities; ///< Handle to the entity manager.
            const MessageManager* m_pMessages; ///< Const handle to the message manager.
			const MessageFactory* m_pMessageFactory; 
			const ComponentFactory* m_pComponentFactory; 
			const QueryFactory* m_pQueryFactory; 

		};

	}
}
