#include "SystemManager.h"
#include "System.h"

namespace acl
{
	namespace ecs
	{

		SystemManager::SystemManager(EntityManager& entityManager, MessageManager& messageManager, const MessageFactory& messageFactory, const ComponentFactory& componentFactory, const QueryFactory& queryFactory): 
			m_pEntityManager(&entityManager), m_pMessageManager(&messageManager), m_pMessageFactory(&messageFactory), m_pComponentFactory(&componentFactory), m_pQueryFactory(&queryFactory)
		{
		}

		SystemManager::~SystemManager(void)
		{
            // call clear to free memory
            Clear();
		}

        void SystemManager::Clear(void)
        {
			for(auto& system : m_mSystems)
			{
				delete system.second;
			}

            m_mSystems.clear();
        }

	}
}
