/*
* Acclimate Engine: Entity/Component
*
* Created by Julian Watzinger
*/

#pragma once
#include <unordered_map>
#include "MessageManager.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace ecs
	{
        struct BaseSystem;
        class EntityManager;
		class MessageFactory;
		class ComponentFactory;
		class QueryFactory;
        struct BaseQuery;

        /// Handles creation and updating of systems
		/** The SystemManager is responsible for creating and storing 
		*   System objects. It allows for update of selective systems. */
		class ACCLIMATE_API SystemManager
		{
			typedef std::unordered_map<size_t, BaseSystem*> SystemMap;

		public:
			SystemManager(EntityManager& entityManager, MessageManager& messageManager, const MessageFactory& messageFactory, const ComponentFactory& componentFactory, const QueryFactory& queryFactory);
			~SystemManager(void);

            /** Creates and adds a system.
             *  This method creates a system using the passed
             *  parameters as its constructor params. Will only
             *  add a new system unless another one of this type
             *  is already found.
             *
             *  @param[in] args The constructor arguments for the system.
             *  
             *  @tparam S The systems type.
             */
			template<typename S, typename... Args>
			void AddSystem(Args&&... args);

            /** Removes a system.
             *  This method deletes a system of given type.
             *
             *  @tparam S The systems type.
             */
            template<typename S>
            void RemoveSystem(void);

            /** Removes all systems.
             *  This method deletes all systems and clears
             *  the system storage. */
            void Clear();
	
            /** Updates a system.
             *  This method requests a update of the system of the
             *  given type. Does nothing if no such system exists.
             *
             *  @param[in] dt The delta timestep.
             *
             *  @tparam S The systems type.
             */
			template<typename S>
			void UpdateSystem(double dt);

		private:
#pragma warning( disable: 4251 )
			SystemMap m_mSystems; ///< The system storage map.

			EntityManager* m_pEntityManager; ///< Pointer to the entity manager.
			MessageManager* m_pMessageManager; ///< Pointer to the message manager.
			const MessageFactory* m_pMessageFactory;
			const ComponentFactory* m_pComponentFactory;
			const QueryFactory* m_pQueryFactory;
#pragma warning( default: 4251 )
		};

		template<typename S, typename... Args>
		void SystemManager::AddSystem(Args&&... args)
		{
            // aquire family id
            BaseSystem::Family family = S::family();

            // system can only be registered once, if a new one needs
            // to be created, the old one has to be unregistered first
            if( !m_mSystems.count(family) )
            {
			    //create new system of type S
			    S* pSystem = new S(args...);

                pSystem->Register(*m_pEntityManager, *m_pMessageManager, *m_pMessageFactory, *m_pComponentFactory, *m_pQueryFactory);

			    //initialize system to allow registration of recieved messages
			    pSystem->Init(*m_pMessageManager);
	
			    //add system to list
			    m_mSystems[family] = pSystem;
            }
		}

        template<typename S>
        void SystemManager::RemoveSystem(void)
        {
            BaseSystem::Family family = S::family();
            if( m_mSystems.count( family ) )
            {
				auto pSystem = m_mSystems[family];
				m_pMessageManager->UnsubscribeAll(*pSystem);
				m_pMessageManager->UnregisterAll(*pSystem);
                delete pSystem;
                m_mSystems.erase(family);
            }
        }

		template<typename S>
		void SystemManager::UpdateSystem(double dt)
        {
            BaseSystem::Family family = S::family();

            if( m_mSystems.count(family) )
			    //update system of given family
			    m_mSystems[family]->Update(dt);
		}

	}
}