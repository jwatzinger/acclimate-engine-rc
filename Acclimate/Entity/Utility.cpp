#include "Utility.h"
#include "Component.h"
#include "Message.h"
#include "System.h"
#include "Query.h"

namespace acl
{
	namespace ecs
	{

		void ResetIdCounts(void)
		{
			BaseComponent::ResetCount();
			BaseMessage::ResetCount();
			BaseSystem::ResetCount();
			BaseQuery::ResetCount();
		}

	}
}