#include "Dir.h"
#include "dirent.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace file
	{

        /*************************************************
		* DirListSub
		*************************************************/

		void DirListSub(const std::wstring& stDirName, DirVector& vOut)
		{
			_WDIR *pDir = _wopendir(stDirName.c_str());

			_wdirent *entry(_wreaddir(pDir));

			// skip first two values => "." and ".."
			_wreaddir(pDir);
			entry = _wreaddir(pDir);

			while (entry)
			{
				if (entry->d_type == DT_DIR)
					vOut.push_back(entry->d_name);   

				entry = _wreaddir(pDir);
			}

			_wclosedir(pDir);
		}

        /*************************************************
		* DirCreate
		*************************************************/

		void DirCreate(const std::wstring& stDirName)
		{
			if(!DirExists(stDirName) && _wmkdir(stDirName.c_str()))
					throw fileException();
		}

        /*************************************************
		* DirExists
		*************************************************/

		bool DirExists(const std::wstring& stDirName)
		{
			_WDIR* pDir = _wopendir(stDirName.c_str());

			if(pDir)
			{
				_wclosedir(pDir);
				return true;
			}
			else
				return false;
		}

        /*************************************************
		* DirName
		*************************************************/
		
		std::wstring DirName(const std::wstring& stPath)
		{
			std::wstring stSub = stPath;

			const size_t filePos = stPath.find_last_of(L".");
			if(filePos != stPath.size())
				stSub = stSub.substr(0, stPath.find_last_of(L"\\"));
			else
			{
				const size_t pos = stPath.find_last_of(L"\\");
				if(pos == stPath.size() - 2)
					stSub = stSub.substr(0, pos);
			}

			stSub = stSub.substr(stSub.find_last_of(L"\\")+1, stPath.size());

			return stSub;
		}

		/*************************************************
		* DirCopy
		*************************************************/

		void DirCopy(const std::wstring& stSourcePath, const std::wstring& stDestPath)
		{
			SHFILEOPSTRUCT s = { 0 };
			s.hwnd = nullptr;
			s.wFunc = FO_COPY;
			s.fFlags = FOF_SILENT;
			s.pTo = stDestPath.c_str();
			s.pFrom = (stSourcePath + L"*").c_str();
			SHFileOperation(&s);
		}

	}
}