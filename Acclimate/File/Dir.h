#pragma once
#include <string>
#include <vector>
#include "..\Core\Dll.h"

namespace acl
{
	namespace file
	{
		typedef std::vector<std::wstring> DirVector;

		ACCLIMATE_API void DirListSub(const std::wstring& stDirName, DirVector& vOut);
		ACCLIMATE_API void DirCreate(const std::wstring& stDirName);
		ACCLIMATE_API bool DirExists(const std::wstring& stDirName);
		ACCLIMATE_API std::wstring DirName(const std::wstring& stPath);

		ACCLIMATE_API void DirCopy(const std::wstring& stSourcePath, const std::wstring& stDestPath);
	}
}
