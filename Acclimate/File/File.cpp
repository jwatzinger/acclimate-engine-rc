#include "File.h"
#include <fstream>
#include <Windows.h>
#include  "dirent.h"
#include "..\System\Convert.h"
#include "..\System\Exception.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace file
    {

		/*************************************************
		* FileCopy
		*************************************************/

        void Copy(const std::wstring& stInName, const std::wstring& stOutName)
        {
            std::ifstream ifs(stInName, std::ios::binary);
            std::ofstream ofs(stOutName, std::ios::binary);

            if(!ifs.is_open())
                throw fileException();
            else if(!ofs.is_open())
                throw fileException();

            ofs << ifs.rdbuf();
        }
        
        /*************************************************
		* ListFiles
		*************************************************/

        void ListFiles(const std::wstring& stDirName, FileVector& vOut)
        {
			_WDIR *pDir = _wopendir(stDirName.c_str());

            if(pDir)
            {
			    struct _wdirent *entry = _wreaddir(pDir);

			    while (entry)
			    {
				    if (entry->d_type != DT_DIR)
					    vOut.push_back(entry->d_name);   

				    entry = _wreaddir(pDir);
			    }

			    _wclosedir(pDir);
            }
            else
                throw fileException();
        }

		/*************************************************
		* FullPath
		*************************************************/

        std::wstring FullPath(const std::wstring& stFile)
        {
            wchar_t buffer[1024];

            GetFullPathName(stFile.c_str(), 1024, buffer, nullptr);

			std::wstring stOut = buffer;
			const auto dotPos = stOut.find('.');
			if(dotPos == std::wstring::npos)
			{
				const auto slashPos = stOut.find_last_of(L"\\/");
				if(slashPos == std::wstring::npos)
					stOut.push_back('/');
			}

			return stOut;
        }

        /*************************************************
		* FilePath
		*************************************************/

        std::wstring FilePath(const std::wstring& stFullPath, const std::wstring& stFile)
        {
            std::wstring stPath(stFullPath);
			if(!stFile.empty())
            {
                const size_t lastIdx = stFullPath.rfind(stFile);
                if (std::string::npos != lastIdx)
                    return stPath.substr(0, lastIdx);
            }
			else
			{
				const size_t pos = stFullPath.find_last_of(L"\\/");
				if(pos != std::wstring::npos)
					return stPath.substr(0, pos);
				else
				{
					const size_t dotPos = stFullPath.find(L".");
					if(dotPos != std::wstring::npos)
						return L".\\";
				}
			}

			return stPath;
        }

		/*************************************************
		* FileName
		*************************************************/

		std::wstring FileName(const std::wstring& stFilePath)
		{
			const size_t pos = stFilePath.find_last_of(L"\\/");
			if(pos != std::wstring::npos)
				return stFilePath.substr(pos + 1);
			else
				return stFilePath;
		}

		/*************************************************
		* RelativePath
		*************************************************/

		std::wstring RelativePath(const std::wstring& stParent, const std::wstring& stTarget)
		{
			const auto& stFullParent = FullPath(stParent);
			const auto& stFullTarget = FullPath(stTarget);

			const auto pos = stFullTarget.find(stFullParent);
			if(pos != std::wstring::npos)
			{
				auto stOut = stFullTarget.substr(stFullParent.size());
				if(stOut.front() == '/' || stOut.front() == '\\')
					stOut.erase(0, 1);

				return stOut;
			}
			else
			{
				unsigned int count = 0;
				auto stTargetCopy = stFullTarget;

				auto slashPos = stTargetCopy.find_last_of(L"/\\");
				while(slashPos != std::wstring::npos)
				{
					if(slashPos != stTargetCopy.size() - 1)
					{
						count++;
						stTargetCopy.erase(slashPos);

						const auto pos = stFullParent.find(stTargetCopy);
						if(pos != std::wstring::npos)
						{
							auto stDir = stFullTarget.substr(stTargetCopy.size()+1);
							for(unsigned int i = 0; i < count; i++)
							{
								stDir.insert(0, L"../");
							}
							return stDir;
						}
					}
					else
						stTargetCopy.erase(slashPos);

					slashPos = stTargetCopy.find_last_of(L"/\\");
				}

				return stTarget;
			}
		}

		/*************************************************
		* RelativeForSubPath
		*************************************************/

		std::wstring RelativeForSubPath(const std::wstring& stParent, const std::wstring& stTarget)
		{
			const auto& stFullParent = FullPath(stParent);
			const auto& stFullTarget = FullPath(stTarget);

			const auto pos = stFullTarget.find(stFullParent);
			if(pos != std::wstring::npos)
			{
				auto stOut = stFullTarget.substr(stFullParent.size());
				if(stOut.front() == '/' || stOut.front() == '\\')
					stOut.erase(0, 1);

				return stOut;
			}
			else
				return stTarget;
		}

		/*************************************************
		* IsInPath
		*************************************************/

		bool IsInPath(const std::wstring& stParent, const std::wstring& stTarget)
		{
			const auto& stFullParent = FullPath(stParent);
			const auto& stFullTarget = FullPath(stTarget);

			return stFullTarget.find(stFullParent) != std::wstring::npos;
		}

        /*************************************************
		* Extention
		*************************************************/

        std::wstring Extention(const std::wstring& stFile)
        {
            size_t pos = stFile.find_last_of(L".");
            if(pos < stFile.size()-1)
                return stFile.substr(pos+1, stFile.size());
            else
                return L"";
        }

        /*************************************************
		* SubExtention
		*************************************************/

        std::wstring SubExtention(const std::wstring& stFile)
        {
            size_t pos = stFile.find_last_of(L".");
            if(pos < stFile.size())
                return stFile.substr(0, pos);
            else
                return stFile;
        }

		/*************************************************
		* FileExists
		*************************************************/

		bool FileExists(const std::wstring& stFileName)
		{
			std::ifstream ifs(stFileName, std::ios::binary);

			return ifs.is_open();
		}

		/*************************************************
		* FileExists
		*************************************************/

		std::string LoadFileA(const std::wstring& stFile)
		{
			// Open the file in binary mode
			FILE* f = nullptr;
			if(fopen_s(&f, conv::ToA(stFile).c_str(), "rb"))
				throw fileException();

			// Determine the size of the file
			fseek(f, 0, SEEK_END);
			int len = ftell(f);
			fseek(f, 0, SEEK_SET);
			// Load the entire file in one call
			std::string script;

			script.resize(len);
			fread(&script[0], len, 1, f);
			fclose(f);

			return script;
		}

		void SaveFileA(const std::wstring& stFile, const std::string& stContent)
		{
			FILE* pFile = nullptr;
			if(fopen_s(&pFile, conv::ToA(stFile).c_str(), "wb"))
				throw fileException();

			fwrite(stContent.c_str(), sizeof(char), stContent.size(), pFile);

			fclose(pFile);
		}

    }
}