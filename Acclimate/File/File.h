#pragma once
#include <string>
#include <vector>
#include "..\Core\Dll.h"

namespace acl
{
    namespace file
    {
        typedef std::vector<std::wstring> FileVector;

        ACCLIMATE_API void Copy(const std::wstring& stInName, const std::wstring& stOutName);

		ACCLIMATE_API void ListFiles(const std::wstring& stDirName, FileVector& vOut);
		ACCLIMATE_API std::wstring FullPath(const std::wstring& stFile);
        ACCLIMATE_API std::wstring FilePath(const std::wstring& stFullPath, const std::wstring& stFile = L"");
		ACCLIMATE_API std::wstring FileName(const std::wstring& stFilePath);
		ACCLIMATE_API std::wstring RelativePath(const std::wstring& stParent, const std::wstring& stTarget);
		ACCLIMATE_API std::wstring RelativeForSubPath(const std::wstring& stParent, const std::wstring& stTarget);
		ACCLIMATE_API bool IsInPath(const std::wstring& stParent, const std::wstring& stTarget);
		ACCLIMATE_API std::wstring Extention(const std::wstring& stFile);
		ACCLIMATE_API std::wstring SubExtention(const std::wstring& stFile);

		ACCLIMATE_API std::string LoadFileA(const std::wstring& stFile);
		ACCLIMATE_API void SaveFileA(const std::wstring& stFile, const std::string& stContent);

		ACCLIMATE_API bool Exists(const std::wstring& stFileName);
    }
}