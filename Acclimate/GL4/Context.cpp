#include "Context.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\Math\Rect.h"
#include "..\..\Extern\GLEW\Glew.h"
#include "..\..\Extern\\GLEW\wglew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			Context::Context(HDC hDc, DEVMODE& mode) : m_vBackbufferSize(mode.dmPelsWidth, mode.dmPelsHeight),
				m_devMode(mode), m_isFullscreen(false)
			{
				/*********************************************
				* Set pixel fromat
				*********************************************/

				PIXELFORMATDESCRIPTOR pfd = {};
				pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
				pfd.nVersion = 1;
				pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
				pfd.iPixelType = PFD_TYPE_RGBA;
				pfd.cColorBits = 24;
				pfd.cAlphaBits = 8;
				pfd.cDepthBits = 32;
				pfd.iLayerType = PFD_MAIN_PLANE;

				int pixelFormat = ChoosePixelFormat(hDc, &pfd);

				SetPixelFormat(hDc, pixelFormat, &pfd);

				/*********************************************
				* Create base context
				*********************************************/

				HGLRC hContext = wglCreateContext(hDc);

				wglMakeCurrent(hDc, hContext);

				glewInit();

				/*********************************************
				* Create 4.4 context
				*********************************************/

				const int attriblist[] = 
				{	
					WGL_CONTEXT_MAJOR_VERSION_ARB, 4, 
					WGL_CONTEXT_MINOR_VERSION_ARB, 2, 
					WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
#ifdef _DEBUG
					WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
#endif
					0, 0 
				};

				m_hContext = wglCreateContextAttribsARB(hDc, hContext, attriblist);
				wglMakeCurrent(hDc, m_hContext);

				wglDeleteContext(hContext); 
				wglSwapIntervalEXT(false);

				GL_CHECK(glDebugMessageCallback(openGlErrorCallback, nullptr));

				GL_CHECK(glViewport(0, 0, m_vBackbufferSize.x, m_vBackbufferSize.y));

				GL_CHECK(glDisable(GL_CULL_FACE));
				GL_CHECK(glDepthFunc(GL_LESS));
				
				SetClearDepth(1.0f);
			}

			Context::~Context(void)
			{
				wglMakeCurrent(nullptr, nullptr);
				wglDeleteContext(m_hContext);
			}

			void Context::SetBlending(bool bEnable) const
			{
				if(bEnable)
				{
					GL_CHECK(glEnable(GL_BLEND));
				}
				else
				{
					GL_CHECK(glDisable(GL_BLEND));
				}
			}

			void Context::SetBlendingFunc(unsigned int src, unsigned int dest) const
			{
				GL_CHECK(glBlendFunc(src, dest));
			}

			void Context::SetBlendingEquation(unsigned int equation) const
			{
				GL_CHECK(glBlendEquation(equation));
			}

			void Context::SetClearColor(float r, float g, float b, float a) const
			{
				GL_CHECK(glClearColor(r, g, b, a));
			}

			void Context::SetClearDepth(float value) const
			{
				GL_CHECK(glClearDepth(value));
			}

			void Context::SetDepthState(bool bEnable, bool bWrite) const
			{
				if(bEnable)
				{
					GL_CHECK(glEnable(GL_DEPTH_TEST));
				}
				else
				{
					GL_CHECK(glDisable(GL_DEPTH_TEST));
				}

				GL_CHECK(glDepthMask(bWrite));
			}

			void Context::SetCullState(unsigned int state) const
			{
				if(state == GL_NONE)
				{
					GL_CHECK(glDisable(GL_CULL_FACE));
				}
				else
				{
					GL_CHECK(glEnable(GL_CULL_FACE));
					GL_CHECK(glCullFace(state));
				}
			}

			void Context::SetFillState(unsigned int state) const
			{
				GL_CHECK(glPolygonMode(GL_FRONT_AND_BACK, state));
			}

			void Context::SetViewport(const math::Rect& rViewport) const
			{
				if(rViewport.width == 0.0f && rViewport.height == 0.0f)
				{
					GL_CHECK(glViewport(0, 0, m_vBackbufferSize.x, m_vBackbufferSize.y));
				}
				else
				{
					GL_CHECK(glViewport(rViewport.x, rViewport.y, rViewport.width, rViewport.height));
				}
			}

			void Context::SetFullscreen(bool isFullscreen)
			{
				if(isFullscreen)
					ChangeDisplaySettings(&m_devMode, CDS_FULLSCREEN);
				else
					ChangeDisplaySettings(&m_devMode, 0);

				m_isFullscreen = isFullscreen;
			}

			bool Context::IsFullscreen(void) const
			{
				return m_isFullscreen;
			}

			const math::Vector2& Context::GetBackbufferSize(void) const
			{
				return m_vBackbufferSize;
			}

			void Context::DrawElements(VertexType type, size_t numElementes, size_t startIndex) const
			{
				GL_CHECK(glDrawElements((GLenum)type, numElementes, GL_UNSIGNED_INT, (const void*)(startIndex*sizeof(unsigned int))));
			}

			void Context::DrawInstanced(VertexType type, size_t numInstances, size_t numElementes, size_t startIndex) const
			{
				GL_CHECK(glDrawElementsInstanced((GLenum)type, numElementes, GL_UNSIGNED_INT, (const void*)(startIndex*sizeof(unsigned int)), numInstances));
			}

			void Context::Clear(ClearType values) const
			{
				GL_CHECK(glDepthMask(true));
				GL_CHECK(glClear(values));
			}

			void Context::Resize(unsigned int width, unsigned int height)
			{
				m_vBackbufferSize.x = width;
				m_vBackbufferSize.y = height;
			}

			void Context::UnbindTexture(unsigned int slot) const
			{
				GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, 0));
			}

			void Context::UnbindFramebuffer(void) const
			{
				GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
			}

			void Context::UnbindVertexArrayObject(void) const
			{
				GL_CHECK(glBindVertexArray(0));
			}
		}
	}
}

#endif
