﻿#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <Windows.h>
#include "..\Math\Vector.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace math
	{
		struct Rect;
	}

	namespace gl4
	{
		namespace ogl
		{
			enum ClearType
			{
				NONE = 0,
				COLOR = GL_COLOR_BUFFER_BIT, 
				DEPTH = GL_DEPTH_BUFFER_BIT
			};

			enum class VertexType
			{
				TRIANGLE = GL_TRIANGLES,
				LINE = GL_LINES,
				TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
				POINT = GL_POINTS
			};

			class Context
			{
			public:
				Context(HDC hDc, DEVMODE& mode);
				~Context(void);

				void SetBlending(bool bEnable) const;
				void SetBlendingFunc(unsigned int src, unsigned int dest) const;
				void SetBlendingEquation(unsigned int equation) const;
				void SetClearColor(float r, float g, float b, float a) const;
				void SetClearDepth(float value) const;
				void SetDepthState(bool bEnable, bool bWrite) const;
				void SetCullState(unsigned int state) const;
				void SetFillState(unsigned int state) const;
				void SetViewport(const math::Rect& rViewport) const;
				void SetFullscreen(bool isFullscreen);
				
				bool IsFullscreen(void) const;
				const math::Vector2& GetBackbufferSize(void) const;

				void DrawElements(VertexType type, size_t numElementes, size_t startIndex) const;
				void DrawInstanced(VertexType type, size_t numInstances, size_t numElementes, size_t startIndex) const;
				void Clear(ClearType values) const;
				void Resize(unsigned int width, unsigned int height);
				
				void UnbindTexture(unsigned int slot) const;
				void UnbindFramebuffer(void) const;
				void UnbindVertexArrayObject(void) const;

			private:

				bool m_isFullscreen;

				HGLRC m_hContext;
				DEVMODE& m_devMode;

				math::Vector2 m_vBackbufferSize;
			};

		}
	}
}

#endif

