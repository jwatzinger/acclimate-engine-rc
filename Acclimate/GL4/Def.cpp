#include "Def.h"
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#pragma comment(lib, "FreeImage/FreeImage.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GLEW/glew32.lib")

#endif