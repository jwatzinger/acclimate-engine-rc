#pragma once

#ifdef USE_API_GL4
#define ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			class Context;
			class Sprite;
			class Texture;
			class VertexBufferObject;
			class IndexBufferObject;
			class Effect;
			class Mesh;
			class Font;
			class DepthBuffer;
			class Line;
			class UniformBufferObject;
		}
	}

	typedef gl4::ogl::Context AclDevice; 
	typedef gl4::ogl::Sprite AclSprite;
	typedef gl4::ogl::Texture AclTexture;
	typedef gl4::ogl::DepthBuffer AclDepth;
	typedef gl4::ogl::VertexBufferObject AclVertexBuffer;
	typedef gl4::ogl::IndexBufferObject AclIndexBuffer;
    typedef gl4::ogl::Effect AclEffect;
    typedef gl4::ogl::Mesh AclMesh;
    typedef gl4::ogl::Font AclFont;
	typedef gl4::ogl::Line AclLine;
	typedef gl4::ogl::UniformBufferObject AclCbuffer;
	typedef void* AclGeometry;

}

#endif