#include "DepthBuffer.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			DepthBuffer::DepthBuffer(int width, int height): m_depthBuffer(0), 
				m_vSize(width, height)
			{
				GL_CHECK(glGenRenderbuffers(1, &m_depthBuffer));

				GL_CHECK(glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer));

				GL_CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height));
			}

			DepthBuffer::~DepthBuffer(void)
			{
				GL_CHECK(glDeleteTextures(1, &m_depthBuffer));
			}

			void DepthBuffer::Recreate(int width, int height)
			{
				m_vSize = math::Vector2(width, height);

				GL_CHECK(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height));
			}

			const math::Vector2& DepthBuffer::GetSize(void) const
			{
				return m_vSize;
			}

			void DepthBuffer::Bind(void) const
			{
				GL_CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer));
			}

		}
	}
}

#endif