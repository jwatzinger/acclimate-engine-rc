#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\Math\Vector.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			class DepthBuffer
			{
			public:
				DepthBuffer(int width, int height);
				~DepthBuffer(void);

				void Recreate(int width, int height);

				const math::Vector2& GetSize(void) const;

				void Bind(void) const;

			private:

				unsigned int m_depthBuffer;

				math::Vector2 m_vSize;
			};

		}
	}
}

#endif