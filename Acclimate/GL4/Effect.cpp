﻿#include "Effect.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "UniformBufferObject.h"
#include "..\System\Log.h"
#include "..\System\Exception.h"
#include "..\..\Extern\GLEW\glew.h"
#include "..\Gfx\Defines.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			const unsigned int NUM_UBOS = gfx::MAX_CBUFFERS*3;

			Effect::Effect(const std::string& stData, const MacroVector& vMacros, bool hasGeometryShader) : m_vUniforms(NUM_UBOS),
				m_geometry(0)
			{	
				GL_CHECK(m_program = glCreateProgram());

				// find seperator
				size_t pixelSeperator = stData.find("// frag_begin");
				size_t vertexSeperator;
				if(hasGeometryShader)
				{
					size_t geometrySeperator = stData.find("// geo_begin");
					vertexSeperator = geometrySeperator;
					CreateShader(m_geometry, GL_GEOMETRY_SHADER, vMacros, stData.substr(geometrySeperator + 12, pixelSeperator - (geometrySeperator+12)));
				}
				else
					vertexSeperator = pixelSeperator;

				CreateShader(m_vertex, GL_VERTEX_SHADER, vMacros, stData.substr(0, vertexSeperator));
				CreateShader(m_fragment, GL_FRAGMENT_SHADER, vMacros, stData.substr(pixelSeperator + 13));


				GL_CHECK(glLinkProgram(m_program));
				CheckAndHandleLinkError();

				CreateUniformBuffers();

				// textures to uniforms

				int numUniforms = 0, nameLength = 0;
				GL_CHECK(glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &numUniforms));
				GL_CHECK(glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &nameLength));

				Bind();
				
				BindTextures(4, 0, vertexSeperator, stData);
				BindTextures(0, pixelSeperator, std::string::npos, stData);
			}

			Effect::~Effect(void)
			{
				glDeleteShader(m_vertex);
				glDeleteShader(m_fragment);
				glDeleteShader(m_geometry);

				glDeleteProgram(m_program);

				for(size_t i = 0; i < NUM_UBOS; i++)
				{
					delete m_vUniforms[i];
				}
			}

			UniformBufferObject* Effect::CloneVertexUBO(size_t id) const
			{
				if(auto pBuffer = m_vUniforms[id])
					return new UniformBufferObject(*pBuffer);
				else
					return nullptr;
			}

			UniformBufferObject* Effect::CloneFragmentUBO(size_t id) const
			{
				if(auto pBuffer = m_vUniforms[id+gfx::MAX_CBUFFERS])
					return new UniformBufferObject(*pBuffer);
				else
					return nullptr;
			}

			UniformBufferObject* Effect::CloneGeometryUBO(size_t id) const
			{
				if(auto pBuffer = m_vUniforms[id + gfx::MAX_CBUFFERS*2])
					return new UniformBufferObject(*pBuffer);
				else
					return nullptr;
			}

			void Effect::Bind(void) const
			{
				GL_CHECK(glUseProgram(m_program));
			}

			void Effect::CheckAndHandleLinkError(void) const
			{
				GLint success;
				GL_CHECK(glGetProgramiv(m_program, GL_LINK_STATUS, &success));

				if(!success)
				{
					GLint logSize;
					GL_CHECK(glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logSize));
					char* errorMessage = new char[logSize];
					GL_CHECK(glGetInfoLogARB(m_program, logSize, &logSize, errorMessage));
					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, errorMessage);

					delete[] errorMessage;

					throw apiException();
				}
			}

			void CheckAndHandleError(unsigned int shader)
			{
				GLint success = 0;
				GL_CHECK(glGetShaderiv(shader, GL_COMPILE_STATUS, &success));

				if(!success)
				{
					GLint logSize = 0;
					GL_CHECK(glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize));
					char* errorMessage = new char[logSize];
					GL_CHECK(glGetShaderInfoLog(shader, logSize, &logSize, errorMessage));

					sys::log->Out(sys::LogModule::API, sys::LogType::ERR, errorMessage);

					delete[] errorMessage;

					throw apiException();
				}
			}

			void Effect::CreateShader(unsigned int& shader, unsigned int type, const MacroVector& vMacros, const std::string& stData)
			{
				GL_CHECK(shader = glCreateShader(type));

				std::string stFullData(stData);
				stFullData.insert(0, "#version 420 core\n");
				for(auto& stMacro : vMacros)
				{
					stFullData.insert(18, stMacro);
				}

				const char* pData = stFullData.c_str();
				GL_CHECK(glShaderSource(shader, 1, &pData, nullptr));
				GL_CHECK(glCompileShader(shader));

				CheckAndHandleError(shader);

				GL_CHECK(glAttachShader(m_program, shader));
			}

			void Effect::CreateUniformBuffers(void)
			{
				QueryUniformBlock("InstanceV", 0);
				QueryUniformBlock("StageV", 1);
				QueryUniformBlock("InstanceF", 2);
				QueryUniformBlock("StageF", 3);
				QueryUniformBlock("InstanceG", 4);
				QueryUniformBlock("StageG", 5);
			}

			void Effect::QueryUniformBlock(const char* stage, unsigned int bind)
			{
				unsigned int index;
				GL_CHECK(index = glGetUniformBlockIndex(m_program, stage));

				if(index != GL_INVALID_INDEX)
				{
					int uniformSize = 0;
					GL_CHECK(glUniformBlockBinding(m_program, index, bind));
					GL_CHECK(glGetActiveUniformBlockiv(m_program, index, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformSize));

					m_vUniforms[bind] = new UniformBufferObject(uniformSize);
				}
			}

			void Effect::BindTextures(size_t startSlot, size_t begin, size_t end, const std::string& stData) const
			{
				unsigned int texture = startSlot;
				auto pos = stData.find("uniform sampler2D ", begin);
				while(pos != std::string::npos && pos < end)
				{
					pos += 18;
					std::string stName;
					while(true)
					{
						char c = stData[pos];
						if(c == ';')
							break;
						else
							stName += c;
						pos++;
					}

					int location;
					GL_CHECK(location = glGetUniformLocation(m_program, stName.c_str()));
					if(location > -1)
						GL_CHECK(glUniform1i(location, texture));

					pos = stData.find("uniform sampler2D ", pos);
					texture++;
				}
			}

		}
	}
}


#endif