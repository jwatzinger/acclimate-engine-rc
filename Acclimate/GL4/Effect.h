#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <vector>
#include <string>

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class UniformBufferObject;

			class Effect
			{
				typedef std::vector<UniformBufferObject*> UniformVector;
			public:
				typedef std::vector<std::string> MacroVector;

				Effect(const std::string& stData, const MacroVector& vMacros, bool hasGeometryShader);
				~Effect(void);

				UniformBufferObject* CloneVertexUBO(size_t id) const;
				UniformBufferObject* CloneFragmentUBO(size_t id) const;
				UniformBufferObject* CloneGeometryUBO(size_t id) const;

				void Bind(void) const;

			private:

				void CheckAndHandleLinkError(void) const;

				void CreateShader(unsigned int& shader, unsigned int type, const MacroVector& vMacros, const std::string& stData);
				void CreateUniformBuffers(void);
				void QueryUniformBlock(const char* stage, unsigned int bind);
				void BindTextures(size_t startSlot, size_t begin, size_t end, const std::string& stData) const;

				unsigned int m_vertex, m_fragment, m_geometry;
				unsigned int m_program;
				UniformVector m_vUniforms;
			};

		}
	}
}

#endif