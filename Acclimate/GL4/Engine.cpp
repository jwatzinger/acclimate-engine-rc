#include "Engine.h"

#ifdef ACL_API_GL4

#include <time.h>
#include "..\Core\Window.h"
#include "..\Gfx\FullscreenEffect.h"
#include "..\System\Exception.h"
#include "..\System\Log.h"
#include "..\Render\AxmLoader.h"
#include "..\Render\Context.h"
#include "..\Math\Vector.h"

#include "OpenGL4.h"
#include "Context.h"
#include "Gfx\TextureLoader.h"
#include "Gfx\MeshLoader.h"
#include "Gfx\EffectLoader.h"
#include "Gfx\MaterialLoader.h"
#include "Gfx\ModelLoader.h"
#include "Gfx\ZBufferLoader.h"
#include "Gfx\FontLoader.h"
#include "Gfx\GeometryCreator.h"
//#include "Sprite.h"
//#include "Line.h"
#include "Gfx\CbufferLoader.h"
#include "Gfx\SpriteBatch.h"
//#include "Gfx\Sprite.h"
//#include "Gfx\Text.h"
//#include "Gfx\Line.h"
#include "Gfx\RenderStateHandler.h"
#include "Render\Renderer.h"
#include "Render\Queue.h"

namespace acl
{
	namespace gl4
	{

		Engine::Engine(HINSTANCE hInstance) : BaseEngine(hInstance), m_pOpenGL(nullptr)
		{
			Queue::Init();
		}

		Engine::~Engine(void)
		{
			delete m_pOpenGL;
		}

		BaseEngine::VideoModeVector Engine::OnSetupAPI(void)
		{
			try
			{
				m_pOpenGL = new OpenGL4(m_pWindow->GethWnd());
			}
			catch (apiException&)
			{
				return BaseEngine::VideoModeVector();
			}

			auto& context = m_pOpenGL->GetContext();

			m_pRenderStateHandler = new RenderStateHandlerInstance;
			m_pCbufferLoader = new CbufferLoader();
			////setup renderer
			m_pRenderer = new Renderer(*m_pOpenGL, *m_pCbufferLoader);

			auto& sprite = m_pOpenGL->GetSprite();

			//// create resource loader
			m_pGeometryCreator = new GeometryCreator;
			m_pEffectLoader = new EffectLoader(m_effects);
			m_pTextureLoader = new TextureLoader(m_textures);
			m_pMeshLoader = new MeshLoader(m_textures, m_meshes, *m_pGeometryCreator);
			m_pMaterialLoader = new MaterialLoader(m_effects, m_textures, m_materials);
			m_pFontLoader = new FontLoader(sprite, m_fonts, *m_pTextureLoader);
			m_pModelLoader = new ModelLoader(m_materials, m_meshes, m_models);
			m_pZbufferLoader = new ZBufferLoader(m_zbuffers);

			//m_pSprite = new Sprite(sprite, nullptr);
			m_pSpriteBatch = new SpriteBatch(sprite, m_fonts, *m_pGeometryCreator);
			//m_pText = new Text(m_fonts, nullptr);
			//m_pLine = new Line(m_pDirectX11->GetLine());

			BaseEngine::VideoModeVector vVideoModes;

			for(auto& mode : m_pOpenGL->GetDeviceModes())
			{
				vVideoModes.emplace_back(math::Vector2(mode.dmPelsWidth, mode.dmPelsHeight), (float)mode.dmDisplayFrequency);
			}

			return vVideoModes;
		}

		bool Engine::OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreen)
		{
			auto& context = m_pOpenGL->GetContext();
			vOldSize = context.GetBackbufferSize();

			if(isFullscreen != context.IsFullscreen())
				context.SetFullscreen(isFullscreen);

			if(vOldSize != vSize)
			{
				context.Resize(vSize.x, vSize.y);
				m_pWindow->Resize(vSize.x, vSize.y);
				return true;
			}
			else
				return false;
		}

	}
}

#endif