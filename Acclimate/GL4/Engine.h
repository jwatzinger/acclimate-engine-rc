#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\BaseEngine.h"

namespace acl
{
	namespace gl4
	{
		class OpenGL4;

		class Engine : 
			public BaseEngine
		{
		public:
			Engine(HINSTANCE hInstance);
			~Engine(void);

		private:

			VideoModeVector OnSetupAPI(void) override;
			bool OnScreenResize(const math::Vector2& vSize, math::Vector2& vOldSize, bool isFullscreens
			) override;

			OpenGL4* m_pOpenGL;
		};

	}
}

#endif