﻿#include "Error.h"

#ifdef ACL_API_GL4

#include "..\System\Exception.h"
#include "..\System\Log.h"
#include "..\System\Assert.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		
		void checkOpenGLError(const char* stmt, const char* fname, int line)
		{
			GLenum err = glGetError();
			if(err != GL_NO_ERROR)
				throw apiException();
		}

		std::string severityToString(GLenum severity)
		{
			switch(severity)
			{
			case GL_DEBUG_SEVERITY_HIGH:
				return "HIGH";
			case GL_DEBUG_SEVERITY_MEDIUM:
				return "MEDIUM";
			case GL_DEBUG_SEVERITY_LOW:
				return "LOW";
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				return "INFO";
			default:
				ACL_ASSERT(false);
			}

			return "";
		}

		std::string sourceToString(GLenum source)
		{
			switch(source)
			{
			case GL_DEBUG_SOURCE_API:
				return "API";
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				return "WINDOW_SYSTEM";
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				return "SHADER_COMPILER";
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				return "THIRD_PARTY";
			case GL_DEBUG_SOURCE_APPLICATION:
				return "APPLICATION";
			case GL_DEBUG_SOURCE_OTHER:
				return "OTHER";
			default:
				ACL_ASSERT(false);
			}

			return "";
		}

		std::string typeToString(GLenum type)
		{
			switch(type)
			{
			case GL_DEBUG_TYPE_ERROR:
				return "ERROR";
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				return "DEPRECATED_BEHAVIOUR";
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				return "UNDEFINED_BEHAVIOUR";
			case GL_DEBUG_TYPE_PORTABILITY:
				return "PORTABILITY";
			case GL_DEBUG_TYPE_PERFORMANCE:
				return "PERFORMANCE";
			case GL_DEBUG_TYPE_MARKER:
				return "MARKER";
			case GL_DEBUG_TYPE_PUSH_GROUP:
				return "PUSH_GROUP";
			case GL_DEBUG_TYPE_POP_GROUP:
				return "POP_GROUP";
			case GL_DEBUG_TYPE_OTHER:
				return "OTHER";
			default:
				ACL_ASSERT(false);
			}

			return "";
		}

		void APIENTRY openGlErrorCallback(GLenum source​, GLenum type, GLuint id​, GLenum severity​, GLsizei length​, const GLchar* message​, void* userParam​)
		{
			const auto stSeverity = severityToString(severity​) + ",";
			const auto stSource = sourceToString(source​);
			const auto stType = typeToString(type) + ",";

			sys::LogType logType;
			if(type == GL_DEBUG_TYPE_ERROR)
				logType = sys::LogType::ERR;
			else
				logType = sys::LogType::WARNING;

			sys::log->Out(sys::LogModule::API, logType, "-", stType, stSeverity, message​, "source:", stSource, "id:", id​);
		}

	}
}

#endif