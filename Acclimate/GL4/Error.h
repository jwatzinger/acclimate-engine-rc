﻿#pragma once
#include <Windows.h>
#include "..\ApiDef.h"
#include "..\..\Extern\GLEW\glew.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{

		void checkOpenGLError(const char* stmt, const char* fname, int line);

#ifdef _DEBUG
#define GL_CHECK(stmt) { \
	stmt; \
	checkOpenGLError(#stmt, __FILE__, __LINE__); \
		}
#else
#define GL_CHECK(stmt) stmt
#endif

		void APIENTRY openGlErrorCallback(GLenum source​, GLenum type​, GLuint id​, GLenum severity​, GLsizei length​, const GLchar* message​, void* userParam​);

	}
}

#endif