#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <string>
#include "VertexArrayObject.h"
#include "..\Gfx\FontFile.h"
#include "..\Math\Vector2f.h"

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

	namespace math
	{
		struct Rect;
	}

	namespace gl4
	{
		namespace ogl
		{

			class Sprite;

			class Font
			{
			public:
				Font(Sprite& sprite, const gfx::FontFile& file);

				const VertexArrayObject& GetVertexArrayObject(void) const;
				const gfx::FontFile& GetFile(void) const;

				size_t DrawString(const math::Rect& rect, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color);
				
			private:

				VertexArrayObject m_vao;
				Sprite* m_pSprite;

				gfx::FontFile m_file;

			};

		}
	}
}

#endif