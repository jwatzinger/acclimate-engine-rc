#include "FrameBuffer.h"

#ifdef ACL_API_GL4

#include "Texture.h"
#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			FrameBuffer::FrameBuffer(void): m_frameBuffer(0)
			{
				GL_CHECK(glGenFramebuffers(1, &m_frameBuffer));

				GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer));

				GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
			}

			FrameBuffer::~FrameBuffer(void)
			{
				GL_CHECK(glDeleteFramebuffers(1, &m_frameBuffer));
			}

			bool FrameBuffer::IsComplete(void) const
			{
				GLenum value;
				GL_CHECK(value = glCheckFramebufferStatus(GL_FRAMEBUFFER));
				return value == GL_FRAMEBUFFER_COMPLETE;
			}

			void FrameBuffer::BindTexture(unsigned int slot, const Texture& texture)
			{
				texture.BindTarget(slot);

				m_textures.set(slot, true);
				UpdateDrawBuffers();
			}

			void FrameBuffer::UnbindTexture(unsigned int slot)
			{
				GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + slot, GL_TEXTURE_2D, 0, 0));

				m_textures.set(slot, false);
				UpdateDrawBuffers();
			}

			void FrameBuffer::UnbindDepthbuffer(void) const
			{
				GL_CHECK(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0));
			}

			void FrameBuffer::Bind(void) const
			{
				GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer));
			}

			void FrameBuffer::Unbind(void) const
			{
				GL_CHECK(glBindFramebuffer(GL_FRAMEBUFFER, 0));
			}

			void FrameBuffer::UpdateDrawBuffers(void) const
			{
				GLenum buffer[4];
				for(unsigned int i = 0; i < 4; i++)
				{
					if(m_textures.at(i))
						buffer[i] = GL_COLOR_ATTACHMENT0 + i;
					else
						buffer[i] = GL_NONE;
				}

				glDrawBuffers(4, buffer);
			}
		}
	}
}

#endif