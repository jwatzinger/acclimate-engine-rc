#pragma once
#include <bitset>
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class FrameBuffer
			{
			public:
				FrameBuffer(void);
				~FrameBuffer(void);

				bool IsComplete(void) const;

				void BindTexture(unsigned int slot, const Texture& texture);
				void UnbindTexture(unsigned int slot);
				void UnbindDepthbuffer(void) const;

				void Bind(void) const;
				void Unbind(void) const;

			private:

				void UpdateDrawBuffers(void) const;

				std::bitset<4> m_textures;
				unsigned int m_frameBuffer;
			};

		}
	}
}

#endif