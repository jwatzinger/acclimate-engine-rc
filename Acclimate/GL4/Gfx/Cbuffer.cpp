#include "Cbuffer.h"

#ifdef ACL_API_GL4

#include "..\UniformBufferObject.h"
#include "..\Render\States.h"
#include "..\..\Gfx\Defines.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace gl4
	{

		Cbuffer::Cbuffer(void) : BaseCbuffer()
		{
		}

		Cbuffer::Cbuffer(size_t size) : BaseCbuffer(size)
		{
		}

		Cbuffer::Cbuffer(AclCbuffer* pBuffer) : BaseCbuffer(pBuffer, pBuffer->GetSize())
		{
		}

		Cbuffer::Cbuffer(const Cbuffer& buffer) : BaseCbuffer(buffer)
		{
			if(buffer.m_pBuffer)
				m_pBuffer = new AclCbuffer(*buffer.m_pBuffer);
		}

		Cbuffer::~Cbuffer(void)
		{
			delete m_pBuffer;
		}

		gfx::ICbuffer& Cbuffer::Clone(void) const
		{
			return *new Cbuffer(*this);
		}

		bool Cbuffer::OnNewBufferIsValid(const AclCbuffer* pNewBuffer) const
		{
			return m_pBuffer->GetSize() <= pNewBuffer->GetSize();
		}

		void Cbuffer::OnOverwrite(float* pData, size_t size)
		{
			m_pBuffer->Overwrite(pData, size);
		}

		void Cbuffer::OnDeleteBuffer(const AclCbuffer* pBuffer) const
		{
			delete pBuffer;
		}

		void Cbuffer::Bind(render::StateGroup& states, gfx::CbufferType type, gfx::CbufferSlot slot) const
		{
			static_assert(gfx::CBUFFER_INSTANCE_ID == 0, "Instance cbuffer ID does not conform.");
			static_assert(gfx::CBUFFER_STAGE_ID == 1, "Stage cbuffer ID does not conform.");

			switch(type)
			{
			case gfx::CbufferType::VERTEX:
				switch(slot)
				{
				case gfx::CbufferSlot::INSTANCE:
					states.Add<UBufferV0>(*m_pBuffer);
					break;
				case gfx::CbufferSlot::STAGE:
					states.Add<UBufferV1>(*m_pBuffer);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			case gfx::CbufferType::PIXEL:
				switch(slot)
				{
				case gfx::CbufferSlot::INSTANCE:
					states.Add<UBufferP0>(*m_pBuffer);
					break;
				case gfx::CbufferSlot::STAGE:
					states.Add<UBufferP1>(*m_pBuffer);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			case gfx::CbufferType::GEOMETRY:
				switch(slot)
				{
				case gfx::CbufferSlot::INSTANCE:
					states.Add<UBufferG0>(*m_pBuffer);
					break;
				case gfx::CbufferSlot::STAGE:
					states.Add<UBufferG1>(*m_pBuffer);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			default:
				ACL_ASSERT(false);
			}
		}

	}
}

#endif
