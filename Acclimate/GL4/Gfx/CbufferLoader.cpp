#include "CbufferLoader.h"

#ifdef ACL_API_GL4

#include "Cbuffer.h"
#include "..\UniformBufferObject.h"

namespace acl
{
	namespace gl4
	{

		gfx::ICbuffer& CbufferLoader::OnCreateFromSize(size_t numBytes) const
		{
			auto pGl4Buffer = new ogl::UniformBufferObject(numBytes);

			return *new Cbuffer(pGl4Buffer);
		}

		gfx::ICbuffer& CbufferLoader::OnCreateFromBuffer(AclCbuffer& buffer) const
		{
			return *new Cbuffer(&buffer);
		}

	}
}

#endif