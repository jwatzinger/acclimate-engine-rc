#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseCbufferLoader.h"

namespace acl
{
	namespace gl4
	{

		class CbufferLoader final :
			public gfx::BaseCbufferLoader
		{

			gfx::ICbuffer& OnCreateFromSize(size_t numBytes) const override;
			gfx::ICbuffer& OnCreateFromBuffer(AclCbuffer& buffer) const override;
		};

	}
}

#endif