#include "Effect.h"

#ifdef ACL_API_GL4

#include "..\Effect.h"
#include "..\Render\States.h"
#include "..\..\System\Log.h"

namespace acl
{
	namespace gl4
	{

		unsigned int ToFilter(gfx::TextureFilter filter, gfx::MipFilter mip)
		{
			switch(mip)
			{
			case gfx::MipFilter::NONE:
				switch(filter)
				{
				case gfx::TextureFilter::POINT:
					return GL_NEAREST;
				default:
					return GL_LINEAR;
				}
			case gfx::MipFilter::POINT:
				switch(filter)
				{
				case gfx::TextureFilter::POINT:
					return GL_NEAREST_MIPMAP_NEAREST;
				default:
					return GL_LINEAR_MIPMAP_NEAREST;
				}
			case gfx::MipFilter::LINEAR:
				switch(filter)
				{
				case gfx::TextureFilter::POINT:
					return GL_NEAREST_MIPMAP_LINEAR;
				default:
					return GL_LINEAR_MIPMAP_LINEAR;
				}
			}

			return GL_NEAREST;
		}

		unsigned int ToWrap(gfx::AdressMode mode)
		{
			switch(mode)
			{
			case gfx::AdressMode::WRAP:
				return GL_REPEAT;
			case gfx::AdressMode::MIRROR:
				return GL_MIRRORED_REPEAT;
			case gfx::AdressMode::CLAMP:
				return GL_CLAMP;
			case gfx::AdressMode::BORDER:
				return  GL_CLAMP_TO_BORDER;
			default:
				return GL_REPEAT;
			}
		}

		unsigned int ToBlend(gfx::BlendMode mode)
		{
			switch(mode)
			{
			case gfx::BlendMode::ONE:
				return GL_ONE;
			case gfx::BlendMode::SRCALPHA:
				return GL_SRC_ALPHA;
			case gfx::BlendMode::INV_SCRALPHA:
				return GL_ONE_MINUS_SRC_ALPHA;
			case gfx::BlendMode::COLOR:
				return GL_SRC_COLOR;
			case gfx::BlendMode::INV_COLOR:
				return GL_ONE_MINUS_SRC_COLOR;
			default:
				return GL_ZERO;
			}
		}

		unsigned int ToEquation(gfx::BlendFunc func)
		{
			switch(func)
			{
			case gfx::BlendFunc::ADD:
				return GL_FUNC_ADD;
			case gfx::BlendFunc::SUB:
				return GL_FUNC_SUBTRACT;
			default:
				return GL_FUNC_ADD;
			}
		}

		unsigned int ToCull(gfx::CullMode cull)
		{
			// culling is inverted due to native directx-order
			switch(cull)
			{
			case gfx::CullMode::NONE:
				return GL_NONE;
			case gfx::CullMode::BACK:
				return GL_FRONT;
			case gfx::CullMode::FRONT:
				return GL_BACK;
			default:
				return GL_NONE;
			}
		}

		unsigned int ToFill(gfx::FillMode fill)
		{
			switch(fill)
			{
			case gfx::FillMode::SOLID:
				return GL_FILL;
			case gfx::FillMode::WIREFRAME:
				return GL_LINE;
			default:
				return GL_FILL;
			}
		}

		void deleteEffect(AclEffect* pEffect)
		{
			delete pEffect;
		}

		Effect::Effect(gfx::EffectFile& file, const gfx::IEffectLoader& loader) : BaseEffect(file, loader, deleteEffect)
		{
			for(int i = 0; i < 5; i++)
			{
				m_sampler[i] = nullptr;
			}
		}

		Effect::~Effect(void)
		{
			for(int i = 0; i < 5; i++)
			{
				delete m_sampler[i];
			}
		}

		void Effect::SetAlphaState(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend)
		{
			m_states.Add<SetBlending>(bEnable, ToBlend(srcBlend), ToBlend(destBlend), ToEquation(blendFunc));
		}

		void Effect::SetDepthState(bool bEnable, bool bWrite)
		{
			m_states.Add<DepthState>(bEnable, bWrite);
		}

		void Effect::SetRasterizerState(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable)
		{
			m_states.Add<RasterizerState>(ToCull(cull), ToFill(fill));
		}

		ogl::Sampler& Effect::ProvideSampler(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			if(m_sampler[index])
				delete m_sampler[index];

			float anisotropy = 1.0f;
			if(minFilter == gfx::TextureFilter::ANISOTROPIC && magFilter == gfx::TextureFilter::ANISOTROPIC)
				anisotropy = 16.0f;

			m_sampler[index] = new ogl::Sampler(ToFilter(minFilter, mipFilter), ToFilter(magFilter, gfx::MipFilter::NONE), ToWrap(u), ToWrap(v), anisotropy);

			return *m_sampler[index];
		}

		void Effect::OnSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			auto& sampler = ProvideSampler(index, minFilter, magFilter, mipFilter, u, v);

			switch(index)
			{
			case 0:
				m_states.Add<SamplerState0>(sampler);
				break;
			case 1:
				m_states.Add<SamplerState1>(sampler);
				break;
			case 2:
				m_states.Add<SamplerState2>(sampler);
				break;
			case 3:
				m_states.Add<SamplerState3>(sampler);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void Effect::OnVertexSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v)
		{
			auto& sampler = ProvideSampler(index + gfx::MAX_PIXEL_TEXTURES, minFilter, magFilter, mipFilter, u, v);

			switch(index)
			{
			case 0:
				m_states.Add<VertexSamplerState0>(sampler);
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		AclCbuffer* Effect::OnCreateVCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.CloneVertexUBO(id);
		}

		AclCbuffer* Effect::OnCreatePCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.CloneFragmentUBO(id);
		}

		AclCbuffer* Effect::OnCreateGCbuffer(AclEffect& effect, unsigned int id) const
		{
			return effect.CloneGeometryUBO(id);
		}

		void Effect::OnBindShader(render::StateGroup& group, AclEffect& effect) const
		{
			group.Add<BindProgram>(effect);
		}

		void Effect::OnDeleteEffect(AclEffect& effect)
		{
			delete &effect;
		}

	}
}

#endif