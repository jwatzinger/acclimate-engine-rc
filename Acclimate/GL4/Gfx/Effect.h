#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\Defines.h"
#include "..\..\Gfx\BaseEffect.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			class Sampler;
		}

		class Effect final :
			public gfx::BaseEffect
		{
		public:
			Effect(gfx::EffectFile& file, const gfx::IEffectLoader& loader);
			~Effect(void);

			void SetAlphaState(bool bEnable, gfx::BlendMode srcBlend, gfx::BlendMode destBlend, gfx::BlendFunc blendFunc, gfx::BlendMode alphaSrcBlend, gfx::BlendMode alphaDestBlend) override;
			void SetDepthState(bool bEnable, bool bWrite) override;
			void SetRasterizerState(gfx::CullMode cull, gfx::FillMode fill, bool bScissorEnable) override;

		private:

			void OnSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v) override;
			void OnVertexSamplerState(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v) override;
			void OnBindShader(render::StateGroup& group, AclEffect& effect) const override;
			void OnDeleteEffect(AclEffect& effect) override;
			AclCbuffer* OnCreateVCbuffer(AclEffect& effect, unsigned int id) const override;
			AclCbuffer* OnCreatePCbuffer(AclEffect& effect, unsigned int id) const override;
			AclCbuffer* OnCreateGCbuffer(AclEffect& effect, unsigned int id) const override;

			ogl::Sampler& ProvideSampler(unsigned int index, gfx::TextureFilter minFilter, gfx::TextureFilter magFilter, gfx::MipFilter mipFilter, gfx::AdressMode u, gfx::AdressMode v);

			ogl::Sampler* m_sampler[gfx::MAX_VERTEX_TEXTURES + gfx::MAX_PIXEL_TEXTURES];
		};

	}
}

#endif