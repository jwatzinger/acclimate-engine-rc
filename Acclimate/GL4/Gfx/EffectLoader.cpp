#include "EffectLoader.h"

#ifdef ACL_API_GL4

#include "Effect.h"
#include "..\Effect.h"
#include "..\..\Gfx\EffectFile.h"
#include "..\..\System\Convert.h"

namespace acl
{
	namespace gl4
	{

		EffectLoader::EffectLoader(gfx::Effects& effects) : BaseEffectLoader(effects, m_parser)
		{
		}

		gfx::IEffect& EffectLoader::OnCreateEffect(gfx::EffectFile& file) const
		{
			return *new Effect(file, *this);
		}

		AclEffect& EffectLoader::OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const
		{
			// macro permutations
			const gfx::EffectFile::PermutationVector vPermutationNames = file.GetPermutations(permutation);

			ogl::Effect::MacroVector vMacros;
			for(auto& macro : vPermutationNames)
			{
				vMacros.push_back("#define " + macro.stName + ' ' + conv::ToStringA(macro.value).c_str() + '\n');
			}

			vMacros.push_back("#define API_GL4\n");

			const std::string stTemp(file.GetData(), file.GetDataLength());

			bool hasGeometryShader = false;
			if(file.GetShaderTypes() == gfx::ShaderTypes::VERTEX_PIXEL_GEOMETRY)
				hasGeometryShader = true;

			return *new AclEffect(stTemp, vMacros, hasGeometryShader);
		}

	}
}

#endif