#pragma once
#include "..\..\Gfx\BaseEffectLoader.h"

#ifdef ACL_API_GL4

#include "GLSLParser.h"

namespace acl
{
	namespace gl4
	{

		class EffectLoader final :
			public gfx::BaseEffectLoader
		{
		public:
			EffectLoader(gfx::Effects& effects);

		private:

			gfx::IEffect& OnCreateEffect(gfx::EffectFile& file) const override;
			AclEffect& OnCreatePermutation(const gfx::EffectFile& file, unsigned int permutation) const override;

			GLSLParser m_parser;
		};

	}
}

#endif