#include "Font.h"

#ifdef ACL_API_GL4

#include "..\Font.h"
#include "..\Render\States.h"
#include "..\..\Gfx\ITexture.h"
#include "..\..\Gfx\TextureInternalAccessor.h"

namespace acl
{
    namespace gl4
    {

		Font::Font(AclFont& font, const gfx::ITexture& texture) : m_pFont(&font), m_pTexture(&texture)
        {
			m_states.Add<BindVAO>(&m_pFont->GetVertexArrayObject());
			m_states.Add<BindTexture0>(&gfx::getTexture(texture));
        }

        Font::~Font(void)
        {
            delete m_pFont;
        }

		size_t Font::DrawString(const math::Rect& rect, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color) const
		{
			return m_pFont->DrawString(rect, stText, dFlags, color);
		}

		const render::StateGroup& Font::GetState(void) const
		{
			return m_states;
		}

		const gfx::ITexture& Font::GetTexture(void) const
		{
			return *m_pTexture;
		}

		const gfx::FontFile& Font::GetFontFile(void) const
		{
			return m_pFont->GetFile();
		}

    }
}

#endif