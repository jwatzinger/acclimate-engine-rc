#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\IFont.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

    namespace gl4
    {

        class Font :
			public gfx::IFont
        {
        public:
            Font(AclFont& font, const gfx::ITexture& texture);
            ~Font(void);

			size_t DrawString(const math::Rect& rect, const std::wstring& stText, unsigned int dFlags, const gfx::Color& color) const override;
			const gfx::ITexture& GetTexture(void) const override;
			const gfx::FontFile& GetFontFile(void) const override;
			const render::StateGroup& GetState(void) const override;

        private:

			const gfx::ITexture* m_pTexture;

            AclFont* m_pFont;

			render::StateGroup m_states;
        };

    }
}

#endif