#include "FontLoader.h"

#ifdef ACL_API_GL4

#include "Font.h"
#include "..\Font.h"

namespace acl
{
    namespace gl4
    {

		FontLoader::FontLoader(AclSprite& sprite, gfx::Fonts& fonts, gfx::ITextureLoader& textureLoader) : BaseFontLoader(fonts, textureLoader),
			m_pSprite(&sprite)
        {
        }

		gfx::IFont& FontLoader::OnCreateFont(gfx::FontFile& file, gfx::ITexture& texture) const
		{
			AclFont& gl4Font = *new AclFont(*m_pSprite, file);
			return *new Font(gl4Font, texture);
		}

    }
}

#endif

