#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseFontLoader.h"

namespace acl
{
    namespace gl4
    {

        class FontLoader final :
			public gfx::BaseFontLoader
        {
        public:
            FontLoader(AclSprite& sprite, gfx::Fonts& fonts, gfx::ITextureLoader& textureLoader);

        private:

			gfx::IFont& OnCreateFont(gfx::FontFile& file, gfx::ITexture& texture) const override;

            AclSprite* m_pSprite;
        };

    }
}

#endif