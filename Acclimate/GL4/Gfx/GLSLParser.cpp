#include "GLSLParser.h"

#ifdef ACL_API_GL4

#include <vector>
#include "..\..\System\Convert.h"
#include "..\..\System\Assert.h"

namespace acl
{
	namespace gl4
	{

		void GLSLParser::OnVertexBegin(std::string& stOut) const
		{
		}

		void GLSLParser::OnPixelBegin(std::string& stOut) const
		{
			stOut += "// frag_begin\n\n";
		}

		void GLSLParser::OnBeginInputBlock(std::string& stOut, gfx::InputRegister reg, gfx::ShaderType type) const
		{
			stOut += "layout(column_major) uniform ";

			switch(reg)
			{
			case gfx::InputRegister::INSTANCE:
				stOut += "Instance";
				break;
			case gfx::InputRegister::STAGE:
				stOut += "Stage";
				break;
			default:
				ACL_ASSERT(false);
			}

			switch(type)
			{
			case gfx::ShaderType::VERTEX:
				stOut += "V";
				break;
			case gfx::ShaderType::PIXEL:
				stOut += "F";
				break;
			case gfx::ShaderType::GEOMETRY:
				stOut += "G";
				break;
			default:
				ACL_ASSERT(false);
			}
			
			stOut += "\n{\n";
		}

		void GLSLParser::OnInputVariable(std::string& stOut, const std::string& stType, const std::string& stName, gfx::InputRegister reg, unsigned int id) const
		{
			stOut += stType;
			stOut.push_back(' ');
			stOut += stName;
		}

		void GLSLParser::OnEndInputBlock(std::string& stOut) const
		{
			stOut += "};\n";
		}

		void GLSLParser::OnBeginIO(std::string& stOut, gfx::InOut io) const
		{
			switch(io)
			{
			case gfx::InOut::I:
				stOut += "//in_block_begin\n";
				break;
			case gfx::InOut::O:
				stOut += "//out_block_begin\n";
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void GLSLParser::OnBeginPixelOut(std::string& stOut) const
		{
		}

		void GLSLParser::OnIOVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id, gfx::InOut io) const
		{
			if(io == gfx::InOut::O && id == 0)
				stOut += "#define out" + stName + " gl_Position";
			else
			{
				std::string stFullName = stName;
				if(io == gfx::InOut::I)
				{
					stOut += "layout(location = " + conv::ToA(conv::ToString(id)) + ") in ";
					stFullName.insert(0, "in");
				}
				else
				{
					stOut += "layout(location = " + conv::ToA(conv::ToString(id-1)) + ") out ";
					stFullName.insert(0, "out");
				}
				if(stType == "uint4")
					stOut += "vec4";
				else
					stOut += stType;

				stOut.push_back(' ');
				stOut += stFullName;
				stOut.push_back(';');
			}
		}

		void GLSLParser::OnPixelOutVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id) const
		{
			stOut += "layout(location = " + conv::ToA(conv::ToString(id)) + ") out " + stType;
			stOut.push_back(' ');
			stOut += "out" + stName + ';';
		}

		void GLSLParser::OnEndIO(std::string& stOut) const
		{
			stOut += "//inout_block_end\n";
		}

		void GLSLParser::OnTexture(std::string& stOut, gfx::TextureType& type, const std::string& stName, unsigned int id) const
		{
			stOut += "uniform ";

			switch(type)
			{
			case gfx::TextureType::_1D:
				stOut += "sampler1D ";
				break;
			case gfx::TextureType::_2D:
				stOut += "sampler2D ";
				break;
			case gfx::TextureType::_3D:
				stOut += "sampler3D ";
				break;
			}

			stOut += stName;
		}

		std::string GLSLParser::OnFunctionCall(const std::string& stName, const ArgumentVector& vArguments) const
		{
			if(stName == "mul")
			{
				ACL_ASSERT(vArguments.size() == 2);

				return "(" + vArguments[1] + "*" + vArguments[0] + ")";
			}
			else
			{
				std::string stFunction;
				if(stName == "Sample")
				{
					ACL_ASSERT(vArguments.size() == 2);
					stFunction += "texture(" + vArguments[0] + ", float2((" + vArguments[1] + ").x" + " + (" + vArguments[1] +").x, 1.0f) - (" + vArguments[1] + "))";
					return stFunction;
				}
				else if(stName == "SampleLOD")
				{
					ACL_ASSERT(vArguments.size() == 3);
					stFunction += "textureLod(" + vArguments[0] + ", float2((" + vArguments[1] + ").x" + " + (" + vArguments[1] + ").x, 1.0f) - (" + vArguments[1] + "), " + vArguments[2] + ")";
					return stFunction;
				}
				else if(stName == "clip")
				{
					ACL_ASSERT(vArguments.size() == 1);
					stFunction += "if(" + vArguments[0] + ")\n\tdiscard;\n";
					return stFunction;
				}
				else if(stName == "saturate")
				{
					ACL_ASSERT(vArguments.size() == 1);
					stFunction = "clamp(" + vArguments[0] + ", 0.0f, 1.0f)";
					return stFunction;
				}
				else if(stName == "lerp")
				{
					ACL_ASSERT(vArguments.size() == 3);
					stFunction = "mix(";
				}
				else if(stName == "frac")
				{
					ACL_ASSERT(vArguments.size() == 1);
					stFunction = "fract(";
				}
				else if(stName == "Append")
				{
					ACL_ASSERT(vArguments.size() == 0);
					stFunction = "EmitVertex(";
				}
				else if(stName == "Restart")
				{
					ACL_ASSERT(vArguments.size() == 0);
					stFunction = "EndPrimitive(";
				}
				else if(stName == "rsqrt")
				{
					ACL_ASSERT(vArguments.size() == 1);
					stFunction = "inversesqrt(";
				}
				else
				{
					auto pos = stName.find("extentions.");
					if(pos != std::string::npos)
					{
						const std::string stExtention = stName.substr(pos + 11);
						stFunction = stExtention + '(';
					}
					else
						stFunction = stName + '(';
				}

				for(auto& stArgument : vArguments)
				{
					stFunction += stArgument + ",";
				}

				if(auto size = vArguments.size())
					stFunction.pop_back();

				stFunction += ")";
				return stFunction;
			}
		}

		std::string GLSLParser::OnConvertType(gfx::Type type) const
		{
			switch(type)
			{
			case gfx::Type::FLOAT3X3:
				return "mat3";
			case gfx::Type::FLOAT:
				return "vec";
			case gfx::Type::MATRIX:
				return "mat4";
			case gfx::Type::OUT_:
				return "out";
			case gfx::Type::IN_:
				return "in";
			case gfx::Type::UINT:
				return "uvec";
			default:
				ACL_ASSERT(false);
			}

			return "";
		}

		void GLSLParser::OnExtention(std::string& stOut, const std::string& stFunction, const std::string& stName, unsigned int id) const
		{
			stOut += "// extention_entry\n";

			stOut += "#ifndef OVERWRITE_" + stName + "_EXT\n";

			stOut += stFunction + '\n';

			stOut += "#endif";
		}

		void GLSLParser::OnExtentionBegin(std::string& stOut, const std::string& stName, const std::string& stParent) const
		{
			stOut += "#define OVERWRITE_" + stParent + "_EXT\n";
		}

		void GLSLParser::OnExtentionEnd(std::string& stOut, const std::string& stName) const
		{
		}

		void GLSLParser::OnMainDeclaration(std::string& stOut) const
		{
			stOut += "void main()";
		}

		void GLSLParser::OnMainTop(std::string& stOut) const
		{
		}

		void GLSLParser::OnMainBottom(std::string& stOut) const
		{
		}

		void GLSLParser::OnPixelMainDeclaration(std::string& stOut) const
		{
			stOut += "void main()";
		}

		void GLSLParser::OnPixelMainTop(std::string& stOut) const
		{
		}

		void GLSLParser::OnPixelMainBottom(std::string& stOut) const
		{
		}

		void GLSLParser::OnPostProcess(std::string& stOut) const
		{
			size_t fragPos = stOut.find("// frag_begin\n\n");
			ACL_ASSERT(fragPos != std::string::npos);

			// get vertex output block
			const size_t outPosBegin = stOut.find("//out_block_begin\n");
			ACL_ASSERT(outPosBegin < fragPos);
			const size_t outPosEnd = stOut.find("//inout_block_end\n", outPosBegin);
			ACL_ASSERT(outPosEnd < fragPos);

			std::string stBlock = stOut.substr(outPosBegin, outPosEnd - outPosBegin);

			// insert into geometry as output
			size_t geoPos = stOut.find("// geo_begin\n\n");
			const size_t geoOutPos = stOut.find("//geo_out_block_begin\n", geoPos);
			if(geoPos != std::string::npos && geoOutPos == std::string::npos)
			{
				stOut.insert(geoPos + 14, stBlock);
				fragPos = stOut.find("// frag_begin\n\n");
			}

			// modify to input
			size_t outPos = stBlock.find(" out");
			while(outPos != std::string::npos)
			{
				stBlock.replace(outPos, 4, " in");
				outPos = stBlock.find(" out");
			}

			if(geoOutPos != std::string::npos)
			{
				// get vertex output block
				ACL_ASSERT(geoOutPos < fragPos);
				const size_t geoOutPosEnd = stOut.find("//inout_block_end\n", geoOutPos);
				ACL_ASSERT(geoOutPosEnd < fragPos);

				std::string stGeoBlock = stOut.substr(geoOutPos, geoOutPosEnd - geoOutPos);

				// modify to input
				size_t outPos = stGeoBlock.find(" out");
				while(outPos != std::string::npos)
				{
					stGeoBlock.replace(outPos, 4, " in");
					outPos = stGeoBlock.find(" out");
				}

				stOut.insert(fragPos + 15, stGeoBlock);
			}
			else
				stOut.insert(fragPos + 15, stBlock);

			// modify to geometry
			geoPos = stOut.find("// geo_begin\n\n");
			if(geoPos != std::string::npos)
			{
				fragPos = stOut.find("// frag_begin\n\n");

				size_t semicolonPos = stBlock.find(';');
				while(semicolonPos != std::string::npos)
				{
					stBlock.replace(semicolonPos, 1, "[];");
					semicolonPos = stBlock.find(';', semicolonPos+4);
				}

				// replace in[]
				size_t inLeftPos = stOut.find("in[", geoPos);
				while(inLeftPos != std::string::npos)
				{
					const size_t inRightPos = stOut.find("].", inLeftPos);
					ACL_ASSERT(inRightPos != std::string::npos);

					const std::string stValue = stOut.substr(inLeftPos + 3, inRightPos - inLeftPos - 3);
					stOut.erase(inLeftPos + 2, inRightPos - inLeftPos);

					const size_t toInsert = stOut.find_first_of(";,\n \t.)", inLeftPos);
					stOut.insert(toInsert, "[" + stValue + "]");

					inLeftPos = stOut.find("in[", inLeftPos);
				}

				// replace in-position
				const size_t definePos = stBlock.find("#define ");
				ACL_ASSERT(definePos != std::string::npos);
				const size_t pos = stBlock.find(" gl_Position\n", definePos);
				ACL_ASSERT(pos != std::string::npos);

				const std::string stInputPos = stBlock.substr(definePos + 8, pos - (definePos + 8));

				size_t glPos = stOut.find(stInputPos + '[', geoPos);
				while(glPos != std::string::npos)
				{
					const size_t bracketPos = glPos + stInputPos.size();
					const size_t rightPos = stOut.find(']', bracketPos);
					const std::string stValue = stOut.substr(bracketPos + 1, rightPos - (bracketPos + 1));

					stOut.erase(bracketPos, rightPos - bracketPos + 1);
					stOut.replace(glPos, stInputPos.size(), "gl_in[" + stValue + "].gl_Position");

					glPos = stOut.find(stInputPos + '[', geoPos);
				}

				stOut.insert(geoPos + 14, stBlock);
			}

			
			size_t outDotPos = stOut.find("out.");
			while(outDotPos != std::string::npos)
			{
				stOut.erase(outDotPos + 3, 1);
				outDotPos = stOut.find("out.");
			}

			size_t inDotPos = stOut.find("in.");
			while(inDotPos != std::string::npos)
			{
				stOut.erase(inDotPos + 2, 1);
				inDotPos = stOut.find("in.");
			}
		}


		void GLSLParser::OnGeometryBegin(std::string& stOut) const
		{
			stOut += "// geo_begin\n\n";
		}

		void GLSLParser::OnGeometryPrimitiveIn(gfx::GeometryPrimitiveType type, unsigned int numPrimitives, std::string& stOut) const
		{
			std::string stType;
			switch(type)
			{
			case gfx::GeometryPrimitiveType::TRIANGLE:
				stType = "triangles";
				break;
			case gfx::GeometryPrimitiveType::LINE:
				stType = "lines";
				break;
			case gfx::GeometryPrimitiveType::POINT:
				stType = "points";
				break;
			default:
				ACL_ASSERT(false);
			}

			stOut += "layout (" + stType + ") in;\n";
		}

		void GLSLParser::OnGeometryPrimitiveOut(gfx::GeometryPrimitiveType type, const std::string& stNumPrimitives, std::string& stOut) const
		{
			std::string stPrimitive;
			switch(type)
			{
			case gfx::GeometryPrimitiveType::TRIANGLE:
				stPrimitive = "triangle_strip";
				break;
			case gfx::GeometryPrimitiveType::LINE:
				stPrimitive = "line_strip";
				break;
			case gfx::GeometryPrimitiveType::POINT:
				stPrimitive = "points";
				break;
			default:
				ACL_ASSERT(false);
			}

			stOut += "layout (" + stPrimitive + ", max_vertices = " + stNumPrimitives + ") out;\n";
		}

		void GLSLParser::OnGeometryMainDeclaration(std::string& stOut) const
		{
			stOut += "void main()";
		}

		void GLSLParser::OnGeometryMainTop(std::string& stOut) const
		{
		}

		void GLSLParser::OnBeginGeometryOut(std::string& stOut) const
		{
			stOut += "//geo_out_block_begin\n";
		}

	}
}

#endif
