#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\IEffectLanguageParser.h"

namespace acl
{
	namespace gl4
	{
		class GLSLParser :
			public gfx::IEffectLanguageParser
		{
		public:

			void OnVertexBegin(std::string& stOut) const override;
			void OnPixelBegin(std::string& stOut) const override;

			void OnBeginInputBlock(std::string& stOut, gfx::InputRegister reg, gfx::ShaderType type) const override;
			void OnInputVariable(std::string& stOut, const std::string& stType, const std::string& stName, gfx::InputRegister reg, unsigned int id) const override;
			void OnEndInputBlock(std::string& stOut) const override;

			void OnBeginIO(std::string& stOut, gfx::InOut io) const override;
			void OnBeginPixelOut(std::string& stOut) const override;
			void OnIOVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id, gfx::InOut io) const override;
			void OnPixelOutVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id) const override;
			void OnEndIO(std::string& stOut) const override;

			void OnTexture(std::string& stOut, gfx::TextureType& type, const std::string& stName, unsigned int id) const override;

			std::string OnFunctionCall(const std::string& stName, const ArgumentVector& vArguments) const override;
			std::string OnConvertType(gfx::Type type) const override;

			void OnExtention(std::string& stOut, const std::string& stFunction, const std::string& stName, unsigned int id) const override;
			void OnExtentionBegin(std::string& stOut, const std::string& stName, const std::string& stParent) const override;
			void OnExtentionEnd(std::string& stOut, const std::string& stName) const override;

			void OnMainDeclaration(std::string& stOut) const override;
			void OnMainTop(std::string& stOut) const override;
			void OnMainBottom(std::string& stOut) const override;
			void OnPixelMainDeclaration(std::string& stOut) const override;
			void OnPixelMainTop(std::string& stOut) const override;
			void OnPixelMainBottom(std::string& stOut) const override;

			void OnPostProcess(std::string& stOut) const override;

			/***********************************
			* GeometryShader
			************************************/

			void OnGeometryBegin(std::string& stOut) const override;
			void OnGeometryPrimitiveIn(gfx::GeometryPrimitiveType type, unsigned int numPrimitives, std::string& stOut) const override;
			void OnGeometryPrimitiveOut(gfx::GeometryPrimitiveType type, const std::string& stNumPrimitves, std::string& stOut) const override;
			void OnGeometryMainDeclaration(std::string& stOut) const override;
			void OnGeometryMainTop(std::string& stOut) const override;
			void OnBeginGeometryOut(std::string& stOut) const override;
		};
	}
}

#endif