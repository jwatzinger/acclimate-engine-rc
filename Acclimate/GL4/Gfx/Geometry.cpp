#include "Geometry.h"

#ifdef ACL_API_GL4

#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace gl4
	{

		Geometry::Geometry(gfx::PrimitiveType type, const AttributeVector& vAttributes) : 
			BaseGeometry(type, vAttributes)
		{
		}

		gfx::IGeometry& Geometry::Clone(void) const
		{
			return *new Geometry(*this);
		}

		void Geometry::Bind(render::StateGroup& states) const
		{
			states.Add<BindVAO>(&m_vao);
			m_vao.Bind();
		}

		void Geometry::Unbind(void) const
		{
			m_vao.Unbind();
		}

	}
}



#endif