#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\VertexArrayObject.h"
#include "..\..\Gfx\BaseGeometry.h"

namespace acl
{
	namespace gl4
	{

		class Geometry :
			public gfx::BaseGeometry
		{
		public:
			Geometry(gfx::PrimitiveType type, const AttributeVector& vAttributes);

			gfx::IGeometry& Clone(void) const override;

			void Bind(render::StateGroup& states) const override;
			void Unbind(void) const;

		private:

			ogl::VertexArrayObject m_vao;
		};

	}
}

#endif