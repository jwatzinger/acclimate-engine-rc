#include "GeometryCreator.h"

#ifdef ACL_API_GL4

#include "Geometry.h"

namespace acl
{
	namespace gl4
	{

		gfx::IGeometry& GeometryCreator::CreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes) const
		{
			return *new Geometry(type, vAttributes);
		}

	}
}

#endif