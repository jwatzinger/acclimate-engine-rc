#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\IGeometryCreator.h"

namespace acl
{
	namespace gl4
	{

		class GeometryCreator :
			public gfx::IGeometryCreator
		{
		public:

			gfx::IGeometry& CreateGeometry(gfx::PrimitiveType type, const gfx::IGeometry::AttributeVector& vAttributes) const override;
		};

	}
}

#endif