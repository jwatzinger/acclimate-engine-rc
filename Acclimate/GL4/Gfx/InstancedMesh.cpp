#include "InstancedMesh.h"

#ifdef ACL_API_GL4

#include "MeshBuffer.h"
#include "Geometry.h"
#include "..\Render\States.h"

namespace acl
{
    namespace gl4
    {

		ogl::VertexType toVertexInstance(gfx::PrimitiveType primitive)
		{
			switch(primitive)
			{
			case gfx::PrimitiveType::TRIANGLE:
				return ogl::VertexType::TRIANGLE;
			case gfx::PrimitiveType::LINE:
				return ogl::VertexType::LINE;
			case gfx::PrimitiveType::TRIANGLE_STRIP:
				return ogl::VertexType::TRIANGLE_STRIP;
			case gfx::PrimitiveType::POINT:
				return ogl::VertexType::POINT;
			default: 
				ACL_ASSERT(false);
			}

			return ogl::VertexType::TRIANGLE;
		}

		InstancedMesh::InstancedMesh(gfx::IVertexBuffer& buffer, size_t numInstances, IMesh& mesh, const Geometry& geometry) : 
			BaseInstancedMesh(buffer, numInstances, mesh, geometry)
        {
			auto& state = GetOwnStateGroup();

			mesh.GetVertexBuffer().Bind(state, gfx::BindTarget::VERTEX, 0);
			mesh.GetIndexBuffer().Bind(state);
			geometry.Unbind();

			OnChangeInstanceCount(numInstances);
        }

		InstancedMesh::InstancedMesh(const InstancedMesh& mesh): 
			BaseInstancedMesh(mesh) 
		{
			auto& state = GetOwnStateGroup();
			auto& ownMesh = GetMeshToInstance();

			ownMesh.GetVertexBuffer().Bind(state, gfx::BindTarget::VERTEX, 0);
			ownMesh.GetIndexBuffer().Bind(state);
			Geometry& geometry = (Geometry&)(GetGeometry());
			geometry.Unbind();

			OnChangeInstanceCount(GetInstanceCount());
		}

        InstancedMesh::~InstancedMesh(void)
        {
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				RemoveDrawCall(i);
			}
        }

		gfx::IInstancedMesh& InstancedMesh::Clone(void) const
		{
			return *new InstancedMesh(*this);
		}

		void InstancedMesh::OnChangeInstanceCount(unsigned int count)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				unsigned int vertexCount = GetVertexCount(i);
				SetDrawCall(i, *new const DrawInstanced(toVertexInstance(GetGeometry().GetPrimitiveType()), count, vertexCount, indexOffset));
				indexOffset += vertexCount;
			}
		}

		void InstancedMesh::OnChangeInstanceBuffer(void)
		{

		}

    }
}

#endif