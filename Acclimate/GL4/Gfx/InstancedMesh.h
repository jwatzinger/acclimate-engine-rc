#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseInstancedMesh.h"

namespace acl
{
	namespace gl4
	{

		class Geometry;

		class InstancedMesh : 
			public gfx::BaseInstancedMesh
		{
		public:
			InstancedMesh(gfx::IVertexBuffer& buffer, size_t numInstances, gfx::IMesh& mesh, const Geometry& geometry);
			InstancedMesh(const InstancedMesh& instancedMesh);
			~InstancedMesh(void);

			gfx::IInstancedMesh& Clone(void) const override;

		private:

			void OnChangeInstanceCount(unsigned int count) override;
			void OnChangeInstanceBuffer(void) override;
		};
	}
}

#endif