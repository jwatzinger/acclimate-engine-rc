#include "Material.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{

		Material::Material(gfx::IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets) : BaseMaterial(effect, permutation, id, numSubsets)
		{
		}

	}
}

#endif