#include "Mesh.h"

#ifdef ACL_API_GL4

#include "MeshBuffer.h"
#include "Geometry.h"
#include "..\Render\States.h"
#include "..\..\System\Assert.h"

namespace acl
{
    namespace gl4
    {

		ogl::VertexType toVertex(gfx::PrimitiveType primitive)
		{
			switch(primitive)
			{
			case gfx::PrimitiveType::TRIANGLE:
				return ogl::VertexType::TRIANGLE;
			case gfx::PrimitiveType::LINE:
				return ogl::VertexType::LINE;
			case gfx::PrimitiveType::TRIANGLE_STRIP:
				return ogl::VertexType::TRIANGLE_STRIP;
			case gfx::PrimitiveType::POINT:
				return ogl::VertexType::POINT;
			default:
				ACL_ASSERT(false);
			}

			return ogl::VertexType::TRIANGLE;
		}

		Mesh::Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, const SubsetVector& vSubsets, const Geometry& geometry, gfx::MeshSkeleton* pSkeleton) :
			BaseMesh(vertexBuffer, indexBuffer, vSubsets, 1, geometry, pSkeleton)
        {
			geometry.Unbind();

			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < GetNumSubsets(); i++)
			{
				unsigned int vertexCount = GetVertexCount(i);
				SetDrawCall(i, *new const DrawElements(toVertex(geometry.GetPrimitiveType()), vertexCount, indexOffset));
				indexOffset += vertexCount;
			}
        }

		void Mesh::OnChangeVertexCount(unsigned int subset)
		{
			unsigned int indexOffset = 0;
			for(unsigned int i = 0; i < subset; i++)
			{
				indexOffset += GetVertexCount(i);
			}

			RemoveDrawCall(subset, true);

			unsigned int vertexCount = GetVertexCount(subset);
			SetDrawCall(subset, *new const DrawElements(toVertex(GetGeometry().GetPrimitiveType()), vertexCount, indexOffset));
			indexOffset += vertexCount;
		}

    }
}

#endif