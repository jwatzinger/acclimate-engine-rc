#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseMesh.h"

namespace acl
{
	namespace gl4
	{

		class Geometry;

		class Mesh : 
			public gfx::BaseMesh
		{
		public:
			Mesh(gfx::IVertexBuffer& vertexBuffer, gfx::IIndexBuffer& indexBuffer, const SubsetVector& vSubsets, const Geometry& geometry, gfx::MeshSkeleton* pSkeleton);

		private:

			void OnChangeVertexCount(unsigned int subset) override;

		};
	}
}

#endif