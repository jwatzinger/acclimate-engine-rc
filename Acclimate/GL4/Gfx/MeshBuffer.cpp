#include "MeshBuffer.h"

#ifdef ACL_API_GL4

#include "..\VertexBufferObject.h"
#include "..\IndexBufferObject.h"
#include "..\Render\States.h"
#include "..\..\Gfx\IGeometry.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace gl4
	{
		
		/*******************************
		* VertexBuffer
		********************************/

		ogl::AttributeType convertToAttributeType(gfx::AttributeType type)
		{
			switch(type)
			{
			case gfx::AttributeType::FLOAT:
				return ogl::AttributeType::FLOAT;
			case gfx::AttributeType::UBYTE4:
				return ogl::AttributeType::UBYTE;
			default:
				ACL_ASSERT(false);
			}

			return ogl::AttributeType::FLOAT;
		}

		ogl::VertexLayout createLayoutFromGeometry(const gfx::IGeometry& geometry, unsigned int startRegister)
		{
			ogl::VertexLayout layout(startRegister);
			auto& vAttributes = geometry.GetAttributes();
			unsigned int offset = 0;

			for(unsigned int i = startRegister; i < vAttributes.size(); i++)
			{
				auto& attribute = vAttributes[i];
				layout.vAttributes.emplace_back(attribute.numElements, offset, convertToAttributeType(attribute.type), attribute.instanceDivisor);
				offset += geometry.CalculateAttributeSize(attribute);
			}

			return layout;
		}

		VertexBuffer::VertexBuffer(size_t numVertices, const gfx::IGeometry& geometry, unsigned int stride, unsigned int startRegister, const void* pData) :
			BaseVertexBuffer(numVertices, stride), m_pBuffer(nullptr)
		{
			auto layout = createLayoutFromGeometry(geometry, startRegister);

			if(pData)
				m_pBuffer = new ogl::VertexBufferObject(stride, layout, numVertices, pData, ogl::VboUsage::STATIC);
			else
				m_pBuffer = new ogl::VertexBufferObject(stride, layout, numVertices, ogl::VboUsage::DYNAMIC);
		}

		VertexBuffer::VertexBuffer(const VertexBuffer& buffer) :
			BaseVertexBuffer(buffer)
		{
			m_pBuffer = new ogl::VertexBufferObject(*buffer.m_pBuffer);
		}

		VertexBuffer::~VertexBuffer(void)
		{
			delete m_pBuffer;
		}

		gfx::IVertexBuffer& VertexBuffer::Clone(void) const
		{
			return *new VertexBuffer(*this);
		}

		void VertexBuffer::OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const
		{
			m_pBuffer->Bind();
		}

		void* VertexBuffer::OnMap(gfx::MapFlags flags)
		{
			return m_pBuffer->GetData();
		}

		void VertexBuffer::OnUnmap(void)
		{
			m_pBuffer->Update();
		}

		/*******************************
		* IndexBuffer
		********************************/

		IndexBuffer::IndexBuffer(size_t numIndices, const unsigned int* pData) :
			BaseIndexBuffer(numIndices), m_pBuffer(nullptr)
		{
			if(pData)
				m_pBuffer = new ogl::IndexBufferObject(numIndices * sizeof(unsigned int), pData, ogl::IboUsage::STATIC);
			else
				m_pBuffer = new ogl::IndexBufferObject(numIndices * sizeof(unsigned int), ogl::IboUsage::DYNAMIC);
		}

		IndexBuffer::~IndexBuffer(void)
		{
			delete m_pBuffer;
		}

		void IndexBuffer::Bind(render::StateGroup& group) const
		{
			m_pBuffer->Bind();
		}

		unsigned int* IndexBuffer::OnMap(gfx::MapFlags flags)
		{
			return (unsigned int*)m_pBuffer->GetData();
		}

		void IndexBuffer::OnUnmap(void)
		{
			m_pBuffer->Update();
		}

	}
}

#endif