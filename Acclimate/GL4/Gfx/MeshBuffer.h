#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseMeshBuffer.h"

namespace acl
{
	namespace gfx
	{
		class IGeometry;
	}

	namespace render
	{
		class StateGroup;
	}

	namespace gl4
	{

		class VertexBuffer :
			public gfx::BaseVertexBuffer
		{
		public:

			VertexBuffer(size_t numVertices, const gfx::IGeometry& geometry, unsigned int stride, unsigned int startRegister, const void* pData);
			VertexBuffer(const VertexBuffer& buffer);
			~VertexBuffer(void);
			gfx::IVertexBuffer& Clone(void) const override;

		private:

			void OnBind(render::StateGroup& group, gfx::BindTarget target, unsigned int offset) const override;

			void* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;

			AclVertexBuffer* m_pBuffer;
		};

		class IndexBuffer :
			public gfx::BaseIndexBuffer
		{
		public:

			IndexBuffer(size_t numVertices, const unsigned int* pData);
			~IndexBuffer(void);

			void Bind(render::StateGroup& group) const override;

		private:

			unsigned int* OnMap(gfx::MapFlags flags) override;
			void OnUnmap(void) override;

			AclIndexBuffer* m_pBuffer;
		};

	}
}

#endif