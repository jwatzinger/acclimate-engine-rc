#include "MeshLoader.h"

#ifdef ACL_API_GL4

#include "Mesh.h"
#include "InstancedMesh.h"
#include "MeshBuffer.h"

namespace acl
{
    namespace gl4
    {

		MeshLoader::MeshLoader(const gfx::Textures& textures, gfx::Meshes& meshes, const gfx::IGeometryCreator& creator) : BaseMeshLoader(textures, meshes, creator)
		{
		}

		gfx::IMesh& MeshLoader::OnCreateMesh(const gfx::IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndices, const unsigned int* pIndicies, const SubsetVector& vSubsets, gfx::MeshSkeleton* pSkeleton) const
		{
			auto& vertexBuffer = *new VertexBuffer(numVertices, geometry, geometry.GetVertexSize(), 0, pVertices);
			auto& indexBuffer = *new IndexBuffer(numIndices, pIndicies);

			return *new Mesh(vertexBuffer, indexBuffer, vSubsets, (Geometry&)geometry, pSkeleton);
		}

		gfx::IInstancedMesh& MeshLoader::OnCreateInstancedMesh(const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh) const
		{
			//create instance buffer
			VertexBuffer& instanceBuffer = *new VertexBuffer(numInstances, geometry, geometry.CalculateVertexSize(1), geometry.GetNumElements(0), nullptr);

			return *new InstancedMesh(instanceBuffer, numInstances, mesh, (Geometry&)geometry);
		}
    
    }
}

#endif
