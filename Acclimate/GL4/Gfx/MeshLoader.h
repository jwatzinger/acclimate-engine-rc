#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseMeshLoader.h"

namespace acl
{
    namespace gl4
    {

        class MeshLoader :
			public gfx::BaseMeshLoader
        {
        public:
            MeshLoader(const gfx::Textures& textures, gfx::Meshes& meshes, const gfx::IGeometryCreator& creator);

        private:

			gfx::IMesh& OnCreateMesh(const gfx::IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndicies, const unsigned int* pIndicies, const SubsetVector& vSubsets, gfx::MeshSkeleton* pSkeleton) const override;
			gfx::IInstancedMesh& OnCreateInstancedMesh(const gfx::IGeometry& geometry, size_t numInstances, gfx::IMesh& mesh) const override;

        };

    }
}

#endif