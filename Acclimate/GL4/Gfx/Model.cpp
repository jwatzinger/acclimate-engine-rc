#include "Model.h"

#ifdef ACL_API_GL4

#include "Cbuffer.h"

namespace acl
{
    namespace gl4
    {

		Model::Model(void) : BaseModel(*new Cbuffer(), *new Cbuffer(), *new Cbuffer())
		{
        }

		Model::Model(gfx::IMesh& mesh) : BaseModel(*new Cbuffer(), *new Cbuffer(), *new Cbuffer(), mesh)
        {
        }

		Model::Model(const Model& model) : BaseModel(model)
		{
			UpdateMaterials();
		}

		gfx::IModel& Model::Clone(void) const
		{
			return *new Model(*this);
		}

    }
}

#endif