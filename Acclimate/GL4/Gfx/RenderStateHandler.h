#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\RenderStateHandler.h"

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace gl4
	{

		class RenderStateHandlerInstance :
			public gfx::RenderStateHandler::IInstance
		{
		public:

			void OnUnbindTexture(render::StateGroup& states, gfx::TextureBindTarget target, unsigned int slot) const override;
		};

	}
}

#endif