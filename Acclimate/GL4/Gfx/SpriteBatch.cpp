#include "SpriteBatch.h"

#ifdef ACL_API_GL4

#include "Geometry.h"
#include "..\Sprite.h"
#include "..\Render\States.h"
#include "..\..\Render\Instance.h"

namespace acl
{
	namespace gl4
	{

		SpriteBatch::SpriteBatch(ogl::Sprite& sprite, const gfx::Fonts& fonts, const gfx::IGeometryCreator& creator) : BaseSpriteBatch(fonts, creator),
			m_pSprite(&sprite)
        {
			m_pSprite->Bind();
            // set geometry resource binds
			Geometry& geometry = (Geometry&)GetGeometry();
			geometry.Unbind();

			SetClipRect(nullptr);
        }

        void SpriteBatch::SetClipRect(const math::Rect* pClip)
        {
        }

		void SpriteBatch::OnPrepareExecute(void)
		{
		}

		size_t SpriteBatch::OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle) const
		{
			return m_pSprite->Draw(vTextureSize, vPosition, vOrigin, rSrcRect, vScale, angle);
		}

		size_t SpriteBatch::OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle) const
		{
			return m_pSprite->Draw(vPosition, vOrigin, vScale, angle);
		}

		void SpriteBatch::OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance)
		{
			instance.CreateCall<DrawElements>(ogl::VertexType::TRIANGLE, numIndices, startIndex);
		}

		void SpriteBatch::OnFinishExecute(void)
		{
			m_pSprite->Upload();
		}

	}
}

#endif