#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseSpriteBatch.h"


namespace acl
{
	namespace gfx
	{
		class IText;
	}

	namespace gl4
	{

		class SpriteBatch final :
			public gfx::BaseSpriteBatch
		{
		public:
			SpriteBatch(ogl::Sprite& sprite, const gfx::Fonts& fonts, const gfx::IGeometryCreator& creator);

			void SetClipRect(const math::Rect* pClip) override;

		protected:

			void OnPrepareExecute(void) override;
			size_t OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle) const override;
			size_t OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle) const override;
			void OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance) override;
			void OnFinishExecute(void) override;

		private:

			ogl::Sprite* m_pSprite;
		};

	}
}

#endif
