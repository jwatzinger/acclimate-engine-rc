#include "Texture.h"

#ifdef ACL_API_GL4

#include "..\Texture.h"
#include "..\Render\States.h"
#include "..\..\Render\StateGroup.h"
#include "..\..\System\Assert.h"
#include "..\..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{

		gfx::TextureFormats ConvertFormat(GLint format)
		{
		    switch(format)
		    {
			case GL_RGB:
				return gfx::TextureFormats::X32;
			case GL_RGBA:
			    return gfx::TextureFormats::A32;
            case GL_RGBA16F:
			    return gfx::TextureFormats::A16F;
			case GL_R8:
				return gfx::TextureFormats::L8;
			case GL_R16:
				return gfx::TextureFormats::L16;
            case GL_R32F:
                return gfx::TextureFormats::R32;
			case GL_RG16F:
				return gfx::TextureFormats::GR16F;
			case GL_R16F:
				return gfx::TextureFormats::R16F;
	       case GL_NONE:
				return gfx::TextureFormats::UNKNOWN;
            default:
                return gfx::TextureFormats::A32;
		    }
		}

		void deleteTexture(AclTexture* pTexture)
		{
			delete pTexture;
		}

		Texture::Texture(AclTexture& texture) : BaseTexture(texture, texture.GetSize(), ConvertFormat(texture.GetFormat()), deleteTexture)
		{
		}

		void Texture::Bind(render::StateGroup& states, BindTarget target, unsigned int slot) const
		{
			switch(target)
			{
			case BindTarget::PIXEL:
			{
				switch(slot)
				{
				case 0:
					states.Add<BindTexture0>(m_pTexture);
					break;
				case 1:
					states.Add<BindTexture1>(m_pTexture);
					break;
				case 2:
					states.Add<BindTexture2>(m_pTexture);
					break;
				case 3:
					states.Add<BindTexture3>(m_pTexture);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			}
			case BindTarget::VERTEX:
			{
				switch(slot)
				{
				case 0:
					states.Add<BindVertexTexture0>(m_pTexture);
					break;
				default:
					ACL_ASSERT(false);
				}
				break;
			}
			default:
				ACL_ASSERT(false);
			}
		}

		void Texture::OnResize(const math::Vector2& vSize)
		{
			m_pTexture->Resize(vSize);
		}

		void Texture::OnMap(gfx::MapFlags flags, gfx::Mapped& mapped) const
		{
			mapped.pData = m_pTexture->GetData();
			mapped.pitch = m_pTexture->GetPitch();
		} 

		void Texture::OnUnmap(void) const
		{
			m_pTexture->Update();
		}

	}
}

#endif