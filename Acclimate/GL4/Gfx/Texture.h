#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseTexture.h"

namespace acl
{
	namespace gl4
	{

		class Texture final :
			public gfx::BaseTexture
		{
		public:
			Texture(AclTexture& texture);

			void Bind(render::StateGroup& states, BindTarget target, unsigned int slot) const override;

		private:

			void OnResize(const math::Vector2& vSize) override;
			void OnMap(gfx::MapFlags flags, gfx::Mapped& mapped) const override;
			void OnUnmap(void) const;
		};

	}
}

#endif
