#include "TextureLoader.h"

#ifdef ACL_API_GL4

#include "Texture.h"
#include "..\Texture.h"
#include "..\..\Math\Vector.h"
#include "..\..\System\Log.h"
#include "..\..\..\Extern\GLEW\glew.h"
#include "..\..\..\Extern\FreeImage\FreeImage.h"

namespace acl
{
    namespace gl4
    {

		void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message) 
		{
			sys::log->Out(sys::LogModule::API, sys::LogType::ERR, "(FreeImage): ", message);
		}

        GLint GetFormat(gfx::TextureFormats format)
		{
		    switch(format)
		    {
            case gfx::TextureFormats::A32:
			    return GL_RGBA;
            case gfx::TextureFormats::A16F:
			    return GL_RGBA16F;
			case gfx::TextureFormats::L8:
				return GL_R8;
			case gfx::TextureFormats::L16:
				return GL_R16;
            case gfx::TextureFormats::R32:
                return GL_R32F;
			case gfx::TextureFormats::GR16F:
				return GL_RG16F;
			case gfx::TextureFormats::R16F:
				return GL_R16F;
            case gfx::TextureFormats::UNKNOWN:
				return GL_NONE;
            default:
                return GL_RGB;
		    }
		}

		TextureLoader::TextureLoader(gfx::Textures& textures) : BaseTextureLoader(textures)
        {
			FreeImage_SetOutputMessage(FreeImageErrorHandler);
        }

        gfx::ITexture& TextureLoader::OnLoadTexture(const std::wstring& stFilename, bool bRead, gfx::TextureFormats format) const
        {
            ogl::Texture& glTexture = *new ogl::Texture(stFilename.c_str(), GetFormat(format));
			return *new Texture(glTexture);
        }

        gfx::ITexture& TextureLoader::OnCreateTexture(const math::Vector2& vSize, gfx::TextureFormats format, gfx::LoadFlags flags) const
        {
			bool bIsRenderTarget = flags == gfx::LoadFlags::RENDER_TARGET || flags == gfx::LoadFlags::RENDER_TARGET_MIPS;

            ogl::Texture& glTexture = *new ogl::Texture(vSize.x, vSize.y, GetFormat(format), bIsRenderTarget);
			return *new Texture(glTexture);
        }

    }
}

#endif