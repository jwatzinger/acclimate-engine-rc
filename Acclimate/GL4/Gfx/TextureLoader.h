#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseTextureLoader.h"

namespace acl
{
    namespace gl4
    {

        class TextureLoader final :
			public gfx::BaseTextureLoader
        {
        public:
            TextureLoader(gfx::Textures& textures);

		private:
            gfx::ITexture& OnLoadTexture(const std::wstring& stFilename, bool bRead, gfx::TextureFormats format) const override;
            gfx::ITexture& OnCreateTexture(const math::Vector2& vSize, gfx::TextureFormats format, gfx::LoadFlags flags) const override;

        };

    }
}

#endif