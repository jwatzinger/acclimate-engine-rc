#include "ZBuffer.h"

#ifdef ACL_API_GL4

#include "..\DepthBuffer.h"

namespace acl
{
	namespace gl4
	{

		void deleteZBuffer(AclDepth* pDepthBuffer)
		{
			delete pDepthBuffer;
		}

		ZBuffer::ZBuffer(AclDepth& depthBuffer): BaseZBuffer(depthBuffer, depthBuffer.GetSize(), deleteZBuffer)
		{
		}

		void ZBuffer::OnResize(const math::Vector2& vSize) 
		{
			m_pDepthBuffer->Recreate(vSize.x, vSize.y);
		}

	}
}

#endif