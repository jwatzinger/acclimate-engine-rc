#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseZBuffer.h"

namespace acl
{
	namespace gl4
	{

		class ZBuffer :
			public gfx::BaseZBuffer
		{
		public:
			ZBuffer(AclDepth& depthBuffer);

			void OnResize(const math::Vector2& vSize) override;
		};

	}
}

#endif
