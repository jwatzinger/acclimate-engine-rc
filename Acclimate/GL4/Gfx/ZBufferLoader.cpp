#include "ZBufferLoader.h"

#ifdef ACL_API_GL4

#include "ZBuffer.h"
#include "..\DepthBuffer.h"
#include "..\..\Math\Vector.h"
#include "..\..\System\Log.h"

namespace acl
{
	namespace gl4
	{

		ZBufferLoader::ZBufferLoader(gfx::ZBuffers& zBuffers): BaseZBufferLoader(zBuffers)
		{
		}

		gfx::IZBuffer& ZBufferLoader::OnCreateZBuffer(const math::Vector2& vSize) const
		{
			AclDepth* pDepthBuffer = new AclDepth(vSize.x, vSize.y);

			return *new ZBuffer(*pDepthBuffer);
		}

	}
}

#endif