#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Gfx\BaseZBufferLoader.h"

namespace acl
{
	namespace gl4
	{

		class ZBufferLoader final :
			public gfx::BaseZBufferLoader
		{
		public:
			ZBufferLoader(gfx::ZBuffers& zBuffers);

		private:

			gfx::IZBuffer& OnCreateZBuffer(const math::Vector2& vSize) const override;
		};

	}
}

#endif