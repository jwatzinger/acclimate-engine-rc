#include "IndexBufferObject.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			GLenum convertUsage(IboUsage usage)
			{
				switch(usage)
				{
				case IboUsage::STATIC:
					return GL_STATIC_DRAW;
				case IboUsage::DYNAMIC:
					return GL_DYNAMIC_DRAW;
				}

				return GL_STATIC_DRAW;
			}

			IndexBufferObject::IndexBufferObject(size_t size, IboUsage usage) : IndexBufferObject(size, nullptr, usage)
			{
			}

			IndexBufferObject::IndexBufferObject(size_t size, const void* pData, IboUsage usage) : m_size(size)
			{
				GL_CHECK(glGenBuffers(1, &m_buffer));
				GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, pData, convertUsage(usage)));
			}

			IndexBufferObject::~IndexBufferObject(void)
			{
				GL_CHECK(glDeleteBuffers(1, &m_buffer));
			}

			void IndexBufferObject::Bind(void) const
			{
				GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer));
			}

			void* IndexBufferObject::GetData(void)
			{
				void* pData = nullptr;
				GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer));
				GL_CHECK(pData = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, m_size, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
				return pData;
			}

			const void* IndexBufferObject::GetData(void) const
			{
				void* pData = nullptr;
				GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer));
				GL_CHECK(pData = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, m_size, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
				return pData;
			}

			void IndexBufferObject::Update(void)
			{
				GL_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_buffer));
				GL_CHECK(glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER));
			}

		}
	}
}

#endif