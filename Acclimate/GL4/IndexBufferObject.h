#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			enum class IboUsage
			{
				STATIC, DYNAMIC
			};

			class IndexBufferObject
			{
			public:
				IndexBufferObject(size_t size, IboUsage usage);
				IndexBufferObject(size_t size, const void* pData, IboUsage usage);
				~IndexBufferObject(void);

				void* GetData(void);
				const void* GetData(void) const;

				void Bind(void) const;
				void Update(void);

			private:

				unsigned int m_buffer;
				size_t m_size;
			};

		}
	}
}

#endif
