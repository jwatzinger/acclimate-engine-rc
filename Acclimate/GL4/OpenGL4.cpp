#include "OpenGL4.h"

#ifdef ACL_API_GL4

#include <Windows.h>
#include "Context.h"
#include "Sprite.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{

		OpenGL4::OpenGL4(HWND hWnd)
		{
			m_hDc = GetDC(hWnd);

			DEVMODE mode;
			unsigned int id = 0;
			while(EnumDisplaySettings(nullptr, id++, &mode))
			{
				auto& lastMode = m_vModes.rbegin();

				if(id==1 || lastMode->dmPelsWidth != mode.dmPelsWidth || lastMode->dmPelsHeight != mode.dmPelsHeight || lastMode->dmDisplayFrequency != mode.dmDisplayFrequency)
					m_vModes.push_back(mode);
			}
			
			m_pContext = new ogl::Context(m_hDc, mode);
			m_pSprite = new ogl::Sprite(*m_pContext);

		}

		OpenGL4::~OpenGL4(void)
		{
			delete m_pSprite;
			delete m_pContext;
		}

		ogl::Context& OpenGL4::GetContext(void) const
		{
			return *m_pContext;
		}

		ogl::Sprite& OpenGL4::GetSprite(void) const
		{
			return *m_pSprite;
		}

		const OpenGL4::DeviceModeVector& OpenGL4::GetDeviceModes(void) const
		{
			return m_vModes;
		}

		void OpenGL4::Present(void) const
		{
			SwapBuffers(m_hDc);
		}

		void OpenGL4::Reset(void) const
		{
			m_pSprite->Reset();
		}

	}
}

#endif