#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <Windows.h>
#include <vector>

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			class Context;
		}

		class OpenGL4
		{
			typedef std::vector<DEVMODE> DeviceModeVector;
		public:
			OpenGL4(HWND hWnd);
			~OpenGL4(void);

			ogl::Context& GetContext(void) const;
			ogl::Sprite& GetSprite(void) const;
			const DeviceModeVector& GetDeviceModes(void) const;

			void Present(void) const;
			void Reset(void) const;

		private:

			ogl::Context* m_pContext;
			ogl::Sprite* m_pSprite;

			DeviceModeVector m_vModes;

			HDC m_hDc;
		};

	}
}

#endif

