#include "Queue.h"

#ifdef ACL_API_GL4

#include <algorithm>
#include "States.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
    namespace gl4
    {

        void Queue::Init(void)
        {
	        BindVAO::type();
			BindProgram::type();
			BindRenderTargets::type();
			BindTexture0::type();
			BindTexture1::type();
			BindTexture2::type();
			BindTexture3::type();
			BindVertexTexture0::type();
			SetBlending::type();
			SamplerState0::type();
			SamplerState1::type();
			SamplerState2::type();
			SamplerState3::type();
			VertexSamplerState0::type();
			DepthState::type();
			RasterizerState::type();
			SetViewport::type();
			UBufferV0::type();
			UBufferV1::type();
			UBufferP0::type();
			UBufferP1::type();
			UBufferG0::type();
			UBufferG1::type();

			// draw calls
			DrawElements::type();
			ClearTargets::type();
			DrawInstanced::type();
        }

        Queue::Queue(const ogl::Context& context) : m_pContext(&context)
        {
        }

		enum class StateType : render::BaseState::Type	{ VAO, Program, RenderTargets, Texture0, Texture1, Texture2, Texture3, VertexTexture0, Blend, Sampler0, Sampler1, Sampler2, Sampler3, VertexSampler0, Depth, Rasterizer, Viewport, VUbuffer0, VUbuffer1, FUbuffer0, FUbuffer1, GUbuffer0, GUbuffer1 };
		enum class CallType : render::BaseDrawCall::Type { Elements, Clear, Instanced, Mipmaps };

        void Queue::OnExecuteSorted(const InstanceVector& vInstances) const
        {
	        //iteratre through render instances
	        for(auto& instance : vInstances)
	        {
		        //store pointer to instance and state groups
				const char* pStream = (char*)instance.first->GetStateStream();

		        unsigned int bPreviousApplied = 0;

				unsigned int offset = 0;
		        //iterate through instance state groups
				for(unsigned int i = 0; i < instance.first->GetStateCount(); i++)
		        {
					const unsigned int commandCount = *reinterpret_cast<const unsigned int*>(pStream +offset);

					//store pointer to state group and state pointer
					const void* pvCmds = pStream +offset + sizeof(unsigned int);
					offset += commandCount*render::STATE_SIZE + sizeof(unsigned int);

			        //iterate through command group
			        for(unsigned int j = 0; j < commandCount; j++)
			        {
						//get state type
						const render::BaseState::Type type = *reinterpret_cast<const render::BaseState::Type*>(pvCmds);
						pvCmds = reinterpret_cast<const char*>(pvCmds) +sizeof(render::BaseState::Type);

						const auto shifted = 1U << type;
				        //ignore if same command was applied earlier on stack
						if((bPreviousApplied & shifted) == shifted)
                        {
							pvCmds = static_cast<const void*>(static_cast<const char*>(pvCmds) +render::STATE_SIZE - sizeof(unsigned int));
					        continue;
                        }

				        //set applied status to skip any further commands of this type
						bPreviousApplied |= shifted;

				        //switch on type
				        switch((StateType)type)
				        {
						case StateType::VAO:
					        {
								const BindVAO& bindVAO = *static_cast<const BindVAO*>(pvCmds);
								bindVAO.Execute(*m_pContext);
						        break;
					        }
						case StateType::Program:
							{
								const BindProgram& bindProg = *static_cast<const BindProgram*>(pvCmds);
								bindProg.Execute(*m_pContext);
								break;
							}
						case StateType::RenderTargets:
							{
								const BindRenderTargets& bindRT = *static_cast<const BindRenderTargets*>(pvCmds);
								bindRT.Execute(*m_pContext);
								break;
							}
						case StateType::Texture0:
							{
								const BindTexture0& bindTex = *static_cast<const BindTexture0*>(pvCmds);
								bindTex.Execute(*m_pContext);
								break;
							}
						case StateType::Texture1:
							{
								const BindTexture1& bindTex = *static_cast<const BindTexture1*>(pvCmds);
								bindTex.Execute(*m_pContext);
								break;
							}
						case StateType::Texture2:
							{
								const BindTexture2& bindTex = *static_cast<const BindTexture2*>(pvCmds);
								bindTex.Execute(*m_pContext);
								break;
							}
						case StateType::Texture3:
							{
								const BindTexture3& bindTex = *static_cast<const BindTexture3*>(pvCmds);
								bindTex.Execute(*m_pContext);
								break;
							}
						case StateType::VertexTexture0:
							{
								const BindVertexTexture0& bindTex = *static_cast<const BindVertexTexture0*>(pvCmds);
								bindTex.Execute(*m_pContext);
								break;
							}
						case StateType::Blend:
							{
								const SetBlending& setBlend = *static_cast<const SetBlending*>(pvCmds);
								setBlend.Execute(*m_pContext);
								break;
							}
						case StateType::Sampler0:
							{
								const SamplerState0& sampler = *static_cast<const SamplerState0*>(pvCmds);
								sampler.Execute(*m_pContext);
								break;
							}
						case StateType::Sampler1:
							{
								const SamplerState1& sampler = *static_cast<const SamplerState1*>(pvCmds);
								sampler.Execute(*m_pContext);
								break;
							}
						case StateType::Sampler2:
							{
								const SamplerState2& sampler = *static_cast<const SamplerState2*>(pvCmds);
								sampler.Execute(*m_pContext);
								break;
							}
						case StateType::Sampler3:
							{
								const SamplerState3& sampler = *static_cast<const SamplerState3*>(pvCmds);
								sampler.Execute(*m_pContext);
								break;
							}
						case StateType::VertexSampler0:
							{
								const VertexSamplerState0& sampler = *static_cast<const VertexSamplerState0*>(pvCmds);
								sampler.Execute(*m_pContext);
								break;
							}
						case StateType::Depth:
							{
								const DepthState& depth = *static_cast<const DepthState*>(pvCmds);
								depth.Execute(*m_pContext);
								break;
							}
						case StateType::Rasterizer:
							{
								const RasterizerState& rasterize = *static_cast<const RasterizerState*>(pvCmds);
								rasterize.Execute(*m_pContext);
								break;
							}
						case StateType::Viewport:
							{
								const SetViewport& viewport = *static_cast<const SetViewport*>(pvCmds);
								viewport.Execute(*m_pContext);
								break;
							}
						case StateType::VUbuffer0:
							{
								const UBufferV0& bindUBuffer = *static_cast<const UBufferV0*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
						case StateType::VUbuffer1:
							{
								const UBufferV1& bindUBuffer = *static_cast<const UBufferV1*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
						case StateType::FUbuffer0:
							{
								const UBufferP0& bindUBuffer = *static_cast<const UBufferP0*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
						case StateType::FUbuffer1:
							{
								const UBufferP1& bindUBuffer = *static_cast<const UBufferP1*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
						case StateType::GUbuffer0:
							{
								const UBufferG0& bindUBuffer = *static_cast<const UBufferG0*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
						case StateType::GUbuffer1:
							{
								const UBufferG1& bindUBuffer = *static_cast<const UBufferG1*>(pvCmds);
								bindUBuffer.Execute(*m_pContext);
								break;
							}
				        }
						pvCmds = static_cast<const void*>(static_cast<const char*>(pvCmds) +render::STATE_SIZE - sizeof(unsigned int));
			        }
		        }

		        //get draw call pointer and type
				const render::BaseDrawCall* pDrawCall = instance.first->GetDrawCall();

		        if(pDrawCall)
		        {
			        const CallType type = (CallType)pDrawCall->GetType();

			        //switch on call type
			        switch(type)
			        {
			        case CallType::Elements:
				        {
					        const DrawElements& drawElements = *static_cast<const DrawElements*>(pDrawCall);
					        drawElements.Execute(*m_pContext);
					        break;
				        }
					case CallType::Clear:
						{
							const ClearTargets& clearTargets = *static_cast<const ClearTargets*>(pDrawCall);
							clearTargets.Execute(*m_pContext);
							break;
						}
					case CallType::Instanced:
						{
							const DrawInstanced& drawInstanced = *static_cast<const DrawInstanced*>(pDrawCall);
							drawInstanced.Execute(*m_pContext);
							break;
						}
					case CallType::Mipmaps:
						{
							const GenerateMips& generateMips = *static_cast<const GenerateMips*>(pDrawCall);
							generateMips.Execute(*m_pContext);
							break;
						}
			        }
		        }
	        }
        }

    }
}

#endif