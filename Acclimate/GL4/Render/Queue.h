#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Render\BaseQueue.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{
			class Context;
		}

		class Queue : 
			public render::BaseQueue
		{
		public:
			Queue(const ogl::Context& context);

			static void Init(void);

		private:

			void OnExecuteSorted(const InstanceVector& vInstances) const override;

			const ogl::Context* m_pContext;
		};

	}
}

#endif