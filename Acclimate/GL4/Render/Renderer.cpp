#include "Renderer.h"

#ifdef ACL_API_GL4

#include "Queue.h"
#include "Stage.h"
#include "..\OpenGL4.h"
#include "..\..\Render\RendererInternalAccessor.h"

#include "..\Context.h"

namespace acl
{
	namespace gl4
    {

		Renderer::Renderer(const OpenGL4& openGL4, const gfx::ICbufferLoader& loader) : m_pOpenGL4(&openGL4),
			m_pLoader(&loader)
        {
        }

		void Renderer::GetDevice(render::RendererInternalAccessor& accessor) const
		{
			accessor.SetDevice(nullptr);
		}

		render::IStage& Renderer::OnCreateStage(unsigned int id, render::IQueue& queue, unsigned int flags) const
		{
			return *new Stage(id, queue, flags, *m_pLoader);
		}

		render::IQueue& Renderer::OnCreateQueue(void) const
		{
			return *new Queue(m_pOpenGL4->GetContext());
		}

        void Renderer::Prepare(void)
        {
        }

		void Renderer::OnExecute(void)
		{
			m_pOpenGL4->Reset();
			m_pOpenGL4->GetContext().UnbindVertexArrayObject();
		}

		void Renderer::Finish(void)
		{
			m_pOpenGL4->Present();
		}
        
    }
}

#endif