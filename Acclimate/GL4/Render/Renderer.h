#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Render\BaseRenderer.h"

namespace acl
{
	namespace gfx
	{
		class ICbufferLoader;
	}

	namespace render
	{
		class IQueue;
	}

	namespace gl4
	{
		class OpenGL4;

		class Renderer :
			public render::BaseRenderer
		{
		public:
			Renderer(const OpenGL4& openGL4, const gfx::ICbufferLoader& loader);

			void GetDevice(render::RendererInternalAccessor& accessor) const override;

			void Prepare(void) override;
			void Finish(void) override;

		private:

			render::IStage& OnCreateStage(unsigned int id, render::IQueue& queue, unsigned int flags) const override;
			render::IQueue& OnCreateQueue(void) const override;

			void OnExecute(void) override;

			const OpenGL4* m_pOpenGL4;
			const gfx::ICbufferLoader* m_pLoader;
		};

	}
}
#endif