#include "Stage.h"

#ifdef ACL_API_GL4

#include <algorithm>
#include "States.h"
#include "..\DepthBuffer.h"
#include "..\..\Gfx\ITexture.h"
#include "..\..\Gfx\IZBuffer.h"
#include "..\..\Math\Rect.h"
#include "..\..\Render\Instance.h"

#undef nullptr

namespace acl
{
    namespace gl4
    {

		Stage::Stage(unsigned int id, render::IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader) :
			BaseStage(id, queue, flags, loader), m_pFrameBuffer(nullptr)
        {
			m_stageStates.Add<BindRenderTargets>(nullptr);

			GenerateDraw();
        }

        Stage::~Stage(void)
        {
			delete m_pFrameBuffer;
        }

		void Stage::SetViewport(const math::Rect& rect)
		{
			m_stageStates.Add<gl4::SetViewport>(rect);
		}

		void Stage::GenerateDraw(void)
		{
			const size_t flags = GetFlags();
			const bool bClear = (flags & render::CLEAR) == render::CLEAR;
			const bool bClearDepth = (flags & render::CLEAR_Z) == render::CLEAR_Z;
			if(bClear || bClearDepth)
			{
				if(bClear)
					SetDrawCall(*new ClearTargets(bClearDepth, &GetClearColor()));
				else
					SetDrawCall(*new ClearTargets(bClearDepth, nullptr));
			}
		}

		void Stage::OnChangeDepthBuffer(const AclDepth* pDepth)
		{
			if(!m_pFrameBuffer)
			{
				m_pFrameBuffer = new ogl::FrameBuffer();
				m_stageStates.Add<BindRenderTargets>(m_pFrameBuffer);
			}

			m_pFrameBuffer->Bind();
			if(pDepth)
				pDepth->Bind();
			else
				m_pFrameBuffer->UnbindDepthbuffer();
			m_pFrameBuffer->Unbind();
		}

		void Stage::OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture)
		{
			if(!m_pFrameBuffer)
			{
				m_pFrameBuffer = new ogl::FrameBuffer();
				m_stageStates.Add<BindRenderTargets>(m_pFrameBuffer);
			}

			m_pFrameBuffer->Bind();
			if(pTexture)
				m_pFrameBuffer->BindTexture(slot, *pTexture);
			else
				m_pFrameBuffer->UnbindTexture(slot);

			m_pFrameBuffer->Unbind();
		}

		void Stage::OnChangeClearColor(const gfx::FColor& color)
		{
			GenerateDraw();
		}

		bool Stage::OnGenerateMips(render::Instance& instance) const
		{
			const auto vTextures = GetTextures();
			auto pTexture = vTextures[0];
			if(pTexture)
			{
				instance.CreateCall<GenerateMips>(*pTexture);
				return true;
			}
			else
				return false;
		}

    }
}

#endif