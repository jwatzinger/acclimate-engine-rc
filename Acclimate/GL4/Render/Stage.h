#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\..\Render\BaseStage.h"
#include "..\..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{
		class ICbufferLoader;
	}

	namespace gl4
	{
		namespace ogl
		{
			class FrameBuffer;
		}

		class Stage :
			public render::BaseStage
		{
		public:
			Stage(unsigned int id, render::IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader);
			~Stage(void);

			void SetViewport(const math::Rect& rect) override;

		private:

			void OnChangeDepthBuffer(const AclDepth* pDepth) override;
			void OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture) override;
			void OnChangeClearColor(const gfx::FColor& color) override;
			bool OnGenerateMips(render::Instance& instance) const override;

			void GenerateDraw(void);

			ogl::FrameBuffer* m_pFrameBuffer;
		};

	}
}

#endif