#pragma once
#include "..\..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\Context.h"
#include "..\Texture.h"
#include "..\Effect.h"
#include "..\Sampler.h"
#include "..\FrameBuffer.h"
#include "..\UniformBufferObject.h"
#include "..\VertexArrayObject.h"
#include "..\..\Gfx\Color.h"
#include "..\..\Math\Rect.h"
#include "..\..\System\Assert.h"
#include "..\..\Render\States.h"
#include "..\..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{

		/****************************************
		* VertexArrayObject
		****************************************/

		class BindVAO : 
			public render::State<BindVAO>
		{
		public:	

			BindVAO(const ogl::VertexArrayObject* vertexArrayObject) : pVertexArrayObject(vertexArrayObject) {}
	
			void Execute(const ogl::Context& context) const
			{
				pVertexArrayObject->Bind();
			};	

		private:	
	
			const ogl::VertexArrayObject* pVertexArrayObject;
		};

		/****************************************
		* BindProgam
		****************************************/

		class BindProgram :
			public render::State<BindProgram>
		{
		public:

			BindProgram(const ogl::Effect& effect) : pEffect(&effect) {}

			void Execute(const ogl::Context& context) const
			{
				pEffect->Bind();
			};

		private:

			const ogl::Effect* pEffect;
		};

		/****************************************
		* RenderTargets
		****************************************/

		class BindRenderTargets :
			public render::State<BindRenderTargets>
		{
		public:

			BindRenderTargets(const ogl::FrameBuffer* pBuffer) : pBuffer(pBuffer) {}

			void Execute(const ogl::Context& context) const
			{
				if(pBuffer)
				{
					pBuffer->Bind();
#ifdef _DEBUG
					ACL_ASSERT(pBuffer->IsComplete());
#endif
				}
				else
					context.UnbindFramebuffer();
			};

		private:

			const ogl::FrameBuffer* pBuffer;
		};

		/****************************************
		* Texture0
		****************************************/

		class BindTexture0 :
			public render::State<BindTexture0>
		{
		public:

			BindTexture0(const ogl::Texture* pTexture) : pTexture(pTexture) {}

			void Execute(const ogl::Context& context) const
			{
				if(pTexture)
					pTexture->Bind(0);
				else
					context.UnbindTexture(0);
			};

		private:

			const ogl::Texture* pTexture;
		};

		/****************************************
		* Texture1
		****************************************/

		class BindTexture1 :
			public render::State<BindTexture1>
		{
		public:

			BindTexture1(const ogl::Texture* pTexture) : pTexture(pTexture) {}

			void Execute(const ogl::Context& context) const
			{
				if(pTexture)
					pTexture->Bind(1);
				else
					context.UnbindTexture(1);
			};

		private:

			const ogl::Texture* pTexture;
		};

		/****************************************
		* Texture2
		****************************************/

		class BindTexture2 :
			public render::State<BindTexture2>
		{
		public:

			BindTexture2(const ogl::Texture* pTexture) : pTexture(pTexture) {}

			void Execute(const ogl::Context& context) const
			{
				if(pTexture)
					pTexture->Bind(2);
				else
					context.UnbindTexture(2);
			};

		private:

			const ogl::Texture* pTexture;
		};

		/****************************************
		* Texture3
		****************************************/

		class BindTexture3 :
			public render::State<BindTexture3>
		{
		public:

			BindTexture3(const ogl::Texture* pTexture) : pTexture(pTexture) {}

			void Execute(const ogl::Context& context) const
			{
				if(pTexture)
					pTexture->Bind(3);
				else
					context.UnbindTexture(3);
			};

		private:

			const ogl::Texture* pTexture;
		};

		/****************************************
		* VertexTexture3
		****************************************/

		class BindVertexTexture0 :
			public render::State<BindVertexTexture0>
		{
		public:

			BindVertexTexture0(const ogl::Texture* pTexture) : pTexture(pTexture) {}

			void Execute(const ogl::Context& context) const
			{
				if(pTexture)
					pTexture->Bind(4);
				else
					context.UnbindTexture(4);
			};

		private:

			const ogl::Texture* pTexture;
		};

		/****************************************
		* Blending
		****************************************/

		class SetBlending :
			public render::State<SetBlending>
		{
		public:

			SetBlending(bool bEnable, unsigned int srcBlend, unsigned int destBlend, unsigned int blendEq) : bEnable(bEnable),
				srcBlend(srcBlend), destBlend(destBlend), blendEq(blendEq)
			{}

			void Execute(const ogl::Context& context) const
			{
				context.SetBlending(bEnable);
				context.SetBlendingFunc(srcBlend, destBlend);
				context.SetBlendingEquation(blendEq);
			}

		private:

			bool bEnable;
			unsigned int srcBlend, destBlend, blendEq;
		};

		/****************************************
		* SamplerState0
		****************************************/

		class SamplerState0 :
			public render::State<SamplerState0>
		{
		public:

			SamplerState0(const ogl::Sampler& sampler) : pSampler(&sampler)
			{}

			void Execute(const ogl::Context& context) const
			{
				pSampler->Bind(0);
			}

		private:

			const ogl::Sampler* pSampler;
		};

		/****************************************
		* SamplerState0
		****************************************/

		class SamplerState1 :
			public render::State<SamplerState1>
		{
		public:

			SamplerState1(const ogl::Sampler& sampler) : pSampler(&sampler)
			{}

			void Execute(const ogl::Context& context) const
			{
				pSampler->Bind(1);
			}

		private:

			const ogl::Sampler* pSampler;
		};

		/****************************************
		* SamplerState2
		****************************************/

		class SamplerState2 :
			public render::State<SamplerState2>
		{
		public:

			SamplerState2(const ogl::Sampler& sampler) : pSampler(&sampler)
			{}

			void Execute(const ogl::Context& context) const
			{
				pSampler->Bind(2);
			}

		private:

			const ogl::Sampler* pSampler;
		};

		/****************************************
		* SamplerState3
		****************************************/

		class SamplerState3 :
			public render::State<SamplerState3>
		{
		public:

			SamplerState3(const ogl::Sampler& sampler) : pSampler(&sampler)
			{}

			void Execute(const ogl::Context& context) const
			{
				pSampler->Bind(3);
			}

		private:

			const ogl::Sampler* pSampler;
		};

		/****************************************
		* VertexSamplerState0
		****************************************/

		class VertexSamplerState0 :
			public render::State<VertexSamplerState0>
		{
		public:

			VertexSamplerState0(const ogl::Sampler& sampler) : pSampler(&sampler)
			{}

			void Execute(const ogl::Context& context) const
			{
				pSampler->Bind(4);
			}

		private:

			const ogl::Sampler* pSampler;
		};

		/****************************************
		* DepthState
		****************************************/

		class DepthState :
			public render::State<DepthState>
		{
		public:

			DepthState(bool bEnable, bool bWrite) : bEnable(bEnable),
				bWrite(bWrite)
			{}

			void Execute(const ogl::Context& context) const
			{
				context.SetDepthState(bEnable, bWrite);
			}

		private:

			bool bEnable, bWrite;
		};

		/****************************************
		* RasterizerState
		****************************************/

		class RasterizerState :
			public render::State<RasterizerState>
		{
		public:

			RasterizerState(unsigned int cull, unsigned int fill) : cull(cull), fill(fill)
			{}

			void Execute(const ogl::Context& context) const
			{
				context.SetCullState(cull);
				context.SetFillState(fill);
			}

		private:

			unsigned int cull, fill;
		};

		/****************************************
		* SetViewport
		****************************************/

		class SetViewport :
			public render::State<SetViewport>
		{
		public:

			SetViewport(const math::Rect& rViewport) : rViewport(rViewport)
			{}

			void Execute(const ogl::Context& context) const
			{
				context.SetViewport(rViewport);
			}

		private:

			math::Rect rViewport;
		};

/***********************************************
*      DRAW CALLS
***********************************************/

		/****************************************
		* Draw indexed
		****************************************/

		class DrawElements : 
			public render::DrawCall<DrawElements>
		{
		public:	

			DrawElements(ogl::VertexType type, unsigned int numIndices, unsigned int startIndex) : vertices(type), 
				numIndices(numIndices), startIndex(startIndex)
			{
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawElements(*this);
			}
	
			void Execute(const ogl::Context& context) const
			{ 
				context.DrawElements(vertices, numIndices, startIndex);
			}

		private:	

			ogl::VertexType vertices;
			unsigned int numIndices, startIndex;
		};

		/****************************************
		* Clear
		****************************************/

		class ClearTargets : 
			public render::DrawCall<ClearTargets>
		{
		public:

			ClearTargets(bool bClearDepth, const gfx::FColor* pColor) : clear(ogl::NONE)
			{ 
				if(pColor)
				{
					clear |= ogl::COLOR;
					color = *pColor;
				}
					
				if(bClearDepth)
					clear |= ogl::DEPTH;
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new ClearTargets(*this);
			}

			void Execute(const ogl::Context& context) const
			{
				if(clear != 0)
				{
					if((clear & ogl::COLOR))
						context.SetClearColor(color.r, color.g, color.b, color.a);

					context.Clear((ogl::ClearType)clear);
				}
			}

		private:

			unsigned int clear;
			gfx::FColor color;
		};

		/****************************************
		* Draw indexed
		****************************************/

		class DrawInstanced :
			public render::DrawCall<DrawInstanced>
		{
		public:

			DrawInstanced(ogl::VertexType type, unsigned int numInstances, unsigned int numIndices, unsigned int startIndex) : 
				vertex(type), numInstances(numInstances), numIndices(numIndices), startIndex(startIndex)
			{
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new DrawInstanced(*this);
			}

			void Execute(const ogl::Context& context) const
			{
				context.DrawInstanced(vertex, numInstances, numIndices, startIndex);
			}

		private:

			ogl::VertexType vertex;
			unsigned int numInstances, numIndices, startIndex;
		};

		/****************************************
		* GenerateMips
		****************************************/

		class GenerateMips:
			public render::DrawCall<GenerateMips>
		{
		public:

			GenerateMips(const AclTexture& texture) : texture(texture)
			{
			}

			render::BaseDrawCall& Clone(void) const
			{
				return *new GenerateMips(*this);
			}

			void Execute(const ogl::Context& context) const
			{
				texture.GenerateMips();
			}

		private:

			const AclTexture& texture;
		};

/***************************************************
* C-Buffer
***************************************************/


		/****************************************
		* C-buffer Vertex 0
		****************************************/

		class UBufferV0 : 
			public render::State<UBufferV0>
		{
		public:

			UBufferV0(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(0);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

		/****************************************
		* C-buffer Vertex 1
		****************************************/

		class UBufferV1 : 
			public render::State<UBufferV1>
		{
		public:

			UBufferV1(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(1);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

		/****************************************
		* C-buffer Pixel 0
		****************************************/

		class UBufferP0 : 
			public render::State<UBufferP0>
		{
		public:

			UBufferP0(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(2);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

		/****************************************
		* C-buffer Pixel 1
		****************************************/

		class UBufferP1 : 
			public render::State<UBufferP1>
		{
		public:

			UBufferP1(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(3);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

		/****************************************
		* C-buffer Geometry 0
		****************************************/

		class UBufferG0 :
			public render::State<UBufferG0>
		{
		public:

			UBufferG0(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(4);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

		/****************************************
		* C-buffer Geometry 1
		****************************************/

		class UBufferG1 :
			public render::State<UBufferG1>
		{
		public:

			UBufferG1(const ogl::UniformBufferObject& ubo) : pUbo(&ubo)
			{
			}

			void Execute(const ogl::Context& context) const
			{
				pUbo->Bind(5);
			}

		private:

			const ogl::UniformBufferObject* pUbo;
		};

	}
}

#endif