#include "Sampler.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			Sampler::Sampler(unsigned int min, unsigned int mag, unsigned int u, unsigned int v, float anisotropy): m_sampler(0)
			{
				GL_CHECK(glGenSamplers(1, &m_sampler));
				GL_CHECK(glSamplerParameteri(m_sampler, GL_TEXTURE_MIN_FILTER, min));
				GL_CHECK(glSamplerParameteri(m_sampler, GL_TEXTURE_MAG_FILTER, mag));
				GL_CHECK(glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_S, u));
				GL_CHECK(glSamplerParameteri(m_sampler, GL_TEXTURE_WRAP_T, v));
				GL_CHECK(glSamplerParameterf(m_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy));
			}

			Sampler::~Sampler(void)
			{
				GL_CHECK(glDeleteSamplers(1, &m_sampler));
			}

			void Sampler::Bind(unsigned int slot) const
			{
				GL_CHECK(glBindSampler(slot, m_sampler));
			}

		}
	}
}

#endif