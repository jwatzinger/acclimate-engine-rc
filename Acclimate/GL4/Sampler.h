#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class Sampler
			{
			public:
				Sampler(unsigned int min, unsigned int mag, unsigned int u, unsigned int v, float anisotropy);
				~Sampler(void);

				void Bind(unsigned int slot) const;

			private:

				unsigned int m_sampler;
			};

		}
	}
}

#endif