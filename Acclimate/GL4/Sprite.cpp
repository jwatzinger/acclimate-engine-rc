#include "Sprite.h"

#ifdef ACL_API_GL4

#include "Context.h"
#include "VertexBufferObject.h"
#include "IndexBufferObject.h"
#include "..\Math\Vector3.h"
#include "..\Math\Vector2f.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			Sprite::Sprite(Context& context) : m_nextFreeId(0), m_numSprites(20000),
				m_pContext(&context)
			{
				ogl::VertexLayout layout;
				layout.vAttributes.emplace_back(3, 0, ogl::AttributeType::FLOAT, 0); // position
				layout.vAttributes.emplace_back(2, sizeof(float)* 3, ogl::AttributeType::FLOAT, 0); // texcoord
				layout.vAttributes.emplace_back(4, sizeof(float)* 5, ogl::AttributeType::FLOAT, 0); // color

				m_pVertices = new SpriteVertex[m_numSprites * 4];
				m_pVertexBuffer = new VertexBufferObject(sizeof(SpriteVertex), layout, m_numSprites, VboUsage::DYNAMIC);

				//index buffer 

				unsigned int* pBuffer = new unsigned int[m_numSprites * 6];

				unsigned int id = 0;
				for(unsigned int i = 0; i < m_numSprites*6; )
				{
					pBuffer[i++] = id;
					pBuffer[i++] = id + 1;
					pBuffer[i++] = id + 3;
					pBuffer[i++] = id + 3;
					pBuffer[i++] = id + 1;
					pBuffer[i++] = id + 2;
					id+=4;
				}

				m_pIndexBuffer = new IndexBufferObject(m_numSprites * 6 * sizeof(unsigned int), pBuffer, ogl::IboUsage::STATIC);
				
				delete[] pBuffer;

				const math::Vector2& vBackbufferSize = context.GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			Sprite::Sprite(const Sprite& sprite): m_nextFreeId(0), m_vInvHalfScreenSize(sprite.m_vInvHalfScreenSize), m_pContext(sprite.m_pContext), m_numSprites(sprite.m_numSprites)
			{
				ogl::VertexLayout layout;
				layout.vAttributes.emplace_back(3, 0, ogl::AttributeType::FLOAT, 0); // position
				layout.vAttributes.emplace_back(2, sizeof(math::Vector3), ogl::AttributeType::FLOAT, 0); // texcoord
				layout.vAttributes.emplace_back(4, sizeof(math::Vector3) + sizeof(math::Vector2), ogl::AttributeType::FLOAT, 0); // color

				m_pVertices = new SpriteVertex[m_numSprites * 4];
				m_pVertexBuffer = new VertexBufferObject(sizeof(SpriteVertex), layout, m_numSprites, VboUsage::DYNAMIC);

				//index buffer 

				unsigned int* pBuffer = new unsigned int[m_numSprites * 6];

				unsigned int id = 0;
				for(unsigned int i = 0; i < m_numSprites * 6;)
				{
					pBuffer[i++] = id;
					pBuffer[i++] = id + 1;
					pBuffer[i++] = id + 3;
					pBuffer[i++] = id + 3;
					pBuffer[i++] = id + 1;
					pBuffer[i++] = id + 2;
					id += 4;
				}

				m_pIndexBuffer = new IndexBufferObject(m_numSprites * 6 * sizeof(unsigned int), pBuffer, ogl::IboUsage::STATIC);

				delete[] pBuffer;

				const math::Vector2& vBackbufferSize = m_pContext->GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			Sprite::~Sprite(void)
			{
				delete m_pVertexBuffer;
				delete m_pIndexBuffer;
				delete[] m_pVertices;
			}

			void Sprite::Bind(void) const
			{
				m_pVertexBuffer->Bind();
				m_pIndexBuffer->Bind();
			}

			void Sprite::Reset(void)
			{
				m_nextFreeId = 0;

				const math::Vector2& vBackbufferSize = m_pContext->GetBackbufferSize();
				m_vInvHalfScreenSize = math::Vector2f(1.0f / (vBackbufferSize.x * 0.5f), 1.0f / (vBackbufferSize.y * 0.5f));
			}

			void Sprite::Upload(void)
			{
				m_pVertexBuffer->Overwrite(m_nextFreeId*4, m_pVertices, VboUsage::DYNAMIC);
			}

			size_t Sprite::Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const RECT& rSrcRect, const math::Vector2f& vScale, float /*fAngle*/)
			{
				RECT rSrc(rSrcRect);
	
				const math::Vector2 vSrcSize(rSrcRect.right - rSrcRect.left, rSrcRect.bottom - rSrcRect.top);
				const math::Vector2 vSize((int)(vSrcSize.x*vScale.x), (int)(vSrcSize.y*vScale.y));

				const float leftVertex = vPosition.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float rightVertex = leftVertex + vSize.x * m_vInvHalfScreenSize.x;
				const float topVertex = -vPosition.y * m_vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = topVertex - vSize.y * m_vInvHalfScreenSize.y;

				const float leftCoord = rSrc.left / (float)vTextureSize.x;
				const float rightCoord = rSrc.right/(float)vTextureSize.x;
				const float topCoord = rSrc.top / (float)vTextureSize.y;
				const float bottomCoord = rSrc.bottom /(float)vTextureSize.y;

				SpriteVertex Vertices[] =
				{
					{ leftVertex, topVertex, vPosition.z, leftCoord, topCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, topVertex, vPosition.z, rightCoord, topCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, bottomVertex, vPosition.z, rightCoord, bottomCoord, 1.0f, 1.0f, 1.0f, 1.0f },
					{ leftVertex, bottomVertex, vPosition.z, leftCoord, bottomCoord, 1.0f, 1.0f, 1.0f, 1.0f }
				};
				
				memcpy(m_pVertices + m_nextFreeId * 4, Vertices, sizeof(SpriteVertex)* 4);

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(const math::Vector3& vPosition, const math::Vector3& /*vOrigin*/, const math::Vector2f& vScale, float /*angle*/)
			{
				const float leftVertex = vPosition.x  * m_vInvHalfScreenSize.x - 1.0f;
				const float rightVertex = leftVertex + vScale.x * m_vInvHalfScreenSize.x;
				const float topVertex = -vPosition.y * m_vInvHalfScreenSize.y + 1.0f;
				const float bottomVertex = topVertex - vScale.y * m_vInvHalfScreenSize.y;

				SpriteVertex Vertices[] =
				{
					{ leftVertex, topVertex, vPosition.z, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, topVertex, vPosition.z, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ rightVertex, bottomVertex, vPosition.z, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },
					{ leftVertex, bottomVertex, vPosition.z, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f }
				};
				
				memcpy(m_pVertices + m_nextFreeId * 4, Vertices, sizeof(SpriteVertex)* 4);

				return m_nextFreeId++;
			}

			size_t Sprite::Draw(SpriteVertex* pVertices, size_t numQuads)
			{
				memcpy(m_pVertices + m_nextFreeId * 4, pVertices, numQuads * 4 * sizeof(SpriteVertex));

				m_nextFreeId += numQuads;
				return m_nextFreeId - numQuads;
			}

			const math::Vector2f& Sprite::GetInvHalfScreenSize(void) const
			{
				return m_vInvHalfScreenSize;
			}

		}
	}
}

#endif