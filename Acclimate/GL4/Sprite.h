#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include "..\Math\Rect.h"
#include "..\Math\Vector2f.h"

namespace acl
{
    namespace math
    {
        struct Vector3;
    }

	namespace gl4
	{
		namespace ogl
		{

			struct SpriteVertex
			{
				float x, y, z;
				float u, v;
				float r, g, b, a;
			};

			class IndexBufferObject;

			class Sprite
			{
			public:
				Sprite(Context& context);
				Sprite(const Sprite& sprite);
				~Sprite(void);

				void Bind(void) const;
				void Reset(void);
				void Upload(void);

				size_t Draw(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle); 
				size_t Draw(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle);
				size_t Draw(SpriteVertex* pVertices, size_t numVertices);

				const math::Vector2f& GetInvHalfScreenSize(void) const;

			private:

				size_t m_numSprites;
				size_t m_nextFreeId;

				math::Vector2f m_vInvHalfScreenSize;

				VertexBufferObject* m_pVertexBuffer;
				IndexBufferObject* m_pIndexBuffer;
				Context* m_pContext;
				SpriteVertex* m_pVertices;

			};

		}
	}
}

#endif