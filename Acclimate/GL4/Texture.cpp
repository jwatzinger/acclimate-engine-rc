#include "Texture.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\System\Assert.h"
#include "..\..\Extern\GLEW\glew.h"
#include "..\..\Extern\FreeImage\FreeImage.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			int formatToFormat(GLint format)
			{
				switch(format)
				{
				case GL_BGRA:
					return GL_RGBA;
				default:
					return format;
				}
			}

			int formatToDataFormat(GLint format)
			{
				switch(format)
				{
				case GL_RGB:
					return GL_RGB;
				case GL_RGBA:
					return GL_RGBA;
				case GL_BGRA:
					return GL_BGRA;
				case GL_RGBA16F:
					return GL_RGBA;
				case GL_R8:
					return GL_RED;
				case GL_R16:
					return GL_RED;
				case GL_R32F:
					return GL_RED;
				case GL_RG16F:
					return GL_RG;
				case GL_R16F:
					return GL_RED;
				case GL_NONE:
					return GL_NONE;
				default:
					return GL_RGBA;
				}
			}

			int formatToType(GLint format)
			{
				switch(format)
				{
				case GL_RGB:
					return GL_UNSIGNED_BYTE;
				case GL_RGBA:
					return GL_UNSIGNED_BYTE;
				case GL_RGBA16F:
					return GL_HALF_FLOAT;
				case GL_R8:
					return GL_UNSIGNED_BYTE;
				case GL_R16:
					return GL_UNSIGNED_SHORT;
				case GL_R32F:
					return GL_FLOAT;
				case GL_RG16F:
					return GL_HALF_FLOAT;
				case GL_R16F:
					return GL_HALF_FLOAT;
				case GL_NONE:
					return GL_UNSIGNED_BYTE;
				default:
					return GL_UNSIGNED_BYTE;
				}
			}

			int formatToNumByte(GLint format)
			{
				switch(format)
				{
				case GL_RGB:
					return 3;
				case GL_RGBA:
					return 4;
				case GL_RGBA16F:
					return 8;
				case GL_R8:
					return 1;
				case GL_R16:
					return 2;
				case GL_R32F:
					return 4;
				case GL_RG16F:
					return 4;
				case GL_R16F:
					return 2;
				case GL_NONE:
					ACL_ASSERT(false);
				default:
					return 4;
				}
			}

			Texture::Texture(const std::wstring& stFilename, int format) : m_texture(0), m_format(format),
				m_isRenderTarget(false), m_pData(nullptr)
			{
				auto type = FreeImage_GetFileTypeU(stFilename.c_str());
				auto image = FreeImage_LoadU(type, stFilename.c_str(), 0);
				//FreeImage_FlipVertical(image);
				m_vSize.x = FreeImage_GetWidth(image);
				m_vSize.y = FreeImage_GetHeight(image);

				GL_CHECK(glGenTextures(1, &m_texture));
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
				
				unsigned int numByte = 0;
				if(m_format == 0)
				{
					if(FreeImage_GetColorType(image) != FIC_RGBALPHA)
						image = FreeImage_ConvertTo32Bits(image);

					m_format = GL_BGRA;
					numByte = 4;
				}
				else
				{
					numByte = formatToNumByte(m_format);
					switch(numByte)
					{
					case 1:
						image = FreeImage_ConvertTo8Bits(image);
						break;
					case 3:
						image = FreeImage_ConvertTo24Bits(image);
						break;
					}
				}

				auto pBits = FreeImage_GetBits(image);
				GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, formatToFormat(m_format), m_vSize.x, m_vSize.y, 0, formatToDataFormat(m_format), formatToType(m_format), pBits));
				GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));

				m_pitch = FreeImage_GetPitch(image);

				const size_t imageSize = m_vSize.x*m_vSize.y * numByte;
				m_pData = new char[imageSize];
				memcpy(m_pData, pBits, imageSize);

				FreeImage_Unload(image);
			}

			Texture::Texture(unsigned int width, unsigned int height, int format, bool bIsRenderTarget) : m_texture(0), 
				m_format(format), m_vSize(width, height), m_isRenderTarget(bIsRenderTarget), m_pitch(formatToNumByte(format)*width), m_pData(nullptr)
			{
				if(m_format == GL_RGBA)
					m_format = GL_BGRA;

				GL_CHECK(glGenTextures(1, &m_texture));
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
				GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, formatToFormat(m_format), m_vSize.x, m_vSize.y, 0, formatToDataFormat(m_format), formatToType(m_format), nullptr));

				if(!bIsRenderTarget)
				{
					const size_t imageSize = m_vSize.x*m_vSize.y * formatToNumByte(m_format);
					m_pData = new char[imageSize];
				}
			}

			Texture::~Texture(void)
			{
				delete[] m_pData;
				GL_CHECK(glDeleteTextures(1, &m_texture));
			}

			const math::Vector2& Texture::GetSize(void) const
			{
				return m_vSize;
			}

			int Texture::GetFormat(void) const
			{
				return m_format;
			}

			int Texture::GetPitch(void) const
			{
				return m_pitch;
			}

			void* Texture::GetData(void)
			{
				return m_pData;
			}

			const void* Texture::GetData(void) const
			{
				return m_pData;
			}

			void Texture::GenerateMips(void) const
			{
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
				GL_CHECK(glGenerateMipmap(GL_TEXTURE_2D));
			}

			void Texture::Bind(unsigned int slot) const
			{
				GL_CHECK(glActiveTexture(GL_TEXTURE0 + slot));
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
			}

			void Texture::BindTarget(unsigned int slot) const
			{
				ACL_ASSERT(m_isRenderTarget);
				GL_CHECK(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + slot, GL_TEXTURE_2D, m_texture, 0));
			}

			void Texture::Update(void)
			{
				GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
				GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, formatToFormat(m_format), m_vSize.x, m_vSize.y, 0, formatToDataFormat(m_format), formatToType(m_format), m_pData));
			}

			void Texture::Resize(const math::Vector2& vSize)
			{
				if(m_isRenderTarget && vSize != m_vSize)
				{
					m_vSize = vSize;

					GL_CHECK(glBindTexture(GL_TEXTURE_2D, m_texture));
					GL_CHECK(glTexImage2D(GL_TEXTURE_2D, 0, m_format, m_vSize.x, m_vSize.y, 0, formatToDataFormat(m_format), formatToType(m_format), nullptr));
				}
			}

		}
	}
}

#endif

