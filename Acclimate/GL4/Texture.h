#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <string>
#include "..\Math\Vector.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class Texture
			{
			public:
				Texture(const std::wstring& stFilename, int format);
				Texture(unsigned int width, unsigned int height, int format, bool bIsRenderTarget);
				~Texture(void);

				void Bind(unsigned int slot) const;
				void BindTarget(unsigned int slot) const;
				void Update(void);
				void Resize(const math::Vector2& vSize);
				void GenerateMips(void) const;

				const math::Vector2& GetSize(void) const;
				int GetFormat(void) const;
				int GetPitch(void) const;
				void* GetData(void);
				const void* GetData(void) const;

			private:

				int m_format, m_pitch;
				bool m_isRenderTarget;
				unsigned int m_texture;
				math::Vector2 m_vSize;

				void* m_pData;
			};

		}
	}
}

#endif
