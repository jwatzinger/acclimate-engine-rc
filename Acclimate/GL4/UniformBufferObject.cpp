#include "UniformBufferObject.h"

#ifdef ACL_API_GL4

#include <minmax.h>
#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			UniformBufferObject::UniformBufferObject(size_t size) : m_size(size), m_buffer(0)
			{
				GL_CHECK(glGenBuffers(1, &m_buffer));
				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_UNIFORM_BUFFER, m_size, 0, GL_DYNAMIC_DRAW));
				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, 0));
			}

			UniformBufferObject::UniformBufferObject(const UniformBufferObject& ubo) : m_size(ubo.m_size), m_buffer(0)
			{
				GL_CHECK(glGenBuffers(1, &m_buffer));
				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_UNIFORM_BUFFER, m_size, 0, GL_DYNAMIC_DRAW));
				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, 0));
			}

			UniformBufferObject::~UniformBufferObject(void)
			{
				GL_CHECK(glDeleteBuffers(1, &m_buffer));
			}

			size_t UniformBufferObject::GetSize(void) const
			{
				return m_size;
			}

			void UniformBufferObject::Overwrite(void* pData, size_t maxRange)
			{
				maxRange = min(m_size, maxRange);

				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_UNIFORM_BUFFER, maxRange, pData, GL_DYNAMIC_DRAW));
				GL_CHECK(glBindBuffer(GL_UNIFORM_BUFFER, 0));
			}

			void UniformBufferObject::Bind(unsigned int slot) const
			{
				GL_CHECK(glBindBufferBase(GL_UNIFORM_BUFFER, slot, m_buffer));
			}

		}
	}
}

#endif

