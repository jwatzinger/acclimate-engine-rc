#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class UniformBufferObject
			{
			public:
				UniformBufferObject(size_t size);
				UniformBufferObject(const UniformBufferObject& ubo);
				~UniformBufferObject(void);

				size_t GetSize(void) const;
				void Overwrite(void* pData, size_t maxRange);

				void Bind(unsigned int slot) const;

			private:

				unsigned int m_buffer;
				size_t m_size;
			};

		}
	}
}


#endif