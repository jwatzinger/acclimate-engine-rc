#include "VertexArrayObject.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			VertexArrayObject::VertexArrayObject(void): m_vao(0)
			{
				// vertex array object
				GL_CHECK(glGenVertexArrays(1, &m_vao));
			}

			VertexArrayObject::~VertexArrayObject(void)
			{
				GL_CHECK(glDeleteVertexArrays(1, &m_vao));
			}

			void VertexArrayObject::Bind(void) const
			{
				GL_CHECK(glBindVertexArray(m_vao));
			}

			void VertexArrayObject::Unbind(void) const
			{
				GL_CHECK(glBindVertexArray(0));
			}

		}
	}
}

#endif