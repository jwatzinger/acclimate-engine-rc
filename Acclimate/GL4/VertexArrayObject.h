#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			class VertexArrayObject
			{
			public:
				VertexArrayObject(void);
				~VertexArrayObject(void);

				void Bind(void) const;
				void Unbind(void) const;

			private:

				unsigned int m_vao;
			};

		}
	}
}

#endif

