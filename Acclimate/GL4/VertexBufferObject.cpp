#include "VertexBufferObject.h"

#ifdef ACL_API_GL4

#include "Error.h"
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			GLenum convertUsage(VboUsage usage)
			{
				switch(usage)
				{
				case VboUsage::STATIC:
					return GL_STATIC_DRAW;
				case  VboUsage::DYNAMIC:
					return GL_DYNAMIC_DRAW;
				}

				return GL_STATIC_DRAW;
			}

			VertexBufferObject::VertexBufferObject(size_t stride, const VertexLayout& layout, size_t numVertices, VboUsage usage) : 
				VertexBufferObject(stride, layout, numVertices, nullptr, usage)
			{
			}

			VertexBufferObject::VertexBufferObject(size_t stride, const VertexLayout& layout, size_t numVertices, const void* pData, VboUsage usage) : m_stride(stride),
				m_layout(layout), m_usage(usage), m_numVertices(numVertices)
			{
				const size_t size = stride*numVertices;

				GL_CHECK(glGenBuffers(1, &m_buffer));
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_ARRAY_BUFFER, size, pData, convertUsage(usage)));
			}

			VertexBufferObject::VertexBufferObject(const VertexBufferObject& buffer) : VertexBufferObject(buffer.m_stride, buffer.m_layout, buffer.m_numVertices, buffer.m_usage)
			{
			}

			VertexBufferObject::~VertexBufferObject()
			{
				GL_CHECK(glDeleteBuffers(1, &m_buffer));
			}

			void* VertexBufferObject::GetData(void)
			{
				void* pData = nullptr;
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));
				GL_CHECK(pData = glMapBufferRange(GL_ARRAY_BUFFER, 0, m_numVertices*m_stride, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
				return pData;
			}

			const void* VertexBufferObject::GetData(void) const
			{
				void* pData = nullptr;
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));
				GL_CHECK(pData = glMapBufferRange(GL_ARRAY_BUFFER, 0, m_numVertices*m_stride, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT));
				return pData;
			}

			void VertexBufferObject::Bind(void) const
			{
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));

				unsigned int i = m_layout.startSlot;
				for(auto& attribute : m_layout.vAttributes)
				{
					GL_CHECK(glEnableVertexAttribArray(i));
					GL_CHECK(glVertexAttribPointer(i, attribute.numValues, (GLenum)attribute.type, GL_FALSE, m_stride, (const void*)attribute.offset));
					GL_CHECK(glVertexAttribDivisor(i, attribute.instanceDivisor));
					i++;
				}
			}

			void VertexBufferObject::Overwrite(size_t numVertices, const void* pData, VboUsage usage)
			{
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));
				GL_CHECK(glBufferData(GL_ARRAY_BUFFER, m_stride*numVertices, pData, convertUsage(usage)));
				m_usage = usage;
			}

			void VertexBufferObject::Update(void)
			{
				GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_buffer));
				GL_CHECK(glUnmapBuffer(GL_ARRAY_BUFFER));
			}

		}
	}
}

#endif
