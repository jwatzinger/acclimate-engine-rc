#pragma once
#include "..\ApiDef.h"

#ifdef ACL_API_GL4

#include <vector>
#include "..\..\Extern\GLEW\glew.h"

namespace acl
{
	namespace gl4
	{
		namespace ogl
		{

			enum class VboUsage
			{
				STATIC, DYNAMIC
			};

			enum class AttributeType
			{
				FLOAT = GL_FLOAT,
				UBYTE = GL_UNSIGNED_BYTE
			};

			struct VertexAttribute
			{
				VertexAttribute(unsigned int numValues, unsigned int offset, AttributeType type, unsigned int instanceDivisor) : numValues(numValues), offset(offset), type(type),
					instanceDivisor(instanceDivisor)
				{
				}

				unsigned int numValues, offset, instanceDivisor;
				AttributeType type;
			};

			struct VertexLayout
			{
				typedef std::vector<VertexAttribute> AttributeVector;

				VertexLayout(void) : startSlot(0)
				{
				}

				VertexLayout(unsigned int startSlot) : startSlot(startSlot)
				{
				}

				unsigned int startSlot;
				AttributeVector vAttributes;
			};

			class VertexBufferObject
			{
			public:

				VertexBufferObject(size_t stride, const VertexLayout& layout, size_t numVertices, VboUsage usage);
				VertexBufferObject(size_t stride, const VertexLayout& layout, size_t numVertices, const void* pData, VboUsage usage);
				VertexBufferObject(const VertexBufferObject& buffer);
				~VertexBufferObject(void);

				void* GetData(void);
				const void* GetData(void) const;

				void Bind(void) const;
				void Overwrite(size_t numVertices, const void* pData, VboUsage usage);
				void Update(void);

			private:

				unsigned int m_buffer;
				size_t m_stride, m_numVertices;
				VertexLayout m_layout;
				VboUsage m_usage;
			};

		}
	}
}


#endif
