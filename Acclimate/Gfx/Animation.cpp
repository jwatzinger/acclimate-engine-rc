#include "Animation.h"
#include "MeshSkeleton.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		/*******************************
		* Keys
		*******************************/

		TranslationKey::TranslationKey(void) : frame(0)
		{
		}

		TranslationKey::TranslationKey(unsigned int frame, const math::Vector3& vTranslation) : frame(frame), vTranslation(vTranslation)
		{
		}

		RotationKey::RotationKey(void) : frame(0)
		{
		}

		RotationKey::RotationKey(unsigned int frame, const math::Quaternion& rotation) : frame(frame), rotation(rotation)
		{
		}

		ScaleKey::ScaleKey(void) : frame(0)
		{
		}

		ScaleKey::ScaleKey(unsigned int frame, const math::Vector3& vScale) : frame(frame), vScale(vScale)
		{
		}

		/*******************************
		* Bone
		*******************************/

		AnimationBone::AnimationBone(unsigned int bone) : m_bone(bone), m_currentRotation(0), 
			m_currentScale(0), m_currentTranslation(0)
		{
		}

		template<typename Key>
		void insertAtFrame(std::vector<Key>& vTarget, const Key& key)
		{
			if(vTarget.empty())
				vTarget.push_back(key);
			else
			{
				auto pKey = vTarget.begin() + vTarget.size() - 1;
				while(true)
				{
					if(pKey == vTarget.begin() || key.frame >= pKey->frame)
						break;
					pKey++;
				}

				vTarget.insert(pKey + 1, key);
			}
		}

		void AnimationBone::AddTranslationKey(const TranslationKey& key)
		{
			insertAtFrame(m_vTranslation, key);
		}

		void AnimationBone::AddRotationKey(const RotationKey& key)
		{
			insertAtFrame(m_vRotations, key);
		}

		void AnimationBone::AddScaleKey(const ScaleKey& key)
		{
			insertAtFrame(m_vScales, key);
		}

		const AnimationBone::TranslationVector& AnimationBone::GetTranslations(void) const
		{
			return m_vTranslation;
		}

		const AnimationBone::RotationVector& AnimationBone::GetRotations(void) const
		{
			return m_vRotations;
		}

		const AnimationBone::ScaleVector& AnimationBone::GetScales(void) const
		{
			return m_vScales;
		}

		const math::Matrix& AnimationBone::GetTransform(void) const
		{
			return m_mTransform;
		}

		unsigned int AnimationBone::GetBoneId(void) const
		{
			return m_bone;
		}

		void AnimationBone::Update(double time)
		{
			IntergrateState(time);

			const auto transform = CalculateTransform(time);

			m_mTransform = math::MatScale(transform.vScale);
			m_mTransform *= transform.rotation.Matrix();
			m_mTransform *= math::MatTranslation(transform.vTranslation);
		}

		void AnimationBone::UpdateInterpolated(const AnimationBone& bone, float interpolate, double time, double otherTime)
		{
			IntergrateState(time);

			const auto thisTransform = CalculateTransform(time);
			const auto otherTransform = bone.CalculateTransform(otherTime);

			m_mTransform = math::MatScale(thisTransform.vScale * interpolate + otherTransform.vScale * (1.0f - interpolate));
			m_mTransform *= math::interpolate(otherTransform.rotation, thisTransform.rotation, interpolate).Matrix();
			m_mTransform *= math::MatTranslation(thisTransform.vTranslation * interpolate + otherTransform.vTranslation * (1.0f - interpolate));
		}

		void AnimationBone::Reset(void)
		{
			m_currentTranslation = 0;
			m_currentRotation = 0;
			m_currentScale = 0;
		}

		void AnimationBone::IntergrateState(double time)
		{
			while(m_currentTranslation+1 < m_vTranslation.size() && time >= m_vTranslation[m_currentTranslation + 1].frame)
				m_currentTranslation++;

			while(m_currentRotation+1 < m_vRotations.size() && time >= m_vRotations[m_currentRotation + 1].frame)
				m_currentRotation++;

			while(m_currentScale+1 < m_vScales.size() && time >= m_vScales[m_currentScale + 1].frame)
				m_currentScale++;
		}

		AnimationBone::TransformComponents AnimationBone::CalculateTransform(double time) const
		{
			TransformComponents transform;

			// translation
			if(!m_vTranslation.empty())
			{
				if(m_currentTranslation + 1 >= m_vTranslation.size())
					transform.vTranslation = m_vTranslation.rbegin()->vTranslation;
				else
				{
					const auto& currentTranslation = m_vTranslation[m_currentTranslation];
					const auto& nextTranslation = m_vTranslation[m_currentTranslation + 1];

					const float alpha = (time - currentTranslation.frame) / (nextTranslation.frame - currentTranslation.frame);
					ACL_ASSERT(alpha <= 1.0f);

					const math::Vector3 vDifference = nextTranslation.vTranslation - currentTranslation.vTranslation;
					transform.vTranslation = currentTranslation.vTranslation + vDifference*alpha;
				}
			}

			// rotation
			if(!m_vRotations.empty())
			{
				if(m_currentRotation + 1 >= m_vRotations.size())
					transform.rotation = m_vRotations.rbegin()->rotation;
				else
				{
					const auto& currentRotation = m_vRotations[m_currentRotation];
					const auto& nextRotation = m_vRotations[m_currentRotation + 1];

					const float alpha = (time - currentRotation.frame) / (nextRotation.frame - currentRotation.frame);
					ACL_ASSERT(alpha <= 1.0f);

					transform.rotation = math::interpolate(currentRotation.rotation, nextRotation.rotation, alpha);
					transform.rotation.w = -transform.rotation.w;
				}
			}

			// rotation
			if(!m_vScales.empty())
			{
				if(m_currentScale + 1 >= m_vScales.size())
					transform.vScale = m_vScales.rbegin()->vScale;
				else
				{
					const auto& currentScale = m_vScales[m_currentScale];
					const auto& nextScale = m_vScales[m_currentScale + 1];

					const float alpha = (time - currentScale.frame) / (nextScale.frame - currentScale.frame);
					ACL_ASSERT(alpha <= 1.0f);

					const math::Vector3 vDifference = nextScale.vScale - currentScale.vScale;
					transform.vScale = currentScale.vScale + vDifference*alpha;
				}
			}
			else
				transform.vScale = math::Vector3(1.0f, 1.0f, 1.0f);

			return transform;
		}

		/*******************************
		* Animation
		*******************************/

		Animation::Animation(unsigned int duration) : m_duration(duration),
			m_numPlayed(0)
		{
		}

		void Animation::AddBone(const AnimationBone& bone)
		{
			m_vBones.push_back(bone);
		}

		void Animation::Reset(void)
		{
			for(auto& bone : m_vBones)
			{
				bone.Reset();
			}
		}

		void Animation::Update(double time)
		{
			const unsigned int numPlayed = (unsigned int)(time / m_duration);
			if(numPlayed != m_numPlayed)
			{
				for(auto& bone : m_vBones)
				{
					bone.Reset();
				}
				m_numPlayed = numPlayed;
			}

			for(auto& bone : m_vBones)
			{
				bone.Update((unsigned int)time % m_duration);
			}
		}

		void Animation::UpdateInterpolated(const Animation& animation, float interpolate, double thisTime, double otherTime)
		{
			const unsigned int numPlayed = (unsigned int)(thisTime / m_duration);
			if(numPlayed != m_numPlayed)
			{
				for(auto& bone : m_vBones)
				{
					bone.Reset();
				}
				m_numPlayed = numPlayed;
			}

			const auto numBones = animation.m_vBones.size()-1;
			for(auto& bone : m_vBones)
			{
				const auto id = bone.GetBoneId();

				if(animation.m_vBones.size() > id)
					bone.UpdateInterpolated(animation.m_vBones[numBones - id], interpolate, (unsigned int)thisTime % m_duration, (unsigned int)otherTime % animation.m_duration);
				else
					bone.Update((unsigned int)thisTime % m_duration);
			}
		}

		void Animation::ApplyToSkeleton(MeshSkeleton& skeleton) const
		{
			for(auto& bone : m_vBones)
			{
				skeleton.SetTransform(bone.GetBoneId(), bone.GetTransform());
			}
		}

		const Animation::BoneVector& Animation::GetBones(void) const
		{
			return m_vBones;
		}

		size_t Animation::GetNumBones(void) const
		{
			return m_vBones.size();
		}

		unsigned int Animation::GetDuration(void) const
		{
			return m_duration;
		}

		/*******************************
		* AnimationSet
		*******************************/

		void AnimationSet::AddAnimation(const std::wstring& stName, const Animation& animation)
		{
			m_mAnimations.emplace(stName, animation);
		}

		AnimationSet::AnimationMap& AnimationSet::GetAnimations(void)
		{
			return m_mAnimations;
		}

		const AnimationSet::AnimationMap& AnimationSet::GetAnimations(void) const
		{
			return m_mAnimations;
		}

		size_t AnimationSet::GetNumAnimations(void) const
		{
			return m_mAnimations.size();
		}

	}
}

