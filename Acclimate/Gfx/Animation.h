#pragma once
#include <vector>
#include <unordered_map>
#include "..\Core\Dll.h"
#include "..\Math\Vector3.h"
#include "..\Math\Quaternion.h"

namespace acl
{
	namespace gfx
	{

		/*******************************
		* Keys
		*******************************/

		struct ACCLIMATE_API TranslationKey
		{
			TranslationKey(void);
			TranslationKey(unsigned int frame, const math::Vector3& vTranslation);

			unsigned int frame;
			math::Vector3 vTranslation;
		};

		struct ACCLIMATE_API RotationKey
		{
			RotationKey(void);
			RotationKey(unsigned int frame, const math::Quaternion& rotation);

			unsigned int frame;
			math::Quaternion rotation;
		};

		struct ACCLIMATE_API ScaleKey
		{
			ScaleKey(void);
			ScaleKey(unsigned int frame, const math::Vector3& vScale);

			unsigned int frame;
			math::Vector3 vScale;
		};

		/*******************************
		* Bone
		*******************************/

		class ACCLIMATE_API AnimationBone
		{
			typedef std::vector<TranslationKey> TranslationVector;
			typedef std::vector<RotationKey> RotationVector;
			typedef std::vector<ScaleKey> ScaleVector;

		public:

			AnimationBone(unsigned int bone);

			void AddTranslationKey(const TranslationKey& key);
			void AddRotationKey(const RotationKey& key);
			void AddScaleKey(const ScaleKey& key);

			const TranslationVector& GetTranslations(void) const;
			const RotationVector& GetRotations(void) const;
			const ScaleVector& GetScales(void) const;

			const math::Matrix& GetTransform(void) const;
			unsigned int GetBoneId(void) const;

			void Update(double time);
			void UpdateInterpolated(const AnimationBone& bone, float interpolate, double time, double otherTime);
			void Reset(void);

		private:
#pragma warning( disable: 4251 )
			struct TransformComponents
			{
				math::Vector3 vTranslation;
				math::Quaternion rotation;
				math::Vector3 vScale;
			};

			void IntergrateState(double time);
			TransformComponents CalculateTransform(double time) const;

			unsigned int m_bone, m_currentTranslation, m_currentRotation, m_currentScale;
			TranslationVector m_vTranslation;
			RotationVector m_vRotations;
			ScaleVector m_vScales;
			math::Matrix m_mTransform;
#pragma warning( default: 4251 )
		};

		/*******************************
		* Animation
		*******************************/

		class MeshSkeleton;

		class ACCLIMATE_API Animation
		{
			typedef std::vector<AnimationBone> BoneVector;
		public:

			Animation(unsigned int duration);

			void AddBone(const AnimationBone& bone);

			void Reset(void);
			void Update(double time);
			void UpdateInterpolated(const Animation& animation, float interpolate, double thisTime, double otherTime);
			void ApplyToSkeleton(MeshSkeleton& skeleton) const;

			const BoneVector& GetBones(void) const;
			size_t GetNumBones(void) const;
			unsigned int GetDuration(void) const;

		private:
#pragma warning( disable: 4251 )

			unsigned int m_duration, m_numPlayed;
			BoneVector m_vBones;
#pragma warning( default: 4251 )
		};

		class ACCLIMATE_API AnimationSet
		{
			typedef std::unordered_map<std::wstring, Animation> AnimationMap;
		public:
			void AddAnimation(const std::wstring& stName, const Animation& animation);

			AnimationMap& GetAnimations(void);
			const AnimationMap& GetAnimations(void) const;
			size_t GetNumAnimations(void) const;

		private:

#pragma warning( disable: 4251 )
			AnimationMap m_mAnimations;
#pragma warning( default: 4251 )
		};

	}
}

