#include "AnimationController.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		AnimationController::AnimationController(const MeshSkeleton& skeleton, const AnimationSet& set) : m_currentTime(0.0f),
			m_set(set), m_skeleton(skeleton), m_pActiveAnimation(nullptr), m_speed(1.0f), m_blendTime(0.0f), m_blendState(0.0f),
			m_pLastAnimation(nullptr), m_lastTime(0.0f)
		{
			m_skeleton.SetupMatrices();
		}
		
		void AnimationController::SetSpeed(float speed)
		{
			m_speed = speed;
		}

		void AnimationController::ActivateAnimation(const std::wstring& stName, float blendTime)
		{
			if(stName.empty())
				m_pActiveAnimation = nullptr;
			else
			{
				auto itr = m_set.GetAnimations().find(stName);
				if(itr != m_set.GetAnimations().end())
				{
					if(m_pActiveAnimation != &itr->second)
					{
						if(m_pActiveAnimation)
						{
							if(blendTime != 0.0f)
							{
								m_pLastAnimation = m_pActiveAnimation;
								m_blendState = 0.0f;
								m_lastTime = m_currentTime;
								m_blendTime = blendTime;
							}
							else
								m_pActiveAnimation->Reset();
						}

						m_pActiveAnimation = &itr->second;
					}
				}
				else
					sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Activating animation", stName, "failed. Animation is not present in set.");
			}
		}

		void AnimationController::Run(double dt)
		{
			if(m_pActiveAnimation)
			{
				m_currentTime += dt * m_speed;

				if(m_pLastAnimation)
				{
					if(m_blendState >= m_blendTime)
					{
						m_pLastAnimation->Reset();
						m_pLastAnimation = nullptr;

						m_pActiveAnimation->Update(m_currentTime * 4000);
					}
					else
						m_pActiveAnimation->UpdateInterpolated(*m_pLastAnimation, m_blendState / m_blendTime, m_currentTime * 4000, m_lastTime * 4000);

					m_blendState += dt;
				}
				else
					m_pActiveAnimation->Update(m_currentTime * 4000);

				m_pActiveAnimation->ApplyToSkeleton(m_skeleton);

				m_skeleton.Update();
			}
		}

		const MeshSkeleton::MatrixVector& AnimationController::GetMatrices(void) const
		{
			return m_skeleton.GetMatrices();
		}

	}
}

