#pragma once
#include <string>
#include "Animation.h"
#include "MeshSkeleton.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		class ACCLIMATE_API AnimationController
		{
		public:
			AnimationController(const MeshSkeleton& skeleto, const AnimationSet& animationSet);

			void SetSpeed(float speed);

			void Run(double dt);
			void ActivateAnimation(const std::wstring& stName, float blendTime);
			const MeshSkeleton::MatrixVector& GetMatrices(void) const;

		private:

			float m_currentTime, m_speed;
			float m_blendState, m_blendTime, m_lastTime;

			MeshSkeleton m_skeleton;
			AnimationSet m_set;
			Animation* m_pActiveAnimation, *m_pLastAnimation;
		};

	}
}

