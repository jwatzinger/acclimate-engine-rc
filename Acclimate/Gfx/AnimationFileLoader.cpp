#include "AnimationFileLoader.h"
#include <fstream>
#include "AnimationFilePlainLoader.h"
//#include "MeshFileBinaryLoader.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace gfx
	{

		AnimationSet& AnimationFileLoader::Load(const std::wstring& stFilename) const
		{
			std::fstream stream;
			stream.open(stFilename, std::ios::in | std::ios::binary);

			if(!stream.is_open())
				throw fileException();
			
			/*******************************************
			* Read header line
			*******************************************/

			char firstLine[25];
			stream.read(firstLine, 25);
			firstLine[24] = '\0';

			if(!strcmp(firstLine,"Acclimate animation file")) // file description + magic number
			{
				char magicNumber[7];
				stream.read(magicNumber, 6);
				magicNumber[6] = '\0';

				if(!strcmp(magicNumber, "881109"))
				{
					AnimationFilePlainLoader loader;
					stream.close(); // todo: better solution
					stream.open(stFilename, std::ios::in);
					char temp[31];
					stream.read(temp, 31);
					return loader.Load(stream);
				}
				else if(!strcmp(magicNumber, "286793"))
				{
					//MeshFileBinaryLoader loader;
					//return loader.Load(stream);
				}
			}

			throw fileException();
		}

	}
}