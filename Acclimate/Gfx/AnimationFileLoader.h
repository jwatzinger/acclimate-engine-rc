#pragma once
#include <string>
#include "Animation.h"

namespace acl
{
	namespace gfx
	{

		class AnimationFileLoader
		{
		public:

			AnimationSet& Load(const std::wstring& stFilename) const;
		};

	}
}
