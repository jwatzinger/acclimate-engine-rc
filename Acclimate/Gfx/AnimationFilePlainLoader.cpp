#include "AnimationFilePlainLoader.h"
#include <sstream>
#include "..\System\Assert.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gfx
	{

		enum KeyframeTypes
		{
			NONE = 0,
			TRANSLATION = 1,
			ROTATION = 2,
			SCALE = 4,
		};

		AnimationSet& AnimationFilePlainLoader::Load(std::fstream& s) const
		{
			s.seekg(31, std::ios::end);
			const size_t dataLength = (size_t)s.tellg();
			s.seekg(31, std::ios::beg);

			char* pData = new char[dataLength];
			s.read(pData, dataLength);

			std::string stShader(pData, dataLength);
			delete[] pData;

			std::stringstream stream(stShader);

			AnimationSet* pSet = new AnimationSet();

			// read animations keyword
			std::string stAnimations;
			stream >> stAnimations;
			
			ACL_ASSERT(stAnimations == "Animations");

			// read animation count
			std::string stNumAnimations;
			stream >> stNumAnimations;
			const unsigned int numAnimations = conv::FromString<unsigned int>(stNumAnimations);

			auto& animations = pSet->GetAnimations();

			for(unsigned int i = 0; i < numAnimations; i++)
			{
				// read animation keyword
				std::string stAnimation;
				stream >> stAnimation;

				ACL_ASSERT(stAnimation == "Animation");

				// read animation name
				std::string stAnimationName;
				stream >> stAnimationName;

				ACL_ASSERT(stAnimationName[0] == '\"' && stAnimationName[stAnimationName.size() - 1] == '\"');
				stAnimationName.erase(0, 1);
				stAnimationName.pop_back();

				std::string stAnimationDuration;
				stream >> stAnimationDuration;
				const unsigned int animationDuration = conv::FromString<unsigned int>(stAnimationDuration);

				const std::wstring& stAnimationNameW = conv::ToW(stAnimationName);
				animations.emplace(stAnimationNameW, animationDuration);
				auto& animation = animations.at(stAnimationNameW);

				ACL_ASSERT(animationDuration < 99999);

				std::string stNumBones;
				stream >> stNumBones;
				const unsigned int numBones = conv::FromString<unsigned int>(stNumBones);

				for(unsigned int j = 0; j < numBones; j++)
				{
					// read bone keyword
					std::string stBone;
					stream >> stBone;

					ACL_ASSERT(stBone == "Bone");

					// read bone id
					std::string stBoneId;
					stream >> stBoneId;
					const unsigned int boneId = conv::FromString<unsigned int>(stBoneId);

					AnimationBone bone(boneId);

					// read frame type (translation, rotation, scale)
					std::string stFrameType;
					stream >> stFrameType;
					const unsigned int frameType = conv::FromString<unsigned int>(stFrameType);

					if(frameType & TRANSLATION)
					{
						// read translation keyword
						std::string stTranslation;
						stream >> stTranslation;

						ACL_ASSERT(stTranslation == "Translate");

						// read translation count
						std::string stNumTranslations;
						stream >> stNumTranslations;
						const unsigned int numTranslations = conv::FromString<unsigned int>(stNumTranslations);

						for(unsigned int k = 0; k < numTranslations; k++)
						{
							TranslationKey key;
							// read frame
							std::string stFrame;
							stream >> stFrame;
							key.frame = conv::FromString<unsigned int>(stFrame);

							// read vector elements
							std::string stX;
							stream >> stX;
							key.vTranslation.x = conv::FromString<float>(stX);

							std::string stY;
							stream >> stY;
							key.vTranslation.y = conv::FromString<float>(stY);

							std::string stZ;
							stream >> stZ;
							key.vTranslation.z = conv::FromString<float>(stZ);

							bone.AddTranslationKey(key);
						}
					}

					if(frameType & ROTATION)
					{
						// read translation keyword
						std::string stRotation;
						stream >> stRotation;

						ACL_ASSERT(stRotation == "Rotate");

						// read translation count
						std::string stNumRotations;
						stream >> stNumRotations;
						const unsigned int numRotations = conv::FromString<unsigned int>(stNumRotations);

						for(unsigned int k = 0; k < numRotations; k++)
						{
							RotationKey key;
							// read frame
							std::string stFrame;
							stream >> stFrame;
							key.frame = conv::FromString<unsigned int>(stFrame);

							// read quaternion elements
							std::string stW;
							stream >> stW;
							key.rotation.w = conv::FromString<float>(stW);

							std::string stX;
							stream >> stX;
							key.rotation.x = conv::FromString<float>(stX);

							std::string stY;
							stream >> stY;
							key.rotation.y = conv::FromString<float>(stY);

							std::string stZ;
							stream >> stZ;
							key.rotation.z = conv::FromString<float>(stZ);

							bone.AddRotationKey(key);
						}
					}

					if(frameType & SCALE)
					{
						// read translation keyword
						std::string stScale;
						stream >> stScale;

						ACL_ASSERT(stScale == "Scale");

						// read translation count
						std::string stNumScales;
						stream >> stNumScales;
						const unsigned int numScales = conv::FromString<unsigned int>(stNumScales);

						for(unsigned int k = 0; k < numScales; k++)
						{
							ScaleKey key;
							// read frame
							std::string stFrame;
							stream >> stFrame;
							key.frame = conv::FromString<unsigned int>(stFrame);

							// read vector elements
							std::string stX;
							stream >> stX;
							key.vScale.x = conv::FromString<float>(stX);

							std::string stY;
							stream >> stY;
							key.vScale.y = conv::FromString<float>(stY);

							std::string stZ;
							stream >> stZ;
							key.vScale.z = conv::FromString<float>(stZ);

							bone.AddScaleKey(key);
						}
					}

					animation.AddBone(bone);
				}
			}

			return *pSet;
		}

	}
}

