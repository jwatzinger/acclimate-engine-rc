#pragma once
#include <fstream>
#include "AnimationController.h"

namespace acl
{
	namespace gfx
	{

		class AnimationFilePlainLoader
		{
		public:

			AnimationSet& Load(std::fstream& stream) const;
		};

	}
}

