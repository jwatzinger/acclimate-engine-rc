#include "AnimationFileSaver.h"
#include <fstream>
#include "Animation.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gfx
	{

		void AnimationFileSaver::Save(const AnimationSet& set, const std::wstring& stFilename) const
		{
			std::fstream stream;
			stream.open(stFilename, std::ios::out);

			stream.write("Acclimate animation file 881109", 31); // file header + magic number
			stream.put('\n');
			stream.put('\n'); // first empty line

			/*******************************************
			* Write vertices
			*******************************************/

			stream.write("Animations ", 11);
			
			// animation count
			const size_t numAnimations = set.GetNumAnimations();
			const std::string stNumAnimations = conv::ToStringA(numAnimations) + " ";
			stream.write(stNumAnimations.c_str(), stNumAnimations.size());

			stream.put('\n'); // new line
			stream.put('\n');

			for(auto animation : set.GetAnimations())
			{
				stream.write("Animation ", 10);

				stream.put('\"');
				stream.write(conv::ToA(animation.first).c_str(), conv::ToA(animation.first).size());
				stream.put('\"');
				stream.put(' ');

				const std::string stDuration = conv::ToStringA(animation.second.GetDuration());
				stream.put(' ');
				stream.write(stDuration.c_str(), stDuration.size());
				stream.put(' ');

				auto& anim = animation.second;
				
				const std::string stNumBones = conv::ToStringA(anim.GetNumBones());
				stream.write(stNumBones.c_str(), stNumBones.size());
				stream.put('\n');
				stream.put('\n');

				for(auto& bone : anim.GetBones())
				{
					stream.write("Bone ", 5);

					const std::string stBoneId = conv::ToStringA(bone.GetBoneId());
					stream.write(stBoneId.c_str(), stBoneId.size());
					stream.put(' ');

					unsigned int options = 0;
					const auto& vTranslations = bone.GetTranslations();
					if(!vTranslations.empty())
						options += 1;
					const auto& vRotations = bone.GetRotations();
					if(!vRotations.empty())
						options += 2;
					const auto& vScales = bone.GetScales();
					if(!vScales.empty())
						options += 4;

					const std::string stOptions = conv::ToStringA(options);
					stream.write(stOptions.c_str(), stOptions.size());
					stream.put('\n');

					if(!vTranslations.empty())
					{
						stream.write("Translate ", 10);

						const std::string stNumTranslations = conv::ToStringA(vTranslations.size());
						stream.write(stNumTranslations.c_str(), stNumTranslations.size());
						stream.put('\n');

						for(auto& translation : vTranslations)
						{
							const std::string stFrame = conv::ToStringA(translation.frame);
							stream.write(stFrame.c_str(), stFrame.size());
							stream.put(' ');
							
							const std::string stX = conv::ToStringA(translation.vTranslation.x, 5);
							stream.write(stX.c_str(), stX.size());
							stream.put(' ');

							const std::string stY = conv::ToStringA(translation.vTranslation.y, 5);
							stream.write(stY.c_str(), stY.size());
							stream.put(' ');

							const std::string stZ = conv::ToStringA(translation.vTranslation.z, 5);
							stream.write(stZ.c_str(), stZ.size());
							stream.put('\n');
						}
					}

					if(!vRotations.empty())
					{
						stream.write("Rotate ", 7);

						const std::string stNumRotations = conv::ToStringA(vRotations.size());
						stream.write(stNumRotations.c_str(), stNumRotations.size());
						stream.put('\n');

						for(auto& rotation : vRotations)
						{
							const std::string stFrame = conv::ToStringA(rotation.frame);
							stream.write(stFrame.c_str(), stFrame.size());
							stream.put(' ');

							const std::string stW = conv::ToStringA(rotation.rotation.w, 5);
							stream.write(stW.c_str(), stW.size());
							stream.put(' ');

							const std::string stX = conv::ToStringA(rotation.rotation.x, 5);
							stream.write(stX.c_str(), stX.size());
							stream.put(' ');

							const std::string stY = conv::ToStringA(rotation.rotation.y, 5);
							stream.write(stY.c_str(), stY.size());
							stream.put(' ');

							const std::string stZ = conv::ToStringA(rotation.rotation.z, 5);
							stream.write(stZ.c_str(), stZ.size());
							stream.put('\n');
						}
					}

					if(!vScales.empty())
					{
						stream.write("Scale ", 6);

						const std::string stNumScales = conv::ToStringA(vScales.size());
						stream.write(stNumScales.c_str(), stNumScales.size());
						stream.put('\n');

						for(auto& scale : vScales)
						{
							const std::string stFrame = conv::ToStringA(scale.frame);
							stream.write(stFrame.c_str(), stFrame.size());
							stream.put(' ');

							const std::string stX = conv::ToStringA(scale.vScale.x, 5);
							stream.write(stX.c_str(), stX.size());
							stream.put(' ');

							const std::string stY = conv::ToStringA(scale.vScale.y, 5);
							stream.write(stY.c_str(), stY.size());
							stream.put(' ');

							const std::string stZ = conv::ToStringA(scale.vScale.z, 5);
							stream.write(stZ.c_str(), stZ.size());
							stream.put('\n');
						}
					}
				}
			}
			
			stream.close();
		}

	}
}