#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{
		
		class AnimationSet;

		class ACCLIMATE_API AnimationFileSaver
		{
		public:
			
			void Save(const AnimationSet& set, const std::wstring& stFilename) const;
		};

	}
}

