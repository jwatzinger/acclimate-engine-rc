#include "AnimationLoader.h"

namespace acl
{
	namespace gfx
	{
		
		AnimationLoader::AnimationLoader(Animations& animations) : m_pAnimations(&animations)
		{
		}

		void AnimationLoader::LoadAnimation(const std::wstring& stName, const std::wstring& stFile) const
		{
			AnimationSet& set = m_loader.Load(stFile);
			m_pAnimations->Add(stName, set);
		}

	}
}