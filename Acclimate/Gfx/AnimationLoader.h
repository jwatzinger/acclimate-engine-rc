#pragma once
#include "Animations.h"
#include "AnimationFileLoader.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		class AnimationSet;

		class ACCLIMATE_API AnimationLoader
		{
		public:

			AnimationLoader(Animations& animations);

			void LoadAnimation(const std::wstring& stName, const std::wstring& stFile) const;

		private:

			AnimationFileLoader m_loader;
			Animations* m_pAnimations;
		};

	}
}