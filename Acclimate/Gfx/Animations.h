#pragma once
#include <string>
#include "Animation.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, AnimationSet> Animations;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, AnimationSet>;

    }
}