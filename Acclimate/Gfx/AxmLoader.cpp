#include "AxmLoader.h"
#include "Screen.h"
#include "IMaterialLoader.h"
#include "IMaterial.h"
#include "IMeshLoader.h"
#include "ITextureLoader.h"
#include "IEffectLoader.h"
#include "IFontLoader.h"
#include "IModelLoader.h"
#include "IZBufferLoader.h"
#include "Context.h"
#include "IEffect.h"
#include "AnimationLoader.h"
#include "..\File\File.h"
#include "..\System\Exception.h"
#include "..\System\Log.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
    namespace gfx
    {

		AxmLoader::AxmLoader(const Screen& screen, const LoadContext& ctx) : m_pScreen(&screen), m_ctx(ctx)
        {
        }

        void AxmLoader::Load(const std::wstring& stFilename) const
        {
			sys::log->Out(sys::LogModule::GFX, sys::LogType::INFO, "loading gfx resource file", file::FullPath(stFilename));

			xml::Doc doc;
			try
			{
				doc.LoadFile(stFilename.c_str());
			}
			catch(...)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "Failed to load gfx resource file", file::FullPath(stFilename));
				throw fileException();
			}

			sys::WorkingDirectory dir(stFilename, true);

			const xml::Node* pResources = doc.Root(L"Resources");

			if(auto pPath = pResources->Attribute(L"path"))
				dir = sys::WorkingDirectory(pPath->GetValue());

            // load textures 
			LoadTextures(*pResources);

			// load effects
			LoadEffects(*pResources);

			//load materials

			if(const xml::Node* pMaterials = pResources->FirstNode(L"Materials"))
			{
				//loop through material list
				if(const xml::Node::NodeVector* pMaterialList = pMaterials->Nodes(L"Material"))
                {
				    for(auto pMaterial : *pMaterialList)
				    {
					    //load material from file, including used effect
						auto pEffect = pMaterial->FirstNode(L"Effect");
						const std::wstring& stEffect = pEffect->GetValue();

						unsigned int permutation = 0;
						if(auto pPermutation = pEffect->Attribute(L"perm"))
							permutation = pPermutation->AsInt();

					    const std::wstring& stName = pMaterial->Attribute(L"name")->GetValue();

						TextureVector vTextures;
					    if(const xml::Node::NodeVector* pTextures = pMaterial->Nodes(L"Texture") )
					    {
						    //loop through materials textures to assign them to texture slot
						    for(auto pTexture : *pTextures)
						    {
							    vTextures.push_back(pTexture->GetValue());
						    }
					    }

						for(unsigned int i = vTextures.size(); i < 4; i++)
						{
							vTextures.push_back(L"");
						}

						IMaterialLoader::SubsetVector vSubsets;
						// load subsets
						if(const xml::Node* pSubsets = pMaterial->FirstNode(L"Subsets"))
						{
							if(const auto pSubsetList = pSubsets->Nodes(L"Subset"))
							{
								size_t subsetCount = 0;
								for(auto pSubset : *pSubsetList)
								{
									vSubsets.resize(vSubsets.size()+1);
									
									//textures 
									if( const xml::Node::NodeVector* pTextures = pSubset->Nodes(L"Texture") )
									{
										//loop through materials textures to assign them to texture slot
										for(auto& texture : *pTextures)
										{
											vSubsets[subsetCount].vTextures.push_back(texture->GetValue());
										}
									}

									// vertex textures
									if( const xml::Node::NodeVector* pVertexTextures = pSubset->Nodes(L"VertexTexture") )
									{
										//loop through materials textures to assign them to texture slot
										for(auto& texture : *pVertexTextures)
										{
											vSubsets[subsetCount].vVertexTextures.push_back(texture->GetValue());
										}
									}

									subsetCount++;
								}
							}
						}

                        m_ctx.materials.Load( stName , stEffect, permutation, vTextures, &vSubsets);
				    }
                }
			}

			//load meshes
			LoadMeshes(*pResources);

            // load fonts
			LoadFonts(*pResources);

			// load model
			if( const xml::Node* pModels = pResources->FirstNode(L"Models") )
			{
                if( const xml::Node::NodeVector* pModelList = pModels->Nodes(L"Model"))
                {
				    for( auto pModel : *pModelList )
				    {
						const std::wstring& stName = pModel->Attribute(L"name")->GetValue();
                        const std::wstring& stMesh = pModel->Attribute(L"mesh")->GetValue();

						IModelLoader::PassVector vPasses;
						
						if(const xml::Node::NodeVector* pPasses = pModel->Nodes(L"Pass"))
						{
							for(auto pass : *pPasses)
							{
								size_t id = pass->Attribute(L"id")->AsInt();

								vPasses.emplace_back(id, pass->Attribute(L"material")->GetValue());
							}
						}

						m_ctx.models.Load(stName, stMesh, vPasses);
				    }
                }
			}

			// load zbuffer

			if(const xml::Node* pZBuffer = pResources->FirstNode(L"ZBuffers"))
			{
				if(const xml::Node::NodeVector* pZBufferList = pZBuffer->Nodes(L"ZBuffer"))
				{
					for(auto pZBuffer : *pZBufferList)
					{
                        const xml::Attribute* pWidth = pZBuffer->Attribute(L"w");
                        const xml::Attribute* pHeight = pZBuffer->Attribute(L"h");

                        math::Vector2 vSize;
                        if(!pWidth || !pHeight)
                            vSize = m_pScreen->GetSize();
                        else
                            vSize = math::Vector2(pWidth->AsInt(), pHeight->AsInt());

						m_ctx.zbuffers.Create(pZBuffer->GetValue(), vSize);
					}
				}
			}

			// load animations
			if(const xml::Node* pAnimations = pResources->FirstNode(L"Animations"))
			{
				if(const auto pAnimationList = pAnimations->Nodes(L"Animation"))
				{
					sys::WorkingDirectory dir;

					if(const xml::Attribute* pPath = pAnimations->Attribute(L"path"))
						dir = sys::WorkingDirectory(pPath->GetValue());

					for(auto pAnimation : *pAnimationList)
					{
						const std::wstring& stName = pAnimation->GetValue();
						const std::wstring& stFile = pAnimation->Attribute(L"file")->GetValue();

						m_ctx.animations.LoadAnimation(stName, stFile);
					}
				}
			}
        }

		void AxmLoader::LoadTextures(const xml::Node& resources) const
		{
			if(const xml::Node* pTextures = resources.FirstNode(L"Textures"))
			{
				sys::WorkingDirectory dir;

				if(const xml::Attribute* pPath = pTextures->Attribute(L"path"))
					dir = sys::WorkingDirectory(pPath->GetValue());

				if(const xml::Node::NodeVector* pTextureList = pTextures->Nodes(L"Texture"))
                {
				    for(auto pTexture : *pTextureList)
				    {
					    std::wstring stFormat = L"";
					    if(pTexture->Attribute(L"format"))
						    stFormat = pTexture->Attribute(L"format")->GetValue();
				
					    //switch on stFormat to get corrent texture format
					    TextureFormats fmt;
					    if(stFormat == L"X32")
						    fmt = TextureFormats::X32;
                        else if(stFormat == L"A32")
                            fmt = TextureFormats::A32;
					    else if(stFormat == L"A16F")
						    fmt = TextureFormats::A16F;
						else if(stFormat == L"A32F")
							fmt = TextureFormats::A32F;
					    else if(stFormat == L"L8")
						    fmt = TextureFormats::L8;
						else if(stFormat == L"L16")
						    fmt = TextureFormats::L16;
                        else if(stFormat == L"R32")
                            fmt = TextureFormats::R32;
						else if(stFormat == L"GR16F")
							fmt = TextureFormats::GR16F;
						else if(stFormat == L"R16F")
							fmt = TextureFormats::R16F;
					    else
						    fmt = TextureFormats::UNKNOWN;

					    if( const xml::Attribute* pAttribute = pTexture->Attribute(L"file"))
					    {
							bool bRead = false;
							if(auto pRead= pTexture->Attribute(L"read"))
								bRead = pRead->AsBool();
							
							const std::wstring& stName = pTexture->GetValue();
							const std::wstring& stFile = pAttribute->GetValue();

                            m_ctx.textures.Load(stName, stFile, bRead, fmt);
					    }
					    else
					    {
                            const xml::Attribute* pWidth = pTexture->Attribute(L"w");
                            const xml::Attribute* pHeight = pTexture->Attribute(L"h");

                            math::Vector2 vSize;
                            if(!pWidth || !pHeight)
							{
								if(auto pScale = pTexture->Attribute(L"scale"))
									vSize = m_pScreen->GetSize() * pScale->AsFloat();
								else
									vSize = m_pScreen->GetSize();
							}
                            else
                                vSize = math::Vector2(pWidth->AsInt(), pHeight->AsInt());

							LoadFlags flag = LoadFlags::NONE;
                            if(auto pFlag = pTexture->Attribute(L"flags"))
								flag = static_cast<LoadFlags>(pFlag->AsInt());

							m_ctx.textures.Create(pTexture->GetValue(), vSize, fmt, flag);
					    }
				    }
                }	
			}
		}

		void AxmLoader::LoadMeshes(const xml::Node& resources) const
		{
			if( const xml::Node* pMeshes = resources.FirstNode(L"Meshes") )
			{
				sys::WorkingDirectory dir;

				if(const xml::Attribute* pPath = pMeshes->Attribute(L"path"))
					dir = sys::WorkingDirectory(pPath->GetValue());

                if(const xml::Node::NodeVector* pMeshList = pMeshes->Nodes(L"Mesh"))
                {
				    for( auto pMesh : *pMeshList )
				    {
					    const std::wstring& stAttribute = pMesh->Attribute(L"type")->GetValue();
					    const std::wstring& stName = pMesh->GetValue();
						if(stAttribute == L"file")
						{
							const std::wstring& stFile = pMesh->Attribute(L"file")->GetValue();
							m_ctx.meshes.Load(stName, stFile);
						}
					    else if(stAttribute == L"screen quad")
						    m_ctx.meshes.ScreenQuad( stName,  m_pScreen->GetSize() );
						else if(stAttribute == L"lines")
							m_ctx.meshes.Line(stName, pMesh->Attribute(L"segments")->AsInt());
					    else if (stAttribute == L"linegrid")
                            m_ctx.meshes.LineGrid( stName, pMesh->Attribute(L"size")->AsFloat(), pMesh->Attribute(L"divisions")->AsInt());
                        else if(stAttribute == L"linecube")
                            m_ctx.meshes.LineCube( stName, pMesh->Attribute(L"size")->AsFloat());
					    else if(stAttribute == L"billboard")
						    m_ctx.meshes.BillBoard( stName, pMesh->Attribute(L"size")->AsFloat() );
					    else if(stAttribute == L"heightmap")
						    m_ctx.meshes.HeightMap( stName, *pMesh->Attribute(L"texture") );
						else if(stAttribute == L"grid")
							m_ctx.meshes.Grid( stName, pMesh->Attribute(L"sizex")->AsInt(), pMesh->Attribute(L"sizey")->AsInt());
				    }
                }
			}
		}

		void AxmLoader::LoadFonts(const xml::Node& resources) const
		{
			if( const xml::Node* pFonts = resources.FirstNode(L"Fonts") )
			{
				sys::WorkingDirectory dir;

				if(const xml::Attribute* pPath = pFonts->Attribute(L"path"))
					dir = sys::WorkingDirectory(pPath->GetValue());

                if( const xml::Node::NodeVector* pFontList = pFonts->Nodes(L"Font"))
                {
				    for( auto pFont : *pFontList )
				    {
                        const std::wstring& stFontFamily = pFont->Attribute(L"name")->GetValue();
                        const size_t fontSize = pFont->Attribute(L"size")->AsInt();

						m_ctx.fonts.Load(stFontFamily, fontSize);
				    }
                }
			}
		}

		void AxmLoader::LoadEffects(const xml::Node& resources) const
		{
			if(const xml::Node* pEffects = resources.FirstNode(L"Effects"))
			{
				sys::WorkingDirectory dir;

				if(const xml::Attribute* pPath = pEffects->Attribute(L"path"))
					dir = sys::WorkingDirectory(pPath->GetValue());

				if(const xml::Node::NodeVector* pEffectList = pEffects->Nodes(L"Effect"))
                {
				    for(auto effect: *pEffectList)
				    {
					    auto* pEffect = m_ctx.effects.Load(effect->Attribute(L"name")->GetValue(), effect->Attribute(L"file")->GetValue());
						if(!pEffect)
							continue;

						// alpha
						if(const auto alphaBlend = effect->FirstNode(L"AlphaBlend"))
						{
							const BlendMode srcBlend = static_cast<BlendMode>(alphaBlend->Attribute(L"srcblend")->AsInt());

							const BlendMode destBlend = static_cast<BlendMode>(alphaBlend->Attribute(L"destblend")->AsInt());

							const BlendFunc blendFunc = static_cast<BlendFunc>(alphaBlend->Attribute(L"func")->AsInt());

							BlendMode alphaSrcBlend = BlendMode::ONE;
							if(auto alphaSrc = alphaBlend->Attribute(L"alphasrcblend"))
								alphaSrcBlend = static_cast<BlendMode>(alphaSrc->AsInt());

							BlendMode alphaDestBlend = BlendMode::ZERO;
							if(auto alphaDest = alphaBlend->Attribute(L"alphadestblend"))
								alphaDestBlend = static_cast<BlendMode>(alphaDest->AsInt());


							pEffect->SetAlphaState(alphaBlend->Attribute(L"enable")->AsBool(), srcBlend, destBlend, blendFunc, alphaSrcBlend, alphaDestBlend);
						}
						// default alpha value
						else
							pEffect->SetAlphaState(0, BlendMode::ONE, BlendMode::ONE, BlendFunc::ADD, BlendMode::ONE, BlendMode::ZERO);

						// depth
						if(const auto depth = effect->FirstNode(L"Depth") )
							pEffect->SetDepthState(depth->Attribute(L"enable")->AsBool(), depth->Attribute(L"write")->AsBool());
						// default depth value
						else
							pEffect->SetDepthState(true, true);

						// rasterizer
						if(const auto pRasterizer = effect->FirstNode(L"Rasterizer") )
						{
							CullMode cull = CullMode::BACK;
							if(auto pCull = pRasterizer->Attribute(L"cull"))
								cull = (CullMode)pCull->AsInt();

							FillMode fill = FillMode::SOLID;
							if(auto pFill = pRasterizer->Attribute(L"fill"))
								fill = (FillMode)pFill->AsInt();

							bool bScissorEnable = false;
							if(auto pScissor = pRasterizer->Attribute(L"scissor"))
								bScissorEnable = pScissor->AsBool();

							pEffect->SetRasterizerState(cull, fill, bScissorEnable);
						}
						else
							pEffect->SetRasterizerState(CullMode::BACK, FillMode::SOLID, false);

						// sampler states
						if(const auto pSamplers = effect->Nodes(L"Sampler"))
						{
							unsigned int count = 0;
							for(auto pSampler : *pSamplers)
							{
								TextureFilter minFilter = static_cast<TextureFilter>(pSampler->Attribute(L"min")->AsInt());
								TextureFilter magFilter = static_cast<TextureFilter>(pSampler->Attribute(L"mag")->AsInt());
								MipFilter mipFilter = static_cast<MipFilter>(pSampler->Attribute(L"mip")->AsInt());
								AdressMode adressU = static_cast<AdressMode>(pSampler->Attribute(L"u")->AsInt());
								AdressMode adressV = static_cast<AdressMode>(pSampler->Attribute(L"v")->AsInt());

								pEffect->SetSamplerState(count, minFilter, magFilter, mipFilter, adressU, adressV);

								count++;
							}
						}

						// vertex sampler states
						if(const auto pSamplers = effect->Nodes(L"VertexSampler"))
						{
							unsigned int count = 0;
							for(auto pSampler : *pSamplers)
							{
								TextureFilter minFilter = static_cast<TextureFilter>(pSampler->Attribute(L"min")->AsInt());
								TextureFilter magFilter = static_cast<TextureFilter>(pSampler->Attribute(L"mag")->AsInt());
								MipFilter mipFilter = static_cast<MipFilter>(pSampler->Attribute(L"mip")->AsInt());
								AdressMode adressU = static_cast<AdressMode>(pSampler->Attribute(L"u")->AsInt());
								AdressMode adressV = static_cast<AdressMode>(pSampler->Attribute(L"v")->AsInt());

								pEffect->SetVertexSamplerState(count, minFilter, magFilter, mipFilter, adressU, adressV);

								count++;
							}
						}
					}
                }

				// extentions

				if(const xml::Node* pExtentions = pEffects->FirstNode(L"Extentions"))
				{
					if(const xml::Node::NodeVector* pExtentionList = pExtentions->Nodes(L"Extention"))
					{
						for(auto extention: *pExtentionList)
						{
							const std::wstring& stName = extention->GetValue();
							const std::wstring& stEffect = extention->Attribute(L"effect")->GetValue();
							const std::wstring& stFile = extention->Attribute(L"file")->GetValue();
							bool bActivate = extention->Attribute(L"activate")->AsBool();

							m_ctx.effects.LoadExtention(stName, stEffect, stFile, bActivate);
						}
					}
				}
			}
		}

    }
}