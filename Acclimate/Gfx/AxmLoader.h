#pragma once
#include "IResourceLoader.h"
#include "Context.h"

namespace acl
{
	namespace xml
	{
		class Node;
	}

	namespace gfx
	{

		class Screen;

        class AxmLoader :
			public IResourceLoader
        {
        public:
            AxmLoader(const Screen& screen, const LoadContext& ctx);

            void Load(const std::wstring& stFilename) const override;

        private:

			void LoadTextures(const xml::Node& resources) const;
			void LoadMeshes(const xml::Node& resources) const;
			void LoadFonts(const xml::Node& resources) const;
			void LoadEffects(const xml::Node& resources) const;

            const Screen* m_pScreen;

            LoadContext m_ctx;
        };

    }
}
