#include "BaseCbuffer.h"
#include <memory>
#include <minmax.h>
#include "CbufferInternalAccessor.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		BaseCbuffer::BaseCbuffer(void) : BaseCbuffer(4096)
		{
		}

		BaseCbuffer::BaseCbuffer(size_t size) : m_pBuffer(nullptr), m_pData(nullptr), m_size(size), m_activeRange(0)
		{
			m_pData = new float[m_size / sizeof(float)];
		}

		BaseCbuffer::BaseCbuffer(AclCbuffer* pBuffer, size_t size) : m_pBuffer(pBuffer), m_pData(nullptr), m_size(size),
			m_activeRange(0)
		{
			m_pData = new float[m_size / sizeof(float)];
		}

		BaseCbuffer::BaseCbuffer(const BaseCbuffer& buffer) : m_pBuffer(nullptr), m_size(buffer.m_size),
			m_activeRange(0)
		{
			m_pData = new float[m_size / sizeof(float)];
			memcpy(m_pData, buffer.m_pData, buffer.m_size);
		}

		void BaseCbuffer::Copy(const ICbuffer& cbuffer)
		{
			const auto size = cbuffer.GetSize();
			ResizeBuffer(size);
			// TODO: also resize internal buffer
		}

		BaseCbuffer::~BaseCbuffer(void)
		{
			delete[] m_pData;
		}

		void BaseCbuffer::GetBuffer(CbufferInternalAccessor& accessor) const
		{
			accessor.SetCbuffer(m_pBuffer);
		}

		size_t BaseCbuffer::GetSize(void) const
		{
			return m_size;
		}

		bool BaseCbuffer::Link(const CbufferInternalAccessor& accessor)
		{
			auto pBuffer = accessor.GetCbuffer();
			if(!pBuffer)
				return false;

			if(!m_pBuffer || OnNewBufferIsValid(pBuffer))
			{
				OnDeleteBuffer(m_pBuffer);

				m_pBuffer = pBuffer;
				ResizeBuffer(OnGetBufferSize(*m_pBuffer));

				if(m_pBuffer)
					OnOverwrite(m_pData, m_activeRange);

				return true;
			}
			else
			{
				OnDeleteBuffer(pBuffer);

				return false;
			}
		}

		void BaseCbuffer::Write(size_t index, const float* constArr, size_t numConsts)
		{
			ACL_ASSERT((index + numConsts)*16 <= m_size);

			m_activeRange = max(m_activeRange, index * 4 * sizeof(float)+numConsts * 4 * sizeof(float));

			memcpy(&m_pData[index*4], constArr, numConsts*4*sizeof(float)); 

			if(m_pBuffer)
				OnOverwrite(m_pData, m_activeRange);
		}

		const float* BaseCbuffer::GetData(void) const
		{
			return m_pData;
		}

		void BaseCbuffer::ResizeBuffer(size_t size)
		{
			if(size > m_size)
			{
				m_size = size;

				auto pTempData = new float[size / sizeof(float)];

				memcpy(m_pData, pTempData, m_activeRange);

				delete[] m_pData;
				m_pData = pTempData;
			}
		}

	}
}