#pragma once
#include "ICbuffer.h"
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class BaseCbuffer : 
			public ICbuffer
		{
		public:
			BaseCbuffer(void);
			BaseCbuffer(size_t size);
			BaseCbuffer(AclCbuffer* pBuffer, size_t size);
			BaseCbuffer(const BaseCbuffer& buffer);
			~BaseCbuffer(void);

			void Copy(const ICbuffer& cbuffer) override final;

			void GetBuffer(CbufferInternalAccessor& accessor) const override final;
			size_t GetSize(void) const override final;

			bool Link(const CbufferInternalAccessor& accessor) override final;

			void Write(size_t index, const float* constArr, size_t numConsts) override final;

		protected:

			AclCbuffer* m_pBuffer;

			const float* GetData(void) const;

		private:

			void ResizeBuffer(size_t size);

			virtual bool OnNewBufferIsValid(const AclCbuffer* pNewBuffer) const = 0;
			virtual void OnOverwrite(float* pData, size_t size) = 0;
			virtual void OnDeleteBuffer(const AclCbuffer* pBuffer) const = 0;
			virtual size_t OnGetBufferSize(const AclCbuffer& buffer) const = 0;

			size_t m_size, m_activeRange;
			float* m_pData;
		};

	}
}