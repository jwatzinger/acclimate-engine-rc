#include "BaseCbufferLoader.h"
#include "CbufferInternalAccessor.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		ICbuffer& BaseCbufferLoader::Create(size_t numConsts) const
		{
			return OnCreateFromSize(numConsts * 16);
		}

		ICbuffer& BaseCbufferLoader::Create(const CbufferInternalAccessor& accessor) const
		{
			auto pBuffer = accessor.GetCbuffer();

			ACL_ASSERT(pBuffer);

			return OnCreateFromBuffer(*pBuffer);
		}

	}
}