#pragma once
#include "..\ApiDef.h"
#include "ICbufferLoader.h"

namespace acl
{
	namespace gfx
	{

		class BaseCbufferLoader :
			public ICbufferLoader
		{
		public:

			ICbuffer& Create(size_t numConsts) const override final;
			ICbuffer& Create(const CbufferInternalAccessor& accessor) const override final;

		private:

			virtual ICbuffer& OnCreateFromSize(size_t numBytes) const = 0;
			virtual ICbuffer& OnCreateFromBuffer(AclCbuffer& cbuffer) const = 0;
		};

	}
}