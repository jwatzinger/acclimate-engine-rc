#include "BaseEffect.h"
#include "Defines.h"
#include "EffectInternalAccessor.h"
#include "EffectFile.h"
#include "IEffectLoader.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseEffect::BaseEffect(EffectFile& file, const IEffectLoader& loader, DeleteEffectFunc del) : m_pFile(&file),
			m_pLoader(&loader), m_numPermutations(file.GetMaxPermutations()), m_vEffects(m_numPermutations), m_vSigsChanged(m_numPermutations),
			m_vActiveEffects(m_numPermutations), m_del(del)
		{
			EffectInternalAccessor accessor;
			m_pLoader->CreatePermutation(file, 0, accessor);
			m_vEffects[0] = accessor.GetEffect();
			m_vActiveEffects[0] = m_vEffects[0];
		}

		BaseEffect::~BaseEffect(void)
		{
			for(auto pEffect : m_vEffects)
			{
				m_del(pEffect);
			}

			for(auto& extentions : m_mExtentions)
			{
				for(auto pEffect : extentions.second)
				{
					m_del(pEffect);
				}
			}

			for(unsigned int i = 0; i < GetNumPermutations(); i++)
			{
				m_vSigsChanged[i](nullptr);
			}
		}

		void BaseEffect::SetSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v)
		{
			if(index >= MAX_PIXEL_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set sampler state to effect on illegal slot", index, "(only slots up to", MAX_PIXEL_TEXTURES, "supported");
				return;
			}

			OnSamplerState(index, minFilter, magFilter, mipFilter, u, v);
		}

		void BaseEffect::SetVertexSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v)
		{
			if(index >= MAX_VERTEX_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set vertex sampler state to effect on illegal slot", index, "(only slots up to", MAX_VERTEX_TEXTURES, "supported");
				return;
			}

			OnVertexSamplerState(index, minFilter, magFilter, mipFilter, u, v);
		}

		void BaseEffect::SelectExtention(const std::wstring& stExtention)
		{
			if(stExtention != m_stExtention)
			{
				m_stExtention = stExtention;

				unsigned int permutation = 0;
				for(auto pEffect : m_vEffects)
				{
					if(pEffect)
					{
						if(stExtention.empty())
						{
							m_vActiveEffects[permutation] = pEffect;
							ChangeEffect(*pEffect, permutation);
						}
						else if(m_mExtentions.count(stExtention))
						{
							auto& effect = GetActivePermutation(permutation);
							ChangeEffect(effect, permutation);
						}
						else
							sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "selected extention", stExtention, "does not exists in effect.");
					}

					permutation++;
				}
			}
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "extention", stExtention, "is already selected in effect.");
		}
		
		void BaseEffect::OnExtentionAdded(const std::wstring& stName)
		{
			m_mExtentions[stName].resize(m_numPermutations);
			if(stName == m_stExtention)
			{
				for(unsigned int i = 0; i < GetNumPermutations(); i++)
				{
					auto& effect = GetActivePermutation(i);
					if(&effect == m_mExtentions[stName][i])
						ChangeEffect(effect, i);
				}
			}
		}

		void BaseEffect::CloneVCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const
		{
			accessor.SetCbuffer(OnCreateVCbuffer(*m_vActiveEffects[permutation], id));
		}

		void BaseEffect::ClonePCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const
		{
			accessor.SetCbuffer(OnCreatePCbuffer(*m_vActiveEffects[permutation], id));
		}

		void BaseEffect::CloneGCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const
		{
#ifndef ACL_API_DX9
			accessor.SetCbuffer(OnCreateGCbuffer(*m_vActiveEffects[permutation], id));
#else
			accessor.SetCbuffer(nullptr);
#endif
		}

		size_t BaseEffect::GetNumPermutations(void) const
		{
			return m_numPermutations;
		}

		EffectFile& BaseEffect::GetFile(void)
		{
			return *m_pFile;
		}

		const EffectFile& BaseEffect::GetFile(void) const
		{
			return *m_pFile;
		}

		void BaseEffect::BindEffect(render::StateGroup& states, unsigned int permutation)
		{
			OnBindShader(states, GetActivePermutation(permutation));
		}

		void BaseEffect::BindEffect(render::StateGroup& states) const
		{
			OnBindShader(states, *m_vEffects[0]);
		}

		const render::StateGroup& BaseEffect::GetState(void) const
		{
			return m_states;
		}

		core::Signal<IEffect*>& BaseEffect::GetChangedSignal(unsigned int permutation)
		{
			return m_vSigsChanged[permutation];
		}

		void BaseEffect::ChangeEffect(AclEffect& effect, unsigned int permutation)
		{
			m_vSigsChanged[permutation](this);
		}

		AclEffect& BaseEffect::GetActivePermutation(unsigned int permutation)
		{
			if(m_stExtention.empty() || !m_mExtentions.count(m_stExtention))
			{
				if(auto pEffect = m_vEffects[permutation])
				{
					m_vActiveEffects[permutation] = pEffect;
					return *pEffect;
				}
				else
				{
					EffectInternalAccessor accessor;
					m_pLoader->CreatePermutation(*m_pFile, permutation, accessor);
					pEffect = accessor.GetEffect();
					m_vActiveEffects[permutation] = pEffect;
					m_vEffects[permutation] = pEffect;
					return *pEffect;
				}
			}
			else
			{
				if(auto pEffect = m_mExtentions[m_stExtention][permutation])
				{
					m_vActiveEffects[permutation] = pEffect;
					return *pEffect;
				}
				else
				{
					EffectInternalAccessor accessor;
					m_pLoader->CreatePermutation(*m_pFile, permutation, accessor);
					pEffect = accessor.GetEffect();
					m_vActiveEffects[permutation] = pEffect;
					m_mExtentions[m_stExtention][permutation] = pEffect;
					return *pEffect;
				}
			}
		}

	}
}