#pragma once
#include "IEffect.h"
#include <unordered_map>
#include "..\ApiDef.h"
#include "..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{
		
		class IEffectLoader;

		class BaseEffect :
			public IEffect
		{
			typedef std::unordered_map<std::wstring, std::vector<AclEffect*>> ExtentionMap;
			typedef std::vector<AclEffect*> EffectVector;
			typedef std::vector<core::Signal<IEffect*>> SignalVector;
			typedef void (*DeleteEffectFunc)(AclEffect* pEffect);
		public:

			BaseEffect(EffectFile& file, const IEffectLoader& loader, DeleteEffectFunc del);
			~BaseEffect(void);

            void SetSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) override final;
			void SetVertexSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) override final;
			void SelectExtention(const std::wstring& stName) override final;
			void OnExtentionAdded(const std::wstring& stName) override final;

			void CloneVCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const override final;
			void ClonePCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const override final;
			void CloneGCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const override final;
			size_t GetNumPermutations(void) const override final;
			EffectFile& GetFile(void) override final;
			const EffectFile& GetFile(void) const override final;

			void BindEffect(render::StateGroup& states, unsigned int permutation) override final;
			void BindEffect(render::StateGroup& states) const override final;
			const render::StateGroup& GetState(void) const override final;
			core::Signal<IEffect*>& GetChangedSignal(unsigned int permutation) override final;

		protected:

			render::StateGroup m_states;

		private:

			AclEffect& GetActivePermutation(unsigned int permutation);
			void ChangeEffect(AclEffect& effect, unsigned int permutation);
			virtual void OnSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) = 0;
			virtual void OnVertexSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) = 0;
			virtual void OnBindShader(render::StateGroup& group, AclEffect& effect) const = 0;
			virtual void OnDeleteEffect(AclEffect& effect) = 0;
			virtual AclCbuffer* OnCreateVCbuffer(AclEffect& effect, unsigned int id) const = 0;
			virtual AclCbuffer* OnCreatePCbuffer(AclEffect& effect, unsigned int id) const = 0;
#ifndef ACL_API_DX9
			virtual AclCbuffer* OnCreateGCbuffer(AclEffect& effect, unsigned int id) const = 0;
#endif

			DeleteEffectFunc m_del;
			size_t m_numPermutations;
			EffectFile* m_pFile;
			const IEffectLoader* m_pLoader;

			EffectVector m_vEffects, m_vActiveEffects;
			ExtentionMap m_mExtentions;

			std::wstring m_stExtention;
			SignalVector m_vSigsChanged;
		};

	}
}