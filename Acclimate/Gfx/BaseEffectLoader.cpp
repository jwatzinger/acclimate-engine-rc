#include "BaseEffectLoader.h"

#include "IEffect.h"
#include "EffectFile.h"
#include "EffectFileParser.h"
#include "EffectInternalAccessor.h"
#include "..\System\Exception.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseEffectLoader::BaseEffectLoader(Effects& effects, const IEffectLanguageParser& parser) : 
			m_pEffects(&effects), m_parser(parser)
		{
		}

		IEffect* BaseEffectLoader::Load(const std::wstring& stName, const std::wstring& stFilename) const
		{
			if(!m_pEffects->Has(stName))
			{
				EffectFile* pFile = nullptr;
				try
				{
					pFile = m_parser.Parse(stFilename);

#ifdef ACL_API_DX9
					if(pFile->GetShaderTypes() == gfx::ShaderTypes::VERTEX_PIXEL_GEOMETRY)
						return nullptr;
#endif

					auto& effect = OnCreateEffect(*pFile);
					m_pEffects->Add(stName, effect);

					return &effect;
				}
				catch(apiException& /*e*/)
				{
					if(pFile)
						sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, pFile->GetData());
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to load effect", stName, "from file", stFilename);
					throw fileException();
				}
				catch(fileException& /*e*/)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to open effect file", stFilename);
					throw fileException();
				}
			}
			else
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to reload already existing effect", stName, "(reloading currently unsupported)");

				return m_pEffects->Get(stName);
			}
		}

		void BaseEffectLoader::LoadExtention(const std::wstring& stName, const std::wstring& stEffectName, const std::wstring& stFilename, bool bActivate) const
		{
			if(auto pEffect = m_pEffects->Get(stEffectName))
			{
				try
				{
					EffectFile& file = pEffect->GetFile();
					m_parser.ParseExtention(stFilename, file);

					pEffect->OnExtentionAdded(stName);
				}
				catch(apiException& /*e*/)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to apply effect extention", stFilename, "to effect", stEffectName);
					throw fileException();
				}
				catch(fileException& /*e*/)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to open effect extention from file", stFilename);
					throw fileException();
				}
			}
		}

		void BaseEffectLoader::CreatePermutation(const EffectFile& file, unsigned int permutation, EffectInternalAccessor& accessor) const
		{
			auto& effect = OnCreatePermutation(file, permutation);
			accessor.SetEffect(&effect);
		}

	}
}