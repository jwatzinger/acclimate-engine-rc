#pragma once
#include "IEffectLoader.h"
#include "Effects.h"
#include "EffectFileParser.h"
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class BaseEffectLoader :
			public IEffectLoader
		{
		public:
			BaseEffectLoader(Effects& effects, const IEffectLanguageParser& parser);

			IEffect* Load(const std::wstring& stName, const std::wstring& stFilename) const override final;
			void LoadExtention(const std::wstring& stName, const std::wstring& stEffectName, const std::wstring& stFilename, bool bActivate) const override final;
			void CreatePermutation(const EffectFile& file, unsigned int permutation, EffectInternalAccessor& accessor) const override final;
			
		protected:

			virtual IEffect& OnCreateEffect(EffectFile& file) const = 0;
			virtual AclEffect& OnCreatePermutation(const EffectFile& file, unsigned int permutation) const = 0;

		private:

			Effects* m_pEffects;
			EffectFileParser m_parser;
		};

	}
}