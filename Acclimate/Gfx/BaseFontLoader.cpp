#include "BaseFontLoader.h"
#include "..\System\Convert.h"
#include "..\System\Log.h"
#include "..\System\Exception.h"

namespace acl
{
    namespace gfx
    {

		BaseFontLoader::BaseFontLoader(Fonts& fonts, ITextureLoader& textureLoader) : m_pFonts(&fonts),
			m_loader(textureLoader)
        {
        }


#pragma warning ( disable: 4715)
		IFont& BaseFontLoader::Load(const std::wstring& stFontFamily, size_t size) const
        {
			const std::wstring stFontName = stFontFamily + conv::ToString(size);
			
			if(!m_pFonts->Has(stFontName))
			{
				// load font file
				FontFile file;
				ITexture* pTexture = nullptr;
				try
				{
					const std::wstring& stFilename = stFontName + L".png";

					file = m_loader.Load(stFontName + L".acf", stFilename, &pTexture);

					if(!pTexture)
					{
						sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "Failed to load font texture", stFilename);
						throw fileException();
					}
				}
				catch(...)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::INFO, "Font", stFontFamily, "with size", size, "could not be loaded from file. To speed up load/font switching times, you might want to export & load the font at startup instead.");

					file = m_loader.Create(stFontFamily, size, &pTexture);

					if(!pTexture)
					{
						sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "Failed to create font texture for font", stFontFamily, "with size", size);
						throw fileException();
					}
				}

				IFont& font = OnCreateFont(file, *pTexture);
				m_pFonts->Add(stFontName, font);

				return font;
			}
			else
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Tried to reload already existing font", stFontName, " (reloading is currently unsupported)");
			}
        }
#pragma warning ( default: 4715)

    }
}
