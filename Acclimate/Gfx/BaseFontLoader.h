#pragma once
#include "IFontLoader.h"
#include "Fonts.h"
#include "FontFileLoader.h"

namespace acl
{
	namespace gfx
	{
		class ITextureLoader;
		class ITexture;
		class FontFile;

        class BaseFontLoader :
			public IFontLoader
        {
        public:
			BaseFontLoader(Fonts& fonts, ITextureLoader& textureLoader);

            IFont& Load(const std::wstring& stFontFamily, size_t size) const override final;

		protected:

			virtual IFont& OnCreateFont(FontFile& file, ITexture& texture) const = 0;

        private:

            Fonts* m_pFonts;
			FontFileLoader m_loader;
        };

    }
}