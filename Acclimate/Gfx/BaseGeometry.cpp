#include "BaseGeometry.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		BaseGeometry::BaseGeometry(PrimitiveType type, const AttributeVector& vAttributes) : m_primitiveType(type), m_vAttributes(vAttributes),
			m_vertexSize(0)
		{
			for(auto& attribute : m_vAttributes)
			{
				m_vertexSize += CalculateAttributeSize(attribute);
			}
		}

		unsigned int BaseGeometry::CalculateAttributeSize(const VertexAttribute& attribute) const
		{
			switch(attribute.type)
			{
			case AttributeType::FLOAT:
				return attribute.numElements * sizeof(float);
			case AttributeType::UBYTE4:
				ACL_ASSERT(attribute.numElements == 4);
				return sizeof(float);
			case AttributeType::SHORT:
				return attribute.numElements * sizeof(short);
			default:
				ACL_ASSERT(false);
			}

			return 0;
		}

		unsigned int BaseGeometry::CalculateVertexSize(unsigned int buffer) const
		{
			unsigned int size = 0;
			for(auto& attribute : m_vAttributes)
			{
				if(attribute.buffer == buffer)
					size += CalculateAttributeSize(attribute);
			}
			return size;
		}

		unsigned int BaseGeometry::GetVertexSize(void) const
		{
			return m_vertexSize;
		}

		PrimitiveType BaseGeometry::GetPrimitiveType(void) const
		{
			return m_primitiveType;
		}

		const BaseGeometry::AttributeVector& BaseGeometry::GetAttributes(void) const
		{
			return m_vAttributes;
		}

		unsigned int BaseGeometry::GetNumVerticesPerFace(void) const
		{
			switch(m_primitiveType)
			{
			case PrimitiveType::TRIANGLE:
				return 3;
			case PrimitiveType::LINE:
				return 2;
			case PrimitiveType::TRIANGLE_STRIP:
				return 1;
			case PrimitiveType::POINT:
				return 1;
			default:
				ACL_ASSERT(false);
				return 0;
			}
		}

		unsigned int BaseGeometry::GetNumElements(unsigned int buffer) const
		{
			unsigned int numAttributes = 0;
			for(auto& attribute : m_vAttributes)
			{
				if(attribute.buffer == buffer)
					numAttributes++;
			}
			return numAttributes;
		}

	}
}