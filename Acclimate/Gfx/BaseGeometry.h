#pragma once
#include "IGeometry.h"

namespace acl
{
	namespace gfx
	{

		class BaseGeometry :
			public IGeometry
		{
		public:
			BaseGeometry(PrimitiveType type, const AttributeVector& vAttributes);

			unsigned int CalculateAttributeSize(const VertexAttribute& attribute) const override final;
			unsigned int CalculateVertexSize(unsigned int buffer) const override final;

			unsigned int GetVertexSize(void) const override final;
			PrimitiveType GetPrimitiveType(void) const override final;
			const AttributeVector& GetAttributes(void) const override final;
			unsigned int GetNumVerticesPerFace(void) const override final;
			unsigned int GetNumElements(unsigned int buffer) const override final;

		private:

			unsigned int m_vertexSize;
			PrimitiveType m_primitiveType;
			AttributeVector m_vAttributes;
		};

	}
}