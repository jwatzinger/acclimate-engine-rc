#include "BaseGeometryCreator.h"

namespace acl
{
	namespace gfx
	{

		unsigned long long getAttributeKey(const VertexAttribute& attrib)
		{
			return (unsigned long long)attrib.semantic + (attrib.slot << 1) + ((unsigned long long)attrib.type << 5) + (attrib.numElements << 7) + (attrib.buffer << 10) + (attrib.instanceDivisor << 11);
		}

		sys::Int128 getKey(PrimitiveType type, const IGeometry::AttributeVector& vAttributes)
		{
			sys::Int128 key;
			key.Add(0, 3, (long long)type);

			unsigned int numAttribute = 0;
			for(auto& attribute : vAttributes)
			{
				const unsigned long long attributeKey = getAttributeKey(attribute);

				key.Add(3 + numAttribute * 12, 12, attributeKey);
				numAttribute++;
			}

			return key;
		}

		BaseGeometryCreator::~BaseGeometryCreator(void)
		{
			for(auto& geometry : m_mGeometries)
			{
				delete geometry.second;
			}
		}

		IGeometry& BaseGeometryCreator::CreateGeometry(PrimitiveType type, const IGeometry::AttributeVector& vAttributes)
		{
			const auto key = getKey(type, vAttributes);

			auto itr = m_mGeometries.find(key);

			if(itr != m_mGeometries.end())
			{
				return *itr->second;
			}
			else
			{
				auto& geometry = OnCreateGeometry(type, vAttributes);

				m_mGeometries.emplace(key, &geometry);

				return geometry;
			}
		}

	}
}