#pragma once
#include "IGeometryCreator.h"
#include <map>
#include "..\System\Int128.h"
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class BaseGeometryCreator :
			public IGeometryCreator
		{
			typedef std::map<sys::Int128, IGeometry*> GeometryMap;
		public:

			~BaseGeometryCreator(void);

			IGeometry& CreateGeometry(PrimitiveType type, const IGeometry::AttributeVector& vAttributes) override final;

		private:

			virtual IGeometry& OnCreateGeometry(PrimitiveType type, const IGeometry::AttributeVector& vAttributes) const = 0;

			GeometryMap m_mGeometries;
		};

	}
}