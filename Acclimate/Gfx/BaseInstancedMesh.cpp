#include "BaseInstancedMesh.h"
#include "IGeometry.h"
#include "MeshBuffer.h"
#include "..\System\Log.h"

namespace acl
{
    namespace gfx
	{

		BaseInstancedMesh::DummyMesh::DummyMesh(const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry) :
			BaseMesh(vSubsets, numStates, geometry)
		{
		}

		void BaseInstancedMesh::DummyMesh::SetStateGroup(unsigned int slot, const render::StateGroup& group)
		{
			BaseMesh::SetStateGroup(slot, group);
		}

		void BaseInstancedMesh::DummyMesh::RemoveStateGroup(unsigned int slot, bool bDelete)
		{
			BaseMesh::RemoveStateGroup(slot, bDelete);
		}

		void BaseInstancedMesh::DummyMesh::SetDrawCall(unsigned int slot, const render::BaseDrawCall& call)
		{
			BaseMesh::SetDrawCall(slot, call);
		}

		void BaseInstancedMesh::DummyMesh::RemoveDrawCall(unsigned int slot, bool bDelete)
		{
			BaseMesh::RemoveDrawCall(slot, bDelete);
		}

		void BaseInstancedMesh::DummyMesh::OnChangeVertexCount(unsigned int subset)
		{
		}

		BaseInstancedMesh::BaseInstancedMesh(IVertexBuffer& instanceBuffer, size_t numInstances, IMesh& mesh, const IGeometry& geometry) :
			m_mesh(mesh.GetSubsets(), 1 + mesh.GetStateCount(), geometry), m_pMesh(&mesh), m_numInstances(numInstances), 
			m_pInstanceBuffer(&instanceBuffer), m_pActiveBuffer(&instanceBuffer), m_offset(0)
        {
			m_mesh.SetStateGroup(0, m_states);
			geometry.Bind(m_states);
			instanceBuffer.Bind(m_states, BindTarget::INSTANCE, m_offset);

			const size_t numStates = m_mesh.GetStateCount();
			auto pMeshStates = mesh.GetStates();

			for(unsigned int i = 1; i < numStates; i++)
			{
				m_mesh.SetStateGroup(i, *pMeshStates[i-1]);
			}
        }

		BaseInstancedMesh::BaseInstancedMesh(const BaseInstancedMesh& mesh) : 
			BaseInstancedMesh(mesh.m_pInstanceBuffer->Clone(), mesh.m_numInstances, *mesh.m_pMesh, mesh.m_mesh.GetGeometry().Clone())
		{
		}

		BaseInstancedMesh::~BaseInstancedMesh(void)
		{
			delete m_pInstanceBuffer;

			for(unsigned int i = 0; i < m_mesh.GetStateCount(); i++)
			{
				m_mesh.RemoveStateGroup(i, false);
			}

			SigChanged(nullptr);
		}

		void BaseInstancedMesh::SetInstanceCount(unsigned int numInstances)
        {
			if(m_numInstances != numInstances)
			{
				m_numInstances = numInstances;

				OnChangeInstanceCount(m_numInstances);

				SigChanged(this);
			}
        }

		void BaseInstancedMesh::SetInstanceBuffer(IVertexBuffer& buffer, unsigned int offset)
		{
			if(&buffer != m_pActiveBuffer || m_offset != offset)
			{
				if(buffer.GetStride() != m_pInstanceBuffer->GetStride())
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "New instance buffer has invalid stride compared to instanced meshes original buffer.");
					return;
				}

				m_offset = offset;

				buffer.Bind(m_states, BindTarget::INSTANCE, m_offset);
				m_pActiveBuffer = &buffer;

				OnChangeInstanceBuffer();
				SigChanged(this);
			}
		}

		void BaseInstancedMesh::SetLoadInfo(const MeshLoadInfo* pInfo)
		{
			delete pInfo;
		}

		void BaseInstancedMesh::SetVertexCount(unsigned int numVertices, unsigned int subset)
		{
			m_pMesh->SetVertexCount(numVertices, subset);
		}

		void BaseInstancedMesh::SetIndexBuffer(IIndexBuffer& buffer)
		{
			m_pMesh->SetIndexBuffer(buffer);
		}

		unsigned int BaseInstancedMesh::GetNumSubsets(void) const
		{
			return m_mesh.GetNumSubsets();
		}

		const render::StateGroup** BaseInstancedMesh::GetStates(void) const
		{
			return m_mesh.GetStates();
		}

		unsigned int BaseInstancedMesh::GetStateCount(void) const
		{
			return m_mesh.GetStateCount();
		}

		const IGeometry& BaseInstancedMesh::GetGeometry(void) const
		{
			return m_mesh.GetGeometry();
		}

		const render::BaseDrawCall& BaseInstancedMesh::GetDraw(unsigned int subset) const
		{
			return m_mesh.GetDraw(subset);
		}

		const MeshSkeleton* BaseInstancedMesh::GetSkeleton(void) const
		{
			return m_mesh.GetSkeleton();
		}

		unsigned int BaseInstancedMesh::GetInstanceCount(void) const
		{
			return m_numInstances;
		}

		IMesh& BaseInstancedMesh::GetMeshToInstance(void) const
		{
			return *m_pMesh;
		}

		IVertexBuffer& BaseInstancedMesh::GetInstanceBuffer(void) const
		{
			return *m_pInstanceBuffer;
		}

		core::Signal<const IMesh*>& BaseInstancedMesh::GetChangedSignal(void)
		{
			return SigChanged;
		}

		const MeshLoadInfo* BaseInstancedMesh::GetLoadInfo(void) const
		{
			return nullptr;
		}

		const IMesh::SubsetVector& BaseInstancedMesh::GetSubsets(void) const
		{
			return m_pMesh->GetSubsets();
		}

		IVertexBuffer& BaseInstancedMesh::GetVertexBuffer(void)
		{
			return m_pMesh->GetVertexBuffer();
		}

		IIndexBuffer& BaseInstancedMesh::GetIndexBuffer(void)
		{
			return m_pMesh->GetIndexBuffer();
		}

		void BaseInstancedMesh::SetDrawCall(unsigned int slot, const render::BaseDrawCall& call)
		{
			m_mesh.SetDrawCall(slot, call);
		}

		void BaseInstancedMesh::RemoveDrawCall(unsigned int slot)
		{
			m_mesh.RemoveDrawCall(slot, false);
		}

		render::StateGroup& BaseInstancedMesh::GetOwnStateGroup(void)
		{
			return m_states;
		}

		unsigned int BaseInstancedMesh::GetVertexCount(unsigned int subset) const
		{
			return m_pMesh->GetVertexCount(subset);
		}

    }
}