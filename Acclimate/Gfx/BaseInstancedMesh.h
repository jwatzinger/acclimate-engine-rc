#pragma once
#include "BaseMesh.h"
#include "IInstancedMesh.h"
#include "..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{

		class BaseInstancedMesh : 
			public IInstancedMesh
		{
		public:
			BaseInstancedMesh(IVertexBuffer& instanceBuffer, size_t numInstances, IMesh& mesh, const IGeometry& geometry);
			BaseInstancedMesh(const BaseInstancedMesh& mesh);
			~BaseInstancedMesh(void);

			void SetInstanceCount(unsigned int numInstances) override final;
			void SetInstanceBuffer(IVertexBuffer& buffer, unsigned int offset) override final;
			void SetLoadInfo(const MeshLoadInfo* pInfo) override final;
			void SetVertexCount(unsigned int numVertices, unsigned int subset) override final;
			void SetIndexBuffer(IIndexBuffer& buffer) override final;

			unsigned int GetNumSubsets(void) const override final;
			unsigned int GetStateCount(void) const override final;
			const IGeometry& GetGeometry(void) const override final;
			const render::StateGroup** GetStates(void) const override final;
			const render::BaseDrawCall& GetDraw(unsigned int subset) const override final;
			const MeshSkeleton* GetSkeleton(void) const override final;
			unsigned int GetInstanceCount(void) const override final;
			IMesh& GetMeshToInstance(void) const override final;
			IVertexBuffer& GetInstanceBuffer(void) const override final;
			core::Signal<const IMesh*>& GetChangedSignal(void) override final;
			const MeshLoadInfo* GetLoadInfo(void) const override final;
			const SubsetVector& GetSubsets(void) const override final;

			IVertexBuffer& GetVertexBuffer(void) override final;
			IIndexBuffer& GetIndexBuffer(void) override final;

		protected:

			void SetDrawCall(unsigned int slot, const render::BaseDrawCall& call);
			void RemoveDrawCall(unsigned int slot);
			render::StateGroup& GetOwnStateGroup(void);
			unsigned int GetVertexCount(unsigned int subset) const;

		private:

			virtual void OnChangeInstanceCount(unsigned int numInstances) = 0;
			virtual void OnChangeInstanceBuffer(void) = 0;

			class DummyMesh :
				public BaseMesh
			{
			public:
				DummyMesh(const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry);

				void SetStateGroup(unsigned int slot, const render::StateGroup& group);
				void RemoveStateGroup(unsigned int slot, bool bDelete);
				void SetDrawCall(unsigned int slot, const render::BaseDrawCall& call);
				void RemoveDrawCall(unsigned int slot, bool bDelete);

				void OnChangeVertexCount(unsigned int subset) override;
			};

			DummyMesh m_mesh;

            size_t m_numInstances, m_offset;

			IMesh* m_pMesh;
			IVertexBuffer* m_pInstanceBuffer, *m_pActiveBuffer;
			render::StateGroup m_states;

			core::Signal<const IMesh*> SigChanged;
		};
	}
}