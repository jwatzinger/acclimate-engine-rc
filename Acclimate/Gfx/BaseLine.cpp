#include "BaseLine.h"
#include <algorithm>
#include "IEffect.h"
#include "IGeometry.h"
#include "IGeometryCreator.h"
#include "..\Render\Instance.h"
#include "..\Render\IStage.h"

namespace acl
{
    namespace gfx
    {

		BaseLine::BaseLine(IGeometryCreator& creator) : m_pEffect(nullptr), m_color(255, 255, 255, 255)
		{
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 4);

			m_pGeometry = &creator.CreateGeometry(PrimitiveType::LINE, vAttributes);
			m_pGeometry->Bind(m_states);
        }

		BaseLine::~BaseLine(void)
		{
#ifdef ACL_API_GL4
			delete m_pGeometry;
#endif
		}

		void BaseLine::SetLine(const math::Vector3& vFrom, const math::Vector3& vTo)
        {
            m_vFrom = vFrom;
            m_vTo = vTo;
        }

		void BaseLine::SetColor(const Color& color)
        {
            m_color = color;
        }

		void BaseLine::SetEffect(const IEffect& effect)
		{
			if(m_pEffect != &effect)
			{
				m_pEffect = &effect;
				m_pEffect->BindEffect(m_states);
			}
		}

		void BaseLine::Draw(const render::IStage& stage)
        {
			if(!m_pEffect)
				return;

			const render::StateGroup* groups[2] = { &m_states, &m_pEffect->GetState() };
			// create render instance and set states
	        render::Instance instance(m_sortKey, groups, 2);

			if(OnDrawLine(instance, m_vFrom, m_vTo, m_color))
				stage.SubmitTempInstance(std::move(instance));
        }

		void BaseLine::DrawMulti(const PointVector& vPoints, const render::IStage& stage)
		{
			if(!m_pEffect)
				return;

			const render::StateGroup* groups[2] = { &m_states, &m_pEffect->GetState() };
			// create render instance and set states
			render::Instance instance(m_sortKey, groups, 2);

			if(OnDrawLines(instance, vPoints, m_color))
				stage.SubmitTempInstance(std::move(instance));
		}

    }
}