#pragma once
#include <memory>
#include "ILine.h"
#include "Color.h"
#include "..\Math\Vector3.h"
#include "..\Render\Key.h"
#include "..\Render\StateGroup.h"

namespace acl
{
	namespace render
	{
		struct Instance;
	}

    namespace gfx
    {

		class IGeometry;
		class IGeometryCreator;

        class BaseLine :
			public ILine
        {
        public:
			BaseLine(IGeometryCreator& creator);
			~BaseLine(void);

			void SetLine(const math::Vector3& vFrom, const math::Vector3& vTo) override final;
			void SetColor(const Color& color) override final;
			void SetEffect(const IEffect& effect) override final;

			void Draw(const render::IStage& stage) override final;
			void DrawMulti(const PointVector& vPoints, const render::IStage& stage) override final;

		protected:

			render::StateGroup m_states;

        private:

			virtual bool OnDrawLine(render::Instance& instance, const math::Vector3& vFrom, const math::Vector3& vTo, const Color& color) = 0;
			virtual bool OnDrawLines(render::Instance& instance, const PointVector& vPoints, const Color& color) = 0;

            Color m_color;
			IGeometry* m_pGeometry;
            const IEffect* m_pEffect;

            math::Vector3 m_vFrom;
            math::Vector3 m_vTo;

			render::Key64 m_sortKey;
        };

    }
}