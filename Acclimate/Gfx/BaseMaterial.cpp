#include "BaseMaterial.h"
#include "Defines.h"
#include "RenderStateHandler.h"
#include "ITexture.h"
#include "IEffect.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

#pragma warning( disable: 4351)
		BaseMaterial::BaseMaterial(IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets) : 
			m_pEffect(&effect), m_id(id), m_vStates(numSubsets), m_pInfo(nullptr), m_permutation(permutation),
			m_textures()
		{
			effect.GetChangedSignal(permutation).Connect(this, &BaseMaterial::EffectChanged);
			effect.BindEffect(m_states, permutation);

			// TODO: find a better solution (this is only necessary due to automatic unbinding of DX11 and state guards)
			for(unsigned int i = 0; i < MAX_PIXEL_TEXTURES; i++)
			{
				RenderStateHandler::UnbindTexture(m_states, TextureBindTarget::PIXEL, i);
			}
		}
#pragma warning( default: 4351)

		BaseMaterial::~BaseMaterial(void)
		{
			delete m_pInfo;

			for(auto pState : m_vStates)
			{
				delete pState;
			}

			if(m_pEffect)
				m_pEffect->GetChangedSignal(m_permutation).Disconnect(this, &BaseMaterial::EffectChanged);

			SigChanged(*this, true);
		}

		void BaseMaterial::SetTexture(unsigned int slotIndex, const ITexture* pTexture)
        {
			if(slotIndex >= MAX_PIXEL_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set texture to nonexistent slot", slotIndex, "of material", m_id, "(only slots up to", MAX_PIXEL_TEXTURES, "are supported)");
				return;
			}

			if(m_textures[slotIndex] != pTexture)
			{
				if(pTexture)
					pTexture->Bind(m_states, ITexture::BindTarget::PIXEL, slotIndex);
				else
					RenderStateHandler::UnbindTexture(m_states, TextureBindTarget::PIXEL, slotIndex);

				SigChanged(*this, false);

				m_textures[slotIndex] = pTexture;
			}
        }

		void BaseMaterial::SetTexture(unsigned int subset, unsigned int slotIndex, const ITexture* pTexture)
        {
			if(slotIndex >= MAX_PIXEL_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set texture to nonexistent slot", slotIndex, "of material", m_id, "(only slots up to", MAX_PIXEL_TEXTURES, "are supported)");
				return;
			}

			// TODO: implement unnecessary state change guard for subsets
			//if(m_textures[slotIndex] != pTexture)
			//{
				ACL_ASSERT(subset < m_vStates.size());

				auto pStates = m_vStates[subset];
				if(!pStates)
				{
					pStates = new render::StateGroup;
					m_vStates[subset] = pStates;
					if(m_pEffect)
						m_pEffect->BindEffect(*pStates, m_permutation);
				}

				if(pTexture)
					pTexture->Bind(*pStates, ITexture::BindTarget::PIXEL, slotIndex);
				else
					RenderStateHandler::UnbindTexture(*pStates, TextureBindTarget::PIXEL, slotIndex);

				SigChanged(*this, false);

			//	m_textures[slotIndex] = pTexture;
			//}
        }

		void BaseMaterial::SetVertexTexture(unsigned int slotIndex, const ITexture* pTexture)
        {
			if(slotIndex >= MAX_PIXEL_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set vertex texture to nonexistent slot", slotIndex, "of material", m_id, "(only slots up to", MAX_VERTEX_TEXTURES, "are supported)");
				return;
			}

			if(pTexture)
				pTexture->Bind(m_states, ITexture::BindTarget::VERTEX, slotIndex);
			else
				RenderStateHandler::UnbindTexture(m_states, TextureBindTarget::VERTEX, slotIndex);

			SigChanged(*this, false);
        }

		void BaseMaterial::SetVertexTexture(unsigned int subset, unsigned int slotIndex, const ITexture* pTexture)
        {
			if(slotIndex >= MAX_PIXEL_TEXTURES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set vertex texture to nonexistent slot", slotIndex, "of material", m_id, "(only slots up to", MAX_VERTEX_TEXTURES, "are supported)");
				return;
			}

			ACL_ASSERT(subset < m_vStates.size());

			auto pStates = m_vStates[subset];
			if(!pStates)
			{
				pStates = new render::StateGroup;
				m_vStates[subset] = pStates;
				if(m_pEffect)
					m_pEffect->BindEffect(*pStates, m_permutation);
			}

			if(pTexture)
				pTexture->Bind(*pStates, ITexture::BindTarget::VERTEX, slotIndex);
			else
				RenderStateHandler::UnbindTexture(m_states, TextureBindTarget::VERTEX, slotIndex);

			SigChanged(*this, false);
        }

		void BaseMaterial::SetEffect(IEffect& effect, unsigned int permutation)
        {
			if(m_pEffect != &effect || m_permutation != permutation)
			{
				m_permutation = permutation;

				m_pEffect->GetChangedSignal(m_permutation).Disconnect(this, &BaseMaterial::EffectChanged);
				m_pEffect = &effect;
				m_pEffect->BindEffect(m_states, m_permutation);
				for(auto pState : m_vStates)
				{
					if(pState)
						m_pEffect->BindEffect(*pState, m_permutation);
				}

				effect.GetChangedSignal(m_permutation).Connect(this, &BaseMaterial::EffectChanged);
				SigChanged(*this, false);
			}
        }

		void BaseMaterial::SetLoadInfo(const MaterialLoadInfo* pInfo)
		{
			if(pInfo != m_pInfo)
			{
				delete m_pInfo;
				m_pInfo = pInfo;
			}
		}

		size_t BaseMaterial::GetId(void) const
		{
			return m_id;
		}

		size_t BaseMaterial::GetEffectPermutation(void) const
		{
			return m_permutation;
		}

		const IEffect& BaseMaterial::GetEffect(void) const
        {
            return *m_pEffect;
        }

		const render::StateGroup& BaseMaterial::GetState(unsigned int subset) const
        {
			if(subset >= m_vStates.size() || !m_vStates[subset])
				return m_states;
			else
				return *m_vStates[subset];
        }

		core::Signal<const IMaterial&, bool>& BaseMaterial::GetChangedSignal(void)
		{
			return SigChanged;
		}

		const MaterialLoadInfo* BaseMaterial::GetLoadInfo(void) const
		{
			return m_pInfo;
		}

		void BaseMaterial::EffectChanged(IEffect* pEffect)
		{
			if(!pEffect)
				m_pEffect = nullptr;
			else
				m_pEffect->BindEffect(m_states, m_permutation);

			SigChanged(*this, false);
		}

	}
}