#pragma once
#include <vector>
#include "IMaterial.h"
#include "Defines.h"
#include "..\ApiDef.h"
#include "..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{

		class BaseMaterial :
			public IMaterial
		{
		public:
			BaseMaterial(IEffect& effect, unsigned int permutation, size_t id, size_t numSubsets);
			~BaseMaterial(void);

			void SetTexture(unsigned int slotIndex, const ITexture* texture) override final;
			void SetTexture(unsigned int subset, unsigned int slotIndex, const ITexture* texture) override final;
			void SetVertexTexture(unsigned int slotIndex, const ITexture* texture) override final;
			void SetVertexTexture(unsigned int subset, unsigned int slotIndex, const ITexture* texture) override final;
			void SetEffect(IEffect& effect, unsigned int permutation) override final;
			void SetLoadInfo(const MaterialLoadInfo* pInfo) override final;

            size_t GetId(void) const override final;
			size_t GetEffectPermutation(void) const override final;
			const IEffect& GetEffect(void) const override final;
			const render::StateGroup& GetState(unsigned int subset) const override final;
			core::Signal<const IMaterial&, bool>& GetChangedSignal(void) override final;
			const MaterialLoadInfo* GetLoadInfo(void) const override final;

		private:

			void EffectChanged(IEffect* pEffect);

            const size_t m_id;
			unsigned int m_permutation;

            IEffect* m_pEffect;
			const ITexture* m_textures[MAX_PIXEL_TEXTURES];
			const MaterialLoadInfo* m_pInfo;

			render::StateGroup m_states;
			std::vector<render::StateGroup*> m_vStates;

			core::Signal<const IMaterial&, bool> SigChanged;
		};

	}
}