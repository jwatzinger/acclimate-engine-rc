#include "BaseMaterialLoader.h"
#include "IMaterial.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseMaterialLoader::BaseMaterialLoader(const Effects& effects, const Textures& textures, Materials& materials) : m_pEffects(&effects), m_pTextures(&textures), m_pMaterials(&materials)
        {
        }

		IMaterial* BaseMaterialLoader::Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const std::wstring& stTexture, const SubsetVector* pSubsets) const
        {
			TextureVector vTextures;
			vTextures.push_back(stTexture);
			return Load(stName, stEffectName, effectPermutation, vTextures, pSubsets);
        }

		IMaterial* BaseMaterialLoader::Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const TextureVector& vTextures, const SubsetVector* pSubsets) const
        {
			if(!m_pMaterials->Has(stName))
			{
				if(auto* pEffect = m_pEffects->Get(stEffectName))
				{
					size_t numSubsets = 0;
					if(pSubsets)
						numSubsets = pSubsets->size();

					MaterialLoadInfo* pInfo = new MaterialLoadInfo(stName, stEffectName, effectPermutation, vTextures);

					IMaterial& material = OnCreateMaterial(*pEffect, effectPermutation, numSubsets, m_pMaterials->Size());
					material.SetLoadInfo(pInfo);

					unsigned int textureCount = 0;
					for(auto& stTexture : vTextures)
					{
						if(!stTexture.empty())
						{
							if(const ITexture* pTexture = m_pTextures->Get(stTexture))
								material.SetTexture(textureCount, pTexture);
							else
								sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Tried to access missing texture:", stTexture, "for material", stName);
						}
						else
							material.SetTexture(textureCount, nullptr);

						textureCount++;
					}

					if(pSubsets)
					{
						unsigned int subsetCount = 0;
						for(const auto& subset : *pSubsets)
						{
							// textures
							unsigned int textureCount = 0;
							for(const auto& stTexture : subset.vTextures)
							{
								if(!stTexture.empty())
								{
									if(const ITexture* pTexture = m_pTextures->Get(stTexture))
										material.SetTexture(subsetCount, textureCount, pTexture);
									else
										sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Tried to access missing texture:", stTexture, "for subset", subsetCount, "of material", stName);
								}
								else 
									material.SetTexture(subsetCount, textureCount, nullptr);

								textureCount++;
							}

							// vertex textures
							unsigned int vertexTextureCount = 0;
							for(const auto& stTexture : subset.vVertexTextures)
							{
								if(!stTexture.empty())
								{
									if(const ITexture* pTexture = m_pTextures->Get(stTexture))
										material.SetVertexTexture(subsetCount, textureCount, pTexture);
									else
										sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Tried to access missing texture:", stTexture, "for subset", subsetCount, "of material", stName);
								}
								else 
									material.SetVertexTexture(subsetCount, textureCount, nullptr);

								textureCount++;
							}

							subsetCount++;
						}
					}

					m_pMaterials->Add(stName, material);

					return &material;
				}
				else
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "Effect", stEffectName, "not found for material", stName, ". Material was not created.");
					
					return nullptr;
				}
			}
			else
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to reload already existing material", stName, "(recreation currently unsupported)");

				return m_pMaterials->Get(stName);
			}
        }

	}
}