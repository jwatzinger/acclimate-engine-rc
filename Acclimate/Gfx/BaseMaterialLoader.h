#pragma once
#include "IMaterialLoader.h"

#include "Effects.h"
#include "Textures.h"
#include "Materials.h"

namespace acl
{
	namespace gfx
	{

		class BaseMaterialLoader :
			public IMaterialLoader
		{
		public:

			BaseMaterialLoader(const Effects& effects, const Textures& textures, Materials& materials);

			IMaterial* Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const std::wstring& stTexture, const SubsetVector* pSubsets = nullptr) const override final;
            IMaterial* Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const TextureVector& vTextures, const SubsetVector* pSubsets = nullptr) const override final;

		protected:

			virtual IMaterial& OnCreateMaterial(IEffect& effect, unsigned int permutation, size_t numSubsets, size_t id) const = 0;

        private:

            Materials* m_pMaterials;
            const Effects* m_pEffects;
            const Textures* m_pTextures;
		};

	}
}