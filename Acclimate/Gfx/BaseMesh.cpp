#include "BaseMesh.h"
#include "IGeometry.h"
#include "MeshSkeleton.h"
#include "MeshBuffer.h"
#include "..\Render\States.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace gfx
    {

		// TODO: this constructor is basically just needed for a hack-around in BaseInstancedMesh, figure out if there is
		// a way to solve this
		BaseMesh::BaseMesh(const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry) : m_pVertexBuffer(nullptr),
			m_pIndexBuffer(nullptr), m_vSubsets(vSubsets), m_pGeometry(&geometry), m_numStates(numStates), m_pSkeleton(nullptr), m_pInfo(nullptr), m_isSharedIndexBuffer(false)
		{
			m_pStates = new const render::StateGroup*[numStates];
			m_pDrawCalls = new const render::BaseDrawCall*[m_vSubsets.size()];
		}

		BaseMesh::BaseMesh(IVertexBuffer& vertexBuffer, IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry, MeshSkeleton* pSkeleton):
			m_pVertexBuffer(&vertexBuffer), m_pIndexBuffer(&indexBuffer), m_vSubsets(vSubsets), m_pGeometry(&geometry), m_numStates(numStates), m_pSkeleton(pSkeleton), m_pInfo(nullptr),
			m_isSharedIndexBuffer(isSharedIndexBuffer)
        {
	        m_pStates = new const render::StateGroup*[numStates];
			m_pDrawCalls = new const render::BaseDrawCall*[m_vSubsets.size()];

			geometry.Bind(m_state);
			vertexBuffer.Bind(m_state, BindTarget::VERTEX);
			indexBuffer.Bind(m_state);

			SetStateGroup(0, m_state);
        }

		BaseMesh::~BaseMesh(void)
        {
			RemoveStateGroup(0, false);

			delete m_pInfo;

			for(unsigned int i = 0; i < m_vSubsets.size(); i++)
			{
				delete m_pDrawCalls[i];
			}
			delete[] m_pDrawCalls;
	        
			for(unsigned int i = 0; i < m_numStates; i++)
			{
				delete m_pStates[i];
			}
	        delete[] m_pStates;

			delete m_pVertexBuffer;

			if(!m_isSharedIndexBuffer)
				delete m_pIndexBuffer;
#ifdef ACL_API_GL4
			delete m_pGeometry;
#endif
			delete m_pSkeleton;

			SigChanged(nullptr);
        }

		void BaseMesh::SetLoadInfo(const MeshLoadInfo* pInfo)
		{
			m_pInfo = pInfo;
		}

		void BaseMesh::SetVertexCount(unsigned int numVertices, unsigned int subset)
		{
			ACL_ASSERT(subset < m_vSubsets.size());

			m_vSubsets[subset] = numVertices;
			OnChangeVertexCount(subset);
			SigChanged(this);
		}

		void BaseMesh::SetIndexBuffer(IIndexBuffer& buffer)
		{
			ACL_ASSERT(m_isSharedIndexBuffer);

			if(m_pIndexBuffer != &buffer)
			{
				buffer.Bind(m_state);
				m_pIndexBuffer = &buffer;
				SigChanged(this);
			}
		}

		unsigned int BaseMesh::GetNumSubsets(void) const
		{
			return m_vSubsets.size();
		}

		const render::StateGroup** BaseMesh::GetStates(void) const
        {
	        return m_pStates;
        }

		unsigned int BaseMesh::GetStateCount(void) const
        {
	        return m_numStates;
        }

		const IGeometry& BaseMesh::GetGeometry(void) const
		{
			return *m_pGeometry;
		}

		const render::BaseDrawCall& BaseMesh::GetDraw(unsigned int subset) const
        {
			ACL_ASSERT(subset < m_vSubsets.size());

	        return *m_pDrawCalls[subset];
        }

		const MeshSkeleton* BaseMesh::GetSkeleton(void) const
		{
			return m_pSkeleton;
		}

		core::Signal<const IMesh*>& BaseMesh::GetChangedSignal(void)
		{
			return SigChanged;
		}

		const MeshLoadInfo* BaseMesh::GetLoadInfo(void) const
		{
			return m_pInfo;
		}

		unsigned int BaseMesh::GetVertexCount(unsigned int subset) const
		{
			ACL_ASSERT(subset < m_vSubsets.size());

			return m_vSubsets[subset];
		}

		const IMesh::SubsetVector& BaseMesh::GetSubsets(void) const
		{
			return m_vSubsets;
		}

		IVertexBuffer& BaseMesh::GetVertexBuffer(void)
		{
			return *m_pVertexBuffer;
		}

		IIndexBuffer& BaseMesh::GetIndexBuffer(void)
		{
			return *m_pIndexBuffer;
		}

		void BaseMesh::SetStateGroup(unsigned int slot, const render::StateGroup& group)
		{
			ACL_ASSERT(slot < m_numStates);

			m_pStates[slot] = &group;
		}

		void BaseMesh::RemoveStateGroup(unsigned int slot, bool bDelete)
		{
			ACL_ASSERT(slot < m_numStates);

			if(bDelete)
				delete m_pStates[slot];

			m_pStates[slot] = nullptr;
		}

		void BaseMesh::SetDrawCall(unsigned int slot, const render::BaseDrawCall& call)
		{
			ACL_ASSERT(slot < m_vSubsets.size());

			m_pDrawCalls[slot] = &call;
		}

		void BaseMesh::RemoveDrawCall(unsigned int slot, bool bDelete)
		{
			ACL_ASSERT(slot < m_vSubsets.size());

			if(bDelete)
				delete m_pDrawCalls[slot];

			m_pDrawCalls[slot] = nullptr;
		}

		render::StateGroup& BaseMesh::GetOwnStates(void)
		{
			return m_state;
		}

    }
}