#pragma once
#include "IMesh.h"
#include "..\ApiDef.h"
#include "..\Render\StateGroup.h"

namespace acl
{
	namespace gfx
	{

		class IVertexBuffer;
		class IIndexBuffer;
		
		class BaseMesh : 
			public IMesh
		{
		public:
			BaseMesh(const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry);
			BaseMesh(IVertexBuffer& vertexBuffer, IIndexBuffer& indexBuffer, bool isSharedIndexBuffer, const SubsetVector& vSubsets, unsigned int numStates, const IGeometry& geometry, MeshSkeleton* pSkeleton);
			~BaseMesh(void);

			void SetLoadInfo(const MeshLoadInfo* pInfo) override final;
			void SetVertexCount(unsigned int numVertices, unsigned int subset) override final;
			void SetIndexBuffer(IIndexBuffer& buffer) override final;

			unsigned int GetNumSubsets(void) const override final;
			unsigned int GetStateCount(void) const override final;
			const IGeometry& GetGeometry(void) const override final;
			const render::StateGroup** GetStates(void) const override final;
			const render::BaseDrawCall& GetDraw(unsigned int subset) const override final;
			const MeshSkeleton* GetSkeleton(void) const override final;
			core::Signal<const IMesh*>& GetChangedSignal(void) override final;
			const MeshLoadInfo* GetLoadInfo(void) const override final;
			unsigned int GetVertexCount(unsigned int subset) const override final;
			const SubsetVector& GetSubsets(void) const override final;

			IVertexBuffer& GetVertexBuffer(void) override final;
			IIndexBuffer& GetIndexBuffer(void) override final;

		protected:

			void SetStateGroup(unsigned int slot, const render::StateGroup& group);
			void RemoveStateGroup(unsigned int slot, bool bDelete);
			void SetDrawCall(unsigned int slot, const render::BaseDrawCall& call);
			void RemoveDrawCall(unsigned int slot, bool bDelete);

			render::StateGroup& GetOwnStates(void);

		private:

			virtual void OnChangeVertexCount(unsigned int subset) = 0;

			bool m_isSharedIndexBuffer;
			unsigned int m_numStates;
			SubsetVector m_vSubsets;

			render::StateGroup m_state;
			const render::StateGroup** m_pStates;
			const render::BaseDrawCall** m_pDrawCalls;

			IVertexBuffer* m_pVertexBuffer;
			IIndexBuffer* m_pIndexBuffer;
			const MeshLoadInfo* m_pInfo;
			const IGeometry* m_pGeometry;
			const MeshSkeleton* m_pSkeleton;

			core::Signal<const IMesh*> SigChanged;
		};
	}
}