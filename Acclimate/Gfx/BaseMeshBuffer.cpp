#include "BaseMeshBuffer.h"
#include "..\System\Log.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		/***********************************
		* VertexBuffer
		***********************************/

		BaseVertexBuffer::BaseVertexBuffer(size_t numVertices, unsigned int stride) : 
			m_pData(nullptr), m_size(numVertices * stride), m_stride(stride)
		{
		}

		void BaseVertexBuffer::Map(MapFlags flags)
		{
			if(m_pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Vertex buffer map call while texture is already mapped.");
			else
				m_pData = OnMap(flags);

			ACL_ASSERT(m_pData);
		}

		void BaseVertexBuffer::Unmap(void)
		{
			if(!m_pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Vertex buffer unmap call while texture is not mapped.");
			else
			{
				m_pData = nullptr;
				OnUnmap();
			}
		}

		void* BaseVertexBuffer::GetData(void)
		{
			return m_pData;
		}

		unsigned int BaseVertexBuffer::GetSize(void) const
		{
			return m_size;
		}

		unsigned int BaseVertexBuffer::GetStride(void) const
		{
			return m_stride;
		}

		void BaseVertexBuffer::Bind(render::StateGroup& group, BindTarget target, unsigned int offset) const
		{
			OnBind(group, target, offset * m_stride);
		}

		/***********************************
		* IndexBuffer
		***********************************/

		BaseIndexBuffer::BaseIndexBuffer(size_t numVertices) :
			m_pData(nullptr), m_size(numVertices * sizeof(unsigned int))
		{
		}

		void BaseIndexBuffer::Map(MapFlags flags)
		{
			if(m_pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Index buffer map call while texture is already mapped.");
			else
				m_pData = OnMap(flags);

			ACL_ASSERT(m_pData);
		}

		void BaseIndexBuffer::Unmap(void)
		{
			if(!m_pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Index buffer unmap call while texture is not mapped.");
			else
			{
				m_pData = nullptr;
				OnUnmap();
			}
		}

		unsigned int* BaseIndexBuffer::GetData(void)
		{
			return m_pData;
		}

		unsigned int BaseIndexBuffer::GetSize(void) const
		{
			return m_size;
		}

	}
}
