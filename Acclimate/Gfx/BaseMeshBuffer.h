#pragma once
#include "MeshBuffer.h"

namespace acl
{
	namespace gfx
	{

		/***********************************
		* VertexBuffer
		***********************************/

		class BaseVertexBuffer :
			public IVertexBuffer
		{
		public:
			BaseVertexBuffer(size_t numVertices, unsigned int stride);

			void Map(MapFlags flags) override final;
			void Unmap(void) override final;

			void* GetData(void) override final;
			unsigned int GetSize(void) const override final;
			unsigned int GetStride(void) const override final;
			
			void Bind(render::StateGroup& group, BindTarget target, unsigned int offset) const override final;

		private:

			virtual void* OnMap(MapFlags flags) = 0;
			virtual void OnUnmap(void) = 0;
			virtual void OnBind(render::StateGroup& group, BindTarget target, unsigned int offset) const = 0;

			void* m_pData;
			size_t m_size;
			unsigned int m_stride;
		};

		/***********************************
		* IndexBuffer
		***********************************/

		class BaseIndexBuffer :
			public IIndexBuffer
		{
		public:
			BaseIndexBuffer(size_t numIndices);

			void Map(MapFlags flags) override final;
			void Unmap(void) override final;

			unsigned int* GetData(void) override final;
			unsigned int GetSize(void) const override final;

		private:

			virtual unsigned int* OnMap(MapFlags flags) = 0;
			virtual void OnUnmap(void) = 0;

			unsigned int* m_pData;
			size_t m_size;
		};

	}
}

