#include "BaseMeshLoader.h"

#include "TextureAccessors.h"
#include "MeshFile.h"
#include "MeshSkeleton.h"
#include "IGeometryCreator.h"
#include "..\File\File.h"
#include "..\Math\Vector.h"
#include "..\Math\Vector2f.h"
#include "..\Math\Vector3.h"
#include "..\System\Exception.h"

namespace acl
{
    namespace gfx
    {

		BaseMeshLoader::BaseMeshLoader(const Textures& textures, Meshes& meshes, IGeometryCreator& creator) : m_pTextures(&textures), 
			m_pMeshes(&meshes), m_pCreator(&creator)
        {
        }
                
        /*********************************************************
		* Mesh-File
		*********************************************************/

		IMesh& BaseMeshLoader::Load(const std::wstring& stName, const std::wstring& stFilename) const
        {	        
			try
			{
				const MeshFile file = m_fileLoader.Load(stFilename);

				IGeometry::AttributeVector vAttributes;
				if(file.HasOptions(POS))
					vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3);
				else
					ACL_ASSERT(false);

				if(file.HasOptions(TEX))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 2); // TEXCOORD

				if(file.HasOptions(NRM))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 1, AttributeType::FLOAT, 3); // NORMAL
				if(file.HasOptions(BIN))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 2, AttributeType::FLOAT, 3); // BINORMAL
				if(file.HasOptions(TAN))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 3, AttributeType::FLOAT, 3); // TANGENT
				if(file.HasOptions(WEIGHTS))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 4, AttributeType::FLOAT, 4); // WEIGHTS
				if(file.HasOptions(INDICES))
					vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 5, AttributeType::UBYTE4, 4); // INDICES

				auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);

				MeshSkeleton* pSkeleton = nullptr;
				if(auto pFileSkeleton = file.GetSkeleton())
					pSkeleton = new MeshSkeleton(*pFileSkeleton);

				auto pSubsetData = file.GetSubsetData();
				auto pNumSubsets = file.GetNumSubsets();
				SubsetVector vSubsets;
				if(pSubsetData)
				{
					for(unsigned int i = 0; i < pNumSubsets; i++)
					{
						vSubsets.push_back(pSubsetData[i] * geometry.GetNumVerticesPerFace());
					}
				}
				else
				{
					vSubsets.push_back(file.NumIndices());
				}

				auto& mesh = OnCreateMesh(geometry, file.GetNumVertices(), file.GetData(), file.NumIndices(), file.GetFaceData(), vSubsets, pSkeleton);
				m_pMeshes->Add(stName, mesh);

				MeshLoadInfo* pInfo = new MeshLoadInfo(stName, stFilename);
				mesh.SetLoadInfo(pInfo);

				return mesh;
			}
			catch(fileException&)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "Failed to load mesh file", file::FullPath(stFilename));
				throw fileException();
			}
        }

        /******************************************
		* Height Map
		******************************************/

		struct HEIGHTMAPVERTEX
		{
			float x, y, z;
			math::Vector3 vNrm;
			float u, v;
		};

		IMesh& BaseMeshLoader::HeightMap(const std::wstring& stName, const std::wstring& stTexture) const
        {
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 3);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 1, AttributeType::FLOAT, 2);

			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);

			ITexture* pTexture = m_pTextures->Get(stTexture);
			const math::Vector2& vSize = pTexture->GetSize();

			const unsigned int numVertices = vSize.x * vSize.y;

			HEIGHTMAPVERTEX* pVertices = new HEIGHTMAPVERTEX[numVertices];
			HEIGHTMAPVERTEX* pVertexBuffer = pVertices;

            const float scale = 0.2f;
            const float texScale = 16.0f;

			TextureAccessorL8 accessor(*pTexture, true);

			for(int i = 0; i < vSize.x; i++)
			{
				for(int j = 0; j  < vSize.y; j++)
				{
					//position
					pVertices->x = -vSize.x / 2 + static_cast<float>(j);
					pVertices->y = accessor.GetPixel(i, j) * scale;
					pVertices->z = -vSize.y / 2 + static_cast<float>(i);

					//texture coordinates
					pVertices->u = static_cast<float>(i) / vSize.x * texScale;
					pVertices->v = static_cast<float>(j) / vSize.y * texScale;

					pVertices++;
				}
			}

			std::vector<math::Vector3> vNormals;
			vNormals.reserve((vSize.x - 1) * (vSize.y - 1));
			for(int j = 0; j<(vSize.y - 1); j++)
			{
				for(int i = 0; i<(vSize.x - 1); i++)
				{
					const size_t index1 = (j * vSize.y) + i;
					const size_t index2 = (j * vSize.y) + (i + 1);
					const size_t index3 = ((j + 1) * vSize.y) + i;

					// Get three vertices from the face.
					const math::Vector3 vVertex1(pVertexBuffer[index1].x, pVertexBuffer[index1].y, pVertexBuffer[index1].z);
					const math::Vector3 vVertex2(pVertexBuffer[index2].x, pVertexBuffer[index2].y, pVertexBuffer[index2].z);
					const math::Vector3 vVertex3(pVertexBuffer[index3].x, pVertexBuffer[index3].y, pVertexBuffer[index3].z);

					// Calculate the two vectors for this face.
					const math::Vector3 vDist1 = vVertex1 - vVertex3;
					const math::Vector3 vDist2 = vVertex3 - vVertex2;

					// Calculate the cross product of those two vectors to get the un-normalized value for this face normal.
					vNormals.push_back(vDist1.Cross(vDist2));
				}
			}

			// Now go through all the vertices and take an average of each face normal 	
			// that the vertex touches to get the averaged normal for that vertex.
			for(int j = 0; j<vSize.y; j++)
			{
				for(int i = 0; i<vSize.x; i++)
				{
					// Initialize the sum.
					math::Vector3 vSum;

					// Initialize the count.
					size_t count = 0;

					// Bottom left face.
					if(((i - 1) >= 0) && ((j - 1) >= 0))
					{
						const size_t index = ((j - 1) * (vSize.y - 1)) + (i - 1);

						vSum += vNormals[index];
						count++;
					}

					// Bottom right face.
					if((i < (vSize.x - 1)) && ((j - 1) >= 0))
					{
						const size_t index = ((j - 1) * (vSize.y - 1)) + i;

						vSum += vNormals[index];
						count++;
					}

					// Upper left face.
					if(((i - 1) >= 0) && (j < (vSize.y - 1)))
					{
						const size_t index = (j * (vSize.y - 1)) + (i - 1);

						vSum += vNormals[index];
						count++;
					}

					// Upper right face.
					if((i < (vSize.x - 1)) && (j < (vSize.y - 1)))
					{
						const size_t index = (j * (vSize.y - 1)) + i;

						vSum += vNormals[index];
						count++;
					}

					// Take the average of the faces touching this vertex.
					vSum /= (float)count;
					vSum.Normalize();

					// Get an index to the vertex location in the height map array.
					const size_t index = (j * vSize.y) + i;

					// Normalize the final shared normal for this vertex and store it in the height map array.
					pVertexBuffer[index].vNrm = vSum;
				}
			}

			const unsigned int numIndices = (vSize.x - 1) * (vSize.y - 1) * 6;
			unsigned int* pIndexBuffer = new unsigned int[numIndices];

			unsigned int index = 0;
			for(unsigned int j = 0; j < (unsigned int)vSize.y-1; j++)
			{
				for(unsigned int i = 0; i < (unsigned int)vSize.x-1; i++)
				{
					pIndexBuffer[index++] = vSize.y * (j + 1) + i + 1;
					pIndexBuffer[index++] = vSize.y *  j + i;
					pIndexBuffer[index++] = vSize.y * (j + 1) + i;
					pIndexBuffer[index++] = vSize.y * (j + 1) + i + 1;
					pIndexBuffer[index++] = vSize.y *  j + i + 1;
					pIndexBuffer[index++] = vSize.y *  j + i;
				}
			}

			SubsetVector vSubsets;
			vSubsets.push_back(numIndices);

			auto& mesh = OnCreateMesh(geometry, numVertices, (const float*)pVertexBuffer, numIndices, pIndexBuffer, vSubsets);

			m_pMeshes->Add(stName, mesh);

			delete[] pVertexBuffer;
			delete[] pIndexBuffer;

			return mesh;
        }

        /******************************************
		* Line Cube
		******************************************/

        struct LINEVERTEX
        {
	        math::Vector3 pos;
        };

		IMesh& BaseMeshLoader::Line(const std::wstring& stName, unsigned int segments) const
		{
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3); // position
			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::LINE, vAttributes);

			const size_t numVertices = segments + 1;

			const size_t numIndices = segments * 2;
			std::vector<unsigned int> vIndices;
			vIndices.reserve(numIndices);

			for(unsigned int i = 0; i < segments; i++)
			{
				vIndices.push_back(i);
				vIndices.push_back(i+1);
			}

			SubsetVector vSubsets;
			vSubsets.push_back(numIndices);

			auto& mesh = OnCreateMesh(geometry, numVertices, nullptr, numIndices, (const unsigned int*)&vIndices[0], vSubsets);
			m_pMeshes->Add(stName, mesh);
			return mesh;
		}

		IMesh& BaseMeshLoader::LineCube(const std::wstring& stName, float size) const
        {
            std::vector<math::Vector3> vPoints;
	        vPoints.push_back(math::Vector3(-size, -size, -size));
	        vPoints.push_back(math::Vector3(-size, -size, size));
	        vPoints.push_back(math::Vector3(-size, size, size));
	        vPoints.push_back(math::Vector3(-size, size, -size));
	        vPoints.push_back(math::Vector3(size, -size, -size));
	        vPoints.push_back(math::Vector3(size, -size, size));
	        vPoints.push_back(math::Vector3(size, size, size));
	        vPoints.push_back(math::Vector3(size, size, -size));

			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3); // position
			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::LINE, vAttributes);

			const std::vector<unsigned int> vIndices = { 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7 };

			SubsetVector vSubsets;
			vSubsets.push_back(vIndices.size());

			auto& mesh = OnCreateMesh(geometry, vPoints.size(), (float*)&vPoints[0], vIndices.size(), (const unsigned int*)&vIndices[0], vSubsets);
			m_pMeshes->Add(stName, mesh);
			return mesh;
        }

        /******************************************
		* Line Grid
		******************************************/

		IMesh& BaseMeshLoader::LineGrid(const std::wstring& stName, float size, int divisions) const
        {
   //         const int divisionFactor = divisions + 1;
	  //      const float sizeFactor = 1.0f / divisions;

	  //      D3DVERTEXELEMENT9 Elements[] =
	  //      {
		 //       {0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},  
		 //       D3DDECL_END() 
	  //      };

	  //      const unsigned int numVertices = divisionFactor*divisionFactor;
	  //      static d3d::VertexDeclaration declaration(*m_pDevice, Elements);
	  //      d3d::VertexBuffer* pVertexBuffer = new d3d::VertexBuffer(*m_pDevice, RenderTypes::LINE, sizeof(LINEVERTEX), declaration, numVertices, d3d::VertexBufferUsage::WRITEONLY);
	  //      LINEVERTEX* pBuffer = nullptr;
	  //      pVertexBuffer->Lock((void**)&pBuffer, d3d::BufferLock::NONE);

	  //      for(int i = 0; i < divisionFactor; i++)
	  //      {
		 //       for(int j = 0; j < divisionFactor; j++)
		 //       {
			//        pBuffer[j+i*divisionFactor].pos =math::Vector3( i*sizeFactor*size*2 - size, 0.0f, j*sizeFactor*size*2 - size);
		 //       }
	  //      }

	  //      pVertexBuffer->Unlock();

	  //      std::vector<int> vIndices;

	  //      for(int i = 0; i < divisionFactor; i++)
	  //      {
		 //       for(int j = 0; j < divisionFactor; j++)
		 //       {
			//        const int node = i*divisionFactor + j;

			//        if( i < divisions)
			//        {
			//	        vIndices.push_back(node);
			//	        vIndices.push_back(node + divisionFactor);
			//        }

			//        if(j > divisions-1)
			//	        continue;

			//        vIndices.push_back(node);
			//        vIndices.push_back(node+1);
		 //       }
	  //      }

	  //      d3d::IndexBuffer* pIndexBuffer = new d3d::IndexBuffer(*m_pDevice, vIndices.size());

	  //      d3d::Mesh* pDX9Mesh = new d3d::Mesh(vIndices.size(), *pVertexBuffer, *pIndexBuffer);
	  //      Mesh* pMesh = new Mesh(*pDX9Mesh);

			//IndexBufferAccessor<unsigned int> accessor(*pMesh);
			//unsigned int* pData = accessor.GetData();
	  //      memcpy(pData, (void*)&vIndices[0], sizeof(unsigned int)*vIndices.size());

	  //      m_pMeshes->Add(stName, *pMesh);

			IMesh* pMesh = nullptr;
			return *pMesh;
        }

        /******************************************
		* Screen Quad
		******************************************/

        struct QUADVERTEX
        {
	        float x,y;
	        float u,v;
        };

		IMesh& BaseMeshLoader::ScreenQuad(const std::wstring& stName, const math::Vector2& vSize) const
		{
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 2);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 2);

			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);

			static const QUADVERTEX quadVertices[] =
			{
				{ -1.0f, 1.0f, 0.0f, 0.0f },
				{ 1.0f, 1.0f, 1.0f, 0.0f },
				{ 1.0f, -1.0f, 1.0f, 1.0f },
				{ -1.0f, -1.0f, 0.0f, 1.0f }
			};

			const unsigned int indices[] = { 0, 1, 3, 3, 1, 2 };

			SubsetVector vSubsets;
			vSubsets.push_back(6);

			auto& mesh = OnCreateMesh(geometry, 4, (const float*)quadVertices, 6, indices, vSubsets);
	        m_pMeshes->Add(stName, mesh);
			return mesh;
        }

        /******************************************
		* Billboard
		******************************************/

        struct BILLBOARDVERTEX
        {
	        float x, y, z;
	        float u, v;
        };

		IMesh& BaseMeshLoader::BillBoard(const std::wstring& stName, float size) const
        {
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3, 0, 0);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 2, 0, 0);

			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);

			const BILLBOARDVERTEX vertices[] =
	        {
			        {0.0f,	-size, -size,	1.0f, 1.0f},
			        {0.0f,	-size, size,	0.0f, 1.0f},
			        {0.0f,	 size, size,	0.0f, 0.0f},
			        {0.0f,	 size, -size,	1.0f, 0.0f}
	        };	

			const unsigned int indices[] = { 3, 0, 2, 2, 0, 1 };

			SubsetVector vSubsets;
			vSubsets.push_back(6);

			auto& mesh = OnCreateMesh(geometry, 4, (const float*)vertices, 6, indices, vSubsets);
	        m_pMeshes->Add(stName, mesh);
			return mesh;
        }

		/******************************************
		* Grid
		******************************************/

		IMesh& BaseMeshLoader::Grid(const std::wstring& stName, int sizeX, int sizeY) const
		{
			IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3); // position
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 2); // texcoord
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 1, AttributeType::FLOAT, 3); // normal

			auto& geometry = m_pCreator->CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);

			const unsigned int numVertices = sizeX*sizeY;

			const unsigned int numIndices = (sizeX-1)*(sizeY-1)*6;
	        unsigned int* pIndices = new unsigned int[numIndices];

			unsigned int i = 0;
			for(int v=0; v < sizeY-1; v++)
			{
				for(int u=0; u < sizeX-1; u++)
				{
					// face 1 |/
					pIndices[i++] = v*sizeX + u;
					pIndices[i++] = v*sizeX + u + 1;
					pIndices[i++] = (v + 1)*sizeX + u;

					// face 2 /|
					pIndices[i++] = (v + 1)*sizeX + u;
					pIndices[i++] = v*sizeX + u + 1;
					pIndices[i++] = (v + 1)*sizeX + u + 1;
				}
			}

			SubsetVector vSubsets;
			vSubsets.push_back(numIndices);

			auto& mesh = OnCreateMesh(geometry, numVertices, nullptr, numIndices, pIndices, vSubsets);
	        m_pMeshes->Add(stName, mesh);

			delete[] pIndices;

			return mesh;
		}

		IMesh& BaseMeshLoader::Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, size_t numVertices, const float* pVertices, size_t numIndices, const unsigned int* pIndices, PrimitiveType type) const
		{
			auto& geometry = m_pCreator->CreateGeometry(type, vAttributes);

			const size_t stride = geometry.GetVertexSize();

			SubsetVector vSubsets;
			vSubsets.push_back(numIndices);

			auto& mesh = OnCreateMesh(geometry, numVertices, pVertices, numIndices, pIndices, vSubsets);

			auto pLoadInfo = new MeshLoadInfo(stName);
			mesh.SetLoadInfo(pLoadInfo);

			m_pMeshes->Add(stName, mesh);
			return mesh;
		}

		IMesh& BaseMeshLoader::Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, const IndexVector& vIndices, PrimitiveType type) const
		{
			auto& geometry = m_pCreator->CreateGeometry(type, vAttributes);

			const size_t numIndices = vIndices.size();
			const size_t stride = geometry.GetVertexSize();

			SubsetVector vSubsets;
			vSubsets.push_back(numIndices);

			auto& mesh = OnCreateMesh(geometry, vVertices.size() / (stride / sizeof(float)), &vVertices[0], numIndices, &vIndices[0], vSubsets);

			auto pLoadInfo = new MeshLoadInfo(stName);
			mesh.SetLoadInfo(pLoadInfo);

			m_pMeshes->Add(stName, mesh);
			return mesh;
		}

		IMesh& BaseMeshLoader::Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, IIndexBuffer& indexBuffer, PrimitiveType type) const
		{
			auto& geometry = m_pCreator->CreateGeometry(type, vAttributes);
			const size_t stride = geometry.GetVertexSize();

			auto& mesh = OnCreateMesh(geometry, vVertices, indexBuffer);

			auto pLoadInfo = new MeshLoadInfo(stName);
			mesh.SetLoadInfo(pLoadInfo);

			m_pMeshes->Add(stName, mesh);
			return mesh;
		}

        /******************************************
		* Instanced
		******************************************/

		IInstancedMesh* BaseMeshLoader::Instanced(const std::wstring& stName, unsigned int numInstances, const IGeometry::AttributeVector& vAttributes) const
        {
			if(auto pMesh = m_pMeshes->Get(stName))
            {
				auto& meshGeometry = pMesh->GetGeometry();
				IGeometry::AttributeVector vSummedAttributes(meshGeometry.GetAttributes());
				for(auto& attribute : vAttributes)
				{
					vSummedAttributes.push_back(attribute);
				}

				auto& geometry = m_pCreator->CreateGeometry(meshGeometry.GetPrimitiveType(), vSummedAttributes);

				return &OnCreateInstancedMesh(geometry, numInstances, *pMesh);
            }

			return nullptr;
        }
    
    }
}