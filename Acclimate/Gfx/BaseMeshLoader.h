#pragma once
#include "IMeshLoader.h"
#include "IGeometry.h"
#include "Meshes.h"
#include "Textures.h"
#include "MeshFileLoader.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace math
	{
		struct Vector2f;
	}

    namespace gfx
    {
		
		class IGeometryCreator;

		enum class BufferUsage
		{
			WRITEONLY
		};

        class BaseMeshLoader :
			public IMeshLoader
        {
        public:
			BaseMeshLoader(const Textures& textures, Meshes& meshes, IGeometryCreator& creator);

			IMesh& Load(const std::wstring& stName, const std::wstring& stFilename) const override final;
			IMesh& HeightMap(const std::wstring& stName, const std::wstring& stTexture) const override final;
			IMesh& Line(const std::wstring& stName, unsigned int segments) const override final;
			IMesh& LineCube(const std::wstring& stName, float size) const override final;
			IMesh& LineGrid(const std::wstring& stName, float size, int divisions) const override final;
			IMesh& ScreenQuad(const std::wstring& stName, const math::Vector2& vSize) const override final;
			IMesh& BillBoard(const std::wstring& stName, float size) const override final;
			IMesh& Grid(const std::wstring& stName, int sizeX, int sizeY) const override final;
			IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, size_t numVertices, const float* pVertices, size_t numIndices, const unsigned int* pIndices, PrimitiveType type) const override final;
			IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, const IndexVector& vIndices, PrimitiveType type) const override final;
			IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, IIndexBuffer& indexBuffer, PrimitiveType type) const override final;

			IInstancedMesh* Instanced(const std::wstring& stName, unsigned int numInstances, const IGeometry::AttributeVector& vAttributes) const override final;

		protected:

			typedef std::vector<unsigned int> SubsetVector;

        private:

			virtual IMesh& OnCreateMesh(const IGeometry& geometry, size_t numVertices, const float* pVertices, size_t numIndicies, const unsigned int* pIndicies, const SubsetVector& vSubsets, MeshSkeleton* pSkeleton = nullptr) const = 0;
			virtual IMesh& OnCreateMesh(const IGeometry& geometry, const VertexVector& vVertices, IIndexBuffer& indexBuffer) const = 0;
			virtual IInstancedMesh& OnCreateInstancedMesh(const IGeometry& geometry, size_t numInstances, IMesh& mesh) const = 0;

            const Textures* m_pTextures;
            Meshes* m_pMeshes;
			IGeometryCreator* m_pCreator;
			MeshFileLoader m_fileLoader;
        };

    }
}