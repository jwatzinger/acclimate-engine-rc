#include "BaseModel.h"

#include "Defines.h"
#include "IMesh.h"
#include "IMaterial.h"
#include "IEffect.h"
#include "ICbuffer.h"
#include "EffectInternalAccessor.h"
#include "CbufferInternalAccessor.h"
#include "ModelInstance.h"
#include "..\System\Log.h"

namespace acl
{
    namespace gfx
    {

		BaseModel::BaseModel(const ICbufferLoader& loader) :
			m_pMesh(nullptr), m_vMaterials(MAX_MODEL_PASSES, nullptr), 
			m_pVCbuffer(nullptr), m_pPCbuffer(nullptr), m_pInfo(nullptr),
			m_pLoader(&loader)
#ifndef ACL_API_DX9
			, m_pGCbuffer(nullptr)
#endif
		{
        }

		BaseModel::BaseModel(const ICbufferLoader& loader, IMesh& mesh) :
			m_pMesh(&mesh), m_vMaterials(MAX_MODEL_PASSES, nullptr), 
			m_pVCbuffer(nullptr), m_pPCbuffer(nullptr), m_pInfo(nullptr), m_pLoader(&loader)
#ifndef ACL_API_DX9
			, m_pGCbuffer(nullptr)
#endif
        {
			m_pMesh->GetChangedSignal().Connect(this, &BaseModel::OnMeshChanged);
        }

		BaseModel::BaseModel(const BaseModel& model) : m_pMesh(model.m_pMesh), m_vMaterials(model.m_vMaterials),
			m_pVCbuffer(nullptr), m_pPCbuffer(nullptr),
			m_pInfo(new ModelLoadInfo(*model.m_pInfo)), m_pLoader(model.m_pLoader)
#ifndef ACL_API_DX9
			, m_pGCbuffer(nullptr)
#endif
		{
			if(model.m_pVCbuffer)
				m_pVCbuffer = &model.m_pVCbuffer->Clone();

			if(model.m_pPCbuffer)
				m_pPCbuffer = &model.m_pPCbuffer->Clone();

#ifndef ACL_API_DX9
			if(model.m_pGCbuffer)
				m_pGCbuffer = &model.m_pGCbuffer->Clone();
#endif

			if(m_pMesh)
				m_pMesh->GetChangedSignal().Connect(this, &BaseModel::OnMeshChanged);

			UpdateMaterials();
		}

		BaseModel::~BaseModel(void)
		{
			delete m_pInfo;

			delete m_pVCbuffer;
			delete m_pPCbuffer;
#ifndef ACL_API_DX9
			delete m_pGCbuffer;
#endif

			if(m_pMesh)
				m_pMesh->GetChangedSignal().Disconnect(this, &BaseModel::OnMeshChanged);

			for(auto pMaterial : m_vMaterials)
			{
				if(pMaterial)
					pMaterial->GetChangedSignal().Disconnect(this, &BaseModel::OnMaterialChanged);
			}

			SigChanged(nullptr, nullptr, nullptr, nullptr);
		}

		IModel& BaseModel::Clone(void) const
		{
			return *new BaseModel(*this);
		}

		void BaseModel::SetMesh(IMesh& mesh)
        {
			if(m_pMesh != &mesh)
			{
				if(m_pMesh)
					m_pMesh->GetChangedSignal().Disconnect(this, &BaseModel::OnMeshChanged);

				m_pMesh = &mesh;
				ChangeSignal();

				if(m_pMesh)
					m_pMesh->GetChangedSignal().Connect(this, &BaseModel::OnMeshChanged);
			}
        }

		void BaseModel::SetMaterial(size_t pass, IMaterial* pMaterial)
        {
			if(pass >= MAX_MODEL_PASSES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set material to model for invalid pass", pass, "(only up to", MAX_MODEL_PASSES, "supported)");
				return;
			}

			if(m_vMaterials[pass] != pMaterial)
			{
				if(auto pMaterial = m_vMaterials[pass])
					pMaterial->GetChangedSignal().Disconnect(this, &BaseModel::OnMaterialChanged);
					
				m_vMaterials[pass] = pMaterial;

				if(pMaterial)
					pMaterial->GetChangedSignal().Connect(this, &BaseModel::OnMaterialChanged);

				MaterialChange(pMaterial);

				ChangeSignal();
			}
        }

		void BaseModel::SetLoadInfo(const ModelLoadInfo* pInfo)
		{
			if(pInfo != m_pInfo)
			{
				delete m_pInfo;
				m_pInfo = pInfo;
			}
		}

		IMaterial* BaseModel::GetMaterial(size_t pass) const
		{
			if(pass >= MAX_MODEL_PASSES)
				return nullptr;

			return m_vMaterials[pass];
		}

		const IMesh* BaseModel::GetMesh(void) const
		{
			return m_pMesh;
		}

		const ModelLoadInfo* BaseModel::GetLoadInfo(void) const
		{
			return m_pInfo;
		}

		bool BaseModel::HasPass(size_t pass) const
		{
			if(pass >= MAX_MODEL_PASSES)
				return nullptr;

			return m_vMaterials[pass] != nullptr;
		}

		ModelInstance& BaseModel::MakeUniqueInstance(void)
		{
			ICbuffer* pVCbuffer = nullptr, *pPCbuffer = nullptr, *pGCbuffer = nullptr;

			if(m_pVCbuffer)
				pVCbuffer = &m_pVCbuffer->Clone();
			if(m_pPCbuffer)
				pPCbuffer = &m_pPCbuffer->Clone();

#ifndef ACL_API_DX9
			if(m_pGCbuffer)
				pGCbuffer = &m_pGCbuffer->Clone();
#endif
			auto& instance = *new ModelInstance(*this, pVCbuffer, pPCbuffer, pGCbuffer, true);

			SigChanged.Connect(&instance, &ModelInstance::OnParentDirty);

			return instance;
		}

		ModelInstance& BaseModel::CreateInstance(void)
		{
			ICbuffer* pVCbuffer = nullptr, *pPCbuffer = nullptr, *pGCbuffer = nullptr;

			if(m_pVCbuffer)
				pVCbuffer = &m_pVCbuffer->Clone();
			if(m_pPCbuffer)
				pPCbuffer = &m_pPCbuffer->Clone();

#ifndef ACL_API_DX9
			if(m_pGCbuffer)
				pGCbuffer = &m_pGCbuffer->Clone();
#endif
			auto& instance = *new ModelInstance(*this, pVCbuffer, pPCbuffer, pGCbuffer, false);

			SigChanged.Connect(&instance, &ModelInstance::OnParentDirty);

			return instance;
		}

		void BaseModel::RemoveInstance(ModelInstance& instance)
		{
			SigChanged.Disconnect(&instance, &ModelInstance::OnParentDirty);
		}

		void BaseModel::GenerateInstances(std::vector<std::vector<render::Instance>>& vInstances, const render::StateGroup& states) const
		{
			vInstances.clear();

			if(!m_pMesh)
				return;

			const unsigned int numSubsets = m_pMesh->GetNumSubsets();
			const unsigned int cMeshStates = m_pMesh->GetStateCount();
			const render::StateGroup** pMeshStates = m_pMesh->GetStates();

			const render::StateGroup** ppGroups = new const render::StateGroup*[NUM_MODEL_STATES + cMeshStates];
			ppGroups[0] = &states;
			for(unsigned int j = 0; j < cMeshStates; j++)
			{
				ppGroups[NUM_MODEL_STATES + j] = pMeshStates[j];
			}

			render::Key64 sortKey;
			sortKey.bits = 0;

			unsigned int pass = 0;
			for(auto pMaterial : m_vMaterials)
			{
				if(pMaterial)
				{
					sortKey.Material = pMaterial->GetId();

					vInstances.resize(pass + 1);
					auto& vInstance = vInstances[pass];
					vInstance.reserve(numSubsets);

					ppGroups[2] = &pMaterial->GetEffect().GetState();

					for(unsigned int i = 0; i < numSubsets; i++)
					{
						ppGroups[1] = &pMaterial->GetState(i);

						vInstance.emplace_back(sortKey, ppGroups, NUM_MODEL_STATES + cMeshStates);
						vInstance.rbegin()->SetCall(m_pMesh->GetDraw(i));
					}
				}

				pass++;
			}

			delete[] ppGroups;
		}

		void BaseModel::UpdateMaterials(void)
		{
			for(auto pMaterial : m_vMaterials)
			{
				MaterialChange(pMaterial);
			}
		}

		void BaseModel::MaterialChange(const IMaterial* pMaterial)
		{
			if(pMaterial)
			{
				/*****************************
				* Instance cbuffers
				*****************************/

				// only link buffer if this materials effect has one, otherwise
				// we might extinquish an existing connection if a later added 
				// material doesn't utilize the instance buffer.
				if(auto pVCbuffer = cloneVCbuffer(pMaterial->GetEffect(), CBUFFER_INSTANCE_ID, pMaterial->GetEffectPermutation()))
				{
					if(m_pVCbuffer)
						linkCbuffer(*m_pVCbuffer, pVCbuffer);
					else
						m_pVCbuffer = &createCbuffer(*m_pLoader, *pVCbuffer);
				}
					
				if(auto pPCbuffer = clonePCbuffer(pMaterial->GetEffect(), CBUFFER_INSTANCE_ID, pMaterial->GetEffectPermutation()))
				{
					if(m_pPCbuffer)
						linkCbuffer(*m_pPCbuffer, pPCbuffer);
					else
						m_pPCbuffer = &createCbuffer(*m_pLoader, *pPCbuffer);
				}
				
#ifndef ACL_API_DX9
				if(auto pGCbuffer = cloneGCbuffer(pMaterial->GetEffect(), CBUFFER_INSTANCE_ID, pMaterial->GetEffectPermutation()))
				{
					if(m_pGCbuffer)
						linkCbuffer(*m_pGCbuffer, pGCbuffer);
					else
						m_pGCbuffer = &createCbuffer(*m_pLoader, *pGCbuffer);
				}
#endif
			}
		}

		void BaseModel::ChangeSignal(void)
		{
#ifndef ACL_API_DX9
			SigChanged(this, m_pVCbuffer, m_pPCbuffer, m_pGCbuffer);
#else
			SigChanged(this, m_pVCbuffer, m_pPCbuffer, nullptr);
#endif
		}

		void BaseModel::OnMaterialChanged(const IMaterial& material, bool bRemove)
		{
			if(bRemove)
			{
				unsigned int i = 0;
				for(auto pMaterial : m_vMaterials)
				{
					if(&material == pMaterial)
					{
						m_vMaterials[i] = nullptr;
						break;
					}
					else
						i++;
				}
			}

			ChangeSignal();
		}

		void BaseModel::OnMeshChanged(const IMesh* pMesh)
		{
			if(!pMesh)
				m_pMesh = nullptr;

			ChangeSignal();
		}

    }
}