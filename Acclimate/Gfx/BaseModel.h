#pragma once
#include "IModel.h"
#include "..\ApiDef.h"
#include "..\Core\Signal.h"


namespace acl
{
	namespace gfx
	{

		class ICbuffer;
		class ICbufferLoader;

		class BaseModel final :
			public IModel
		{
			typedef std::vector<IMaterial*> MaterialVector;
		public:

			BaseModel(const ICbufferLoader& loader);
			BaseModel(const ICbufferLoader& loader, IMesh& mesh);
			BaseModel(const BaseModel& model);
			~BaseModel(void);

			IModel& Clone(void) const override;

			void SetMesh(IMesh& mesh) override;
			void SetMaterial(size_t pass, IMaterial* material) override;
			void SetLoadInfo(const ModelLoadInfo* pInfo) override;

			IMaterial* GetMaterial(size_t pass) const override;
			const IMesh* GetMesh(void) const override;
			const ModelLoadInfo* GetLoadInfo(void) const override;

			bool HasPass(size_t pass) const override;

			ModelInstance& MakeUniqueInstance(void) override;
			ModelInstance& CreateInstance(void) override;
			void RemoveInstance(ModelInstance& instance) override;
			void GenerateInstances(InstanceVector& vInstances, const render::StateGroup& states) const override;

		private:

			void UpdateMaterials(void);
			void MaterialChange(const IMaterial* pMaterial);
			void ChangeSignal(void);
			void OnMaterialChanged(const IMaterial& material, bool bRemove);
			void OnMeshChanged(const IMesh* pMesh);
		
			IMesh* m_pMesh;
			ICbuffer* m_pVCbuffer, *m_pPCbuffer;
#ifndef ACL_API_DX9
			ICbuffer* m_pGCbuffer;
#endif
			const ICbufferLoader* m_pLoader;
			const ModelLoadInfo* m_pInfo;

			MaterialVector m_vMaterials;
			
			// TODO: extend to other cbuffers to (now only pixelshader)
			core::Signal<const IModel*, const ICbuffer*, const ICbuffer*, const ICbuffer*> SigChanged;
		};

	}
}