#include "BaseModelLoader.h"
#include "IModel.h"
#include "BaseModel.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseModelLoader::BaseModelLoader(const Materials& materials, const Meshes& meshes, Models& models, const ICbufferLoader& cbufferLoader) : m_materials(materials), m_meshes(meshes), m_models(models),
			m_cbufferLoader(cbufferLoader)
		{
		}

		IModel& BaseModelLoader::Load(const std::wstring& stName, const std::wstring& stMesh, const PassVector& vPasses) const
		{
			if(!m_models.Has(stName))
			{
				IMesh* pMesh = m_meshes[stMesh];
				
				if(!pMesh && !stMesh.empty())
					sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to load model", stName, "using missing mesh : ", stMesh);

				ModelLoadInfo::PassMap mPasses;
				for(auto& pass : vPasses)
				{
					mPasses[pass.id] = pass.stMaterial;
				}

				ModelLoadInfo* pInfo = new ModelLoadInfo(stName, stMesh, mPasses);
				
				IModel& model = CreateModel(pMesh);
				model.SetLoadInfo(pInfo);

				for(auto& pass : vPasses)
				{
					IMaterial* pMaterial = m_materials[pass.stMaterial];
					if(pMaterial)
						model.SetMaterial(pass.id, pMaterial);
					else
						sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set missing material:", pass.stMaterial, "to model:", stName, "on pass:", pass.id, ".");
				}

				m_models.Add(stName, model);

				return model;
			}
			else
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to reload already existing model", stName, "(recreation currently unsupported)");
				return *m_models[stName];
			}
		}

		IModel& BaseModelLoader::Load(const std::wstring& stName, const std::wstring& stMesh, const std::wstring& stMaterial) const
		{
			const PassVector vPasses = { { 0, stMaterial } };

			return Load(stName, stMesh, vPasses);
		}

		ModelInstance& BaseModelLoader::LoadInstance(const std::wstring& stMesh, const PassVector& vPasses) const
		{
			IMesh* pMesh = m_meshes[stMesh];

			if(!pMesh && !stMesh.empty())
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to load model instance using missing mesh : ", stMesh);

			ModelLoadInfo::PassMap mPasses;
			for(auto& pass : vPasses)
			{
				mPasses[pass.id] = pass.stMaterial;
			}

			IModel& model = CreateModel(pMesh);

			for(auto& pass : vPasses)
			{
				IMaterial* pMaterial = m_materials[pass.stMaterial];
				if(pMaterial)
					model.SetMaterial(pass.id, pMaterial);
				else
					sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to set missing material:", pass.stMaterial, "to model-instance on pass:", pass.id, ".");
			}

			return model.MakeUniqueInstance();
		}

		ModelInstance& BaseModelLoader::LoadInstance(const std::wstring& stMesh, const std::wstring& stMaterial) const
		{
			const PassVector vPasses = { { 0, stMaterial } };

			return LoadInstance(stMesh, vPasses);
		}

		IModel& BaseModelLoader::CreateModel(IMesh* pMesh) const
		{
			if(pMesh)
				return *new BaseModel(m_cbufferLoader, *pMesh);
			else
				return *new BaseModel(m_cbufferLoader);
		}

	}
}
