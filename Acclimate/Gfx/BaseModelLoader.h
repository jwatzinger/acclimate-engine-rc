#pragma once
#include "IModelLoader.h"
#include "Materials.h"
#include "Meshes.h"
#include "Models.h"

namespace acl
{
	namespace gfx
	{

		class ICbufferLoader;

		class BaseModelLoader :
			public IModelLoader
		{
		public:
			BaseModelLoader(const Materials& materials, const Meshes& meshes, Models& models, const ICbufferLoader& cbufferLoader);
			
			IModel& Load(const std::wstring& stName, const std::wstring& stMesh, const PassVector& vPasses) const override final;
			IModel& Load(const std::wstring& stName, const std::wstring& stMesh, const std::wstring& stMaterial) const override final;
			ModelInstance& LoadInstance(const std::wstring& stMesh, const PassVector& vPasses) const override final;
			ModelInstance& LoadInstance(const std::wstring& stMesh, const std::wstring& stMaterial) const override final;

		private:

			IModel& CreateModel(IMesh* pMesh) const;

			Models& m_models;

			const Materials& m_materials;
			const Meshes& m_meshes;
			const ICbufferLoader& m_cbufferLoader;
		};

	}
}
