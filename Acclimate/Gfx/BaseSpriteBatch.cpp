#include "BaseSpriteBatch.h"
#include <algorithm>
#include "RenderStateHandler.h"
#include "ITexture.h"
#include "IEffect.h"
#include "IFontLoader.h"
#include "..\ApiDef.h"
#include "..\Gfx\IGeometryCreator.h"
#include "..\Math\Vector.h"
#include "..\System\Convert.h"
#include "..\Render\Instance.h"
#include "..\Render\IStage.h"

// TODO: gl4-also
#ifdef ACL_API_DX11
#define USE_GEOMETRY_SHADER
#endif

namespace acl
{
	namespace gfx
	{

		BaseSpriteBatch::BaseSpriteBatch(const Fonts& fonts, const IFontLoader& loader, IGeometryCreator& creator) : m_pTexture(nullptr), m_width(0), m_height(0),
			m_pEffect(nullptr), m_numBatches(0), m_currentBatch(-1), m_pStage(nullptr), m_sort(Sorting::NONE), m_pFonts(&fonts),
			m_pActiveFont(nullptr), m_pLoader(&loader)
        {
			m_sortKey.bits = 0;

			IGeometry::AttributeVector vAttributes;

#ifdef USE_GEOMETRY_SHADER
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 4);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 4);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 1, AttributeType::FLOAT, 1);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 2, AttributeType::FLOAT, 4);

			m_pGeometry = &creator.CreateGeometry(PrimitiveType::POINT, vAttributes);
#else
			vAttributes.emplace_back(AttributeSemantic::POSITION, 0, AttributeType::FLOAT, 3);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 0, AttributeType::FLOAT, 2);
			vAttributes.emplace_back(AttributeSemantic::TEXCOORD, 1, AttributeType::FLOAT, 4);

			m_pGeometry = &creator.CreateGeometry(PrimitiveType::TRIANGLE, vAttributes);
#endif


			m_pGeometry->Bind(m_states);
        }

        void BaseSpriteBatch::SetTexture(const ITexture* pTexture)
        {
            // set texture
	        m_pTexture = pTexture;

            // return if texture is set to nullptr
	        if(!m_pTexture)
		        return;

            // aquire textures size
	        
			math::Vector2 vSize = m_pTexture->GetSize();
			if(m_width == 0 || m_height == 0)
			{
				m_width = vSize.x;
				m_height = vSize.y;
			}

            // adjust sourc rect if it is set to a null area
	        if (m_rSrcRect.width - m_rSrcRect.x == 0)
		        m_rSrcRect.width = vSize.x;
	        if (m_rSrcRect.height - m_rSrcRect.y == 0)
		        m_rSrcRect.height = vSize.y;
        }

        void BaseSpriteBatch::SetPosition(int x, int y)
        {
	        m_vPosition.x = (float)x;
	        m_vPosition.y = (float)y;
        }

        void BaseSpriteBatch::SetZ(float z)
        {
	        m_vPosition.z = z;
        }

        void BaseSpriteBatch::SetSize(int width, int height)
        {
	        m_width = width; 
	        m_height = height;
        }

        void BaseSpriteBatch::SetSrcRect(int x, int y, int width, int height)
        {
	        m_rSrcRect.Set(x, y, x+width, y+height);
        }

        void BaseSpriteBatch::SetSrcRect(const math::Rect& rSrcRect)
        {
	        m_rSrcRect = rSrcRect;
            // adjust source rects parameters to fit expected rect format for dx9 sprite renderer
	        m_rSrcRect.width += m_rSrcRect.x;
	        m_rSrcRect.height += m_rSrcRect.y;
        }

		void BaseSpriteBatch::SetEffect(const IEffect& effect)
		{
			m_pEffect = &effect;
		}

		void BaseSpriteBatch::SetFont(const std::wstring& stName, unsigned int size)
		{
			m_pActiveFont = m_pFonts->Get(stName + conv::ToString(size));
			if(!m_pActiveFont)
				m_pActiveFont = &m_pLoader->Load(stName, size);
		}

		void BaseSpriteBatch::Begin(const render::IStage& stage, Sorting sort)
		{
			m_sort = sort;
			m_pStage = &stage;
		}

        void BaseSpriteBatch::Draw(void)
        {
			Draw(gfx::Color(255, 255, 255, 255));
        }

		void BaseSpriteBatch::Draw(const gfx::Color& color)
		{
			if(!m_pStage)
				return;

			const int width = max(1, m_rSrcRect.width - m_rSrcRect.x);
			math::Vector2f vScale(1.0f, 1.0f);
			if(m_width != width)
			{
				vScale.x *= m_width;
				vScale.x /= width;
			}

			const int height = max(1, m_rSrcRect.height - m_rSrcRect.y);
			if(m_height != height)
			{
				vScale.y *= m_height;
				vScale.y /= height;
			}

			m_vCommands.emplace_back(new SpriteCommand(m_vPosition, vScale, m_rSrcRect, m_pTexture, color));
		}

		void BaseSpriteBatch::Draw(const std::wstring& stText, unsigned int flags, const Color& color)
		{
			m_vCommands.emplace_back(new SpriteCommand(math::Rect((int)m_vPosition.x, (int)m_vPosition.y, m_width, m_height), m_vPosition.z, stText, *m_pActiveFont, flags, color));
		}

		void BaseSpriteBatch::Submit(void)
		{
			if(m_vCommands.empty())
				return;

			if(!m_pStage || !m_pEffect)
				return;

			// sort & execute
			
			switch(m_sort)
			{
			case Sorting::TEXTURE_TO_BACK:
				std::stable_sort(m_vCommands.begin(), m_vCommands.end(), TextureToBackSorter());
				break;
			case Sorting::TEXTURE_TO_FRONT:
				std::stable_sort(m_vCommands.begin(), m_vCommands.end(), TextureToFrontSorter());
				break;
			case Sorting::TEXTURE:
				std::stable_sort(m_vCommands.begin(), m_vCommands.end(), TextureSorter());
				break;
			case Sorting::FRONT_TO_BACK:
				std::stable_sort(m_vCommands.begin(), m_vCommands.end(), FrontToBackSorter());
				break;
			case Sorting::BACK_TO_FRONT:
				std::stable_sort(m_vCommands.begin(), m_vCommands.end(), BackToFrontSorter());
				break;
			}

			OnPrepareExecute();

			if(m_sort == Sorting::REVERSE)
				ProcessReverse();
			else
				Process();

			OnFinishExecute();

			for(auto pCommand : m_vCommands)
			{
				delete pCommand;
			}
			m_vCommands.clear();
		}

		const IGeometry& BaseSpriteBatch::GetGeometry(void) const
		{
			return *m_pGeometry;
		}

		void BaseSpriteBatch::Process(void)
		{
			const ITexture* pTexture = nullptr;
			for(auto& command : m_vCommands)
			{
				ProcessCommand(&pTexture, *command);
			}
			ExecuteDraw(pTexture);
		}

		void BaseSpriteBatch::ProcessReverse(void)
		{
			const ITexture* pTexture = nullptr;
			for(auto command = m_vCommands.rbegin(); command != m_vCommands.rend(); command++)
			{
				ProcessCommand(&pTexture, **command);
			}
			ExecuteDraw(pTexture);
		}

		void BaseSpriteBatch::ProcessCommand(const ITexture** ppTexture, const SpriteCommand& command)
		{
			static const math::Vector3 vOrigin(0.0f, 0.0f, 0.0f);
			if(m_numBatches != 0 && command.pTexture != *ppTexture)
				ExecuteDraw(*ppTexture);

			*ppTexture = command.pTexture;

			size_t id = 0;
			if(command.pFont)
			{
				id = command.pFont->DrawString(command.rSrc, command.vPos.z, command.stText, command.flags, command.color);

				m_numBatches += command.stText.size();
			}
			else
			{
				if(*ppTexture)
					id = OnDrawSprite((*ppTexture)->GetSize(), command.vPos, vOrigin, command.rSrc.GetWinRect(), command.vScale, 0.0f, command.color);
				else
					id = OnDrawSprite(command.vPos, vOrigin, command.vScale, 0.0f, command.color);

				m_numBatches++;
			}

			if(m_currentBatch == -1)
				m_currentBatch = id;
		}

		void BaseSpriteBatch::End(void)
		{
			Submit();
			m_pStage = nullptr;
		}

		void BaseSpriteBatch::ExecuteDraw(const ITexture* pTexture)
		{
			if(!m_numBatches)
				return;

			if(pTexture)
				pTexture->Bind(m_states, ITexture::BindTarget::PIXEL, 0);
			else
				RenderStateHandler::UnbindTexture(m_states, TextureBindTarget::PIXEL, 0);

			m_pEffect->BindEffect(m_states);
			const render::StateGroup* groups[2] = { &m_states, &m_pEffect->GetState() };
			render::Instance instance(m_sortKey, groups, 2);

#ifdef USE_GEOMETRY_SHADER
			OnSubmitInstance(m_numBatches, m_currentBatch, instance);
#else
			OnSubmitInstance(m_numBatches * 6, m_currentBatch * 6, instance);
#endif
			m_pStage->SubmitTempInstance(std::move(instance));

			m_numBatches = 0;
			m_currentBatch = -1;
		}

	}
}

#undef USE_GEOMETRY_SHADER