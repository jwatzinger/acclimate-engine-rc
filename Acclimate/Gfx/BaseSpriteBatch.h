#pragma once
#include <vector>
#include "ISpriteBatch.h"
#include "Fonts.h"
#include "Color.h"
#include "IFont.h"
#include "..\Math\Rect.h"
#include "..\Math\Vector3.h"
#include "..\Math\Vector2f.h"
#include "..\Render\StateGroup.h"
#include "..\Render\Key.h"

namespace acl
{
	namespace render
	{
		struct Instance;
	}

	namespace gfx
	{

		class IText;
		class IGeometry;
		class IGeometryCreator;
		class IFontLoader;

		class BaseSpriteBatch :
			public ISpriteBatch
		{
			struct SpriteCommand
			{
				// TODO: use color
				SpriteCommand(const math::Vector3& vPos, const math::Vector2f& vScale, const math::Rect& rSrc, const ITexture* pTexture, const Color& color) : vPos(vPos), vScale(vScale),
					rSrc(rSrc), pTexture(pTexture), pFont(nullptr), color(color)
				{
				}

				SpriteCommand(const math::Rect& rDest, float z, const std::wstring& stText, const IFont& font, unsigned int flags, const Color& color) :
					rSrc(rDest), vPos(0.0f, 0.0f, z), stText(stText), pFont(&font), pTexture(&font.GetTexture()), flags(flags),
					color(color)
				{
				}

				SpriteCommand(SpriteCommand&& command) : rSrc(command.rSrc), vPos(command.vPos), vScale(command.vScale), pTexture(command.pTexture), pFont(command.pFont),
					stText(std::move(command.stText)), flags(command.flags), color(command.color)
				{
				}

				math::Rect rSrc;
				math::Vector3 vPos;
				math::Vector2f vScale;
				const ITexture* pTexture;
				const IFont* pFont;
				std::wstring stText;
				unsigned int flags;
				Color color;
			};

			typedef std::vector<SpriteCommand*> CommandVector;

			struct TextureToBackSorter
			{
				inline bool operator()(const SpriteCommand* left, const SpriteCommand* right)
				{
					return (left->vPos.z > right->vPos.z) ||
						((left->vPos.z == right->vPos.z) && (left->pTexture < right->pTexture));
				}
			};

			struct TextureToFrontSorter
			{
				inline bool operator()(const SpriteCommand* left, const SpriteCommand* right)
				{
					return (left->vPos.z < right->vPos.z) ||
						((left->vPos.z == right->vPos.z) && (left->pTexture < right->pTexture));
				}
			};

			struct FrontToBackSorter
			{
				inline bool operator()(const SpriteCommand* left, const SpriteCommand* right)
				{
					return left->vPos.z > right->vPos.z;
				}
			};

			struct BackToFrontSorter
			{
				inline bool operator()(const SpriteCommand* left, const SpriteCommand* right)
				{
					return left->vPos.z < right->vPos.z;
				}
			};

			struct TextureSorter
			{
				inline bool operator()(const SpriteCommand* left, const SpriteCommand* right)
				{
					return left->pTexture < right->pTexture;
				}
			};

		public:

			BaseSpriteBatch(const Fonts& fonts, const IFontLoader& loader, IGeometryCreator& creator);
			
			void SetPosition(int x, int y) override final;
			void SetZ(float z) override final;
			void SetSize(int width, int heigh) override final;
			void SetTexture(const ITexture* pTexture) override final;
			void SetSrcRect(int x, int y, int width, int height) override final;
			void SetSrcRect(const math::Rect& rSrcRect) override final;
			void SetEffect(const IEffect& effect) override final;
			void SetFont(const std::wstring& stName, unsigned int size) override final;

			void Begin(const render::IStage& stage, Sorting sort) override final;
			void Draw(void) override final;
			void Draw(const Color& color) override final;
			void Draw(const std::wstring& stText, unsigned int flags, const Color& color) override final;
			void Submit(void) override final;
			void End(void) override final;

		protected:
			
			virtual void OnPrepareExecute(void) = 0;
			virtual size_t OnDrawSprite(const math::Vector2& vTextureSize, const math::Vector3& vPosition, const math::Vector3& vOrigin, const RECT& rSrcRect, const math::Vector2f& vScale, float angle, const Color& color) const = 0;
			virtual size_t OnDrawSprite(const math::Vector3& vPosition, const math::Vector3& vOrigin, const math::Vector2f& vScale, float angle, const Color& color) const = 0;
			virtual void OnSubmitInstance(size_t numIndices, size_t startIndex, render::Instance& instance) = 0;
			virtual void OnFinishExecute(void) = 0;

			const IGeometry& GetGeometry(void) const;

			const IEffect* m_pEffect;

			render::StateGroup m_states;
			render::Key64 m_sortKey;

		private:

			void Process(void);
			void ProcessReverse(void);
			void ProcessCommand(const ITexture** ppTexture, const SpriteCommand& command);

			void ExecuteDraw(const ITexture* pTexture);

			IGeometry* m_pGeometry;
			const ITexture* m_pTexture;
			const Fonts* m_pFonts;
			const IFont* m_pActiveFont;
			const IFontLoader* m_pLoader;

			int m_width, m_height, m_currentBatch;
			math::Rect m_rSrcRect;
			math::Vector3 m_vPosition;
			size_t m_numBatches;
			Sorting m_sort;

			const render::IStage* m_pStage;

			CommandVector m_vCommands;
		};

	}
}