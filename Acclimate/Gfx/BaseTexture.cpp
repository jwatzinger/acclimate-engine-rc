#include "BaseTexture.h"
#include "TextureInternalAccessor.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseTexture::BaseTexture(AclTexture& texture, const math::Vector2& vSize, TextureFormats format, DeleteTextureFunc func) :
			m_pTexture(&texture), m_format(format), m_vSize(vSize), m_pInfo(nullptr), m_deleteFunc(func)
		{
		}

		BaseTexture::~BaseTexture(void)
		{
			m_deleteFunc(m_pTexture);

			delete m_pInfo;
		}

		void BaseTexture::Map(MapFlags flags)
		{
			if(m_mapped.pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Texture map call while texture is already mapped.");
			else
			{
				OnMap(flags, m_mapped);
				ACL_ASSERT(m_mapped.pData);
			}
		}

		void BaseTexture::Unmap(void)
		{
			if(!m_mapped.pData)
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Texture unmap call while texture is not mapped.");
			else
			{
				OnUnmap();
				m_mapped.pData = nullptr;
				m_mapped.pitch = 0;
			}
		}

		void BaseTexture::Resize(const math::Vector2& vSize)
		{
			if(m_vSize != vSize)
			{
				OnResize(vSize);
				m_vSize = vSize;
			}
		}

		void BaseTexture::SetLoadInfo(TextureLoadInfo* pInfo)
		{
			if(pInfo != m_pInfo)
			{
				delete m_pInfo;
				m_pInfo = pInfo;
			}
		}

		const math::Vector2& BaseTexture::GetSize(void) const
		{
			return m_vSize;
		}

		void BaseTexture::GetAclTexture(TextureInternalAccessor& accessor) const
		{
			accessor.SetTexture(m_pTexture);
		}

		TextureFormats BaseTexture::GetFormat(void) const
		{
			return m_format;
		}

		void* BaseTexture::GetData(void) const
		{
			return m_mapped.pData;
		}

		unsigned int BaseTexture::GetPitch(void) const
		{
			return m_mapped.pitch;
		}

		const TextureLoadInfo* BaseTexture::GetLoadInfo(void) const
		{
			return m_pInfo;
		}

	}
}