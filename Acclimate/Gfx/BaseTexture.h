#pragma once
#include "ITexture.h"
#include "TextureFormats.h"
#include "..\ApiDef.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{

		struct Mapped
		{
			Mapped(void) : pData(nullptr), pitch(0)
			{
			}

			void* pData;
			unsigned int pitch;
		};

		class BaseTexture :
			public ITexture
		{
			typedef void(*DeleteTextureFunc)(AclTexture* pTexture);
		public:
			BaseTexture(AclTexture& texture, const math::Vector2& vSize, TextureFormats format, DeleteTextureFunc func);
			~BaseTexture(void);

			void Map(MapFlags flags) override final;
			void Unmap(void) override final;
			void Resize(const math::Vector2& vSize) override final;
			
			void SetLoadInfo(TextureLoadInfo* pInfo) override final;

			const math::Vector2& GetSize(void) const override final;
			void GetAclTexture(TextureInternalAccessor& accessor) const override final;
			TextureFormats GetFormat(void) const override final;
			const TextureLoadInfo* GetLoadInfo(void) const override final;

			void* GetData(void) const override final;
			unsigned int GetPitch(void) const override final;

		protected:

			AclTexture* m_pTexture;

		private:

			virtual void OnResize(const math::Vector2& vSize) = 0;
			virtual void OnMap(MapFlags flags, Mapped& mapped) const = 0;
			virtual void OnUnmap(void) const = 0;

			Mapped m_mapped;
			DeleteTextureFunc m_deleteFunc;

			TextureFormats m_format;
			TextureLoadInfo* m_pInfo;

			math::Vector2 m_vSize;
		};

	}
}