#include "BaseTextureLoader.h"
#include "TextureInternalAccessor.h"
#include "..\File\File.h"
#include "..\System\Exception.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseTextureLoader::BaseTextureLoader(Textures& textures) : m_pTextures(&textures)
        {
        }

		ITexture* BaseTextureLoader::Load(const std::wstring& stName, const std::wstring& stFilename, bool bRead, TextureFormats format) const
        {
            ITexture* pTexture = m_pTextures->Get(stName);

            if(!pTexture)
            {
				try
				{
					TextureLoadInfo* pInfo = new TextureLoadInfo(stName, file::FullPath(stFilename), bRead, format);
					pTexture = &OnLoadTexture(stFilename, bRead, format);
					pTexture->SetLoadInfo(pInfo);

					m_pTextures->Add(stName, *pTexture);
				}
				catch(apiException&)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to load texture", stName, "from file", stFilename);
				}
            }
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to reload already existing texture", stName, ". (reloading currently unsupported)");

            return pTexture;
        }

        ITexture* BaseTextureLoader::Create(const std::wstring& stName, const math::Vector2& vSize, TextureFormats format, LoadFlags flags) const
        {
            ITexture* pTexture = m_pTextures->Get(stName);

            if(!pTexture)
            {
				try
				{
					TextureLoadInfo* pInfo = new TextureLoadInfo(stName, vSize, format, flags);
					pTexture = &OnCreateTexture(vSize, format, flags);
					pTexture->SetLoadInfo(pInfo);

					m_pTextures->Add(stName, *pTexture);
				}
				catch(apiException&)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to create texture", stName);
				}
            }
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to recreate already existing texture", stName, ". (recreation currently unsupported)");

            return pTexture;
        }

		ITexture* BaseTextureLoader::Custom(const std::wstring& stName, const math::Vector2& vSize, const void* pData, TextureFormats format, LoadFlags flags) const
		{
			ITexture* pTexture = m_pTextures->Get(stName);

			if(!pTexture)
			{
				try
				{
					pTexture = &OnCreateTexture(vSize, pData, format, flags);

					m_pTextures->Add(stName, *pTexture);
				}
				catch(apiException&)
				{
					sys::log->Out(sys::LogModule::GFX, sys::LogType::ERR, "failed to create texture", stName);
				}
			}
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to recreate already existing texture", stName, ". (recreation currently unsupported)");

			return pTexture;
		}

		void BaseTextureLoader::SaveTexture(const std::wstring& stTexture, const std::wstring& stFile, FileFormat format) const
		{
			if(auto pTexture = m_pTextures->Get(stTexture))
			{
				auto& texture = getTexture(*pTexture);
				if(!OnSaveTexture(texture, stFile, format))
					sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Failed to save texture", stTexture, "to file", stFile, ".");
			}
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Failed to save texture", stTexture, "to file", stFile, ". Texture does not exist.");
		}

	}
}