#pragma once

#include "ITextureLoader.h"
#include "Textures.h"
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class BaseTextureLoader :
			public ITextureLoader
		{
		public:
			BaseTextureLoader(Textures& textures);

			ITexture* Load(const std::wstring& stName, const std::wstring& stFilename, bool bRead, TextureFormats format = TextureFormats::UNKNOWN) const override final;
            ITexture* Create(const std::wstring& stName, const math::Vector2& vSize, TextureFormats format, LoadFlags flags) const override final;
			ITexture* Custom(const std::wstring& stName, const math::Vector2& vSize, const void* pData, TextureFormats format, LoadFlags flags) const override final;

			void SaveTexture(const std::wstring& stTexture, const std::wstring& stFile, FileFormat format) const override final;

		private:

			virtual ITexture& OnLoadTexture(const std::wstring& stFilename, bool bRead, TextureFormats format) const = 0;
			virtual ITexture& OnCreateTexture(const math::Vector2& vSize, TextureFormats format, LoadFlags flags) const = 0;
			virtual ITexture& OnCreateTexture(const math::Vector2& vSize, const void* pData, TextureFormats format, LoadFlags flags) const = 0;
			virtual bool OnSaveTexture(AclTexture& texture, const std::wstring& stFilename, FileFormat format) const = 0;
			
			Textures* m_pTextures;
		};

	}
}