#include "BaseZBuffer.h"
#include "ZbufferInternalAccessor.h"

namespace acl
{
	namespace gfx
	{

		BaseZBuffer::BaseZBuffer(AclDepth& depthBuffer, const math::Vector2& vSize, DeleteZBufferFunc deleteFunc) : m_pDepthBuffer(&depthBuffer),
			m_deleteFunc(deleteFunc), m_vSize(vSize)
		{
		}

		BaseZBuffer::~BaseZBuffer(void)
		{
			m_deleteFunc(m_pDepthBuffer);
		}

		void BaseZBuffer::GetBuffer(ZBufferInternalAccessor& accessor) const
		{
			accessor.SetZBuffer(m_pDepthBuffer);
		}

		const math::Vector2& BaseZBuffer::GetSize(void) const
		{
			return m_vSize;
		}


		void BaseZBuffer::Resize(const math::Vector2& vSize)
		{
			if(m_vSize != vSize)
			{
				OnResize(vSize);
				m_vSize = vSize;
			}
		}

	}
}