#pragma once
#include "IZBuffer.h"
#include "..\ApiDef.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{

		class BaseZBuffer :
			public IZBuffer
		{
			typedef void(*DeleteZBufferFunc)(AclDepth* pDepth);
		public:
			BaseZBuffer(AclDepth& depthBuffer, const math::Vector2& vSize, DeleteZBufferFunc deleteFunc);
			~BaseZBuffer(void);

			void GetBuffer(ZBufferInternalAccessor& accessor) const override final;
			const math::Vector2& GetSize(void) const override final;

			void Resize(const math::Vector2& vSize) override final;

		protected:

			AclDepth* m_pDepthBuffer;

		private:

			virtual void OnResize(const math::Vector2& vSize) = 0;
			DeleteZBufferFunc m_deleteFunc;

			math::Vector2 m_vSize;
		};

	}
}