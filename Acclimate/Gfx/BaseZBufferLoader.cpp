#include "BaseZBufferLoader.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		BaseZBufferLoader::BaseZBufferLoader(ZBuffers& zBuffers) : 
			m_zBuffers(zBuffers)
		{
		}

		IZBuffer* BaseZBufferLoader::Create(const std::wstring& stName, const math::Vector2& vSize) const
		{
			if(!m_zBuffers.Has(stName))
			{
				auto& zBuffer = OnCreateZBuffer(vSize);
				m_zBuffers.Add(stName, zBuffer);
				return &zBuffer;
			}
			else
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Zbuffer with name", stName, "already exists (recreation currently unsupported).");

				return m_zBuffers.Get(stName);
			}	
		}

	}
}