#pragma once
#include "IZBufferLoader.h"
#include "ZBuffers.h"

namespace acl
{
	namespace gfx
	{

		class BaseZBufferLoader :
			public IZBufferLoader
		{
		public:
			BaseZBufferLoader(ZBuffers& zBuffers);

			IZBuffer* Create(const std::wstring& stName, const math::Vector2& vSize) const override final;

		private:

			virtual IZBuffer& OnCreateZBuffer(const math::Vector2& vSize) const = 0;
			
			ZBuffers& m_zBuffers;
		};

	}
}