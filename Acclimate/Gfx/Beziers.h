#pragma once
#include <vector>
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		template<typename Vector, unsigned int NumPoints>
		class Beziers
		{
		public:

			typedef std::vector<Vector> PointVector;

			Beziers(float density) : m_density(density)
			{
			}

			Beziers(const Vector points[NumPoints], float density) : m_density(density)
			{
				static_assert(NumPoints >= 2, "Bezier-Curve must at least have two control points.");

				memcpy(m_points, points, sizeof(Vector)* NumPoints);

				Recalculate();
			}

			void SetDensity(float density)
			{
				// TODO: exception would probably be better suited
				ACL_ASSERT(density > 0.0f);
				ACL_ASSERT(density <= 1.0f);

				if(m_density != density)
				{
					m_density = density;
				}
			}

			void SetPoints(const Vector points[NumPoints])
			{
				bool different = false;
				for(unsigned int i = 0; i < NumPoints; i++)
				{
					if(points[i] != m_points[i])
					{
						different = true;
						break;
					}
				}

				if(different)
				{
					memcpy(m_points, points, sizeof(Vector)* NumPoints);
					Recalculate();
				}
			}

			const PointVector& GetPoints(void) const
			{
				return m_vPoints;
			}

		private:

			void Recalculate(void)
			{
				m_vPoints.clear();

				// calculate number of generated points & reserve appropriate memory in vector
				const size_t numOutputPoints = (size_t)(1.0f / m_density) + 2;
				m_vPoints.reserve(numOutputPoints);

				float i;
				for(i = 0.0f; i <= 1.0f; i+= m_density)
				{
					m_vPoints.push_back(GetBezierPoint(i));
				}

				if(i != 1.0f)
					m_vPoints.push_back(GetBezierPoint(1.0f));

				ACL_ASSERT(*m_vPoints.begin() == m_points[0]);
			}

			Vector GetBezierPoint(float t) const
			{
				Vector tmp[NumPoints];
				memcpy(tmp, m_points, NumPoints * sizeof(Vector));

				for(int i = NumPoints - 1; i > 0; i--)
				{
					for(int k = 0; k < i; k++)
					{
						tmp[k] = tmp[k] + (tmp[k + 1] - tmp[k]) * t;
					}
				}
				return tmp[0];
			}

			float m_density;
			Vector m_points[NumPoints];

			PointVector m_vPoints;
		};

	}
}