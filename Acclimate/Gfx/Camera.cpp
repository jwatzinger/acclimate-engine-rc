#include "Camera.h"

namespace acl
{
    namespace gfx
    {

		Camera::Camera(const math::Matrix& mViewProjection) : m_mViewProjection(mViewProjection), 
			m_bOrtho(false), m_near(0.0f), m_far(1000.0f), m_fovy(1.0f)
		{
			m_frustum.CalculateFrustum(mViewProjection);
		}

		Camera::Camera(const math::Vector2& vScreenSize, const math::Vector3& vPos): m_vPosition(vPos), m_vLookAt(0.0f, 0.0f, 0.0f), 
			m_vUp(0.0f, 1.0f, 0.0f), m_vScreenSize(vScreenSize), m_near(0.5f), m_far(1000.0f), m_bOrtho(true)
        {
	        CalculateDirection();
	        CalculateMatrices();
        }

        Camera::Camera(const math::Vector2& vScreenSize, const math::Vector3& vPos, float fovY): m_vPosition(vPos), m_vLookAt(0.0f, 0.0f, 0.0f), 
			m_vUp(0.0f, 1.0f, 0.0f), m_vScreenSize(vScreenSize), m_near(0.5f), m_far(1000.0f), m_fovy(fovY), m_bOrtho(false)
        {
	        CalculateDirection();
	        CalculateMatrices();
        }

        void Camera::SetPosition(float x, float y, float z)
        {
            m_vPosition.x = x;
            m_vPosition.y = y;
            m_vPosition.z = z;
            CalculateDirection();
            CalculateMatrices();
        }

        void Camera::SetPosition(const math::Vector3& vPosition)
        {
	        m_vPosition = vPosition;
	        CalculateDirection();
	        CalculateMatrices();
        }

        void Camera::SetUp(const math::Vector3& vUp)
        {
	        m_vUp = vUp;
	        CalculateMatrices();
        }

        void Camera::SetLookAt(float x, float y, float z)
        {
            m_vLookAt.x = x;
            m_vLookAt.y = y;
            m_vLookAt.z = z;
            CalculateDirection();
            CalculateMatrices();
        }

        void Camera::SetLookAt(const math::Vector3& vLookAt)
        {
            m_vLookAt = vLookAt;
            CalculateDirection();
            CalculateMatrices();
        }

        void Camera::SetClipPlanes(float nearp, float farp)
        {
            m_near = nearp;
            m_far = farp;
			CalculateMatrices();
        }

		void Camera::SetScreenSize(const math::Vector2& vScreenSize)
		{
			m_vScreenSize = vScreenSize;
			CalculateMatrices();
		}

		void Camera::SetOrtho(void)
		{
			m_bOrtho = true;
			CalculateMatrices();
		}

		void Camera::SetPerspective(float fovY)
		{
			m_fovy = fovY;
			m_bOrtho = false;
			CalculateMatrices();
		}

		float Camera::GetNear(void) const
		{
			return m_near;
		}

		float Camera::GetFar(void) const
		{
			return m_far;
		}

		float Camera::GetDistance(void) const
        {
	        return m_vDirection.length();
        }

        math::Vector3 Camera::GetDirection(void) const
        {
	        return m_vDirection.normal();
        }

        const math::Vector3& Camera::GetUp(void) const
        {
	        return m_vUp;
        }

        const math::Vector3& Camera::GetPosition(void) const
        {
	        return m_vPosition;
        }

        const math::Vector3& Camera::GetLookAt(void) const
        {
	        return m_vLookAt;
        }

        const math::Matrix& Camera::GetViewMatrix(void) const
        {
	        return m_mView;
        }

        const math::Matrix& Camera::GetProjectionMatrix(void) const
        {
	        return m_mProjection;
        }

        const math::Matrix& Camera::GetViewProjectionMatrix(void) const
        {
	        return m_mViewProjection;
        }

        const math::Frustum& Camera::GetFrustum(void) const
        {
	        return m_frustum;
        }

		const math::Vector2& Camera::GetScreenSize(void) const
		{
			return m_vScreenSize;
		}

        void Camera::CalculateMatrices(void)
        {
	        m_mView = math::MatLookAt(m_vPosition, m_vLookAt, m_vUp);

			if(m_bOrtho)
				m_mProjection = math::MatOrthoLH((float)m_vScreenSize.x, (float)m_vScreenSize.y, m_near, m_far);
			else
				m_mProjection = math::MatPerspFovLH(m_fovy, m_vScreenSize.x / (float)m_vScreenSize.y, m_near, m_far);

	        m_mViewProjection = m_mView * m_mProjection;
	        m_frustum.CalculateFrustum(m_mViewProjection);
        }

        void Camera::CalculateDirection(void)
        {
	        math::Vector3 vDir(m_vLookAt);
	        vDir -= m_vPosition;
	
	        if(vDir.length() == 0.0f)
		        return;

	        m_vDirection = vDir;
        }

    }
}