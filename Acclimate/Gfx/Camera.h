#pragma once
#include "..\Core\Dll.h"
#include "..\Math\Frustum.h"
#include "..\Math\Vector.h"
#include "..\Math\Matrix.h"

namespace acl
{
    namespace gfx
    {

        class ACCLIMATE_API Camera
        {
        public:
			Camera(const math::Matrix& mViewProjection);
			Camera(const math::Vector2& vScreenSize, const math::Vector3& vPos);
	        Camera(const math::Vector2& vScreenSize, const math::Vector3& vPos, float fovY);

            void SetPosition(float x, float y, float);
	        void SetPosition(const math::Vector3& vPosition);
	        void SetUp(const math::Vector3& vUp);
            void SetLookAt(float x, float y, float z);
            void SetLookAt(const math::Vector3& vLookAt);
            void SetClipPlanes(float nearp, float farp);
			void SetScreenSize(const math::Vector2& vScreenSize);
			void SetOrtho(void);
			void SetPerspective(float fovY);

			float GetNear(void) const;
			float GetFar(void) const;
	        float GetDistance(void) const;
	        math::Vector3 GetDirection(void) const;
	        const math::Vector3& GetPosition(void) const;
	        const math::Vector3& GetLookAt(void) const;
	        const math::Vector3& GetUp(void) const;
	        const math::Matrix& GetViewMatrix(void) const;
	        const math::Matrix& GetProjectionMatrix(void) const;
	        const math::Matrix& GetViewProjectionMatrix(void) const;
	        const math::Frustum& GetFrustum(void) const;
			const math::Vector2& GetScreenSize(void) const;

        private:
#pragma warning( disable: 4251 )
			bool m_bOrtho;

	        void CalculateMatrices(void);
	        void CalculateDirection(void);

            float m_near, m_far, m_fovy;

	        math::Vector2 m_vScreenSize;

	        math::Matrix m_mViewProjection;
	        math::Matrix m_mProjection;
	        math::Matrix m_mView;

	        math::Vector3 m_vLookAt;
	        math::Vector3 m_vPosition;
	        math::Vector3 m_vUp;
	        math::Vector3 m_vDirection;

	        math::Frustum m_frustum;
#pragma warning( default: 4251 )
        };

    }
}

