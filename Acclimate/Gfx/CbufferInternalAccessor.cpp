#include "CbufferInternalAccessor.h"
#include "ICbuffer.h"
#include "ICbufferLoader.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		CbufferInternalAccessor::CbufferInternalAccessor(void) : m_pCbuffer(nullptr)
		{
		}

		CbufferInternalAccessor::CbufferInternalAccessor(AclCbuffer& cBuffer) : m_pCbuffer(&cBuffer)
		{
		}

		void CbufferInternalAccessor::SetCbuffer(AclCbuffer* pCbuffer)
		{
			m_pCbuffer = pCbuffer;
		}

		AclCbuffer* CbufferInternalAccessor::GetCbuffer(void) const
		{
			return m_pCbuffer;
		}

		const AclCbuffer* getCbuffer(const ICbuffer& cBuffer)
		{
			CbufferInternalAccessor accessor;
			cBuffer.GetBuffer(accessor);
			return accessor.GetCbuffer();
		}

		const AclCbuffer* getCbuffer(const ICbuffer* pCbuffer)
		{
			if(pCbuffer)
				return getCbuffer(*pCbuffer);
			else
				return nullptr;
		}

		bool linkCbuffer(ICbuffer& cBuffer, AclCbuffer& aclCbuffer)
		{
			const CbufferInternalAccessor accessor(aclCbuffer);
			return cBuffer.Link(accessor);
		}

		bool linkCbuffer(ICbuffer& cBuffer, AclCbuffer* pAclCbuffer)
		{
			if(!pAclCbuffer)
				return false;
			else
			{
				const CbufferInternalAccessor accessor(*pAclCbuffer);
				return cBuffer.Link(accessor);
			}
		}

		ICbuffer& createCbuffer(const ICbufferLoader& loader, AclCbuffer& aclCbuffer)
		{
			const CbufferInternalAccessor accessor(aclCbuffer);

			return loader.Create(accessor);
		}

		ICbuffer* createCbuffer(const ICbufferLoader& loader, AclCbuffer* pAclCbuffer)
		{
			if(pAclCbuffer)
			{
				const CbufferInternalAccessor accessor(*pAclCbuffer);

				return &loader.Create(accessor);
			}
			else
				return nullptr;
		}

	}
}

