#pragma once
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class ICbuffer;

		class CbufferInternalAccessor
		{
		public:

			CbufferInternalAccessor(void);
			CbufferInternalAccessor(AclCbuffer& cBuffer);

			void SetCbuffer(AclCbuffer* pCbuffer);
			AclCbuffer* GetCbuffer(void) const;

		private:

			AclCbuffer* m_pCbuffer;
		};

		const AclCbuffer* getCbuffer(const ICbuffer& Cbuffer);
		const AclCbuffer* getCbuffer(const ICbuffer* pCbuffer);

		bool linkCbuffer(ICbuffer& cBuffer, AclCbuffer& aclCbuffer);
		bool linkCbuffer(ICbuffer& cBuffer, AclCbuffer* pAclCbuffer);

		class ICbufferLoader;

		ICbuffer& createCbuffer(const ICbufferLoader& loader, AclCbuffer& aclCbuffer);
		ICbuffer* createCbuffer(const ICbufferLoader& loader, AclCbuffer* pAclCbuffer);

	}
}


