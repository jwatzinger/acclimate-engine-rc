#pragma once

namespace acl
{
	namespace gfx
	{

		struct Color3
		{
			Color3(void) : r(0), g(0), b(0)
			{
			}

			Color3(unsigned char r, unsigned char g, unsigned char b) : r(r), g(g), b(b) { };

			bool operator==(const Color3& color) const
			{
				return r == color.r && g == color.g && b == color.b;
			}

			bool operator!=(const Color3& color) const
			{
				return r != color.r || g != color.g || b != color.b;
			}

			unsigned char r, g, b;
		};

		struct Color
		{
			Color(void): r(0), g(0), b(0), a(0) 
			{
			}

			Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a = 255) : r(r), g(g), b(b), a(a) { };

			bool operator==(const Color& color) const
			{
				return r == color.r && g == color.g && b == color.b && a == color.a;
			}

			bool operator!=(const Color& color) const
			{
				return r != color.r || g != color.g || b != color.b || a != color.a;
			}

			unsigned char r, g, b, a;
		};

		struct FColor
		{
			FColor(void): r(0.0f), g(0.0f), b(0.0f), a(1.0f)
			{
			};

			FColor(float r, float g, float b, float a = 1.0f): r(r), g(g), b(b), a(a) { };

			FColor operator+(const FColor& color) const
			{
				return FColor(r + color.r, g + color.g, b + color.b, a + color.a);
			}

			FColor operator-(const FColor& color) const
			{
				return FColor(r - color.r, g - color.g, b - color.b, a - color.a);
			}

			FColor operator*(float value) const
			{
				return FColor(r * value, g * value, b * value, a * value);
			}

			FColor operator/(float value) const
			{
				return FColor(r / value, g / value, b / value, a / value);
			}

			bool operator==(const FColor& color) const
			{
				return r == color.r && g == color.g && b == color.b && a == color.a;
			}

			bool operator!=(const FColor& color) const
			{
				return r != color.r || g != color.g || b != color.b || a != color.a;
			}

			float r, g, b, a;
		};

	}
}