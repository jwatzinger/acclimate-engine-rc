#pragma once
#include "..\Core\Dll.h"
#include "Textures.h"
#include "Effects.h"
#include "Meshes.h"
#include "Materials.h"
#include "Fonts.h"
#include "Models.h"
#include "ZBuffers.h"
#include "Animations.h"

namespace acl
{
    namespace gfx
    {

        struct ACCLIMATE_API ResourceContext
        {
            ResourceContext(Textures& textures, Effects& effects, Meshes& meshes, Materials& materials, Fonts& fonts, Models& models, ZBuffers& zbuffers, Animations& animations): 
				textures(textures), effects(effects), meshes(meshes), materials(materials), fonts(fonts), models(models), zbuffers(zbuffers), animations(animations)
			{
			}

            Textures& textures;
            Effects& effects;
            Meshes& meshes;
            Materials& materials;
            Fonts& fonts;
			Models& models;
            ZBuffers& zbuffers;
			Animations& animations;
        };

		class ITextureLoader;
		class IEffectLoader;
		class IMeshLoader;
		class IMaterialLoader;
		class IFontLoader;
		class IModelLoader;
		class IZBufferLoader;
		class IResourceLoader;
		class IGeometryCreator;
		class AnimationLoader;

        struct ACCLIMATE_API LoadContext
        {
			LoadContext(const ITextureLoader& textures, const IEffectLoader& effects, const IMeshLoader& meshes, const IMaterialLoader& materials, const IFontLoader& fonts, const IModelLoader& models, const IZBufferLoader& zbuffers, const IGeometryCreator& creator, const AnimationLoader& animations) :
				textures(textures), effects(effects), meshes(meshes), materials(materials), fonts(fonts), models(models), zbuffers(zbuffers), animations(animations), pLoader(nullptr)
			{
			}

            const ITextureLoader& textures;
            const IEffectLoader& effects;
            const IMeshLoader& meshes;
            const IMaterialLoader& materials;
            const IFontLoader& fonts;
			const IModelLoader& models;
            const IZBufferLoader& zbuffers;
			const AnimationLoader& animations;
			IResourceLoader* pLoader;
        };

		class ISprite;
		class ISpriteBatch;
		class IText;
		class ILine;
		class Screen;
		class FullscreenEffect;

		struct ACCLIMATE_API Context
		{
			Context(ResourceContext& resources, LoadContext& load, ISprite& sprite, IText& text, ISpriteBatch& spriteBatch, Screen& screen, FullscreenEffect& effect, ILine& line) :
				resources(resources), load(load), sprite(sprite), text(text), spriteBatch(spriteBatch), screen(screen), effect(effect), line(line)
			{
			}

			ResourceContext& resources;
			LoadContext& load;
			ISprite& sprite;
			IText& text;
			ILine& line;
			ISpriteBatch& spriteBatch;
			Screen& screen;
			FullscreenEffect& effect;
		};

    }
}