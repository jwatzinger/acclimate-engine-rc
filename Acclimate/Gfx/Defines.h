#pragma once

namespace acl
{
	namespace gfx
	{
		// mesh
		const unsigned int MAX_VERTEX_BUFFERS_MESH = 2;

		// usable textures
		const unsigned int MAX_PIXEL_TEXTURES = 4;
		const unsigned int MAX_VERTEX_TEXTURES = 1;

		// model states
		const unsigned int MAX_MODEL_PASSES = 8;
		const unsigned int NUM_MODEL_STATES = 3;

		// buffer settings
		const unsigned int MAX_VERTEX_BUFFERS = 1;
		const unsigned int INSTANCE_BUFFER_POS = MAX_VERTEX_BUFFERS;

		// cbuffers
		const unsigned int MAX_CBUFFERS = 2;
		const unsigned int CBUFFER_INSTANCE_ID = 0;
		const unsigned int CBUFFER_STAGE_ID = 1;

	}
}