#include "EffectFile.h"
#include "..\System\Assert.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace gfx
	{

		Permutation::Permutation(const std::string& stName, unsigned int value) : stName(stName), value(value)
		{
		}

		EffectFile::EffectFile(const std::string& stSource, const std::string& stShader, const std::wstring& stPath, ShaderTypes type) : m_stSource(stSource), m_stName(stPath),
			m_stData(stShader), m_type(type)
		{
		}

		EffectFile::EffectFile(const EffectFile& file) : m_stSource(file.m_stSource), m_stName(file.m_stName), m_vPermutations(file.m_vPermutations),
			m_stData(file.m_stData), m_vLinkage(file.m_vLinkage), m_type(file.m_type)
		{
		}

		EffectFile::EffectFile(EffectFile&& file) : m_stSource(std::move(file.m_stSource)), m_stName(std::move(file.m_stName)), m_vPermutations(std::move(file.m_vPermutations)),
			m_stData(std::move(file.m_stData)), m_vLinkage(std::move(file.m_vLinkage)), m_type(file.m_type)
		{
		}

		size_t EffectFile::GetMaxPermutations(void) const
		{
			return (size_t)pow(2, m_vPermutations.size());
		}

		const EffectFile::PermutationVector& EffectFile::GetPermutations(void) const
		{
			return m_vPermutations;
		}

		EffectFile::PermutationVector EffectFile::GetPermutations(size_t permutation) const
		{
			PermutationVector vPermutations;
			IdMap mIds;

			if(permutation == 0)
			{
				CheckRequired(mIds, vPermutations);
				return vPermutations;
			}
				
			const double maxPermutation = log(permutation)/log(2);
			for(unsigned int i = 0; i <= maxPermutation; i++)
			{
				const int shift = 1 << i;
				if(permutation & shift)
				{
					const auto& perm = m_vPermutations[i];
					if(!mIds.count(perm.stName))
					{
						mIds.emplace(perm.stName, vPermutations.size());
						vPermutations.push_back(perm);
					}
					else
						vPermutations[mIds[perm.stName]].value += perm.value;
				}
			}

			CheckRequired(mIds, vPermutations);

			return vPermutations;
		}

		const EffectFile::LinkageVector& EffectFile::GetLinkage(void) const
		{
			return m_vLinkage;
		}

		const char* EffectFile::GetData(void) const
		{
			return m_stData.c_str();
		}

		size_t EffectFile::GetDataLength(void) const
		{
			return m_stData.size();
		}

		ShaderTypes EffectFile::GetShaderTypes(void) const
		{
			return m_type;
		}

		const std::string& EffectFile::GetSource(void) const
		{
			return m_stSource;
		}

		void EffectFile::AddPermutation(const std::string& stName, unsigned int numBits)
		{
			for(unsigned int i = 0; i < numBits; i++)
			{
				m_vPermutations.emplace_back(stName, (unsigned int)pow(2, i));
			}

			if(numBits != 1)
				m_vRequired.push_back(stName);
		}

		void EffectFile::AttachExtention(const std::string& stExtention)
		{
			const size_t extentionPos = m_stData.find("// extention_entry\n");

			ACL_ASSERT(extentionPos != std::string::npos);

			m_stData.insert(extentionPos + 19, stExtention);
		}

		void EffectFile::AddExtention(const std::string& stName)
		{
			m_vLinkage.emplace_back(stName, 0);
		}

		const std::wstring& EffectFile::GetFilename(void) const
		{
			return m_stName;
		}

		void EffectFile::CheckRequired(const IdMap& mIds, PermutationVector& vOut) const
		{
			for(auto& required : m_vRequired)
			{
				if(!mIds.count(required))
					vOut.emplace_back(required, 1);
			}
		}

	}
}
