#pragma once
#include <string>
#include <vector>
#include <unordered_map>
#include "Linkage.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		struct Permutation
		{
			Permutation(const std::string& stName, unsigned int value);

			std::string stName;
			unsigned int value;
		};

		enum class ShaderTypes
		{
			VERTEX_PIXEL,
			VERTEX_PIXEL_GEOMETRY,
		};

		class ACCLIMATE_API EffectFile
		{
			typedef std::vector<std::string> RequiredPermutationVector;
			typedef std::unordered_map<std::string, unsigned int> IdMap;
		public:

			typedef std::vector<Permutation> PermutationVector;
			typedef std::vector<Linkage> LinkageVector;

			EffectFile(const std::string& stSource, const std::string& stShader, const std::wstring& stPath, ShaderTypes type);
			EffectFile(const EffectFile& file);
			EffectFile(EffectFile&& file);

			size_t GetMaxPermutations(void) const;
			const PermutationVector& GetPermutations(void) const;
			PermutationVector GetPermutations(size_t permutation) const;
			const LinkageVector& GetLinkage(void) const;
			const char* GetData(void) const;
			size_t GetDataLength(void) const;
			const std::wstring& GetFilename(void) const;
			ShaderTypes GetShaderTypes(void) const;
			const std::string& GetSource(void) const;

			void AddPermutation(const std::string& stName, unsigned int numBits);
			void AddExtention(const std::string& stName);
			void AttachExtention(const std::string& stExtention);

		private:
#pragma warning( disable: 4251 )

			void CheckRequired(const IdMap& mIds, PermutationVector& vOut) const;

			ShaderTypes m_type;
			std::string m_stSource, m_stData;
			std::wstring m_stName;

			PermutationVector m_vPermutations;
			RequiredPermutationVector m_vRequired;
			LinkageVector m_vLinkage;
#pragma warning( default: 4251 )
		};

	}
}
