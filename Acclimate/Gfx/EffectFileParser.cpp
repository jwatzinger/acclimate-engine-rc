#include "EffectFileParser.h"
#include <string>
#include <set>
#include <minmax.h>
#include "EffectFile.h"
#include "IEffectLanguageParser.h"
#include "..\ApiDef.h"
#include "..\System\Exception.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gfx
	{

		EffectFileParser::EffectFileParser(const IEffectLanguageParser& parser) : m_pLanguage(&parser)
		{
		}

		void searchAndReplaceScreenShader(size_t pos, std::string& stShader)
		{
			if(stShader.substr(pos, 20) == "vertexShader(SCREEN)")
			{
				stShader.erase(pos + 12, 8);
				stShader.insert(pos + 12, "\n{\nin\n{\nfloat2 vPos;\nfloat2 vTex0;\n}\n\nout\n{\nfloat4 vPos;\nfloat2 vTex0;\n}\n\nmain\n{\nout.vPos = float4(in.vPos, 0.5, 1.0);\nout.vTex0 = in.vTex0;\n}\n}\n\n");
			}	
		}

		void ParseOptions(std::stringstream& stream, EffectFile& file);

		EffectFile* EffectFileParser::Parse(const std::wstring& stFilename) const
		{
			std::ifstream stream;
			stream.open(stFilename.c_str(), std::ios::in | std::ios::binary);

			if(!stream.is_open())
				throw fileException();

			stream.seekg(0, std::ios::end);
			const size_t dataLength = (size_t)stream.tellg();
			stream.seekg(0, std::ios::beg);

			char* pData = new char[dataLength];
			stream.read(pData, dataLength);

			std::string stShader(pData, dataLength);
			const std::string stSource(stShader);
			delete[] pData;

			std::string stShaderOut;
			stShaderOut.reserve(stShader.size());

			/******************************************
			* VertexShader
			*******************************************/

			// find vertex shader
			const size_t vertexPos = stShader.find("vertexShader");
			if(vertexPos != std::string::npos)
			{
				// prepare screen quad shader
				searchAndReplaceScreenShader(vertexPos, stShader);

				m_pLanguage->OnVertexBegin(stShaderOut);

				std::stringstream stream(stShader.substr(vertexPos+12));
				std::string stBracket;
				stream >> stBracket;
				if(stBracket == "{")
				{
					std::string stToken;

					while(stream.good())
					{
						stream >> stToken;
						if(stToken == "input")
							ParseInput(stream, stShaderOut, ShaderType::VERTEX);
						else if(stToken == "in")
							ParseInOut(stream, stShaderOut, InOut::I, ShaderType::VERTEX);
						else if(stToken == "out")
							ParseInOut(stream, stShaderOut, InOut::O, ShaderType::VERTEX);
						else if(stToken == "main")
							ParseVertexMain(stream, stShaderOut);
						else if(stToken == "functions")
							ParseFunctions(stream, stShaderOut);
						else if(stToken == "textures")
							ParseTextures(stream, stShaderOut);
						else
							break;
					}
				}

				stShader.erase(vertexPos, (unsigned int)stream.tellg() - vertexPos);
			}
			else
			{
				// error
			}

			/******************************************
			* GeometryShader
			*******************************************/

			bool hasGeometryShader = false;
			// find geometry
			const size_t geometryPos = stShader.find("geometryShader");
			if(geometryPos != std::string::npos)
			{
#ifdef ACL_API_DX9
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Shader", stFilename, "contains a geometry-shader. Geometry-shader is not supported in DX9-mode. Ensure that the implementation reflects this behaviour, or create a seperate shader for DX9.");
#endif
				hasGeometryShader = true;

				m_pLanguage->OnGeometryBegin(stShaderOut);

				std::stringstream stream(stShader.substr(geometryPos + 14));
				std::string stBracket;
				stream >> stBracket;
				if(stBracket == "{")
				{
					std::string stToken;

					while(stream.good())
					{
						stream >> stToken;
						if(stToken == "input")
							ParseInput(stream, stShaderOut, ShaderType::GEOMETRY);
						else if(stToken == "out")
							ParseInOut(stream, stShaderOut, InOut::O, ShaderType::GEOMETRY);
						else if(stToken == "primitives")
							ParseGeometryPrimitive(stream, stShaderOut);
						else if(stToken == "main")
							ParseGeometryMain(stream, stShaderOut);
						else
							break;
					}
				}

				stShader.erase(geometryPos, (unsigned int)stream.tellg() - geometryPos);

			}

			/******************************************
			* PixelShader
			*******************************************/

			const size_t pixelPos = stShader.find("pixelShader");
			if(pixelPos != std::string::npos)
			{
				m_pLanguage->OnPixelBegin(stShaderOut);

				std::stringstream stream(stShader.substr(pixelPos+11));
				std::string stBracket;
				stream >> stBracket;
				if(stBracket == "{")
				{
					std::string stToken;

					while(stream.good())
					{
						stream >> stToken;
						if(stToken == "input")
							ParseInput(stream, stShaderOut, ShaderType::PIXEL);
						else if(stToken == "out")
							ParseInOut(stream, stShaderOut, InOut::O, ShaderType::PIXEL);
						else if(stToken == "main")
							ParsePixelMain(stream, stShaderOut);
						else if(stToken == "textures")
							ParseTextures(stream, stShaderOut);
						else if(stToken == "extentions")
							ParseExtentions(stream, stShaderOut);
						else if(stToken == "functions")
							ParseFunctions(stream, stShaderOut);
						else
							break;
					}

					stShader.erase(pixelPos, (unsigned int)stream.tellg() - pixelPos);
				}
			}
			else
			{
				// error
			}

			ReplaceType(stShaderOut, Type::FLOAT3X3);
			ReplaceType(stShaderOut, Type::FLOAT);
			ReplaceType(stShaderOut, Type::MATRIX);
			ReplaceType(stShaderOut, Type::UINT);
			ReplaceType(stShaderOut, Type::OUT_);
			ReplaceType(stShaderOut, Type::IN_);

			m_pLanguage->OnPostProcess(stShaderOut);

			//sys::log->Out(sys::LogModule::GFX, sys::LogType::INFO, stShaderOut);

			// find settings
			const size_t settingPos = stShader.find("shaderSettings");

			ShaderTypes type = ShaderTypes::VERTEX_PIXEL;
			if(hasGeometryShader)
				type = ShaderTypes::VERTEX_PIXEL_GEOMETRY;

			EffectFile* pFile = new EffectFile(stSource, stShaderOut, stFilename, type);

			if(settingPos != std::string::npos)
			{
				std::stringstream stream(stShader.substr(settingPos + 14));
				std::string stBracket;
				stream >> stBracket;
				if(stBracket == "{")
				{
					std::string stToken;

					while(stream.good())
					{
						stream >> stToken;
						if(stToken == "options")
							ParseOptions(stream, *pFile);
						else
						{
							break;
						}
					}
				}
			}

			return pFile;
		}

		/************************************************************
		* Settings
		************************************************************/

		void ParseOptions(std::stringstream& stream, EffectFile& file)
		{
			std::string stToken;
			stream >> stToken;

			ACL_ASSERT(stToken == "{");

			while(stream.good())
			{
				stream >> stToken;
				if(stToken == "}")
					break;

				ACL_ASSERT(stToken.size() > 1);
				
				unsigned int numBits = 1;
				const size_t bracketPos = stToken.find('(');
				if(bracketPos != std::string::npos)
				{
					const size_t closeBracketPos = stToken.find(')');
					ACL_ASSERT(closeBracketPos != std::string::npos);

					const std::string stNumBits = stToken.substr(bracketPos + 1, closeBracketPos - bracketPos-1);
					numBits = conv::FromString<unsigned int>(stNumBits);

					stToken.erase(bracketPos, closeBracketPos - bracketPos+1);
				}

				const size_t semicolonPos = stToken.find(';');
				ACL_ASSERT(semicolonPos == stToken.size() - 1);

				stToken.erase(semicolonPos);

				file.AddPermutation(stToken, numBits);
			}
		}

		/************************************************************
		* Extention
		************************************************************/

		void EffectFileParser::ParseExtention(const std::wstring& stFilename, EffectFile& file) const
		{
			std::ifstream stream;
			stream.open(stFilename.c_str(), std::ios::in | std::ios::binary);

			if(!stream.is_open())
				throw fileException();

			stream.seekg(0, std::ios::end);
			const size_t dataLength = (size_t)stream.tellg();
			stream.seekg(0, std::ios::beg);

			char* pData = new char[dataLength];
			stream.read(pData, dataLength);
			std::string stExtention(pData, dataLength);
			delete[] pData;

			std::string stExtentionOut;
			stExtentionOut.reserve(stExtention.size());

			// find extention
			const size_t extentionPos = stExtention.find("extention(");
			if(extentionPos != std::string::npos)
			{
				const size_t extentionSize = extentionPos + 10;
				const size_t bracketPos = stExtention.find(')', extentionSize);

				const std::string stExtentionName = stExtention.substr(extentionSize, bracketPos - extentionSize);

				const size_t dotPos = stExtention.find(':', bracketPos);
				ACL_ASSERT(dotPos != std::string::npos);

				const size_t newLinePos = stExtention.find("\r\n", dotPos);
				ACL_ASSERT(newLinePos != std::string::npos);

				const std::string stParentExtention = stExtention.substr(dotPos + 2, newLinePos - dotPos - 2);

				std::stringstream stream(stExtention.substr(newLinePos+2));
				std::string stBracket;
				stream >> stBracket;
				if(stBracket == "{")
				{
					std::string stToken;

					while(stream.good())
					{
						stream >> stToken;
						if(stToken == "main")
						{
							// TODO: Execution order might be a problem here, especially for DX11 and when adding more blocks
							m_pLanguage->OnExtentionBegin(stExtentionOut, stExtentionName, stParentExtention);
							ParseExtentionMain(stream, stExtentionOut);
							m_pLanguage->OnExtentionEnd(stExtentionOut, stExtentionName);

							file.AddExtention(stExtentionName);
						}
						else if(stToken == "textures")
							ParseTextures(stream, stExtentionOut);
						else
						{
							break;
						}
					}

					ReplaceType(stExtentionOut, Type::FLOAT3X3);
					ReplaceType(stExtentionOut, Type::FLOAT);
					ReplaceType(stExtentionOut, Type::MATRIX);
					ReplaceType(stExtentionOut, Type::UINT);
				}
			}

			file.AttachExtention(stExtentionOut);
		}

		void eraseSemicolon(std::stringstream& stream, std::string& stValue);

		/************************************************************
		* Input
		************************************************************/

		static const std::set<std::string> allowedTokens = { "float", "float2", "float3", "float4", "matrix", "uint4" };

		void EffectFileParser::ParseInput(std::stringstream& stream, std::string& stOut, ShaderType shader) const
		{
			std::string stRegister;
			stream >> stRegister;

			InputRegister reg;
			if(stRegister == "Instance")
				reg = InputRegister::INSTANCE;
			else if(stRegister == "Stage")
				reg = InputRegister::STAGE;
			else
				ACL_ASSERT(false);

			m_pLanguage->OnBeginInputBlock(stOut, reg, shader);

			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
				TranslateInput(stream, stOut, reg);
			else
			{
				// error
			}
		}

		void EffectFileParser::TranslateInput(std::stringstream& stream, std::string& stOut, InputRegister reg) const
		{
			std::string stToken;

			unsigned int id = 0;
			while(true)
			{
				stream >> stToken;
				if(allowedTokens.count(stToken))
				{
					std::string stValue;
					stream >> stValue;
					eraseSemicolon(stream, stValue);

					m_pLanguage->OnInputVariable(stOut, stToken, stValue, reg, id);

					stOut.append(";\n");

					size_t numElements = 1;
					const size_t openBracket = stToken.find("[");
					const size_t closeBracket = stToken.find("]");
					if(openBracket != std::string::npos && closeBracket != std::string::npos)
					{
						const std::string stNumElements = stToken.substr(openBracket, closeBracket - openBracket);
						conv::FromString(&numElements, stNumElements.c_str());
						stToken.erase(openBracket);
					}

					if(stToken == "matrix")
						id += 4;
					else
						id++;
				}
				else if(stToken.find('}') != std::string::npos)
					break;
				else
				{
					// error;
				}
			}

			m_pLanguage->OnEndInputBlock(stOut);
			stOut.push_back('\n');
		}

		/************************************************************
		* In
		************************************************************/

		void EffectFileParser::ParseInOut(std::stringstream& stream, std::string& stOut, InOut io, ShaderType type) const
		{
			ACL_ASSERT(io == InOut::O || type != ShaderType::PIXEL);

			switch(type)
			{
			case ShaderType::VERTEX:
				m_pLanguage->OnBeginIO(stOut, io);
				break;
			case ShaderType::PIXEL:
				m_pLanguage->OnBeginPixelOut(stOut);
				break;
			case ShaderType::GEOMETRY:
				m_pLanguage->OnBeginGeometryOut(stOut);
				break;
			default:
				ACL_ASSERT(false);
			}

			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
				TranslateInOut(stream, stOut, io, type);
			else
			{
				// error
			}
		}

		void EffectFileParser::TranslateInOut(std::stringstream& stream, std::string& stOut, InOut io, ShaderType type) const
		{
			std::string stToken;

			unsigned int id = 0;
			while(true)
			{
				stream >> stToken;
				if(allowedTokens.count(stToken))
				{
					std::string stValue;
					stream >> stValue;

					eraseSemicolon(stream, stValue);

					if(type == ShaderType::PIXEL)
						m_pLanguage->OnPixelOutVariable(stOut, stToken, stValue, id);
					else
						m_pLanguage->OnIOVariable(stOut, stToken, stValue, id, io);	

					stOut.push_back('\n');
				}
				else if(stToken.find('}') != std::string::npos)
					break;
				else
				{
					// error;
				}

				id++;
			}

			m_pLanguage->OnEndIO(stOut);
		}

		/************************************************************
		* Textures
		************************************************************/

		void EffectFileParser::ParseTextures(std::stringstream& stream, std::string& stOut) const
		{
			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
			{
				unsigned int id = 0;
				std::string stToken;
				while(true)
				{
					stream >> stToken;
					if(stToken.find('}') != std::string::npos)
						break;
					else
					{
						TextureType type;
						if(stToken == "1D")
							type = TextureType::_1D;
						else if(stToken == "2D")
							type = TextureType::_2D;
						else if(stToken == "3D")
							type = TextureType::_3D;

						std::string stValue;
						stream >> stValue;
						eraseSemicolon(stream, stValue);

						const size_t bracketPos = stValue.find("(");
						if(bracketPos != std::string::npos)
						{
							const size_t closePos = stValue.find(")", bracketPos);

							const std::string stRegister = stValue.substr(bracketPos + 1, closePos - (bracketPos + 1));
							conv::FromString(&id, stRegister.c_str());

							stValue.erase(bracketPos, closePos + 1 - bracketPos);
						}

						m_pLanguage->OnTexture(stOut, type, stValue, id);

						stOut += ";\n";

						id++;
					}
				}

				stOut.push_back('\n');
			}
			else
			{
				// error
			}
		}

		/************************************************************
		* Extentions
		************************************************************/

		void EffectFileParser::ParseExtentions(std::stringstream& stream, std::string& stOut) const
		{
			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
			{
				std::string stFunction;
				char c;
				stream.get(c);
				while(c != '{')
				{
					if(c != '\t')
						stFunction += c;
					stream.get(c);
				}

				stFunction += '{';
				ParseFunction(stream, stFunction);
				stFunction += '}';

				while(c != '}')
					stream.get(c);

				const size_t bracketPos = stFunction.find('(');
				const size_t whiteSpacePos = stFunction.find(" ");

				const std::string stName = stFunction.substr(whiteSpacePos + 1, bracketPos - (whiteSpacePos+1));

				m_pLanguage->OnExtention(stOut, stFunction, stName, 0);

				stOut.push_back('\n');
			}
			else
			{
				// error
			}
		}

		/************************************************************
		* Main
		************************************************************/

		void EffectFileParser::ParseVertexMain(std::stringstream& stream, std::string& stOut) const
		{
			m_pLanguage->OnMainDeclaration(stOut);
			stOut += "\n{\n";
			m_pLanguage->OnMainTop(stOut);

			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
			{
				ParseFunction(stream, stOut);

				m_pLanguage->OnMainBottom(stOut);
				stOut += "}\n\n";
			}
			else
			{
				// error
			}
		}

		void EffectFileParser::ParsePixelMain(std::stringstream& stream, std::string& stOut) const
		{
			m_pLanguage->OnPixelMainDeclaration(stOut);
			stOut += "\n{\n";
			m_pLanguage->OnPixelMainTop(stOut);

			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
			{
				ParseFunction(stream, stOut);

				m_pLanguage->OnPixelMainBottom(stOut);
				stOut += "}\n";
			}
			else
			{
				// error
			}
		}

		void EffectFileParser::ParseExtentionMain(std::stringstream& stream, std::string& stOut) const
		{
			std::string stToken;
			stream >> stToken;

			ACL_ASSERT(stToken == "{");

			while(stream.good())
			{
				char c;
				stream.get(c);

				if(c == '{')
					break;

				if(c != '\t')
					stOut += c;
			}

			stOut += '{';
			ParseFunction(stream, stOut);
			stOut += "}\n";
		}

		/************************************************************
		* Function
		************************************************************/

		void EffectFileParser::ParseFunctions(std::stringstream& stream, std::string& stOut) const
		{
			std::string stToken;
			stream >> stToken;

			ACL_ASSERT(stToken == "{");

			while(stream.good())
			{
				char c;
				stream.get(c);
				while(c != '{' && c != '}')
				{
					if(c != '\t' && c != '\n' && c != '\r')
						stOut += c;

					stream.get(c);
				}

				if(c == '}')
					break;

				stOut += '{';
				ParseFunction(stream, stOut);
				stOut += "}\n";

				while(c == '\n' || c == '\r' || c == '\t')
					stream.get(c);

				if(c == '}')
					break;
			}
		}

		void EffectFileParser::ParseFunction(std::stringstream& stream, std::string& stOut) const
		{
			unsigned int brackets = 1;

			std::string stToken;
			while(stream.good())
			{
				char c;
				stream.get(c);
				switch(c)
				{
				case '}':
					brackets -= 1;
					if(!brackets)
					{
						stOut += stToken;
						stOut += "\n";
						return;
					}
					else
						stToken += '}';
					break;
				case '{':
					stToken += '{';
					brackets += 1;
					break;
				case '(':
					{
						size_t whitePos = stToken.find_last_of("\n\t ");
						size_t size = stToken.size() - 1;
						while(whitePos == size)
						{
							whitePos = stToken.find_last_of("\n\t ", --size);
						}
						if(whitePos != std::string::npos)
						{
							stOut += stToken.substr(0, whitePos + 1);
							stToken.erase(0, whitePos + 1);
						}
						stToken = ParseFunctionCall(stream, stToken);
						break;
					}
				case '\t':
					break;
				case '\r':
					break;
				default:
					stToken.push_back(c);
					break;
				}
 			}
		}

		std::string EffectFileParser::ParseFunctionCall(std::stringstream& stream, const std::string& stName) const
		{
			IEffectLanguageParser::ArgumentVector vArguments;

			std::string stToken;
			while(stream.good())
			{
				char c;
				stream.get(c);
				if(c == ')')
					break;
				else if(c == '(')
				{
					while(stToken[0] == ' ')
					{
						stToken.erase(0, 1);
					}
					stToken = ParseFunctionCall(stream, stToken);
				}
				else if(c == ',')
				{
					if(!stToken.empty())
					{
						vArguments.push_back(stToken);
						stToken.clear();
					}
				}
				else if(c != '\t')
					stToken.push_back(c);
			}

			if(!stToken.empty())
				vArguments.push_back(stToken);

			return m_pLanguage->OnFunctionCall(stName, vArguments);
		}

		/************************************************************
		* Helper
		************************************************************/

		void replace(std::string& stTarget, const std::string& stOld, const std::string& stNew);

		void EffectFileParser::ReplaceType(std::string& stShader, Type type) const
		{
			std::string stType;
			switch(type)
			{
			case Type::FLOAT3X3:
				stType = "float3x3";
				break;
			case Type::FLOAT:
				stType = "float";
				break;
			case Type::MATRIX:
				stType = "matrix";
				break;
			case Type::UINT:
				stType = "uint";
				break;
			case Type::OUT_:
				stType = "out";
				break;
			case Type::IN_:
				stType = "in";
				break;
			default:
				ACL_ASSERT(false);
			}

			ACL_ASSERT(!stType.empty());

			auto stNew = m_pLanguage->OnConvertType(type);
			if(stNew != stType && !stNew.empty())
			{
				if(type != Type::FLOAT)
					replace(stShader, stType, stNew);
				else
				{
					size_t pos = 0;
					while((pos = stShader.find(stType, pos)) != std::string::npos)
					{
						const char c = stShader[pos + 5];
						if(c == '2' || c == '3' || c == '4')
						{
							stShader.replace(pos, stType.length(), stNew);
							pos += stNew.length();
						}
						else
							pos += stType.length();
					}
				}
			}
				
		}

		void replace(std::string& stTarget, const std::string& stOld, const std::string& stNew)
		{
			size_t pos = 0;
			while((pos = stTarget.find(stOld, pos)) != std::string::npos) 
			{
				auto temp1 = stTarget[pos - 1];
				auto temp2 = stTarget[pos + stOld.size()];
				auto temp3 = stTarget.substr(pos - 1, stOld.size() + 1);
				if(!isalpha(stTarget[pos - 1]) && !isalpha(stTarget[pos + stOld.size()]))
				{
					stTarget.replace(pos, stOld.length(), stNew);
					pos += stNew.length();
				}
				else
					pos += stOld.length();
			}
		}

		void eraseSemicolon(std::stringstream& stream, std::string& stValue)
		{
			const size_t semicolonPos = stValue.size() - 1;
			if(stValue[semicolonPos] != ';')
			{
				std::string stDelim;
				stream >> stDelim;
				if(stDelim.size() != 1 || stDelim[0] != ';')
				{
					// error
				}
			}
			else
				stValue = stValue.erase(semicolonPos);
		}

		/*****************************************
		* GeometryShader
		******************************************/

		void EffectFileParser::ParseGeometryPrimitive(std::stringstream& stream, std::string& stOut) const
		{
			std::string stToken;
			stream >> stToken;

			ACL_ASSERT(stToken == "{");

			const char* pTokens[2] = { "in:", "out:" };
			for(unsigned int i = 0; i < 2; i++)
			{
				stream >> stToken;

				ACL_ASSERT(stToken == pTokens[i]);

				stream >> stToken;

				const size_t beginPosition = stToken.find("[");
				ACL_ASSERT(beginPosition != std::string::npos);
				const size_t endPosition = stToken.find("]");
				ACL_ASSERT(endPosition != std::string::npos);

				const std::string stPrimitive = stToken.substr(0, beginPosition);

				GeometryPrimitiveType type;
				if(stPrimitive == "triangle")
					type = GeometryPrimitiveType::TRIANGLE;
				else if(stPrimitive == "line")
					type = GeometryPrimitiveType::LINE;
				else if(stPrimitive == "point")
					type = GeometryPrimitiveType::POINT;
				else
					ACL_ASSERT(false);

				const std::string stNumPrimitives = stToken.substr(beginPosition + 1, endPosition - beginPosition - 1);

				if(i == 0)
				{
					const unsigned int numPrimitives = conv::FromString<unsigned int>(stNumPrimitives);
					m_pLanguage->OnGeometryPrimitiveIn(type, numPrimitives, stOut);
				}
				else
					m_pLanguage->OnGeometryPrimitiveOut(type, stNumPrimitives, stOut);
			}

			stream >> stToken;
			
			ACL_ASSERT(stToken == "}");
		}

		void EffectFileParser::ParseGeometryMain(std::stringstream& stream, std::string& stOut) const
		{
			m_pLanguage->OnGeometryMainDeclaration(stOut);
			stOut += "\n{\n";
			m_pLanguage->OnGeometryMainTop(stOut);

			std::string stBracket;
			stream >> stBracket;
			if(stBracket == "{")
			{
				ParseFunction(stream, stOut);

				stOut += "}\n";
			}
			else
			{
				// error
			}
		}

	}
}

