#pragma once
#include <sstream>

namespace acl
{
	namespace gfx
	{

		class EffectFile;
		class IEffectLanguageParser;

		enum class InOut;
		enum class ShaderType;
		enum class InputRegister;
		enum class Type;

		class EffectFileParser
		{
		public:

			EffectFileParser(const IEffectLanguageParser& parser);

			EffectFile* Parse(const std::wstring& stFilename) const;
			void ParseExtention(const std::wstring& stFilename, EffectFile& file) const;

		private:

			// input
			void ParseInput(std::stringstream& stream, std::string& stOut, ShaderType shader) const;
			void TranslateInput(std::stringstream& stream, std::string& stOut, InputRegister reg) const;

			// in/out
			void ParseInOut(std::stringstream& stream, std::string& stOut, InOut io, ShaderType type) const;
			void TranslateInOut(std::stringstream& stream, std::string& stOut, InOut io, ShaderType type) const;

			// textures
			void ParseTextures(std::stringstream& stream, std::string& stOut) const;

			// extentions
			void ParseExtentions(std::stringstream& stream, std::string& stOut) const;

			// main
			void ParseVertexMain(std::stringstream& stream, std::string& stOut) const;
			void ParsePixelMain(std::stringstream& stream, std::string& stOut) const;
			void ParseExtentionMain(std::stringstream& stream, std::string& stOut) const;

			// function
			void ParseFunctions(std::stringstream& stream, std::string& stOut) const;
			void ParseFunction(std::stringstream& stream, std::string& stOut) const;
			std::string ParseFunctionCall(std::stringstream& stream, const std::string& stName) const;
			
			// postprocess
			void ReplaceType(std::string& stShader, Type type) const;

			/*******************************
			* GeometryShader
			*******************************/

			void ParseGeometryPrimitive(std::stringstream& stream, std::string& stOut) const;
			void ParseGeometryMain(std::stringstream& stream, std::string& stOut) const;


			const IEffectLanguageParser* m_pLanguage;
		};

	}
}



