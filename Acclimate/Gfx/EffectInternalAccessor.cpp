#include "EffectInternalAccessor.h"
#include "IEffect.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		EffectInternalAccessor::EffectInternalAccessor(void) : m_pEffect(nullptr), m_pCbuffer(nullptr)
		{
		}

		EffectInternalAccessor::EffectInternalAccessor(AclEffect& effect) : m_pEffect(&effect), m_pCbuffer(nullptr)
		{
		}

		void EffectInternalAccessor::SetEffect(AclEffect* pEffect)
		{
			m_pEffect = pEffect;
		}

		AclEffect* EffectInternalAccessor::GetEffect(void) const
		{
			return m_pEffect;
		}

		void EffectInternalAccessor::SetCbuffer(AclCbuffer* pCbuffer)
		{
			m_pCbuffer = pCbuffer;
		}

		AclCbuffer* EffectInternalAccessor::GetCbuffer(void) const
		{
			return m_pCbuffer;
		}

		AclCbuffer* cloneVCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation)
		{
			EffectInternalAccessor accessor;
			effect.CloneVCbuffer(accessor, id, permutation);
			return accessor.GetCbuffer();
		}

		AclCbuffer* clonePCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation)
		{
			EffectInternalAccessor accessor;
			effect.ClonePCbuffer(accessor, id, permutation);
			return accessor.GetCbuffer();
		}

		AclCbuffer* cloneGCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation)
		{
			EffectInternalAccessor accessor;
			effect.CloneGCbuffer(accessor, id, permutation);
			return accessor.GetCbuffer();
		}

	}
}

