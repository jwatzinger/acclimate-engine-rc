#pragma once
#include <string>
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class IEffect;

		class EffectInternalAccessor
		{
		public:

			EffectInternalAccessor(void);
			EffectInternalAccessor(AclEffect& effect);

			void SetEffect(AclEffect* pEffect);
			AclEffect* GetEffect(void) const;

			void SetCbuffer(AclCbuffer* pCbuffer);
			AclCbuffer* GetCbuffer(void) const;

		private:

			AclEffect* m_pEffect;
			AclCbuffer* m_pCbuffer;
		};

		AclCbuffer* cloneVCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation);
		AclCbuffer* clonePCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation);
		AclCbuffer* cloneGCbuffer(const IEffect& effect, unsigned int id, unsigned int permutation);

	}
}


