#include "EffectReflector.h"
#include <sstream>
#include "..\System\Assert.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gfx
	{

#pragma warning (disable: 4351)
		ReflectionData::ReflectionData(void) : bufferSize()
		{
		}
#pragma warning (default: 4351)

		EffectReflector::EffectReflector(const EffectFile& file):
			m_pFile(&file)
		{
		}

		ReflectionData EffectReflector::Reflect(unsigned int permutation) const
		{
			ACL_ASSERT(permutation <= m_pFile->GetMaxPermutations());

			const auto vPermutations = m_pFile->GetPermutations(permutation);

			return Reflect(vPermutations);
		}

		ReflectionData EffectReflector::Reflect(const EffectFile::PermutationVector& vPermutations) const
		{
			ReflectionData data;

			std::string stSource = m_pFile->GetSource();

			// preprocess
			for(auto& permutation : vPermutations)
			{
				const size_t permLength = permutation.stName.length();
				size_t permPosition = stSource.find(permutation.stName);
				while(permPosition != std::string::npos)
				{
					stSource.replace(permPosition, permLength, conv::ToStringA(permutation.value));
					permPosition = stSource.find(permutation.stName);
				}
			}

			const auto pixelPos = stSource.find("pixelShader");
			const auto geometryPos = stSource.find("geometryShader");

			size_t inputPos = stSource.find("input ");
			while(inputPos != std::string::npos)
			{
				size_t bufferSize = 0;
				const size_t rightBracketPos = stSource.find('}', inputPos+5);
				
				ReflectionData::ShaderTypes shader;
				if(inputPos > pixelPos)
					shader = ReflectionData::ShaderTypes::PIXEL;
				else if(inputPos > geometryPos)
					shader = ReflectionData::ShaderTypes::GEOMETRY;
				else
					shader = ReflectionData::ShaderTypes::VERTEX;

				std::stringstream stream(stSource.substr(inputPos + 5, rightBracketPos - (inputPos + 5)));
				std::string stData;
				stream >> stData;
				
				ReflectionData::BufferTypes type = ReflectionData::BufferTypes::INSTANCE;
				if(stData.find("Instance") != std::string::npos)
					type = ReflectionData::BufferTypes::INSTANCE;
				else if(stData.find("Stage") != std::string::npos)
					type = ReflectionData::BufferTypes::STAGE;

				while(stream.good())
				{
					stream >> stData;
					// float
					if(stData.find("float") != std::string::npos)
					{
						if(stData.size() == 5)
							bufferSize += sizeof(float);
						else
						{
							const char numFloats = stData[5];
							switch(numFloats)
							{
							case '2':
								bufferSize += sizeof(float)* 2;
								break;
							case '3':
								bufferSize += sizeof(float)* 3;
								break;
							case '4':
								bufferSize += sizeof(float)* 4;
								break;
							}
						}
					}
					else if(stData == "matrix")
						bufferSize += sizeof(float)* 16;
					else if(stData.find("int") != std::string::npos)
					{
						if(stData.size() == 3)
							bufferSize += sizeof(int);
						else
						{
							const char numInts = stData[3];
							switch(numInts)
							{
							case '2':
								bufferSize += sizeof(int)* 2;
								break;
							case '3':
								bufferSize += sizeof(int)* 3;
								break;
							case '4':
								bufferSize += sizeof(int)* 4;
								break;
							}
						}
					}
					else if(stData.find("uint") != std::string::npos)
					{
						if(stData.size() == 4)
							bufferSize += sizeof(unsigned int);
						else
						{
							const char numInts = stData[4];
							switch(numInts)
							{
							case '2':
								bufferSize += sizeof(unsigned int)* 2;
								break;
							case '3':
								bufferSize += sizeof(unsigned int)* 3;
								break;
							case '4':
								bufferSize += sizeof(unsigned int)* 4;
								break;
							}
						}
					}
				}

				data.bufferSize[(size_t)shader][(size_t)type] = bufferSize;
				inputPos = stSource.find("input ", inputPos + 5);
			}

			return data;
		}

	}
}

