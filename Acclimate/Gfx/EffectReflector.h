#pragma once
#include "EffectFile.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		struct ReflectionData
		{
			ReflectionData(void);
			
			enum class ShaderTypes
			{
				VERTEX, PIXEL, GEOMETRY, SIZE
			};

			enum class BufferTypes
			{
				INSTANCE, STAGE, SIZE
			};

			size_t bufferSize[(size_t)ShaderTypes::SIZE][(size_t)BufferTypes::SIZE];
		};

		class EffectFile;

		class ACCLIMATE_API EffectReflector
		{
		public:
			EffectReflector(const EffectFile& file);

			ReflectionData Reflect(unsigned int permutation) const;
			ReflectionData Reflect(const EffectFile::PermutationVector& vPermutations) const;

		private:

			const EffectFile* m_pFile;
		};

	}
}

