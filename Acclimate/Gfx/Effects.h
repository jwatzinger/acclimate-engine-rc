#pragma once
#include <string>
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"
#include "IEffect.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, IEffect> Effects;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, IEffect>;

    }
}