#include "FontFile.h"
#include <memory>

namespace acl
{
	namespace gfx
	{
		
		FontFile::FontFile(void) : m_fontHeight(0)
		{
		}

		FontFile::FontFile(unsigned int fontHeight, CharData data[256]): m_fontHeight(fontHeight)
		{
			memcpy(m_chars, data, sizeof(CharData)*256);
		}

		unsigned int FontFile::GetFontHeight(void) const
		{
			return m_fontHeight;
		}

		unsigned int FontFile::CalculateWidth(const std::wstring& stText) const
		{
			unsigned int width = 0;
			for(auto glyph : stText)
			{
				if(glyph <= 255)
					width += m_chars[glyph].width;
			}

			return width;
		}

	}
}