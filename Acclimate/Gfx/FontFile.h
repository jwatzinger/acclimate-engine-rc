#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		struct ACCLIMATE_API CharData
		{
			CharData(void): ul(0.0f), ur(0.0f), vt(0.0f), vb(0.0f), width(0) {}
			CharData(float ul, float ur, float vt, float vb, unsigned int width): ul(ul), ur(ur), vt(vt), vb(vb), width(width) {}

			float ul, ur, vt, vb;
			unsigned int width;
		};

		class ACCLIMATE_API FontFile
		{
		public:

			FontFile(void);
			FontFile(unsigned int fontHeight, CharData data[256]);

			unsigned int GetFontHeight(void) const;

			inline const CharData& GetCharData(unsigned char c) const
			{
				return m_chars[c];
			}

			inline const CharData& GetCharData(wchar_t c) const
			{
				if(c >= 256)
				{
					static const CharData data;
					return data;
				}
				else
					return m_chars[c];
			}

			unsigned int CalculateWidth(const std::wstring& stText) const;

		private:
			
			unsigned int m_fontHeight;

			CharData m_chars[256];
		};

	}
}
