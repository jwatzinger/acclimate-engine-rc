#include "FontFileLoader.h"
#include <fstream>
#include "TextureDataProxies.h"
#include "ITextureLoader.h"
#include "..\System\Exception.h"
#include "..\System\Convert.h"

#include "TextureAccessors.h"

namespace acl
{
	namespace gfx
	{

		FontFileLoader::FontFileLoader(const gfx::ITextureLoader& loader) : m_dc(GetDC(nullptr)),
			m_pLoader(&loader)
		{
		}

		FontFileLoader::~FontFileLoader(void)
		{
			ReleaseDC(nullptr, m_dc);
		}

		FontFile FontFileLoader::Load(const std::wstring& stFilename, const std::wstring& stTexture, ITexture** ppTexture) const
		{
			std::fstream stream(stFilename, std::ios::in);

			if(!stream.is_open())
				throw fileException();

			/*******************************************
			* Read header line
			*******************************************/

			char firstLine[20];
			stream.read(firstLine, 20);
			firstLine[19] = '\0';

			if(!strcmp(firstLine,"Acclimate font file")) // file description + magic number
			{
				char magicNumber[7];
				stream.read(magicNumber, 6);
				magicNumber[6] = '\0';

				if(strcmp(magicNumber, "789456"))
					throw fileException();
			}
			else
				throw fileException();

			char c;
			stream.get(c);

			/*******************************************
			* Read font data
			*******************************************/

			std::string stTextureWidth, stTextureHeight, stHeight;

			// texture width
			stream >> stTextureWidth;
			unsigned int textureWidth;
			conv::FromString(&textureWidth, stTextureWidth.c_str());

			// texture height
			stream >> stTextureHeight;
			unsigned int textureHeight;
			conv::FromString(&textureHeight, stTextureHeight.c_str());

			// glyph max height
			stream >> stHeight;
			unsigned int height;
			conv::FromString(&height, stHeight.c_str());

			/*******************************************
			* Read char data
			*******************************************/

			float topCoordinate = 0.0f;
			const float verticalDistance = height / (float)textureHeight;
			float leftCoordinate = 0.0f;
			float horizontalDistance = 1.0f / textureWidth;

			CharData chars[256];

			stream.get(c);
			while(!stream.eof())
			{
				stream.get(c);

				std::string stCharWidth;
				stream >> stCharWidth;
				unsigned int charWidth;
				conv::FromString(&charWidth, stCharWidth.c_str());
				charWidth += 1;
				unsigned char index = c;
				chars[index].width = charWidth;
				chars[index].ul = leftCoordinate;
				leftCoordinate += horizontalDistance * charWidth;
				chars[index].ur = leftCoordinate;
				chars[index].vt = topCoordinate;
				chars[index].vb = topCoordinate + verticalDistance;
				

				if(leftCoordinate >= 1.0f)
				{
					leftCoordinate = 0.0f;
					topCoordinate += verticalDistance;
				}

				stream.get(c);
			}

			auto& space = chars[' '];
			if(space.width)
			{
				chars['\t'] = space;
				chars['\t'].width *= 4;
			}

			*ppTexture = m_pLoader->Load(stFilename, stTexture, false);
			return FontFile(height, chars);
			
		}

		FontFile FontFileLoader::Create(const std::wstring& stFontFamily, unsigned int fontSize, ITexture** ppTexture) const
		{
			auto font = CreateFont(-(int)fontSize, 0, 0, 0, 0, false, false, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, stFontFamily.c_str());
			SelectObject(m_dc, font);

			TEXTMETRIC metric;
			GetTextMetrics(m_dc, &metric);

			unsigned int textureWidth = 0;
			CharData chars[256];
			for(unsigned int c = 0; c < 256; c++)
			{
				std::wstring glyph;
				glyph.push_back(c);

				WORD out;
				GetGlyphIndices(m_dc, glyph.c_str(), 1, &out, GGI_MARK_NONEXISTING_GLYPHS);
				if(out == 0xffff)
				{
					chars[c].width = 0;
					continue;
				}

				int width = 0;
				GetCharWidth32(m_dc, c, c, &width);
				width += 1;
				chars[c].width = width;
				chars[c].vt = 0.0f;
				chars[c].vb = 1.0f;
				textureWidth += width;
			}

			// texture
			const auto vTextureSize = math::Vector2(textureWidth, fontSize);
			gfx::TextureDataProxyA32 textureData(vTextureSize);

			// coordinates
			float horizontalDistance = 1.0f / textureWidth;
			
			const int origin = metric.tmAscent - metric.tmDescent + metric.tmInternalLeading;

			unsigned int offset = 0;
			for(unsigned int c = 0; c < 256; c++)
			{
				if(chars[c].width == 0)
					continue;

				// glphy coordinates
				chars[c].ul = offset * horizontalDistance;

				// glyph bitmap
				GLYPHMETRICS metrics = { 0, };
				static const MAT2 mat = { { 0, 1 }, { 0, 0 }, { 0, 0 }, { 0, 1 } };

				const auto bytes = GetGlyphOutline(m_dc, c, GGO_GRAY8_BITMAP, &metrics, 0, nullptr, &mat);

				if(bytes == -1)
				{
					chars[c].ur = chars[c].ur;
					continue;
				}

				if(GetGlyphOutline(m_dc, c, GGO_METRICS, &metrics, 0, nullptr, &mat) == GDI_ERROR)
				{
					// error
					int i = 0;
				}

				auto pBuffer = new BYTE[bytes];
				if(GetGlyphOutline(m_dc, c, GGO_GRAY8_BITMAP, &metrics, bytes, pBuffer, &mat) == GDI_ERROR)
				{
					// error
					int i = 0;
				}

				const unsigned int rowWidth = bytes / metrics.gmBlackBoxY;
				for(unsigned int i = 0; i < rowWidth; i++)
				{
					for(unsigned int j = 0; j < metrics.gmBlackBoxY; j++)
					{
						int color = pBuffer[j * rowWidth + i] * 4;
						color = min(255, color);

						const auto offX = offset + i + metrics.gmptGlyphOrigin.x;
						const auto offY = (int)j + (origin - metrics.gmptGlyphOrigin.y);
						if(offX < textureWidth && offY >= 0 && offY < (int)fontSize)
							textureData.SetPixel(offX, offY, Color(color, color, color, color));
					}
				}

				offset += chars[c].width;
				chars[c].ur = offset * horizontalDistance;

				delete[] pBuffer;
			}

			*ppTexture = m_pLoader->Custom(stFontFamily + conv::ToString(fontSize), vTextureSize, textureData.GetData(), gfx::TextureFormats::A32, gfx::LoadFlags::NONE);
			return FontFile(fontSize, chars);
		}

	}
}