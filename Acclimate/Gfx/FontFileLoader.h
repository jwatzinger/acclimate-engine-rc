#pragma once
#include <string>
#include <Windows.h>
#include "FontFile.h"

namespace acl
{
	namespace gfx
	{

		class ITexture;
		class ITextureLoader;

		class FontFileLoader
		{
		public:

			FontFileLoader(const ITextureLoader& loader);
			~FontFileLoader(void);

			FontFile Load(const std::wstring& stFilename, const std::wstring& stTexture, ITexture** ppTexture) const;
			FontFile Create(const std::wstring& stFontFamily, unsigned int size, ITexture** ppTexture) const;

		private:

			const ITextureLoader* m_pLoader;
			HDC m_dc;
		};

	}
}
