#include "FontFileSaver.h"
#include "..\System\Convert.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		FontFileSaver::FontFileSaver(const Fonts& fonts)
		{

		}

		void FontFileSaver::Save(const std::wstring& stFilename, const std::wstring& stFontfamily, unsigned int fontSize) const
		{
			auto pFont = m_pFonts->Get(stFontfamily + conv::ToString(fontSize));

			if(pFont)
			{
				
			}
			else
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Failed to save font", stFilename, "with size", fontSize, "to file", stFilename, ". Font does not exist, create or load it first.");
			//auto dc = GetDC(nullptr);

			//TEXTMETRIC metric;
			//GetTextMetrics(dc, &metric);

			//auto font = CreateFont(32, 0, 0, 0, 0, false, false, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, L"Lunchtime Doubly So");
			//
			//SelectObject(dc, font);

			//int width;
			//GetCharWidth32(dc, 'A', 'A', &width);

			//ReleaseDC(nullptr, dc);
		}

	}
}

