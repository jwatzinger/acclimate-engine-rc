#pragma once
#include <string>
#include "Fonts.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		class ACCLIMATE_API FontFileSaver
		{
		public:

			FontFileSaver(const Fonts& fonts);
			
			void Save(const std::wstring& stFilename, const std::wstring& stFontfamily, unsigned int fontSize) const;

		private:

			const Fonts* m_pFonts;
		};

	}
}


