#include "FullscreenEffect.h"
#include "IModelLoader.h"
#include "FxInstance.h"
#include "..\Math\Rect.h"
#include "..\Math\Vector.h"
#include "..\System\Log.h"
#include "..\Render\IRenderer.h"
#include "..\Render\IStage.h"

namespace acl
{
	namespace gfx
	{

		FullscreenEffect::FullscreenEffect(const Textures& textures, const IModelLoader& loader, render::IRenderer& renderer): 
			m_textures(textures), m_pRenderer(&renderer), m_loader(loader)
		{
		}

		void FullscreenEffect::SetMesh(const std::wstring& stMesh)
		{
			m_stMesh = stMesh;
		}

		FxInstance& FullscreenEffect::CreateInstance(const std::wstring& stName, const std::wstring& stRenderGroup, const std::wstring& stRenderTarget, unsigned int flags, float viewPortScale) const
		{
			TextureVector vTargets = { stRenderTarget };
			return CreateInstance(stName, stRenderGroup, vTargets, flags, viewPortScale); 
		}

		FxInstance& FullscreenEffect::CreateInstance(const std::wstring& stMaterial, const std::wstring& stRenderGroup, const TextureVector& vRenderTargets, unsigned int flags, float viewPortScale) const
		{
			if(m_stMesh.empty())
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "No mesh set for fullscreen-effect, instance", stMaterial, "will not render correctly.");

			const IModelLoader::PassVector vPasses = { { 3, stMaterial } };
			auto& model = m_loader.LoadInstance(m_stMesh, vPasses);

			size_t id = m_pRenderer->AddQueue(stRenderGroup);

			unsigned int stageFlags = 0;
			if(flags & CLEAR_TARGET)
				stageFlags |= render::CLEAR;
			if(flags & GENERATE_MIPS)
				stageFlags |= render::GENERATE_MIPS;

			auto pStage = m_pRenderer->AddStage(stMaterial, stRenderGroup, id, stageFlags);
			FxInstance& fx = *new FxInstance(model, *pStage, *m_pRenderer);

			size_t slot = 0;
			for(auto& stTarget : vRenderTargets)
			{
				auto pTexture = m_textures[stTarget];

				if(!pTexture)
				{
					if(!stTarget.empty())
					{
						if(slot == 0)
							sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Texture", stTarget, "should be set as target", slot, "for fullscreen effect", stMaterial, "but was not found. Will instead render to backbuffer.");
						else
							sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Texture", stTarget, "should be set as target", slot, "for fullscreen effect", stMaterial, "but was not found.");
					}
				}
				else
				{
					pStage->SetRenderTarget(slot, pTexture);

					if(!slot)
					{
						const math::Vector2& vSize = pTexture->GetSize();
						math::Rect r(0, 0, (int)(vSize.x * viewPortScale), (int)(vSize.y * viewPortScale));
						pStage->SetViewport(r);
					}
				}

				slot++;
			}

			return fx;
		}

	}
}
