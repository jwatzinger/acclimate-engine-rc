#pragma once
#include "Textures.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace render
	{
		class IRenderer;
	}

	namespace gfx
	{

		enum RenderFlags
		{
			CLEAR_TARGET = 1,
			GENERATE_MIPS = 2
		};

		class FxInstance;
		class IModelLoader;

		class ACCLIMATE_API FullscreenEffect
		{
		public:
			
			typedef std::vector<std::wstring> TextureVector;

			FullscreenEffect(const Textures& textures, const IModelLoader& loader, render::IRenderer& renderer);

			void SetMesh(const std::wstring& stMesh);

			FxInstance& CreateInstance(const std::wstring& stName, const std::wstring& stRenderGroup, const std::wstring& stRenderTarget, unsigned int flags, float viewPortScale = 1.0f) const;
			FxInstance& CreateInstance(const std::wstring& stName, const std::wstring& stRenderGroup, const TextureVector& vRenderTargets, unsigned int flags, float viewPortScale = 1.0f) const;

		private:
#pragma warning ( disable : 4251 )
			std::wstring m_stMesh;

			const Textures& m_textures;
			const IModelLoader& m_loader;

			render::IRenderer* m_pRenderer;
#pragma warning ( default : 4251)
		};

	}
}
