#include "FxInstance.h"
#include "IModel.h"
#include "ModelInstance.h"
#include "..\Gfx\IMaterial.h"
#include "..\Render\IStage.h"
#include "..\Render\IRenderer.h"

namespace acl
{
	namespace gfx
	{

		FxInstance::FxInstance(ModelInstance& model, render::IStage& stage, render::IRenderer& renderer): 
			m_pStage(&stage), m_pRenderer(&renderer), m_pInstance(&model)
		{
		}

		FxInstance::~FxInstance(void)
		{
			m_pRenderer->RemoveStage(*m_pStage, true);
			delete m_pInstance;
		}

		void FxInstance::SetTexture(unsigned int slot, const ITexture* pTexture)
		{
			auto pMaterial = m_pInstance->GetParent()->GetMaterial(3);
			pMaterial->SetTexture(slot, pTexture);
		}

		void FxInstance::SetRenderTarget(unsigned int slot, const ITexture* pTexture)
		{
			m_pStage->SetRenderTarget(slot, pTexture);
		}

		void FxInstance::SetShaderConstant(unsigned int index, const float* constArray, size_t numConsts)
		{
			m_pInstance->SetPixelConstant(index, constArray, numConsts);
		}

		void FxInstance::Draw(void) const
		{
			m_pInstance->Draw(*m_pStage, 3);
		}

	}
}
