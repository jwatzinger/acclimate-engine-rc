#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace render
	{
		class IStage;
		class IRenderer;
	}

	namespace gfx
	{

		class ITexture;
		class ModelInstance;

		class ACCLIMATE_API FxInstance
		{
		public:
			FxInstance(ModelInstance& model, render::IStage& stage, render::IRenderer& renderer);
			~FxInstance(void);

			void SetTexture(unsigned int slot, const ITexture* pTexture);
			void SetRenderTarget(unsigned int slot, const ITexture* pTexture);
			void SetShaderConstant(unsigned int index, const float* constArray, size_t numConsts);
			void Draw(void) const;

		private:
#pragma warning( disable: 4251 )
			ModelInstance* m_pInstance;
			render::IStage* m_pStage;
			render::IRenderer* m_pRenderer;
#pragma warning( default: 4251 )
		};

	}
}

