#pragma once

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace gfx
	{

		enum class CbufferType
		{
			VERTEX, PIXEL, GEOMETRY
		};

		enum class CbufferSlot
		{
			INSTANCE, STAGE
		};

		class CbufferInternalAccessor;

		/// API independant constant buffer interface
		/** This class represents an RAM contained constant buffer, for implementation of
		*	shader constants independant of the used API. It currently only offers a method
		*	for writing. */
		class ICbuffer
		{
		public:

			virtual ~ICbuffer(void) {};

			virtual ICbuffer& Clone(void) const = 0;
			virtual void Copy(const ICbuffer& cbuffer) = 0;

			virtual void GetBuffer(CbufferInternalAccessor& accessor) const = 0;
			virtual size_t GetSize(void) const = 0;

			virtual bool Link(const CbufferInternalAccessor& accessor)= 0;

			/** Writes to the cbuffer.
			 *	This method should write floating point data to the cbuffer. It should check if the 
			 *	buffer is exceeded.
			 *
			 *	@param[in] index Number of the starting entry.
			 *	@param[in] constArr Pointer to an array of data to be written to the buffer.
			 *	@param[in] numConsts Number of constants to be written, might be interpreted differently depending on the implementation.
			 */
			virtual void Write(size_t index, const float* constArr, size_t numConsts) = 0;

			virtual void Bind(render::StateGroup& states, CbufferType type, CbufferSlot slot) const = 0;
		};

	}
}

