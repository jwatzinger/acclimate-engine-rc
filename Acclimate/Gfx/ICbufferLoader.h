#pragma once

namespace acl
{
	namespace gfx
	{
		class ICbuffer;
		class CbufferInternalAccessor;

		/// Constant buffer loader interface.
		/** This class acts as a factory for a cbuffer. It is implemented out to allow creating
		*	a class of the ICbuffer interface for a given api framework. */
		class ICbufferLoader
		{
		public:

			virtual ~ICbufferLoader(void) = 0 {};

			/** Creates a constant buffer.
			 *	This method shall create a cbuffer with enough space in both RAM and, if required
			 *	also in video RAM, to hold the specified number fo constants.
			 *
			 *	@param[in] numConsts Number of float constants the buffer must be able to hold.
			 *	
			 *	@return Reference to the created buffer.
			 */
			virtual ICbuffer& Create(size_t numConsts) const = 0;
			virtual ICbuffer& Create(const CbufferInternalAccessor& accessor) const = 0;
		};
	}
}