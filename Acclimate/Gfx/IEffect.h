#pragma once
#include <string>
#include "..\Core\Signal.h"

namespace acl
{
    namespace render
    {
        class StateGroup;
    }

    namespace gfx
    {

        enum class TextureFilter
		{
			POINT,
			LINEAR,
			ANISOTROPIC
		};

		enum class MipFilter
		{
			NONE,
			POINT, 
			LINEAR
		};

		enum class AdressMode
		{
			WRAP,
			MIRROR,
			CLAMP,
			BORDER
		};

		enum class BlendMode
		{
			ONE,
			SRCALPHA,
			INV_SCRALPHA,
			COLOR,
			INV_COLOR,
            ZERO,
		};

		enum class BlendFunc
		{
			ADD,
			SUB
		};

		enum class CullMode
		{
			NONE,
			BACK,
			FRONT
		};

		enum class FillMode
		{
			SOLID, WIREFRAME
		};

		enum class SamplerType
		{
			PIXEL, VERTEX
		};

		class ICbuffer;
		class EffectInternalAccessor;
		class EffectFile;

        class IEffect
        {
        public:

            virtual ~IEffect(void) {}

			virtual void SetAlphaState(bool bEnable, BlendMode srcBlend, BlendMode destBlend, BlendFunc blendFunc, BlendMode alphaSrcBlend, BlendMode alphaDestBlend) = 0;
			virtual void SetDepthState(bool bEnable, bool bWrite) = 0;
			virtual void SetRasterizerState(CullMode cull, FillMode fill, bool bScissorEnable) = 0;
			virtual void SetSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) = 0;
			virtual void SetVertexSamplerState(unsigned int index, TextureFilter minFilter, TextureFilter magFilter, MipFilter mipFilter, AdressMode u, AdressMode v) = 0;
			virtual void OnExtentionAdded(const std::wstring& stName) = 0;
			virtual void SelectExtention(const std::wstring& stName) = 0;

			virtual void CloneVCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const = 0;
			virtual void ClonePCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const = 0;
			virtual void CloneGCbuffer(EffectInternalAccessor& accessor, unsigned int id, unsigned int permutation) const = 0;
			virtual size_t GetNumPermutations(void) const = 0;
			virtual EffectFile& GetFile(void) = 0;
			virtual const EffectFile& GetFile(void) const = 0;

			virtual void BindEffect(render::StateGroup& states, unsigned int permutation) = 0;
			virtual void BindEffect(render::StateGroup& states) const = 0;
			virtual const render::StateGroup& GetState(void) const = 0;
			virtual core::Signal<IEffect*>& GetChangedSignal(unsigned int permutation) = 0;
        };

    }
}