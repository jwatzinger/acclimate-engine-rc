#pragma once
#include <string>
#include <vector>

namespace acl
{
	namespace gfx
	{
		
		enum class InOut
		{
			I, O
		};

		enum class TextureType
		{
			_1D, _2D, _3D
		};

		enum class InputRegister
		{
			INSTANCE = 2, STAGE = 3
		};

		enum class Type
		{
			FLOAT3X3, FLOAT, MATRIX, UINT, IN_, OUT_
		};

		enum class ShaderType
		{
			VERTEX, PIXEL, GEOMETRY
		};

		enum class GeometryPrimitiveType
		{
			TRIANGLE, LINE, POINT
		};

		class IEffectLanguageParser
		{
		public:

			typedef std::vector<std::string> ArgumentVector;

			virtual ~IEffectLanguageParser(void) = 0 {}

			virtual void OnVertexBegin(std::string& stOut) const = 0;
			virtual void OnPixelBegin(std::string& stOut) const = 0;

			virtual void OnBeginInputBlock(std::string& stOut, InputRegister reg, ShaderType type) const = 0;
			virtual void OnInputVariable(std::string& stOut, const std::string& stType, const std::string& stName, InputRegister reg, unsigned int id) const = 0;
			virtual void OnEndInputBlock(std::string& stOut) const = 0;

			virtual void OnBeginIO(std::string& stOut, InOut io) const = 0;
			virtual void OnBeginPixelOut(std::string& stOut) const = 0;
			virtual void OnIOVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id, InOut io) const = 0;
			virtual void OnPixelOutVariable(std::string& stOut, const std::string& stType, const std::string& stName, unsigned int id) const = 0;
			virtual void OnEndIO(std::string& stOut) const = 0;

			virtual void OnTexture(std::string& stOut, TextureType& type, const std::string& stName, unsigned int id) const = 0;

			virtual std::string OnFunctionCall(const std::string& stName, const ArgumentVector& vArguments) const = 0;
			virtual std::string OnConvertType(Type type) const = 0;
			virtual void OnExtention(std::string& stOut, const std::string& stFunction, const std::string& stName, unsigned int id) const = 0;
			virtual void OnExtentionBegin(std::string& stOut, const std::string& stName, const std::string& stParent) const = 0;
			virtual void OnExtentionEnd(std::string& stOut, const std::string& stName) const = 0;

			virtual void OnMainDeclaration(std::string& stOut) const = 0;
			virtual void OnMainTop(std::string& stOut) const = 0;
			virtual void OnMainBottom(std::string& stOut) const = 0;
			virtual void OnPixelMainDeclaration(std::string& stOut) const = 0;
			virtual void OnPixelMainTop(std::string& stOut) const = 0;
			virtual void OnPixelMainBottom(std::string& stOut) const = 0;

			virtual void OnPostProcess(std::string& stOut) const = 0;

			/*************************************
			* GeometryShader
			*************************************/

			virtual void OnGeometryBegin(std::string& stOut) const = 0;
			virtual void OnGeometryPrimitiveOut(GeometryPrimitiveType type, const std::string& stNumPrimitives, std::string& stOut) const = 0;
			virtual void OnGeometryPrimitiveIn(GeometryPrimitiveType type, unsigned int numPrimitives, std::string& stOut) const = 0;
			virtual void OnGeometryMainDeclaration(std::string& stOut) const = 0;
			virtual void OnGeometryMainTop(std::string& stOut) const = 0;
			virtual void OnBeginGeometryOut(std::string& stOut) const = 0;

		};

	}
}