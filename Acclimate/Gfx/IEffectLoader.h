#pragma once
#include <string>

namespace acl
{
    namespace gfx
    {

		class IEffect;
        class EffectPermutator;
		class EffectFile;
		class EffectInternalAccessor;

        class IEffectLoader
        {
        public:

			virtual ~IEffectLoader(void) = 0 {};

			virtual IEffect* Load(const std::wstring& stName, const std::wstring& stFilename) const = 0;
			virtual void LoadExtention(const std::wstring& stName, const std::wstring& stEffectName, const std::wstring& stFilename, bool bActivate) const = 0;
			virtual void CreatePermutation(const EffectFile& file, unsigned int permutation, EffectInternalAccessor& accessor) const = 0;

		};

    }
}