#pragma once
#include <string>

namespace acl
{
    namespace math
    {
        struct Rect;
    }

	namespace render
	{
		class StateGroup;
	}

    namespace gfx
    {

		class ITexture;
		class FontFile;
		struct Color;

        class IFont
        {
        public:

            virtual ~IFont(void) = 0 {};

			virtual size_t DrawString(const math::Rect& rect, float z, const std::wstring& stText, unsigned int dFlags, const Color& color) const = 0;

			virtual const ITexture& GetTexture(void) const = 0;
			virtual const FontFile& GetFontFile(void) const = 0;

			virtual const render::StateGroup& GetState(void) const = 0;
        };

    }
}