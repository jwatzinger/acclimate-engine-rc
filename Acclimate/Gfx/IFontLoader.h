#pragma once
#include <string>

namespace acl
{
    namespace gfx
    {
		
		class IFont;

        class IFontLoader
        {
        public:

			virtual ~IFontLoader(void) = 0 {};

            virtual IFont& Load(const std::wstring& stFontFamily, size_t size) const = 0;
        };
    }
}