#pragma once
#include <vector>

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace gfx
	{

		enum class AttributeSemantic
		{
			POSITION, TEXCOORD
		};

		enum class AttributeType
		{
			FLOAT, UBYTE4, SHORT
		};

		enum class PrimitiveType
		{
			TRIANGLE, LINE, TRIANGLE_STRIP, POINT
		};

		struct VertexAttribute
		{
			VertexAttribute(AttributeSemantic semantic, unsigned int slot, AttributeType type, unsigned int numElements, unsigned int buffer, unsigned int instanceDivisor) : 
				semantic(semantic), slot(slot), type(type), numElements(numElements), buffer(buffer), instanceDivisor(instanceDivisor)
			{
			}

			VertexAttribute(AttributeSemantic semantic, unsigned int slot, AttributeType type, unsigned int numElements) : VertexAttribute(semantic, slot, type, numElements, 0, 0)
			{
			}

			AttributeSemantic semantic;
			unsigned int slot;
			AttributeType type;
			unsigned int numElements;
			unsigned int buffer;
			unsigned int instanceDivisor;
		};

		class IGeometry
		{
		public:

			typedef std::vector<VertexAttribute> AttributeVector;

			virtual ~IGeometry(void) = 0 {}
			virtual IGeometry& Clone(void) const = 0;

			virtual unsigned int CalculateAttributeSize(const VertexAttribute& attribute) const = 0;
			virtual unsigned int CalculateVertexSize(unsigned int buffer) const = 0;

			virtual unsigned int GetVertexSize(void) const = 0;
			virtual PrimitiveType GetPrimitiveType(void) const = 0;
			virtual const AttributeVector& GetAttributes(void) const = 0;
			virtual unsigned int GetNumVerticesPerFace(void) const = 0;
			virtual unsigned int GetNumElements(unsigned int buffer) const = 0;

			virtual void Bind(render::StateGroup& group) const = 0;
		};

	}
}