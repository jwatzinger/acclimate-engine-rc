#pragma once
#include "IGeometry.h"

namespace acl
{
	namespace gfx
	{

		class IGeometryCreator
		{
		public:

			virtual ~IGeometryCreator(void) = 0 {}

			virtual IGeometry& CreateGeometry(PrimitiveType type, const IGeometry::AttributeVector& vAttributes) = 0;
		};


	}
}


