#pragma once
#include "IMesh.h"


namespace acl
{
	namespace gfx
	{

		class IInstancedMesh : 
			public IMesh
		{
		public:

			virtual ~IInstancedMesh(void) {};

			virtual IInstancedMesh& Clone(void) const = 0;

			virtual void SetInstanceCount(unsigned int cInstances) = 0;
			virtual void SetInstanceBuffer(IVertexBuffer& buffer, unsigned int offset) = 0;

			virtual IMesh& GetMeshToInstance(void) const = 0;
			virtual unsigned int GetInstanceCount(void) const = 0;
			virtual IVertexBuffer& GetInstanceBuffer(void) const = 0;
		};

	}
}