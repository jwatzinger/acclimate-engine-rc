#pragma once
#include <vector>

namespace acl
{
	namespace math
	{
		struct Vector3;
	}

    namespace render
    {
        class IStage;
    }

    namespace gfx
    {

        class IEffect;
		struct Color;

		typedef std::vector<math::Vector3> PointVector;

        class ILine
        {
        public:

			virtual ~ILine(void) = 0 {};

            virtual ILine& Clone(void) const = 0;

            virtual void SetLine(const math::Vector3& vFrom, const math::Vector3& vTo) = 0;
            virtual void SetColor(const Color& color) = 0;
			virtual void SetEffect(const IEffect& effect) = 0;

            virtual void Draw(const render::IStage& stage) = 0;
			virtual void DrawMulti(const PointVector& vPoints, const render::IStage& stage) = 0;
        };

    }
}

