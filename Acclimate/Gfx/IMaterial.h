#pragma once
#include <vector>
#include "..\Core\Signal.h"

namespace acl
{
    namespace render
    {
        class StateGroup;
    }

    namespace gfx
    {

		struct MaterialLoadInfo
		{
			typedef std::vector<std::wstring> TextureVector;

			MaterialLoadInfo(void)
			{
			}

			MaterialLoadInfo(const std::wstring& stName, const std::wstring& stEffect, unsigned int permutation, const TextureVector& vTextures) :
				stName(stName), stEffect(stEffect), permutation(permutation), vTextures(vTextures)
			{
			}

			std::wstring stName, stEffect;
			unsigned int permutation;
			TextureVector vTextures;
		};

        class IEffect;
        class ITexture;
            
        class IMaterial
        {
        public:

			virtual ~IMaterial(void) {};

			virtual void SetTexture(unsigned int slotIndex, const ITexture* texture) = 0;
			virtual void SetTexture(unsigned int subset, unsigned int slotIndex, const ITexture* texture) = 0;
			virtual void SetVertexTexture(unsigned int slotIndex, const ITexture* texture) = 0;
			virtual void SetVertexTexture(unsigned int subset, unsigned int slotIndex, const ITexture* texture) = 0;
            virtual void SetEffect(IEffect& effect, unsigned int permutation) = 0;
			virtual void SetLoadInfo(const MaterialLoadInfo* pInfo) = 0;

            virtual size_t GetId(void) const = 0;
			virtual size_t GetEffectPermutation(void) const = 0;
            virtual const IEffect& GetEffect(void) const = 0;
			virtual const render::StateGroup& GetState(unsigned int subset) const = 0;
			virtual const MaterialLoadInfo* GetLoadInfo(void) const = 0;

			virtual core::Signal<const IMaterial&, bool>& GetChangedSignal(void) = 0;
        };

    }
}