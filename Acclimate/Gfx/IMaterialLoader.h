#pragma once
#include <string>
#include <vector>
#include "..\Core\Dll.h"

namespace acl
{
    namespace gfx
    {

		typedef std::vector<std::wstring> TextureVector;

		struct MaterialSubset
		{
			TextureVector vTextures;
			TextureVector vVertexTextures;
		};

        class IMaterial;

        class IMaterialLoader
        {
        public:
			
			typedef std::vector<MaterialSubset> SubsetVector;

			virtual ~IMaterialLoader(void) {};

			virtual IMaterial* Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const std::wstring& stTexture, const SubsetVector* pSubsets = nullptr) const = 0;
            virtual IMaterial* Load(const std::wstring& stName, const std::wstring& stEffectName, unsigned int effectPermutation, const TextureVector& vTextures, const SubsetVector* pSubsets = nullptr) const = 0;
        };

    }
}