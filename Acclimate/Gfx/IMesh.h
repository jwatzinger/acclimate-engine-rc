#pragma once
#include <vector>
#include "..\Core\Signal.h"

namespace acl
{

	namespace render
	{
		class BaseDrawCall;
		class StateGroup;
	}

	namespace gfx
	{

		struct MeshLoadInfo
		{
			enum class Type
			{
				FILE, CUSTOM
			};

			MeshLoadInfo(void)
			{
			}

			MeshLoadInfo(const std::wstring& stName) :
				type(Type::CUSTOM), stName(stName)
			{
			}

			MeshLoadInfo(const std::wstring& stName, const std::wstring& stPath) :
				type(Type::FILE), stName(stName), stPath(stPath)
			{
			}

			Type type;
			std::wstring stName, stPath;
		};

		class IGeometry;
		class MeshSkeleton;
		class IVertexBuffer;
		class IIndexBuffer;

		class IMesh
		{
		public:
			typedef std::vector<unsigned int> SubsetVector;

			virtual ~IMesh(void) = 0 {}

			virtual void SetLoadInfo(const MeshLoadInfo* pInfo) = 0;
			virtual void SetVertexCount(unsigned int numVertices, unsigned int subset = 0) = 0;
			virtual void SetIndexBuffer(IIndexBuffer& buffer) = 0;

			virtual unsigned int GetNumSubsets(void) const = 0;
			virtual unsigned int GetStateCount(void) const = 0;
			virtual const IGeometry& GetGeometry(void) const = 0;
			virtual const render::StateGroup** GetStates(void) const = 0;
			virtual const render::BaseDrawCall& GetDraw(unsigned int subset) const = 0;
			virtual const MeshSkeleton* GetSkeleton(void) const = 0;
			virtual const MeshLoadInfo* GetLoadInfo(void) const = 0;
			virtual	unsigned int GetVertexCount(unsigned int subset) const = 0;
			virtual const SubsetVector& GetSubsets(void) const = 0;
			
			virtual IVertexBuffer& GetVertexBuffer(void) = 0;
			virtual IIndexBuffer& GetIndexBuffer(void) = 0;

			virtual core::Signal<const IMesh*>& GetChangedSignal(void) = 0;
		};
	}
}