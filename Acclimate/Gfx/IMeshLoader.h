#pragma once
#include <string>
#include "IGeometry.h"

namespace acl
{
    namespace math
    {
        struct Vector2;
    }

    namespace gfx
    {
		class IMesh;
		class IInstancedMesh;
		class IIndexBuffer;

        class IMeshLoader
        {
        public:

			virtual ~IMeshLoader(void) {}

            virtual IMesh& Load(const std::wstring& stName, const std::wstring& stFilename) const = 0;
			virtual IMesh& HeightMap(const std::wstring& stName, const std::wstring& stFilename) const = 0;
			virtual IMesh& Line(const std::wstring& stName, unsigned int segments) const = 0;
			virtual IMesh& LineCube(const std::wstring& stName, float size) const = 0;
			virtual IMesh& LineGrid(const std::wstring& stName, float size, int divisions) const = 0;
			virtual IMesh& ScreenQuad(const std::wstring& stName, const math::Vector2& vSize) const = 0;
			virtual IMesh& BillBoard(const std::wstring& stName, float size) const = 0;
			virtual IMesh& Grid(const std::wstring& stName, int sizeX, int sizeY) const = 0;

			typedef std::vector<float> VertexVector;
			typedef std::vector<unsigned int> IndexVector;

			virtual IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, size_t numVertices, const float* pVertices, size_t numIndices, const unsigned int* pIndices, PrimitiveType type) const = 0;
			virtual IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, const IndexVector& vIndices, PrimitiveType type) const = 0;
			virtual IMesh& Custom(const std::wstring& stName, const IGeometry::AttributeVector& vAttributes, const VertexVector& vVertices, IIndexBuffer& indexBuffer, PrimitiveType type) const = 0;

			virtual IInstancedMesh* Instanced(const std::wstring& stName, unsigned int cInstances, const IGeometry::AttributeVector& vAttributes) const = 0;

			virtual IIndexBuffer& Indexbuffer(const IndexVector& vIndices) const = 0;

        };

    }
}