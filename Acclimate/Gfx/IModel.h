#pragma once
#include <map>
#include <vector>

namespace acl
{
    namespace render
    {
		class StateGroup;
        struct Instance;
    }

	namespace gfx
	{

		struct ModelLoadInfo
		{
			typedef std::map<int, std::wstring> PassMap;

			ModelLoadInfo(void)
			{
			}

			ModelLoadInfo(const std::wstring& stName, const std::wstring& stMesh, const PassMap& mMaterials) :
				stName(stName), stMesh(stMesh), mMaterials(mMaterials)
			{
			}

			std::wstring stName, stMesh;
			PassMap mMaterials;
		};

        class IMesh;
        class IMaterial;
		class ModelInstance;

		typedef std::vector<std::vector<render::Instance>> InstanceVector;

		class IModel
		{
		public:
			virtual ~IModel(void) {};

			virtual IModel& Clone(void) const = 0;

			virtual void SetMesh(IMesh& mesh) = 0;
			virtual void SetMaterial(size_t pass, IMaterial* material) = 0;
			virtual void SetLoadInfo(const ModelLoadInfo* pInfo) = 0;

			virtual IMaterial* GetMaterial(size_t pass) const = 0;
			virtual const IMesh* GetMesh(void) const = 0;
			virtual const ModelLoadInfo* GetLoadInfo(void) const = 0;

			virtual bool HasPass(size_t pass) const = 0;

			virtual ModelInstance& MakeUniqueInstance(void) = 0;
			virtual ModelInstance& CreateInstance(void) = 0;
			virtual void RemoveInstance(ModelInstance& instance) = 0;
			virtual void GenerateInstances(InstanceVector& vInstances, const render::StateGroup& states) const = 0;

		};
	}
}