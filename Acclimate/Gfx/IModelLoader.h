#pragma once
#include <string>
#include <vector>

namespace acl
{
	namespace gfx
	{
		struct Pass
		{
			Pass(int id, const std::wstring& stMaterial): id(id), stMaterial(stMaterial) {}

			int id;
			std::wstring stMaterial;
		};

		class IModel;
		class ModelInstance;

		class IModelLoader
		{
		public:

			typedef std::vector<Pass> PassVector;

			virtual ~IModelLoader(void) = 0 {};

			virtual IModel& Load(const std::wstring& stName, const std::wstring& stMesh, const PassVector& vPasses) const = 0;
			virtual IModel& Load(const std::wstring& stName, const std::wstring& stMesh, const std::wstring& stMaterial) const = 0;
			virtual ModelInstance& LoadInstance(const std::wstring& stMesh, const PassVector& vPasses) const = 0;
			virtual ModelInstance& LoadInstance(const std::wstring& stMesh, const std::wstring& stMaterial) const = 0;

		};

	}
}
