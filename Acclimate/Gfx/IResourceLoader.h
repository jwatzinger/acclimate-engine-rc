#pragma once
#include <string>

namespace acl
{
    namespace gfx
    {

        class IResourceLoader
        {
        public:

			virtual ~IResourceLoader(void) {};

            virtual void Load(const std::wstring& stFilename) const = 0;

        };
    }
}