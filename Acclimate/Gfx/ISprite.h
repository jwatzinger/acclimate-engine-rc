#pragma once

namespace acl
{
    namespace math
    {
        struct Rect;
    }

    namespace render
    {
        class IStage;
    }

	namespace gfx
	{

		class ITexture;
		class IEffect;

		class ISprite
		{
		public:

			virtual ~ISprite(void) {};

            virtual ISprite& Clone(void) const = 0;

			virtual void SetPosition(int x, int y) = 0;
			virtual void SetZ(float z) = 0;
			virtual void SetSize(int width, int heigh) = 0;
			virtual void SetScale(float scaleX, float scaleY) = 0;
			virtual void SetOrigin(int originX, int originY) = 0;
			virtual void SetAngle(float fAngle) = 0;
			virtual void SetTexture(ITexture* pTexture) = 0;
			virtual void SetClipRect(const math::Rect* pClip) = 0;
			virtual void SetSrcRect(int x, int y, int width, int height) = 0;
			virtual void SetSrcRect(const math::Rect& rSrcRect) = 0;
			virtual void SetPixelConstant(unsigned int index, const float constArr[4]) = 0;
			virtual void SetEffect(const IEffect& effect) = 0;
			virtual void SetVisible(bool bVisible) = 0;

			virtual ITexture* GetTexture(void) const = 0;

			virtual void Draw(const render::IStage& stage) = 0;

		};

	}
}

