#pragma once
#include <string>

namespace acl
{
	namespace math
	{
		struct Rect;
	}

	namespace render
	{
		class IStage;
	}

	namespace gfx
	{

		enum class Sorting
		{
			NONE = 0,
			REVERSE,
			FRONT_TO_BACK,
			BACK_TO_FRONT,
			TEXTURE,
			TEXTURE_TO_BACK,
			TEXTURE_TO_FRONT,
		};

		class ITexture;
		class IEffect;
		struct Color;

		class ISpriteBatch
		{
		public:
			virtual ~ISpriteBatch(void) {};

			virtual void SetPosition(int x, int y) = 0;
			virtual void SetZ(float z) = 0;
			virtual void SetSize(int width, int heigh) = 0;
			virtual void SetTexture(const ITexture* pTexture) = 0;
			virtual void SetClipRect(const math::Rect* pClip) = 0;
			virtual void SetSrcRect(int x, int y, int width, int height) = 0;
			virtual void SetSrcRect(const math::Rect& rSrcRect) = 0;
			virtual void SetEffect(const IEffect& effect) = 0;
			virtual void SetFont(const std::wstring& stName, unsigned int size) = 0;

			virtual void Begin(const render::IStage& stage, Sorting sort) = 0;
			virtual void Draw(void) = 0;
			virtual void Draw(const gfx::Color& color) = 0;
			virtual void Draw(const std::wstring& stText, unsigned int flags, const Color& color) = 0;
			virtual void Submit(void) = 0;
			virtual void End(void) = 0;
		};

	}
}