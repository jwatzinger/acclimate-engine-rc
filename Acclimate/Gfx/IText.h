#pragma once

namespace acl
{
	namespace gfx
	{
		struct Color;
	}

    namespace math
    {
        struct Rect;
    }

    namespace render
    {
        class IStage;
    }

    namespace gfx
    {

		class IEffect;

        class IText
        {
        public:
	
	        virtual ~IText(void) {};

            virtual void SetClipRect(const math::Rect* pClip) = 0;
	        virtual void SetColor(const Color& color) = 0;
	        virtual void SetFontProperties(int fontSize, const std::wstring& stFontName) = 0;
			virtual void SetEffect(const IEffect& effect) = 0;

	        virtual void Draw(render::IStage& stage, const math::Rect& rect, const std::wstring& stText, unsigned long dFlags, float f = 0.0f) =  0;

        };
    }
}

