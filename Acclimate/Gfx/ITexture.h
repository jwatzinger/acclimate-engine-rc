#pragma once
#include <string>
#include "TextureFormats.h"
#include "MapFlags.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

    namespace gfx
    {

		enum class LoadFlags;

		struct TextureLoadInfo
		{
			enum class Type
			{
				FILE, CREATED
			};

			TextureLoadInfo(void)
			{
			}

			TextureLoadInfo(const std::wstring& stName, const std::wstring& stFilename, bool bRead, TextureFormats format) :
				type(Type::FILE), stName(stName), stPath(stFilename), bRead(bRead), format(format)
			{
			}

			TextureLoadInfo(const std::wstring& stName, const math::Vector2& vSize, TextureFormats format, LoadFlags flags) :
				type(Type::CREATED), stName(stName), vSize(vSize), format(format), flags(flags), bRead(false)
			{
			}

			// general
			Type type;
			std::wstring stName;
			TextureFormats format;

			// file
			std::wstring stPath;
			bool bRead;

			// created
			math::Vector2 vSize;
			LoadFlags flags;
		};

		class TextureInternalAccessor;

        class ITexture
        {
        public:

			enum class BindTarget
			{
				PIXEL, VERTEX
			};

            virtual ~ITexture(void) {};

			virtual void Map(MapFlags flags) = 0;
			virtual void Unmap(void) = 0;
			virtual void Resize(const math::Vector2& vSize) = 0;

			virtual void SetLoadInfo(TextureLoadInfo* pInfo) = 0;
            virtual const math::Vector2& GetSize(void) const = 0; 
            virtual void GetAclTexture(TextureInternalAccessor& accessor) const = 0;
			virtual TextureFormats GetFormat(void) const = 0;
			virtual const TextureLoadInfo* GetLoadInfo(void) const = 0;

			virtual void* GetData(void) const = 0;
			virtual unsigned int GetPitch(void) const = 0;

			virtual void Bind(render::StateGroup& states, BindTarget target, unsigned int slot) const = 0;
        };

    }
}