#pragma once
#include <string>
#include "TextureFormats.h"

namespace acl
{
    namespace math
    {
        struct Vector2;
    }

    namespace gfx
    {

		enum class LoadFlags
		{
			NONE = 0,
			RENDER_TARGET = 1,
			RENDER_TARGET_MIPS = 2,
			CPU_WRITE = 3,
		};

		enum class FileFormat
		{
			DDS, PNG
		};

        class ITexture;

        class ITextureLoader
        {
        public:

			virtual ~ITextureLoader(void) = 0 {};
            virtual ITexture* Load(const std::wstring& stName, const std::wstring& stFilename, bool bRead, TextureFormats format = TextureFormats::UNKNOWN) const = 0;
            virtual ITexture* Create(const std::wstring& stName, const math::Vector2& vSize, TextureFormats format, LoadFlags flags) const = 0;
			virtual ITexture* Custom(const std::wstring& stName, const math::Vector2& vSize, const void* pData, TextureFormats format, LoadFlags flags) const = 0;

			virtual void SaveTexture(const std::wstring& stTexture, const std::wstring& stFile, FileFormat format) const = 0;

        };

    }
}