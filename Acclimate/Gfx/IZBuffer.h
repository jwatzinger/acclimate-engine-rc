#pragma once

namespace acl
{
	namespace math
	{
		struct Vector2;
	}

	namespace gfx
	{

		class ZBufferInternalAccessor;

		class IZBuffer
		{
		public:

			virtual ~IZBuffer(void) = 0 {};

			virtual void Resize(const math::Vector2& vSize) = 0;

			virtual void GetBuffer(ZBufferInternalAccessor& accessor) const = 0;
			virtual const math::Vector2& GetSize(void) const = 0;
		};
	}
}
