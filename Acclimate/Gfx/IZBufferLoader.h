#pragma once
#include <string>

namespace acl
{
    namespace math
    {
        struct Vector2;
    }

    namespace gfx
    {

        class IZBuffer;

        class IZBufferLoader
        {
        public:

			virtual ~IZBufferLoader(void) {};

            virtual IZBuffer* Create(const std::wstring& stName, const math::Vector2& vSize) const = 0;

        };

    }
}