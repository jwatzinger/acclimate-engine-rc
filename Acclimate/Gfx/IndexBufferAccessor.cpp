#include "IndexBufferAccessor.h"
#include "IMesh.h"
#include "MeshBuffer.h"

namespace acl
{
	namespace gfx
	{

		IndexBufferAccessor::IndexBufferAccessor(IMesh& mesh) :
			IndexBufferAccessor(mesh.GetIndexBuffer())
		{
		}

		IndexBufferAccessor::IndexBufferAccessor(IIndexBuffer& buffer) :
			m_pBuffer(&buffer)
		{
			m_pBuffer->Map(MapFlags::WRITE_DISCARD);
			m_pData = m_pBuffer->GetData();
		}

		IndexBufferAccessor::~IndexBufferAccessor(void)
		{
			if(m_pBuffer)
				m_pBuffer->Unmap();
		}

		unsigned int* IndexBufferAccessor::GetData(void) const
		{
			return m_pData;
		}

		void IndexBufferAccessor::Quit(void)
		{
			m_pBuffer->Unmap();
			m_pBuffer = nullptr;
		}

	}
}