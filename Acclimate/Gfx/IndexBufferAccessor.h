#pragma once

namespace acl
{
	namespace gfx
	{

		class IMesh;
		class IIndexBuffer;

		class IndexBufferAccessor
		{
		public:
			IndexBufferAccessor(IMesh& mesh);
			IndexBufferAccessor(IIndexBuffer& buffer);
			~IndexBufferAccessor(void);

			unsigned int* GetData(void) const;

			void Quit(void);

		private:

			unsigned int* m_pData;
			IIndexBuffer* m_pBuffer;

		};

	}
}