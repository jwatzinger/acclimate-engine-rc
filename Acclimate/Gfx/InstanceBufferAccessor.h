#pragma once
#include "IInstancedMesh.h"
#include "MeshBuffer.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		template<typename Type>
		class InstanceBufferAccessor
		{
		public:
			InstanceBufferAccessor(IInstancedMesh& mesh) :
				InstanceBufferAccessor(mesh.GetInstanceBuffer())
			{
			}

			InstanceBufferAccessor(IVertexBuffer& buffer) :
				m_pBuffer(&buffer)
			{
#ifdef _DEBUG
				const size_t size = sizeof(Type);
				ACL_ASSERT(size == m_pBuffer->GetStride());
#endif

				m_pBuffer->Map(MapFlags::WRITE_DISCARD);
				m_pData = (Type*)m_pBuffer->GetData();
			}

			~InstanceBufferAccessor(void)
			{
				if(m_pBuffer)
					m_pBuffer->Unmap();
			}

			Type* GetData(void) const
			{
				return m_pData;
			}

			void Quit(void)
			{
				m_pBuffer->Unmap();
				m_pBuffer = nullptr;
			}

		private:

			Type* m_pData;
			IVertexBuffer* m_pBuffer;
		};

	}
}