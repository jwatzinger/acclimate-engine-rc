#pragma once
#include <string>

namespace acl
{
	namespace gfx
	{
		
		struct Linkage
		{
			Linkage(void): stClassName(""), slotId(0) {}
			Linkage(const std::string& stClassName, size_t slotId): stClassName(stClassName), slotId(slotId) 
			{
			}

			std::string stClassName;
			size_t slotId;
		};

	}
}