#pragma once

namespace acl
{
	namespace gfx
	{

		enum class MapFlags
		{
			READ, WRITE, READ_WRITE, WRITE_DISCARD
		};

	}
}