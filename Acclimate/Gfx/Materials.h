#pragma once
#include <string>
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"
#include "IMaterial.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, IMaterial> Materials;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, IMaterial>;

    }
}