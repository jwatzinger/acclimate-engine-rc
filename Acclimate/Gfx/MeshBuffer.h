#pragma once
#include "MapFlags.h"

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace gfx
	{

		enum class BindTarget
		{
			VERTEX = 0, INSTANCE
		};

		class IVertexBuffer
		{
		public:

			virtual ~IVertexBuffer(void) = 0 {};
			virtual IVertexBuffer& Clone(void) const = 0;

			virtual void Map(MapFlags flags) = 0;
			virtual void Unmap(void) = 0;

			virtual void* GetData(void) = 0;
			virtual unsigned int GetSize(void) const = 0;
			virtual unsigned int GetStride(void) const = 0;

			virtual void Bind(render::StateGroup& group, BindTarget target, unsigned int offset = 0) const = 0;
		};

		class IIndexBuffer
		{
		public:
			virtual ~IIndexBuffer(void) = 0 {};

			virtual void Map(MapFlags flags) = 0;
			virtual void Unmap(void) = 0;

			virtual unsigned int* GetData(void) = 0;
			virtual unsigned int GetSize(void) const = 0;

			virtual void Bind(render::StateGroup& group) const = 0;
		};

	}
}