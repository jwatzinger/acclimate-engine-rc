#include "MeshFile.h"
#include "MeshSkeleton.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace gfx
	{

		MeshFile::MeshFile(void) : m_numOptions(0), m_numVertices(0), m_numPerFace(0), m_numFaces(0), m_pData(nullptr), m_pFaceData(nullptr),
			m_pSkeleton(nullptr), m_pSubsetData(nullptr)
		{

		}

		MeshFile::MeshFile(MeshOptions options, unsigned int numOptions, size_t numVertices, float* pVertexData, unsigned int numPerFace, size_t numFaces, unsigned int* pFaceData, MeshSkeleton* pSkeleton) :
			m_options(options), m_numOptions(numOptions), m_numVertices(numVertices), m_numPerFace(numPerFace), m_numFaces(numFaces), m_pData(pVertexData), m_pFaceData(pFaceData),
			m_pSubsetData(nullptr), m_numSubsets(1), m_pSkeleton(pSkeleton)
		{
		}

		MeshFile::MeshFile(MeshOptions options, unsigned int numOptions, size_t numVertices, float* pVertexData, unsigned int numPerFace, size_t numFaces, unsigned int* pFaceData, size_t numSubsets, unsigned int* pSubsetData, MeshSkeleton* pSkeleton) :
			m_options(options), m_numOptions(numOptions), m_numVertices(numVertices), m_numPerFace(numPerFace), m_numFaces(numFaces), m_pData(pVertexData), m_pFaceData(pFaceData),
			m_pSubsetData(pSubsetData), m_numSubsets(numSubsets), m_pSkeleton(pSkeleton)
		{
		}

		MeshFile::MeshFile(const MeshFile& file): m_options(file.m_options), m_numOptions(file.m_numOptions), m_numVertices(file.m_numVertices), 
			m_numPerFace(file.m_numPerFace), m_numFaces(file.m_numFaces), m_numSubsets(file.m_numSubsets)
		{
			m_pData = new float[m_numVertices];
			memcpy(m_pData, file.m_pData, sizeof(float)*m_numVertices);

			m_pFaceData = new unsigned int[m_numFaces];
			memcpy(m_pFaceData, file.m_pFaceData, sizeof(unsigned int)*m_numVertices);

			m_pSubsetData = new unsigned int[m_numSubsets];
			memcpy(m_pSubsetData, file.m_pSubsetData, sizeof(unsigned int)*m_numSubsets);

			m_pSkeleton = new MeshSkeleton(*file.m_pSkeleton);
		}

		MeshFile::MeshFile(MeshFile&& file): m_options(file.m_options), m_numOptions(file.m_numOptions), m_numVertices(file.m_numVertices), 
			m_numPerFace(file.m_numPerFace), m_numFaces(file.m_numFaces), m_numSubsets(file.m_numSubsets), m_pData(file.m_pData), m_pFaceData(file.m_pFaceData), 
			m_pSubsetData(file.m_pSubsetData), m_pSkeleton(file.m_pSkeleton)
		{
			file.m_pData = nullptr;
			file.m_pFaceData = nullptr;
			file.m_pSubsetData = nullptr;
			file.m_numVertices = 0;
			file.m_numFaces = 0;
			file.m_numSubsets = 0;
			file.m_pSkeleton = nullptr;
		}

		MeshFile::~MeshFile(void)
		{
			delete[] m_pData;
			delete[] m_pFaceData;
			delete[] m_pSubsetData;

			delete m_pSkeleton;
		}

		void MeshFile::SetVertex(unsigned int pos, float* data, unsigned int numOptions)
		{
			if(pos >= m_numVertices || numOptions != m_numOptions)
				throw AclException(); // todo: custom exception

			memcpy(&m_pData+pos*numOptions, &data, numOptions*sizeof(float));
		}

		void MeshFile::SetFace(unsigned int pos, int* data, unsigned int numPerFace)
		{
			if(pos >= m_numFaces || numPerFace != m_numPerFace)
				throw AclException(); // todo: custom exception

			memcpy(&m_pFaceData+pos*numPerFace, &data, numPerFace*sizeof(int));
		}

		size_t MeshFile::GetNumVertices(void) const
		{
			return m_numVertices;
		}

		size_t MeshFile::GetNumFaces(void) const
		{
			return m_numFaces;
		}

		size_t MeshFile::GetNumSubsets(void) const
		{
			return m_numSubsets;
		}

		unsigned int MeshFile::GetVerticesPerFace(void) const
		{
			return m_numPerFace;
		}

		const float* MeshFile::GetData(void) const
		{
			return m_pData;
		}

		const unsigned int* MeshFile::GetFaceData(void) const
		{
			return m_pFaceData;
		}

		const unsigned int* MeshFile::GetSubsetData(void) const
		{
			return m_pSubsetData;
		}

		MeshOptions MeshFile::GetOptions(void) const
		{
			return m_options;
		}

		unsigned int MeshFile::GetNumOptions(void) const
		{
			return m_numOptions;
		}

		size_t MeshFile::DataBufferSize(void) const
		{
			return m_numVertices * m_numOptions * sizeof(float);
		}

		size_t MeshFile::FaceBufferSize(void) const
		{
			return m_numFaces * m_numPerFace * sizeof(unsigned int);
		}

		size_t MeshFile::SubsetBufferSize(size_t subset) const
		{
			return m_pSubsetData[subset] * m_numPerFace * sizeof(unsigned int);
		}

		size_t MeshFile::VertexSize(void) const
		{
			return m_numOptions * sizeof(float);
		}

		size_t MeshFile::NumIndices(void) const
		{
			return m_numFaces * m_numPerFace;
		}

		const MeshSkeleton* MeshFile::GetSkeleton(void) const
		{
			return m_pSkeleton;
		}

		bool MeshFile::HasOptions(MeshOptions options) const
		{
			return (m_options & options) == options;
		}

		bool MeshFile::IsValid(void) const
		{
			return m_pData && m_pFaceData;
		}

	}
}
