#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		enum MeshOptions
		{
			POS_X = 1, 
			POS_Y = 2, 
			POS_Z = 4,
			POS = 7,
			TEX_U = 8, 
			TEX_V = 16, 
			TEX = 24,
			NRM = 32, 
			TAN = 64,
			BIN = 128,
			WEIGHTS = 256,
			INDICES = 512
		};

		class MeshSkeleton;

		class ACCLIMATE_API MeshFile
		{
		public:
			MeshFile(void);
			MeshFile(MeshOptions options, unsigned int numOptions, size_t numVertices, float* vertexData, unsigned int numPerFace, size_t numFaces, unsigned int* faceData, MeshSkeleton* pSkeleton);
			MeshFile(MeshOptions options, unsigned int numOptions, size_t numVertices, float* vertexData, unsigned int numPerFace, size_t numFaces, unsigned int* faceData, size_t numSubsets, unsigned int* subsetData, MeshSkeleton* pSkeleton);
			MeshFile(const MeshFile& file);
			MeshFile(MeshFile&& file);
			~MeshFile(void);

			void SetVertex(unsigned int pos, float* data, unsigned int numOptions);
			void SetFace(unsigned int pos, int* data, unsigned int numPerFace);

			size_t GetNumVertices(void) const;
			size_t GetNumFaces(void) const;
			size_t GetNumSubsets(void) const;
			unsigned int GetVerticesPerFace(void) const;
			const float* GetData(void) const;
			const unsigned int* GetFaceData(void) const;
			const unsigned int* GetSubsetData(void) const;
			MeshOptions GetOptions(void) const;
			unsigned int GetNumOptions(void) const;
			size_t DataBufferSize(void) const;
			size_t FaceBufferSize(void) const;
			size_t SubsetBufferSize(size_t subset) const;
			size_t VertexSize(void) const;
			size_t NumIndices(void) const;
			const MeshSkeleton* GetSkeleton(void) const;
			bool IsValid(void) const;

			bool HasOptions(MeshOptions options) const;

		private:

			MeshOptions m_options;
			unsigned int m_numOptions, m_numPerFace;
			size_t m_numVertices, m_numFaces, m_numSubsets;

			float* m_pData;
			unsigned int* m_pFaceData, *m_pSubsetData;
			MeshSkeleton* m_pSkeleton;
		};

	}
}
