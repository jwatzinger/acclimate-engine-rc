#include "MeshFileBinaryLoader.h"
#include <fstream>
#include "MeshFile.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace gfx
	{

		MeshFile MeshFileBinaryLoader::Load(std::fstream& stream) const
		{
			/*******************************************
			* Read vertex description
			*******************************************/

			// vertex count
			size_t numVertices;
			stream.read((char*)&numVertices, sizeof(size_t));

			// option count
			unsigned int numOptions;
			stream.read((char*)&numOptions, sizeof(unsigned int));
			
			// options
			MeshOptions meshOptions;
			stream.read((char*)&meshOptions, sizeof(unsigned int));
			
			/*******************************************
			* Read vertices
			*******************************************/	

			// vertex data
			float* pData = new float[numOptions*numVertices];
			stream.read((char*)pData, sizeof(float)*numOptions*numVertices);

			/*******************************************
			* Read faces
			*******************************************/

			// face count
			size_t numFaces;
			stream.read((char*)&numFaces, sizeof(size_t));

			// per face count
			unsigned short numPerFace;
			stream.read((char*)&numPerFace, sizeof(unsigned short));

			// read face data
			unsigned int* pFaceData = new unsigned int[numPerFace*numFaces];
			stream.read((char*)pFaceData, sizeof(unsigned int)*numPerFace*numFaces);

			char c;
			stream.get(c);
			if(c != 'N')
			{
				stream.close();

				// TODO: skeleton
				return MeshFile(meshOptions, numOptions, numVertices, pData, numPerFace, numFaces, pFaceData, nullptr);
			}

			// subset count
			size_t numSubsets;
			stream.read((char*)&numSubsets, sizeof(size_t));

			// read subset data
			unsigned int* pSubsetData = new unsigned int[numSubsets];
			stream.read((char*)pSubsetData, sizeof(unsigned int)*numSubsets);

			stream.close();

			// TODO: skeleton
			return MeshFile(meshOptions, numOptions, numVertices, pData, numPerFace, numFaces, pFaceData, numSubsets, pSubsetData, nullptr);
		}

	}
}