#pragma once
#include <string>
#include "MeshFile.h"

namespace acl
{
	namespace gfx
	{
		
		class MeshFileBinaryLoader
		{
		public:

			MeshFile Load(std::fstream& stream) const;
		};

	}
}
