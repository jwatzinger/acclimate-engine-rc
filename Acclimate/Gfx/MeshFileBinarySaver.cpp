#include "MeshFileBinarySaver.h"
#include <fstream>
#include "MeshFile.h"

namespace acl
{
	namespace gfx
	{

		void MeshFileBinarySaver::Save(const MeshFile& mesh, const std::wstring& stFilename) const
		{
			std::fstream stream;
			stream.open(stFilename, std::ios::out | std::ios::binary);

			stream.write("Acclimate mesh file 286793", 26); // file header + magic number

			/*******************************************
			* Write vertices
			*******************************************/
			
			// write vertex count
			const size_t numVertices = mesh.GetNumVertices();
			stream.write((char*)&numVertices, sizeof(size_t));

			// write option count
			const unsigned int numOptions = mesh.GetNumOptions();
			stream.write((char*)&numOptions, sizeof(unsigned int));

			// write options
			const unsigned int options = mesh.GetOptions();
			stream.write((char*)&options, sizeof(unsigned int));

			// write vertex data
			const float* pData = mesh.GetData();
			stream.write((char*)pData, sizeof(float)*numOptions*numVertices);

			/*******************************************
			* Write faces
			*******************************************/

			// write face count
			const size_t numFaces = mesh.GetNumFaces();
			stream.write((char*)&numFaces, sizeof(size_t));

			// write per face count
			const unsigned short numPerFace = mesh.GetVerticesPerFace();
			stream.write((char*)&numPerFace, sizeof(unsigned short));

			// write face data
			const unsigned int* pFaceData = mesh.GetFaceData();
			stream.write((char*)pFaceData, sizeof(unsigned int)*numPerFace*numFaces);

			/*******************************************
			* Write subsets 
			*******************************************/

			// write subset count
			const size_t numSubsets = mesh.GetNumSubsets();

			if(numSubsets > 1)
			{
				stream.put('N');
				stream.write((char*)&numSubsets, sizeof(size_t));

				// write subset data
				const unsigned int* pSubsetData = mesh.GetSubsetData();
				stream.write((char*)pSubsetData, sizeof(unsigned int)*numSubsets);
			}

			stream.close();
		}

	}
}