#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{
		
		class MeshFile;

		class ACCLIMATE_API MeshFileBinarySaver
		{
		public:
			
			void Save(const MeshFile& mesh, const std::wstring& stFilename) const;
		};

	}
}
