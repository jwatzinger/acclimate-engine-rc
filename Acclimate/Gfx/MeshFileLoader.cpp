#include "MeshFileLoader.h"
#include <fstream>
#include "MeshFilePlainLoader.h"
#include "MeshFileBinaryLoader.h"
#include "..\System\Exception.h"

namespace acl
{
	namespace gfx
	{

		MeshFile MeshFileLoader::Load(const std::wstring& stFilename) const
		{
			std::fstream stream;
			stream.open(stFilename, std::ios::in | std::ios::binary);

			if(!stream.is_open())
				throw fileException();
			
			/*******************************************
			* Read header line
			*******************************************/

			char firstLine[20];
			stream.read(firstLine, 20);
			firstLine[19] = '\0';

			if(!strcmp(firstLine,"Acclimate mesh file")) // file description + magic number
			{
				char magicNumber[7];
				stream.read(magicNumber, 6);
				magicNumber[6] = '\0';

				if(!strcmp(magicNumber, "175682"))
				{
					MeshFilePlainLoader loader;
					stream.close(); // todo: better solution
					stream.open(stFilename, std::ios::in);
					char temp[26];
					stream.read(temp, 26);
					return loader.Load(stream);
				}
				else if(!strcmp(magicNumber, "286793"))
				{
					MeshFileBinaryLoader loader;
					return loader.Load(stream);
				}
			}

			throw fileException();
		}

	}
}