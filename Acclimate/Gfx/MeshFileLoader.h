#pragma once
#include <string>
#include "MeshFile.h"

namespace acl
{
	namespace gfx
	{

		class MeshFileLoader
		{
		public:

			MeshFile Load(const std::wstring& stFilename) const;
		};

	}
}
