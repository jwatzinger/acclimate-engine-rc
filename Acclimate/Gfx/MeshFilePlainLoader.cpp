#include "MeshFilePlainLoader.h"
#include <fstream>
#include <set>
#include "MeshFile.h"
#include "MeshSkeleton.h"
#include "..\System\Exception.h"
#include "..\System\Convert.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		void traverseSkeleton(Bone& node, unsigned int id, const std::vector<math::Matrix>& vMatrices, const std::vector<std::vector<unsigned int>>& vChilds)
		{
			node.mRelative = vMatrices[id];

			for(auto childId : vChilds[id])
			{
				node.m_vChildren.resize(node.m_vChildren.size() + 1);
				auto& child = *node.m_vChildren.rbegin();
				traverseSkeleton(child, childId, vMatrices, vChilds);
			}
		}

		MeshSkeleton* ReadSkeleton(std::fstream& stream)
		{
			Bone root;

			std::vector<math::Matrix> vMatrices;
			std::vector<std::vector<unsigned int>> vChilds;

			std::string stNumBones;
			char c;

			stream.get(c);
			ACL_ASSERT(c == ' ');

			stream.get(c);
			while(c != '\n')
			{
				stNumBones += c;
				stream.get(c);
			}

			unsigned int numBones;
			conv::FromString(&numBones, stNumBones.c_str());

			for(unsigned int i = 0; i < numBones; i++)
			{
				math::Matrix matrix;
				for(unsigned int m = 0; m < 4; m++)
				{
					for(unsigned int n = 0; n < 4; n++)
					{
						std::string stValue;
						stream.get(c);
						while(c != ' ' && c != '\n')
						{
							stValue += c;
							stream.get(c);
						}

						conv::FromString(&matrix.m[m][n], stValue.c_str());
					}
				}

				vMatrices.push_back(matrix);

				stream.get(c);
				ACL_ASSERT(c == '\n');

				std::string stNumChilds;
				stream.get(c);
				while(c != ' ' && c != '\n')
				{
					stNumChilds += c;
					stream.get(c);
				}

				unsigned int numChilds;
				conv::FromString(&numChilds, stNumChilds.c_str());

				vChilds.resize(vChilds.size()+1);
				auto& vChildIds = *vChilds.rbegin();
				for(unsigned int j = 0; j < numChilds; j++)
				{
					std::string stChild;
					stream.get(c);
					while(c != ' ' && c != '\n')
					{
						stChild += c;
						stream.get(c);
					}

					unsigned int child;
					conv::FromString(&child, stChild.c_str());

					vChildIds.push_back(child);
				}

				stream.get(c);
				ACL_ASSERT(c == '\n');
			}

			traverseSkeleton(root, 0, vMatrices, vChilds);

			std::string stNumOrder;
			stream.get(c);
			while(c != ' ' && c != '\n')
			{
				stNumOrder += c;
				stream.get(c);
			}

			const unsigned int numOrder = conv::FromString<unsigned int>(stNumOrder);

			MeshSkeleton::OrderVector vOrder;

			for(unsigned int i = 0; i < numOrder; i++)
			{
				std::string stOrder;
				stream.get(c);
				while(c != ' ' && c != '\n')
				{
					stOrder += c;
					stream.get(c);
				}

				const unsigned int order = conv::FromString<unsigned int>(stOrder);
				vOrder.push_back(order);
			}

			MeshSkeleton::MatrixVector vInvBindPoses;

			for(unsigned int i = 0; i < numOrder; i++)
			{
				math::Matrix matrix;
				for(unsigned int m = 0; m < 4; m++)
				{
					for(unsigned int n = 0; n < 4; n++)
					{
						std::string stValue;
						stream.get(c);
						while(c != ' ' && c != '\n')
						{
							stValue += c;
							stream.get(c);
						}

						conv::FromString(&matrix.m[m][n], stValue.c_str());
					}
				}

				vInvBindPoses.push_back(matrix);

				stream.get(c);
				ACL_ASSERT(i == numOrder-1 || c == '\n');
			}

			return new MeshSkeleton(root, vOrder, vInvBindPoses);
		}

		MeshFile MeshFilePlainLoader::Load(std::fstream& stream) const
		{
			/*******************************************
			* Read vertex description
			*******************************************/	
			
			char c;
			stream.get(c);
			stream.get(c);
			if(c != '\n') // next is an empty line
				throw fileException();

			// read vertex keyword
			char word[9];
			stream.read(word, 9);
			word[8] = '\0';

			if(strcmp(word,"Vertices"))
				throw fileException();

			// next is vertex count
			std::string stNumVertices;
			stream >> stNumVertices;

			size_t numVertices;
			conv::FromString(&numVertices, stNumVertices.c_str()); 

			// next is mesh options (position, uv, ...)
			unsigned int meshOptions = 0;
			unsigned int numOptions = 0;

			std::set<char> optionSet;
			stream.get(c);
			unsigned int indexPos = 999, tempPos = 0;
			while(c != '\n')
			{
				if(c != ' ')
				{
					if(optionSet.find(c) != optionSet.end())
						throw fileException();

					optionSet.insert(c);
				}

				switch(c)
				{
				case 'X':
					meshOptions |= POS_X;
					numOptions++;
					tempPos += 1;
					break;
				case 'Y':
					meshOptions |= POS_Y;
					numOptions++;
					tempPos += 1;
					break;
				case 'Z':
					meshOptions |= POS_Z;
					numOptions++;
					tempPos += 1;
					break;
				case 'U':
					meshOptions |= TEX_U;
					numOptions++;
					tempPos += 1;
					break;
				case 'V':
					meshOptions |= TEX_V;
					numOptions++;
					tempPos += 1;
					break;
				case 'N':
					meshOptions |= NRM;
					numOptions+=3;
					tempPos += 3;
					break;
				case 'T':
					meshOptions |= TAN;
					numOptions+=3;
					tempPos += 3;
					break;
				case 'B':
					meshOptions |= BIN;
					numOptions+=3;
					tempPos += 3;
					break;
				case 'W':
					meshOptions |= WEIGHTS;
					numOptions += 4;
					tempPos += 4;
					break;
				case 'I':
					meshOptions |= INDICES;
					numOptions += 1;
					indexPos = tempPos;
					break;
				case ' ':
					break;
				default: // no other chars allowed
					throw fileException();
					break;
				}
				stream.get(c);
			}

			/*******************************************
			* Read vertices
			*******************************************/	

			unsigned int vertex = 0;
			float* pData = new float[numOptions*numVertices];

// string-less implementation is about 100% faster in debug mode, but about the same time slower in release
// so we pick the best fit - this will make running in debug mode a lot easier
#ifdef _DEBUG 
			char cOptions[32];
			stream.get(c);
			while(vertex < numVertices)
			{
				ACL_ASSERT(c != ' ' && c != '\n');

				unsigned int options = 0;
				while(options < numOptions)
				{
					if(options == indexPos)
					{
						unsigned char indices[4];
						for(unsigned int i = 0; i < 4; i++)
						{
							unsigned int digit = 0;
							while(c != ' ' && c != '\n')
							{
								cOptions[digit] = c;
								stream.get(c);
								digit++;
							}
							cOptions[digit] = '\0';

							ACL_ASSERT(c != '\n' || options == numOptions + 4);

							conv::FromString(&indices[i], cOptions);

							stream.get(c);
						}

						pData[options + numOptions*vertex] = *(float*)indices;
					}
					else
					{
						unsigned int digit = 0;
						while(c != ' ' && c != '\n')
						{
							cOptions[digit] = c;
							stream.get(c);
							digit++;
						}
						cOptions[digit] = '\0';

						ACL_ASSERT(c != '\n' || options == numOptions - 1);

						float dat;
						conv::FromString(&dat, cOptions);
						pData[options + numOptions*vertex] = dat;
					}


					options++;
					
					stream.get(c);
				}

				vertex++;
			}
#else 
			std::string stOption;
			while(vertex < numVertices)
			{
				unsigned int options = 0;
				while(options < numOptions)
				{
					if(options == indexPos)
					{
						unsigned char indices[4];
						for(unsigned int i = 0; i < 4; i++)
						{
							stream >> stOption;
							conv::FromString(&indices[i], stOption.c_str());
						}

						pData[options + numOptions*vertex] = *(float*)indices;

						stream.get(c);
					}
					else
					{
						stream >> stOption;

						//ACL_ASSERT(options == numOptions - 1);

						float dat;
						conv::FromString(&dat, stOption.c_str());
						pData[options + numOptions*vertex] = dat;
					}

					options++;
				}

				vertex++;
			}

			stream.get(c);
			stream.get(c);
#endif

			/*******************************************
			* Read faces
			*******************************************/

			if(c != '\n') // next is an empty line
			{
				delete[] pData;
				throw fileException();
			}

			// read face keyword
			char faceWord[6];
			stream.read(faceWord, 6);
			faceWord[5] = '\0';

			if(strcmp(faceWord, "Faces"))
			{
				delete[] pData;
				throw fileException();
			}

			// read face count
			std::string stFaceCount;
			stream >> stFaceCount;
			
			size_t numFaces;
			conv::FromString(&numFaces, stFaceCount.c_str());

			// read per face count
			std::string stPerFaceCount;
			stream >> stPerFaceCount;

			unsigned int numPerFace;
			conv::FromString(&numPerFace, stPerFaceCount.c_str());

			unsigned int face = 0;
			unsigned int* pFaceData = new unsigned int[numPerFace*numFaces];

			// read face data
			stream.get(c);
			std::string stFace;
			while(face < numFaces)
			{
				unsigned int faces = 0;
				while(faces < numPerFace)
				{
					stream >> stFace;

					unsigned int dat;
					conv::FromString(&dat, stFace.c_str());
					pFaceData[faces + numPerFace*face] = dat;

					faces++;
				}

				face++;
			}

			stream.get(c);
			stream.get(c); // eliminate \n's

			/*******************************************
			* Read subsets
			*******************************************/

			// read subset definition
			char subsetWord[8];
			stream.read(subsetWord, 8);
			subsetWord[7] = '\0';

			if(strcmp(subsetWord, "Subsets"))
			{
				MeshSkeleton* pSkeleton = nullptr;
				if(!strcmp(subsetWord, "Skeleto"))
					pSkeleton = ReadSkeleton(stream);
				
				stream.close();

				return MeshFile((MeshOptions)meshOptions, numOptions, numVertices, pData, numPerFace, numFaces, pFaceData, pSkeleton);
			}
			
			// read subset count
			std::string stSubsetCount;
			stream >> stSubsetCount;

			unsigned int subsetCount;
			conv::FromString(&subsetCount, stSubsetCount.c_str());

			unsigned int* pSubsetData = new unsigned int[subsetCount];

			unsigned int subset = 0;

			// read subset data
			while(subset < subsetCount)
			{
				std::string stSubsetFaceCount;
				stream >> stSubsetFaceCount;

				unsigned int subsetFaceCount;
				conv::FromString(&subsetFaceCount, stSubsetFaceCount.c_str()); 

				pSubsetData[subset++] = subsetFaceCount;
			}

			// skeleton
			std::string stSkeleton;
			stream >> stSkeleton;

			MeshSkeleton* pSkeleton = nullptr;
			if(stSkeleton == "Skeleton")
				pSkeleton = ReadSkeleton(stream);

			stream.close();

			return MeshFile((MeshOptions)meshOptions, numOptions, numVertices, pData, numPerFace, numFaces, pFaceData, subsetCount, pSubsetData, pSkeleton);

		}

	}
}