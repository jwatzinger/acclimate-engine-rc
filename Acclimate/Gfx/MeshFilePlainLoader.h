#pragma once
#include <string>
#include "MeshFile.h"

namespace acl
{
	namespace gfx
	{
		
		class MeshFilePlainLoader
		{
		public:

			MeshFile Load(std::fstream& stream) const;
		};

	}
}
