#include "MeshFileSaver.h"
#include <fstream>
#include "MeshFile.h"
#include "MeshSkeleton.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gfx
	{

		void HelpWrite(char c, std::fstream& stream)
		{
			stream.put(c);
			stream.put(' ');
		}

		void traverseSkeleton(const Bone& bone, unsigned int& bones, std::vector<math::Matrix>& vMatrices, std::vector<std::vector<unsigned int>>& vChilds)
		{
			bones++;
			vMatrices.push_back(bone.mRelative);

			const size_t id = vChilds.size();
			vChilds.resize(id + 1);
			for(auto& child : bone.m_vChildren)
			{
				vChilds[id].push_back(bones);
				traverseSkeleton(child, bones, vMatrices, vChilds);
			}
		}

		void MeshFileSaver::Save(const MeshFile& mesh, const std::wstring& stFilename) const
		{
			std::fstream stream;
			stream.open(stFilename, std::ios::out);

			stream.write("Acclimate mesh file 175682", 26); // file header + magic number
			stream.put('\n');
			stream.put('\n'); // first empty line

			/*******************************************
			* Write vertices
			*******************************************/

			stream.write("Vertices ", 9);
			
			// vertex count
			const size_t numVertices = mesh.GetNumVertices();
			const std::string stVertexCount = conv::ToStringA(numVertices) + " ";
			stream.write(stVertexCount.c_str(), stVertexCount.size());

			// vertex options
			unsigned int indexPos = 999, tempPos = 0;
			const unsigned int options = mesh.GetOptions();
			if(options & POS_X)
			{
				HelpWrite('X', stream);
				tempPos += 1;
			}
			if(options & POS_Y)
			{
				HelpWrite('Y', stream);
				tempPos += 1;
			}
			if(options & POS_Z)
			{
				HelpWrite('Z', stream);
				tempPos += 1;
			}	
			if(options & TEX_U)
			{
				HelpWrite('U', stream);
				tempPos += 1;
			}
			if(options & TEX_V)
			{
				HelpWrite('V', stream);
				tempPos += 1;
			}
			if(options & NRM)
			{
				HelpWrite('N', stream);
				tempPos += 3;
			}
			if(options & TAN)
			{
				HelpWrite('T', stream);
				tempPos += 3;
			}
			if(options & BIN)
			{
				HelpWrite('B', stream);
				tempPos += 3;
			}
			if(options & WEIGHTS)
			{
				HelpWrite('W', stream);
				tempPos += 4;
			}
			if(options & INDICES)
			{
				HelpWrite('I', stream);
				indexPos = tempPos;
			}

			stream.put('\n'); // new line

			// write vertices
			const unsigned int numOptions = mesh.GetNumOptions();
			const float* pData = mesh.GetData();

			size_t vertex = 0;
			while(vertex < numVertices)
			{
				size_t attribute = 0;
				while(attribute < numOptions)
				{
					if(attribute == indexPos)
					{
						unsigned char* pIndices = (unsigned char*)&pData[vertex*numOptions + attribute];
						for(unsigned int i = 0; i < 4; i++)
						{
							const std::string stAttribute = conv::ToStringA(pIndices[i]) + ' ';
							stream.write(stAttribute.c_str(), stAttribute.size());
						}
					}
					else
					{
						const std::string stAttribute = conv::ToStringA(pData[vertex*numOptions + attribute], 5);
						stream.write(stAttribute.c_str(), stAttribute.size());
					}

					attribute++;

					if(attribute < numOptions)
						stream.put(' ');
				}

				stream.put('\n');

				vertex++;
			}

			/*******************************************
			* Write faces
			*******************************************/

			stream.put('\n'); // empty line

			stream.write("Faces ", 6);
			
			// number of faces
			const size_t numFaces = mesh.GetNumFaces();
			const std::string stNumFaces = conv::ToStringA(numFaces) + " ";
			stream.write(stNumFaces.c_str(), stNumFaces.size());

			// vertices per face
			const unsigned int numPerFace = mesh.GetVerticesPerFace();
			const std::string stNumPerFace = conv::ToStringA(numPerFace);
			stream.write(stNumPerFace.c_str(), stNumPerFace.size());
			
			stream.put('\n'); // new line

			const unsigned int* pFaceData = mesh.GetFaceData();

			unsigned int face = 0;
			while(face < numFaces)
			{
				unsigned int vertex = 0;
				while(vertex < numPerFace)
				{
					const std::string stIndex = conv::ToStringA(pFaceData[face*numPerFace + vertex]);
					stream.write(stIndex.c_str(), stIndex.size());
					vertex++;

					if(vertex <numPerFace)
						stream.put(' ');
				}
				face++;

				if(face < numFaces)
					stream.put('\n');
			}

			/*******************************************
			* Write subsets 
			*******************************************/

			const size_t numSubsets = mesh.GetNumSubsets();

			if(numSubsets > 1)
			{
				stream.put('\n');
				stream.put('\n');
				//subset word
				stream.write("Subsets ", 8);

				// number of subsets
				std::string stNumSubsets = conv::ToStringA(numSubsets);
				stream.write(stNumSubsets.c_str(), stNumSubsets.size());
				stream.put('\n');

				const unsigned int* pSubsetData = mesh.GetSubsetData();
				for(size_t i = 0; i < numSubsets; i++)
				{
					// subset data
					std::string stSubset = conv::ToStringA(pSubsetData[i]);
					stream.write(stSubset.c_str(), stSubset.size());

					if(i < numSubsets-1)
						stream.put('\n');
				}
			}

			/*******************************************
			* Write skeleton
			*******************************************/

			const auto pSkeleton = mesh.GetSkeleton();

			if(pSkeleton)
			{
				stream.put('\n');
				stream.put('\n');
				stream.write("Skeleton ", 9);

				auto& root = pSkeleton->GetRoot();
				std::vector<math::Matrix> vMatrices;
				std::vector<std::vector<unsigned int>> vChilds;
				unsigned int numBones = 0;
				traverseSkeleton(root, numBones, vMatrices, vChilds);

				// number of bones
				std::string stNumBones = conv::ToStringA(numBones);
				stream.write(stNumBones.c_str(), stNumBones.size());
				stream.put('\n');

				for(unsigned int i = 0; i < numBones; i++)
				{
					const auto& matrix = vMatrices[i];
					for(unsigned int m = 0; m < 4; m++)
					{
						for(unsigned int n = 0; n < 4; n++)
						{
							const std::string stMatrixValue = conv::ToStringA(matrix.m[m][n]);
							stream.write(stMatrixValue.c_str(), stMatrixValue.size());
							stream.put(' ');
						}
					}
					stream.put('\n');

					const size_t numChilds = vChilds[i].size();
					const std::string stNumChild = conv::ToStringA(numChilds);
					stream.write(stNumChild.c_str(), stNumChild.size());
					stream.put(' ');

					for(auto child : vChilds[i])
					{
						const std::string stChild = conv::ToStringA(child);
						stream.write(stChild.c_str(), stChild.size());
						stream.put(' ');
					}

					stream.put('\n');
				}

				// write bone order
				const auto& vOrder = pSkeleton->GetOrder();
				const std::string stNumOrder = conv::ToStringA(vOrder.size());
				stream.write(stNumOrder.c_str(), stNumOrder.size());
				stream.put(' ');

				unsigned int i = 0;
				for(auto order : vOrder)
				{
					const std::string stOrder = conv::ToStringA(order);
					stream.write(stOrder.c_str(), stOrder.size());
					if(i < vOrder.size()-1)
					{
						stream.put(' ');
						i++;
					}
				}

				stream.put('\n');

				// write bind pose matrices
				const auto& vInvBindPoses = pSkeleton->GetInvBindPoses();
				for(auto& mInvBindPose : vInvBindPoses)
				{
					for(unsigned int m = 0; m < 4; m++)
					{
						for(unsigned int n = 0; n < 4; n++)
						{
							const std::string stMatrixValue = conv::ToStringA(mInvBindPose.m[m][n]);
							stream.write(stMatrixValue.c_str(), stMatrixValue.size());
							stream.put(' ');
						}
					}
					stream.put('\n');
				}
			}
			
			stream.close();
			
		}

	}
}