#include "MeshSkeleton.h"
#include "..\Math\Vector3.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		MeshSkeleton::MeshSkeleton(const Bone& root, const OrderVector& vOrder, const MatrixVector& vInvBindPoses) : m_root(root),
			m_vOrder(vOrder), m_vInvBindPoses(vInvBindPoses)
		{
			ProcessBone(m_root);
		}

		MeshSkeleton::MeshSkeleton(const MeshSkeleton& skeleton) : MeshSkeleton(skeleton.m_root, skeleton.m_vOrder, skeleton.m_vInvBindPoses)
		{
		}

		void MeshSkeleton::SetTransform(unsigned int bone, const math::Matrix& matrix)
		{
			ACL_ASSERT(bone < m_vBones.size());
			m_vBones[bone]->mRelative = matrix;
		}

		const MeshSkeleton::MatrixVector& MeshSkeleton::GetMatrices(void) const
		{
			return m_vOrderedMatrices;
		}

		const MeshSkeleton::MatrixVector& MeshSkeleton::GetInvBindPoses(void) const
		{
			return m_vInvBindPoses;
		}

		const Bone& MeshSkeleton::GetRoot(void) const
		{
			return m_root;
		}

		const MeshSkeleton::OrderVector& MeshSkeleton::GetOrder(void) const
		{
			return m_vOrder;
		}

		void MeshSkeleton::SetupMatrices(void)
		{
			ProcessSkeleton();
		}

		void MeshSkeleton::Update(void)
		{
			ProcessSkeleton();
			SortMatrices();
		}

		void MeshSkeleton::ProcessSkeleton(void)
		{
			m_vMatrices.clear();
			size_t numBone = 0;

			ProcessBone(m_root, nullptr, numBone);
		}

		void MeshSkeleton::ProcessBone(Bone& bone, Bone* pParent, size_t& numBone)
		{
			if(pParent)
				bone.mAbsolute = bone.mRelative * pParent->mAbsolute;
			else
				bone.mAbsolute = bone.mRelative;

			m_vMatrices.push_back(bone.mAbsolute);

			numBone++;

			for(auto& child : bone.m_vChildren)
			{
				ProcessBone(child, &bone, numBone);
			}
		}

		void MeshSkeleton::CalculateBindPoses(void)
		{
			m_vInvBindPoses.clear();
			CalculateBindPose(m_root);
		}

		void MeshSkeleton::CalculateBindPose(Bone& bone)
		{
			m_vInvBindPoses.push_back(bone.mAbsolute.inverse());

			for(auto& child : bone.m_vChildren)
			{
				CalculateBindPose(child);
			}
		}

		void MeshSkeleton::SortMatrices(void)
		{
			m_vOrderedMatrices.clear();

			unsigned int i = 0;
			for(auto id : m_vOrder)
			{
				m_vOrderedMatrices.push_back(m_vInvBindPoses[i++] * m_vMatrices[id]);
			}
		}

		void MeshSkeleton::ProcessBone(Bone& bone)
		{
			m_vBones.push_back(&bone);

			for(auto& child : bone.m_vChildren)
			{
				ProcessBone(child);
			}
		}

	}
}

