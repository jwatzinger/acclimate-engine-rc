#pragma once
#include <vector>
#include "..\Core\Dll.h"
#include "..\Math\Matrix.h"

namespace acl
{
	namespace gfx
	{

		struct Bone
		{
			typedef std::vector<Bone> BoneVector;

			math::Matrix mRelative, mAbsolute;
			BoneVector m_vChildren;
		};

		class ACCLIMATE_API MeshSkeleton
		{
			typedef std::vector<Bone*> BoneVector;
		public:
			typedef std::vector<math::Matrix> MatrixVector;
			typedef std::vector<unsigned int> OrderVector;

			MeshSkeleton(const Bone& root, const OrderVector& vOrder, const MatrixVector& vInvBindPoses);
			MeshSkeleton(const MeshSkeleton& skeleton);

			void SetTransform(unsigned int bone, const math::Matrix& matrix);

			const MatrixVector& GetMatrices(void) const;
			const MatrixVector& GetInvBindPoses(void) const;
			const Bone& GetRoot(void) const;
			const OrderVector& GetOrder(void) const;

			void SetupMatrices(void);
			void Update(void);

		private:
#pragma warning( disable: 4251 )
			void ProcessSkeleton(void);
			void ProcessBone(Bone& bone, Bone* pParent, size_t& numBone);
			void CalculateBindPoses(void);
			void CalculateBindPose(Bone& bone);
			void SortMatrices(void);

			void ProcessBone(Bone& bone);

			Bone m_root;
			BoneVector m_vBones;
			MatrixVector m_vMatrices, m_vOrderedMatrices, m_vInvBindPoses;
			OrderVector m_vOrder;
#pragma warning( default: 4251 )
		};

	}
}


