#pragma once
#include <string>
#include "IMesh.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, IMesh> Meshes;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, IMesh>;

    }
}