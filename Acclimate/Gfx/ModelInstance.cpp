#include "ModelInstance.h"
#include "ICbuffer.h"
#include "IModel.h"
#include "Defines.h"
#include "..\System\Log.h"
#include "..\Render\IStage.h"

namespace acl
{
	namespace gfx
	{

		ModelInstance::ModelInstance(IModel& parent, ICbuffer* pVertexCbuffer, ICbuffer* pPixelCbuffer, ICbuffer* pGeometryCbuffer, bool isUniqueInstance) : m_pParent(&parent),
			m_depth(0.0f), m_bDirty(true), m_pVCbuffer(pVertexCbuffer), m_pPCbuffer(pPixelCbuffer), m_pGCbuffer(pGeometryCbuffer), m_isUniqueInstance(isUniqueInstance)
		{
			if(m_pVCbuffer)
				m_pVCbuffer->Bind(m_states, CbufferType::VERTEX, CbufferSlot::INSTANCE);

			if(m_pPCbuffer)
				m_pPCbuffer->Bind(m_states, CbufferType::PIXEL, CbufferSlot::INSTANCE);

#ifndef ACL_API_DX9
			if(m_pGCbuffer)
				m_pGCbuffer->Bind(m_states, CbufferType::GEOMETRY, CbufferSlot::INSTANCE);
#endif

			m_vInstances.reserve(MAX_MODEL_PASSES);
		}

		ModelInstance& ModelInstance::Clone(void) const
		{
			if(m_isUniqueInstance)
			{
				auto& newModel = m_pParent->Clone();
				return newModel.MakeUniqueInstance();
			}
			else
				return m_pParent->CreateInstance();
		}

		ModelInstance::~ModelInstance(void)
		{
			if(m_isUniqueInstance)
			{
				delete m_pParent;
			}

			delete m_pVCbuffer;
			delete m_pPCbuffer;
#ifndef ACL_API_DX9
			delete m_pGCbuffer;
#endif

			if(m_pParent)
				m_pParent->RemoveInstance(*this);
		}

		void ModelInstance::SetDepth(float depth)
		{
			m_depth = depth;
		}

		void ModelInstance::SetVertexConstant(unsigned int index, const float* constArray, size_t numConsts)
		{
			if(m_pVCbuffer)
				m_pVCbuffer->Write(index, constArray, numConsts);
		}

		void ModelInstance::SetPixelConstant(unsigned int index, const float* constArray, size_t numConsts)
		{
			if(m_pPCbuffer)
				m_pPCbuffer->Write(index, constArray, numConsts);
		}

		void ModelInstance::SetGeometryConstant(unsigned int index, const float* constArray, size_t numConsts)
		{
#ifdef ACL_API_DX9
			sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Failed to set geometry constant in model. Geometry shader is not supported in DX9 mode. Please check your render code and create a different implementation to support DX9.");
#else
			if(m_pGCbuffer)
				m_pGCbuffer->Write(index, constArray, numConsts);
#endif
		}

		void ModelInstance::SetParent(IModel& parent)
		{
			if(m_pParent != &parent)
			{
				if(m_isUniqueInstance)
					delete m_pParent;

				m_pParent->RemoveInstance(*this);
				m_pParent = &parent;
				m_bDirty = true;
			}
		}

		IModel* ModelInstance::GetParent(void) const
		{
			return m_pParent;
		}

		void ModelInstance::RefreshInstances(void)
		{
			if(m_pParent)
				m_pParent->GenerateInstances(m_vInstances, m_states);
			else
				m_vInstances.clear();
		}

		void ModelInstance::Draw(const render::IStage& stage, size_t pass)
		{
			if(pass >= MAX_MODEL_PASSES)
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "tried to draw model with invalid pass", pass, "(only up to", MAX_MODEL_PASSES, "supported)");
				return;
			}

			// TODO: this should not happen on rendering, for performance reasons and since it can introduces bugs up to crashes
			if(m_bDirty)
			{
				RefreshInstances();
				m_bDirty = false;
			}

			if(m_vInstances.size() > pass)
			{
				auto& vInstances = m_vInstances[pass];

				for(auto& instance : vInstances)
				{
					instance.sortKey.Depth = reinterpret_cast<int>(&m_depth);
					stage.SubmitInstance(instance);
				}
			}
		}

		void ModelInstance::OnParentDirty(const IModel* pModel, const ICbuffer* pVCbuffer, const ICbuffer* pPCbuffer, const ICbuffer* pGCbuffer)
		{
			// TODO: update cbuffers in case material changed
			if(!pModel)
				m_pParent = nullptr;
			else
			{
				if(pVCbuffer)
				{
					if(!m_pVCbuffer || pVCbuffer->GetSize() > m_pVCbuffer->GetSize())
					{
						delete m_pVCbuffer;
						m_pVCbuffer = &pVCbuffer->Clone();
						m_pVCbuffer->Bind(m_states, CbufferType::VERTEX, CbufferSlot::INSTANCE);
					}
				}
				else
				{
					// TODO: unbind buffer
					// delete m_pVCbuffer;
					// m_pVCbuffer = nullptr;
				}

				if(pPCbuffer)
				{
					if(!m_pPCbuffer || pPCbuffer->GetSize() > m_pPCbuffer->GetSize())
					{
						delete m_pPCbuffer;
						m_pPCbuffer = &pPCbuffer->Clone();
						m_pPCbuffer->Bind(m_states, CbufferType::PIXEL, CbufferSlot::INSTANCE);
					}
				}
				else
				{
					// TODO: unbind buffer
					//delete m_pPCbuffer;
					//m_pPCbuffer = nullptr;
				}

#ifndef ACL_API_DX9
				if(pGCbuffer)
				{
					if(!m_pGCbuffer || pGCbuffer->GetSize() > m_pGCbuffer->GetSize())
					{
						delete m_pGCbuffer;
						m_pGCbuffer = &pGCbuffer->Clone();
						m_pGCbuffer->Bind(m_states, CbufferType::GEOMETRY, CbufferSlot::INSTANCE);
					}
				}
				else
				{
					// TODO: unbind buffer
					// delete m_pVCbuffer;
					// m_pVCbuffer = nullptr;
				}
			}
#endif

			m_bDirty = true;
		}

	}
}

