#pragma once
#include <vector>
#include "..\Core\Dll.h"
#include "..\Render\StateGroup.h"
#include "..\Render\Instance.h"

namespace acl
{
	namespace render
	{
		class IStage;
	}

	namespace gfx
	{

		class ICbuffer;
		class IModel;
		
		class ACCLIMATE_API ModelInstance
		{
			typedef std::vector<std::vector<render::Instance>> InstanceVector;
		public:
			ModelInstance(IModel& parent, ICbuffer* pVertexCbuffer, ICbuffer* pPixelCbuffer, ICbuffer* pGeometryCbuffer, bool isUniqueInstance);
			~ModelInstance(void);

			ModelInstance& Clone(void) const;

			void SetDepth(float depth);
			void SetVertexConstant(unsigned int index, const float* constArray, size_t numConsts);
			void SetPixelConstant(unsigned int index, const float* constArray, size_t numConsts);
			void SetGeometryConstant(unsigned int index, const float* constArray, size_t numConsts);
			void SetParent(IModel& parent);

			IModel* GetParent(void) const;

			void Draw(const render::IStage& stage, size_t pass);

			void OnParentDirty(const IModel* pModel, const ICbuffer* pVCbuffer, const ICbuffer* pPCbuffer, const ICbuffer* pGCbuffer);

		private:

			ModelInstance(const ModelInstance& model) = delete;
			ModelInstance& operator=(const ModelInstance& model) = delete;
#pragma warning( disable: 4251 )
			void RefreshInstances(void);

			float m_depth;
			bool m_bDirty, m_isUniqueInstance;

			ICbuffer* m_pVCbuffer, *m_pPCbuffer, *m_pGCbuffer;
			IModel* m_pParent;

			InstanceVector m_vInstances;
			render::StateGroup m_states;
#pragma warning( default: 4251 )
		};

	}
}


