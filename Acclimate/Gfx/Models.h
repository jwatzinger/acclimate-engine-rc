#pragma once
#include <string>
#include "IModel.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, IModel> Models;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, IModel>;

    }
}