#pragma once
#include <string>
#include "..\Core\Resources.h"

namespace acl
{
    namespace particle
    {

        class Effect;
        typedef core::Resources<std::wstring, Effect> Particles;

    }
}