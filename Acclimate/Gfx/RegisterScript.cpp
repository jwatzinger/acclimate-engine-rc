#include "RegisterScript.h"
#include "Camera.h"
#include "Color.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace gfx
	{

		/*************************************
		* Color
		*************************************/

		void colorConstruct(void *memory)
		{
			new(memory)Color();
		}

		void colorConstruct(unsigned char r, unsigned char g, unsigned char b, void* memory)
		{
			new(memory)Color(r, g, b);
		}

		void colorConstruct(unsigned char r, unsigned char g, unsigned char b, unsigned char a, void* memory)
		{
			new(memory)Color(r, g, b, a);
		}

		void RegisterScript(script::Core& script)
		{
			// camera
			auto camera = script.RegisterReferenceType("Camera", script::REF_NOCOUNT);
			camera.RegisterMethod("void SetPosition(const Vector3& in)", asMETHODPR(Camera, SetPosition, (const math::Vector3&), void));
			camera.RegisterMethod("void SetPosition(float, float, float)", asMETHODPR(Camera, SetPosition, (float, float, float), void));
			camera.RegisterMethod("void SetLookAt(const Vector3& in)", asMETHODPR(Camera, SetLookAt, (const math::Vector3&), void));
			camera.RegisterMethod("void SetLookAt(float, float, float)", asMETHODPR(Camera, SetLookAt, (float, float, float), void));
			camera.RegisterMethod("void SetUp(const Vector3& in)", asMETHOD(Camera, SetUp));
			camera.RegisterMethod("void SetClipPlanes(float, float)", asMETHOD(Camera, SetClipPlanes));

			camera.RegisterMethod("const Vector3& GetPosition(void) const", asMETHOD(Camera, GetPosition));
			camera.RegisterMethod("Vector3 GetDirection(void) const", asMETHOD(Camera, GetDirection));
			camera.RegisterMethod("const Vector3& GetUp(void) const", asMETHOD(Camera, GetUp));
			camera.RegisterMethod("const Vector3& GetLookAt(void) const", asMETHOD(Camera, GetLookAt));
			camera.RegisterMethod("const Vector2i& GetScreenSize(void) const", asMETHOD(Camera, GetScreenSize));

			// color
			auto color = script.RegisterValueType<Color>("Color", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR);
			color.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(colorConstruct, (void*), void), asCALL_CDECL_OBJLAST);
			color.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(uint8, uint8, uint8)", asFUNCTIONPR(colorConstruct, (unsigned char, unsigned char, unsigned char, void*), void), asCALL_CDECL_OBJLAST);
			color.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(uint8, uint8,  uint8, uint8)", asFUNCTIONPR(colorConstruct, (unsigned char, unsigned char, unsigned char, unsigned char, void*), void), asCALL_CDECL_OBJLAST);
			color.RegisterProperty("uint8 r", asOFFSET(Color, r));
			color.RegisterProperty("uint8 g", asOFFSET(Color, g));
			color.RegisterProperty("uint8 b", asOFFSET(Color, b));
			color.RegisterProperty("uint8 a", asOFFSET(Color, a));
		}

	}
}
