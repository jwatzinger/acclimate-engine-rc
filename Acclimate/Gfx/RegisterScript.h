#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace gfx
	{

		void RegisterScript(script::Core& core);

	}
}