#include "RenderStateHandler.h"

namespace acl
{
	namespace gfx
	{

		void RenderStateHandler::SetInstance(const IInstance& instance)
		{
			m_pInstance = &instance;
		}

		void RenderStateHandler::UnbindTexture(render::StateGroup& states, TextureBindTarget target, unsigned int slot)
		{
			m_pInstance->OnUnbindTexture(states, target, slot);
		}

		const RenderStateHandler::IInstance* RenderStateHandler::m_pInstance = nullptr;

	}
}