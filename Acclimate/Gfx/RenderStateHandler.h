#pragma once

namespace acl
{
	namespace render
	{
		class StateGroup;
	}

	namespace gfx
	{

		enum class TextureBindTarget
		{
			PIXEL, VERTEX
		};

		class RenderStateHandler
		{
		public:

			class IInstance
			{
			public:

				virtual void OnUnbindTexture(render::StateGroup& states, TextureBindTarget target, unsigned int slot) const = 0;
			};

			static void SetInstance(const IInstance& instance);

			static void UnbindTexture(render::StateGroup& states, TextureBindTarget target, unsigned int slot);

		private:

			static const IInstance* m_pInstance;
		};

	}
}

