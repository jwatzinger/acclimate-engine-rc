#include "ResourceBlock.h"
#include "Context.h"

namespace acl
{
	namespace gfx
	{

		PathData::PathData(void)
		{
		}

		PathData::PathData(const std::wstring& stPath, const std::wstring& stTextures, const std::wstring& stEffects, const std::wstring& stMeshes) : stPath(stPath),
			stTextures(stTextures), stEffects(stEffects), stMeshes(stMeshes)
		{
		}

		void PathData::SetPath(const std::wstring& stValue)
		{
			stPath = stValue;
		}

		const std::wstring& PathData::GetPath(void) const
		{
			return stPath;
		}

		std::wstring PathData::GetTextures(void) const
		{
			return stPath + stTextures;
		}

		std::wstring PathData::GetEffects(void) const
		{
			return stPath + stEffects;
		}

		std::wstring PathData::GetMeshes(void) const
		{
			return stPath + stMeshes;
		}

		ResourceBlock::ResourceBlock(const ResourceContext& context) : m_textures(context.textures), m_effects(context.effects),
			m_materials(context.materials), m_meshes(context.meshes), m_models(context.models), m_zBuffers(context.zbuffers), m_paths()
		{
		}

		ResourceBlock::ResourceBlock(const ResourceContext& context, const PathData& data) : m_textures(context.textures, data.GetTextures()), m_effects(context.effects, data.GetEffects()),
			m_materials(context.materials), m_meshes(context.meshes, data.GetMeshes()), m_models(context.models), m_zBuffers(context.zbuffers), m_paths(data)
		{
		}

		void ResourceBlock::SetPath(const std::wstring& stPath)
		{
			if(m_paths.GetPath() != stPath)
			{
				m_paths.SetPath(stPath);

				m_textures.SetPath(m_paths.GetTextures());
				m_effects.SetPath(m_paths.GetEffects());
				m_meshes.SetPath(m_paths.GetMeshes());
			}
		}

		const std::wstring& ResourceBlock::GetPath(void) const
		{
			return m_paths.GetPath();
		}

		void ResourceBlock::Begin(void)
		{
			m_textures.Begin();
			m_effects.Begin();
			m_materials.Begin();
			m_meshes.Begin();
			m_models.Begin();
			m_zBuffers.Begin();
		}

		void ResourceBlock::Clear(void)
		{
			m_textures.Clear();
			m_models.Clear();
			m_materials.Clear();
			m_effects.Clear();
			m_meshes.Clear();
			m_zBuffers.Clear();
		}

		void ResourceBlock::End(void)
		{
			m_textures.End();
			m_effects.End();
			m_materials.End();
			m_meshes.End();
			m_models.End();
			m_zBuffers.End();
		}

		Textures::Block& ResourceBlock::Textures(void)
		{
			return m_textures;
		}

		Effects::Block& ResourceBlock::Effects(void)
		{
			return m_effects;
		}

		Materials::Block& ResourceBlock::Materials(void)
		{
			return m_materials;
		}

		Meshes::Block& ResourceBlock::Meshes(void)
		{
			return m_meshes;
		}

		Models::Block& ResourceBlock::Models(void)
		{
			return m_models;
		}

		const Textures::Block& ResourceBlock::Textures(void) const
		{
			return m_textures;
		}

		const Effects::Block& ResourceBlock::Effects(void) const
		{
			return m_effects;
		}

		const Materials::Block& ResourceBlock::Materials(void) const
		{
			return m_materials;
		}

		const Meshes::Block& ResourceBlock::Meshes(void) const
		{
			return m_meshes;
		}

		const Models::Block& ResourceBlock::Models(void) const
		{
			return m_models;
		}

	}
}

