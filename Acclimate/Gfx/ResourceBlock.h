#pragma once
#include "Textures.h"
#include "Materials.h"
#include "Meshes.h"
#include "ZBuffers.h"
#include "Models.h"
#include "Effects.h"

namespace acl
{
	namespace gfx
	{

		struct ACCLIMATE_API PathData
		{
			PathData(void);
			PathData(const std::wstring& stPath, const std::wstring& stTextures, const std::wstring& stEffects, const std::wstring& stMeshes);

			void SetPath(const std::wstring& stPath);

			const std::wstring& GetPath(void) const;
			std::wstring GetTextures(void) const;
			std::wstring GetEffects(void) const;
			std::wstring GetMeshes(void) const;

		private:

#pragma warning( disable: 4251 )
			std::wstring stPath;
			std::wstring stTextures, stEffects, stMeshes;
#pragma warning( default: 4251 )
		};

		struct ResourceContext;

		class ACCLIMATE_API ResourceBlock
		{
		public:
			ResourceBlock(const ResourceContext& context);
			ResourceBlock(const ResourceContext& context, const PathData& data);

			void SetPath(const std::wstring& stPath);

			const std::wstring& GetPath(void) const;

			void Begin(void);
			void Clear(void);
			void End(void);

			Textures::Block& Textures(void);
			Effects::Block& Effects(void);
			Materials::Block& Materials(void);
			Meshes::Block& Meshes(void);
			Models::Block& Models(void);

			const Textures::Block& Textures(void) const;
			const Effects::Block& Effects(void) const;
			const Materials::Block& Materials(void) const;
			const Meshes::Block& Meshes(void) const;
			const Models::Block& Models(void) const;

		private:
#pragma warning( disable: 4251 )

			PathData m_paths;

			Textures::Block m_textures;
			Effects::Block m_effects;
			Materials::Block m_materials;
			Meshes::Block m_meshes;
			Models::Block m_models;
			ZBuffers::Block m_zBuffers;
#pragma warning( default: 4251 )
		};

	}
}


