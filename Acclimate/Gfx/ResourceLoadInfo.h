#pragma once
#include <string>
#include <map>
#include <vector>
#include "TextureFormats.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{

		struct MaterialLoadInfo
		{
			std::wstring stName, stEffect;
			unsigned int permutation;
			std::vector<std::wstring> vTextures;
		};

		struct MeshLoadInfo
		{
			std::wstring stName, stPath;
		};

		struct ModelLoadInfo
		{
			std::wstring stName, stMesh;
			std::map<int, std::wstring> mMaterial;
		};

	}
} 