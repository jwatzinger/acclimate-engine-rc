#include "ResourceSaver.h"
#include "ResourceBlock.h"
#include "Context.h"
#include "..\File\File.h"
#include "..\System\Convert.h"
#include "..\System\Assert.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace gfx
	{

		ResourceSaver::ResourceSaver(const ResourceContext& resources) :
			m_pResources(&resources)
		{
		}

		void storeTexture(xml::Node& node, const ITexture& texture, const std::wstring& stPath)
		{
			auto& textureNode = node.InsertNode(L"Texture");

			auto pInfo = texture.GetLoadInfo();
			textureNode.SetValue(pInfo->stName);

			/*******************************
			* Format
			********************************/

			std::wstring stFormat(L"");
			switch(pInfo->format)
			{
			case TextureFormats::X32:
				stFormat = L"X32";
				break;
			case TextureFormats::A32:
				stFormat = L"A32";
				break;
			case TextureFormats::A16F:
				stFormat = L"A16F";
				break;
			case TextureFormats::L8:
				stFormat = L"L8";
				break;
			case TextureFormats::L16:
				stFormat = L"L16";
				break;
			case TextureFormats::R32:
				stFormat = L"R32";
				break;
			case TextureFormats::GR16F:
				stFormat = L"GR16F";
				break;
			case TextureFormats::R16F:
				stFormat = L"R16F";
				break;
			case TextureFormats::UNKNOWN:
				break;
			default:
				ACL_ASSERT(false);
			}

			if(!stFormat.empty())
				textureNode.ModifyAttribute(L"format", stFormat);

			/*******************************
			* File only
			********************************/
			if(pInfo->type == TextureLoadInfo::Type::FILE)
			{
				textureNode.ModifyAttribute(L"read", conv::ToString(pInfo->bRead));

				textureNode.ModifyAttribute(L"file", file::RelativeForSubPath(stPath, pInfo->stPath));
			}
			/*******************************
			* Created only
			********************************/
			else
			{
				textureNode.ModifyAttribute(L"read", conv::ToString(pInfo->bRead));

				textureNode.ModifyAttribute(L"flags", conv::ToString((unsigned int)pInfo->flags));

				// TODO: account for screen sized quads
				textureNode.ModifyAttribute(L"w", conv::ToString(pInfo->vSize.x));
				textureNode.ModifyAttribute(L"h", conv::ToString(pInfo->vSize.y));
			}
		}

		void storeMaterial(xml::Node& node, const IMaterial& material)
		{
			auto& materialNode = node.InsertNode(L"Material");

			auto pInfo = material.GetLoadInfo();
			materialNode.ModifyAttribute(L"name", pInfo->stName);
			materialNode.ModifyAttribute(L"perm", conv::ToString(pInfo->permutation));

			auto& effect = materialNode.InsertNode(L"Effect");
			effect.ModifyAttribute(L"name", pInfo->stEffect);

			for(auto& stTexture : pInfo->vTextures)
			{
				auto& texture = materialNode.InsertNode(L"Texture");
				texture.ModifyAttribute(L"name", stTexture);
			}
		}

		void storeMesh(xml::Node& node, const IMesh& mesh)
		{
			auto& meshNode = node.InsertNode(L"Mesh");
			auto pInfo = mesh.GetLoadInfo();

			meshNode.SetValue(pInfo->stName);
			meshNode.ModifyAttribute(L"type", L"file");

			meshNode.ModifyAttribute(L"file", pInfo->stPath);
		}

		void storeModel(xml::Node& node, const IModel& model)
		{
			auto& modelNode = node.InsertNode(L"Model");
			auto pInfo = model.GetLoadInfo();

			modelNode.ModifyAttribute(L"name", pInfo->stName);
			modelNode.ModifyAttribute(L"mesh", pInfo->stMesh);

			for(auto& material : pInfo->mMaterials)
			{
				auto& pass = modelNode.InsertNode(L"Pass");

				pass.ModifyAttribute(L"id", conv::ToString(material.first));
				pass.ModifyAttribute(L"material", material.second);
			}
		}

		void ResourceSaver::Save(const std::wstring& stFile) const
		{
			xml::Doc doc;
			auto& root = doc.InsertNode(L"Resources");

			// save textures
			auto& textures = m_pResources->textures.Map();
			if(!textures.empty())
			{
				auto& textureNode = root.InsertNode(L"Textures");
				for(auto texture : textures)
				{
					storeTexture(textureNode, *texture.second, L"");
				}
			}

			doc.SaveFile(stFile);
		}

		void ResourceSaver::Save(const std::wstring& stFile, const ResourceBlock& block) const
		{
			xml::Doc doc;
			auto& root = doc.InsertNode(L"Resources");

			sys::WorkingDirectory dir(stFile, true);

			const auto stPath = file::RelativePath(dir.GetDirectory(), block.GetPath());
			if(!stPath.empty())
				root.ModifyAttribute(L"path", stPath);

			// save textures
			auto& textures = block.Textures();
			if(auto numTextures = textures.Size())
			{
				auto& textureNode = root.InsertNode(L"Textures");

				auto& stTextures = file::RelativeForSubPath(stPath, textures.GetPath());
				if(!stTextures.empty() && stTextures != stPath)
					textureNode.ModifyAttribute(L"path", stTextures);

				for(unsigned int i = 0; i < numTextures; i++)
				{
					storeTexture(textureNode, *textures[i], file::FullPath(stPath + stTextures));
				}
			}

			// save materials
			auto& materials = block.Materials();
			if(auto numMaterials = materials.Size())
			{
				auto& materialNode = root.InsertNode(L"Materials");
				for(unsigned int i = 0; i < numMaterials; i++)
				{
					storeMaterial(materialNode, *materials[i]);
				}
			}

			// save meshes
			auto& meshes = block.Meshes();
			if(auto numMeshes = meshes.Size())
			{
				auto& meshNode = root.InsertNode(L"Meshes");
				for(unsigned int i = 0; i < numMeshes; i++)
				{
					storeMesh(meshNode, *meshes[i]);
				}
			}

			// save model
			auto& models = block.Models();
			if(auto numModels = models.Size())
			{
				auto& modelNode = root.InsertNode(L"Models");
				for(unsigned int i = 0; i < numModels; i++)
				{
					storeModel(modelNode, *models[i]);
				}
			}

			doc.SaveFile(stFile);
		}

	}
}
