#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{

		struct ResourceContext;
		class ResourceBlock;

		class ACCLIMATE_API ResourceSaver
		{
		public:
			ResourceSaver(const ResourceContext& resources);

			void Save(const std::wstring& stFile) const;
			void Save(const std::wstring& stFile, const ResourceBlock& block) const;

		private:

			const ResourceContext* m_pResources;
		};

	}
}


