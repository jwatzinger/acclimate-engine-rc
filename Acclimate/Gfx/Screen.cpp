#include "Screen.h"
#include "..\System\Log.h"

namespace acl
{
    namespace gfx
    {

		Screen::Screen(const VideoModeVector& vVideoModes, unsigned int activeMode) : m_vSize(vVideoModes[activeMode].vSize),
			m_rSize(0, 0, m_vSize.x, m_vSize.y), m_minZ(0.0f), m_maxZ(1.0f),
			m_vVideoModes(vVideoModes)
        {
        }

		void Screen::Resize(const math::Vector2& vSize)
		{
			if(!IsValidSize(vSize))
			{
				sys::log->Out(sys::LogModule::GFX, sys::LogType::WARNING, "Invalid screen size", vSize.x, "x", vSize.y, "specified.");
				return;
			}
				
			m_vSize = vSize;
			m_rSize.width = m_vSize.x;
			m_rSize.height = m_vSize.y;
		}

		bool Screen::IsValidSize(const math::Vector2& vSize) const
		{
			for(auto& videoMode : m_vVideoModes)
			{
				if(videoMode.vSize == vSize)
					return true;
			}

			return false;
		}

        const math::Vector2& Screen::GetSize(void) const
        {
	        return m_vSize;
        }

        const math::Rect& Screen::GetRect(void) const
        {
	        return m_rSize;
        }

        float Screen::GetMinZ(void) const
        {
            return m_minZ;
        }

        float Screen::GetMaxZ(void) const
        {
            return m_maxZ;
        }

		const Screen::VideoModeVector& Screen::GetVideoModes(void) const
		{
			return m_vVideoModes;
		}

    }
}