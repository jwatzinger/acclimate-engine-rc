#pragma once
#include <vector>
#include "VideoMode.h"
#include "..\Core\Dll.h"
#include "..\Math\Vector.h"
#include "..\Math\Rect.h"

namespace acl
{
    namespace gfx
    {

        class ACCLIMATE_API Screen
        {
        public:
			typedef std::vector<VideoMode> VideoModeVector;

			Screen(const VideoModeVector& vVideoModes, unsigned int activeMode);

			void Resize(const math::Vector2& vSize);
			bool IsValidSize(const math::Vector2& vSize) const;

	        const math::Vector2& GetSize(void) const;
	        const math::Rect& GetRect(void) const;
            float GetMinZ(void) const;
            float GetMaxZ(void) const;
			const VideoModeVector& GetVideoModes(void) const;

        private:
#pragma warning( disable: 4251 )
			float m_minZ, m_maxZ;

	        math::Vector2 m_vSize;
	        math::Rect m_rSize;

			VideoModeVector m_vVideoModes;
#pragma warning( default: 4251 )
        };

    }
}

