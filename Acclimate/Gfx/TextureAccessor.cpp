#include "TextureAccessor.h"
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		unsigned calculateIndex(unsigned int x, unsigned int y, unsigned int pitch, unsigned int height)
		{
#ifdef ACL_API_GL4
			return x + (height - 1 - y)*pitch;
#else
			return x + y*pitch;
#endif
		}

	}
}