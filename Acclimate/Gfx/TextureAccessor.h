#pragma once
#include "ITexture.h"
#include "..\Core\Dll.h"
#include "..\Math\Rect.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"

namespace acl
{
	namespace gfx
	{

		ACCLIMATE_API unsigned int calculateIndex(unsigned int x, unsigned int y, unsigned int pitch, unsigned int height);

		template<typename DataType, TextureFormats format>
		class TextureAccessor
		{
		public:
			TextureAccessor(ITexture& texture, bool bRead): m_pTexture(&texture), m_bRead(bRead),
				m_vSize(texture.GetSize())
			{
				ACL_ASSERT(texture.GetFormat() == format);

				if(!bRead)
					texture.Map(MapFlags::WRITE_DISCARD);
				else
					texture.Map(MapFlags::READ);

				m_pData = (DataType*)texture.GetData();
				m_pitch = texture.GetPitch()/sizeof(DataType);
			}

			~TextureAccessor(void)
			{
				if(m_pTexture)
					m_pTexture->Unmap();
			}

			inline void SetAt(unsigned int index, DataType value) const
			{
				ACL_ASSERT(!m_bRead);
				m_pData[index] = value;
			}

			inline void SetPixel(int x, int y, DataType value) const
			{
				ACL_ASSERT(!m_bRead);
				m_pData[calculateIndex(x, y, m_pitch, m_vSize.y)] = value;
			}

			inline void SetLine(int x, int y, const DataType& values, int width) const
			{
				ACL_ASSERT(!m_bRead);
				memcpy(&m_pData[calculateIndex(x, y, m_pitch, m_vSize.y)], &values, sizeof(DataType)*width);
			}

			inline DataType GetPixel(int x, int y) const
			{
				ACL_ASSERT(m_bRead);
				return m_pData[calculateIndex(x, y, m_pitch, m_vSize.y)];
			}

			void CopyRect(const TextureAccessor& target, const math::Rect& rSource, const math::Vector2& vDest)
			{
				ACL_ASSERT(m_bRead);
				ACL_ASSERT(!target.m_bRead);

				for(int j = 0; j < rSource.height; j++)
				{
					const unsigned int y = vDest.y + j;
					const unsigned int index = rSource.x + (rSource.y + j)*m_pitch;

					target.SetLine(vDest.x, y, m_pData[index], rSource.width);
				}
			}

			void Quit(void)
			{
				if(m_pTexture)
				{
					m_pTexture->Unmap();
					m_pTexture = nullptr;
					m_pData = nullptr;
				}
				else
					sys::log->Out("Warning: Texture acccessor quit called multiple times.");
			}
			
		private:

			DataType* m_pData;
			unsigned int m_pitch;
			bool m_bRead;
			math::Vector2 m_vSize;
			
			ITexture* m_pTexture;
		};

	}
}
