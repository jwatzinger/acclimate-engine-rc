#pragma once
#include "TextureAccessor.h"
#include "Color.h"

namespace acl
{
	namespace gfx
	{

		typedef TextureAccessor<unsigned char, TextureFormats::L8> TextureAccessorL8;
		typedef TextureAccessor<short, TextureFormats::L16> TextureAccessorL16;
		typedef TextureAccessor<float, TextureFormats::R32> TextureAccessorR32;
		typedef TextureAccessor<Color3, TextureFormats::X32> TextureAccessorX32;
		typedef TextureAccessor<Color, TextureFormats::A32> TextureAccessorA32;

	}
}
