#pragma once
#include "TextureDataProxy.h"
#include "Color.h"

namespace acl
{
	namespace gfx
	{

		typedef TextureDataProxy<unsigned char, TextureFormats::L8> TextureDataProxyL8;
		typedef TextureDataProxy<short, TextureFormats::L16> TextureDataProxyL16;
		typedef TextureDataProxy<float, TextureFormats::R32> TextureDataProxyR32;
		typedef TextureDataProxy<Color3, TextureFormats::X32> TextureDataProxyX32;
		typedef TextureDataProxy<Color, TextureFormats::A32> TextureDataProxyA32;

	}
}