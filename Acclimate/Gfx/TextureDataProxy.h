#pragma once
#include "TextureFormats.h"
#include "..\Math\Vector.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		template<typename Data, TextureFormats format>
		class TextureDataProxy
		{
		public:

			TextureDataProxy(const math::Vector2& vSize) : m_vSize(vSize)
			{
				m_pData = new Data[vSize.x * vSize.y];
			}
			
			TextureDataProxy(const TextureDataProxy& proxy) = delete;
			
			~TextureDataProxy(void)
			{
				delete m_pData;
			}

			const void* GetData(void) const
			{
				return m_pData;
			}

			void SetPixel(unsigned int x, unsigned int y, const Data& data)
			{
				ACL_ASSERT((int)x < m_vSize.x && (int)y < m_vSize.y);
				m_pData[y * m_vSize.x + x] = data;
			}

		private:

			math::Vector2 m_vSize;
			Data* m_pData;
		};

	}
}