#pragma once

namespace acl
{
	namespace gfx
	{

	    enum class TextureFormats
		{
			UNKNOWN = 0, X32, A32, A16F, L8, R32, GR16F, R16F, L16, A32F
        };

	}
}