#include "TextureInternalAccessor.h"
#include "ITexture.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		TextureInternalAccessor::TextureInternalAccessor(void) : m_pTexture(nullptr)
		{
		}

		void TextureInternalAccessor::SetTexture(AclTexture* pTexture)
		{
			m_pTexture = pTexture;
		}

		AclTexture* TextureInternalAccessor::GetTexture(void) const
		{
			return m_pTexture;
		}

		AclTexture& getTexture(const ITexture& texture)
		{
			TextureInternalAccessor accessor;
			texture.GetAclTexture(accessor);
			ACL_ASSERT(accessor.GetTexture());
			return *accessor.GetTexture();
		}

		AclTexture* getTexture(const ITexture* pTexture)
		{
			if(pTexture)
				return &getTexture(*pTexture);
			else
				return nullptr;
		}

	}
}

