#pragma once
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class ITexture;

		class TextureInternalAccessor
		{
		public:

			TextureInternalAccessor(void);

			void SetTexture(AclTexture* pTexture);
			AclTexture* GetTexture(void) const;

		private:

			AclTexture* m_pTexture;
		};

		AclTexture& getTexture(const ITexture& texture);
		AclTexture* getTexture(const ITexture* pTexture);

	}
}


