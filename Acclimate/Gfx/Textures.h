#pragma once
#include <string>
#include "ITexture.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, ITexture> Textures;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, ITexture>;

    }
}