#include "Utility.h"
#include "ISpriteBatch.h"
#include "Screen.h"
#include "Camera.h"
#include "..\Math\Ray.h"
#include "..\Math\Vector.h"
#include "..\Math\Vector3.h"
#include "..\Math\Vector4.h"
#include "..\Math\Matrix.h"

namespace acl
{
    namespace gfx
    {

        math::Ray ViewRayScreenPos(const math::Vector2& vScreenPos, const Camera& camera, const Screen& screen)
        {
			const math::Matrix& mProjection = camera.GetProjectionMatrix();
			// Compute the vector of the pick ray in screen space
			math::Vector3 v;
			v.x =  ( ( ( 2.0f * vScreenPos.x ) / screen.GetSize().x  ) - 1 ) / mProjection.m11;
			v.y = -( ( ( 2.0f * vScreenPos.y ) / screen.GetSize().y ) - 1 ) / mProjection.m22;
			v.z =  1.0f;

			// Get the inverse view matrix
			math::Matrix mWorldView = camera.GetViewMatrix();
			math::Matrix m = mWorldView.inverse();

			math::Vector3 vDir(	v.x*m.m11 + v.y*m.m21 + v.z*m.m31,
								v.x*m.m12 + v.y*m.m22 + v.z*m.m32,
								v.x*m.m13 + v.y*m.m23 + v.z*m.m33);

			math::Vector3 vOrigin( m.m41, m.m42, m.m43);
			// Transform the screen space pick ray into 3D space

	        return math::Ray(vOrigin, vDir);
        }

		math::Vector3 WorldToScreen(const math::Vector3& vWorldPos, const Camera& camera, const Screen& screen)
		{
	        math::Matrix mWorld;
			mWorld.Identity();

	        math::Vector3 vP1 = math::Vec3Project(vWorldPos, screen.GetRect(), camera.GetProjectionMatrix(), camera.GetViewMatrix(), mWorld); 

	        return vP1;
		}

		math::Matrix clipPlaneMatrix(const math::Matrix& mViewProjection, const math::Vector4& vPlane)
		{
			const math::Matrix mInvViewProjection = mViewProjection.inverse();
			const math::Matrix mInvTransposed = mInvViewProjection.transpose();

			math::Vector4 vProjClipPlane = mInvTransposed.TransformVector4(vPlane);

			if(vProjClipPlane.w > 0)
				vProjClipPlane = mInvTransposed.TransformVector4(-vPlane);

			vProjClipPlane.Normalize();

			math::Matrix mClipViewProj;
			mClipViewProj.Identity();

			mClipViewProj.m13 = vProjClipPlane.x;
			mClipViewProj.m23 = vProjClipPlane.y;
			mClipViewProj.m33 = vProjClipPlane.z;
			mClipViewProj.m43 = vProjClipPlane.w;

			return mViewProjection * mClipViewProj;
		}

		void fillRectangle(ISpriteBatch& batch, const math::Rect& rDest, const math::Rect& rSrc, const gfx::Color& color)
		{
			auto width = rDest.width;
			auto height = rDest.height;

			auto x = rDest.x;
			auto y = rDest.y;

			while(width > rSrc.width)
			{
				auto height = rDest.height;
				auto y = rDest.y;

				while(height > rSrc.height)
				{
					batch.SetSrcRect(rSrc.x, rSrc.y, rSrc.width, rSrc.height);
					batch.SetSize(rSrc.width, rSrc.height);
					batch.SetPosition(x, y);
					batch.Draw(color);

					height -= rSrc.height;
					y += rSrc.height;
				}

				if(height > 0)
				{
					batch.SetSrcRect(rSrc.x, rSrc.y, rSrc.width, height);
					batch.SetSize(rSrc.width, height);
					batch.SetPosition(x, y);
					batch.Draw(color);
				}

				width -= rSrc.width;
				x += rSrc.width;
			}

			if(width > 0)
			{
				auto height = rDest.height;
				auto y = rDest.y;

				while(height > rSrc.height)
				{
					batch.SetSrcRect(rSrc.x, rSrc.y, width, rSrc.height);
					batch.SetSize(width, rSrc.height);
					batch.SetPosition(x, y);
					batch.Draw(color);

					height -= rSrc.height;
					y += rSrc.height;
				}

				if(height > 0)
				{
					batch.SetSrcRect(rSrc.x, rSrc.y, width, height);
					batch.SetSize(width, height);
					batch.SetPosition(x, y);
					batch.Draw(color);
				}
			}
		}
        
    }
}