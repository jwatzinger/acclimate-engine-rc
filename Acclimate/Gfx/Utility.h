#pragma once
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {
        struct Vector2;
        struct Vector3;
		struct Vector4;
		struct Matrix;
        struct Ray;
		struct Rect;
    }

	namespace gfx
	{
        class Camera;
        class Screen;
		class ISpriteBatch;
		struct Color;

        math::Ray ViewRayScreenPos(const math::Vector2& vScreenPos, const Camera& camera, const Screen& screen);

		math::Vector3 WorldToScreen(const math::Vector3& vWorldPos, const Camera& camera, const Screen& screen);

		ACCLIMATE_API math::Matrix clipPlaneMatrix(const math::Matrix& mViewProjection, const math::Vector4& vPlane);

		ACCLIMATE_API void fillRectangle(ISpriteBatch& batch, const math::Rect& vDest, const math::Rect& vSrc, const gfx::Color& color);
	}
}