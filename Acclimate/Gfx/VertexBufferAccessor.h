#pragma once
#include "IMesh.h"
#include "MeshBuffer.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		template<typename Type>
		class VertexBufferAccessor
		{
		public:
			VertexBufferAccessor(IMesh& mesh) :
				VertexBufferAccessor(mesh.GetVertexBuffer())
			{
			}

			VertexBufferAccessor(IVertexBuffer& buffer) :
				m_pBuffer(&buffer)
			{
#ifdef _DEBUG
				const size_t size = sizeof(Type);
				ACL_ASSERT(size == m_pBuffer->GetStride());
#endif

				m_pBuffer->Map(MapFlags::WRITE_DISCARD);
				m_pData = (Type*)m_pBuffer->GetData();
			}

			~VertexBufferAccessor(void)
			{
				if(m_pBuffer)
					m_pBuffer->Unmap();
			}

			Type* GetData(void) const
			{
				return m_pData;
			}

			void Quit(void)
			{
				m_pBuffer->Unmap();
				m_pBuffer = nullptr;
			}

		private:

			Type* m_pData;
			IVertexBuffer* m_pBuffer;
		};

	}
}