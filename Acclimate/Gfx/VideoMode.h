#pragma once
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{

		struct VideoMode
		{
			VideoMode(const math::Vector2& vSize, float refreshRate) : vSize(vSize),
				refreshRate(refreshRate)
			{
			}

			VideoMode(unsigned int width, unsigned int height, float refreshRate) : vSize(width, height),
				refreshRate(refreshRate)
			{
			}

			math::Vector2 vSize;
			float refreshRate;
		};

	}
}