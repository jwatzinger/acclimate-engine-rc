#pragma once
#include <string>
#include "IZBuffer.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gfx
    {

        typedef core::Resources<std::wstring, IZBuffer> ZBuffers;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, IZBuffer>;

    }
}