#include "ZBufferInternalAccessor.h"
#include "IZBuffer.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gfx
	{

		ZBufferInternalAccessor::ZBufferInternalAccessor(void) : m_pZBuffer(nullptr)
		{
		}

		void ZBufferInternalAccessor::SetZBuffer(const AclDepth* pZBuffer)
		{
			m_pZBuffer = pZBuffer;
		}

		const AclDepth* ZBufferInternalAccessor::GetZBuffer(void) const
		{
			return m_pZBuffer;
		}

		const AclDepth& getDepthBuffer(const IZBuffer& zBuffer)
		{
			ZBufferInternalAccessor accessor;
			zBuffer.GetBuffer(accessor);
			ACL_ASSERT(accessor.GetZBuffer());
			return *accessor.GetZBuffer();
		}

		const AclDepth* getDepthBuffer(const IZBuffer* pZBuffer)
		{
			if(pZBuffer)
				return &getDepthBuffer(*pZBuffer);
			else
				return nullptr;
		}


	}
}

