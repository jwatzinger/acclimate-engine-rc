#pragma once
#include "..\ApiDef.h"

namespace acl
{
	namespace gfx
	{

		class IZBuffer;

		class ZBufferInternalAccessor
		{
		public:

			ZBufferInternalAccessor(void);

			void SetZBuffer(const AclDepth* pZBuffer);
			const AclDepth* GetZBuffer(void) const;

		private:

			const AclDepth* m_pZBuffer;
		};

		const AclDepth& getDepthBuffer(const IZBuffer& zBuffer);
		const AclDepth* getDepthBuffer(const IZBuffer* pZBuffer);

	}
}


