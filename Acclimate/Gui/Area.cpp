#include "Area.h"
#include "Module.h"

namespace acl
{
    namespace gui
    {

        Area::Area(float x, float y, float width, float height) : Widget(x, y, width, height), m_offsetX(0), m_offsetY(0)
        {
            m_bClipping = true;
        }

		void Area::UpdateChildren(void)
		{
			m_aX -= m_offsetX;
			m_aY -= m_offsetY;

			Widget::UpdateChildren();

			m_aX += m_offsetX;
			m_aY += m_offsetY;
		}

        int Area::GetOffsetX(void) const
        {
            return m_offsetX;
        }

        int Area::GetOffsetY(void) const
        {
            return m_offsetY;
        }

        void Area::ChangeOffsetX(float offsetX)
        {
	        m_offsetX = (int)offsetX;
        }

        void Area::ChangeOffsetY(float offsetY)
        {
	        m_offsetY = (int)offsetY;
        }

    }
}
