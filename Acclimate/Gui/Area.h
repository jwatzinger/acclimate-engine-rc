#pragma once
#include "Widget.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace gui
    {

        class ACCLIMATE_API Area :
	        public Widget
        {
        public:
	        Area(float x, float y, float width, float height);

	        void UpdateChildren(void);

            int GetOffsetX(void) const;
            int GetOffsetY(void) const;

	        void ChangeOffsetX(float offsetX);
	        void ChangeOffsetY(float offsetY);

        private:

	        int m_offsetX, m_offsetY;
        };

    }
}

