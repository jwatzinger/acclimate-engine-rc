#include "AxmLoader.h"
#include "BaseController.h"
#include "Image.h"
#include "Texture.h"
#include "Button.h"
#include "Area.h"
#include "Label.h"
#include "Window.h"
#include "ScrollArea.h"
#include "Toolbar.h"
#include "CheckBox.h"
#include "MenuBar.h"
#include "Slider.h"
#include "SliderBar.h"
#include "Textbox.h"
#include "Combobox.h"
#include "Group.h"
#include "List.h"
#include "RadioBox.h"
#include "TreeView.h"
#include "Table.h"
#include "MultiTable.h"
#include "TextArea.h"
#include "TabBar.h"
#include "DockArea.h"
#include "Dock.h"
#include "..\Core\Resources.h"
#include "..\System\Assert.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
    namespace gui
    {

        AxmLoader::AxmLoader(BaseController& controller, Resources& resources): m_controller(controller), m_resources(resources)
        {
        }

		void AxmLoader::Load(Widget& widget, const std::wstring& stFilename) const
		{
			xml::Doc* pDoc = nullptr;

			if(!m_resources.Has(stFilename))
			{
				pDoc = new xml::Doc;
				pDoc->LoadFile(stFilename.c_str());
				m_resources.Add(stFilename, *pDoc);
			}
			else
				pDoc = m_resources.Get(stFilename);

			m_controller.SetAsMainWidget(&widget);
			if(const xml::Node* pController = pDoc->Root(L"Controller"))
				ParseWidget(&widget, *pController);
		}

        void AxmLoader::Load(const std::wstring& stFilename) const
        {
			xml::Doc* pDoc = nullptr;

			sys::WorkingDirectory dir;

			if(!m_resources.Has(stFilename))
			{
				pDoc = new xml::Doc;
				pDoc->LoadFile(stFilename.c_str());
				m_resources.Add(dir.GetDirectory() + stFilename, *pDoc);
			}
			else
				pDoc = m_resources.Get(dir.GetDirectory() + stFilename);

			if(const xml::Node* pController = pDoc->Root(L"Controller"))
				ParseWidget(nullptr, *pController);
        }

        void AxmLoader::ParseWidget(Widget* pParent, const xml::Node& node) const
        {
			for(auto& widget : node.GetNodes())
			{
				std::wstring stTemp = L"";
				if(auto pName = widget->Attribute(L"name"))
					stTemp = *pName;

				const std::wstring stName = stTemp;

				const std::wstring& stType = widget->GetName();

                Widget* pWidget = nullptr;

				// load metrics
				float x = 0.0f, y = 0.0f, width = 1.0f, height = 1.0f;
                if(const xml::Node* pMetrics = widget->FirstNode(L"Metrics"))
                {
                    x = pMetrics->Attribute(L"x")->AsFloat();
                    y = pMetrics->Attribute(L"y")->AsFloat();
                    width = pMetrics->Attribute(L"w")->AsFloat();
                    height = pMetrics->Attribute(L"h")->AsFloat();
                }

                /****************************************
                * Image
                ****************************************/
                if(stType == L"Image")
                {
                    const std::wstring& stImg = widget->Attribute(L"img")->GetValue();

					Image* pImage = &m_controller.AddWidget<Image>(stName, x, y, width, height, stImg.c_str());

					if(auto pCrop = widget->FirstNode(L"Crop"))
						pImage->SetCrop(true);

					pWidget = pImage;
                }
				/****************************************
				* Texture
				****************************************/
				else if (stType == L"Texture")
				{
					pWidget = &m_controller.AddWidget<Texture>(stName, x, y, width, height, nullptr);
				}
                /****************************************
                * Button
                ****************************************/
                else if(stType == L"Button")
                {
                    const std::wstring& stLabel = widget->Attribute(L"label")->GetValue();

                    pWidget = &m_controller.AddWidget<Button>(stName, x, y, width, height, stLabel.c_str());
                }
                /****************************************
                * Area
                ****************************************/
                else if(stType == L"Area")
                {
                    pWidget = &m_controller.AddWidget<Area>(stName, x, y, width, height);
                }
				/****************************************
                * Scrollarea
                ****************************************/
                else if(stType == L"Scrollarea")
                {
					const unsigned int areaWidth = widget->Attribute(L"areaw")->AsInt();
					const unsigned int areaHeight = widget->Attribute(L"areah")->AsInt();
                    pWidget = &m_controller.AddWidget<ScrollArea>(stName, x, y, width, height, areaWidth, areaHeight);
                }
                /****************************************
                * Label
                ****************************************/
                else if(stType == L"Label")
                {
                    const std::wstring& stLabel = widget->Attribute(L"label")->GetValue();

                    Label& label = m_controller.AddWidget<Label>(stName, x, y, width, height, stLabel.c_str());

                    // text align
                    if(const xml::Node* pAlign = widget->FirstNode(L"Align"))
                        label.SetAlign(pAlign->Attribute(L"value")->AsInt());
                    // text color
                    if(const xml::Node* pColor = widget->FirstNode(L"Color"))
                    {
                        gfx::Color color(pColor->Attribute(L"r")->AsInt(), pColor->Attribute(L"g")->AsInt(), pColor->Attribute(L"b")->AsInt());
                        label.SetColor(&color);
                    }

					if(auto pTextSize = widget->Attribute(L"size"))
						label.SetTextSize(pTextSize->AsInt());

                    pWidget = &label;
                }
				/****************************************
                * Window
                ****************************************/
				else if(stType == L"Window")
				{
					const std::wstring& stLabel = widget->Attribute(L"label")->GetValue();

					Window& window = m_controller.AddWidget<Window>(stName, x, y, width, height, stLabel.c_str());

					// style flags
					if(const xml::Node* pStyle = widget->FirstNode(L"Style"))
					{
						unsigned char flags = (char)pStyle->Attribute(L"flags")->AsInt();
						window.SetStyleFlags(flags);
					}

					// icon
					if(const xml::Node* pIcon = widget->FirstNode(L"Icon"))
					{
						const std::wstring& stName = pIcon->Attribute(L"name")->GetValue();
						unsigned int iconX = pIcon->Attribute(L"x")->AsInt();
						unsigned int iconY = pIcon->Attribute(L"y")->AsInt();
						unsigned int iconWidth = pIcon->Attribute(L"w")->AsInt();
						unsigned int iconHeight = pIcon->Attribute(L"h")->AsInt();
						window.SetIcon(stName.c_str(), iconX, iconY, iconWidth, iconHeight);
					}

					if(auto pAreaClipping = widget->FirstNode(L"AreaClipping"))
						window.SetAreaClipping(conv::FromString<bool>(pAreaClipping->GetValue()));

					pWidget = &window;
				}
				/****************************************
                * Toolbar
                ****************************************/
                else if(stType == L"Toolbar")
				{
					pWidget = &m_controller.AddWidget<Toolbar>(stName, y, height);
				}
				/****************************************
                * CheckBox
                ****************************************/
				else if(stType == L"Checkbox")
				{
					const float size = widget->Attribute(L"size")->AsFloat();

					CheckBox& box = m_controller.AddWidget<CheckBox>(stName, x, y, size);

					if(widget->FirstNode(L"Checked"))
						box.OnCheck(true);

					pWidget = &box;
				}
				/****************************************
                * Menubar
                ****************************************/
				else if(stType == L"Menubar")
				{
					pWidget = &m_controller.AddWidget<MenuBar>(stName, y, height);
				}
				/****************************************
                * Slider
                ****************************************/
				else if(stType == L"Slider")
				{
					const float min = widget->Attribute(L"min")->AsFloat();
					const float max = widget->Attribute(L"max")->AsFloat();
					const float slider = widget->Attribute(L"slider")->AsFloat();
					const bool bVertical = widget->Attribute(L"vertical")->AsBool();

					Slider* pSlider;
					if(bVertical)
						pSlider = &m_controller.AddWidget<Slider>(stName, x, y, height, width, min, max, slider, bVertical);
					else
						pSlider = &m_controller.AddWidget<Slider>(stName, x, y, width, height, min, max, slider, bVertical);

					if(widget->FirstNode(L"Integer"))
						pSlider->SetIntegerMode(true);

					pWidget = pSlider;
				}
				/****************************************
                * Textbox
                ****************************************/
				else if(stType == L"Textbox")
				{
					const std::wstring& stType = widget->Attribute(L"type")->GetValue();
					const auto pDefault = widget->Attribute(L"default");

					if(stType == L"string")
					{
						auto pBox = &m_controller.AddWidget<Textbox<std::wstring>>(stName, x, y, width, height, pDefault->GetValue());
						if(!widget->FirstNode(L"Editable"))
							pBox->SetEditable(false);
						pWidget = pBox;
					}
					else if(stType == L"int")
					{
						auto pBox = &m_controller.AddWidget<Textbox<int>>(stName, x, y, width, height, pDefault->AsInt());
						if(!widget->FirstNode(L"Editable"))
							pBox->SetEditable(false);
						pWidget = pBox;
					}
					else if (stType == L"float")
					{
						auto pBox = &m_controller.AddWidget<Textbox<float>>(stName, x, y, width, height, pDefault->AsFloat());
						if (!widget->FirstNode(L"Editable"))
							pBox->SetEditable(false);
						pWidget = pBox;
					}
				}
				/****************************************
                * Sliderbar
                ****************************************/
				else if(stType == L"Sliderbar")
				{
					const float min = widget->Attribute(L"min")->AsFloat();
					const float max = widget->Attribute(L"max")->AsFloat();
					const float slider = widget->Attribute(L"slider")->AsFloat();
					const bool bVertical = widget->Attribute(L"vertical")->AsBool();

					SliderBar* pSliderBar;

					if(bVertical)
						pSliderBar = &m_controller.AddWidget<SliderBar>(stName, x, y, height, width, min, max, slider, bVertical);
					else
						pSliderBar = &m_controller.AddWidget<SliderBar>(stName, x, y, width, height, min, max, slider, bVertical);

					if(auto pPrecision = widget->FirstNode(L"Precision"))
						pSliderBar->SetPrecicion(pPrecision->Attribute(L"value")->AsInt());

					if(auto pDefault = widget->Attribute(L"default"))
						pSliderBar->SetValue(pDefault->AsFloat());
					
					pWidget = pSliderBar;
				}
				/****************************************
                * Combobox
                ****************************************/
				else if(stType == L"Combobox")
				{
					const float dropDownMaxHeight = widget->Attribute(L"max")->AsFloat();
					const std::wstring& stType = widget->Attribute(L"type")->GetValue();

					if(stType == L"int")
					{
						auto& box = m_controller.AddWidget<ComboBox<int>>(stName, x, y, width, height, dropDownMaxHeight);

						if(auto pItems = widget->Nodes(L"Item"))
						{
							for(auto pItem : *pItems)
							{
								const std::wstring& stText = pItem->Attribute(L"text")->GetValue();
								const int value = pItem->Attribute(L"value")->AsInt();

								box.AddItem(stText, value);
							}
						}

						pWidget = &box;
					}
					else if(stType == L"string")
					{
						auto& box = m_controller.AddWidget<ComboBox<std::wstring>>(stName, x, y, width, height, dropDownMaxHeight);

						if(auto pItems = widget->Nodes(L"Item"))
						{
							for(auto pItem : *pItems)
							{
								const std::wstring& stText = pItem->Attribute(L"text")->GetValue();
								const std::wstring& stValue = pItem->Attribute(L"value")->GetValue();

								box.AddItem(stText, stValue);
							}
						}

						pWidget = &box;
					}
				}
				/****************************************
                * Group
                ****************************************/
				else if(stType == L"Group")
				{
					const std::wstring& stCaption = widget->Attribute(L"caption")->GetValue();

					pWidget = &m_controller.AddWidget<Group>(stName, x, y, width, height, stCaption);
				}
				/****************************************
				* List
				****************************************/
				else if(stType == L"List")
				{
					const float itemHeight = widget->Attribute(L"itemh")->AsFloat();
					const std::wstring& stType = widget->Attribute(L"type")->GetValue();

					if(stType == L"string")
						pWidget = &m_controller.AddWidget<List<std::wstring>>(stName, x, y, width, height, itemHeight);
				}
				/****************************************
				* RadioBox
				****************************************/
				else if(stType == L"RadioBox")
				{
					const std::wstring& stType = widget->Attribute(L"type")->GetValue();

					if (stType == L"int")
					{
						auto& box = m_controller.AddWidget<GuiRadioBox<int>>(stName, x, y, width, height);

						if(auto pFields = widget->Nodes(L"Field"))
						{
							for(auto pField : *pFields)
							{
								box.AddRadioField(pField->Attribute(L"name")->GetValue(), pField->Attribute(L"data")->AsInt());
							}
						}

						pWidget = &box;
					}
				}
				/****************************************
				* Tree
				****************************************/
				else if(stType == L"Tree")
				{
					const float itemHeight = widget->Attribute(L"itemheight")->AsFloat();

					pWidget = &m_controller.AddWidget<TreeView>(stName, x, y, width, height, itemHeight);
				}
				/****************************************
				* Table
				****************************************/
				else if(stType == L"Table")
				{
					const float itemHeight = widget->Attribute(L"itemheight")->AsFloat();

					auto& table = m_controller.AddWidget<Table>(stName, x, y, width, height, itemHeight);

					if(auto pColumnSize = widget->Attribute(L"columnsize"))
						table.SetLeftColumnSize(pColumnSize->AsFloat());

					pWidget = &table;
				}
				/****************************************
				* MultiTable
				****************************************/
				else if(stType == L"Multitable")
				{
					const float itemHeight = widget->Attribute(L"itemheight")->AsFloat();

					pWidget = &m_controller.AddWidget<MultiTable>(stName, x, y, width, height, itemHeight);
				}
				/****************************************
				* TextArea
				****************************************/
				else if(stType == L"Textarea")
				{
					auto pTextArea = &m_controller.AddWidget<TextArea>(stName, x, y, width, height, L"");

					if(widget->FirstNode(L"HideBackground"))
						pTextArea->SetBackgroundVisible(false);

					if(const auto pColor = widget->FirstNode(L"Color"))
					{
						gfx::Color color(pColor->Attribute(L"r")->AsInt(), pColor->Attribute(L"g")->AsInt(), pColor->Attribute(L"b")->AsInt());
						pTextArea->SetColor(color);
					}

					pWidget = pTextArea;
				}
				/****************************************
				* TabBar
				****************************************/
				else if(stType == L"TabBar")
				{
					pWidget = &m_controller.AddWidget<TabBar>(stName, x, y, width, height);
				}
				/****************************************
				* DockArea
				****************************************/
				else if(stType == L"DockArea")
				{
					auto& dockArea = m_controller.AddWidget<DockArea>(stName, x, y, width, height);

					if(auto pDockState = widget->FirstNode(L"DockState"))
						dockArea.SetDockState((gui::DockState)conv::FromString<int>(pDockState->GetValue()));

					pWidget = &dockArea;
				}
				/****************************************
				* Dock
				****************************************/
				else if(stType == L"Dock")
				{
					const std::wstring& stLabel = *widget->Attribute(L"label");

					pWidget = &m_controller.AddWidget<Dock>(stName, x, y, width, height, stLabel);
				}
				else
                    continue;

				ACL_ASSERT(pWidget);

                /****************************************
                * General attributes
                ****************************************/

                // invisible tag
                if(const xml::Node* pInvisible = widget->FirstNode(L"Invisible"))
                    pWidget->OnInvisible();
                // disabled tag
                if(const xml::Node* pDisabled = widget->FirstNode(L"Disabled"))
                    pWidget->OnDisable();
                // aligmenent
                if(const xml::Node* pAligement = widget->FirstNode(L"Alignment"))
                {
                    const HorizontalAlign hAlign = static_cast<HorizontalAlign>(pAligement->Attribute(L"h")->AsInt());
                    const VerticalAlign vAlign = static_cast<VerticalAlign>(pAligement->Attribute(L"v")->AsInt());
                    pWidget->SetAlignement(hAlign, vAlign);
                }
                // center
                if(const xml::Node* pCenter = widget->FirstNode(L"Center"))
                {
                    const HorizontalAlign hAlign = static_cast<HorizontalAlign>(pCenter->Attribute(L"h")->AsInt());
                    const VerticalAlign vAlign = static_cast<VerticalAlign>(pCenter->Attribute(L"v")->AsInt());
                    pWidget->SetCenter(hAlign, vAlign);
                }
                // border relation
                if(const xml::Node* pBorderRel = widget->FirstNode(L"BorderRel"))
                {
                    const BorderRelation relation = static_cast<BorderRelation>(conv::FromString<int>(pBorderRel->GetValue()));
					pWidget->SetBorderRelation(relation, pBorderRel->Attribute(L"factor")->AsFloat());
                }
                // main tag
                if(const xml::Node* pMain = widget->FirstNode(L"Main"))
                    m_controller.SetAsMainWidget(pWidget);
				// padding
				if(const xml::Node* pPadding = widget->FirstNode(L"Padding"))
				{
					unsigned int padX = pPadding->Attribute(L"x")->AsInt();
					unsigned int padY = pPadding->Attribute(L"y")->AsInt();
					unsigned int padWidth = pPadding->Attribute(L"w")->AsInt();
					unsigned int padHeight = pPadding->Attribute(L"h")->AsInt();
					pWidget->SetPadding(padX, padY, padWidth, padHeight);
				}

				// position modes
				if(const xml::Node* pPositonModes = widget->FirstNode(L"PositionModes"))
				{
					const PositionMode x = static_cast<PositionMode>(pPositonModes->Attribute(L"x")->AsInt());
					const PositionMode y = static_cast<PositionMode>(pPositonModes->Attribute(L"y")->AsInt());
					const PositionMode width = static_cast<PositionMode>(pPositonModes->Attribute(L"width")->AsInt());
					const PositionMode height = static_cast<PositionMode>(pPositonModes->Attribute(L"height")->AsInt());
					pWidget->SetPositionModes(x, y, width, height);
				}
				// cap metrics
				if (const xml::Node* pCap = widget->FirstNode(L"Cap"))
				{
					const unsigned int minW = pCap->Attribute(L"minw")->AsInt();
					const unsigned int maxW = pCap->Attribute(L"maxw")->AsInt();
					const unsigned int minH = pCap->Attribute(L"minh")->AsInt();
					const unsigned int maxH = pCap->Attribute(L"maxh")->AsInt();
					pWidget->SetCapMetrics(minW, maxW, minH, maxH);
				}
				// clip
				if (const xml::Node* pClip = widget->FirstNode(L"Clip"))
					pWidget->SetClipping(pClip->Attribute(L"value")->AsBool());

				// tooltip
				if(const xml::Node* pTooltip = widget->FirstNode(L"Tooltip"))
					pWidget->SetTooltip(pTooltip->GetValue());

				// focus state
				if(const xml::Node* pFocus = widget->FirstNode(L"Focus"))
					pWidget->SetFocusState((FocusState)conv::FromString<int>(pFocus->GetValue()));

				if(pParent)
					pParent->AddChild(*pWidget);
				else
					pWidget->Free();	

				// order
				if(auto pOrder = widget->FirstNode(L"Order"))
					pWidget->SetOrder(conv::FromString<int>(pOrder->GetValue()));

                if(pWidget)
                    ParseWidget(pWidget, *widget);
            }
        }

    }
}