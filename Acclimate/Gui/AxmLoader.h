#pragma once
#include "ILoader.h"
#include "Resources.h"

namespace acl
{
    namespace xml
    {
        class Node;
    }

    namespace gui
    {
        class Widget;
        class BaseController;

        class AxmLoader :
            public ILoader
        {
        public:
            AxmLoader(BaseController& controller, Resources& resources);

			void Load(Widget& parent, const std::wstring& stFilename) const override;
            void Load(const std::wstring& stFilename) const override;

        private:

            void ParseWidget(Widget* pParent, const xml::Node& node) const;

            BaseController& m_controller;
			Resources& m_resources;
        };

    }
}

