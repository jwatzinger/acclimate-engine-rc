#include "BaseButton.h"
#include "Label.h"
#include "Icon.h"
#include "Module.h"

namespace acl
{
    namespace gui
    {

        BaseButton::BaseButton(float x, float y, float width, float height) : Widget(x, y, width, height), m_bDown(false), 
			m_bDownResponseState(false), m_buttonState(0)
        {
	        m_cTimer.Reset();
        }

		void BaseButton::OnClick(MouseType mouse)
        {
			if(mouse == MouseType::LEFT)
			{
				if(m_bDown)
				{
					if(m_bDownResponseState)
						Widget::OnClick(mouse);
					return;
				}

				m_cTimer.Reset();
				OnStateChange(2);
			}

			Widget::OnClick(mouse);
        }

		void BaseButton::OnRelease(MouseType mouse, bool bMouseOver)
        {
			//if button is fixed down or not active
			if(!m_bActive)
				return;

			OnActivate(false);

			if(mouse == MouseType::LEFT)
			{
				//if mouse is over button
				if(bMouseOver)
				{
					if(m_bDown)
					{
						if(m_bDownResponseState)
							Up();
					}
					else
					{
						if(m_bDownResponseState)
							Down();
						else if(!m_bDisabled) //if button is enabled
							OnStateChange(1);
					}
					SigReleased();
				}
				else if(!m_bDown)
					OnStateChange(0);
			}
        }

        void BaseButton::OnMoveOn(void)
        {
	        Widget::OnMoveOn();

	        if(m_bDown)
		        return;

	        if(!m_bActive)
		        OnStateChange(1);
        }

        void BaseButton::OnHold(bool bMouseOver)
        {
	        if(bMouseOver && m_cTimer.Duration() > 0.1f)
	        {
		        m_cTimer.Reset();
		        SigRepeat();
	        }
        }

        void BaseButton::OnMoveOff(void)
        {
			Widget::OnMoveOff();

	        if(m_bDown)
		        return;

	        if(!m_bActive)
		        OnStateChange(0);
        }

        void BaseButton::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData grData;

			int width = GetWidth(), height = GetHeight();

			const Element* pElement = nullptr;
			switch(m_buttonState)
			{
			case 0:
				pElement = m_pModule->m_layout.GetElement(L"Button");
				break;
			case 1:
				pElement = m_pModule->m_layout.GetElement(L"ButtonOver");
				break;
			case 2:
				pElement = m_pModule->m_layout.GetElement(L"ButtonDown");
				break;
			case 3:
				pElement = m_pModule->m_layout.GetElement(L"ButtonDisabled");
				break;
			}

			const math::Rect& rBottom = pElement->GetPart(L"Bottom");

	        //draw top left
			const math::Rect& rTopLeft = pElement->GetPart(L"TopLeft");
	        grData.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
	        grData.rSrcRect = rTopLeft;
	        m_vRenderData.push_back(grData);

	        //draw top right
			const math::Rect& rTopRight = pElement->GetPart(L"TopRight");
	        grData.rDestRect.Set(width-rTopRight.width, 0, rTopRight.width, rTopRight.height);
	        grData.rSrcRect = rTopRight;
	        m_vRenderData.push_back(grData);

			//draw top middle
			const math::Rect& rTop = pElement->GetPart(L"Top");
	        grData.rDestRect.Set(rTopLeft.width, 0, width-rTopLeft.width-rTopRight.width, rTop.height);
	        grData.rSrcRect = rTop;
	        m_vRenderData.push_back(grData);

			//draw bottom left
			const math::Rect& rBottomLeft = pElement->GetPart(L"BottomLeft");
	        grData.rDestRect.Set(0, height-rBottom.height, rBottomLeft.width, rBottomLeft.height);
	        grData.rSrcRect = rBottomLeft;
	        m_vRenderData.push_back(grData);

	        //draw left shape
			const math::Rect& rLeft = pElement->GetPart(L"Left");
	        grData.rDestRect.Set(0, rTopLeft.height, rLeft.width, height-rTopLeft.height-rBottomLeft.height);
	        grData.rSrcRect = rLeft;
	        m_vRenderData.push_back(grData);

	        //draw bottom right
			const math::Rect& rBottomRight = pElement->GetPart(L"BottomRight");
	        grData.rDestRect.Set(width-rBottomRight.width, height-rBottomRight.height, rBottomRight.width, rBottomRight.height);
	        grData.rSrcRect = rBottomRight;
	        m_vRenderData.push_back(grData);

	        //draw bottom shape
	        grData.rDestRect.Set(rBottomLeft.width, height-rBottom.height, width-rBottomLeft.width-rBottomRight.width, rBottom.height);
	        grData.rSrcRect = rBottom;
	        m_vRenderData.push_back(grData);

	        //draw right shape
			const math::Rect& rRight = pElement->GetPart(L"Right");
	        grData.rDestRect.Set(width-rRight.width, rTopRight.height, rRight.width, height-rTopRight.height-rBottomRight.height);
	        grData.rSrcRect = rRight;
	        m_vRenderData.push_back(grData);

	        //render button back
			const math::Rect& rBack = pElement->GetPart(L"Back");
	        grData.rDestRect.Set(rTopLeft.width, rTopLeft.height, width-rBottomLeft.width-rBottomRight.width, height-rBottomLeft.height-rBottomRight.height);
	        grData.rSrcRect = rBack;
	        m_vRenderData.push_back(grData);
        }

        void BaseButton::OnStateChange(int newState)
        {
	        m_buttonState = newState;
	        m_bDirty = true;
        }

        void BaseButton::OnEnable(void)
        {
	        if(m_bDisabled)
	        {
		        m_bDisabled = false;
		        OnStateChange(0);
	        }
        }

        void BaseButton::OnDisable(void)
        {
	        if(!m_bDisabled)
	        {
		        m_bDisabled = true;
		        OnStateChange(3);
	        }
        }

        void BaseButton::Up(void)
        {
	        m_bDown = false;
	        OnStateChange(0);
        }

        void BaseButton::Down(void)
        {
	        m_bDown = true;
	        OnStateChange(2);
        }

        bool BaseButton::IsDown(void)
        {
	        return m_bDown;
        }

        void BaseButton::SetDownResponseState(bool bState)
        {
	        m_bDownResponseState = bState;
        }

    }
}