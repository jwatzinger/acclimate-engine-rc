#pragma once
#include "Widget.h"
#include "..\Core\Timer.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API BaseButton :
			public Widget
		{
		public:
			BaseButton(float x, float y, float width, float height);

			void SetDownResponseState(bool bState);

			void Down(void);
			void Up(void);
			bool IsDown(void);

			//events
            void OnStateChange(int newState);
			void OnHold(bool bMouseOver) override;
			void OnRelease(MouseType mouse, bool bMouseOver) override;
			void OnMoveOn(void) override;
			void OnMoveOff(void) override;
			void OnDisable(void) override;
			void OnEnable(void) override;
            virtual void OnClick(MouseType mouse) override;
			virtual void OnRedraw(void) override;

			//signal
			core::Signal<> SigRepeat;

		protected:
	
			int m_buttonState;
			bool m_bDown, m_bDownResponseState;
			core::Timer m_cTimer;
		};

	}
}

