#include "BaseController.h"
#include "Widget.h"
#include "AxmLoader.h"

namespace acl
{
	namespace gui
	{
        
        BaseController::BaseController(Module& module, const std::wstring& stFilename): m_pMainWidget(nullptr), m_bFocus(false),
			m_pModule(&module)
        {
            m_pModule->RegisterController(*this);

            if(!stFilename.empty())
            {
                AxmLoader loader(*this, m_resources);
                loader.Load(stFilename);
            }
        }

		BaseController::BaseController(Module& module, Widget& widget, const std::wstring& stFilename) : m_pMainWidget(nullptr), m_bFocus(false),
			m_pModule(&module)
		{
			m_pModule->RegisterController(*this);

			if(!stFilename.empty())
			{
				AxmLoader loader(*this, m_resources);
				loader.Load(widget, stFilename);
			}
		}

		BaseController::~BaseController(void)
		{
			// delete all registered controller
			for(BaseController* pController : m_vController)
			{
				delete pController;
			}

			// delete all registered widgets
			for(Widget* pWidget : m_vWidgets)
			{
                if(pWidget)
                {
                    pWidget->SetParentController(nullptr);
				    delete pWidget;
                }
			}

            m_pModule->UnregisterController(*this);
		}

		void BaseController::SetParent(Widget& widget)
		{
			if(m_pMainWidget)
				m_pMainWidget->SetParent(&widget);
		}

		Widget* BaseController::GetWidgetByName(const std::wstring& stName) const
		{
			auto itr = m_mWidgets.find(stName);
			if (itr != m_mWidgets.end())
				return itr->second;
			else
				return nullptr;
		}

		Widget* BaseController::GetMainWidget(void)
		{
			return m_pMainWidget;
		}

		const Widget* BaseController::GetMainWidget(void) const
		{
			return m_pMainWidget;
		}

		Module& BaseController::GetModule(void) const
		{
			return *m_pModule;
		}

		bool BaseController::IsVisible(void) const
		{
			if(m_pMainWidget)
				return m_pMainWidget->IsVisible();
			else
				return true;
		}

        void BaseController::RemoveController(BaseController& controller)
        {
            // check vector for controller
            ControllerVector::iterator ii = std::find(m_vController.begin(), m_vController.end(), &controller);

            // delete and remove controller if it was found
            if( ii != m_vController.end() )
            {
                delete *ii;
                m_vController.erase(ii);
            }
        }

		void BaseController::Update(void)
		{
            if(!m_pMainWidget || m_pMainWidget->IsVisible()) 
            {
                // update all controllers
			    for(BaseController* pController : m_vController)
			    {
				    pController->Update();
			    }
            }
		}
        
	    void BaseController::SetAsMainWidget(Widget* pWidget)
        {
            // set as main widget
            m_pMainWidget = pWidget;
        }

        void BaseController::RemoveWidget(Widget& widget)
        {
            WidgetVector::iterator ii = std::find(m_vWidgets.begin(), m_vWidgets.end(), &widget);

            if( ii != m_vWidgets.end() )
            {
                *ii = nullptr;
                for(WidgetNames::iterator jj = m_mWidgets.begin(); jj != m_mWidgets.end(); ++jj)
                {
                    if(jj->second == &widget)
                    {
                        m_mWidgets.erase(jj);
						break;
                    }
                }
				
				m_vWidgets.erase(ii);
            }
        }

        void BaseController::OnToggle(void)
        {
            if(m_pMainWidget)
            {
	            if(!m_pMainWidget->IsVisible())
                {
		            m_pMainWidget->OnVisible();
                    // also push as top widget
                    m_pMainWidget->PushToTop();
                }
	            else
		            m_pMainWidget->OnInvisible();
            }
        }

		void BaseController::OnToggle(bool bVisible)
		{
			if(m_pMainWidget)
			{
				if(bVisible)
				{
					m_pMainWidget->OnVisible();
					// also push as top widget
					m_pMainWidget->PushToTop();
				}
				else
					m_pMainWidget->OnInvisible();
			}
		}

        void BaseController::OnFocus(bool bFocus)
        {
            if(bFocus != m_bFocus)
            {
                m_bFocus = bFocus;
                if(bFocus)
                    m_pModule->SetFocusController(this);
                else if(m_pModule->GetFocusController() == this)
                    m_pModule->SetFocusController(nullptr);
            }
        }

		Resources BaseController::m_resources = Resources();

	}
}