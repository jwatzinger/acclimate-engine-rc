#pragma once
#include <vector>
#include <map>
#include "Resources.h"
#include "Module.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class Widget;

		enum class Keys;

		class ACCLIMATE_API BaseController
		{
		protected:

			typedef std::vector<BaseController*> ControllerVector;
			typedef std::vector<Widget*> WidgetVector;
            typedef std::map<std::wstring, Widget*> WidgetNames;

		public:

            BaseController(Module& module, const std::wstring& stFilename = L"");
			BaseController(Module& module, Widget& parent, const std::wstring& stFilename = L"");
			virtual ~BaseController(void);

			void SetParent(Widget& widget);
            void RemoveWidget(Widget& widget);
			Widget* GetMainWidget(void);
			const Widget* GetMainWidget(void) const;

			virtual void Update(void);

            void OnToggle(void);
			void OnToggle(bool bVisible);
            void OnFocus(bool bFocus);
			virtual void OnKeyPress(Keys key) {}

			Widget* GetWidgetByName(const std::wstring& stName) const;
            template<typename W>
            W* GetWidgetByName(const std::wstring& stName) const;
			Module& GetModule(void) const;
			bool IsVisible(void) const;

            template<typename W, typename... Args>
			W& AddWidget(const std::wstring& stName, Args&&... args);
            void SetAsMainWidget(Widget* pWidget);

			template<typename C>
			C& AddController(void);
			template<typename C, typename Arg>
			C& AddController(Arg&& arg);
			template<typename C, typename Arg, typename... Args>
			C& AddController(Arg&& arg, Args&&... args);
			void RemoveController(BaseController& controller);

		protected:

            template<typename W, typename... Args>
			W& AddWidget(Args&&... args);

            Widget* m_pMainWidget;

			Module* m_pModule;

		private:

#pragma warning( disable: 4251 )
            
            bool m_bFocus;

            ControllerVector m_vController;
			WidgetVector m_vWidgets;
            WidgetNames m_mWidgets;

			static Resources m_resources;

#pragma warning( default: 4251 )

		};

		template<typename C>
		C& BaseController::AddController(void)
		{
			C* pController = new C(*m_pModule);
			m_vController.push_back(pController);

			return *pController;
		}

		template<typename C, typename Arg>
		C& BaseController::AddController(Arg&& arg)
		{
			C* pController = new C(*m_pModule, arg);
			m_vController.push_back(pController);

			return *pController;
		}

		template<typename C, typename Arg, typename... Args>
		C& BaseController::AddController(Arg&& arg, Args&&... args)
		{
			C* pController = new C(*m_pModule, arg, args...);
			m_vController.push_back(pController);

			return *pController;
		}

		template<typename W, typename... Args>
		W& BaseController::AddWidget(const std::wstring& stName, Args&&... args)
		{
			if(auto pOldWidget = m_mWidgets[stName])
			{
				delete pOldWidget;
				m_mWidgets.erase(stName);
			} 

			W* pWidget = new W(args...);
			m_pModule->RegisterWidget(*pWidget);

            if(m_pMainWidget)
                m_pMainWidget->AddChild(*pWidget);
            else
                pWidget->SetParentController(this);

			m_vWidgets.push_back(pWidget);
            
            if(!stName.empty())
                m_mWidgets[stName] = pWidget;

			return *pWidget;
		}

        template<typename W, typename... Args>
		W& BaseController::AddWidget(Args&&... args)
		{
			W* pWidget = new W(args...);
			m_pModule->RegisterWidget(*pWidget);

            if(m_pMainWidget)
                m_pMainWidget->AddChild(*pWidget);
            else
                pWidget->SetParentController(this);

			m_vWidgets.push_back(pWidget);

			return *pWidget;
		}

        template<typename W>
        W* BaseController::GetWidgetByName(const std::wstring& stName) const
        {
			if(auto pWidget = GetWidgetByName(stName))
			{
#ifdef _DEBUG
				return dynamic_cast<W*>(pWidget);
#else
				return static_cast<W*>(pWidget);
#endif
			}
            else
                return nullptr;
        }

	}
}