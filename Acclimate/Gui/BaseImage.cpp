#include "BaseImage.h"

namespace acl
{
	namespace gui
	{

		BaseImage::BaseImage(float x, float y, float width, float height): Widget(x, y, width, height), m_offsetX(0), m_offsetY(0), m_bCrop(false),
			m_zoom(1.0f)
		{
		}

		void BaseImage::SetCrop(bool bCrop)
		{
			m_bCrop = bCrop;
		}

		void BaseImage::SetZoomFactor(float zoom)
		{
			m_bDirty |= zoom != m_zoom;
			m_zoom = zoom;
		}

		float BaseImage::GetZoomFactor(void) const
		{
			return m_zoom;
		}

		void BaseImage::ChangeOffsetY(int offsetY)
		{
			m_offsetY = offsetY;
			m_bDirty = true;
		}

		void BaseImage::ChangeOffsetX(int offsetX)
		{
			m_offsetX = offsetX;
			m_bDirty = true;
		}

	}
}