#pragma once
#include "Widget.h"


namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API BaseImage :
			public Widget
		{
		public:
			BaseImage(float x, float y, float width, float height);
            virtual ~BaseImage(void) {};

			void SetCrop(bool bCrop);
			void SetZoomFactor(float zoom);

			float GetZoomFactor(void) const;

			void ChangeOffsetY(int offsetY);
			void ChangeOffsetX(int offsetX);

		protected:

			int m_offsetX, m_offsetY;
			float m_zoom;
			bool m_bCrop;
		};

	}
}


