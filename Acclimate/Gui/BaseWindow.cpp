#include "BaseWindow.h"
#include "Module.h"
#include "Area.h"
#include "..\System\Bit.h"

namespace acl
{
    namespace gui
    {

		BaseWindow::BaseWindow(float x, float y, float width, float height, const std::wstring& stName) : Widget(x, y, width, height), m_cStyleFlags(TopBar | Border),
			m_stName(stName), m_bResizing(false), m_dragBorder(WindowBorders::None), m_area(0.0f, 0.0f, 1.0f, 1.0f), m_leftBorder(0), m_rightBorder(0), m_bottomBorder(0),
			m_topBorder(24)
        {
			Widget::AddChild(m_area);
        }

		void BaseWindow::AddChild(Widget& child)
        {
			child.SigKey.Connect(this, &BaseWindow::OnKeyDown);
	        m_area.AddChild(child);
        }

        void BaseWindow::RemoveChild(Widget& child)
        {
			child.SigKey.Disconnect(this, &BaseWindow::OnKeyDown);
	        m_area.RemoveChild(child);
        }

		void BaseWindow::SetAreaClipping(bool bClip)
		{
			m_area.SetClipping(bClip);
		}

		void BaseWindow::SetLabel(const std::wstring& stName)
		{
			m_stName = stName;
			m_bDirty = true;
		}

        void BaseWindow::SetStyleFlags(unsigned char cStyleFlags)
        {
	        unsigned char cTempFlags = m_cStyleFlags;
	        m_cStyleFlags |= cStyleFlags;
			m_bDirty = true;

			ChangeArea();
        }

        void BaseWindow::RemoveStyleFlags(unsigned char cStyleFlags)
        {
	        unsigned char cTempFlags = m_cStyleFlags;
	        m_cStyleFlags &= ~cStyleFlags;
			m_bDirty = true;

			ChangeArea();
        }

        bool BaseWindow::HasStyleFlags(unsigned char cStyleFlags) const
        {
	        return bit::contains(m_cStyleFlags, cStyleFlags);
        }

		const std::wstring& BaseWindow::GetName(void) const
		{
			return m_stName;
		}

		void BaseWindow::OnClick(MouseType mouse)
        {
	        Widget::OnClick(mouse);
	        PushToTop();
        }

		void BaseWindow::OnRelease(MouseType mouse, bool bMouseOver)
        {
	        Widget::OnRelease(mouse, bMouseOver);

			if(mouse == MouseType::LEFT)
			{
				m_bResizing = false;
				m_dragBorder = WindowBorders::None;
			}
			else
				OnActivate(true);
        }

        void BaseWindow::OnDrag(const math::Vector2& vDistance)
        {
	        //mark as dirty
	        m_bDirty = true;

	        //check if window is maximized?
	        if(HasStyleFlags(Maximized))
		        return;

	        //check for horizontal resize
	        //if no resize, drag
	        if (!m_bResizing)
	        {
		        m_dragBorder = CheckBorder();
		        m_bResizing = true;

				auto& vCursorPos = m_pModule->m_cursor.GetPosition();
				m_vDragDist = vCursorPos - math::Vector2(GetX(), GetY());
	        }

	        if(m_dragBorder != WindowBorders::None)
	        {
				if(!HasStyleFlags(NoResize))
				{
					bool doReturn = false;
					if(!HasStyleFlags(NoResizeH))
					{
						if(bit::contains(m_dragBorder, WindowBorders::Left))
						{
							OnResizeH(true);
							doReturn = true;
						}
						else if(bit::contains(m_dragBorder, WindowBorders::Right))
						{
							OnResizeH(false);
							doReturn = true;
						}
					}

					if(!HasStyleFlags(NoResizeV))
					{
						if(bit::contains(m_dragBorder, WindowBorders::Top))
						{
							OnResizeV(true);
							doReturn = true;
						}
						else if(bit::contains(m_dragBorder, WindowBorders::Bottom))
						{
							OnResizeV(false);
							doReturn = true;
						}
					}

					if(doReturn)
						return;
				}
	        }

			auto& vCursorPos = m_pModule->m_cursor.GetPosition();
			auto& vScreenSize = m_pModule->GetScreenSize();

			m_x = (float)(vCursorPos.x - m_vDragDist.x);
			m_x -= m_pParent->GetX();

			switch(m_positionModeX)
			{
			case PositionMode::REL:
				m_x /= m_pParent->GetWidth();
				break;
			case PositionMode::REF:
				m_x /= m_pModule->m_layout.GetReferenceSize().x;
				break;
			case PositionMode::SCREEN:
				m_x /= (float)vScreenSize.x;
				break;
			}
			
			m_y = (float)(vCursorPos.y - m_vDragDist.y);
			m_y -= m_pParent->GetY();

			switch(m_positionModeY)
			{
			case PositionMode::REL:
				m_y /= m_pParent->GetHeight();
				break;
			case PositionMode::REF:
				m_y /= m_pModule->m_layout.GetReferenceSize().y;
				break;
			case PositionMode::SCREEN:
				m_y /= (float)vScreenSize.y;
				break;
			}

			float width = (float)GetWidth();
			switch(m_positionModeX)
			{
			case PositionMode::REL:
				width /= m_pParent->GetWidth();
				break;
			case PositionMode::REF:
				width /= m_pModule->m_layout.GetReferenceSize().x;
				break;
			case PositionMode::SCREEN:
				width /= m_pModule->GetScreenSize().x;
				break;
			}

			switch(m_hCenter)
			{
			case HorizontalAlign::CENTER:
				m_x += width / 2.0f;
				break;
			case HorizontalAlign::RIGHT:
				m_x += width;
				break;
			}

			float height = (float)GetHeight();
			switch(m_positionModeX)
			{
			case PositionMode::REL:
				height /= m_pParent->GetHeight();
				break;
			case PositionMode::REF:
				height /= m_pModule->m_layout.GetReferenceSize().y;
				break;
			case PositionMode::SCREEN:
				height /= m_pModule->GetScreenSize().y;
				break;
			}

			switch(m_vCenter)
			{
			case VerticalAlign::CENTER:
				m_y += height/2.0f;
				break;
			case VerticalAlign::BOTTOM:
				m_y += height;
				break;
			}

	        SigDrag(vDistance);
        }

        void BaseWindow::OnResizeH(bool left)
        {
	        if(HasStyleFlags(NoResizeH))
		        return;

	        int newWidth;
	        //calculate new width based on left or right drag
	        if (left)
				newWidth = GetX() + GetWidth() - m_vLastMousePos.x;
	        else
				newWidth = m_vLastMousePos.x - GetX();
	        //cap width to minimal size
			const int amount = newWidth - GetWidth();
			const float recipAmount = amount / (float)m_pModule->GetScreenSize().x;

			const float scaleFactor = m_width / GetHeight();
			SetWidth(m_width + recipAmount);
			m_width = max(m_minW, min(m_maxW, m_width / scaleFactor)) * scaleFactor;
			//if left border drag, adjust position
			if(left)
				SetX(m_x - recipAmount);
        }

        void BaseWindow::OnResizeV(bool top)
        {
	        if(HasStyleFlags(NoResizeV))
		        return;

	        //if new size is too small
			int newHeight;
			if(top)
				newHeight = GetY() + GetHeight() - m_vLastMousePos.y;
			else
				newHeight = m_vLastMousePos.y - GetY();
			const int amount = newHeight - GetHeight();

			const float recipAmount = amount / (float)m_pModule->GetScreenSize().y;

			const float scaleFactor = m_height / GetHeight();
	        SetHeight(m_height + recipAmount);
			m_height = max(m_minH, min(m_maxH, m_height/scaleFactor)) * scaleFactor;
			if(top)
				SetY(m_y - recipAmount);
        }

        void BaseWindow::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData grData;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Window");

	        //draw window backs
	        grData.rDestRect.Set(0, 1, width, height-2);
	        grData.rSrcRect = element.GetPart(L"Back");
	        m_vRenderData.push_back(grData);

	        //draw topbar, if any
			if(HasStyleFlags(TopBar))
			{
				// access layout
				const Element& element = *m_pModule->m_layout.GetElement(L"WindowTopbar");

				//draw left
				const math::Rect rLeft = element.GetPart(L"Left");

				grData.rDestRect.Set(0, 0, rLeft.width, rLeft.height);
				if(!HasStyleFlags(Maximized))
					grData.rSrcRect = rLeft;
				else
					grData.rSrcRect.Set(16, 0, 32, 24);
				m_vRenderData.push_back(grData);

				//draw right
				const math::Rect rRight = element.GetPart(L"Right");

				grData.rDestRect.Set(width - rRight.width, 0, rRight.width, rRight.height);
				if(!HasStyleFlags(Maximized))
					grData.rSrcRect = rRight;
				else
					grData.rSrcRect.Set(16, 0, 32, 24);
				m_vRenderData.push_back(grData);

				//draw middle
				const math::Rect rMiddle = element.GetPart(L"Center");

				grData.rDestRect.Set(rLeft.width, 0, width - rLeft.width - rRight.width, rMiddle.height);
				grData.rSrcRect = rMiddle;
				m_vRenderData.push_back(grData);
			}
			else
			{
				//draw top left corner
				const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
				grData.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
				grData.rSrcRect = rTopLeft;
				m_vRenderData.push_back(grData);

				//draw top left corner
				const math::Rect& rTopRight = element.GetPart(L"TopRight");
				grData.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
				grData.rSrcRect = rTopRight;
				m_vRenderData.push_back(grData);
				
				//draw top bar
				const math::Rect& rTop = element.GetPart(L"Top");
				grData.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
				grData.rSrcRect = rTop;
				m_vRenderData.push_back(grData);
			}

	        //draw border, if any
	        if(HasStyleFlags(Border) && !HasStyleFlags(Maximized)) 
	        {
				const math::Rect& rBottom = element.GetPart(L"Bottom");
				const math::Rect& rLeft = element.GetPart(L"Left");
				const math::Rect& rRight = element.GetPart(L"Right");

				//draw left shape
				grData.rDestRect.Set(0, m_topBorder, rLeft.width, height - m_topBorder - rBottom.height);
		        grData.rSrcRect = rLeft;
		        m_vRenderData.push_back(grData);

		        //draw bottom left corner
				const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
		        grData.rDestRect.Set(0, height-rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
		        grData.rSrcRect = rBottomLeft;
		        m_vRenderData.push_back(grData);

		        //draw bottom shape
		        grData.rDestRect.Set(rBottomLeft.width, height-rBottom.height, width-rLeft.width-rRight.width, rBottom.height);
		        grData.rSrcRect = rBottom;
		        m_vRenderData.push_back(grData);

		        //draw bottom right corner
				const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
				grData.rDestRect.Set(width - rBottom.width, height - rBottom.height, rBottomRight.width, rBottomRight.height);
		        grData.rSrcRect = rBottomRight;
		        m_vRenderData.push_back(grData);

		        //draw right shape
				grData.rDestRect.Set(width - rRight.width, m_topBorder, rRight.width, height - m_topBorder - rRight.height);
		        grData.rSrcRect = rRight;
		        m_vRenderData.push_back(grData);
	        }

	        //draw caption
			if(!m_stName.empty())
			{
				grData.stLabel = m_stName;
				grData.labelFlags = DT_VCENTER;
				grData.rDestRect.Set(8, 0, width - 16, 24);
				m_vRenderData.push_back(grData);
			}
        }

        void BaseWindow::OnMove(const math::Vector2& vMousePos)
        {
	        Widget::OnMove(vMousePos);
	        //if dragging, use this border, else pick accordingly
	        WindowBorders border = m_dragBorder;
	        if(border == WindowBorders::None)
		        border = CheckBorder();

	        CursorState state = CursorState::IDLE;
	
	        //check which border the mouse is over and if this direction allows resizing
	        if(bit::contains(border , WindowBorders::Bottom)  && !HasStyleFlags(NoResizeV))
	        {
		        if(!HasStyleFlags(NoResizeH))
		        {
			        if(bit::contains(border , WindowBorders::Left))
				        state = CursorState::DRAG_LD;
			        else if(bit::contains(border , WindowBorders::Right))
				        state = CursorState::DRAG_RD;
			        else
				        state = CursorState::DRAG_V;
		        }
		        else
			        state = CursorState::DRAG_V;
	        }
	        else if(border != WindowBorders::None && !HasStyleFlags(NoResizeH))
	        {
		        state = CursorState::DRAG_H;
	        }

	        //send cursor state change signal
	        SigChangeCursorState(state);
        }

		void BaseWindow::OnExecute(void)
		{
			SigExecute();
		}

		void BaseWindow::OnClose(void)
		{
			if(IsVisible())
			{
				OnActivate(false);
				if(!HasStyleFlags(StayVisibleClosed))
					OnInvisible();
				SigClose();
			}
		}

		void BaseWindow::OnLayoutChanged(const Layout& layout)
		{
			if(HasStyleFlags(Border))
			{
				const auto& window = *layout.GetElement(L"Window");

				m_leftBorder = window.GetPart(L"Left").width;
				m_rightBorder = window.GetPart(L"Right").width;
				m_bottomBorder = window.GetPart(L"Bottom").height;

				if(!HasStyleFlags(Maximized))
					m_area.SetPadding(m_leftBorder, 24, m_leftBorder + m_rightBorder, 24 + m_bottomBorder);
			}
		}

        WindowBorders BaseWindow::CheckBorder(void) const
        {
	        int area = 0;
	        //check for horizontal resize
	        if(m_vLastMousePos.x <= GetX() + m_leftBorder)
				area |= (int)WindowBorders::Left;//left border
			else if(m_vLastMousePos.x >= GetX() + GetWidth() - (m_leftBorder + m_rightBorder))
				area |= (int)WindowBorders::Right;

			//check for vertical resize
			if(m_vLastMousePos.y >= GetY() + GetHeight() - (m_topBorder + m_bottomBorder))
				area |= (int)WindowBorders::Bottom;
			else if(m_vLastMousePos.y <= GetY() + m_bottomBorder)
				area |= (int)WindowBorders::Top;
				
	        return (WindowBorders)area;
        }

		int BaseWindow::Execute(void)
		{
			OnVisible();
			PushToTop();
			m_pModule->ExecuteWidget(*this);
			return 0;
		}

		void BaseWindow::ChangeArea(void)
		{
			// area
			if(HasStyleFlags(TopBar))
				m_topBorder = 24;
			else
				m_topBorder = 8;

			if(!HasStyleFlags(Border) || HasStyleFlags(Maximized))
				m_bottomBorder = 0;
			else
				m_bottomBorder = m_pModule->m_layout.GetElement(L"Window")->GetPart(L"Bottom").height;

			m_area.SetPadding(PaddingSide::TOP, m_topBorder);
			m_area.SetPadding(PaddingSide::BOTTOM, m_topBorder + m_bottomBorder);
		}

    }
}