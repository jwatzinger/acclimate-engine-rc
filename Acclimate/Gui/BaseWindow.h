#pragma once
#include "Widget.h"
#include "Area.h"

namespace acl
{
	namespace gui
	{

		enum WindowStyleFlags 
		{
			Border				= 0x01,
			TopBar				= 0x02,
			Maximized			= 0x04,
			NoResizeH			= 0x08,
			NoResizeV			= 0x10,
			NoResize			= 0x18,
			StayVisibleClosed	= 0x20,
			NoClose				= 0x40,
			NoMaximize			= 0x80,
		};

		enum class WindowBorders : char 
		{
			None			= 0x00,
			Left			= 0x01,
			Right			= 0x02,
			Bottom			= 0x04,
			LeftBottom		= 0x05,
			RightBottom		= 0x06,
			Top				= 0x08,
			TopLeft			= 0x09,
			TopRight		= 0x0A,
		};

		class Area;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;

		class ACCLIMATE_API BaseWindow :
			public Widget
		{
		public:
			BaseWindow(float x, float y, float width, float height, const std::wstring& stName=L"Untitled");

			void AddChild(Widget& child) override;
	        void RemoveChild(Widget& child) override;

			void SetAreaClipping(bool bClip);
			void SetStyleFlags(unsigned char cStyleFlags);
			void SetLabel(const std::wstring& stName);
			void RemoveStyleFlags(unsigned char cStyleFlags);
			bool HasStyleFlags(unsigned char cStyleFlags) const;

			const std::wstring& GetName(void) const;

			//events
			void OnResizeH(bool left = false);
			void OnResizeV(bool top = false);
			void OnMove(const math::Vector2& vMousePos) override;
			void OnClick(MouseType mouse) override;
			void OnDrag(const math::Vector2& vDistance) override;
			void OnRelease(MouseType mouse, bool bMouseOver) override;
			void OnExecute(void) override;
			void OnRedraw(void) override;
			void OnLayoutChanged(const Layout& layout) override;
			void OnClose(void) override;

			virtual int Execute(void);

			core::Signal<> SigExecute;

		protected:
#pragma warning( disable: 4251 )

			virtual void ChangeArea(void);

			WindowBorders CheckBorder(void) const;
			WindowBorders m_dragBorder;

			Area m_area;
			
			int m_leftBorder, m_rightBorder, m_bottomBorder, m_topBorder;
			bool m_bResizing;
			unsigned char m_cStyleFlags;
			std::wstring m_stName;
			math::Vector2 m_vDragDist;
#pragma warning( default: 4251 )
		};

	}
}


