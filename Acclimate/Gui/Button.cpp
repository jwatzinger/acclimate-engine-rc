#include "Button.h"

namespace acl
{
	namespace gui
	{

		Button::Button(float x, float y, float width, float height, const std::wstring& stLabel) : 
			BaseButton(x, y, width, height), m_stLabel(stLabel)
		{	
		}

        void Button::SetLabel(const std::wstring& stLabel)
        {
            m_stLabel = stLabel;
            m_bDirty = true;
        }

		void Button::OnRedraw(void)
		{
			BaseButton::OnRedraw();
			//draw caption
			RenderData grData;
			grData.stLabel = m_stLabel;
			grData.labelFlags = DT_VCENTER | DT_CENTER;
			grData.rDestRect.Set(0, 0, GetWidth(), GetHeight());
			m_vRenderData.push_back(grData);
		}

	}
}
