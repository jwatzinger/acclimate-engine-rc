#pragma once
#include "BaseButton.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API Button :
			public BaseButton
		{
		public:
			Button(float x, float y, float width, float height, const std::wstring& stLabel);

            void SetLabel(const std::wstring& stLabel);

			void OnRedraw(void) override;

		private:
#pragma warning( disable: 4251 )
			std::wstring m_stLabel;
#pragma warning( default: 4251 )
		};

	}
}
