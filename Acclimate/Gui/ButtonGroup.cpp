#include "ButtonGroup.h"
#include "ToolButton.h"

namespace acl
{
    namespace gui
    {

        ButtonGroup::ButtonGroup(void): Widget(0.0f, 0.0f, 1.0, 1.0f)
        {
        }

		ButtonGroup::~ButtonGroup(void)
		{
			for(auto pButton: m_vButtons)
			{
				delete pButton;
			}
		}

        void ButtonGroup::AddButton(ToolButton& button)
        {
	        if(std::find(m_vButtons.begin(), m_vButtons.end(), &button) == m_vButtons.cend())
	        {
		        button.SetPadding(m_vButtons.size()*32, 0, 0, 0);
		        m_vButtons.push_back(&button);
		        AddChild(button);
	        }
        }

        int ButtonGroup::NumButtons(void) const
        {
	        return m_vButtons.size();
        }

    }
}