#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{
		class ToolButton;

		class ButtonGroup : 
			public Widget
		{
			typedef std::vector<ToolButton*> ButtonVector;

		public:
			ButtonGroup(void);
			~ButtonGroup(void);

			void AddButton(ToolButton& button);

			int NumButtons(void) const;

		private:

			ButtonVector m_vButtons;
		};

	}
}
