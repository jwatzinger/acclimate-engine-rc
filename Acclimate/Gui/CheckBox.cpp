#include "CheckBox.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		CheckBox::CheckBox(float x, float y, float size): Widget(x, y, size, size), m_bChecked(false)
		{
		}

		bool CheckBox::IsChecked(void) const
		{
			return m_bChecked;
		}

		void CheckBox::OnCheck(bool bChecked)
		{
			if (m_bChecked != bChecked)
			{
				m_bChecked = bChecked;
				SigChecked(m_bChecked);

				m_bDirty = true;
			}
		}

		void CheckBox::OnClick(MouseType mouse)
		{
			if(mouse == MouseType::LEFT)
			{
				m_bChecked = !m_bChecked;
				SigChecked(m_bChecked);

				m_bDirty = true;
			}
		}

		void CheckBox::OnRedraw(void)
		{
			m_vRenderData.clear();
		    RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Checkbox");
				
			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
		    Data.rSrcRect = rTopLeft;
		    Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
		    m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
		    Data.rSrcRect = rTopRight;
		    Data.rDestRect.Set(width-rTopRight.width, 0, rTopRight.width, rTopRight.height);
		    m_vRenderData.push_back(Data);

			// top
			const math::Rect& rTop = element.GetPart(L"Top");
		    Data.rSrcRect = rTop;
		    Data.rDestRect.Set(rTopLeft.width, 0, width-rTopLeft.width-rTopRight.width, rTop.height);
		    m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
		    Data.rSrcRect = rBottomLeft;
		    Data.rDestRect.Set(0, height-rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
		    m_vRenderData.push_back(Data);
			
		    // left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
		    Data.rSrcRect = rLeft;
		    Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height-rTopLeft.height-rBottomLeft.height);
		    m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
		    Data.rSrcRect = rBottomRight;
		    Data.rDestRect.Set(width-rBottomRight.width, height-rBottomRight.height, rBottomRight.width, rBottomRight.height);
		    m_vRenderData.push_back(Data);

		    // bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
		    Data.rSrcRect = rBottom;
		    Data.rDestRect.Set(rBottomLeft.width, height-rBottom.height, width-rBottomLeft.width-rBottomRight.width, rBottom.height);
		    m_vRenderData.push_back(Data);

		    // right bar
			const math::Rect& rRight = element.GetPart(L"Right");
		    Data.rSrcRect = rRight;
		    Data.rDestRect.Set(width-rRight.width, rTopRight.height, rRight.width, height-rTopRight.height-rBottomRight.height);
		    m_vRenderData.push_back(Data);

		    // background
			const math::Rect& rBack = element.GetPart(L"Back");
		    Data.rSrcRect = rBack;
		    Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width-rTopLeft.width-rBottomRight.width, height-rTopLeft.height-rBottomRight.width);
		    m_vRenderData.push_back(Data);
			
			// checked
			if(m_bChecked)
			{
				const math::Rect& rCross = element.GetPart(L"Cross");
				Data.rSrcRect = rCross;
				Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width-rBottomLeft.width-rBottomRight.width, height-rBottomLeft.height-rBottomRight.height);
				m_vRenderData.push_back(Data);
			}
		}

		void CheckBox::OnParentResize(int x, int y, int width, int height)
		{
			Widget::OnParentResize(x, y, width, height);

			if(m_absWidth > m_absHeight)
				m_absWidth = m_absHeight;
			else
				m_absHeight = m_absWidth;
		}

	}
}
