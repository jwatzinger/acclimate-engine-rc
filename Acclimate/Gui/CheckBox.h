#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<bool>;

		class ACCLIMATE_API CheckBox : 
			public Widget
		{
		public:
			CheckBox(float x, float y, float size);

			bool IsChecked(void) const;

			void OnCheck(bool bChecked);
			void OnClick(MouseType mouse) override;
			void OnRedraw(void) override;
			void OnParentResize(int x, int y, int width, int height) override;

			core::Signal<bool> SigChecked;

		private:

			bool m_bChecked;
		};

	}
}
