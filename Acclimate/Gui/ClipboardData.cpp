#include "ClipboardData.h"

namespace acl
{
	namespace gui
	{

		ClipboardTextData::ClipboardTextData(const std::wstring& stText) : stText(stText)
		{
		}

		const std::wstring& ClipboardTextData::GetText(void) const
		{
			return stText;
		}

	}
}

