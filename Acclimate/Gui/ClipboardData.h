#pragma once
#include <string>
#include "..\System\ClipboardData.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API ClipboardTextData :
			public sys::ClipboardData<ClipboardTextData>
		{
		public:

			ClipboardTextData(const std::wstring& stText);

			const std::wstring& GetText(void) const;
		private:
#pragma warning( disable: 4251 )
			std::wstring stText;
#pragma warning( default: 4251 )
		};

		USE_CLIPBOARD_DATA(ClipboardTextData);

	}
}


