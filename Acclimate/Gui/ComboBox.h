#pragma once
#include "ScrollArea.h"
#include "ComboItem.h"
#include "Textbox.h"
#include "Icon.h"
#include "ToolButton.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace gui
    {

        template<typename I>
        class ComboBox :
	        public Widget
        {
	        typedef std::vector<ComboItem<I>*> ItemVector;

        public:

	        ComboBox(float x, float y, float width, float height, float dropDownMaxHeight): Widget(x, y, width, height), m_maxDropDownHeight(dropDownMaxHeight),
				m_box(0.0f, 0.0f, 1.0f, 1.0f, L""), m_dropDown(0.0f, 1.0f, 1.0f, 2.0f, 0, 1), m_pOverItem(false), m_pIcon(nullptr)
	        {
				m_maxDropDownHeight = max(2.0f, m_maxDropDownHeight);
				m_focus = FocusState::KEEP_FOCUS;

		        //text box
                m_box.SigReleased.Connect(this, &ComboBox::OnExpand);
                m_box.SetEditable(false);
		        AddChild(m_box);

		        //drop down button
		        Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", math::Rect(40, 40, 16, 16));
		        m_pButton = new ToolButton(0.0f, 0.0f, pIcon);
				m_pButton->SetAlignement(HorizontalAlign::RIGHT, VerticalAlign::TOP);
				m_pButton->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
		        m_pButton->SigReleased.Connect(this, &ComboBox::OnExpand);
		        AddChild(*m_pButton);

		        //drop down scroll area
		        m_dropDown.OnInvisible();
				AddChild(m_dropDown);
	        }

			~ComboBox(void)
			{
				for(auto pItem : m_vItems)
				{
					delete pItem;
				}
				delete m_pButton;
				delete m_pIcon;
			}

	        ComboItem<I>& AddItem(const std::wstring& stName, I item)
	        {
		        const int height = m_vItems.size()*20;

		        ComboItem<I>* pItem = new ComboItem<I>(0.0f, 0.0f, 1.0f, stName, item);
				pItem->SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
				pItem->SigWheelMoved.Connect(&m_dropDown, &ScrollArea::OnWheelMove);
		        pItem->SigPicked.Connect(this, &ComboBox::OnPickItem);
				pItem->SetPadding(0, height, 0, 0);
				pItem->SigMoveOver.Connect(this, &ComboBox::OnItemOver);

				m_dropDown.SetAreaHeight(height);
				m_dropDown.AddChild(*pItem);

				// mark previous last item not last
				if(!m_vItems.empty())
					m_vItems.back()->SetLast(false);

		        m_vItems.push_back(pItem);

				const float size = (float)m_vItems.size();
				m_dropDown.SetHeight(min(m_maxDropDownHeight, size));
				m_dropDown.SetScrollbar(size > m_maxDropDownHeight);

				return *pItem;
	        }

	        I GetContent(void) const
	        {
		        return m_selectedItem;
	        }

			size_t GetSize(void) const
			{
				return m_vItems.size();
			}

            void Clear(void)
            {
                for(ComboItem<I>* pItem : m_vItems)
                {
                    delete pItem;
                }

                m_vItems.clear();

				m_dropDown.OnParentResize(GetX(), GetY(), GetWidth(), GetHeight());
            }

            void SelectByName(const std::wstring& stName)
            {
                for(auto pItem : m_vItems)
                {
                    const std::wstring& stItemName( pItem->GetName() );
                    if( stItemName == stName )
                    {
						OnPickItem(*pItem);
						return;
                    }
                }
            }

			void SelectByItem(I item)
			{
				for(auto pItem : m_vItems)
				{
					if(pItem->GetItem() == item)
					{
						OnPickItem(*pItem);
						return;
					}
				}
			}

			void Select(size_t id)
			{
				if (m_vItems.size() > id)
				{
					auto itr = m_vItems.at(id);
					OnPickItem(*itr);
				}
			}

	        core::Signal<I> SigItemPicked;

            void OnFocus(bool bFocus) override
            {
                Widget::OnFocus(bFocus);

                if(!bFocus)
					m_dropDown.OnInvisible();
            }

			void OnKeyDown(Keys key) override
			{
				switch(key)
				{
				case Keys::ENTER:
					SelectOver();
					break;
				case Keys::UP_ARROW:
					MoveSelection(-1);
					break;
				case Keys::DOWN_ARROW:
					MoveSelection(1);
					break;
				}
			}

			void UpdateChildren(void) override
			{
				Widget::UpdateChildren();
				m_box.SetPadding(0, 0, m_pButton->GetWidth(), 0);
			}

        private:

			void SelectOver(void)
			{
				if(m_pOverItem)
					m_pOverItem->OnMoveOn();
				else
					OnExpand();
			}

			void MoveSelection(int direction)
			{
				if(m_pOverItem)
				{
					auto itr = std::find(m_vItems.begin(), m_vItems.end(), m_pOverItem);
					ACL_ASSERT(itr != m_vItems.end());

					if((direction < 0 && itr != m_vItems.begin()) ||
						(direction > 0 && itr != m_vItems.end()))
						itr += direction;
					if(itr != m_vItems.end())
						(*itr)->OnMoveOn();
				}
				else if(!m_vItems.empty())
					OnItemOver(*m_vItems.front());
			}

	        void OnExpand(void)
	        {
				if(m_dropDown.IsVisible())
				{
					m_dropDown.OnInvisible();
					if(m_pOverItem)
						m_pOverItem->Unselect();
					m_pOverItem = false;
				}
		        else
		        {
					m_dropDown.OnVisible();
					m_dropDown.PushToTop();
                    OnFocus(true);
		        }
	        }

	        void OnPickItem(ComboItem<I>& item)
	        {
                I& tempItem = item.GetItem();
		        SigItemPicked(tempItem);
		        m_box.SetText(item.GetName());

		        m_selectedItem = tempItem;

				if (m_dropDown.IsVisible())
		            OnExpand();
	        }

			void OnItemOver(ComboItem<I>& item)
			{
				if(m_pOverItem)
					m_pOverItem->Unselect();

				m_pOverItem = &item;
			}

			float m_maxDropDownHeight;

	        ItemVector m_vItems;
	        I m_selectedItem;
			
			// widgets

			Textbox<std::wstring> m_box;
			ToolButton* m_pButton;
			ScrollArea m_dropDown;
			ComboItem<I>* m_pOverItem;
			Icon* m_pIcon;

        };

    }
}
