#pragma once
#include "Widget.h"
#include "Label.h"
#include "Layout.h"
#include "Input.h"
#include "Icon.h"

namespace acl
{
    namespace gui
    {

        template<typename I>
        class ComboItem :
	        public Widget
        {
        public:
	        ComboItem(float x, float y, float width, const std::wstring& stName, I item): Widget(x, y, width, 0.019f), m_item(item), m_stName(stName),
				m_label(0, 0, 1.0f, 1.0f, stName.c_str()), m_bOver(false), m_isLast(true), m_pIcon(nullptr)
	        {
		        m_label.OnDisable();
		        m_label.SetAlign(LEFT | VCENTER);
				m_label.SetPadding(gui::PaddingSide::LEFT, 8);
				m_label.SetPadding(gui::PaddingSide::RIGHT, 8);
		        AddChild(m_label);
	        }

			~ComboItem(void)
			{
				delete m_pIcon;
			}

			void OnMoveOn(void) override
			{
				if(!m_bOver)
				{
					m_bOver = true;
					m_bDirty = true;

					SigMoveOver(*this);
				}
			}

			void SetLast(bool isLast)
			{
				if(m_isLast != isLast)
				{
					m_isLast = isLast;
					m_bDirty = true;
				}
			}

			Icon* SetIcon(const std::wstring& stName, const math::Rect& rSrc)
			{
				delete m_pIcon;

				if(!stName.empty())
				{
					m_pIcon = new Icon(0.0f, 0.0f, 1.0f, stName, rSrc);
					m_pIcon->SetPadding(gui::PaddingSide::LEFT, 8);
					AddChild(*m_pIcon);

					m_label.SetPadding(gui::PaddingSide::LEFT, m_pIcon->GetWidth() + 16);
				}
				else
				{
					m_pIcon = nullptr;
					m_label.SetPadding(gui::PaddingSide::LEFT, 0);
				}

				m_bDirty = true;

				return m_pIcon;
			}

			void Unselect(void)
			{
				m_bOver = false;
				m_bDirty = true;
			}

            const std::wstring& GetName(void) const
            {
                return m_stName;
            }

            I& GetItem(void)
            {
                return m_item;
            }
			
			bool IsOver(void)  const
			{
				return m_bOver;
			}

	        void OnRelease(MouseType mouse, bool bMouseOver) override
	        {
		        Widget::OnRelease(mouse, bMouseOver);
		        if(mouse == MouseType::LEFT && bMouseOver)
			        SigPicked(*this);
	        }

	        void OnRedraw(void) override
	        {
				const auto width = GetWidth();
				const auto height = GetHeight();

				m_vRenderData.clear();
				RenderData data;

				auto& element = *m_pModule->m_layout.GetElement(L"ComboDropdown");

				// left
				auto& left = element.GetPart(L"Left");
				data.rSrcRect = left;
				data.rDestRect.Set(0, 0, left.width, height);
				m_vRenderData.push_back(data);
				
				// right
				auto& right = element.GetPart(L"Right");
				data.rSrcRect = right;
				data.rDestRect.Set(width - right.width, 0, right.width, height);
				m_vRenderData.push_back(data);

				// bottom
				unsigned int heightOffset = 0;
				if(m_isLast)
				{
					auto& bottom = element.GetPart(L"Bottom");
					data.rSrcRect = bottom;
					data.rDestRect.Set(left.width, height - bottom.height, width - left.width - right.width, bottom.height);
					m_vRenderData.push_back(data);

					heightOffset = bottom.height;
				}

				data.rDestRect.Set(left.width, 0, width - left.width - right.width, height - heightOffset);
		        data.rSrcRect.Set(24, 56, 16, 16);
		        m_vRenderData.push_back(data);

				if(m_bOver)
				{
					data.rSrcRect.Set(0, 88, 8, 8);
					m_vRenderData.push_back(data);
				}
	        }

			void OnLayoutChanged(const Layout& layout) override
			{
				m_label.SetColor(&layout.GetElement(L"Combobox")->GetColor(L"ItemText"));
			}

	        core::Signal<ComboItem<I>&> SigPicked;
			core::Signal<ComboItem<I>&> SigMoveOver;

        private:

			bool m_bOver, m_isLast;
	        const std::wstring m_stName;

	        I m_item;
			Label m_label;
			Icon* m_pIcon;
        };

    }
}