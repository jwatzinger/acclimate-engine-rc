#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class Module;
		class LayoutLoader;
		class ShortcutRegistry;
		class Renderer;
		class InputHandler;

		struct ACCLIMATE_API Context
		{
			Context(Module& module, LayoutLoader& loader, ShortcutRegistry& shortcuts, InputHandler& input): module(module), loader(loader),
				shortcuts(shortcuts), input(input), pRenderer(nullptr) {}

			Module& module;
			LayoutLoader& loader;
			ShortcutRegistry& shortcuts;
			InputHandler& input;
			Renderer* pRenderer;
		};

	}
}