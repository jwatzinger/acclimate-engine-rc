#include "ContextMenu.h"
#include "Module.h"
#include "ContextMenuItem.h"
#include "Input.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		ContextMenu::ContextMenu(float itemHeight) : Widget(0.0f, 0.0f, 0.0f, itemHeight),
			m_itemHeight(itemHeight)
		{
			SetPositionModes(PositionMode::ABS, PositionMode::ABS, PositionMode::ABS, PositionMode::REF);

			m_bVisible = false;
			m_focus = FocusState::KEEP_FOCUS;
		}

		ContextMenu::~ContextMenu(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
		}

		ContextMenuItem& ContextMenu::AddItem(const std::wstring& stName)
		{
			const size_t numItems = m_vItems.size();

			auto pItem = new ContextMenuItem(m_itemHeight*numItems, m_itemHeight, stName);
			pItem->SigReleased.Connect(this, &ContextMenu::OnInvisible);
			m_vItems.push_back(pItem);
			AddChild(*pItem);

			SetHeight(m_itemHeight*(numItems + 1));

			return *pItem;
		}

		void ContextMenu::RemoveItem(const std::wstring& stName)
		{
			for(auto itr = m_vItems.begin(); itr != m_vItems.end(); ++itr)
			{
				if((*itr)->GetName() == stName)
				{
					delete *itr;
					m_vItems.erase(itr);

					SetHeight(m_itemHeight*m_vItems.size());
					return;
				}
			}
		}

		void ContextMenu::Clear(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
			m_vItems.clear();
		}

		void ContextMenu::PopUp(const math::Vector2& vPos)
		{
			auto pMainWidget = &m_pModule->GetMainWidget();
			if(m_pParent != pMainWidget)
				SetParent(pMainWidget);

			PushToTop();
			OnVisible();
			OnFocus(true);

			// adjust width

			m_width = 0;
			unsigned int shortcut = 0;
			for(auto pItem : m_vItems)
			{
				m_width -= shortcut;
				shortcut = max(shortcut, pItem->GetShortcutWidth());
				m_width += shortcut;

				SetWidth(max(m_width, pItem->GetContentWidth() + shortcut * 2));
			}

			const math::Vector2& vScreenSize = m_pModule->GetScreenSize();
			if(vPos.x + m_width < vScreenSize.x)
				SetX((float)vPos.x);
			else
				SetX((float)vPos.x - m_width);
				
			if(vPos.y + m_height < vScreenSize.y)
				SetY((float)vPos.y);
			else
				SetY((float)vPos.y - m_height);
		}

		void ContextMenu::InsertSeperator(void)
		{
			m_vSeperator.push_back(m_vItems.size());
			m_bDirty = true;
		}

		void ContextMenu::Execute(const math::Vector2& vPos)
		{
			PopUp(vPos);
			m_pModule->ExecuteWidget(*this);
		}

		void ContextMenu::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Contextmenu");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			Data.rSrcRect = rTopRight;
			Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(Data);

			// top
			const math::Rect& rTop = element.GetPart(L"Top");
			Data.rSrcRect = rTop;
			Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
			m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			Data.rSrcRect = rBottomLeft;
			Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			Data.rSrcRect = rBottomRight;
			Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(Data);

			// bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
			Data.rSrcRect = rBottom;
			Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(Data);

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// icon area
			const math::Rect& rArea = element.GetPart(L"IconArea");
			const unsigned int itemWidth = (unsigned int)((m_itemHeight * m_pModule->m_layout.GetReferenceSize().y) * 1.1f) - 1;
			Data.rSrcRect = rArea;
			Data.rDestRect.Set(rTopLeft.width, rTop.height, itemWidth, height - rBottomRight.height - rTop.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width+itemWidth, rTopLeft.height, width - rTopLeft.width - rBottomRight.width - itemWidth, height - rTopLeft.height - rBottomRight.width);
			m_vRenderData.push_back(Data);

			for(auto position : m_vSeperator)
			{
				Data.rDestRect.Set(itemWidth + 5, (int)(position*m_itemHeight*m_pModule->m_layout.GetReferenceSize().y), width - itemWidth - 10, 1);
				Data.rSrcRect = rTop;
				m_vRenderData.push_back(Data);
			}
		}

		void ContextMenu::OnFocus(bool bFocus)
		{
			if(!bFocus)
				OnInvisible();

			Widget::OnFocus(bFocus);
		}

		void ContextMenu::OnKeyDown(Keys key)
		{
			switch(key)
			{
			case Keys::ESC:
				OnInvisible();
				break;
			}

			Widget::OnKeyDown(key);
		}

	}
}
