#pragma once
#include "ContextMenuItem.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API ContextMenu :
			public Widget
		{
			typedef std::vector<ContextMenuItem*> ItemVector;
			typedef std::vector<unsigned int> SeperatorVector;
		public:
			ContextMenu(float itemHeight);
			~ContextMenu(void);

			ContextMenuItem& AddItem(const std::wstring& stName);
			void RemoveItem(const std::wstring& stName);

			void InsertSeperator(void);
			void Clear(void);

			void PopUp(const math::Vector2& vPos);
			void Execute(const math::Vector2& vPos);

			void OnRedraw(void) override;
			void OnFocus(bool bFocus) override;
			void OnKeyDown(Keys key) override;

		private:
#pragma warning( disable: 4251 )
			float m_itemHeight;
			ItemVector m_vItems;
			SeperatorVector m_vSeperator;
#pragma warning( default: 4251 )
		};

	}
}


