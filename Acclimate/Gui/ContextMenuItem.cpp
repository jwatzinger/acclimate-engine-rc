#include "ContextMenuItem.h"
#include "Module.h"
#include "ContextMenu.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		ContextMenuItem::ContextMenuItem(float y, float height, const std::wstring& stName) : 
			Widget(0.0f, y, 1.0f, height), m_stName(stName), m_bOver(false)
		{
			SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
			SetPadding(2, 1, 4, 1);
		}

		unsigned int ContextMenuItem::GetContentWidth(void) const
		{
			return m_pModule->CalculateTextRect(m_stName).width + 46 + 22;
		}

		unsigned int ContextMenuItem::GetShortcutWidth(void) const
		{
			if(m_pShortcut)
				return m_pModule->CalculateTextRect(ShortcutToString(*m_pShortcut)).width;
			else
				return 0;
		}

		const std::wstring& ContextMenuItem::GetName(void) const
		{
			return m_stName;
		}

		void ContextMenuItem::OnRedraw(void)
		{
			m_vRenderData.clear();
			//don't draw if no text
			if(m_stName.empty())
				return;
			
			int width = GetWidth(), height = GetHeight();
			RenderData grData;
			if(m_bOver)
			{
				grData.rDestRect.Set(0, 0, width, height);
				grData.rSrcRect.Set(32, 88, 8, 8);
				m_vRenderData.push_back(grData);
			}

			// text
			grData.stLabel = m_stName;
			grData.labelColor = gfx::Color(255, 255, 255);
			grData.labelFlags = DT_LEFT | DT_VCENTER;
			grData.rDestRect.Set((int)(height * 1.5f), 0, width-height, height);
			m_vRenderData.push_back(grData);

			// visualize shortcut
			if(m_pShortcut)
			{
				grData.stLabel = ShortcutToString(*m_pShortcut);
				const int textWidth = m_pModule->CalculateTextRect(grData.stLabel).width + 6;
				grData.rDestRect = math::Rect(width - textWidth, 0, textWidth, height);
				m_vRenderData.push_back(grData);
			}
		}

		void ContextMenuItem::OnMoveOn(void)
		{
			m_bOver = true;
			m_bDirty = true;
		}

		void ContextMenuItem::OnMoveOff(void)
		{
			m_bOver = false;
			m_bDirty = true;
		}

	}
}

