#pragma once
#include "Widget.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API ContextMenuItem :
			public Widget
		{
		public:
			ContextMenuItem(float y, float height, const std::wstring& stName);

			unsigned int GetContentWidth(void) const;
			unsigned int GetShortcutWidth(void) const;
			const std::wstring& GetName(void) const;

			void OnRedraw(void) override;
			void OnMoveOn(void) override;
			void OnMoveOff(void) override;

		private:
#pragma warning( disable: 4251 )
			bool m_bOver;
			std::wstring m_stName;
#pragma warning( default: 4251 )
		};

	}
}


