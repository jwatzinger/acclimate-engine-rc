#include "Cursor.h"
#include "CursorImplementation.h"
#include "..\Math\Vector.h"
#include "..\System\Log.h"

namespace acl
{
    namespace gui
    {

		Cursor::Cursor(void) : m_vPosition(0, 0), m_vCursors((size_t)CursorState::SIZE),
			m_pActiveCursor(nullptr), m_isVisible(true)
        {
	        ChangeState(CursorState::IDLE);
        }

		Cursor::~Cursor(void)
		{
			for(auto pCursor : m_vCursors)
			{
				delete pCursor;
			}
		}

        void Cursor::SetPosition(const math::Vector2& vPosition)
        {
            m_vPosition = vPosition;
        }

		void Cursor::SetState(CursorState state, gfx::ITexture& texture, unsigned int offX, unsigned int offY)
		{
			if(auto pCursor = m_vCursors[(size_t)state])
				delete pCursor;
			
			m_vCursors[(size_t)state] = new CursorImplementation(texture, offX, offY);

			if(!m_pActiveCursor && state == m_state)
				ActivateCursor(*m_vCursors[(size_t)state]);
		}

		void Cursor::SetVisible(bool isVisible)
		{
			if(m_isVisible != isVisible)
			{
				if(m_pActiveCursor)
				{
					if(isVisible)
						m_pActiveCursor->Activate();
					else
						m_pActiveCursor->Hide();
				}

				m_isVisible = isVisible;
			}
		}

        math::Vector2 Cursor::GetPosition(void) const
        {
            return m_vPosition;
        }

        void Cursor::ChangeState(CursorState newState)
        {
			if(m_state != newState)
			{
				m_state = newState;
				if(auto pCursor = m_vCursors[(size_t)newState])
					ActivateCursor(*pCursor);
				else
					sys::log->Out(sys::LogModule::GUI, sys::LogType::WARNING, "No cursor for state", (size_t)newState, "registered.");
				m_state = newState;
			}
        }

		bool Cursor::IsVisible(void) const
		{
			return m_isVisible;
		}

		void Cursor::ActivateCursor(CursorImplementation& cursor)
		{
			m_pActiveCursor = &cursor;
			m_pActiveCursor->Activate();
		}

		void Cursor::MoveTo(const math::Vector2& vPosition)
		{
			m_pActiveCursor->MoveTo(vPosition.x, vPosition.y);
		}

    }
}