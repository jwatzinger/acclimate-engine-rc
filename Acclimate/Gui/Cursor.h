#pragma once
#include <string>
#include <vector>
#include "..\Core\Dll.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

    namespace gui
    {
        enum class CursorState 
		{
	        IDLE = 0, DRAG_V, DRAG_H, DRAG_LD, DRAG_RD, TEXT_OVER, SIZE
        };

		class CursorImplementation;

        class ACCLIMATE_API Cursor
        {
			typedef std::vector<CursorImplementation*> CursorVector;
        public:
	        Cursor(void);
			~Cursor(void);

            void SetPosition(const math::Vector2& vector);
			void SetState(CursorState state, gfx::ITexture& texture, unsigned int offX, unsigned int offY);
			void SetVisible(bool isVisible);

            math::Vector2 GetPosition(void) const;
			bool IsVisible(void) const;

	        void ChangeState(CursorState newState);
			void MoveTo(const math::Vector2& vPosition);

        private:
#pragma warning( disable: 4251 )

			void ActivateCursor(CursorImplementation& cursor);

            math::Vector2 m_vPosition;
			bool m_isVisible;
			
	        CursorState m_state;
			CursorImplementation* m_pActiveCursor;
			CursorVector m_vCursors;
#pragma warning( default: 4251 )

        };

    }
}
