#include "CursorImplementation.h"
#include "..\Gfx\TextureAccessors.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gui
	{

		CursorImplementation::CursorImplementation(gfx::ITexture& texture, unsigned int offX, unsigned int offY):
			m_offX(offX), m_offY(offY)
		{
			const auto& vSize = texture.GetSize();

			ICONINFO iconinfo = { 0 };
			iconinfo.fIcon = FALSE;
			iconinfo.xHotspot = offX;
			iconinfo.yHotspot = offY;

			switch(texture.GetFormat())
			{
			case gfx::TextureFormats::X32:
			{
				HDC hDC = ::GetDC(NULL);
				HDC hAndMaskDC = ::CreateCompatibleDC(hDC);
				HDC hXorMaskDC = ::CreateCompatibleDC(hDC);

				HBITMAP hAndMaskBitmap = CreateCompatibleBitmap(hDC, vSize.x, vSize.y);
				HBITMAP hXorMaskBitmap = CreateCompatibleBitmap(hDC, vSize.x, vSize.y);

				//Select the bitmaps to DC
				HBITMAP hOldAndMaskBitmap = (HBITMAP)SelectObject(hAndMaskDC, hAndMaskBitmap);
				HBITMAP hOldXorMaskBitmap = (HBITMAP)SelectObject(hXorMaskDC, hXorMaskBitmap);

				gfx::TextureAccessorX32 accessor(texture, true);
				const gfx::Color3 transparent(254, 254, 254);
				for(unsigned int x = 0; x < (unsigned int)vSize.x; x++)
				{
					for(unsigned int y = 0; y < (unsigned int)vSize.y; y++)
					{
						auto& pixel = accessor.GetPixel(x, y);

						if(pixel == transparent)
						{
							SetPixel(hAndMaskDC, x, y, RGB(255, 255, 255));
							SetPixel(hXorMaskDC, x, y, RGB(0, 0, 0));
						}
						else
						{
							SetPixel(hAndMaskDC, x, y, RGB(0, 0, 0));
							SetPixel(hXorMaskDC, x, y, RGB(pixel.r, pixel.g, pixel.b));
						}
					}
				}

				SelectObject(hAndMaskDC, hOldAndMaskBitmap);
				SelectObject(hXorMaskDC, hOldXorMaskBitmap);

				DeleteDC(hXorMaskDC);
				DeleteDC(hAndMaskDC);

				ReleaseDC(NULL, hDC);

				iconinfo.hbmMask = hAndMaskBitmap;
				iconinfo.hbmColor = hXorMaskBitmap;

				m_hCursor = CreateIconIndirect(&iconinfo);

				break;
			}
			case gfx::TextureFormats::A32:
			{
				BITMAPV5HEADER bi;
				HBITMAP hBitmap;
				void *lpBits;

				ZeroMemory(&bi, sizeof(BITMAPV5HEADER));
				bi.bV5Size = sizeof(BITMAPV5HEADER);
				bi.bV5Width = vSize.x;
				bi.bV5Height = vSize.y;
				bi.bV5Planes = 1;
				bi.bV5BitCount = 32;
				bi.bV5Compression = BI_BITFIELDS;
				// The following mask specification specifies a supported 32 BPP
				// alpha format for Windows XP.
				bi.bV5RedMask = 0x00FF0000;
				bi.bV5GreenMask = 0x0000FF00;
				bi.bV5BlueMask = 0x000000FF;
				bi.bV5AlphaMask = 0xFF000000;

				HDC hdc;
				hdc = GetDC(NULL);

				// Create the DIB section with an alpha channel.
				hBitmap = CreateDIBSection(hdc, (BITMAPINFO *)&bi, DIB_RGB_COLORS,
					(void **)&lpBits, NULL, (DWORD)0);

				// Create an empty mask bitmap.
				HBITMAP hMonoBitmap = CreateBitmap(vSize.x, vSize.y, 1, 1, NULL);

				// Set the alpha values for each pixel in the cursor so that
				// the complete cursor is semi-transparent.
				DWORD *lpdwPixel;
				lpdwPixel = (DWORD *)lpBits;
				gfx::TextureAccessorA32 accessor(texture, true);
				for(unsigned int x = 0; x < (unsigned int)vSize.x; x++)
				{
					for(unsigned int y = 0; y < (unsigned int)vSize.y; y++)
					{
						auto& pixel = accessor.GetPixel(vSize.x-1-x, y);

						// Clear the alpha bits
						*lpdwPixel &= 0x00FFFFFF;
						// Set the alpha bits to 0x9F (semi-transparent)
						*lpdwPixel = RGB(pixel.r, pixel.g, pixel.b);
						*lpdwPixel |= pixel.a << 24;
						lpdwPixel++;
					}
				}

				iconinfo.hbmMask = hMonoBitmap;
				iconinfo.hbmColor = hBitmap;

				m_hCursor = CreateIconIndirect(&iconinfo);

				DeleteObject(hBitmap);
				DeleteObject(hMonoBitmap);

				break;
			}
			default:
				sys::log->Out(sys::LogModule::GUI, sys::LogType::WARNING, "Incompatible texture format for cursor.");
				break;
			}

			ShowCursor(true);

			SetCursor(m_hCursor);
		}

		void CursorImplementation::Activate(void)
		{
			SetCursor(m_hCursor);
		}

		void CursorImplementation::Hide(void)
		{
			SetCursor(nullptr);
		}

		void CursorImplementation::MoveTo(unsigned int x, unsigned int y)
		{
			SetCursorPos(x, y);
		}
	}
}

