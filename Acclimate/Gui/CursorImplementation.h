#pragma once
#include <Windows.h>

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

	namespace gui
	{

		class CursorImplementation
		{
		public:
			CursorImplementation(gfx::ITexture& texture, unsigned int offX, unsigned int offY);

			void Activate(void);
			void Hide(void);

			void MoveTo(unsigned int x, unsigned int y);

		private:

			HCURSOR m_hCursor;
			unsigned int m_offX, m_offY;
		};

	}
}

