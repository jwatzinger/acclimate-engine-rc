#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		template<typename Data, typename WidgetType=Widget, typename... Args>
		class DataContainer
		{
			typedef DataContainer<Data, WidgetType, Args...> SelfContainer;
		public:

			DataContainer(void) : m_data(Data()), m_pWidget(nullptr), m_pSignal(nullptr)
			{
			}

			DataContainer(WidgetType& widget, core::Signal<Args...>& signal, Data data) : m_data(data), m_pWidget(&widget),
				m_pSignal(&signal)
			{
				m_pSignal->Connect(this, &DataContainer::OnAccessData);
			}

			DataContainer(const SelfContainer& container) : m_data(container.m_data), m_pWidget(container.m_pWidget),
				SigAccess(container.SigAccess), m_pSignal(container.m_pSignal)
			{
				m_pSignal->Connect(this, &DataContainer::OnAccessData);
			}

			DataContainer(SelfContainer&& container) : m_data(std::move(container.m_data)), m_pWidget(container.m_pWidget),
				SigAccess(std::move(container.SigAccess)), m_pSignal(container.m_pSignal)
			{
				m_pSignal->Disconnect(&container, &DataContainer::OnAccessData);
				m_pSignal->Connect(this, &DataContainer::OnAccessData);
				container.m_pWidget = nullptr;
				container.m_pSignal = nullptr;
			}

			~DataContainer(void)
			{
				if(m_pWidget)
					m_pSignal->Disconnect(this, &DataContainer::OnAccessData);
			}

			void SetWidget(WidgetType* pWidget)
			{
				if(m_pWidget && pWidget != m_pWidget)
					m_pSignal->Disconnect(this, &DataContainer::OnAccessData);

				if(pWidget)
					m_pSignal->Connect(this, &DataContainer::OnAccessData);

				m_pWidget = pWidget;
			}

			WidgetType* GetWidget(void) const
			{
				return m_pWidget;
			}

			WidgetType* operator->(void) const
			{
				return m_pWidget;
			}

			SelfContainer& operator=(const SelfContainer& container)
			{
				m_data = container.m_data;
				m_pWidget = container.m_pWidget;
				m_pSignal = container.m_pSignal;
				SigAccess = container.SigAccess;

				m_pSignal->Connect(this, &DataContainer::OnAccessData);

				return *this;
			}

			void OnAccessData(Args... args)
			{
				SigAccess(m_data, args...);
			}

			core::Signal<Data, Args...> SigAccess;

		private:

			Data m_data;
			WidgetType* m_pWidget;
			core::Signal<Args...>* m_pSignal;
		};

	}
}

