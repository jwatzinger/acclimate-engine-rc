#include "Dock.h"
#include "Module.h"
// TODO: remove
#include "DockArea.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		Dock::Dock(float x, float y, float width, float height, const std::wstring& stLabel) :
			Window(x, y, width, height, stLabel), m_isDocked(false), m_isDragging(false),
			m_state(DockState::OPEN)
		{
		}

		Dock::~Dock(void)
		{
			SigDestroy();
		}

		void Dock::SetDockState(DockState state)
		{
			if(m_state != state)
			{
				m_state = state;

				switch(state)
				{
				case gui::DockState::OPEN:
					RemoveStyleFlags(NoClose);
					break;
				case gui::DockState::LOCKED:
					RemoveStyleFlags(NoClose);
					break;
				case gui::DockState::FIXED:
					SetStyleFlags(NoClose);
					break;
				case gui::DockState::STATIC:
					SetStyleFlags(NoClose);
					break;
				}
			}
		}

		void Dock::OnDock(void)
		{
			ACL_ASSERT(!m_isDocked);

			OnMaximize();
			SetStyleFlags(NoResize);

			m_isDocked = true;
		}

		void Dock::OnUndock(void)
		{
			ACL_ASSERT(m_isDocked);

			auto& vMousePos = m_pModule->m_cursor.GetPosition();
			auto& vScreenSize = m_pModule->GetScreenSize();
			// modify position and size
			if(m_isDragging)
			{
				SetX((vMousePos.x - m_vDragDist.x) / (float)vScreenSize.x);
				SetY((vMousePos.y - m_vDragDist.y) / (float)vScreenSize.y);
			}
			else
			{
				SetX(GetX() / (float)vScreenSize.x);
				SetY(GetY() / (float)vScreenSize.y);
			}

			SetWidth(GetWidth() / (float)vScreenSize.x);
			SetHeight(GetHeight() / (float)vScreenSize.y);

			RemoveStyleFlags(NoResize);
			OnShrink();
			SigUndock(*this);
			SigUndock.Clear();
			Free();
			
			m_isDocked = false;
		}

		void Dock::OnDrag(const math::Vector2& vDistance)
		{
			if(m_isDocked)
			{
				if(m_state != DockState::OPEN)
					return;

				auto& vCursorPos = m_pModule->m_cursor.GetPosition();
				const int x = GetX(), y = GetY();

				if(!m_isDragging)
				{
					m_vDragDist = vCursorPos - math::Vector2(x, y);
					m_vDragDist.x = max(9, min(m_vDragDist.x, GetWidth() - 9));
					m_vDragDist.y = max(9, min(m_vDragDist.y, 24 - 9));
					m_isDragging = true;
				}

				if(vCursorPos.x < x || vCursorPos.x >(x + GetWidth()) || vCursorPos.y < y || vCursorPos.y >(y + 24))
					OnUndock();

				return;
			}

			Window::OnDrag(vDistance);
		}

		void Dock::OnParentResize(int x, int y, int width, int height)
		{
			if(m_isDocked)
			{
				int minW = m_minW, minH = m_minH, maxW = m_maxW, maxH = m_maxH;

				SetCapMetrics(0, 0, 0, 0);
				Window::OnParentResize(x, y, width, height);
				SetCapMetrics(minW, maxW, minH, maxH);
			}
			else
				Window::OnParentResize(x, y, width, height);
		}

		void Dock::OnRelease(MouseType type, bool bMouseOver)
		{
			Window::OnRelease(type, bMouseOver);
			m_isDragging = false;
		}

	}
}

