#pragma once
#include "Window.h"

namespace acl
{
	namespace gui
	{

		class Dock;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<Dock&>;

		enum class DockState;

		class ACCLIMATE_API Dock :
			public Window
		{
		public:
			Dock(float x, float y, float width, float height, const std::wstring& stLabel);
			~Dock(void);

			void SetDockState(DockState state);

			void OnDock(void);
			void OnUndock(void);
			void OnDrag(const math::Vector2& vDistance) override;
			void OnRelease(MouseType type, bool bMouseOve) override;
			void OnParentResize(int x, int y, int width, int height) override;

			core::Signal<Dock&> SigUndock;
			core::Signal<> SigDestroy;

		private:

			bool m_isDocked, m_isDragging;
			DockState m_state;
			math::Vector2 m_vDragOffset;
		};

	}
}


