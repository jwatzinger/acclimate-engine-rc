#include "DockArea.h"
#include "Dock.h"
#include "Area.h"
#include "TabBarItem.h"
#include "DockSeperator.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		DockArea::DockWrapper::DockWrapper(Dock& dock) :
			Widget(0.0f, 0.0f, 1.0f, 1.0f), m_pDock(&dock), m_isClosed(!dock.IsVisible())
		{
			m_pDock->OnDock();
			m_pDock->SigVisible.Connect(this, &DockWrapper::OnDockVisible);
			m_pDock->SigInvisible.Connect(this, &DockWrapper::OnDockInvisible);
			m_pDock->SigUndock.Connect(this, &DockWrapper::OnUndock);
			m_pDock->SigDestroy.Connect(this, &DockWrapper::OnDestroy);
		}

		DockArea::DockWrapper::~DockWrapper(void)
		{
			if(m_pDock)
			{
				m_pDock->SigVisible.Disconnect(this, &DockWrapper::OnDockVisible);
				m_pDock->SigInvisible.Disconnect(this, &DockWrapper::OnDockInvisible);
				m_pDock->SigUndock.Disconnect(this, &DockWrapper::OnUndock);
				m_pDock->SigDestroy.Disconnect(this, &DockWrapper::OnDestroy);
				m_pDock->OnUndock();
			}
		}

		void DockArea::DockWrapper::OnDockVisible(void)
		{
			m_isClosed = false;
			OnVisible();
			SigDockVisible();
		}

		void DockArea::DockWrapper::OnDockInvisible(void)
		{
			m_isClosed = true;
			OnInvisible();
			SigDockInvisible();
		}

		void DockArea::DockWrapper::OnUndock(Dock& dock)
		{
			SigUndock(*this);
			if(m_pDock)
			{
				m_pDock->SigVisible.Disconnect(this, &DockWrapper::OnDockVisible);
				m_pDock->SigInvisible.Disconnect(this, &DockWrapper::OnDockInvisible);
				m_pDock = nullptr;
			}
			delete this;
		}

		void DockArea::DockWrapper::OnDestroy(void)
		{
			SigUndock(*this);
			m_pDock = nullptr;
			delete this;
		}

		void DockArea::DockWrapper::SetDockState(DockState state)
		{
			m_pDock->SetDockState(state);
		}

		bool DockArea::DockWrapper::IsClosed(void) const
		{
			return m_isClosed;
		}

		const std::wstring& DockArea::DockWrapper::GetName(void) const
		{
			return m_pDock->GetName();
		}

		bool DockArea::DockWrapper::operator==(const Dock& dock) const
		{
			return m_pDock == &dock;
		}

		DockArea::DockArea(float x, float y, float width, float height) :
			Widget(x, y, width, height), m_vDockAreas((size_t)DockType::SIZE),
			m_area(0.0f, 0.0f, 1.0f, 1.0f), m_vSeperators((size_t)DockType::SIZE),
			m_numActiveDocks(0), m_numActiveAreas(0), m_tabBar(0.0f, 1.0f, 1.0f, 21.0f),
			m_isChild(false), m_state(DockState::OPEN)
		{
			m_tabBar.SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::REL, PositionMode::ABS);
			m_tabBar.SetCenter(HorizontalAlign::LEFT, VerticalAlign::BOTTOM);
			m_tabBar.SetVisible(false);
			AddChild(m_tabBar);

			AddChild(m_area);
		}

		DockArea::~DockArea(void)
		{
			for(auto pSeperator : m_vSeperators)
			{
				delete pSeperator;
			}

			for(auto pDockArea : m_vDockAreas)
			{
				delete pDockArea;
			}

			for(auto pDock : m_vDocks)
			{
				delete pDock;
			}
		}

		void DockArea::SetDockState(DockState state)
		{
			if(state != m_state)
			{
				m_state = state;
				for(auto pDock : m_vDocks)
				{
					pDock->SetDockState(state);
				}

				for(auto pArea : m_vDockAreas)
				{
					if(pArea)
						pArea->SetDockState(state);
				}

				for(auto pSeperator : m_vSeperators)
				{
					if(pSeperator)
						pSeperator->SetDockState(state);
				}
			}
		}

		void DockArea::AddDock(Dock& dock, DockType type, bool bStack)
		{
			if(type == DockType::INSERT)
			{
				if(m_width > m_height)
					InsertDock(dock, DockType::LEFT, DockType::RIGHT);
				else
					InsertDock(dock, DockType::TOP, DockType::BOTTOM);
			}
			else if(type == DockType::MIDDLE)
			{
				for(auto pDock : m_vDocks)
				{
					if(*pDock == dock)
						return;
				}

				auto pWrapper = new DockWrapper(dock);
				m_area.AddChild(*pWrapper);
				pWrapper->AddChild(dock);
				m_vDocks.push_back(pWrapper);

				OnChangeDockCount();

				pWrapper->SetDockState(m_state);
				pWrapper->SigDockVisible.Connect(this, &DockArea::OnDockVisible);
				pWrapper->SigDockInvisible.Connect(this, &DockArea::OnDockInvisible);
				pWrapper->SigUndock.Connect(this, &DockArea::OnUndock);

				auto& item = m_tabBar.AddItem(dock.GetName());
				item.OnSelect();
				item.SigSelection.Connect(this, &DockArea::OnSelectTab);

				m_numActiveDocks++;
			}
			else
			{
				auto pDockArea = m_vDockAreas[(size_t)type];
				if(!pDockArea)
				{
					// TODO: correct size/position calculation
					float x = 0.0f, y = 0.0f, width = 1.0f, height = 1.0f;

					switch(type)
					{
					case DockType::LEFT:
						{
							width = dock.GetWidth() / (float)m_area.GetWidth();

							const float offset = width;
							m_area.SetX(m_area.GetRelX() + offset);
							m_area.SetWidth(m_area.GetRelWidth() - offset);
							break;
						}
					case DockType::RIGHT:
						{
							width = dock.GetWidth() / (float)m_area.GetWidth();
							x = 1.0f - width;

							const float offset = (m_area.GetRelX() + m_area.GetRelWidth()) - x;
							m_area.SetWidth(m_area.GetRelWidth() - offset);
							break;
						}
					case DockType::TOP:
						{
							height = dock.GetHeight() / (float)m_area.GetHeight();
							height = min(height, 0.75f);

							const float offset = height;
							m_area.SetY(m_area.GetRelY() + offset);
							m_area.SetHeight(m_area.GetRelHeight() - offset);
							break;
						}
					case DockType::BOTTOM:
						{
							height = dock.GetHeight() / (float)m_area.GetHeight();
							height = min(height, 0.75f);
							y = 1.0f - height;

							const float offset = (m_area.GetRelY() + m_area.GetRelHeight()) - y;
							m_area.SetHeight(m_area.GetRelHeight() - offset);
							break;
						}
					default:
						ACL_ASSERT(false);
					}

					pDockArea = new DockArea(x, y, width, height);
					pDockArea->SetDockState(m_state);
					pDockArea->SetChildStatus(true);
					pDockArea->SigVisible.Connect(this, &DockArea::OnAreaVisible);
					pDockArea->SigInvisible.Connect(this, &DockArea::OnAreaInvisible);
					pDockArea->SigRemoved.Connect(this, &DockArea::OnRemoveArea);
					AddChild(*pDockArea);
					SetDock(pDockArea, type);

					pDockArea->AddDock(dock, DockType::MIDDLE, true);

					m_numActiveAreas++;
				}
				else
				{
					if(bStack)
						pDockArea->AddDock(dock, DockType::MIDDLE, true);
					else
						pDockArea->AddDock(dock, DockType::INSERT, false);
				}
			}
		}

		void DockArea::SetChildStatus(bool bChildStatus)
		{
			m_isChild = bChildStatus;
		}

		void DockArea::SetDock(DockArea* pArea, DockType type)
		{
			m_vDockAreas[(size_t)type] = pArea;

			auto pSeperator = new DockSeperator(type, m_area, *pArea);
			pSeperator->SetDockState(m_state);
			m_vSeperators[(size_t)type] = pSeperator;
			AddChild(*pSeperator);
		}

		DockArea* DockArea::GetDock(DockType type) const
		{
			return m_vDockAreas[(size_t)type];
		}

		void DockArea::InsertDock(Dock& dock, DockType first, DockType second)
		{
			if(auto pFirst = GetDock(first))
			{
				auto pSecond = GetDock(second);
				if(!pSecond)
					AddDock(dock, second, false);
				else
					pSecond->AddDock(dock, DockType::MIDDLE, false);
			}
			else
				AddDock(dock, first, false);
		}

		void DockArea::RefreshTabs(void)
		{
			m_tabBar.Clear();
			TabBarItem* pLastItem = nullptr;

			for(auto pDock : m_vDocks)
			{
				if(!pDock->IsClosed())
				{
					auto& item = m_tabBar.AddItem(pDock->GetName());
					item.SigSelection.Connect(this, &DockArea::OnSelectTab);
					pLastItem = &item;
				}
			}

			if(pLastItem)
				pLastItem->OnSelect();
		}

		void DockArea::OnDockVisible(void)
		{
			ACL_ASSERT(m_numActiveDocks < m_vDocks.size());

			if(!m_numActiveDocks)
			{
				m_area.OnVisible();
				if(!m_numActiveAreas)
					OnVisible();
			}

			m_numActiveDocks++;

			RefreshTabs();
		}

		void DockArea::OnDockInvisible(void)
		{
			ACL_ASSERT(m_numActiveDocks > 0);

			m_numActiveDocks--;

			if(!m_numActiveDocks)
			{
				m_area.OnInvisible();
				if(!m_numActiveAreas)
					OnInvisible();
			}

			RefreshTabs();
		}

		void DockArea::OnAreaVisible(void)
		{
			if(!m_numActiveAreas && !m_numActiveDocks)
				OnVisible();
				
			m_numActiveAreas++;
		}
	
		void DockArea::OnAreaInvisible(void)
		{
			ACL_ASSERT(m_numActiveAreas > 0);

			m_numActiveAreas--;

			if(!m_numActiveAreas && !m_numActiveDocks)
				OnInvisible();
		}

		void DockArea::OnUndock(DockWrapper& dock)
		{
			auto itr = std::find(m_vDocks.begin(), m_vDocks.end(), &dock);
			ACL_ASSERT(itr != m_vDocks.end());
			const size_t offset = itr - m_vDocks.begin();
			m_vDocks.erase(itr);

			dock.SigDockVisible.Disconnect(this, &DockArea::OnDockVisible);
			dock.SigDockInvisible.Disconnect(this, &DockArea::OnDockInvisible);
			dock.SigClose.Disconnect(this, &DockArea::RefreshTabs);

			m_numActiveDocks--;

			m_area.RemoveChild(dock);
			OnChangeDockCount();

			m_tabBar.RemoveItem(dock.GetName());
			
			if(m_vDocks.empty())
			{
				if(!m_numActiveAreas && !m_isChild)
				{
					SigRemoved(*this);
					delete this;
					return;
				}
				else
				{
					for(auto pSeperator : m_vSeperators)
					{
						if(pSeperator)
							pSeperator->OnAreaInvisible();
					}
				}
			}
			
			RefreshTabs();
		}

		void DockArea::OnRemoveArea(DockArea& area)
		{
			auto itr = std::find(m_vDockAreas.begin(), m_vDockAreas.end(), &area);
			ACL_ASSERT(itr != m_vDockAreas.end());
			const size_t offset = itr - m_vDockAreas.begin();
			m_vDockAreas[offset] = nullptr;

			area.OnInvisible();
			delete m_vSeperators[offset];
			m_vSeperators[offset] = nullptr;

			if(m_isChild && !m_numActiveAreas && m_vDocks.empty())
			{
				SigRemoved(*this);
				delete this;
			}
		}

		void DockArea::OnChangeDockCount(void)
		{
			const size_t size = m_vDocks.size();
			if(size > 1)
			{
				m_tabBar.SetVisible(true);
				m_area.SetPadding(PaddingSide::BOTTOM, m_tabBar.GetHeight());
			}
			else
			{
				if(size != 0)
					(*m_vDocks.begin())->SetVisible(true);

				m_tabBar.SetVisible(false);
				m_area.SetPadding(PaddingSide::BOTTOM, 0);
			}
		}

		void DockArea::OnSelectTab(TabBarItem* pItem)
		{
			auto& stName = pItem->GetName();

			// increase dock counter to avoid closing this dock area
			for(auto pDock : m_vDocks)
			{
				pDock->SetVisible(stName == pDock->GetName());
			}
		}

	}
}

