#pragma once
#include "Widget.h"
#include <vector>
#include "Area.h"
#include "TabBar.h"

namespace acl
{
	namespace gui
	{

		enum class DockType
		{
			INSERT = -2, MIDDLE=-1, LEFT, RIGHT, TOP, BOTTOM, SIZE
		};

		class Dock;
		class DockSeperator;
		class TabBarItem;

		enum class DockState
		{
			OPEN, ///< Docks can be closed, resized and removed
			LOCKED, ///< Docks can only be closed and resized
			FIXED, ///< Docks can only be resized
			STATIC ///< Docks cannot be altered
		};

		class ACCLIMATE_API DockArea :
			public Widget
		{

			class DockWrapper :
				public Widget
			{
			public:
				DockWrapper(Dock& dock);
				~DockWrapper(void);

				void SetDockState(DockState state);

				bool IsClosed(void) const;
				const std::wstring& GetName(void) const;

				bool operator==(const Dock& dock) const;

				core::Signal<> SigDockVisible;
				core::Signal<> SigDockInvisible;
				core::Signal<DockWrapper&> SigUndock;

			private:

				void OnDockVisible(void);
				void OnDockInvisible(void);
				void OnUndock(Dock& dock);
				void OnDestroy(void);

				Dock* m_pDock;

				bool m_isClosed;
			};

			typedef std::vector<DockArea*> DockAreaVector;
			typedef std::vector<DockWrapper*> DockVector;
			typedef std::vector<DockSeperator*> DockSeperatorVector;
		public:
			DockArea(float x, float y, float width, float height);
			~DockArea(void);

			void SetDockState(DockState state);

			void AddDock(Dock& dock, DockType type, bool bStack);

		private:
#pragma warning( disable: 4251 )
			void SetChildStatus(bool isChild);
			void SetDock(DockArea* pArea, DockType type);
			DockArea* GetDock(DockType type) const;
			void InsertDock(Dock& dock, DockType first, DockType second);
			void RefreshTabs(void);

			void OnDockVisible(void);
			void OnDockInvisible(void);
			void OnAreaVisible(void);
			void OnAreaInvisible(void);
			void OnUndock(DockWrapper& dock);
			void OnRemoveArea(DockArea& area);
			void OnChangeDockCount(void);
			void OnSelectTab(TabBarItem* pItem);

			Area m_area;
			TabBar m_tabBar;
			DockVector m_vDocks;
			DockAreaVector m_vDockAreas;
			DockSeperatorVector m_vSeperators;

			bool m_isChild;
			size_t m_numActiveDocks, m_numActiveAreas;
			DockState m_state;

			core::Signal<DockArea&> SigRemoved;
#pragma warning( default: 4251 )
		};

	}
}

