#include "DockSeperator.h"
#include "DockArea.h"
#include "Module.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		Orientation dockTypeToOrientation(DockType type)
		{
			switch(type)
			{
			case DockType::LEFT:
				return Orientation::VERTICAL;
			case DockType::RIGHT:
				return Orientation::VERTICAL;
			case DockType::TOP:
				return Orientation::HORIZONTAL;
			case DockType::BOTTOM:
				return Orientation::HORIZONTAL;
			default:
				ACL_ASSERT(false);
			}

			return Orientation::HORIZONTAL;
		}

		const unsigned int SIZE = 3;

		DockSeperator::DockSeperator(DockType type, Area& area, DockArea& dock) :
			Widget(0.0f, 0.0f, 1.0f, 1.0f), m_pArea(&area), m_pDock(&dock), m_orientation(dockTypeToOrientation(type)),
			m_type(type), m_relation(0.0f), m_state(DockState::OPEN)
		{
			SetCapMetrics(0, 2560, 0, 1600);

			dock.SigVisible.Connect(this, &DockSeperator::OnDockVisible);
			dock.SigInvisible.Connect(this, &DockSeperator::OnDockInvisible);

			area.SigVisible.Connect(this, &DockSeperator::OnAreaVisible);
			area.SigInvisible.Connect(this, &DockSeperator::OnAreaInvisible);

			if(m_orientation == Orientation::HORIZONTAL)
			{
				SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::REL, PositionMode::ABS);
				SetHeight((float)SIZE);

				if(type == DockType::TOP)
				{
					area.SetPadding(0, SIZE, 0, SIZE);
					m_relation = dock.GetRelY() + dock.GetRelHeight();
				}
				else
				{
					dock.SetPadding(0, SIZE, 0, SIZE);
					m_relation = area.GetRelY() + area.GetRelHeight();
				}
					
				SetY(m_relation);	
			}
			else
			{
				SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::ABS, PositionMode::REL);
				SetWidth((float)SIZE);

				if(type == DockType::LEFT)
				{
					area.SetPadding(SIZE, 0, SIZE, 0);
					m_relation = dock.GetRelX() + dock.GetRelWidth();
				}
				else
				{
					dock.SetPadding(SIZE, 0, SIZE, 0);
					m_relation = area.GetRelX() + area.GetRelWidth();
				}

				SetX(m_relation);
			}
		}

		DockSeperator::~DockSeperator(void)
		{
			m_pDock->SigVisible.Disconnect(this, &DockSeperator::OnDockVisible);
			m_pDock->SigInvisible.Disconnect(this, &DockSeperator::OnDockInvisible);

			m_pArea->SigVisible.Disconnect(this, &DockSeperator::OnAreaVisible);
			m_pArea->SigInvisible.Disconnect(this, &DockSeperator::OnAreaInvisible);
		}

		void DockSeperator::SetDockState(DockState state)
		{
			m_state = state;
		}

		void DockSeperator::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData grData;

			const int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"DockSeperator");

			const auto& back = element.GetPart(L"Back");

			if(m_orientation == Orientation::VERTICAL)
			{
				const auto& left = element.GetPart(L"Left");
				const auto& right = element.GetPart(L"Right");

				//draw left border
				grData.rDestRect.Set(0, 0, left.width, height);
				grData.rSrcRect = left;
				m_vRenderData.push_back(grData);

				//draw back
				grData.rDestRect.Set(left.width, 0, width - left.width - right.width, height);
				grData.rSrcRect = back;
				m_vRenderData.push_back(grData);

				//draw right border
				grData.rDestRect.Set(width - right.width, 0, right.width, height);
				grData.rSrcRect = right;
				m_vRenderData.push_back(grData);
			}
			else
			{
				const auto& top = element.GetPart(L"Top");
				const auto& bottom = element.GetPart(L"Bottom");

				//draw top border
				grData.rDestRect.Set(0, 0, width, top.height);
				grData.rSrcRect = top;
				m_vRenderData.push_back(grData);

				//draw back
				grData.rDestRect.Set(0, top.height, width, height - top.height - bottom.height);
				grData.rSrcRect = back;
				m_vRenderData.push_back(grData);

				//draw bottom border
				grData.rDestRect.Set(0, height - bottom.height, width, bottom.height);
				grData.rSrcRect = bottom;
				m_vRenderData.push_back(grData);
			}
		}

		void DockSeperator::OnDrag(const math::Vector2& vDistance)
		{
			if(m_state == DockState::STATIC)
				return;

			Widget::OnDrag(vDistance);

			if(m_orientation == Orientation::HORIZONTAL)
			{
				const int parentHeight = m_pParent->GetHeight();
				const int projectedPos = max(16, min(parentHeight-16, m_pModule->m_cursor.GetPosition().y - m_pParent->GetY()));
				m_relation = projectedPos / (float)parentHeight;
			}
			else
			{
				const int parentWidth = m_pParent->GetWidth();
				const int projectedPos = max(16, min(parentWidth - 16, m_pModule->m_cursor.GetPosition().x - m_pParent->GetX()));
				m_relation = projectedPos / (float)parentWidth;
			}

			UpdateRelation(m_relation);
		}

		void DockSeperator::OnDockVisible(void)
		{
			OnVisible();

			if(m_orientation == Orientation::HORIZONTAL)
				m_pArea->SetPadding(0, SIZE, 0, SIZE);
			else
				m_pArea->SetPadding(SIZE, 0, SIZE, 0);

			UpdateRelation(m_relation);
		}

		void DockSeperator::OnDockInvisible(void)
		{
			OnInvisible();

			m_pArea->SetPadding(0, 0, 0, 0);

			if(m_type == DockType::LEFT || m_type == DockType::TOP)
				UpdateRelation(0.0f);
			else if(m_type == DockType::RIGHT || m_type == DockType::BOTTOM)
				UpdateRelation(1.0f);
		}

		void DockSeperator::OnAreaVisible(void)
		{
			OnVisible();

			UpdateRelation(m_relation);
		}

		void DockSeperator::OnAreaInvisible(void)
		{
			OnInvisible();

			if(m_type == DockType::LEFT || m_type == DockType::TOP)
				UpdateRelation(1.0f);
			else if(m_type == DockType::RIGHT || m_type == DockType::BOTTOM)
				UpdateRelation(0.0f);
		}

		void DockSeperator::UpdateRelation(float relation)
		{
			if(m_orientation == Orientation::HORIZONTAL)
			{
				m_y = relation;

				if(m_type == DockType::TOP)
				{
					m_pDock->SetHeight(relation);
					m_pArea->SetY(relation);
					m_pArea->SetHeight(1.0f - relation);
				}
				else
				{
					m_pArea->SetHeight(relation);
					m_pDock->SetY(relation);
					m_pDock->SetHeight(1.0f - relation);
				}
			}
			else
			{
				m_x = relation;

				if(m_type == DockType::LEFT)
				{
					m_pDock->SetWidth(relation);
					m_pArea->SetX(relation);
					m_pArea->SetWidth(1.0f - relation);
				}
				else
				{
					m_pArea->SetWidth(relation);
					m_pDock->SetX(relation);
					m_pDock->SetWidth(1.0f - relation);
				}
			}
		}

	}
}