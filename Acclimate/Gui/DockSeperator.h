#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		enum class Orientation
		{
			HORIZONTAL, VERTICAL
		};

		class Area;
		class DockArea;

		enum class DockType;
		enum class DockState;

		class DockSeperator :
			public Widget
		{
		public:
			DockSeperator(DockType type, Area& area, DockArea& dock);
			~DockSeperator(void);

			void SetDockState(DockState state);

			void OnRedraw(void) override;
			void OnDrag(const math::Vector2& vDistance) override;

			void OnAreaVisible(void);
			void OnAreaInvisible(void);

		private:

			// TODO: possibly private again
			void OnDockVisible(void);
			void OnDockInvisible(void);
			
			void UpdateRelation(float relation);

			DockType m_type;
			Orientation m_orientation;
			Area* m_pArea;
			DockArea* m_pDock;

			float m_relation;
			DockState m_state;
		};

	}
}


