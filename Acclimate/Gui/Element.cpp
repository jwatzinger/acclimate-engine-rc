#include "Element.h"

namespace acl
{
	namespace gui
	{

		void Element::SetPart(const std::wstring& stName, const math::Rect& rect)
		{
			m_mParts[stName] = rect;
		}

		void Element::SetColor(const std::wstring& stName, const gfx::Color& color)
		{
			m_mColors[stName] = color;
		}

		const math::Rect& Element::GetPart(const std::wstring& stName) const
		{			
			auto itr = m_mParts.find(stName);
			if(itr != m_mParts.end())
				return itr->second;
			else
				return m_defaultRect;
		}

		const gfx::Color& Element::GetColor(const std::wstring& stName) const
		{			
			auto itr = m_mColors.find(stName);
			if(itr != m_mColors.end())
				return itr->second;
			else
				return m_defaultColor;
		}
	
	}
}
