#pragma once
#include <string>
#include <unordered_map>
#include "..\Core\Dll.h"
#include "..\Gfx\Color.h"
#include "..\Math\Rect.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API Element
		{
			typedef std::unordered_map<std::wstring, math::Rect> PartMap;
			typedef std::unordered_map<std::wstring, gfx::Color> ColorMap;
		public:

			void SetPart(const std::wstring& stName, const math::Rect& rect);
			void SetColor(const std::wstring& stName, const gfx::Color& color);

			const math::Rect& GetPart(const std::wstring& stName) const;
			const gfx::Color& GetColor(const std::wstring& stName) const;

		private:

#pragma warning( disable: 4251 )

			math::Rect m_defaultRect;
			gfx::Color m_defaultColor;

			PartMap m_mParts;
			ColorMap m_mColors;

#pragma warning( default: 4251 )
		};

	}
}