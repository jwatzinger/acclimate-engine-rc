#include "FileDialog.h"
#include "..\System\WorkingDirectory.h"

namespace acl
{
    namespace gui
    {

        FileDialog::FileDialog(DialogType type, const std::wstring& stTitle, LPCWSTR lpFilter): Widget(0, 0, 0, 0), m_pFileBuffer(nullptr), 
			m_pFilterBuffer(nullptr), m_stTitle(stTitle)
        {
            m_pFileBuffer = new wchar_t[1024];
            memset(m_pFileBuffer, 0, 1024);

            m_pNameBuffer = new wchar_t[1024];
            memset(m_pNameBuffer, 0, 1024);

			// filter
			const size_t filterSize = wcslen(lpFilter);

			if(filterSize)
			{
				//m_pFilterBuffer = new wchar_t[filterSize + 1];
				//memcpy(m_pFilterBuffer, lpFilter, sizeof(wchar_t)*filterSize);
				//m_pFilterBuffer[filterSize] = '\0';
				m_pFilterBuffer = new wchar_t[1024];
				memcpy(m_pFilterBuffer, lpFilter, 1024);
			}
            
            memset(&m_ofn, 0, sizeof(m_ofn));
            m_ofn.lStructSize = sizeof(m_ofn);
            m_ofn.lpstrFile = m_pFileBuffer;
            m_ofn.lpstrFileTitle = m_pNameBuffer;
            m_ofn.nMaxFileTitle = 1024;
            m_ofn.nMaxFile = 1024;
            m_ofn.lpstrFilter = m_pFilterBuffer;
			m_ofn.nFilterIndex = filterSize;
            m_ofn.lpstrTitle = m_stTitle.c_str();

			if (type == DialogType::OPEN)
				m_ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
			else
				m_ofn.Flags = OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
        }

        FileDialog::~FileDialog(void)
        {
            delete m_pFileBuffer;
            delete m_pNameBuffer;
			delete m_pFilterBuffer;
        }

        std::wstring FileDialog::GetFullPath(void) const
        {
            return m_pFileBuffer;
        }

        std::wstring FileDialog::GetFileName(void) const
        {
            return m_pNameBuffer;
        }

        bool FileDialog::Execute(void)
        {
			sys::WorkingDirectory dir;

			return GetOpenFileName(&m_ofn) == TRUE;
        }

        void FileDialog::OnRedraw(void)
        {
        }

    }
}