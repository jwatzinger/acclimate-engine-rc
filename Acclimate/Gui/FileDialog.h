#pragma once
#include "Widget.h"

namespace acl
{
    namespace gui
    {

		enum class DialogType
		{
			OPEN, SAVE
		};

        class ACCLIMATE_API FileDialog :
            public Widget
        {
        public:
            FileDialog(DialogType type, const std::wstring& stTitle = L"", LPCWSTR lpFilter=L"");
            ~FileDialog(void);

            std::wstring GetFullPath(void) const;
            std::wstring GetFileName(void) const;

            bool Execute(void);

            void OnRedraw(void) override;

        private:
#pragma warning( disable: 4251 )
            std::wstring m_stTitle;

            wchar_t* m_pFileBuffer;
            wchar_t* m_pNameBuffer;
			wchar_t* m_pFilterBuffer;

            OPENFILENAME m_ofn;
#pragma warning( default: 4251 )
        };

    }
}

