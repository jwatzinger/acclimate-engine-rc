#include "Group.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		Group::Group(float x, float y, float width, float height, const std::wstring& stCaption): Widget(x, y, width, height),
			m_area(0.0f, 0.0f, 1.0f, 1.0f), m_stCaption(stCaption)
		{
			m_area.SetPadding(8, 8, 16, 16);
			Widget::AddChild(m_area);
		}

		void Group::SetClipping(bool bClip)
		{
			m_area.SetClipping(bClip);
		}

		void Group::AddChild(Widget& child)
		{
			m_area.AddChild(child);
		}

		void Group::RemoveChild(Widget& child)
		{
			m_area.RemoveChild(child);
		}

		void Group::OnRedraw(void)
		{
			RenderData data;
			m_vRenderData.clear();

			const int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Group");

			// top left
			const math::Rect rTopLeft = element.GetPart(L"TopLeft");
			data.rSrcRect = rTopLeft;
			data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(data);

			const math::Rect rTopRight = element.GetPart(L"TopRight");
			data.rSrcRect = rTopRight;
			data.rDestRect.Set(width-rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(data);

			// top middle
			const math::Rect rTop = element.GetPart(L"Top");
			data.rSrcRect = rTop;

			if(m_stCaption.empty())
			{
				data.rDestRect.Set(rTopLeft.width, 0, width-rTopLeft.width-rTopRight.width, rTop.height); 
				m_vRenderData.push_back(data);
			}
			else
			{
				data.rDestRect.Set(rTopLeft.width, 0, rTop.width, rTop.height); 
				m_vRenderData.push_back(data);

				// top middle
				data.stLabel = m_stCaption;
				data.rDestRect.Set(rTopLeft.width+rTop.width, 0, width-rTopLeft.width-rTopRight.width, rTop.height); 
				data.labelFlags = DT_LEFT;
				m_vRenderData.push_back(data);
				data.stLabel.clear();

				const size_t offset = rTopLeft.width+rTop.width+m_stCaption.size()*6;
				// top middle
				data.rDestRect.Set(offset, 0, width-offset-rTopRight.width, rTop.height); 
				m_vRenderData.push_back(data);
			}

			// bottom right
			const math::Rect rBottomRight = element.GetPart(L"BottomRight");
			data.rSrcRect = rBottomRight;
			data.rDestRect.Set(width-rBottomRight.width, height-rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(data);

			// right
			const math::Rect rRight = element.GetPart(L"Right");
			data.rSrcRect = rRight;
			data.rDestRect.Set(width-rRight.width, rTopRight.height, rRight.width, height-rTopRight.height-rBottomRight.height);
			m_vRenderData.push_back(data);

			// bottom left
			const math::Rect rBottomLeft = element.GetPart(L"BottomLeft");
			data.rSrcRect = rBottomLeft;
			data.rDestRect.Set(0, height-rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(data);

			// left
			const math::Rect rLeft = element.GetPart(L"Left");
			data.rSrcRect = rLeft;
			data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height-rTopLeft.height-rBottomLeft.height);
			m_vRenderData.push_back(data);

			// bottom
			const math::Rect rBottom = element.GetPart(L"Bottom");
			data.rSrcRect = rBottom;
			data.rDestRect.Set(rBottomLeft.width, height-rBottom.height, width-rBottomLeft.width-rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(data);

		}

	}
}
