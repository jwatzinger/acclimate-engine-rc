#pragma once
#include "Widget.h"
#include "Area.h"

namespace acl
{
	namespace gui
	{

		class Group :
			public Widget
		{
		public:
			Group(float x, float y, float width, float height, const std::wstring& stCaption = L"");

			void SetClipping(bool bClip) override;

			void AddChild(Widget& widget) override;
			void RemoveChild(Widget& widget) override;

			void OnRedraw(void);

		private:

			Area m_area;

			std::wstring m_stCaption;
		};

	}
}

