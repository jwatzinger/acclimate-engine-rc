#pragma once

namespace acl
{
	namespace math
	{
		struct Vector2;
	}

	namespace gui
	{

		class Widget;

		class IDragDropHandler
		{
		public:

			virtual ~IDragDropHandler(void) = 0 {}

			virtual void OnBegin(gui::Widget& dragWidget) = 0;
			virtual void OnDrag(const math::Vector2& vMousePos) = 0;
			virtual void OnDrop(gui::Widget& targetWidget) = 0;

		};

	}
}