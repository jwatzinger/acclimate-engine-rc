#pragma once
#include <string>

namespace acl
{
    namespace gui
    {

		class Widget;

        class ILoader
        {
        public:
            virtual ~ILoader(void) = 0 {};

			virtual void Load(Widget& widget, const std::wstring& stFilename) const = 0;
            virtual void Load(const std::wstring& stFilename) const = 0;
        };

    }
}

