#pragma once
#include <vector>
#include "..\Core\Signal.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ITreeNodeCallback;

		struct Node
		{
			Node(void) : stName(L""), pCallback(nullptr) {}
			Node(const std::wstring& stName) : stName(stName), pCallback(nullptr) {}

			std::wstring stName;
			std::vector<Node> vChilds;
			ITreeNodeCallback* pCallback;
		};

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;

		class ITreeModel
		{
		public:

			virtual ~ITreeModel(void) = 0 {}

			virtual const Node& GetRoot(void) const = 0;

			virtual core::Signal<>& GetUpdateSignal(void) = 0;
		};

	}
}