#pragma once

namespace acl
{
	namespace gui
	{

		class ContextMenu;
			
		class ITreeNodeCallback
		{
		public:

			virtual ~ITreeNodeCallback() = 0 {};

			virtual const std::wstring* GetIconName(void) const = 0;
			virtual bool IsDeletable(void) const = 0;
			virtual bool IsRenameable(void) const = 0;
			virtual bool IsDefaultExpanded(void) const = 0;

			virtual void OnSelect(void) = 0;
			virtual void OnDelete(void) = 0;
			virtual ContextMenu* OnContextMenu(void) = 0;
			virtual void OnContextMenuClose(ContextMenu& menu) = 0;
			virtual void OnRename(const std::wstring& stName) = 0;
		};

	}
}


