#include "Icon.h"

namespace acl
{
    namespace gui
    {

		Icon::Icon(float x, float y, float size, const std::wstring& lpFileName, const math::Rect& rSrcRect) : Widget(x, y, size, size), m_stFileName(lpFileName),
			m_rSrcRect(rSrcRect) 
        {
			m_borderRelation = BorderRelation::WIDTH_FROM_HEIGHT;
        }

		void Icon::Set(const std::wstring& lpFileName, const math::Rect& rSrcRect)
        {
	        m_stFileName = lpFileName;
	        m_rSrcRect = rSrcRect;
			m_bDirty = true;
        }

        void Icon::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData grData;

	        grData.stFileName = m_stFileName;
	        grData.rDestRect.Set(0, 0, GetWidth(), GetHeight());
			if(m_rSrcRect.width != 0 && m_rSrcRect.height != 0)
				grData.rSrcRect = m_rSrcRect;
			else
				grData.bCrop = false;
	        m_vRenderData.push_back(grData);
        }

    }
}