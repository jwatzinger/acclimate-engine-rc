#pragma once
#include "Widget.h"

namespace acl
{
    namespace gui
    {

        class ACCLIMATE_API Icon :
	        public Widget
        {
        public:
	        Icon(float x, float y, float size, const std::wstring& stFilename, const math::Rect& rSrcRect);

	        void Set(const std::wstring& stFileName, const math::Rect& rSrcRect);

	        void OnRedraw(void) override;

        private:

#pragma warning( disable: 4251 )
	        std::wstring m_stFileName;
	        math::Rect m_rSrcRect;
#pragma warning( default: 4251 )

        };

    }
}

