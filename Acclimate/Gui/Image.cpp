#include "Image.h"

namespace acl
{
    namespace gui
    {

		Image::Image(float x, float y, float width, float height, const std::wstring& stFileName) : BaseImage(x, y, width, height), m_stFileName(stFileName)
        {
        }

		void Image::SetFileName(const std::wstring& stFileName)
        {
	        m_stFileName = stFileName;
	        m_bDirty = true;
        }

		const std::wstring& Image::GetFilename(void) const
		{
			return m_stFileName;
		}

        void Image::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData Data;
	        Data.stFileName = m_stFileName;

			int width = GetWidth(), height = GetHeight();

			//crop or scale?
			if(m_bCrop)
				Data.rSrcRect.Set(-m_offsetX, -m_offsetY, (int)(width/m_zoom), (int)(height/m_zoom));
			else
				Data.bCrop = false;

	        Data.rDestRect.Set(0, 0, width, height);
	        m_vRenderData.push_back(Data);
        }
    
    }
}