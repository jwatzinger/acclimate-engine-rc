#pragma once
#include "BaseImage.h"

namespace acl
{
    namespace gui
    {

		class ACCLIMATE_API Image :
	        public BaseImage
        {
        public:
	        Image(float x, float y, float width, float height, const std::wstring& stFileName);

			void SetFileName(const std::wstring& stFileName);

			const std::wstring& GetFilename(void) const;

	        //events
	        void OnRedraw(void) override;

        private:
#pragma warning( disable: 4251 )
	        std::wstring m_stFileName;
#pragma warning( default: 4251 )
        };

    }
}

