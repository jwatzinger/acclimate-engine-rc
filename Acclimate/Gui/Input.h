#pragma once

namespace acl
{
	namespace gui
	{

		// TODO: always ensure this and "Keys" matches
		enum class Actions
		{
			LEFT_MOUSE = 0, LEFT_DOUBLE, RIGHT_MOUSE,
			DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, UP_ARROW,
			ENTER, ESC, DELETE_KEY,
			A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
		};

		enum class State
		{
			LEFT_HOLD = 0,
			RIGHT_HOLD,
			SHIFT_HOLD,
			CTRL_HOLD,
			ALT_HOLD
		};

		enum class Ranges
		{
			MOVE_X = 0,
			MOVE_Y,
			MOUSE_WHEEL,
		};

		enum class Values
		{
			MOUSE_X = 0,
			MOUSE_Y
		};

		enum class Keys
		{
			DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, UP_ARROW,
			ENTER, ESC, DELETE_KEY,
			A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
			NUM_KEYS
		};

	}
}