#include "InputHandler.h"
#include "Module.h"
#include "Widget.h"
#include "BaseController.h"
#include "Shortcut.h"

namespace acl
{
    namespace gui
    {

		typedef input::MappedInputWrapper<Actions, State, Ranges, Values> MappedInput;

        InputHandler::InputHandler(Module& module): m_pModule(&module), m_bLeftHold(false),
			m_bRightHold(false), m_pOldWidget(nullptr), m_pHandler(nullptr)
        {
        }

		void InputHandler::SetHandler(input::Handler& handler)
		{
			if(m_pHandler)
				m_pHandler->SigProcessInput.Disconnect(this, &InputHandler::ReceiveInput);

			handler.SigProcessInput.Connect(this, &InputHandler::ReceiveInput);
			m_pHandler = &handler;
		}

		void InputHandler::ReceiveInput(const input::MappedInput& input)
		{
			HandleInput(input);
		}

		void InputHandler::HandleInput(MappedInput input)
        {
			// mouse cursor
			float x = 0.0f, y = 0.0f;
			input.HasValue(Values::MOUSE_X, x);
			input.HasValue(Values::MOUSE_Y, y);
			const math::Vector2 vMousePos((int)x, (int)y);
				
			float moveX = 0.0f, moveY = 0.0f;
			bool bMovedX = input.HasRange(Ranges::MOVE_X, moveX);
			bool bMovedY = input.HasRange(Ranges::MOVE_Y, moveY);
			
			const math::Vector2 vMoved((int)moveX, (int)moveY);

			Widget* pWidget = m_pModule->GetWidget(vMousePos, true);
			Widget* pActiveWidget = m_pModule->GetActiveWidget();

			if(bMovedX || bMovedY || m_pOldWidget != pWidget)
			{
				if(m_pOldWidget && !m_pModule->HasRegistered(*m_pOldWidget))
					m_pOldWidget = nullptr;

                // check for active Widget
		        if(pActiveWidget)
		        {
                    // if current and active differ, emit mouse off event
			        if(pWidget != pActiveWidget)
				        pActiveWidget->OnMoveOff();
                    // if old and active differ, emit mouse on event
			        if(m_pOldWidget != pActiveWidget)
				        pActiveWidget->OnMoveOn();

                    // emit mouse move event on active
			        pActiveWidget->OnMove(vMousePos);
		        }
				// emit mouse move event to current widget
		        else if(pWidget)
			        pWidget->OnMove(vMousePos);

				//if mouse moved to another widget
				if(m_pOldWidget != pWidget)
				{
					// emit mouse off event to old widget
					if(m_pOldWidget)
						m_pOldWidget->OnMoveOff();
					// emit mouse on event to current widget
					if(pWidget)
						pWidget->OnMoveOn();
				}

				// update last mouse position
				m_vLastMousePos = vMousePos;
				m_pOldWidget = pWidget;
			}

			Widget* pFocusWidget = m_pModule->GetFocusWidget();
			pWidget = m_pModule->GetWidget(m_vLastMousePos);
	        //left mouse click event
	        if(input.HasAction(Actions::LEFT_MOUSE))
	        {
                // emit click event for current widget
				if(pWidget)
					pWidget->OnClick(MouseType::LEFT);
				else if(pFocusWidget) // todo: probably find out better way to handle focus loss for executive focused widget => window "bing"ing when clicking empty area, context menu closing...
					pFocusWidget->OnFocus(false);

				m_bLeftHold = true;
	        }
	        //left double click event
			else if(input.HasAction(Actions::LEFT_DOUBLE))
	        {
                // emit double click event for current widget
		        if(pWidget)
			        pWidget->OnDoubleClick();
	        }
	        //left mouse hold events
	        else if(input.HasState(State::LEFT_HOLD))
	        {
		        //do we have an active widget?
		        if(pActiveWidget)
		        {
			        //check if mouse has been moved
			        if(bMovedX || bMovedY)
				        pActiveWidget->OnDrag(vMoved);
                    
                    // emit hold event if mouse is over active widget
			        pActiveWidget->OnHold(pActiveWidget->Over(m_vLastMousePos));
		        }
		        else
		        {
			        //check if mouse has been moved
			        if(pWidget && (bMovedX || bMovedY))
				        pWidget->OnDrag(vMoved);
		        }
				
				m_bLeftHold = true;
	        }	
	        //mouse release event
	        else if(m_bLeftHold)
	        {
				if(pActiveWidget)
					pActiveWidget->OnRelease(MouseType::LEFT, pActiveWidget->Over(m_vLastMousePos));
                // emit release event on current widget
		        if(pWidget && pWidget != pActiveWidget)
			        pWidget->OnRelease(MouseType::LEFT, pWidget->Over(m_vLastMousePos));

				m_bLeftHold = false;
	        } 

			//right mouse click event
			if(input.HasAction(Actions::RIGHT_MOUSE))
			{
				// emit click event for current widget
				if(pWidget)
					pWidget->OnClick(MouseType::RIGHT);
				else if(pFocusWidget) // todo: probably find out better way to handle focus loss for executive focused widget => window "bing"ing when clicking empty area, context menu closing...
					pFocusWidget->OnFocus(false);

				m_bRightHold = true;
			}
			else if(m_bRightHold && !input.HasState(State::RIGHT_HOLD))
			{
				if(pWidget)
					pWidget->OnRelease(MouseType::RIGHT, pWidget->Over(m_vLastMousePos));

				m_bRightHold = false;
			}

	        //mouse wheel event
			float wheel;
	        if(input.HasRange(Ranges::MOUSE_WHEEL, wheel))
	        {
                // emit wheel move event on current widget
		        if(pWidget)
			        pWidget->OnWheelMove((int)wheel);
	        }
            // emit hover event on current widget
            if(pWidget)
                pWidget->OnHover();

			// set shortcut additional keys
			m_pModule->SetComboKeyState(ComboKeys::SHIFT, input.HasState(State::SHIFT_HOLD));
			m_pModule->SetComboKeyState(ComboKeys::CTRL, input.HasState(State::CTRL_HOLD));
			m_pModule->SetComboKeyState(ComboKeys::ALT, input.HasState(State::ALT_HOLD));

            // set cursor position
			if(m_pModule->m_cursor.IsVisible())
				m_pModule->m_cursor.SetPosition(m_vLastMousePos);
			else
			{
				const auto& vScreenSize = m_pModule->GetScreenSize();
				m_vLastMousePos = math::Vector2(vScreenSize.x / 2, vScreenSize.y / 2);
				m_pModule->m_cursor.MoveTo(m_vLastMousePos);
			}

            if(pFocusWidget)
	        {
		        //key down events
		        for(unsigned int i=0; i<(unsigned int)Keys::NUM_KEYS; i++)
		        {
			        if(input.HasAction((Actions)(i+3)))
						pFocusWidget->OnKeyDown((Keys)i);
		        }
	        }

            // update module
	        m_pModule->Update();
        }
        
    }
}