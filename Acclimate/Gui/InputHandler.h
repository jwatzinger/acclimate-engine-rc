#pragma once
#include "Input.h"
#include "..\Core\Dll.h"
#include "..\Input\Handler.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gfx
	{
		class Screen;
	}

	namespace input
	{
		struct MappedInput;
		class Handler;
	}

	namespace gui
	{

		class Module;
		class Widget;

		class ACCLIMATE_API InputHandler
		{
			typedef input::MappedInputWrapper<Actions, State, Ranges, Values> MappedInput;
		public:
			InputHandler(Module& module);

			void SetHandler(input::Handler& handler);
			void ReceiveInput(const input::MappedInput& input);

		private:

			void HandleInput(MappedInput input);

			Module* m_pModule;
			Widget* m_pOldWidget;

			input::Handler* m_pHandler;

			bool m_bLeftHold, m_bRightHold;
			math::Vector2 m_vLastMousePos;
		};

	}
}

