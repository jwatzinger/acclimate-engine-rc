#include "Label.h"
#include "Layout.h"

namespace acl
{
    namespace gui
    {

        Label::Label(float x, float y, float width, float height, const std::wstring& stText, const gfx::Color* pColor) : Widget(x, y, width, height), 
			m_align(CENTER | VCENTER), m_stText(stText), m_textSize(0)
        {
			if(pColor)
			{
				m_color = *pColor;
				m_bHasCustomColor = true;
			}
			else
				m_bHasCustomColor = false;
        }

		void Label::SetString(const std::wstring& stText)
		{
			m_stText = stText;
			m_bDirty = true;
		}

        void Label::SetAlign(unsigned int align)
        {
	        m_align = align;
        }

        void Label::SetColor(const gfx::Color* pColor)
        {
			if(pColor)
			{
				if(m_color != *pColor)
				{
					m_color = *pColor;
					m_bHasCustomColor = true;
					m_bDirty = true;
				}
			}
			else
			{
				if(m_color != m_layoutColor)
				{
					m_color = m_layoutColor;
					m_bHasCustomColor = false;
					m_bDirty = true;
				}
			}
        }

		void Label::SetTextSize(unsigned int size)
		{
			m_textSize = size;
		}

		const std::wstring& Label::GetString(void) const
		{
			return m_stText;
		}

		void Label::OnLayoutChanged(const Layout& layout)
		{
			m_layoutColor = layout.GetElement(L"Label")->GetColor(L"Color");
			if(!m_bHasCustomColor)
				m_color = m_layoutColor;
		}

        void Label::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        //don't draw if no text
	        if(m_stText.empty())
		        return;

	        RenderData grData;
	        grData.stLabel = m_stText;
            grData.labelColor = m_color;
		    grData.labelFlags = m_align;
		    grData.rDestRect.Set(0, 0, GetWidth(), GetHeight());
			grData.textSize = m_textSize;

	        m_vRenderData.push_back(grData);
        }

    }
}