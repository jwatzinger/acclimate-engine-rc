#pragma once
#include "Widget.h"
#include "..\Gfx\Color.h"

namespace acl
{
	namespace gui
	{

		enum Align
		{
			LEFT = 0x00, CENTER = 0x01, RIGHT = 0x02, TOP = 0x00, VCENTER = 0x04, BOTTOM = 0x08
		};

		class ACCLIMATE_API Label :
			public Widget
		{
		public:
			Label(float x, float y, float width, float height, const std::wstring& stText = L"", const gfx::Color* pColor = nullptr);

			void SetAlign(unsigned int align);
			void SetString(const std::wstring& stText);
            void SetColor(const gfx::Color* pColor);
			void SetTextSize(unsigned int size);

			const std::wstring& GetString(void) const;

			void OnRedraw(void) override;
			void OnLayoutChanged(const Layout& layout) override;

		private:
#pragma warning( disable: 4251 )
			unsigned int m_align, m_textSize;
			bool m_bHasCustomColor;
			std::wstring m_stText;
			math::Rect m_rLabel;

            gfx::Color m_color, m_layoutColor;
#pragma warning( default: 4251 )
		};

	}
}

