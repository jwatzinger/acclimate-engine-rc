#include "Layout.h"

namespace acl
{
	namespace gui
	{

		Layout::Layout(const std::wstring& stFilename): m_stFilename(stFilename), m_vReferenceSize(1920, 1080)
		{
		}

		void Layout::SetLayout(const std::wstring& stFilename)
		{
			m_stFilename = stFilename;
		}

		const std::wstring& Layout::GetLayout(void) const
		{
			return m_stFilename;
		}

		void Layout::SetReferenceSize(const math::Vector2& vSize)
		{
			m_vReferenceSize = vSize;
		}

		const math::Vector2& Layout::GetReferenceSize(void) const
		{
			return m_vReferenceSize;
		}

		void Layout::AddElement(const std::wstring& stName, const Element& element)
		{
			m_mElements[stName] = element;
		}

		Element* Layout::GetElement(const std::wstring& stName)
		{
			if(m_mElements.count(stName))
				return &m_mElements[stName];
			else
				return nullptr;			
		}

		const Element* Layout::GetElement(const std::wstring& stName) const
		{
			if(m_mElements.count(stName))
				return &m_mElements.at(stName);
			else
				return nullptr;
		}

	}
}
