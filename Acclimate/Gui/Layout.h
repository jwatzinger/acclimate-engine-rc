#pragma once
#include <string>
#include <unordered_map>
#include "Element.h"
#include "..\Core\Dll.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API Layout
		{
			typedef std::unordered_map<std::wstring, Element> ElementMap;
		public:
			Layout(const std::wstring& stFilename);

			void SetLayout(const std::wstring& stFilename);
			const std::wstring& GetLayout(void) const;

			void SetReferenceSize(const math::Vector2& vSize);
			const math::Vector2& GetReferenceSize(void) const;

			void AddElement(const std::wstring& stName, const Element& element);
			Element* GetElement(const std::wstring& stName);
			const Element* GetElement(const std::wstring& stName) const;

		private:

#pragma warning( disable: 4251 )

			std::wstring m_stFilename;
			math::Vector2 m_vReferenceSize;

			ElementMap m_mElements;

#pragma warning( default: 4251 )
		};

	}
}
