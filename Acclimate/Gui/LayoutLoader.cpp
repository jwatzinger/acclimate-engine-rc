#include "LayoutLoader.h"
#include "Element.h"
#include "..\System\Log.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace gui
	{

		LayoutLoader::LayoutLoader(Module& module, const gfx::Textures& textures) : m_module(module),
			m_pTextures(&textures)
		{
		}

		void LayoutLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			if(auto pRoot = doc.Root(L"Layout"))
			{
				Layout layout(pRoot->Attribute(L"texture")->GetValue());

				m_module.m_layout = layout;
				
				// reference size
				if(auto pReference = pRoot->FirstNode(L"Reference"))
				{
					const unsigned int width = pReference->Attribute(L"width")->AsInt();
					const unsigned int height = pReference->Attribute(L"height")->AsInt();

					m_module.m_layout.SetReferenceSize(math::Vector2(width, height));
				}

				// elements
				if(auto pElements = pRoot->FirstNode(L"Elements"))
				{
					if(auto pElementNodes = pElements->Nodes(L"Element"))
					{
						for(auto pElement : *pElementNodes)
						{
							const std::wstring& stName = pElement->Attribute(L"name")->GetValue();

							Element element;

							if(auto pParts = pElement->Nodes(L"Part"))
							{
								for(auto pPart : *pParts)
								{
									const std::wstring& stPartName = pPart->Attribute(L"name")->GetValue();

									const unsigned int x = pPart->Attribute(L"x")->AsInt();
									const unsigned int y = pPart->Attribute(L"y")->AsInt();
									const unsigned int w = pPart->Attribute(L"w")->AsInt();
									const unsigned int h = pPart->Attribute(L"h")->AsInt();

									const math::Rect r(x, y, w, h);

									element.SetPart(stPartName, r);
								}
							}

							if(auto pColors = pElement->Nodes(L"Color"))
							{
								for(auto pColor : * pColors)
								{
									const std::wstring& stColorName = pColor->Attribute(L"name")->GetValue();

									const unsigned int r = pColor->Attribute(L"r")->AsInt();
									const unsigned int g = pColor->Attribute(L"g")->AsInt();
									const unsigned int b = pColor->Attribute(L"b")->AsInt();

									unsigned int a = 255;
									if(auto pAlpha = pColor->Attribute(L"a"))
										a = pAlpha->AsInt();

									element.SetColor(stColorName, gfx::Color(r, g, b, a));
								}
							}

							m_module.m_layout.AddElement(stName, element);
						
						}
					}
				}

				// cursors
				if(auto pCursors = pRoot->FirstNode(L"Cursors"))
				{
					if(auto pCursorNodes = pCursors->Nodes(L"Cursor"))
					{
						for(auto pCursor : *pCursorNodes)
						{
							const CursorState state = pCursor->Attribute(L"state")->AsType<CursorState>();
							if(state >= CursorState::SIZE)
								sys::log->Out(sys::LogModule::GUI, sys::LogType::WARNING, "cursor state", (unsigned int)state, "is not available.");
							else
							{
								const std::wstring& stTextureName = *pCursor->Attribute(L"texture");
								if(auto pTexture = m_pTextures->Get(stTextureName))
								{
									unsigned int offX = 0, offY = 0;
									if(auto pOffX = pCursor->Attribute(L"offx"))
										offX = pOffX->AsInt();
									if(auto pOffY = pCursor->Attribute(L"offy"))
										offY = pOffY->AsInt();

									m_module.m_cursor.SetState(state, *pTexture, offX, offY);
								}
								else
									sys::log->Out(sys::LogModule::GUI, sys::LogType::WARNING, "texture", stTextureName, "not found for cursor", (unsigned int)state);
							}
						}
					}
				}
			}
		}

	}
}
