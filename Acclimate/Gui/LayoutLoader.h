#pragma once
#include "Module.h"
#include "..\Core\Dll.h"
#include "..\Gfx\Textures.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API LayoutLoader
		{
		public:
			LayoutLoader(Module& module, const gfx::Textures& textures);

			void Load(const std::wstring& stFilename) const;

		private:

			Module& m_module;
			const gfx::Textures* m_pTextures;
		};

	}
}

