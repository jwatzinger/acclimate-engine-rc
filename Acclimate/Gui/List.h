#pragma once
#include "ScrollArea.h"
#include "ListItem.h"
#include "Textbox.h"
#include "ToolButton.h"
#include "Selector.h"
#include "Input.h"
#include "Layout.h"

namespace acl
{
    namespace gui
    {

        template<typename I>
        class List :
	        public Widget
        {
	        typedef std::vector<ListItem<I>*> ItemVector;

        public:
	        List(float x, float y, float width, float height, float itemHeight): Widget(x, y, width, height), 
				m_itemHeight(itemHeight), m_selectedId(-1), m_area(0.0f, 0.0f, 1.0f, 1.0f, 0, 1), 
				m_selector(0.0f, 0.0f, 1.0f, m_itemHeight)
	        {
				m_focus = FocusState::KEEP_FOCUS;

		        //drop down scroll area
		        AddChild(m_area);

                //select image
                m_selector.OnInvisible();
				m_selector.OnDisable();
				m_selector.SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
                m_area.AddChild(m_selector);
	        }

	        ListItem<I>& AddItem(const std::wstring& stName, I item)
	        {
		        const float pos = m_vItems.size()*m_itemHeight;

		        ListItem<I>* pItem = new ListItem<I>(0.0f, pos, 1.0f, m_itemHeight, m_vItems.size(), stName, item);
				pItem->SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
		        pItem->SigWheelMoved.Connect(&m_area, &ScrollArea::OnWheelMove);
		        pItem->SigPicked.Connect(this, &List::OnPickItem);
				pItem->SetColor(m_textColor);

		        m_area.SetAreaHeight((int)((m_vItems.size()+1)*m_itemHeight*m_pModule->GetScreenSize().y));
		        m_area.AddChild(*pItem);

		        m_vItems.push_back(pItem);
                
                m_selector.PushToTop();

                return *pItem;
	        }

			void SetContextMenu(ContextMenu* pMenu)
			{
				m_area.SetContextMenu(pMenu);
			}

	        I GetContent(void) const
	        {
		        return m_selectedItem;
	        }

            size_t GetSize(void) const
            {
                return m_vItems.size();
            }

			bool IsEmpty(void) const
			{
				return m_vItems.empty();
			}

			int GetSelectedId(void) const
			{
				return m_selectedId;
			}

			const std::wstring& GetSelectedName(void) const
			{
				if(m_selectedId != -1)
					return m_vItems[m_selectedId]->GetName();
				else
				{
					static const std::wstring& stName = L"";
					return stName;
				}
			}

            bool IsSelected(void) const
            {
                return m_selectedId > -1;
            }

            void SelectByName(const std::wstring& stName)
            {
                size_t id = 0;
                for(auto pItem : m_vItems)
                {
                    if( pItem->GetName() == stName )
                    {
						OnPickItem(*pItem);
						return;
                    }

                    id++;
                }

                m_selector.OnInvisible();
                m_selectedId = -1;
            }

            void SelectById(size_t id)
            {
                if(id < m_vItems.size())
					OnPickItem(*m_vItems[id]);
            }

            void Clear(void)
            {
                for(ListItem<I>* pItem : m_vItems)
                {
                    delete pItem;
                }

                m_vItems.clear();
				m_selector.OnInvisible();
                m_selectedId = -1;
            }

            void OnKeyDown(Keys key) override
            {
				Widget::OnKeyDown(key);

                switch(key)
                {
				case Keys::DOWN_ARROW:
                    SelectById(m_selectedId + 1);
                    break;
				case Keys::UP_ARROW:
                    SelectById(m_selectedId - 1);
                    break;
				case Keys::ENTER:
                    if(m_selectedId > -1)
                        SigConfirm();
                    break;
                }
            }

	        void OnRedraw(void) override
            {
                m_vRenderData.clear();
		        RenderData data;
		        data.rDestRect.Set(0, 0, GetWidth()-16, GetHeight());
		        data.rSrcRect = m_pModule->m_layout.GetElement(L"List")->GetPart(L"Fill");
		        m_vRenderData.push_back(data);
            }

			void OnLayoutChanged(const Layout& layout) override
			{
				const auto pElement = layout.GetElement(L"List");
				m_textColor = pElement->GetColor(L"ItemText");
			}

            core::Signal<> SigConfirm;
	        core::Signal<I> SigItemPicked;
			core::Signal<const std::wstring&> SigTextChanged;

        private:

	        void OnPickItem(ListItem<I>& item)
	        {
                I& tempItem = item.GetItem();
				m_selectedId = item.GetId();
				m_selectedItem = tempItem;

		        SigItemPicked(tempItem);
		        SigTextChanged(item.GetName());

                m_selector.OnVisible();
                m_selector.SetY(item.GetId()*m_itemHeight);

				// TODO: better solution to resolve issue when item is selected before
				// first update
				if(!item.GetY() && !m_area.GetOffsetY())
					return;

				const auto y = (int)(item.GetY() - GetY() + item.GetHeight());
				const auto height = GetHeight();

				if(y > height)
					m_area.SetOffsetY(y - height + m_area.GetOffsetY());
				else
				{
					const auto leftY = y - item.GetHeight();
					if(leftY < 0)
						m_area.SetOffsetY(leftY + m_area.GetOffsetY());
				}
	        }

            float m_itemHeight;
            int m_selectedId;
			gfx::Color m_textColor;

	        ScrollArea m_area;
            Selector m_selector;

	        ItemVector m_vItems;

	        I m_selectedItem;
        };

    }
}
