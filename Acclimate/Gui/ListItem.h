#pragma once
#include "Widget.h"
#include "Icon.h"

namespace acl
{
    namespace gui
    {

        template<typename I>
        class ListItem :
	        public Widget
        {
        public:
	        ListItem(float x, float y, float width, float height, int id, const std::wstring& stName, I item): Widget(x, y, width, height), 
				m_item(item), m_stName(stName), m_id(id), m_pIcon(nullptr)
	        {
	        }

			~ListItem(void)
			{
				delete m_pIcon;
			}

            void SetColor(const gfx::Color& color)
            {
				m_color = color;
            }

            int GetId(void) const
            {
                return m_id;
            }

            I& GetItem(void)
            {
                return m_item;
            }

            const std::wstring& GetName(void) const
            {
                return m_stName;
            }

			Icon* SetIcon(const std::wstring& stName, const math::Rect& rSrc)
			{
				delete m_pIcon;

				if(!stName.empty())
				{
					m_pIcon = new Icon(0.0f, 0.0f, 1.0f, stName, rSrc);
					m_pIcon->SetPadding(gui::PaddingSide::LEFT, 8);
					AddChild(*m_pIcon);
				}
				else
					m_pIcon = nullptr;

				m_bDirty = true;

				return m_pIcon;
			}

	        void OnRelease(MouseType mouse, bool bMouseOver) override
	        {
		        Widget::OnRelease(mouse, bMouseOver);
		        if(mouse == MouseType::LEFT && bMouseOver)
			        SigPicked(*this);
	        }

			void OnRedraw(void) override
			{
				m_vRenderData.clear();
				//don't draw if no text
				if(m_stName.empty())
					return;

				unsigned int offset = 8;
				if(m_pIcon)
					offset += m_pIcon->GetWidth();

				RenderData grData;
				grData.stLabel = m_stName;
				grData.labelColor = m_color;
				grData.labelFlags = DT_LEFT | DT_VCENTER;
				grData.rDestRect.Set(offset + 8, 0, GetWidth() - 8 - offset, GetHeight());

				m_vRenderData.push_back(grData);
			}

	        core::Signal<ListItem<I>&> SigPicked;

        private:

			Icon* m_pIcon;

            int m_id;
			gfx::Color m_color;
			const std::wstring m_stName;

	        I m_item;
        };

    }
}