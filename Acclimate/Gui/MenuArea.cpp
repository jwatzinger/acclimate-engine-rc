#include "MenuArea.h"
#include "Module.h"
#include "MenuOption.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		MenuArea::MenuArea(float itemHeight) : Widget(0.0f, 1.0f, 0, itemHeight),
			m_itemHeight(itemHeight)
		{
			SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::ABS, PositionMode::REF);

			m_bVisible = false;
		}

		MenuArea::~MenuArea(void)
		{
			for(auto option : m_mOptions)
			{
				delete option.second;
			}
		}

		MenuOption& MenuArea::AddOption(const std::wstring& stName)
		{
			ACL_ASSERT(m_pModule);

			const size_t numOptions = m_mOptions.size();
			auto pOption = new MenuOption((float)numOptions*m_itemHeight, m_itemHeight, stName);
			pOption->SigHideOtherOptions.Connect(this, &MenuArea::OnHideOtherOptions);
			m_mOptions[stName] = pOption;
			AddChild(*pOption);

			// adjust metrics
			SetHeight(m_itemHeight*(numOptions + 1.0f));
			return *pOption;
		}

		void MenuArea::InsertSeperator(void)
		{
			m_vSeperator.push_back(m_mOptions.size());
			m_bDirty = true;
		}

		void MenuArea::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"MenuArea");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			Data.rSrcRect = rTopRight;
			Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			Data.rSrcRect = rBottomLeft;
			Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, 0, rLeft.width, height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			Data.rSrcRect = rBottomRight;
			Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(Data);

			// bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
			Data.rSrcRect = rBottom;
			Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(Data);

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// icon area
			const math::Rect& rArea = element.GetPart(L"IconArea");
			const unsigned int itemWidth = (unsigned int)((m_itemHeight * m_pModule->m_layout.GetReferenceSize().y) * 1.1f) - 1;
			Data.rSrcRect = rArea;
			Data.rDestRect.Set(rTopLeft.width, 0, itemWidth, height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width + itemWidth, 0, width - rTopLeft.width - rBottomRight.width - itemWidth, height - rBottomRight.width);
			m_vRenderData.push_back(Data);

			// top
			int parentWidth = 0;
			if(!dynamic_cast<MenuOption*>(m_pParent))
				parentWidth = m_pParent->GetWidth();
			const math::Rect& rTop = element.GetPart(L"Top");
			Data.rSrcRect = rTop;
			Data.rDestRect.Set(parentWidth - 1, 0, width - rTopRight.width - parentWidth + 1, rTop.height);
			m_vRenderData.push_back(Data);

			for(auto position : m_vSeperator)
			{
				Data.rDestRect.Set(itemWidth+5, (int)(position*m_itemHeight*m_pModule->m_layout.GetReferenceSize().y), width - itemWidth - 10, 1);
				m_vRenderData.push_back(Data);
			}
		}

		void MenuArea::OnVisible(void)
		{
			m_width = 0;
			unsigned int shortcut = 0;
			for(auto option : m_mOptions)
			{
				m_width -= shortcut;
				shortcut = max(shortcut, option.second->GetShortcutWidth());
				m_width += shortcut;

				SetWidth(max(m_width, option.second->GetContentWidth()+shortcut*2));
			}
			Widget::OnVisible();
		}

		void MenuArea::OnInvisible(void)
		{
			Widget::OnInvisible();
			m_pModule->AddDirtyRect(math::Rect(GetX(), GetY(), GetWidth(), GetHeight()));
			for(auto& option : m_mOptions)
			{
				option.second->SetOver(false);
			}
		}

		void MenuArea::OnHideOtherOptions(const MenuOption& option)
		{
			for(auto optionItem : m_mOptions)
			{
				if(optionItem.second != &option)
					optionItem.second->OnCollapse();
			}
		}

	}
}

