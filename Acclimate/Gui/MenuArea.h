#pragma once
#include <unordered_map>
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class MenuOption;

		class MenuArea :
			public Widget
		{
			typedef std::unordered_map<std::wstring, MenuOption*> OptionMap;
			typedef std::vector<unsigned int> SeperatorVector;
		public:
			MenuArea(float itemHeight);
			~MenuArea(void);

			MenuOption& AddOption(const std::wstring& stName);
			void InsertSeperator(void);

			void OnRedraw(void) override;
			void OnVisible(void) override;
			void OnInvisible(void) override;

		private:

			void OnHideOtherOptions(const MenuOption& option);

			float m_itemHeight;

			OptionMap m_mOptions;
			SeperatorVector m_vSeperator;
		};

	}
}


