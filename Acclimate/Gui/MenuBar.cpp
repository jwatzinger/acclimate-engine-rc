#include "MenuBar.h"
#include "Module.h"
#include "MenuItem.h"

namespace acl
{
	namespace gui
	{

		MenuBar::MenuBar(float y, float height): Widget(0.0f, y, 1.0f, height), m_currentWidth(0)
		{
		}

		MenuBar::~MenuBar(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
		}

		MenuItem& MenuBar::AddItem(const std::wstring& stName)
		{
			MenuItem* pItem = new MenuItem((float)m_currentWidth, stName);
			pItem->SigExpand.Connect(this, &MenuBar::OnExpandItem);
			m_vItems.push_back(pItem);
			AddChild(*pItem);

			m_currentWidth += pItem->GetWidth();

			return *pItem;
		}

		MenuItem* MenuBar::GetItem(const std::wstring& stName)
		{
			for(auto pItem : m_vItems)
			{
				if(pItem->GetName() == stName)
					return pItem;
			}

			return nullptr;
		}

		void MenuBar::OnRedraw(void)
		{
	        m_vRenderData.clear();
	        RenderData Data;
	        Data.rSrcRect = m_pModule->m_layout.GetElement(L"Toolbar")->GetPart(L"Body");
	        Data.rDestRect.Set(0, 0, GetWidth(), GetHeight());
	        m_vRenderData.push_back(Data);
		}

		void MenuBar::OnExpandItem(bool bExpand)
		{
			for(auto pItem : m_vItems)
			{
				pItem->SetExpandOnHover(bExpand);
			}
		}

	}
}
