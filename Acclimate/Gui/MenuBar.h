#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class MenuItem;

		class ACCLIMATE_API MenuBar :
			public Widget
		{
			typedef std::vector<MenuItem*> ItemVector;
		public:
			MenuBar(float y, float height);
			~MenuBar(void);
			
			MenuItem& AddItem(const std::wstring& stName);

			MenuItem* GetItem(const std::wstring& stName);

			void OnRedraw(void) override;

		private:
#pragma warning( disable: 4251 )

			void OnExpandItem(bool bExpand);

			unsigned int m_currentWidth;

			ItemVector m_vItems;
#pragma warning( default: 4251 )
		};

	}
}
