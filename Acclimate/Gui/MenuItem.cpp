#include "MenuItem.h"
#include "Module.h"
#include "MenuOption.h"

namespace acl
{
	namespace gui
	{

		MenuItem::MenuItem(float x, const std::wstring& stName) : Widget(x, 0.0f, 0.0f, 1.0f), m_bOver(false), m_stName(stName),
			m_area(0.02f), m_bExpandOnHover(false)
		{
			SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::ABS, PositionMode::REL);

			AddChild(m_area);

			m_focus = FocusState::KEEP_FOCUS;
		}

		void MenuItem::InsertSeperator(void)
		{
			m_area.InsertSeperator();
			m_bDirty = true;
		}

		MenuOption& MenuItem::AddOption(const std::wstring& stName)
		{
			auto& option = m_area.AddOption(stName);
			option.SigHideParent.Connect(this, &MenuItem::OnHide);
			return option;
		}

		void MenuItem::SetExpandOnHover(bool bHover)
		{
			m_bExpandOnHover = bHover;
		}

		const std::wstring& MenuItem::GetName(void) const
		{
			return m_stName;
		}

		void MenuItem::OnClick(MouseType mouse)
		{
			if(mouse == MouseType::LEFT)
				OnExpand(!m_area.IsVisible());

			Widget::OnClick(mouse);
		}

		void MenuItem::OnMoveOn(void)
		{
			if(m_bExpandOnHover)
				OnExpand(true);

			m_bOver = true;
			m_bDirty = true;
		}

		void MenuItem::OnMoveOff(void)
		{
			m_bOver = false;
			m_bDirty = true;
		}

		void MenuItem::OnFocus(bool bFocus)
		{
			Widget::OnFocus(bFocus);
			
			if(!bFocus)
				OnExpand(false);
		}

		void MenuItem::OnRedraw(void)
		{
			m_vRenderData.clear();
	        RenderData Data;

			const int width = GetWidth(), height = GetHeight();

			// select
			if(m_area.IsVisible())
			{
				const Element& element = *m_pModule->m_layout.GetElement(L"MenuItemSelect");

				//top left
				const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
				Data.rSrcRect = rTopLeft;
				Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
				m_vRenderData.push_back(Data);

				// top right
				const math::Rect& rTopRight = element.GetPart(L"TopRight");
				Data.rSrcRect = rTopRight;
				Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
				m_vRenderData.push_back(Data);

				// top
				const math::Rect& rTop = element.GetPart(L"Top");
				Data.rSrcRect = rTop;
				Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
				m_vRenderData.push_back(Data);

				// left bar
				const math::Rect& rLeft = element.GetPart(L"Left");
				Data.rSrcRect = rLeft;
				Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height);
				m_vRenderData.push_back(Data);

				// right bar
				const math::Rect& rRight = element.GetPart(L"Right");
				Data.rSrcRect = rRight;
				Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height);
				m_vRenderData.push_back(Data);

				// background
				const math::Rect& rBack = element.GetPart(L"Back");
				Data.rSrcRect = rBack;
				Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rRight.width, height - rTopLeft.height);
				m_vRenderData.push_back(Data);
			}
			// hover
			else if(m_bOver)
			{
				const Element& element = *m_pModule->m_layout.GetElement(L"MenuItemOver");

				//top left
				const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
				Data.rSrcRect = rTopLeft;
				Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
				m_vRenderData.push_back(Data);

				// top right
				const math::Rect& rTopRight = element.GetPart(L"TopRight");
				Data.rSrcRect = rTopRight;
				Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
				m_vRenderData.push_back(Data);

				// top
				const math::Rect& rTop = element.GetPart(L"Top");
				Data.rSrcRect = rTop;
				Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
				m_vRenderData.push_back(Data);

				// bottom left
				const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
				Data.rSrcRect = rBottomLeft;
				Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
				m_vRenderData.push_back(Data);

				// left bar
				const math::Rect& rLeft = element.GetPart(L"Left");
				Data.rSrcRect = rLeft;
				Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
				m_vRenderData.push_back(Data);

				// bottom right
				const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
				Data.rSrcRect = rBottomRight;
				Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
				m_vRenderData.push_back(Data);

				// bottom bar
				const math::Rect& rBottom = element.GetPart(L"Bottom");
				Data.rSrcRect = rBottom;
				Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
				m_vRenderData.push_back(Data);

				// right bar
				const math::Rect& rRight = element.GetPart(L"Right");
				Data.rSrcRect = rRight;
				Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
				m_vRenderData.push_back(Data);

				// background
				const math::Rect& rBack = element.GetPart(L"Back");
				Data.rSrcRect = rBack;
				Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rBottomRight.width, height - rTopLeft.height - rBottomRight.width);
				m_vRenderData.push_back(Data);
			}
			
			// caption
			Data.rDestRect.Set(0, 0, width, height);
			Data.stLabel = m_stName;
			Data.labelFlags = DT_CENTER | DT_VCENTER;
	        m_vRenderData.push_back(Data);
		}

		void MenuItem::OnExpand(bool bExpand)
		{
			if(bExpand)
			{
				PushToTop();
				m_area.OnVisible();
				OnFocus(true);
			}
			else
			{
				m_area.OnInvisible();
				m_bOver = false;
				m_bDirty = true;
			}

			SigExpand(bExpand);
		}

		void MenuItem::OnHide(void)
		{
			OnExpand(false);
		}

		void MenuItem::OnRegister(Module& module)
		{
			Widget::OnRegister(module);

			auto rect = module.CalculateTextRect(m_stName);
			SetWidth(rect.width + 18.0f);
			m_absWidth = (int)m_width; // hack, in order to make calculation of next item work => TODO: fix
		}

	}
}
