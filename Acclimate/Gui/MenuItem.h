#pragma once
#include "Widget.h"
#include "MenuArea.h"

namespace acl
{
	namespace gui
	{

		class MenuOption;

		class ACCLIMATE_API MenuItem :
			public Widget
		{
		public:
			MenuItem(float x, const std::wstring& stName);

			void InsertSeperator(void);
			MenuOption& AddOption(const std::wstring& stName);
			void SetExpandOnHover(bool bExpand);

			const std::wstring& GetName(void) const;

			void OnClick(MouseType mouse) override;
			void OnMoveOn(void) override;
			void OnMoveOff(void) override;
			void OnFocus(bool bFocus) override;
			void OnRedraw(void) override;
			void OnRegister(Module& module) override;
			
			core::Signal<bool> SigExpand;

		private:
#pragma warning( disable: 4251 )

			void OnExpand(bool bExpand);
			void OnHide(void);

			bool m_bOver, m_bExpandOnHover;
			std::wstring m_stName;

			MenuArea m_area;
#pragma warning( default: 4251 )
		};

	}
}