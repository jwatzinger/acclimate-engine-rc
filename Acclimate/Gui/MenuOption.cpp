#include "MenuOption.h"
#include "Module.h"
#include "MenuArea.h"

namespace acl
{
	namespace gui
	{

		MenuOption::MenuOption(float y, float itemHeight, const std::wstring& stName) : Widget(0.0f, y, 1.0f, itemHeight), m_stName(stName),
			m_bOver(false), m_pArea(nullptr)
		{
			SetPadding(2, 1, 4, 1);
			SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);

			m_focus = FocusState::IGNORE_FOCUS;
		}

		MenuOption::~MenuOption(void)
		{
			delete m_pArea;
		}

		void MenuOption::SetOver(bool bOver)
		{
			m_bDirty |= m_bOver != bOver;
			m_bOver = bOver;
		}

		unsigned int MenuOption::GetContentWidth(void) const
		{
			return m_pModule->CalculateTextRect(m_stName).width + 46 + 22;
		}

		unsigned int MenuOption::GetShortcutWidth(void) const
		{
			if(m_pShortcut)
				return m_pModule->CalculateTextRect(ShortcutToString(*m_pShortcut)).width;
			else
				return 0;
		}

		MenuOption& MenuOption::AddOption(const std::wstring& stName)
		{
			if(!m_pArea)
			{
				m_pArea = new MenuArea(0.02f);
				m_pArea->SetX(0.98f);
				m_pArea->SetY(0.02f);
				AddChild(*m_pArea);
				m_bDirty = true;
			}

			auto& option = m_pArea->AddOption(stName);
			option.SigHideParent.Connect(this, &MenuOption::OnHideParent);
			option.SigReleased.Connect(m_pArea, &MenuArea::OnInvisible);
			return option;
		}

		void MenuOption::InsertSeperator(void)
		{
			if(m_pArea)
				m_pArea->InsertSeperator();
		}

		void MenuOption::Clear(void)
		{
			delete m_pArea;
			m_pArea = nullptr;
		}

		void MenuOption::OnEnable(void)
		{
			m_bDirty |= m_bDisabled;
			Widget::OnEnable();
		}

		void MenuOption::OnDisable(void)
		{
			m_bDirty |= !m_bDisabled;
			Widget::OnDisable();
		}

		void MenuOption::OnMoveOn(void)
		{
			if(m_pArea)
				m_pArea->OnVisible();

			SigHideOtherOptions(*this);
			SetOver(true);
		}

		void MenuOption::OnMoveOff(void)
		{
			if(m_pArea)
				return;

			SetOver(false);
		}

		void MenuOption::OnRelease(MouseType mouse, bool bMouseOver)
		{
			if(!m_bActive)
				return;

			if(m_pArea)
				OnActivate(false);
			else
			{
				if(mouse == MouseType::LEFT && bMouseOver)
					SetOver(false);

				OnHideParent();
				Widget::OnRelease(mouse, bMouseOver);
			}
		}

		void MenuOption::OnRedraw(void)
		{
			m_vRenderData.clear();
	        RenderData Data;

			const int width = GetWidth(), height = GetHeight();
			Data.rDestRect.Set(0, 0, width, height);
			if(m_bOver || m_bActive)
				Data.rSrcRect = m_pModule->m_layout.GetElement(L"MenuOption")->GetPart(L"Hover");

			m_vRenderData.push_back(Data);

			// caption
			Data.rDestRect.x = (int)((int)(height * 1.1f) * 1.3f);
			Data.stLabel = m_stName;
			if(m_bDisabled)
				Data.labelColor = gfx::Color(155, 155, 155);
			else
				Data.labelColor = gfx::Color(255, 255, 255);
			Data.labelFlags = DT_VCENTER | DT_LEFT;
			m_vRenderData.push_back(Data);

			// suboptions icon
			if(m_pArea)
			{
				Data.rDestRect = math::Rect(width - 16, (height-7)/2, 4, 7);
				Data.rSrcRect = math::Rect(96, 58, 4, 7);
				Data.stLabel = L"";
				m_vRenderData.push_back(Data);
			}

			// visualize shortcut
			if(m_pShortcut)
			{
				Data.stLabel = ShortcutToString(*m_pShortcut);
				const int textWidth = m_pModule->CalculateTextRect(Data.stLabel).width+6;
				Data.rDestRect = math::Rect(width - textWidth, 0, textWidth, height);
				m_vRenderData.push_back(Data);
			}
		}

		void MenuOption::OnCollapse(void)
		{
			if(m_pArea)
				m_pArea->OnInvisible();

			SetOver(false);
		}

		void MenuOption::OnHideParent(void)
		{
			SigHideParent();
		}

		void MenuOption::OnShortcut(void)
		{
			OnHideParent();
			Widget::OnShortcut();
		}

	}
}