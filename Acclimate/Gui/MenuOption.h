#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class MenuArea;
		class MenuOption;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<bool>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const MenuOption&>;

		class ACCLIMATE_API MenuOption :
			public Widget
		{
		public:

			MenuOption(float y, float itemHeight, const std::wstring& stName);
			~MenuOption(void);

			void SetOver(bool bOver);

			unsigned int GetContentWidth(void) const;
			unsigned int GetShortcutWidth(void) const;

			MenuOption& AddOption(const std::wstring& stName);
			void InsertSeperator(void);
			void Clear(void);
			
			void OnEnable(void) override;
			void OnDisable(void) override;
			void OnMoveOn(void) override;
			void OnMoveOff(void) override;
			void OnRelease(MouseType mouse, bool bMouseOver) override;
			void OnRedraw(void) override;
			void OnShortcut(void) override;
			void OnCollapse(void);

			core::Signal<> SigHideParent;
			core::Signal<const MenuOption&> SigHideOtherOptions;

		private:
#pragma warning( disable: 4251 )

			void OnHideParent(void);

			bool m_bOver;
			std::wstring m_stName;

			MenuArea* m_pArea;
#pragma warning( default: 4251 )
		};

	}
}
