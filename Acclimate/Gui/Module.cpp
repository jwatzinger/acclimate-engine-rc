#include "Module.h"
#include <algorithm>
#include "BaseController.h"
#include "ShortcutRegistry.h"
#include "..\Gfx\FontFile.h"
#include "..\Input\Keys.h"
#include "..\System\Bit.h"

namespace acl
{
	namespace gui
	{

		Module::Module(ShortcutRegistry& shortcuts, const gfx::Fonts& fonts): m_vScreenSize(2560, 1600), m_pActiveWidget(nullptr), 
			m_pExecutiveWidget(nullptr), m_pFocusController(nullptr), m_pFocusWidget(nullptr), m_layout(L"Frame"), m_fontSize(16), 
			m_mainWidget(0.0f, 0.0f, 1.0f, 1.0f), m_pShortcuts(&shortcuts), m_pFonts(&fonts)
		{
			m_mainWidget.OnRegister(*this);
			m_mainWidget.OnParentUpdate();
			m_rDirty = math::Rect(0, 0, m_vScreenSize.x, m_vScreenSize.y);
		}

		Module::~Module(void)
		{
			m_mainWidget.Unregister();
		}

		void Module::RegisterWidget(Widget& widget)
		{
			//check for double registration
			WidgetVector::iterator ii = std::find(m_vWidgets.begin(), m_vWidgets.end(), &widget);
			if (ii != m_vWidgets.end())
				return;
			//put widget at the stack
			m_vWidgets.push_back(&widget);

			widget.OnRegister(*this);

			if(!widget.GetParent() && &m_mainWidget != &widget)
				m_mainWidget.AddChild(widget);

			widget.OnLayoutChanged(m_layout);

			//connect to cursor
			widget.SigChangeCursorState.Connect(&m_cursor, &Cursor::ChangeState);
		}

		void Module::UnregisterWidget(Widget& widget)
		{
			widget.OnFocus(false);
			widget.OnActivate(false);
			//find widget, then delete it
			WidgetVector::iterator ii = std::find(m_vWidgets.begin(), m_vWidgets.end(), &widget);
			if (ii != m_vWidgets.end())
			{
				m_vWidgets.erase(ii);
			}
		}

        void Module::RegisterController(BaseController& controller)
		{
			//check for double registration
			ControllerVector::iterator ii = std::find(m_vController.begin(), m_vController.end(), &controller);
			if (ii != m_vController.end())
				return;
			//put controller at the stack
			m_vController.push_back(&controller);
		}

		void Module::UnregisterController(BaseController& controller)
		{
			//find controller, then delete it
			ControllerVector::iterator ii = std::find(m_vController.begin(), m_vController.end(), &controller);
			if (ii != m_vController.end())
			{
                if(m_pFocusController == *ii)
                {
                    // todo: better way to resolve possible infinite recursion, probably split methods for setting and loosing focus?
                    m_pFocusController = nullptr;
					m_vController.erase(ii);
					SetFocusController(nullptr);
                }
				else
					m_vController.erase(ii);
            }
		}

		bool Module::HasRegistered(Widget& widget) const
		{
			return std::find(m_vWidgets.begin(), m_vWidgets.end(), &widget) != m_vWidgets.end();
		}

		void Module::Update(void)
		{
			m_mainWidget.OnParentUpdate();
		}

		void Module::OnKeyStroke(WCHAR key)
		{
			if(m_pFocusWidget)
			    m_pFocusWidget->OnKeyStroke(key);
			if(m_pFocusController)
			{
				const Keys guiKey = (Keys)((unsigned int)input::winToKey(key)-3);
				m_pFocusController->OnKeyPress(guiKey);
			}    
		}

		void Module::SetComboKeyState(ComboKeys keys, bool bDown)
		{
			if(bDown)
				m_comboKeys.insert(keys);
			else
				m_comboKeys.erase(keys);
		}

		bool Module::ComboKeysActive(ComboKeys keys)
		{
			unsigned int k = 0;
			for(auto key : m_comboKeys)
			{
				k |= (unsigned int)key;
			}
			return bit::contains(k, static_cast<unsigned int>(keys));
		}

		void Module::OnShortcut(unsigned int key)
		{
			if(m_pExecutiveWidget) // todo: possibly enable some shortcuts while executing?
				return;

			unsigned int keys = 0;
			for(auto key : m_comboKeys)
			{
				keys |= (unsigned int)key;
			}

			m_pShortcuts->Call(static_cast<ComboKeys>(keys), key);
		}

		Widget* Module::GetWidget(const math::Vector2& vMousePos, bool bIgnoreActive)
		{
			if(m_pExecutiveWidget)
			{
				Widget* pPickedWidget = PickWidget(vMousePos, m_pExecutiveWidget);
				if(pPickedWidget && pPickedWidget->Over(vMousePos))
					return pPickedWidget;
				else
					return nullptr;
			}
			//if an widget has the focus and we can't ignore it
			if(!bIgnoreActive && m_pActiveWidget)
				return m_pActiveWidget;

			Widget* pPickedObject = PickWidget(vMousePos, &m_mainWidget);
			if(pPickedObject && pPickedObject->Over(vMousePos))
				return pPickedObject;
			else 
				return nullptr;
		}

		Widget* Module::PickWidget(const math::Vector2& vMousePos, Widget* pParent)
		{
			//return nothing if there is no parent
			if(!pParent)
				return nullptr;

			Widget* pWidget = nullptr;

			//iterate through all children
			const WidgetVector& vChildren = pParent->GetChildren();
			for(auto pChildWidget : vChildren)
			{
				// pick if the mouse is over a non-disabled visible widget
				if( !pChildWidget->GetDisable() && pChildWidget->IsVisible())
                {
                    if( pChildWidget->Over(vMousePos) )
					    pWidget = pChildWidget;
                    else if( !pChildWidget->IsClipping() )
                    {
						Widget* pPicked = PickWidget(vMousePos, pChildWidget);
                        if(pPicked->Over(vMousePos))
                            pWidget = pPicked;
                    }
                }
			}

			//return the parent if we didn't find an widget
			//otherwise, check widgets children
			if(!pWidget)
				return pParent;
			else
				return PickWidget(vMousePos, pWidget);
		}

		void Module::SetActiveWidget(Widget* pWidget)
		{
			//tell active widget its about to become inactive
			if(m_pActiveWidget)
				m_pActiveWidget->OnActivate(false);

			m_pActiveWidget = pWidget;
		}

		void Module::SetExecutiveWidget(Widget* pWidget)
		{
			m_pExecutiveWidget = pWidget;
		}

		void Module::SetAsFree(Widget& widget)
		{
			m_mainWidget.AddChild(widget);
		}

		Widget* Module::GetActiveWidget(void)
		{
			return m_pActiveWidget;
		}

		Widget* Module::GetFocusWidget(void)
		{
			return m_pFocusWidget;
		}

        BaseController*  Module::GetFocusController(void)
        {
            return m_pFocusController;
        }

		void Module::SetFocusWidget(Widget* pWidget)
		{
			//tell focused widget its about to loose keyboard focus
			if(m_pFocusWidget)
            {
                Widget* pTemp = m_pFocusWidget;
                m_pFocusWidget = nullptr;
				pTemp->OnFocus(false);
            }

			m_pFocusWidget = pWidget;
		}

        void Module::SetFocusController(BaseController* pController)
        {
            if(m_pFocusController)
            {
                BaseController* pTemp = m_pFocusController;
                m_pFocusController = nullptr;
                pTemp->OnFocus(false);
            }

            m_pFocusController = pController;

            if(!m_pFocusController && m_vController.size())
                m_pFocusController = *m_vController.begin();
        }

		void Module::ExecuteWidget(Widget& widget)
		{
			Update();
			SetExecutiveWidget(&widget);

			//loop till widget is closed
			while(widget.IsVisible())
			{
				//make sure widget is "executive"
				if(m_pExecutiveWidget != &widget)
					SetExecutiveWidget(&widget);
				//handle windows messages
				MSG msg = { 0 };
				if(PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);	
				}
				//send update command
				SigMessageLoop();
				Update();
				if(m_pExecutiveWidget)
					m_pExecutiveWidget->OnExecute();
				SigMessageLoopEnd();
			}

			SetExecutiveWidget(nullptr);
		}

		void Module::AddDirtyRect(const math::Rect& rDirty)
		{
			const int x = min(m_rDirty.x, max(0, rDirty.x));
			const int y = min(m_rDirty.y, max(0, rDirty.y));

			const int width = min(m_vScreenSize.x, max(m_rDirty.x + m_rDirty.width, rDirty.x + rDirty.width)) - x;
			const int height = min(m_vScreenSize.y, max(m_rDirty.y + m_rDirty.height, rDirty.y + rDirty.height)) - y;

			m_rDirty = math::Rect(x, y, width, height);
		}

		void Module::ResetDirtyRect(void)
		{
			m_rDirty = math::Rect(m_vScreenSize.x, m_vScreenSize.y, -m_vScreenSize.x, -m_vScreenSize.y);
		}

		math::Rect Module::CalculateTextRect(const std::wstring& stText) const
		{
			auto pFont = m_pFonts->Get(L"Arial16");
			if(pFont && !stText.empty())
			{
				auto& fontFile = pFont->GetFontFile();
				const unsigned int width = fontFile.CalculateWidth(stText);
				return math::Rect(0, 0, width, fontFile.GetFontHeight());
			}
			else
				return math::Rect(0, 0, 0, 0);
		}

		const math::Rect& Module::GetDirtyRect(void) const
		{
			return m_rDirty;
		}

		bool Module::PostCloseMessage(void)
		{
			//if we have an executing widget, close it
			if (m_pExecutiveWidget)
			{
				m_pExecutiveWidget->OnClose();
				return false;
			}
			else //otherwise, close main frame...
			{
				WidgetVector::const_iterator ii = m_vWidgets.cbegin();
				//...if there is any
				if(ii != m_vWidgets.cend())
					(*ii)->OnClose();
				return true;
			}
		}

		Widget& Module::GetMainWidget(void)
		{
			return m_mainWidget;
		}

		const Widget& Module::GetMainWidget(void) const
		{
			return m_mainWidget;
		}

		void Module::SetScreenSize(const math::Vector2& vScreenSize)
		{
			if(m_vScreenSize != vScreenSize)
			{
				m_rDirty = math::Rect(0, 0, m_vScreenSize.x, m_vScreenSize.y);
				m_vScreenSize = vScreenSize;
			}
		}

		const math::Vector2& Module::GetScreenSize(void) const
		{
			return m_vScreenSize;
		}

		void Module::RegisterShortcut(const Shortcut& shortcut, Widget& widget)
		{
			m_pShortcuts->RegisterShortcut(shortcut, widget);
		}

		void Module::UnregisterShortcut(const Shortcut& shortcut)
		{
			m_pShortcuts->UnregisterShortcut(shortcut);
		}
	}
}