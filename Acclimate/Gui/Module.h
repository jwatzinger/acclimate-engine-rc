#pragma once
#include <vector>
#include <set>
#include "Cursor.h"
#include "Layout.h"
#include "Widget.h"
#include "Shortcut.h"
#include "..\Core\Dll.h"
#include "..\Core\Signal.h"
#include "..\Gfx\Fonts.h"
#include "..\System\Clipboard.h"

namespace acl
{
    namespace math
    {
        struct Vector2;
    }

	namespace gui
	{

        class BaseController;
		class ShortcutRegistry;

		class ACCLIMATE_API Module
		{
		public:

			typedef std::vector<Widget*> WidgetVector;
            typedef std::vector<BaseController*> ControllerVector;
			typedef std::set<ComboKeys> ComboKeySet;

			Module(ShortcutRegistry& shortcuts, const gfx::Fonts& fonts);
			~Module(void);

			//register
			void RegisterWidget(Widget& widget);
			void UnregisterWidget(Widget& widget);
            void RegisterController(BaseController& controller);
            void UnregisterController(BaseController& controller);
			bool HasRegistered(Widget& widget) const;

			//update
			void Update(void);
			void OnKeyStroke(wchar_t key);
			void SetComboKeyState(ComboKeys keys, bool bDown);
			bool ComboKeysActive(ComboKeys keys);
			void OnShortcut(unsigned int key);

			//message box looping
			void ExecuteWidget(Widget& widget);

			// dirty rect
			void AddDirtyRect(const math::Rect& rDirty);
			void ResetDirtyRect(void);
			const math::Rect& GetDirtyRect(void) const;

			//widget handling
			void SetActiveWidget(Widget* pWidget);
			void SetFocusWidget(Widget* pWidget);
            void SetFocusController(BaseController* pController);
			void SetAsFree(Widget& widget);
			Widget* GetActiveWidget(void);
			Widget* GetFocusWidget(void);
			Widget* GetExecutiveWidget(void);
            BaseController* GetFocusController(void);
			Widget& GetMainWidget(void);
			const Widget& GetMainWidget(void) const;
			Widget* GetWidget(const math::Vector2& vMousePos, bool bIgnoreActive=false);

			//messaging
			bool PostCloseMessage(void);

			//signals
			core::Signal<> SigMessageLoop;
			core::Signal<> SigMessageLoopEnd;

			//screen size
			void SetScreenSize(const math::Vector2& vScreenSize);
			const math::Vector2& GetScreenSize(void) const;

			// text
			math::Rect CalculateTextRect(const std::wstring& stText) const;

			// shortcut
			void RegisterShortcut(const Shortcut& shortcut, Widget& widget);
			void UnregisterShortcut(const Shortcut& shortcut);

			Cursor m_cursor;
			Layout m_layout;
			size_t m_fontSize;
			sys::Clipboard m_clipboard;

		private:

#pragma warning( disable: 4251 )

			Widget* PickWidget(const math::Vector2& vMousePos, Widget* pParent);

			void SetExecutiveWidget(Widget* pWidget);

			math::Vector2 m_vScreenSize;
			math::Rect m_rDirty;
			Widget* m_pActiveWidget;
			Widget* m_pExecutiveWidget;
			Widget* m_pFocusWidget;
			Widget m_mainWidget;

			const gfx::Fonts* m_pFonts;

            BaseController* m_pFocusController;
			WidgetVector m_vWidgets;
            ControllerVector m_vController;

			ShortcutRegistry* m_pShortcuts;
			ComboKeySet m_comboKeys;

#pragma warning( default: 4251 )
		};
	
	}
}