#include "MsgBox.h"
#include "Button.h"
#include "Label.h"
#include "Icon.h"
#include "Module.h"
#include "Layout.h"

namespace acl
{
	namespace gui
	{

		const float buttonHeight = 0.022222f;

		MsgBox::MsgBox(LPCWSTR lpName, LPCWSTR lpText, IconType type, const CustomButtonVector* pCustomVector) : 
			BaseWindow(0.5f - 0.075f, 0.5f - 0.08f, 0.075f, 0, lpName), m_pLabel(nullptr), m_pIcon(nullptr)
		{	
			if(!pCustomVector)
			{
				//ok button
				Button* pButton = new Button(0.0f, 0.9f, 0.044791f, buttonHeight, L"OK");
				pButton->SetCenter(HorizontalAlign::CENTER, VerticalAlign::BOTTOM);
				pButton->SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::REF, PositionMode::REF);
				AddChild(*pButton);
				m_vButtons.emplace_back(*pButton, pButton->SigReleased, 0);
				m_vButtons[0].SigAccess.Connect(this, &MsgBox::OnButtonPressed);

				//cancel button
				pButton = new Button(0.0f, 0.9f, 0.044791f, buttonHeight, L"Cancel");
				pButton->SetCenter(HorizontalAlign::CENTER, VerticalAlign::BOTTOM);
				pButton->SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::REF, PositionMode::REF);
				AddChild(*pButton);
				m_vButtons.emplace_back(*pButton, pButton->SigReleased, 1);
				m_vButtons[1].SigAccess.Connect(this, &MsgBox::OnButtonPressed);
			}
			else
			{
				unsigned int i = 0;
				for(auto& stButton : *pCustomVector)
				{
					Button* pButton = new Button(0.0f, 0.9f, 0.044791f, buttonHeight, stButton);
					pButton->SetCenter(HorizontalAlign::CENTER, VerticalAlign::BOTTOM);
					pButton->SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::REF, PositionMode::REF);
					AddChild(*pButton);
					m_vButtons.emplace_back(*pButton, pButton->SigReleased, i);
					m_vButtons[i].SigAccess.Connect(this, &MsgBox::OnButtonPressed);
					i++;
				}
			}

			// icon
			if(type != IconType::NONE)
			{
				math::Rect rect;
				switch(type)
				{
				case IconType::INFO:
					rect = math::Rect(105, 24, 32, 32);
					break;
				case IconType::ALERT:
					rect = math::Rect(137, 24, 32, 32);
					break;
				case IconType::WARNING:
					rect = math::Rect(105, 56, 32, 32);
					break;
				case IconType::QUESTION:
					rect = math::Rect(137, 56, 32, 32);
					break;
				}
				m_pIcon = new Icon(40.0f, 0.325f, 32, L"", rect);
				m_pIcon->SetCenter(HorizontalAlign::CENTER, VerticalAlign::CENTER);
				m_pIcon->SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::ABS, PositionMode::ABS);
				AddChild(*m_pIcon);
			}

			//text
			if(lpText && wcslen(lpText) != 0)
			{
				m_pLabel = new Label(0.0f, 0.1f, 1.0f, 0.65f, lpText);
				if(m_pIcon)
					m_pLabel->SetPadding(56, 0, 56, 0);
				Widget::AddChild(*m_pLabel);
			}

			//setup
			m_cStyleFlags |= NoResize;
			m_buttonId = 0;
			OnInvisible();
			SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::ABS, PositionMode::ABS);
		}

		MsgBox::~MsgBox(void)
		{
			for(auto& container : m_vButtons)
			{
				container.SetWidget(nullptr);
				delete container.GetWidget();
			}

			// since RemoveChild is overloaded for BaseWindow, but label is added as self child
			Widget::RemoveChild(*m_pLabel);
			delete m_pLabel;

			delete m_pIcon;
		}

		int MsgBox::Execute(void)
		{
			BaseWindow::Execute();
			return m_buttonId;
		}

		void MsgBox::OnButtonPressed(unsigned int id)
		{
			OnClose();
			m_buttonId = id;
		}

		void MsgBox::OnLayoutChanged(const Layout& layout)
		{
			BaseWindow::OnLayoutChanged(layout);

			const math::Vector2 vLayout = layout.GetReferenceSize();

			const float buttonWidth = 0.04895f * vLayout.x;
			m_width += 32;
			// adjust width for icon
			if(m_pIcon)
				m_width += 56;
			SetWidth(max((float)((int)(buttonWidth)*m_vButtons.size()), m_width));

			int leftPos = (int)(0.5f * m_width);
			leftPos -= (int)(buttonWidth*(m_vButtons.size() - 1)*0.5f);

			for(auto& container : m_vButtons)
			{
				container->SetX((float)leftPos);
				leftPos += (int)buttonWidth;
			}

			m_width += m_leftBorder + m_rightBorder;

			// add window height based on buttons & offset
			m_height += (buttonHeight * vLayout.y)*2;
		}

		void MsgBox::OnRegister(Module& module)
		{
			// basic window height;
			SetHeight(32);

			auto& rect = module.CalculateTextRect(m_pLabel->GetString());
			SetWidth((float)(rect.width));
			// add window height based on string + offset
			m_height += max(64, rect.height+16);

			Widget::OnRegister(module);
		}

	}
}