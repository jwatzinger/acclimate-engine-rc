#pragma once
#include "Basewindow.h"
#include "DataContainer.h"
#include "Button.h"

namespace acl
{
	namespace gui
	{

		class Button;
		class Label;
		class Icon;

		enum class IconType
		{
			NONE, INFO, ALERT, WARNING, QUESTION
		};

		class ACCLIMATE_API MsgBox :
			public BaseWindow
		{
			typedef DataContainer<unsigned int, Button> ButtonContainer;
			typedef std::vector<ButtonContainer> ButtonVector;
		public:

			typedef std::vector<std::wstring> CustomButtonVector;
			MsgBox(LPCWSTR lpName, LPCWSTR lpText, IconType type = IconType::NONE, const CustomButtonVector* pCustomVector = nullptr);
			~MsgBox(void);

			int Execute(void) override;

			void OnButtonPressed(unsigned int id);
			void OnLayoutChanged(const Layout& layout) override;
			void OnRegister(Module& module) override;

		private:
#pragma warning( disable: 4251 )

			int m_buttonId;

			Label* m_pLabel;
			Icon* m_pIcon;
			ButtonVector m_vButtons;
#pragma warning( default: 4251 )
		};

	}
}

