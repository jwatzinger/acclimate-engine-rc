#include "MultiTable.h"
#include "Table.h"
#include "Module.h"
#include "Layout.h"

namespace acl
{
	namespace gui
	{
		MultiTable::MultiTable(float x, float y, float width, float height, float itemHeight) : Widget(x, y, width, height),
			m_itemHeight(itemHeight), m_area(0.0f, 0.0f, 1.0f, 1.0f, 0, 1)
		{
			AddChild(m_area);
		}

		MultiTable::~MultiTable(void)
		{
			for(auto pTable : m_vTables)
			{
				delete pTable;
			}
		}

		Table& MultiTable::AddTable(const std::wstring& stName)
		{
			const size_t numTables = m_vTables.size();
			Item* pItem;
			if(numTables)
				pItem = new Item(1.0f, m_itemHeight, stName);
			else
				pItem = new Item(0.0f, m_itemHeight, stName);
			
			if(numTables)
				m_vTables[numTables-1]->AddChild(*pItem);
			else
				m_area.AddChild(*pItem);
			m_vTables.push_back(pItem);

			int height = 0;
			for(auto pItem : m_vTables)
			{
				height += pItem->GetHeight() * (pItem->GetTable().GetNumItems()+1);
			}

			if(m_area.GetHeight() > height)
				m_area.SetScrollbar(false);
			else
			{
				m_area.SetScrollbar(true);
				m_area.SetAreaHeight(height);
			}

			return pItem->GetTable();
		}

		void MultiTable::Clear(void)
		{
			for(auto pTable : m_vTables)
			{
				delete pTable;
			}
			m_vTables.clear();
		}

		MultiTable::Item::Item(float y, float itemHeight, const std::wstring& stName) : Widget(0.0f, y, 1.0f, itemHeight),
			m_stName(stName), m_table(0.0f, itemHeight, 1.0f, 0.0f, itemHeight),
			m_label(0.0f, 0.0f, 1.0f, itemHeight, stName), m_icon(9.0f, 9.0f, 9.0f, L"", math::Rect(96, 40, 9, 9))
		{
			SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::REL, PositionMode::REF);

			m_table.SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
			m_table.SigResize.Connect(this, &MultiTable::Item::OnResize);
			m_table.SetPadding(15, 0, 15, 0);

			m_label.SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::REL, PositionMode::REF);
			m_label.SetAlign(LEFT | VCENTER);
			m_label.SetPadding(8+15, 0, 16+15, 0);

			m_icon.SigClicked.Connect(this, &MultiTable::Item::OnToggle);
			m_icon.SetCapMetrics(0, 2560, 0, 1600);
			m_icon.SetCenter(HorizontalAlign::CENTER, VerticalAlign::CENTER);
			m_icon.SetPositionModes(PositionMode::ABS, PositionMode::ABS, PositionMode::ABS, PositionMode::ABS);

			AddChild(m_table);
			AddChild(m_label);
			AddChild(m_icon);
		}

		Table& MultiTable::Item::GetTable(void)
		{
			return m_table;
		}

		void MultiTable::Item::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			const int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"TableItem");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, GetWidth(), GetHeight());
			m_vRenderData.push_back(Data);
		}

		void MultiTable::Item::OnResize(size_t size)
		{
			const float itemHeight = m_table.GetItemHeight();
			m_table.SetHeight((size + 1)*itemHeight);
			SetHeight((size+1)*itemHeight);
		}

		void MultiTable::Item::OnToggle(void)
		{
			if(m_table.IsVisible())
			{
				SetHeight(m_table.GetItemHeight());
				m_icon.Set(L"", math::Rect(96, 49, 9, 9));
			}
			else
			{
				SetHeight(m_table.GetItemHeight()*(m_table.GetNumItems() + 1));
				m_icon.Set(L"", math::Rect(96, 40, 9, 9));
			}

			m_table.SetVisible(!m_table.IsVisible());
		}

	}
}
