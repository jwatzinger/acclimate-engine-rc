#pragma once
#include "Widget.h"
#include "Label.h"
#include "Table.h"
#include "Icon.h"
#include "ScrollArea.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API MultiTable :
			public Widget
		{
			class Item;
			typedef std::vector<Item*> TableVector;
		public:
			MultiTable(float x, float y, float width, float height, float itemHeight);
			~MultiTable(void);

			Table& AddTable(const std::wstring& stName);

			void Clear(void);

		private:
#pragma warning( disable: 4251 )
			float m_itemHeight;
			ScrollArea m_area;
			TableVector m_vTables;

			class Item :
				public Widget
			{
			public:
				Item(float y, float itemHeight, const std::wstring& stName);

				Table& GetTable(void);

				void OnRedraw(void) override;

			private:

				void OnResize(size_t size);
				void OnToggle(void);

				std::wstring m_stName;

				Icon m_icon;
				Label m_label;
				Table m_table;
			};
#pragma warning( default: 4251 )
		};

	}
}

