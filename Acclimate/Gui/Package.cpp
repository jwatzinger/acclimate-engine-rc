#include "Package.h"
#include "Renderer.h"
#include "..\Gfx\Context.h"
#include "..\Gfx\Screen.h"
#include "..\Gui\WindowProcHook.h"

namespace acl
{
	namespace gui
	{

		Package::Package(const gfx::Fonts& fonts, const gfx::Textures& texture): m_module(m_shortcuts, fonts), m_layoutLoader(m_module, texture), 
			m_context(m_module, m_layoutLoader, m_shortcuts, m_handler), m_pRenderer(nullptr), m_handler(m_module)
		{
			WindowProcHook::SetModule(m_module);
		}

		Package::~Package(void)
		{
			delete m_pRenderer;
		}

		void Package::InitRenderer(const gfx::Context& context)
		{
			m_module.SetScreenSize(context.screen.GetSize());
			
			m_pRenderer = new Renderer(m_module, context.spriteBatch, context.resources.textures);

			m_context.pRenderer = m_pRenderer;
		}

		void Package::Update(void)
		{
			m_module.Update();
		}

		void Package::Render(void)
		{
			if(m_pRenderer)
				m_pRenderer->Draw();
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

	}
}
