#pragma once
#include "LayoutLoader.h"
#include "Context.h"
#include "Module.h"
#include "InputHandler.h"
#include "ShortcutRegistry.h"
#include "..\Core\Dll.h"
#include "..\Gfx\Fonts.h"
#include "..\Gfx\Textures.h"

namespace acl
{
	namespace gfx
	{
		struct Context;
	}

	namespace gui
	{
		class Renderer;

		class ACCLIMATE_API Package
		{
		public:
			Package(const gfx::Fonts& fonts, const gfx::Textures& textures);
			~Package(void);

			void InitRenderer(const gfx::Context& context);

			void Update(void);
			void Render(void);

			const Context& GetContext(void) const;

		private:

			ShortcutRegistry m_shortcuts;
			Module m_module;
			LayoutLoader m_layoutLoader;
			InputHandler m_handler;
			Renderer* m_pRenderer;
			Context m_context;
		};

	}
}
