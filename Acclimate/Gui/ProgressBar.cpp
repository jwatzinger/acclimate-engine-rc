#include "ProgressBar.h"

namespace acl
{
	namespace gui
	{

		ProgressBar::ProgressBar(float x, float y, float width, float height): Widget(x, y, width, height), m_progress(0.0f)
		{
		}

		void ProgressBar::SetProgress(float progress)
		{
			m_progress = progress;
		}

		void ProgressBar::OnRedraw(void)
		{
			m_vRenderData.clear();
	        RenderData Data;

	        //top
	        Data.rSrcRect.Set(9, 88, 6, 1);
		    Data.rDestRect.Set(1, 0, m_absWidth-2, 1);
	        m_vRenderData.push_back(Data);

			//bottom
	        Data.rSrcRect.Set(9, 95, 6, 1);
		    Data.rDestRect.Set(1, m_absHeight-1, m_absWidth-2, 1);
	        m_vRenderData.push_back(Data);

			//left
	        Data.rSrcRect.Set(8, 89, 1, 6);
		    Data.rDestRect.Set(0, 1, 1, m_absHeight-2);
	        m_vRenderData.push_back(Data);

			//right
	        Data.rSrcRect.Set(8, 95, 1, 6);
		    Data.rDestRect.Set(m_absWidth-1, 1, 1, m_absHeight-2);
	        m_vRenderData.push_back(Data);

			const int emptyPortion = (int)((m_absWidth-2)*(100.0f-m_progress)/100.0f);
			const int fullPortion = (int)((m_absWidth-2)*(m_progress/100.0f));

			//fill empty
	        Data.rSrcRect.Set(25, 41, 14, 14);
		    Data.rDestRect.Set(1+fullPortion, 1, emptyPortion, m_absHeight-2);
	        m_vRenderData.push_back(Data);

			//fill full
	        Data.rSrcRect.Set(9, 89, 6, 6);
		    Data.rDestRect.Set(1, 1, fullPortion, m_absHeight-2);
	        m_vRenderData.push_back(Data);
		}

	}
}
