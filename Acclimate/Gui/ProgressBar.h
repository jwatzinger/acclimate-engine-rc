#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class ProgressBar :
			public Widget
		{
		public:
			ProgressBar(float x, float y, float width, float height);

			void SetProgress(float percent);

			void OnRedraw(void) override;

		private:

			float m_progress;

		};

	}
}

