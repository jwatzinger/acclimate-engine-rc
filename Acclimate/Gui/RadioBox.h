#pragma once
#include "RadioButton.h"
#include "Label.h"

namespace acl
{
    namespace gui
    {

        template<typename Data>
        class GuiRadioBox :
	        public Widget
        {
			
	        typedef std::vector<RadioButton<Data>*> ButtonVector;
			typedef std::vector<Label*> LabelVector;

        public:
	        GuiRadioBox(float x, float y, float width, float height): Widget(x, y, width, height), m_numAddedButtons(0), m_ticked(0)
	        {
	        };

			~GuiRadioBox(void)
			{
				for(auto pRadio : m_vButtons)
				{
					delete pRadio;
				}

				for(auto pLabel : m_vLabels)
				{
					delete pLabel;
				}
			}

	        const Data& GetContent(void) const
	        {
		        return m_vButtons[m_ticked]->GetData();
	        }

	        void AddRadioField(const std::wstring& stName, Data data)
	        {
		        const float comboHeight = m_numAddedButtons*0.15f;

		        //button
		        RadioButton<Data>* pRadio = new RadioButton<Data>(0.0f, 0.1f + comboHeight, m_numAddedButtons, data);
		        if(m_numAddedButtons == 0)
			        pRadio->OnTick();
		        pRadio->SigTicked.Connect(this, &GuiRadioBox::OnButtonTick);
		        m_vButtons.push_back(pRadio);
		        AddChild(*pRadio);

		        //label
		        Label* pLabel = new Label(0.15f, 0.1f + comboHeight, 0.8f, 0.1f, stName.c_str());
		        pLabel->SetAlign(LEFT | VCENTER);
		        pLabel->SigClicked.Connect(pRadio, &RadioButton<Data>::OnTick);
				m_vLabels.push_back(pLabel);
		        AddChild(*pLabel);

		        m_numAddedButtons += 1;
	        }

			core::Signal<Data> SigTickedItem;

        private:

	        void OnButtonTick(int index)
	        {
		        m_ticked = index;
		        for(int i = 0; i < m_numAddedButtons; i++)
		        {
			        if(i == index)
				        continue;

			        m_vButtons[i]->OnUntick();
		        }

				SigTickedItem(m_vButtons[index]->GetData());
	        }

	        int m_numAddedButtons;
	        int m_ticked;

	        ButtonVector m_vButtons;
			LabelVector m_vLabels;

        };

    }
}

