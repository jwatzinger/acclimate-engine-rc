#pragma once
#include "BaseButton.h"

namespace acl
{
    namespace gui
    {

		template<typename Data>
        class RadioButton :
	        public Widget
        {
        public:
			RadioButton(float x, float y, int index, Data data) : Widget(x, y, 0.1f, 0.1f), m_index(index), m_bTicked(false),
				m_data(data)
			{
			}

			void OnRedraw(void) override
			{
				m_vRenderData.clear();
				RenderData data;
				data.rDestRect.Set(0, 0, GetWidth(), GetHeight());
				if(m_bTicked)
					data.rSrcRect.Set(88, 12, 12, 12);
				else
					data.rSrcRect.Set(88, 0, 12, 12);
				m_vRenderData.push_back(data);
			}

			void OnClick(MouseType mouse) override
			{
				OnTick();
			}

			void OnTick(void)
			{
				m_bTicked = true;
				SigTicked(m_index);
				m_bDirty = true;
			}

			void OnUntick(void)
			{
				m_bTicked = false;
				m_bDirty = true;
			}

			const Data& GetData(void) const
			{
				return m_data;
			}

	        core::Signal<int> SigTicked;

        private:

	        int m_index;
	        bool m_bTicked;
			Data m_data;
        };

    }
}
