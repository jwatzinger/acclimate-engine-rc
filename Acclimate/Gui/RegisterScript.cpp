#include "RegisterScript.h"
#include "Renderer.h"
#include "BaseController.h"
#include "SliderBar.h"
#include "Label.h"
#include "CheckBox.h"
#include "TextArea.h"
#include "..\Script\Core.h"
#include "..\Script\Function.h"

namespace acl
{
	namespace gui
	{
		namespace detail
		{
			Module* _pModule = nullptr;
			Renderer* _pRenderer = nullptr;
			script::Core* _pScript = nullptr;
		}

		BaseController* loadController(const std::wstring& stName)
		{
			return new BaseController(*detail::_pModule, stName);
		}

		void setCursorVisible(bool isVisible)
		{
			detail::_pModule->m_cursor.SetVisible(isVisible);
		}

		void unloadController(BaseController* pController)
		{
			delete pController;
		}
		
		void connectClicked(Widget* pWidget, asIScriptFunction* pFunction)
		{
			// TODO: memory-leak
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			pWidget->SigClicked.Connect(pFunc, &script::Function::Call<void>);
		}

		void connectMoveOn(Widget* pWidget, asIScriptFunction* pFunction)
		{
			// TODO: memory-leak
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			pWidget->SigMoveOn.Connect(pFunc, &script::Function::Call<void>);
		}

		void connectMoveOff(Widget* pWidget, asIScriptFunction* pFunction)
		{
			// TODO: memory-leak
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			pWidget->SigMoveOff.Connect(pFunc, &script::Function::Call<void>);
		}

		void connectValueChangedSlider(Slider* pSlider, asIScriptFunction* pFunction)
		{
			// TODO: memory-leak
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			pSlider->SigValueChanged.Connect(pFunc, &script::Function::CallValues<void, float>);
		}

		void connectValueChanged(SliderBar* pSliderBar, asIScriptFunction* pFunction)
		{
			connectValueChangedSlider(&pSliderBar->GetSlider(), pFunction);
		}

		void connectChecked(CheckBox* pBox, asIScriptFunction* pFunction)
		{
			// TODO: memory-leak
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			pBox->SigChecked.Connect(pFunc, &script::Function::CallValues<void, bool>);
		}

		float getSliderBarValue(const SliderBar* pSliderBar)
		{
			return pSliderBar->GetSlider().GetValue();
		}

		void setFontSize(unsigned int size)
		{
			detail::_pModule->m_fontSize = size;
		}

		void setFontName(const std::wstring& stName)
		{
			detail::_pRenderer->SetFontName(stName);
		}

		void RegisterScript(script::Core& script, Module& module, Renderer& renderer)
		{
			detail::_pScript = &script;
			detail::_pModule = &module;
			detail::_pRenderer = &renderer;

			//signals
			script.RegisterFuncdef("void OnClickCallback()");
			script.RegisterFuncdef("void OnMoveOnCallback()");
			script.RegisterFuncdef("void OnMoveOffCallback()");
			script.RegisterFuncdef("void OnValueChangedCallback(float)");
			script.RegisterFuncdef("void OnCheckedCallback(bool)");

			// widget
			auto widget = script.RegisterReferenceType("Widget", script::REF_NOCOUNT);
			widget.RegisterMethod("void ConnectClicked(OnClickCallback@)", asFUNCTION(connectClicked), asCALL_CDECL_OBJFIRST);
			widget.RegisterMethod("void ConnectMoveOn(OnMoveOnCallback@)", asFUNCTION(connectMoveOn), asCALL_CDECL_OBJFIRST);
			widget.RegisterMethod("void ConnectMoveOff(OnMoveOffCallback@)", asFUNCTION(connectMoveOff), asCALL_CDECL_OBJFIRST);
			widget.RegisterMethod("void SetVisible(bool)", asMETHOD(Widget, SetVisible));
			widget.RegisterMethod("void SetX(float)", asMETHOD(Widget, SetX));
			widget.RegisterMethod("void SetY(float)", asMETHOD(Widget, SetY));
			widget.RegisterMethod("int GetX() const", asMETHOD(Widget, GetX));
			widget.RegisterMethod("int GetY() const", asMETHOD(Widget, GetY));
			widget.RegisterMethod("int GetWidth() const", asMETHOD(Widget, GetWidth));
			widget.RegisterMethod("int GetHeight() const", asMETHOD(Widget, GetHeight));
			widget.RegisterMethod("float GetRelX() const", asMETHOD(Widget, GetRelX));
			widget.RegisterMethod("float GetRelY() const", asMETHOD(Widget, GetRelY));

			// sliderbar
			auto sliderBar = script.RegisterReferenceType("SliderBar", script::REF_NOCOUNT);
			sliderBar.RegisterMethod("void ConnectValueChanged(OnValueChangedCallback@)", asFUNCTION(connectValueChanged), asCALL_CDECL_OBJFIRST);
			sliderBar.RegisterMethod("float GetValue(void) const", asFUNCTION(getSliderBarValue), asCALL_CDECL_OBJFIRST);

			// label
			auto label = script.RegisterReferenceType("Label", script::REF_NOCOUNT);
			label.RegisterMethod("void SetString(const string& in)", asMETHOD(Label, SetString));
			label.RegisterMethod("void SetColor(const Color& in)", asMETHOD(Label, SetColor));
			
			// checkbox
			auto checkBox = script.RegisterReferenceType("CheckBox", script::REF_NOCOUNT);
			checkBox.RegisterMethod("void SetChecked(bool)", asMETHOD(CheckBox, OnCheck));
			checkBox.RegisterMethod("bool IsChecked(void) const", asMETHOD(CheckBox, IsChecked));
			checkBox.RegisterMethod("void ConnectChecked(OnCheckedCallback@)", asFUNCTION(connectChecked), asCALL_CDECL_OBJFIRST);

			// slider
			auto slider = script.RegisterReferenceType("Slider", script::REF_NOCOUNT);
			slider.RegisterMethod("void ConnectValueChanged(OnValueChangedCallback@)", asFUNCTION(connectValueChangedSlider), asCALL_CDECL_OBJFIRST);
			slider.RegisterMethod("void SetValue(float)", asMETHOD(Slider, SetValue));
			slider.RegisterMethod("float GetValue() const", asMETHOD(Slider, GetValue));

			// textarea
			auto textarea = script.RegisterReferenceType("TextArea", script::REF_NOCOUNT);
			textarea.RegisterMethod("void SetText(const string& in)", asMETHOD(TextArea, SetText));
			textarea.RegisterMethod("void SetVisible(bool)", asMETHOD(TextArea, SetVisible));

			// controller
			auto controller = script.RegisterReferenceType("GuiController", script::REF_NOCOUNT);
			controller.RegisterMethod("void Unload()", asFUNCTION(unloadController), asCALL_CDECL_OBJFIRST);
			controller.RegisterMethod("Widget@ GetWidget(const string& in) const", asMETHODPR(BaseController, GetWidgetByName, (const std::wstring& stName) const, Widget*));
			controller.RegisterMethod("SliderBar@ GetSliderBar(const string& in) const", asMETHODPR(BaseController, GetWidgetByName<SliderBar>, (const std::wstring& stName) const, SliderBar*));
			controller.RegisterMethod("Slider@ GetSlider(const string& in) const", asMETHODPR(BaseController, GetWidgetByName<Slider>, (const std::wstring& stName) const, Slider*));
			controller.RegisterMethod("Label@ GetLabel(const string& in) const", asMETHODPR(BaseController, GetWidgetByName<Label>, (const std::wstring& stName) const, Label*));
			controller.RegisterMethod("CheckBox@ GetCheckBox(const string& in) const", asMETHODPR(BaseController, GetWidgetByName<CheckBox>, (const std::wstring& stName) const, CheckBox*));
			controller.RegisterMethod("TextArea@ GetTextArea(const string& in) const", asMETHODPR(BaseController, GetWidgetByName<TextArea>, (const std::wstring& stName) const, TextArea*));
			
			script.RegisterGlobalFunction("GuiController@ loadController(const string& in)", asFUNCTION(loadController));
			script.RegisterGlobalFunction("void setCursorVisible(bool)", asFUNCTION(setCursorVisible));
			script.RegisterGlobalFunction("void setFontSize(uint)", asFUNCTION(setFontSize));
			script.RegisterGlobalFunction("void setFontName(const string& in)", asFUNCTION(setFontName));
		}

		void setScriptRenderer(Renderer& renderer)
		{
			detail::_pRenderer = &renderer;
		}

	}
}
