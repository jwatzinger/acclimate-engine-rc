#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace gui
	{

		class Module;
		class Renderer;

		void RegisterScript(script::Core& core, Module& module, Renderer& renderer);
		ACCLIMATE_API void setScriptRenderer(Renderer& renderer);

	}
}