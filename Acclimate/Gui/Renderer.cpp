#include "Renderer.h"
#include "Module.h"
#include "Widget.h"
#include "Layout.h"
#include "Cursor.h"
#include "..\Gfx\ISpriteBatch.h"
#include "..\Gfx\IFontLoader.h"
#include "..\Render\IStage.h"
#include "..\System\Convert.h"

namespace acl
{
    namespace gui
    {

		Renderer::Renderer(Module& module, gfx::ISpriteBatch& sprite, const gfx::Textures& textures) : m_pStage(nullptr),
			m_pTextures(&textures), m_fontSize(0), m_pTexture(nullptr), m_pModule(&module), m_pSprite(&sprite), m_pEffect(nullptr),
			m_stFontname(L"Arial")
        {
        }

		void Renderer::SetEffect(const gfx::IEffect& effect)
		{
			m_pEffect = &effect;
		}

		void Renderer::SetStage(render::IStage& stage)
		{
			m_pStage = &stage;
		}

		void Renderer::SetTextSize(size_t size)
		{
			m_fontSize = size;
		}

		void Renderer::SetFontName(const std::wstring& stFontname)
		{
			m_stFontname = stFontname;
		}

        void Renderer::Draw(void)
        {
			if(!m_pEffect)
				return;

			// update font
			SetTextSize(m_pModule->m_fontSize);

            // get layout texture
			m_pTexture = m_pTextures->Get(m_pModule->m_layout.GetLayout());

	        // create initial rectangle for clipping
			const math::Rect& r = m_pModule->GetDirtyRect();
			//const math::Rect& r = math::Rect(0, 0, m_pModule->GetScreenSize().x, m_pModule->GetScreenSize().y);

			if(r.width <= 0 || r.height <= 0)
				return;

            // draw all main widgets

			m_pSprite->SetFont(m_stFontname, m_fontSize);
			m_pSprite->SetEffect(*m_pEffect);
			m_pSprite->Begin(*m_pStage, gfx::Sorting::NONE);
			DrawObject(m_pModule->GetMainWidget(), r);
			m_pSprite->End();

			m_pModule->ResetDirtyRect();
        }
	
        void Renderer::DrawObject(const Widget& widget, const math::Rect& parentRect)
        {
	        //don't draw if invisible
	        if(!widget.IsVisible())
		        return;
	        //aquire render data
	        const std::vector<RenderData>& vData =  widget.GetRenderData();

	        //draw widgets subparts
	        const int x = widget.GetX(), y = widget.GetY();
	        math::Rect r(parentRect);
            const math::Rect childRect(x, y, widget.GetWidth(), widget.GetHeight());

			const bool bInside = parentRect.Inside(childRect);
			if(widget.IsClipping())
			{
				if(bInside)
					r = parentRect.Merge(childRect);
				else
					return;
			}

			if(bInside)
			{
				m_pSprite->SetClipRect(&r);

				for(auto& data : vData)
				{
					//check for label
					math::Rect rDest(x + data.rDestRect.x, y + data.rDestRect.y, data.rDestRect.width, data.rDestRect.height);
					if (!r.Inside(rDest))
						continue;

					if(data.stLabel.empty())
					{
						math::Rect srcRect = data.rSrcRect;
						//check for texture
						if(!data.pTexture)
						{
							//check for filename
							if(data.stFileName.empty())
								//draw from layout
								m_pSprite->SetTexture(m_pTexture);
							else
							{
								//draw from file
								gfx::ITexture* pTexture = m_pTextures->Get(data.stFileName);
								if(!data.bCrop && pTexture)
								{
									const math::Vector2& vSize = pTexture->GetSize();
									srcRect.Set(0, 0, vSize.x, vSize.y);
								}
								m_pSprite->SetTexture(pTexture);
							}
						}
						else
							//draw from texture
							m_pSprite->SetTexture(data.pTexture);

						//setup sprite
						m_pSprite->SetSrcRect(srcRect);
						m_pSprite->SetPosition(rDest.x, rDest.y);
						m_pSprite->SetSize(rDest.width, rDest.height);
						//draw
						m_pSprite->Draw();
					}
					else
					{
						//draw label
						m_pSprite->SetPosition(rDest.x, rDest.y);
						m_pSprite->SetSize(data.rDestRect.width, data.rDestRect.height);

						if(data.textSize != 0)
							m_pSprite->SetFont(m_stFontname, data.textSize);
						m_pSprite->Draw(data.stLabel, data.labelFlags, data.labelColor);
						if(data.textSize != 0)
							m_pSprite->SetFont(m_stFontname, m_fontSize);
					}
				}
			}

	        //draw all children
	        const Module::WidgetVector& vChildren = widget.GetChildren();
	        for(auto pWidget: vChildren)
	        {
		        //draw child
		        DrawObject(*pWidget, r);
	        }
        }

    }
}