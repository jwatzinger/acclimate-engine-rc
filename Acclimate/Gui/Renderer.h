#pragma once
#include <vector>
#include "..\Core\Dll.h"
#include "..\Gfx\Textures.h"

namespace acl
{

	namespace gfx
	{
		class ITexture;
		class ISpriteBatch;
		class IEffect;
	}

    namespace math
    {
        struct Rect;
    }

	namespace render
	{
		class IStage;
	}

    namespace gui
    {

        class Widget;
		class Module;

        class ACCLIMATE_API Renderer
        {
        public:
	        Renderer(Module& module, gfx::ISpriteBatch& sprite, const gfx::Textures& textures);

	        void Draw(void);

			void SetTextSize(size_t size);
			void SetFontName(const std::wstring& stName);
			void SetEffect(const gfx::IEffect& effect);
			void SetStage(render::IStage& stage);

        private:
#pragma warning( disable: 4251 )
	        void DrawObject(const Widget& widget, const math::Rect& parentRect);

			size_t m_fontSize;
			std::wstring m_stFontname;

			Module* m_pModule;

			gfx::ISpriteBatch* m_pSprite;
	        gfx::ITexture* m_pTexture;
			const gfx::IEffect* m_pEffect;
            const gfx::Textures* m_pTextures;

	        render::IStage* m_pStage;

#pragma warning( default: 4251 )
        };

    }
}

