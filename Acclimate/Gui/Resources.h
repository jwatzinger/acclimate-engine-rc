#pragma once
#include <string>
#include "..\XML\Doc.h"
#include "..\Core\Resources.h"

namespace acl
{
    namespace gui
    {

        typedef core::Resources<std::wstring, xml::Doc> Resources;

    }
}