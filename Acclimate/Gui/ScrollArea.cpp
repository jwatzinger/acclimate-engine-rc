#include "ScrollArea.h"
#include "Scrollbar.h"

namespace acl
{
    namespace gui
    {

        ScrollArea::ScrollArea(float x, float y, float width, float height, int areaWidth, int areaHeight): Widget(x, y, width, height), m_pRight(nullptr),
			m_areaWidth(areaWidth), m_areaHeight(areaHeight), m_pDown(nullptr), m_area(0.0f, 0.0f, 1.0f, 1.0f)
        {
			const bool bHasRightBar = areaHeight != 0, bHasBottomBar = areaWidth != 0;
			m_bClipping = true;
	        //add area
			m_area.SetPadding(0, 0, 16 * (int)(bHasRightBar), 16 * (int)(bHasBottomBar));
            this->SigWheelMoved.Connect(&m_area, &Area::OnWheelMove);
	        Widget::AddChild(m_area);

	        //right scrollbar
			if(bHasRightBar)
	        {
		        m_pRight = new Scrollbar(0.0f, 0.0f, 1.0f, 0.0f, (float)areaHeight, 1.0f, true); 
                m_pRight->SetAlignement(HorizontalAlign::RIGHT, VerticalAlign::TOP);
                m_pRight->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
				if(bHasBottomBar)
					m_pRight->SetPadding(0, 0, 0, 32);
				else
					m_pRight->SetPadding(0, 0, 0, 16);
		        m_pRight->SigValueChanged.Connect(this, &ScrollArea::OnChangeOffsetY);
		        SigResizeSliderV.Connect(m_pRight, &Scrollbar::SetSliderValue);
		        m_area.SigWheelMoved.Connect(m_pRight, &Scrollbar::OnWheelMove);
		        Widget::AddChild(*m_pRight);
	        }
	        //down scrollbar
			if(bHasBottomBar)
	        {
		        m_pDown = new Scrollbar(0.0f, 1.0f, 1.0f, 0.0f, (float)areaWidth, height, false);
				m_pDown->SetCenter(HorizontalAlign::LEFT, VerticalAlign::BOTTOM);
				if(bHasRightBar)
					m_pDown->SetPadding(0, 0, 32, 0);
				else
					m_pDown->SetPadding(0, 0, 16, 0);
		        m_pDown->SigValueChanged.Connect(this, &ScrollArea::OnChangeOffsetX);
		        SigResizeSliderH.Connect(m_pDown, &Scrollbar::SetSliderValue);
		        Widget::AddChild(*m_pDown);
	        }
	        //setup
	        OnRedraw();
        }

		ScrollArea::~ScrollArea(void)
		{
			Widget::RemoveChild(*m_pRight);
			delete m_pRight;
			Widget::RemoveChild(*m_pDown);
			delete m_pDown;
		}

		void ScrollArea::SetOffsetX(int offX)
		{
			if(m_pDown)
			{
				const int possibleWidth = m_areaWidth - m_area.GetWidth();
				const float relation = offX / (float)possibleWidth;
				m_pDown->SetValue(relation * m_areaWidth);
			}
			m_area.ChangeOffsetX((float)offX);
		}

		void ScrollArea::SetOffsetY(int offY)
		{
			if(m_pRight)
			{
 				const int possibleHeight = m_areaHeight - m_area.GetHeight();
				const float relation = offY / (float)possibleHeight;
				m_pRight->SetValue(relation * m_areaHeight);
			}
			m_area.ChangeOffsetY((float)offY);
		}

        void ScrollArea::SetAreaWidth(int newWidth)
        {
			m_areaWidth = newWidth;
	        if(m_pDown)
		        m_pDown->SetRange(0.0f, (float)newWidth);
        }

        void ScrollArea::SetAreaHeight(int newHeight)
        {
			m_areaHeight = newHeight;
	        if(m_pRight)
		        m_pRight->SetRange(0.0f, (float)newHeight);
        }

		void ScrollArea::SetScrollbar(bool hasScrollbar)
		{
			if(hasScrollbar)
			{
				m_pRight->OnVisible();
				m_pDown->OnInvisible();
				m_area.SetPadding(0, 0, 16 * (int)(m_pRight != nullptr), 16 * (int)(m_pDown != nullptr));
			}
			else
			{
				m_pRight->OnInvisible();
				m_pDown->OnInvisible();
				m_area.SetPadding(0, 0, 0, 0);
			}
		}

		void ScrollArea::SetContextMenu(ContextMenu* pMenu)
		{
			m_area.SetContextMenu(pMenu);
		}

        int ScrollArea::GetOffsetX(void) const
        {
            return m_area.GetOffsetX();
        }

        int ScrollArea::GetOffsetY(void) const
        {
            return m_area.GetOffsetY();
        }

        void ScrollArea::AddChild(Widget& child)
        {
	        m_area.AddChild(child);
			if(m_pRight)
				child.SigWheelMoved.Connect(m_pRight, &Scrollbar::OnWheelMove);
        }

        void ScrollArea::RemoveChild(Widget& child)
        {
	        m_area.RemoveChild(child);
			if(m_pRight)
				child.SigWheelMoved.Disconnect(m_pRight, &Scrollbar::OnWheelMove);
        }

        void ScrollArea::OnParentResize(int x, int y, int width, int height)
        {
	        Widget::OnParentResize(x, y, width, height);
			SigResizeSliderH((float)GetWidth()-16);
	        SigResizeSliderV((float)GetHeight()-16);
        }

		void ScrollArea::OnChangeOffsetX(float x)
		{
			if(m_areaWidth > 0)
			{
				const float relation = x / m_areaWidth;
				m_area.ChangeOffsetX(relation * (m_areaWidth - (GetWidth() - 16)));
			}
			else
				m_area.ChangeOffsetX(0);

			m_bDirty = true;
		}

		void ScrollArea::OnChangeOffsetY(float y)
		{
			if(m_areaHeight > 0)
			{
				const float relation = y / m_areaHeight;
				m_area.ChangeOffsetY(relation * (m_areaHeight - (GetHeight() - 16)));
			}
			else
				m_area.ChangeOffsetY(0);

			m_bDirty = true;
		}

    }
}