#pragma once
#include "Area.h"

namespace acl
{
    namespace gui
    {
		class Scrollbar;

        class ACCLIMATE_API ScrollArea :
	        public Widget
        {
        public:
	        ScrollArea(float x, float y, float width, float height, int areaWidth, int areaHeight);
			~ScrollArea(void);

	        void SetAreaWidth(int newWidth);
	        void SetAreaHeight(int newHeight);
			void SetScrollbar(bool hasScrollbar);
			void SetContextMenu(ContextMenu* pMenu) override;

			void SetOffsetX(int offX);
			void SetOffsetY(int offY);
            int GetOffsetX(void) const;
            int GetOffsetY(void) const;

	        void AddChild(Widget& child) override;
	        void RemoveChild(Widget& child) override;

	        void OnParentResize(int x, int y, int width, int height) override;

        private:
#pragma warning( disable: 4251 )
			void OnChangeOffsetX(float x);
			void OnChangeOffsetY(float y);

	        core::Signal<float> SigResizeSliderV;
	        core::Signal<float> SigResizeSliderH;

			int m_areaWidth, m_areaHeight;

	        Area m_area;
	        Scrollbar* m_pDown;
	        Scrollbar* m_pRight;
#pragma warning( default: 4251 )
        };

    }
}