#include "Scrollbar.h"
#include "ToolButton.h"
#include "Icon.h"
#include "Module.h"

namespace acl
{
    namespace gui
    {

        Scrollbar::Scrollbar(float x, float y, float length, float min, float max, float sliderValue, bool bVertical) : Widget(x, y, 0.0f, 0.0f), m_bVertical(bVertical),
			m_slider(0.0f, 0.0f, length, 1.0f, min, max, sliderValue, bVertical)
        {
            /***************************************
            * Slider
            ***************************************/
			m_slider.SigValueChanged.Connect(this, &Scrollbar::OnSliderValueChange);
            AddChild(m_slider);

            /****************************************
            * Lower button
            ****************************************/

            math::Rect r;
            if(bVertical)
            {
                m_height = length;

                //create button for vertical scrollbar
		        Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", math::Rect(40, 40, 16, 16));
		        m_pLowerButton = new ToolButton(0.0f, length, pIcon);

                r = math::Rect(40, 24, 16, 16);

                // set slider padding
                m_slider.SetPadding(0, 16, 0, 32);
            }
            else
            {
                m_width = length;

                //create button for horizontal scrollbar
		        Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", math::Rect(40, 72, 16, 16));
		        m_pLowerButton = new ToolButton(length, 0.0f, pIcon);

                r = math::Rect(40, 56, 16, 16);

                // set slider padding
                m_slider.SetPadding(16, 0, 32, 0);
            }

			m_pLowerButton->SigRepeat.Connect(this, &Scrollbar::OnMoveSliderDown);
            AddChild(*m_pLowerButton);

            /****************************************
            * Upper button
            ****************************************/

	        Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", r);
	        m_pUpperButton = new ToolButton(0.0f, 0.0f, pIcon);
			m_pUpperButton->SigRepeat.Connect(this, &Scrollbar::OnMoveSliderUp);
	        AddChild(*m_pUpperButton);

			m_slider.SigInvisible.Connect(m_pUpperButton, &ToolButton::OnDisable);
			m_slider.SigInvisible.Connect(m_pLowerButton, &ToolButton::OnDisable);
			m_slider.SigVisible.Connect(m_pUpperButton, &ToolButton::OnEnable);
			m_slider.SigVisible.Connect(m_pLowerButton, &ToolButton::OnEnable);
        }

		Scrollbar::~Scrollbar(void)
		{
			delete m_pUpperButton;
			delete m_pLowerButton;
		}

		void Scrollbar::SetValue(float value)
		{
			m_slider.SetValue(value);
		}

        void Scrollbar::SetSliderValue(float sliderValue)
        {
            m_slider.SetSliderValue(sliderValue);
        }

        void Scrollbar::SetRange(float min, float max)
        {
			m_slider.SetRange(min, max);
        }

        void Scrollbar::OnParentResize(int x, int y, int width, int height)
        {
            Widget::OnParentResize(x, y, width, height);

            if(m_bVertical)
                m_absWidth = 16;
            else
                m_absHeight = 16;
        }

        void Scrollbar::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData Data;

	        //Bar
	        Data.rSrcRect = m_pModule->m_layout.GetElement(L"Scrollbar")->GetPart(L"Main");
	        if(m_bVertical)
		        Data.rDestRect.Set(0, 16, 16, GetHeight()-16);
	        else
		        Data.rDestRect.Set(16, 0, GetWidth()-16, 16);
	        m_vRenderData.push_back(Data);
        }

		void Scrollbar::OnWheelMove(int distance)
		{
			Widget::OnWheelMove(distance);
			m_slider.OnWheelMove(distance);
		}

		void Scrollbar::OnSliderValueChange(float sliderValue) const
		{
			SigValueChanged(sliderValue);
		}

		void Scrollbar::OnMoveSliderUp(void)
		{
			if(m_bVertical)
				m_slider.OnDrag(math::Vector2(0, 2));
			else
				m_slider.OnDrag(math::Vector2(2, 0));
		}

		void Scrollbar::OnMoveSliderDown(void)
		{
			if(m_bVertical)
				m_slider.OnDrag(math::Vector2(0, -2));
			else
				m_slider.OnDrag(math::Vector2(-2, 0));

		}

    }
}
