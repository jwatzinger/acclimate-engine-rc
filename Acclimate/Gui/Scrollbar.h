#pragma once
#include "Widget.h"
#include "Slider.h"

namespace acl
{
	namespace gui
	{
        class Slider;
        class ToolButton;

		class Scrollbar :
			public Widget
		{
		public:
			Scrollbar(float x, float y, float length, float min, float max, float sliderValue, bool bVertical);
			~Scrollbar(void);

			void SetValue(float value);
            void SetSliderValue(float sliderValue);
            void SetRange(float min, float max);

            core::Signal<float> SigValueChanged;

            void OnParentResize(int x, int y, int width, int height) override;
            void OnRedraw(void) override;
			void OnWheelMove(int distance) override;

		private:

			void OnMoveSliderUp(void);
			void OnMoveSliderDown(void);

			void OnSliderValueChange(float sliderValue) const;

            bool m_bVertical;

            Slider m_slider;
            ToolButton* m_pUpperButton, * m_pLowerButton;

		};

	}
}

