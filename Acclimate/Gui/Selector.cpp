#include "Selector.h"

namespace acl
{
    namespace gui
    {

        Selector::Selector(float x, float y, float width, float height): Widget(x, y, width, height)
        {
        }

        void Selector::OnRedraw(void)
        {
            m_vRenderData.clear();
		    RenderData data;
		    data.rDestRect.Set(0, 0, GetWidth(), GetHeight());
		    data.rSrcRect.Set(0, 88, 8, 8);
		    m_vRenderData.push_back(data);
        }

    }
}
