#pragma once
#include "Widget.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace gui
    {

        class ACCLIMATE_API Selector :
            public Widget
        {
        public:
            Selector(float x, float y, float width, float height);

            void OnRedraw(void) override;

        };

    }
}

