#include "Shortcut.h"

namespace acl
{
	namespace gui
	{

		std::wstring ComboToString(ComboKeys keys)
		{
			switch(keys)
			{
			case ComboKeys::CTRL:
				return L"Ctrl";
			case ComboKeys::ALT:
				return L"Alt";
			case ComboKeys::SHIFT:
				return L"Shift";
			case ComboKeys::CTRL_ALT:
				return L"Ctrl+Alt";
			case ComboKeys::SHIFT_ALT:
				return L"Alt+Shift";
			case ComboKeys::SHIFT_CTRL:
				return L"Ctrl+Shift";
			case ComboKeys::SHIFT_CTRL_ALT:
				return L"Ctrl+Alt+Shift";
			}

			return L"";
		}

		std::wstring ShortcutToString(const Shortcut& shortcut)
		{
			std::wstring stLabel = ComboToString(shortcut.comboKeys);
			if(!stLabel.empty())
				stLabel += L"+";
			stLabel += shortcut.key;
			return stLabel;
		}

	}
}