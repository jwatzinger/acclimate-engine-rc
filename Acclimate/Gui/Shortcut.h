#pragma once
#include <string>

namespace acl
{
	namespace gui
	{

		enum class ComboKeys
		{
			NONE = 0,
			SHIFT = 1,
			CTRL = 2,
			SHIFT_CTRL = 3,
			ALT = 4,
			SHIFT_ALT = 5,
			CTRL_ALT = 6,
			SHIFT_CTRL_ALT = 7
		};

		enum class ShortcutState
		{
			ALWAYS, VISIBLE, FOCUS, NEVER
		};

		struct Shortcut
		{
			ComboKeys comboKeys;
			char key;
			ShortcutState state;
		};

		std::wstring ComboToString(ComboKeys keys);

		std::wstring ShortcutToString(const Shortcut& shortcut);

	}
}