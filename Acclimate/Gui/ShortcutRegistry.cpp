#include "ShortcutRegistry.h"
#include "Widget.h"
#include "..\System\Bit.h"

namespace acl
{
	namespace gui
	{

		void ShortcutRegistry::RegisterShortcut(const Shortcut& shortcut, Widget& widget)
		{
			const unsigned int keyCode = ConvertToKeyCode(shortcut.comboKeys, shortcut.key);
			auto& itr = m_mSignals[keyCode];
			itr.first = &widget;
			itr.second.Clear();
			itr.second.Connect(&widget, &Widget::OnShortcut);
		}

		void ShortcutRegistry::UnregisterShortcut(const Shortcut& shortcut)
		{
			auto itr = m_mSignals.find(ConvertToKeyCode(shortcut.comboKeys, shortcut.key));
			if(itr != m_mSignals.end())
				m_mSignals.erase(itr);
		}

		void ShortcutRegistry::Call(ComboKeys keys, unsigned int key) const
		{
			auto itr = m_mSignals.find(ConvertToKeyCode(keys, key));
			if(itr != m_mSignals.end())
			{
				if(itr->second.first->CanDoShortcut())
					itr->second.second();
			}
		}

		unsigned int ShortcutRegistry::ConvertToKeyCode(ComboKeys keys, unsigned int key)
		{
			unsigned int upperKey = towupper(key);
			if(bit::contains(keys, ComboKeys::SHIFT) && (upperKey != key))
			{
				switch(keys)
				{
				case ComboKeys::SHIFT:
					keys = ComboKeys::NONE;
					break;
				case ComboKeys::SHIFT_ALT:
					keys = ComboKeys::ALT;
					break;
				case ComboKeys::SHIFT_CTRL:
					keys = ComboKeys::SHIFT_CTRL;
					break;
				case ComboKeys::SHIFT_CTRL_ALT:
					keys = ComboKeys::CTRL_ALT;
					break;
				}
			}
			key = upperKey;
			key = key << 3;
			key |= (unsigned int)(keys);
			return key;
		}

	}
}