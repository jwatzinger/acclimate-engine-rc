#pragma once
#include <unordered_map>
#include "Shortcut.h"
#include "..\Core\Dll.h"
#include "..\Core\Signal.h"

namespace acl
{
	namespace gui
	{

		class Widget;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;

		class ACCLIMATE_API ShortcutRegistry
		{
			typedef std::unordered_map<unsigned int, std::pair<Widget*, core::Signal<>>> SignalMap;
		public:

			void RegisterShortcut(const Shortcut& shortcut, Widget& widget);
			void UnregisterShortcut(const Shortcut& shortcut);

			void Call(ComboKeys keys, unsigned int key) const;

		private:
#pragma warning( disable: 4251 )

			static unsigned int ConvertToKeyCode(ComboKeys keys, unsigned int key);

			SignalMap m_mSignals;
#pragma warning( default: 4251 )
		};

	}
}
