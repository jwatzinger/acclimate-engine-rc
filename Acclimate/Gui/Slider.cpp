#include "Slider.h"
#include "Module.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		Slider::Slider(float x, float y, float length, float size, float min, float max, float sliderValue, bool bVertical) : BaseButton(x, y, 0.0f, 0.0f), m_min(min),
            m_max(max), m_now(min), m_bVertical(bVertical), m_slider(sliderValue), m_length(length), m_aLength(0), m_pos(0), m_overCap(0), m_bValueDirty(false),
			m_isInteger(false)
		{
            if(bVertical)
                m_width = size;
            else
                m_height = size;
		}

        void Slider::SetLength(float length)
        {
            m_length = length;
        }

        void Slider::SetSliderValue(float sliderValue)
        {
            m_slider = sliderValue;
            OnParentResize(m_pParent->GetX(), m_pParent->GetY(), m_pParent->GetWidth(), m_pParent->GetHeight());
        }

		void Slider::SetRange(float min, float max)
		{
			m_min = min;
			m_max = max;
			OnDrag(math::Vector2(0, 0));
		}

		void Slider::SetValue(float value)
		{
			m_now = max(m_min, min(value, m_max));
			m_bValueDirty = true;
		}

		void Slider::SetIntegerMode(bool isInteger)
		{
			if(m_isInteger != isInteger)
			{
				m_isInteger = isInteger;
				m_bDirty = true;
			}
		}

		float Slider::GetMin(void) const
		{
			return m_min;
		}

		float Slider::GetMax(void) const
		{
			return m_max;
		}

		float Slider::GetValue(void) const
		{
			return m_now;
		}

		void Slider::OnRelease(MouseType mouse, bool bMouseOver)
		{
			BaseButton::OnRelease(mouse, bMouseOver);

			m_overCap = 0;
		}

		void Slider::OnDrag(const math::Vector2& vDistance)
		{
			if(!IsVisible())
				return;

			const int maxPosition = GetMaxPosition();

			if (m_bValueDirty)
			{
				const float relation = (m_now - m_min) / (m_max - m_min);
				m_pos = (int)(maxPosition*relation);
				m_bValueDirty = false;
			}

			int dist = 0;
			if (m_bVertical)
				dist = vDistance.y;
			else
				dist = vDistance.x;

			if (maxPosition < 0)
				m_now = max(m_min, min(m_max, m_now - dist));
			else
			{
				if (m_overCap != 0)
				{
					if (m_pos == maxPosition)
					{
						m_overCap -= dist;
						if (m_overCap <= 0)
							m_overCap = 0;
						else
							return;
					}
					else if (m_pos == 0)
					{
						m_overCap -= dist;
						if (m_overCap >= 0)
							m_overCap = 0;
						else
							return;
					}
					else
						m_overCap = 0;
				}
				// calculate value step size for 1 pixel drag distance
				const float step = 1.0f / (maxPosition);
				m_pos -= dist;

				// cap position
				const int newPos = m_pos;
				m_pos = max(0, min(maxPosition, m_pos));
				m_overCap = newPos - m_pos;

				// calculate relation from slider in range 0.0 to 1.0
				const float relation = m_pos*step;
				// check that relation is within bounds
				ACL_ASSERT(relation >= 0.0f && relation <= 1.0f);

				// calculate new value for 
				const float now = m_now;
				m_now = (m_max - m_min) * relation + m_min;
				m_bDirty |= m_now != now;
				// check that value is within bounds
			}

            ACL_ASSERT(m_now >= m_min && m_now <= m_max);

			SigValueChanged(m_now);
		}

        void Slider::OnParentResize(int x, int y, int width, int height)
        {
			float max = m_max, min = m_min, now = m_now;
			if(m_isInteger)
			{
				max = floor(max);
				min = floor(min);
				now = floor(now);
			}

            // calculate relation of slider value to borders
			float dist = max - min;
            // calculate relative slider size
            float sliderSize = m_slider / dist;
            // check if slider size exceeds border
			if(sliderSize >= 1.0f)
			{
				OnInvisible();
				return;
			}
			else
				OnVisible();

            // adjust slider relative values based on orientation
            if(m_bVertical)
            {
                //m_y = 0.0f;
                m_height = sliderSize * m_length;
				m_padH -= 16;
            }
            else
            {
                //m_x = 0.0f;
                m_width = sliderSize * m_length;
				m_padW -= 16;
            }

            BaseButton::OnParentResize(x, y, width, height);

            float relation = (now - min) / dist;
            relation *= m_length;

            if(m_bVertical)
            {
				m_padH += 16;
                m_aY += (int)(relation * (height - GetHeight() + 16 - m_padH));
                m_aLength = (int)((height - m_padH) * m_length);
            }
            else
            {
				m_padW += 16;
                m_aX += (int)(relation * (width - GetWidth() + 16 - m_padW));
                m_aLength = (int)(width * m_length) - m_padW;
            }
        }

        void Slider::OnRedraw(void)
		{
			BaseButton::OnRedraw();
			RenderData grData;
			//"icon"
			grData.rDestRect.Set(GetWidth()/2-8, GetHeight()/2-8, 16, 16);
			if(m_bVertical)
				grData.rSrcRect = m_pModule->m_layout.GetElement(L"SliderVertical")->GetPart(L"Icon");
			else
				grData.rSrcRect =  m_pModule->m_layout.GetElement(L"SliderHorizontal")->GetPart(L"Icon");
			m_vRenderData.push_back(grData);
		}

		void Slider::OnWheelMove(int distance)
		{
			m_overCap = 0;
			OnDrag(math::Vector2(0, -distance));
		}

		int Slider::GetMaxPosition(void) const
		{
			if(m_bVertical)
				return m_aLength-1-GetHeight()+16;
			else
				return m_aLength-1-GetWidth()+16;
		}

	}
}