#pragma once
#include "Button.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

#pragma warning( disable: 4251 )
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<float>;
#pragma warning( default: 4251 )

		class ACCLIMATE_API Slider :
			public BaseButton
		{
		public:
			Slider(float x, float y, float length, float size, float min, float max, float sliderValue, bool bVertical);

            void SetLength(float length);
            void SetSliderValue(float sliderValue);
			void SetRange(float min, float max);
			void SetValue(float value);
			void SetIntegerMode(bool isInteger);

			float GetMin(void) const;
			float GetMax(void) const;
			float GetValue(void) const;
	
			void OnRelease(MouseType mouse, bool bMouseOver) override;
            void OnDrag(const math::Vector2& vDistance) override;
            void OnParentResize(int x, int y, int width, int height) override;
            void OnRedraw(void) override;
			void OnWheelMove(int distance) override;

            core::Signal<float> SigValueChanged;

        private:

			int GetMaxPosition(void) const;

            int m_aLength, m_pos, m_overCap;
            float m_min, m_max, m_now, m_slider, m_length;
            bool m_bVertical, m_bValueDirty, m_isInteger;

		};

	}
}

