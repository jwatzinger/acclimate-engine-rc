#include "SliderBar.h"
#include "Module.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace gui
	{

		SliderBar::SliderBar(float x, float y, float length, float size, float min, float max, float slider, bool bVertical): Widget(x, y, 0.0f, 0.0f),
			m_precision(2), m_slider(0.0f, 0.5f, 1.0f, 0.5f, min, max, slider, bVertical), m_min(0.05f, 0.0f, 0.1f, 0.5f, conv::ToString(min, m_precision).c_str()),
			m_max(0.85f, 0.0f, 0.1f, 0.5f, conv::ToString(max, m_precision).c_str()), m_now(0.45f, 0.0f, 0.1f, 0.5f, conv::ToString(min, m_precision).c_str()),
			m_bVertical(bVertical)
		{
			AddChild(m_slider);
			AddChild(m_min);
			AddChild(m_max);
			AddChild(m_now);

			if(bVertical)
			{
                m_width = size;
				m_height = length;
			}
            else
			{
                m_height = size;
				m_width = length;
			}

			m_slider.SigValueChanged.Connect(this, &SliderBar::OnValueChanged);
		}

		void SliderBar::SetPrecicion(unsigned int precision)
		{
			m_precision = precision;
			m_min.SetString(conv::ToString(m_slider.GetMin(), m_precision).c_str());
			m_max.SetString(conv::ToString(m_slider.GetMax(), m_precision).c_str());
			m_now.SetString(conv::ToString(m_slider.GetValue(), m_precision).c_str());
		}

		void SliderBar::SetValue(float value)
		{
			m_slider.SetValue(value);
			OnValueChanged(value);
		}

		Slider& SliderBar::GetSlider(void)
		{
			return m_slider;
		}

		const Slider& SliderBar::GetSlider(void) const
		{
			return m_slider;
		}

		void SliderBar::OnValueChanged(float value)
		{
			m_now.SetString(conv::ToString(value, m_precision).c_str());
		}

		void SliderBar::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData grData;
			//"icon"
			grData.rSrcRect = m_pModule->m_layout.GetElement(L"Sliderbar")->GetPart(L"Body");

			const int width = GetWidth(), height = GetHeight();
			
			if(m_bVertical)
				grData.rDestRect.Set((int)(width*0.625f), 0, (int)(width*0.125f), height);
			else
				grData.rDestRect.Set(0, (int)(height*0.7f), width, (int)(height*0.125f));
			m_vRenderData.push_back(grData);
		}

	}
}