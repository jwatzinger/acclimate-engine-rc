#pragma once
#include "Widget.h"
#include "Slider.h"
#include "Label.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API SliderBar :
			public Widget
		{
		public:
			SliderBar(float x, float y, float length, float size, float min, float max, float slider, bool bVertical);
			
			void SetPrecicion(unsigned int precision);
			void SetValue(float value);

			Slider& GetSlider(void);
			const Slider& GetSlider(void) const;

			void OnRedraw(void);

		private:

			void OnValueChanged(float value);

			unsigned int m_precision;
			bool m_bVertical;

			Slider m_slider;
			Label m_min, m_now, m_max;
		};

	}
}

