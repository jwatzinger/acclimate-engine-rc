#include "SplashScreen.h"

namespace acl
{
    namespace gui
    {

        SplashScreen::SplashScreen(float width, float height, LPCWSTR lpFileName, double duration) : Widget( 0.5f - width / 2.0f, 0.5f - height / 2.0f, width, height),
			m_duration(duration), m_splash(0.0f, 0.0f, width, height, lpFileName)
        {
	        m_timer.Reset();
	        AddChild(m_splash);
        }

        void SplashScreen::OnParentUpdate(int x, int y)
        {
	        Widget::UpdateChildren();
	        if(m_timer.Duration() >= m_duration)
		        OnInvisible();
        }

    }
}