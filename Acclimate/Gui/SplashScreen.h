#pragma once
#include "Widget.h"
#include "Image.h"
#include "..\Core\Timer.h"

namespace acl 
{
	namespace gui
	{

		class SplashScreen :
			public Widget
		{
		public:
			SplashScreen(float width, float height, LPCWSTR lpFileName, double duration);

			void OnParentUpdate(int x, int y);

		private:

			double m_duration;
			core::Timer m_timer;

			Image m_splash;
		};

	}
}

