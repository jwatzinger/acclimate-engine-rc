#include "TabBar.h"
#include "Module.h"
#include "TabBarItem.h"
#include "ToolButton.h"
#include "Icon.h"
#include "ContextMenu.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		TabBar::TabBar(float x, float y, float width, float height) : Widget(x, y, width, height),
			m_alignement(TabAlignement::TOP), m_pActiveItem(nullptr), m_offset(0), m_area(0.0f, 0.0f, 1.0f, 1.0f)
		{
			Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", math::Rect(40, 72, 16, 16));
			m_pButton = new ToolButton(1.0f, 0.0f, pIcon);
			m_pButton->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
			m_pButton->OnInvisible();
			m_pButton->SigClicked.Connect(this, &TabBar::OnOpenTabSelect);

			m_pContextMenu = new ContextMenu(0.02f);
			m_pContextMenu->OnInvisible();
			m_pButton->AddChild(*m_pContextMenu);

			AddChild(m_area);
			AddChild(*m_pButton);
		}

		TabBar::~TabBar(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}

			delete m_pButton;
		}

		TabBarItem& TabBar::AddItem(const std::wstring& stText)
		{
			auto pItem = new TabBarItem(m_offset, m_alignement, stText);
			pItem->SigSelection.Connect(this, &TabBar::OnSelectItem);
			m_area.AddChild(*pItem);

			m_offset += (unsigned int)pItem->GetRelWidth();
			m_pContextMenu->AddItem(stText).SigClicked.Connect(pItem, &TabBarItem::OnSelect);

			m_vItems.push_back(pItem);

			return *pItem;
		}

		void TabBar::RemoveItem(const std::wstring& stText)
		{
			for(auto itr = m_vItems.begin(); itr != m_vItems.end(); ++itr)
			{
				if((*itr)->GetName() == stText)
				{
					m_pContextMenu->RemoveItem(stText);

					m_offset -= (unsigned int)(*itr)->GetRelWidth();
					delete *itr;
					m_vItems.erase(itr);
					return;
				}
			}
		}

		void TabBar::SelectByName(const std::wstring& stName)
		{
			for(auto pItem : m_vItems)
			{
				if(pItem->GetName() == stName)
				{
					pItem->OnSelect();
					return;
				}
			}
		}

		void TabBar::Clear(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
			m_vItems.clear();
			m_offset = 0;
			m_pActiveItem = nullptr;
		}

		void TabBar::SetContextMenu(ContextMenu* pMenu)
		{
			m_area.SetContextMenu(pMenu);
		}

		void TabBar::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"TabBar");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			Data.rSrcRect = rTopRight;
			Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(Data);

			// top
			const math::Rect& rTop = element.GetPart(L"Top");
			Data.rSrcRect = rTop;
			Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
			m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			Data.rSrcRect = rBottomLeft;
			Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			Data.rSrcRect = rBottomRight;
			Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(Data);

			// bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
			Data.rSrcRect = rBottom;
			Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(Data);

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rBottomRight.width, height - rTopLeft.height - rBottomRight.width);
			m_vRenderData.push_back(Data);
		}

		void TabBar::OnParentResize(int x, int y, int width, int height)
		{
			Widget::OnParentResize(x, y, width, height);

			if(m_offset > (unsigned int)GetWidth())
				m_pButton->OnVisible();
		}

		void TabBar::OnSelectItem(TabBarItem* pItem)
		{
			ACL_ASSERT(m_pActiveItem != pItem);
			if(m_pActiveItem)
				m_pActiveItem->Unselect();
			m_pActiveItem = pItem;

			const auto x = m_pActiveItem->GetX() - GetX() + m_pActiveItem->GetWidth();
			const auto width = GetWidth() - GetHeight();

			if(x > width)
				m_area.ChangeOffsetX((float)(x - width + m_area.GetOffsetX()));
			else
			{
				const auto leftX = x - m_pActiveItem->GetWidth();
				if(leftX < 0)
					m_area.ChangeOffsetX((float)leftX + m_area.GetOffsetX());
			}

			SigItemSelected(*pItem);
		}

		void TabBar::OnOpenTabSelect(void)
		{
			m_pContextMenu->PopUp(math::Vector2(m_pButton->GetX(), m_pButton->GetY()));
		}

	}
}

