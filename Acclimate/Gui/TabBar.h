#pragma once
#include "Area.h"

namespace acl
{
	namespace gui
	{

		enum class TabAlignement;

		class TabBarItem;
		class ToolButton;
		class ContextMenu;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const TabBarItem&>;

		class ACCLIMATE_API TabBar :
			public Widget
		{
			typedef std::vector<TabBarItem*> ItemVector;
		public:
			TabBar(float x, float y, float width, float height);
			~TabBar(void);

			TabBarItem& AddItem(const std::wstring& stText);
			void RemoveItem(const std::wstring& stText);
			void SelectByName(const std::wstring& stName);
			void Clear(void);

			void SetContextMenu(ContextMenu* pMenu) override;

			void OnRedraw(void) override;
			void OnParentResize(int x, int y, int width, int height) override;

			core::Signal<const TabBarItem&> SigItemSelected;

		private:
#pragma warning( disable: 4251 )
			void OnSelectItem(TabBarItem* pItem);
			void OnOpenTabSelect(void);

			unsigned int m_offset;

			Area m_area;
			TabAlignement m_alignement;
			ItemVector m_vItems;
			TabBarItem* m_pActiveItem;
			ToolButton* m_pButton;
			ContextMenu* m_pMenu;

#pragma warning( default: 4251 )
		};

	}
}


