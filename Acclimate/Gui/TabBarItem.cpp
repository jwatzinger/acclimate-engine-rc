#include "TabBarItem.h"
#include "Module.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		TabBarItem::TabBarItem(unsigned int x, TabAlignement alignment, const std::wstring& stLabel) : Widget((float)x, 0.0f, 0.0f, 1.0f),
			m_stLabel(stLabel), m_isSelected(false), m_isHovered(false)
		{
			SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::ABS, PositionMode::REL);
		}

		unsigned int TabBarItem::GetAbsWidth(void) const
		{
			return (unsigned int)m_width;
		}

		const std::wstring& TabBarItem::GetName(void) const
		{
			return m_stLabel;
		}

		void TabBarItem::Unselect(void)
		{
			ACL_ASSERT(m_isSelected);
			m_isSelected = false;
			m_bDirty = true;
		}

		void TabBarItem::OnSelect(void)
		{
			if(!m_isSelected)
			{
				m_isSelected = true;
				m_bDirty = true;
				SigSelect();
				SigSelection(this);
			}
		}

		void TabBarItem::OnMoveOn(void)
		{
			Widget::OnMoveOn();

			m_isHovered = true;
			m_bDirty = true;
		}

		void TabBarItem::OnMoveOff(void)
		{
			Widget::OnMoveOff();

			m_isHovered = false;
			m_bDirty = true;
		}

		void TabBarItem::OnClick(MouseType mouse)
		{
			Widget::OnClick(mouse);

			OnSelect();
		}

		void TabBarItem::OnRegister(Module& module)
		{
			Widget::OnRegister(module);

			m_width = (float)(m_pModule->CalculateTextRect(m_stLabel).width + 14);
		}

		void TabBarItem::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element* pElement = nullptr;
			if(m_isSelected)
				pElement = m_pModule->m_layout.GetElement(L"TabBarItemSelected");
			else
			{
				if(m_isHovered)
					pElement = m_pModule->m_layout.GetElement(L"TabBarItemHover");
				else
					pElement = m_pModule->m_layout.GetElement(L"TabBarItem");
			}
				
			const Element& element = *pElement;

			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			const math::Rect& rTop = element.GetPart(L"Top");

			if(m_alignment != TabAlignement::TOP)
			{
				//top left
				Data.rSrcRect = rTopLeft;
				Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
				m_vRenderData.push_back(Data);

				// top right
				Data.rSrcRect = rTopRight;
				Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
				m_vRenderData.push_back(Data);

				// top
				Data.rSrcRect = rTop;
				Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
				m_vRenderData.push_back(Data);
			}

			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			const math::Rect& rBottom = element.GetPart(L"Bottom");

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			if(m_alignment != TabAlignement::BOTTOM)
			{
				// bottom left
				Data.rSrcRect = rBottomLeft;
				Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
				m_vRenderData.push_back(Data);

				// bottom right

				Data.rSrcRect = rBottomRight;
				Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
				m_vRenderData.push_back(Data);

				// bottom bar
				Data.rSrcRect = rBottom;
				Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
				m_vRenderData.push_back(Data);
			}

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rBottomRight.width, height - rTopLeft.height - rBottomRight.width);
			m_vRenderData.push_back(Data);

			// text
			Data.stLabel = m_stLabel;
			Data.labelColor = element.GetColor(L"TextColor");
			Data.labelFlags = 5;
			Data.rDestRect.Set(0, 0, width, height);
			m_vRenderData.push_back(Data);
		}

	}
}

