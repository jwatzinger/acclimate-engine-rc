#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		enum class TabAlignement
		{
			NONE = 0, TOP = 1, BOTTOM = 2
		};

		class TabBarItem;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<TabBarItem*>;

		class ACCLIMATE_API TabBarItem :
			public Widget
		{
		public:
			TabBarItem(unsigned int x, TabAlignement alignmet, const std::wstring& stLabel);

			unsigned int GetAbsWidth(void) const;
			const std::wstring& GetName(void) const;

			void Unselect(void);

			void OnSelect(void);
			void OnMoveOn(void) override;
			void OnMoveOff(void) override;
			void OnClick(MouseType mouse) override;
			void OnRegister(Module& module) override;
			void OnRedraw(void) override;

			core::Signal<> SigSelect;
			core::Signal<TabBarItem*> SigSelection;

		private:
#pragma warning( disable: 4251 )
			bool m_isSelected, m_isHovered;
			std::wstring m_stLabel;

			TabAlignement m_alignment;
#pragma warning( default: 4251 )
		};

	}
}


