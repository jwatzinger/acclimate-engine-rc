#include "Table.h"
#include "TableItem.h"
#include "Label.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		Table::Table(float x, float y, float width, float height, float itemHeight) : Widget(x, y, width, height),
			m_itemHeight(itemHeight), m_leftColumnSize(0.5f)
		{
		}

		Table::~Table(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
		}

		void Table::SetLeftColumnSize(float size)
		{
			if(m_leftColumnSize != size)
			{
				if(!m_vItems.empty())
				{
					bool isLeft = true;
					for(auto pItem : m_vItems)
					{
						if(isLeft)
							pItem->SetWidth(size);
						else
						{
							pItem->SetX(size);
							pItem->SetWidth(1.0f - size);
						}

						isLeft = !isLeft;
					}

					ACL_ASSERT(!isLeft);
				}

				m_leftColumnSize = size;
			}
		}

		size_t Table::GetNumItems(void) const
		{
			return m_vItems.size()/2;
		}

		float Table::GetItemHeight(void) const
		{
			return m_itemHeight;
		}

		size_t Table::GetItemAreaSize(void) const
		{
			if(m_vItems.empty())
				return 0;
			else
				return (m_vItems.size() / 2) * m_vItems[0]->GetHeight();
		}

		TableItem& Table::AddRow(const std::wstring& stName)
		{
			// name
			const float y = m_vItems.size()/2*m_itemHeight;
			auto pName = new TableItem(0.0f, y, m_leftColumnSize, m_itemHeight);
			auto pLabel = new Label(0.0f, 0.0f, 1.0f, 1.0f, stName);
			pLabel->SetAlign(4);
			pLabel->SetPadding(8, 0, 16, 0);
			pName->AddChild(*pLabel);
			AddChild(*pName);
			m_vItems.push_back(pName);

			// item
			auto pItem = new TableItem(m_leftColumnSize, y, 1.0f - m_leftColumnSize, m_itemHeight);
			AddChild(*pItem);
			m_vItems.push_back(pItem);

			SigResize(m_vItems.size()/2);

			return *pItem;
		}

		void Table::RemoveRow(size_t id)
		{
			ACL_ASSERT(id < GetNumItems());

			auto realId = id * 2;
			
			delete m_vItems[realId];
			delete m_vItems[realId + 1];

			// delete name item first - value item will move to that same place afterwards
			m_vItems.erase(m_vItems.begin() + realId);
			m_vItems.erase(m_vItems.begin() + realId);

			auto itr = m_vItems.begin() + realId;
			while(itr != m_vItems.end())
			{
				(*itr)->SetX((*itr)->GetRelX() - m_itemHeight);
			}
		}

		void Table::Clear(void)
		{
			for(auto pItem : m_vItems)
			{
				delete pItem;
			}
			m_vItems.clear();
			SigResize(0);
		}

	}
}
