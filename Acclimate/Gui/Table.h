#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class TableItem;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<size_t>;

		class ACCLIMATE_API Table :
			public Widget
		{
			typedef std::vector<TableItem*> ItemVector;

		public:
			Table(float x, float y, float width, float height, float itemHeight);
			~Table(void);

			void SetLeftColumnSize(float size);

			size_t GetNumItems(void) const;
			float GetItemHeight(void) const;
			size_t GetItemAreaSize(void) const;

			TableItem& AddRow(const std::wstring& stName);
			void RemoveRow(size_t id);
			void Clear(void);

			core::Signal<size_t> SigResize;

		private:
#pragma warning( disable: 4251 )
			float m_itemHeight, m_leftColumnSize;

			ItemVector m_vItems;
#pragma warning( default: 4251 )
		};

	}
}

