#include "TableItem.h"
#include "Layout.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		TableItem::TableItem(float x, float y, float width, float height) : Widget(x, y, width, height)
		{
			SetPositionModes(PositionMode::REL, PositionMode::REF, PositionMode::REL, PositionMode::REF);
		}

		void TableItem::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			const int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"TableItem");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			Data.rSrcRect = rTopRight;
			Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			Data.rSrcRect = rBottomLeft;
			Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			Data.rSrcRect = rBottomRight;
			Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(Data);

			// bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
			Data.rSrcRect = rBottom;
			Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(Data);

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rBottomRight.width, height - rTopLeft.height - rBottomRight.width);
			m_vRenderData.push_back(Data);
		}

	}
}

