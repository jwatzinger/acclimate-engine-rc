#pragma once
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class TableItem :
			public Widget
		{
		public:
			TableItem(float x, float y, float width, float height);

			void OnRedraw(void);
		};

	}
}
