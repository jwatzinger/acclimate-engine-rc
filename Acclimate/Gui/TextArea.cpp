#include "TextArea.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		TextArea::TextArea(float x, float y, float width, float height, const std::wstring& stText) : Widget(x, y, width, height)
		{
			AddChild(m_field);
			SetText(stText);
		}

		void TextArea::SetText(const std::wstring& stName)
		{
			m_field.Clear();
			size_t pos = stName.find_first_of('\n');
			if(pos == std::wstring::npos)
			{
				if(!stName.empty())
					m_field.AddLine(stName);
			}
			else
			{
				std::wstring stTempName = stName;
				while(pos != std::wstring::npos)
				{
					m_field.AddLine(stTempName.substr(0, pos));
					stTempName = stTempName.substr(pos + 1);
					pos = stTempName.find_first_of('\n');
				}

				if(!stTempName.empty())
					m_field.AddLine(stTempName);
			}

			if(m_pModule)
			{
				const size_t height = m_field.GetNumLines()*m_pModule->m_fontSize;
				m_field.SetAreaHeight(height);
			}
		}

		void TextArea::SetBackgroundVisible(bool visible)
		{
			m_field.SetBackgroundVisible(visible);
		}

		void TextArea::SetColor(const gfx::Color& color)
		{
			m_field.SetColor(color);
		}

		TextArea::TextField::TextField(void) : ScrollArea(0.0f, 0.0f, 1.0f, 1.0f, 1, 1),
			m_backgroundVisible(true), m_color(0, 0, 0)
		{
			m_focus = FocusState::KEEP_FOCUS;
		}

		void TextArea::TextField::AddLine(const std::wstring& stLine)
		{
			m_vLines.push_back(stLine);
			m_bDirty = true;
		}

		void TextArea::TextField::Clear(void)
		{
			SetOffsetY(0);
			m_vLines.clear();
			m_bDirty = true;
		}

		void TextArea::TextField::SetBackgroundVisible(bool visible)
		{
			if(m_backgroundVisible != visible)
			{
				m_backgroundVisible = visible;
				m_bDirty = true;
				SetScrollbar(visible);
			}
		}

		void TextArea::TextField::SetColor(const gfx::Color& color)
		{
			m_color = color;
		}

		size_t TextArea::TextField::GetNumLines(void) const
		{
			return m_vLines.size();
		}

		void TextArea::TextField::OnKeyStroke(WCHAR key)
		{
			switch(key)
			{
			case VK_BACK:

			default:
				m_vLines[0].insert(0, &key);
			}

			m_bDirty = true;
		}

		void TextArea::TextField::OnRedraw(void)
		{
			m_vRenderData.clear();
			const int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Textbox");

			RenderData Data;

			// background
			if(m_backgroundVisible)
			{
				const math::Rect& rBack = element.GetPart(L"Back");
				Data.rSrcRect = rBack;
				Data.rDestRect.Set(0, 0, width - 16, height - 16);
				m_vRenderData.push_back(Data);
			}

			Data.rDestRect.Set(6, 3 - GetOffsetY(), width - 12, m_pModule->m_fontSize);

			const size_t firstLine = max(0, GetOffsetY() / (int)m_pModule->m_fontSize);
			Data.rDestRect.y += firstLine*m_pModule->m_fontSize;
			const size_t maxToDraw = firstLine + min(m_vLines.size(), height / m_pModule->m_fontSize);
			for(unsigned int i = firstLine; i < maxToDraw; i++)
			{
				//text
				Data.stLabel = m_vLines[i];
				Data.labelFlags = 0;
				Data.labelColor = m_color;
				m_vRenderData.push_back(Data);

				Data.rDestRect.y += m_pModule->m_fontSize;
			}
		}

	}
}

