#pragma once
#include <vector>
#include "Widget.h"
#include "ScrollArea.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API TextArea :
			public Widget
		{
		public:
			TextArea(float x, float y, float width, float height, const std::wstring& stText);

			void SetText(const std::wstring& stName);
			void SetBackgroundVisible(bool visible);
			void SetColor(const gfx::Color& color);

		private:
#pragma warning( disable: 4251 )

			class TextField :
				public ScrollArea
			{
				typedef std::vector<std::wstring> LineVector;
			public:
				TextField(void);

				void AddLine(const std::wstring& stLine);
				void Clear(void);

				void SetBackgroundVisible(bool visible);
				void SetColor(const gfx::Color& color);

				size_t GetNumLines(void) const;

				void OnRedraw(void) override;
				void OnKeyStroke(WCHAR key) override;

			private:

				bool m_backgroundVisible;
				gfx::Color m_color;
				LineVector m_vLines;
			};

			TextField m_field;
#pragma warning( default: 4251 )
		};

	}
}
