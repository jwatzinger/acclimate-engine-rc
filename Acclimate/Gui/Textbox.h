#pragma once
#include "Widget.h"
#include "Input.h"
#include "Layout.h"
#include "Module.h"
#include "ClipboardData.h"
#include "..\Core\Timer.h"
#include "..\System\Convert.h"

namespace acl
{
    namespace gui
    {

        template<typename DataType = std::wstring>
        class Textbox :
	        public Widget
        {
        public:
	        Textbox(float x, float y, float width, float height, DataType value): Widget(x, y, width, height), m_stText(conv::ToString(value)), 
				m_bShowCursor(true), m_bEditable(true), m_color(0, 0, 0), m_selected(0), m_stOriginalText(m_stText)
	        {
				m_focus = FocusState::KEEP_FOCUS;
				m_bClipping = true;
                m_cursorPos = m_stText.size();
	        }

	        void SetEditable(bool bEditable)
	        {
		        m_bEditable = bEditable;
	        }

	        void SetText(const std::wstring& stValue)
	        {
				if(m_stText != stValue)
				{
					m_stText = stValue;
					m_stOriginalText = stValue;
					m_cursorPos = m_stText.size();
					m_bDirty = true;
				}
	        }

			void SetTextColor(const gfx::Color& color)
			{
				m_color = color;
			}

	        DataType GetContent(void) const
	        {
		        DataType out;
		        conv::FromString(&out, m_stText.c_str());
		        return out;
	        }

			void OnClick(MouseType mouse) override
			{
				Widget::OnClick(mouse);
				
				MoveCursorByMouse(m_pModule->ComboKeysActive(ComboKeys::SHIFT));
			}

	        void OnMoveOn(void)
	        {
		        if(m_bEditable)
			        SigChangeCursorState(CursorState::TEXT_OVER);
	        }

	        void OnRedraw(void) override
	        {
		        m_vRenderData.clear();
		        RenderData Data;

				int width = GetWidth(), height = GetHeight();
				 
				const Element& element = *m_pModule->m_layout.GetElement(L"Textbox");
				
				//top left
				const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
				Data.rSrcRect = rTopLeft;
				Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
				m_vRenderData.push_back(Data);

				// top right
				const math::Rect& rTopRight = element.GetPart(L"TopRight");
				Data.rSrcRect = rTopRight;
				Data.rDestRect.Set(width-rTopRight.width, 0, rTopRight.width, rTopRight.height);
				m_vRenderData.push_back(Data);

				// top
				const math::Rect& rTop = element.GetPart(L"Top");
				Data.rSrcRect = rTop;
				Data.rDestRect.Set(rTopLeft.width, 0, width-rTopLeft.width-rTopRight.width, rTop.height);
				m_vRenderData.push_back(Data);

				// bottom left
				const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
				Data.rSrcRect = rBottomLeft;
				Data.rDestRect.Set(0, height-rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
				m_vRenderData.push_back(Data);
			
				// left bar
				const math::Rect& rLeft = element.GetPart(L"Left");
				Data.rSrcRect = rLeft;
				Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height-rTopLeft.height-rBottomLeft.height);
				m_vRenderData.push_back(Data);

				// bottom right
				const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
				Data.rSrcRect = rBottomRight;
				Data.rDestRect.Set(width-rBottomRight.width, height-rBottomRight.height, rBottomRight.width, rBottomRight.height);
				m_vRenderData.push_back(Data);

				// bottom bar
				const math::Rect& rBottom = element.GetPart(L"Bottom");
				Data.rSrcRect = rBottom;
				Data.rDestRect.Set(rBottomLeft.width, height-rBottom.height, width-rBottomLeft.width-rBottomRight.width, rBottom.height);
				m_vRenderData.push_back(Data);

				// right bar
				const math::Rect& rRight = element.GetPart(L"Right");
				Data.rSrcRect = rRight;
				Data.rDestRect.Set(width-rRight.width, rTopRight.height, rRight.width, height-rTopRight.height-rBottomRight.height);
				m_vRenderData.push_back(Data);

				// background
				const math::Rect& rBack = element.GetPart(L"Back");
				Data.rSrcRect = rBack;
				Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width-rTopLeft.width-rBottomRight.width, height-rTopLeft.height-rBottomRight.width);
				m_vRenderData.push_back(Data);

				Data.labelFlags = DT_VCENTER;
				Data.labelColor = m_color;

				if(!m_stText.empty())
				{
					if(m_selected)
					{
						// selection
						Data.rSrcRect = element.GetPart(L"Select");
						Data.rDestRect.Set(6, 3, 0, 0);

						std::wstring stSelected;
						if(m_selected > 0)
							stSelected = m_stText.substr(m_cursorPos, m_selected);
						else
							stSelected = m_stText.substr(m_cursorPos + m_selected, -m_selected);
						Data.rDestRect += m_pModule->CalculateTextRect(stSelected);

						std::wstring stText;
						if(m_selected > 0)
							stText = m_stText.substr(0, m_cursorPos);
						else
							stText = m_stText.substr(0, m_cursorPos + m_selected);

						Data.rDestRect.x += m_pModule->CalculateTextRect(stText).width;

						m_vRenderData.push_back(Data);
					}

					//text
					Data.rDestRect.Set(6, 3, width - 12, height - 6);
					Data.stLabel = m_stText;
					m_vRenderData.push_back(Data);
				}
				if(m_bShowCursor && m_bFocus && m_bEditable)
				{
					Data.rDestRect.Set(6, 3, width - 12, height - 6);
					Data.rDestRect.x += m_pModule->CalculateTextRect(m_stText.substr(0, m_cursorPos)).width;
					Data.stLabel = L"|";
					m_vRenderData.push_back(Data);
				}
	        }

	        void OnKeyStroke(WCHAR key)
			{
				if (!m_bEditable)
					return;

				switch(key)
				{
				case VK_BACK:
			        if( !m_stText.empty() )
			        {
						if(m_selected)
							EraseSelected();
						else if(m_cursorPos != 0)
						{
							m_stText.erase(m_stText.begin() + m_cursorPos - 1);
							m_cursorPos -= 1;
						}
						else
							return;
				        
						m_bShowCursor = true;
						m_timer.Reset();
			        }
					break;
				case VK_RETURN: //enter
		        {
					m_stOriginalText = m_stText;
					SigContentSet(conv::FromString<DataType>(m_stText.c_str()));
					SigConfirm();
					break;
		        }
				default:
					if(iswprint(key))
					{
						if(m_selected)
							EraseSelected();
						m_stText.insert(m_cursorPos, &key);
						m_cursorPos += 1;
						m_timer.Reset();
					}
		        }

				SigContentChanged(conv::FromString<DataType>(m_stText.c_str()));
				m_bDirty = true;
	        }

			void OnKeyDown(Keys key) override
	        {
		        switch(key)
		        {
				case Keys::LEFT_ARROW:
					if(m_cursorPos > 0)
					{
						if(m_pModule->ComboKeysActive(ComboKeys::SHIFT))
							m_selected += 1;
						else
							m_selected = 0;
						m_cursorPos -= 1;
					}
				    break;
				case Keys::RIGHT_ARROW:
					if(m_cursorPos < m_stText.size())
					{
						if(m_pModule->ComboKeysActive(ComboKeys::SHIFT))
							m_selected -= 1;
						else
							m_selected = 0;
						m_cursorPos += 1;
					}
				    break;
				case Keys::A:
					if(m_pModule->ComboKeysActive(ComboKeys::CTRL))
					{
						m_cursorPos = m_stText.size();
						m_selected = -(int)m_cursorPos;
					}
					break;
				case Keys::C:
					if(m_pModule->ComboKeysActive(ComboKeys::CTRL))
					{
						if(m_selected)
							m_pModule->m_clipboard.AddData<ClipboardTextData>(GetSelectedText());
					}
					break;
				case Keys::V:
					if(m_pModule->ComboKeysActive(ComboKeys::CTRL))
					{
						if(auto pData = m_pModule->m_clipboard.GetCurrentData<ClipboardTextData>())
						{
							if(m_selected)
								EraseSelected();
							auto& stText = pData->GetText();
							m_stText.insert(m_cursorPos, stText);
							m_cursorPos += stText.size();
						}
					}
					break;
				case Keys::DELETE_KEY:
					if(!m_stText.empty())
					{
						if(m_selected)
							EraseSelected();
						else if(m_cursorPos != m_stText.size())
							m_stText.erase(m_stText.begin() + m_cursorPos);
						else if(m_cursorPos != 0)
						{
							m_stText.erase(m_stText.begin() + m_cursorPos - 1);
							m_cursorPos -= 1;
						}
						else
							return;
					}
					break;
				case Keys::ESC:
					m_stText = m_stOriginalText;
					OnFocus(false);
					break;
				default:
					Widget::OnKeyDown(key);
					return;
		        }

				m_bShowCursor = true;
				m_timer.Reset();
				m_bDirty = true;
	        }

			void OnParentUpdate(void) override
			{
				const double duration = m_timer.Duration();
				const bool showCursor = m_bShowCursor;
				m_bShowCursor = ((unsigned int)duration % 2) == 0;
				if(m_bShowCursor != showCursor)
					m_bDirty = true;

				Widget::OnParentUpdate();
			}

			void OnDrag(const math::Vector2& vDistance) override
			{
				Widget::OnDrag(vDistance);
				MoveCursorByMouse(true);
			}

			void OnFocus(bool bFocus) override
			{
				if(!bFocus)
				{
					m_selected = 0;
					m_cursorPos = 0;
					if(m_stOriginalText != m_stText)
					{
						m_stOriginalText = m_stText;
						SigContentSet(conv::FromString<DataType>(m_stText.c_str()));
					}
				}
				else
					m_timer.Reset();
				m_bDirty = true;

				Widget::OnFocus(bFocus);
			}

	        core::Signal<DataType> SigContentChanged;
			core::Signal<DataType> SigContentSet;
	        core::Signal<> SigConfirm;

        private:

			void MoveCursorByMouse(bool bSelect)
			{
				if(!m_stText.empty())
				{
					int width = 6;
					const int projectedMouse = m_vLastMousePos.x - GetX();
					const size_t textSize = m_stText.size();
					for(unsigned int i = 0; i <= textSize; i++)
					{
						if(i < textSize)
							width += m_pModule->CalculateTextRect(m_stText.substr(i, 1)).width;
						if(projectedMouse < width || i == textSize)
						{
							if(bSelect)
								m_selected = (m_cursorPos + m_selected) - i;
							else
								m_selected = 0;

							m_cursorPos = i;
							m_bDirty = true;
							return;
						}
					}
				}
			}

			void EraseSelected(void)
			{
				if(m_selected > 0)
					m_stText.erase(m_cursorPos, m_selected);
				else
				{
					m_cursorPos += m_selected;
					m_stText.erase(m_cursorPos, -m_selected);
				}

				m_selected = 0;
			}

			std::wstring GetSelectedText(void) const
			{
				if(m_selected > 0)
					return m_stText.substr(m_cursorPos, m_selected);
				else
					return m_stText.substr(m_cursorPos + m_selected, -m_selected);
			}

	        unsigned int m_cursorPos;
			int m_selected;
	        bool m_bShowCursor, m_bEditable;
	        std::wstring m_stText, m_stOriginalText;

			gfx::Color m_color;

			static core::Timer m_timer;
        };

		template<typename DataType>
		core::Timer Textbox<DataType>::m_timer = core::Timer();

    }
}

