#include "Texture.h"
#include "..\Gfx\ITexture.h"

namespace acl
{
	namespace gui
	{

		Texture::Texture(float x, float y, float width, float height, const gfx::ITexture* pTexture) : BaseImage(x, y, width, height), m_pTexture(pTexture)
		{
		}

		void Texture::OnRedraw(void)
		{
			m_vRenderData.clear();

			if (!m_pTexture)
				return;

			RenderData Data;
			Data.pTexture = m_pTexture;
			//crop or scale?
			if(m_bCrop)
				Data.rSrcRect.Set(-m_offsetX, -m_offsetY, (int)(m_absWidth / m_zoom), (int)(m_absHeight / m_zoom));
			else
			{
				const math::Vector2& rSize = m_pTexture->GetSize();
				Data.rSrcRect.Set(0, 0, (int)(rSize.x / m_zoom), (int)(rSize.y / m_zoom));
			}
			Data.rDestRect.Set(0, 0, m_absWidth, m_absHeight);
			m_vRenderData.push_back(Data);
		}

        void Texture::SetTexture(const gfx::ITexture* pTexture)
        {
            m_pTexture = pTexture;
			m_bDirty = true;
        }

		const gfx::ITexture* Texture::GetTexture(void) const
		{
			return m_pTexture;
		}

	}
}
