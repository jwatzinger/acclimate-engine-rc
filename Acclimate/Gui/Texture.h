#pragma once
#include "BaseImage.h"

namespace acl
{
	namespace gui
	{

		class ACCLIMATE_API Texture :
			public BaseImage
		{
		public:
			Texture(float x, float y, float width, float height, const gfx::ITexture* pTexture);

			//events
			void OnRedraw(void) override;

            void SetTexture(const gfx::ITexture* pTexture);

			const gfx::ITexture* GetTexture(void) const;

		private:

			const gfx::ITexture* m_pTexture;
		};

	}
}

