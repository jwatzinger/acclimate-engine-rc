#include "ToolButton.h"
#include "Icon.h"

namespace acl
{
	namespace gui
	{

		ToolButton::ToolButton(float x, float y, Icon* pIcon) : BaseButton(x, y, 0.0f, 0.0f), m_pIcon(pIcon)
		{
			if(m_pIcon)
			{
				AddChild(*pIcon);
				pIcon->OnDisable();
				pIcon->SetCenter(HorizontalAlign::CENTER, VerticalAlign::CENTER);
				pIcon->SetAlignement(HorizontalAlign::CENTER, VerticalAlign::CENTER);
			}
		}

        ToolButton::~ToolButton(void)
        {
            delete m_pIcon;
        }

        void ToolButton::OnParentResize(int x, int y, int width, int height)
        {
			const int parentWidth = m_pParent->GetWidth();
			const int parentHeight = m_pParent->GetHeight();

			if(parentWidth > parentHeight)
				SetBorderRelation(gui::BorderRelation::WIDTH_FROM_HEIGHT);
			else
				SetBorderRelation(gui::BorderRelation::HEIGHT_FROM_WIDTH);

            BaseButton::OnParentResize(x, y, width, height);

        }

	}
}