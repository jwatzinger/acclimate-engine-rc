#pragma once
#include "BaseButton.h"

namespace acl
{
	namespace gui
	{
		class Icon;

		class ACCLIMATE_API ToolButton :
			public BaseButton
		{
		public:
			ToolButton(float x, float y, Icon* pIcon = nullptr);
            ~ToolButton(void);
	
            void OnParentResize(int x, int y, int width, int height) override;

        private:
#pragma warning( disable: 4251 )
            Icon* m_pIcon;
#pragma warning( default: 4251 )

		};

	}
}

