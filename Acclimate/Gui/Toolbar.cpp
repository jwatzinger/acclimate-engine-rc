#include "Toolbar.h"
#include "ToolButton.h"
#include "ButtonGroup.h"
#include "Icon.h"
#include "Module.h"

namespace acl
{
    namespace gui
    {

		Toolbar::Toolbar(float y, float height) : Widget(0.0f, y, 1.0f, height)
        {
        }

		Toolbar::~Toolbar(void)
		{
			for(auto& buttonGroup : m_mButtons)
			{
				delete buttonGroup.second;
			}
		}

		ToolButton* Toolbar::AddButton(const std::wstring& stIcon, const std::wstring& stGroup)
        {
	        ButtonGroup* pButtonGroup = nullptr;
	        if(!m_mButtons.count(stGroup))
	        {
		        pButtonGroup = new ButtonGroup();
				m_mButtons.emplace(stGroup, pButtonGroup);
		        AddChild(*pButtonGroup);
	        }
	        else
		        pButtonGroup = m_mButtons.at(stGroup);

	        Icon* pIcon = new Icon(0.0f, 0.0f, 1.0f, stIcon, math::Rect(0, 0, 22, 22));

	        ToolButton* pButton = new ToolButton(0.0f, 0.0f, pIcon);
	        pButtonGroup->AddButton(*pButton);

	        //adjust button groups
	        int posX = 0;
	        for(auto& buttonGroup : m_mButtons)
	        {
				auto pGroup = buttonGroup.second;
		        posX += 8;
		        pGroup->SetPadding(posX, 0, 0, 0);
		        const int numButtons = pGroup->NumButtons();
		        posX += numButtons*32;
	        }

	        return pButton;
        }

        void Toolbar::OnRedraw(void)
        {
	        m_vRenderData.clear();
	        RenderData Data;
	        Data.rSrcRect = m_pModule->m_layout.GetElement(L"Toolbar")->GetPart(L"Body");
	        Data.rDestRect.Set(0, 0, GetWidth(), GetHeight());
	        m_vRenderData.push_back(Data);
        }

    }
}
