#pragma once
#include <map>
#include "Widget.h"

namespace acl
{
	namespace gui
	{

		class ToolButton;
		class ButtonGroup;

		class ACCLIMATE_API Toolbar :
			public Widget
		{
			typedef std::map<std::wstring, ButtonGroup*> ButtonGroupMap;

		public:
			Toolbar(float y, float height);
			~Toolbar(void);

			ToolButton* AddButton(const std::wstring& stIcon, const std::wstring& stGroup = L"");

			void OnRedraw(void) override;

		private:
#pragma warning( disable: 4251 )
			ButtonGroupMap m_mButtons;
#pragma warning( default: 4251 )
		};

	}
}

