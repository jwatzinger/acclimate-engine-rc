#include "Tooltip.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		Tooltip::Tooltip(const std::wstring& stText) : Widget(0, 0, 0, 0), m_stText(stText),
			m_toShow(false)
		{
			SetVisible(false);
			OnDisable();
			SetPositionModes(PositionMode::ABS, PositionMode::ABS, PositionMode::ABS, PositionMode::ABS);
		}

		void Tooltip::SetText(const std::wstring& stText)
		{
			if(m_stText != stText)
			{
				m_stText = stText;
				MarkDirtyRect();
			}
		}

		void Tooltip::Show(void)
		{
			m_toShow = true;
			m_timer.Reset();
		}

		void Tooltip::Hide(void)
		{
			m_toShow = false;
			SetVisible(false);
		}

		void Tooltip::OnRegister(Module& module)
		{
			Widget::OnRegister(module);

			auto rect = module.CalculateTextRect(m_stText);
			m_width = (float)rect.width + 10;
			m_height = (float)rect.height + 12;
		}

		void Tooltip::OnParentUpdate(void)
		{
			Widget::OnParentUpdate();

			if(m_toShow && m_timer.Duration() >= 0.5f)
			{
				m_toShow = false;
				auto& vCursor = m_pModule->m_cursor.GetPosition();
				m_x = (float)(vCursor.x - m_pParent->GetX());
				m_y = (float)((vCursor.y + 24) - m_pParent->GetY());
				SetVisible(true);
			}
		}

		void Tooltip::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData Data;

			int width = GetWidth(), height = GetHeight();

			const Element& element = *m_pModule->m_layout.GetElement(L"Tooltip");

			//top left
			const math::Rect& rTopLeft = element.GetPart(L"TopLeft");
			Data.rSrcRect = rTopLeft;
			Data.rDestRect.Set(0, 0, rTopLeft.width, rTopLeft.height);
			m_vRenderData.push_back(Data);

			// top right
			const math::Rect& rTopRight = element.GetPart(L"TopRight");
			Data.rSrcRect = rTopRight;
			Data.rDestRect.Set(width - rTopRight.width, 0, rTopRight.width, rTopRight.height);
			m_vRenderData.push_back(Data);

			// top
			const math::Rect& rTop = element.GetPart(L"Top");
			Data.rSrcRect = rTop;
			Data.rDestRect.Set(rTopLeft.width, 0, width - rTopLeft.width - rTopRight.width, rTop.height);
			m_vRenderData.push_back(Data);

			// bottom left
			const math::Rect& rBottomLeft = element.GetPart(L"BottomLeft");
			Data.rSrcRect = rBottomLeft;
			Data.rDestRect.Set(0, height - rBottomLeft.height, rBottomLeft.width, rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// left bar
			const math::Rect& rLeft = element.GetPart(L"Left");
			Data.rSrcRect = rLeft;
			Data.rDestRect.Set(0, rTopLeft.height, rLeft.width, height - rTopLeft.height - rBottomLeft.height);
			m_vRenderData.push_back(Data);

			// bottom right
			const math::Rect& rBottomRight = element.GetPart(L"BottomRight");
			Data.rSrcRect = rBottomRight;
			Data.rDestRect.Set(width - rBottomRight.width, height - rBottomRight.height, rBottomRight.width, rBottomRight.height);
			m_vRenderData.push_back(Data);

			// bottom bar
			const math::Rect& rBottom = element.GetPart(L"Bottom");
			Data.rSrcRect = rBottom;
			Data.rDestRect.Set(rBottomLeft.width, height - rBottom.height, width - rBottomLeft.width - rBottomRight.width, rBottom.height);
			m_vRenderData.push_back(Data);

			// right bar
			const math::Rect& rRight = element.GetPart(L"Right");
			Data.rSrcRect = rRight;
			Data.rDestRect.Set(width - rRight.width, rTopRight.height, rRight.width, height - rTopRight.height - rBottomRight.height);
			m_vRenderData.push_back(Data);

			// background
			const math::Rect& rBack = element.GetPart(L"Back");
			Data.rSrcRect = rBack;
			Data.rDestRect.Set(rTopLeft.width, rTopLeft.height, width - rTopLeft.width - rBottomRight.width, height - rTopLeft.height - rBottomRight.width);
			m_vRenderData.push_back(Data);

			// text
			Data.stLabel = m_stText;
			Data.labelColor = element.GetColor(L"TextColor");
			Data.labelFlags = 5;
			Data.rDestRect.Set(0, 0, width, height);
			m_vRenderData.push_back(Data);
		}

	}
}

