#pragma once
#include "Widget.h"
#include "..\Core\Timer.h"

namespace acl
{
	namespace gui
	{

		class Tooltip :
			public Widget
		{
		public:
			Tooltip(const std::wstring& stText);

			void SetText(const std::wstring& stText);

			void Show(void);
			void Hide(void);

			void OnRegister(Module& module) override;
			void OnParentUpdate(void) override;
			void OnRedraw(void) override;

		private:

			std::wstring m_stText;
			bool m_toShow;
			core::Timer m_timer;
		};

	}
}


