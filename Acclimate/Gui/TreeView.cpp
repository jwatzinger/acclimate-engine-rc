#include "TreeView.h"
#include "ITreeModel.h"
#include "Input.h"
#include "Module.h"
#include "ITreeNodeCallback.h"

namespace acl
{
	namespace gui
	{

		TreeView::TreeView(float x, float y, float width, float height, float itemHeight) : Widget(x, y, width, height),
			m_area(0.0f, 0.0f, 1.0f, 1.0f, 0, 1), m_root(0.0f, itemHeight, 0, L"Root", nullptr), m_pModel(nullptr), 
			m_pSelected(nullptr), m_itemHeight(itemHeight)
		{
			AddChild(m_area);

			m_root.SigSelected.Connect(this, &TreeView::OnSelectItem);
			m_area.AddChild(m_root);
		}

		TreeView::~TreeView(void)
		{
			if(m_pModel)
				m_pModel->GetUpdateSignal().Disconnect(this, &TreeView::OnUpdateModel);
		}

		void TreeView::SetModel(ITreeModel* pModel)
		{
			if(pModel != m_pModel)
			{
				if(pModel)
					pModel->GetUpdateSignal().Connect(this, &TreeView::OnUpdateModel);
				else
					m_pModel->GetUpdateSignal().Disconnect(this, &TreeView::OnUpdateModel);

				m_pModel = pModel;

				OnUpdateModel();
			}
		}

		void TreeView::SetContextMenu(gui::ContextMenu* pMenu)
		{
			m_area.SetContextMenu(pMenu);
		}

		void TreeView::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData data;
			data.rDestRect.Set(0, 0, GetWidth() - 16, GetHeight());
			data.rSrcRect.Set(24, 56, 16, 16);
			m_vRenderData.push_back(data);
		}

		void TreeView::OnSelectItem(TreeViewNode* pNode)
		{
			if(m_pSelected != pNode)
			{
				if(m_pSelected)
					m_pSelected->OnDeselect();

				m_pSelected = pNode;

				if(!m_pSelected)
					return;

				// view change
				const auto y = (int)(m_pSelected->GetY() - GetY() + m_pSelected->GetHeight());
				const auto height = GetHeight();

				if(y > height)
					m_area.SetOffsetY(y - height + m_area.GetOffsetY());
				else
				{
					const auto leftY = y - m_pSelected->GetHeight();
					if(leftY < 0)
						m_area.SetOffsetY(leftY + m_area.GetOffsetY());
				}
			}
		}

		void TreeView::OnUpdateModel(void)
		{
			OnSelectItem(nullptr);
			m_root.Clear();
			
			if(m_pModel)
			{
				auto& root = m_pModel->GetRoot();
				m_root.SetName(root.stName);

				NodeVector vNodes;

				for(auto& node : root.vChilds)
				{
					TraverseNode(node, m_root, vNodes);
				}

				for(auto pNode : vNodes)
				{
					auto pCallback = pNode->GetCallback();
					if(pCallback && !pCallback->IsDefaultExpanded())
						pNode->SetNodesVisible(false);
				}
			}

			m_area.SetAreaHeight(m_root.GetTreeHeight() * (int)(m_itemHeight * m_pModule->m_layout.GetReferenceSize().y));

			UpdateChildren();
		}

		void TreeView::TraverseNode(const Node& node, TreeViewNode& parent, NodeVector& vNodes)
		{
			auto& newNode = parent.AddNode(node.stName, node.pCallback);
			newNode.SigSelected.Connect(this, &TreeView::OnSelectItem);
			newNode.SigWheelMoved.Connect(&m_area, &ScrollArea::OnWheelMove);
			newNode.SigKey.Connect(this, &TreeView::OnKeyDown);
			vNodes.push_back(&newNode);

			for(auto& child : node.vChilds)
			{
				TraverseNode(child, newNode, vNodes);
			}
		}

	}
}

