#pragma once
#include "Widget.h"
#include "ScrollArea.h"
#include "TreeViewNode.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		class ITreeModel;
		struct Node;

		class ACCLIMATE_API TreeView :
			public Widget
		{
			typedef std::vector<gui::TreeViewNode*> NodeVector;
		public:
			TreeView(float x, float y, float width, float height, float itemHeight);
			~TreeView(void);

			void SetModel(ITreeModel* pModel);
			void SetContextMenu(gui::ContextMenu* pMenu) override;

			void OnRedraw(void) override;

		private:
#pragma warning( disable: 4251 )
			void OnUpdateModel(void);
			void OnSelectItem(TreeViewNode* pNode);

			void TraverseNode(const Node& node, TreeViewNode& parent, NodeVector& vNodes);

			float m_itemHeight;
			ITreeModel* m_pModel;

			ScrollArea m_area;
			TreeViewNode m_root, *m_pSelected;
#pragma warning( default: 4251 )
		};

	}
}


