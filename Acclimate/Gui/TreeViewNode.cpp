#include "TreeViewNode.h"
#include "Icon.h"
#include "Input.h"
#include "Module.h"
#include "ContextMenu.h"
#include "ITreeNodeCallback.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		static const math::Rect rIcon = math::Rect();

		TreeViewNode::TreeViewNode(float y, float height, size_t depth, const std::wstring& stName, ITreeNodeCallback* pCallback): 
			Widget(0.0f, y, 1.0f, height), m_stName(stName), m_bSelected(false), m_depth(depth), m_pExpander(nullptr), m_bChildrenVisible(true), m_pIcon(nullptr),
			m_pCallback(pCallback), m_pRenameBox(nullptr)
		{
			SetPositionModes(PositionMode::REL, PositionMode::REL, PositionMode::REL, PositionMode::REF);

			m_focus = FocusState::KEEP_FOCUS;

			if(pCallback)
			{
				if(auto pIconName = pCallback->GetIconName())
				{
					m_pIcon = new Icon(0.0f, 0.5f, 0.75f, *pIconName, rIcon);
					m_pIcon->SetCenter(HorizontalAlign::LEFT, VerticalAlign::CENTER);
					m_pIcon->OnDisable();
					AddChild(*m_pIcon);
				}
			}
		}

		TreeViewNode::~TreeViewNode()
		{
			for(auto pNode : m_vChildNodes)
			{
				delete pNode;
			}
			delete m_pExpander;
			delete m_pIcon;
			if(m_pCallback)
			{
				if(m_pContextMenu)
					m_pCallback->OnContextMenuClose(*m_pContextMenu);
				delete m_pCallback;
			}
			delete m_pRenameBox;
		}
		
		void TreeViewNode::SetName(const std::wstring& stName)
		{
			m_stName = stName;
		}

		void TreeViewNode::SetNodesVisible(bool bVisible)
		{
			if(m_bChildrenVisible && !bVisible || !m_bChildrenVisible && bVisible)
				OnToggleNodes();
		}

		size_t TreeViewNode::GetTreeHeight(void) const
		{
			size_t size = 1;
			if(m_bChildrenVisible)
			{
				for(auto pChild : m_vChildNodes)
				{
					size += pChild->GetTreeHeight();
				}
			}
			return size;
		}

		bool TreeViewNode::IsSelected(void) const
		{
			return m_bSelected;
		}

		bool TreeViewNode::HasSelectedNode(void) const
		{
			if(m_bSelected)
				return true;
			else
			{
				for(auto pNode : m_vChildNodes)
				{
					if(pNode->HasSelectedNode())
						return true;
				}

				return false;
			}
		}

		const ITreeNodeCallback* TreeViewNode::GetCallback(void) const
		{
			return m_pCallback;
		}

		TreeViewNode& TreeViewNode::AddNode(const std::wstring& stName, ITreeNodeCallback* pCallback)
		{
			if(!m_vChildNodes.size())
			{
				m_pExpander = new Icon(0.0f, 0.5f, 0.25f, L"", math::Rect(96, 32, 8, 8));
				m_pExpander->SetCapMetrics(0, 0, 8, 8);
				m_pExpander->SetCenter(HorizontalAlign::LEFT, VerticalAlign::CENTER);
				m_pExpander->SigClicked.Connect(this, &TreeViewNode::OnToggleNodes);
				AddChild(*m_pExpander);
			}

			auto pNode = new TreeViewNode((float)GetTreeHeight(), m_height, m_depth+1, stName, pCallback);
			pNode->SigExpand.Connect(this, &TreeViewNode::OnExpandNode);
			pNode->SigMoveSelection.Connect(this, &TreeViewNode::OnMoveChildSelection);
			pNode->SigSelectParent.Connect(this, &TreeViewNode::OnSelect);
			pNode->SigHeightChanged.Connect(this, &TreeViewNode::OnHeightChanged);
			AddChild(*pNode);
			
			m_vChildNodes.push_back(pNode);

			return *pNode;
		}

		void TreeViewNode::Clear(void)
		{
			for(auto pNode : m_vChildNodes)
			{
				delete pNode;
			}
			m_vChildNodes.clear();
			delete m_pExpander;
			m_pExpander = nullptr;
		}

		void TreeViewNode::Move(int distance)
		{
			m_y += (float)distance;
		}

		void TreeViewNode::UpdateChildren(void)
		{
			if(m_pExpander)
				m_pExpander->SetPadding(m_depth*GetHeight(), 0, 0, 0);
			if(m_pIcon)
				m_pIcon->SetPadding((m_depth + 1)*GetHeight(), 0, 0, 0);

			Widget::UpdateChildren();
		}

		void TreeViewNode::OnRedraw(void)
		{
			m_vRenderData.clear();
			RenderData data;

			const int width = GetWidth(), height = GetHeight();

			// selector
			if(m_bSelected)
			{
				data.rDestRect.Set(0, 0, width, height);
				data.rSrcRect.Set(0, 88, 8, 8);
				m_vRenderData.push_back(data);
			}

			// label
			const int offset = GetTextOffset();

			data.rDestRect.Set(offset + height*m_depth, 0, width - offset, height);
			data.stLabel = m_stName;
			data.labelColor = gfx::Color(0, 0, 0);
			data.labelFlags = DT_LEFT | DT_VCENTER;
			m_vRenderData.push_back(data);
		}

		void TreeViewNode::OnSelect(void)
		{
			if(!m_bSelected)
			{
				m_bSelected = true;
				m_bDirty = true;
				SigSelected(this);
				
				if(m_pCallback)
				{
					SetContextMenu(m_pCallback->OnContextMenu());
					if(m_pContextMenu)
					{
						if(m_pCallback->IsDeletable())
						{
							auto& del = m_pContextMenu->AddItem(L"Delete");
							del.SigReleased.Connect(m_pCallback, &ITreeNodeCallback::OnDelete);
						}

						if(m_pCallback->IsRenameable())
						{
							auto& rename = m_pContextMenu->AddItem(L"Rename");
							rename.SigReleased.Connect(this, &TreeViewNode::OnRename);
						}

						m_pModule->RegisterWidget(*m_pContextMenu);
					}

					m_pCallback->OnSelect();
				}

				OnFocus(true);
			}
		}

		void TreeViewNode::OnSelectLast(void)
		{
			if(!m_bChildrenVisible || m_vChildNodes.empty())
				OnSelect();
			else
				m_vChildNodes[m_vChildNodes.size()-1]->OnSelectLast();
		}

		void TreeViewNode::OnDeselect(void)
		{
			if(m_bSelected)
			{
				if(m_pCallback && m_pContextMenu)
				{
					m_pCallback->OnContextMenuClose(*m_pContextMenu);
					m_pContextMenu = nullptr;
				}
					
				m_bSelected = false;
				m_bDirty = true;
			}
		}

		void TreeViewNode::OnClick(MouseType mouse)
		{
			OnSelect();
			Widget::OnClick(mouse);
		}

		void TreeViewNode::OnRelease(MouseType mouse, bool bMouseOver)
		{
			if(mouse == MouseType::LEFT && bMouseOver && m_bSelected && m_bFocus && m_pCallback && m_pCallback->IsRenameable())
			{
				const int leftBorder = GetX() + GetTextOffset() + GetHeight()*m_depth;
				const int rightBorder = leftBorder + m_stName.size()*12;
				if(m_vLastMousePos.x > leftBorder && m_vLastMousePos.x < rightBorder)
					OnRename();
			}
				
			Widget::OnRelease(mouse, bMouseOver);
		}

		void TreeViewNode::OnVisible(void)
		{
			Widget::OnVisible();
			if(m_bChildrenVisible)
			{
				for(auto pNode : m_vChildNodes)
				{
					pNode->OnVisible();
				}
			}
		}

		void TreeViewNode::OnInvisible(void)
		{
			Widget::OnInvisible();
			
			for(auto pNode : m_vChildNodes)
			{
				pNode->OnInvisible();
			}
		}

		void TreeViewNode::OnKeyDown(Keys key)
		{
			Widget::OnKeyDown(key);

			switch(key)
			{
			case Keys::DOWN_ARROW:
				if(m_bChildrenVisible && !m_vChildNodes.empty())
					m_vChildNodes[0]->OnSelect();
				else
					SigMoveSelection(1, this);
				break;
			case Keys::UP_ARROW:
				SigMoveSelection(-1, this);
				break;
			case Keys::LEFT_ARROW:
				if(m_bChildrenVisible && !m_vChildNodes.empty())
					SetNodesVisible(false);
				else
					SigSelectParent();
				break;
			case Keys::RIGHT_ARROW:
				if(!m_vChildNodes.empty())
				{
					if(!m_bChildrenVisible)
						SetNodesVisible(true);
					else
						m_vChildNodes[0]->OnSelect();
				}
				else
					SigMoveSelection(1, this);
				break;
			case Keys::DELETE_KEY:
				if(m_pCallback)
					m_pCallback->OnDelete();
			}
		}

		int TreeViewNode::GetTextOffset(void) const
		{
			int offset = 16;
			if(m_pIcon)
				offset += m_pIcon->GetWidth() + 8;
			return offset;
		}

		void TreeViewNode::OnRename(void)
		{
			if(!m_pRenameBox)
			{
				m_pRenameBox = new Textbox<>((float)(GetTextOffset() + GetHeight()*m_depth), 0.0f, m_stName.size() * 12.0f, 1.0f, m_stName);
				m_pRenameBox->SetPositionModes(PositionMode::ABS, PositionMode::REL, PositionMode::ABS, PositionMode::REL);
				m_pRenameBox->SigContentSet.Connect(this, &TreeViewNode::OnConfirmRename);
				m_pRenameBox->SigFocus.Connect(this, &TreeViewNode::OnRenameFocus);
				m_pRenameBox->SigKey.Connect(this, &TreeViewNode::OnRenameKey);
				AddChild(*m_pRenameBox);
				m_pRenameBox->OnFocus(true);
				return;
			}
			else if(!m_pRenameBox->IsVisible())
			{
				m_pRenameBox->SetText(m_stName);
				m_pRenameBox->OnFocus(true);
				m_pRenameBox->OnVisible();
				return;
			}
		}

		void TreeViewNode::OnToggleNodes(void)
		{
			size_t height;
			if(m_bChildrenVisible)
				height = GetTreeHeight();

			if(!m_vChildNodes.empty())
			{
				m_bChildrenVisible = !m_bChildrenVisible;

				for(auto pNode : m_vChildNodes)
				{
					pNode->SetVisible(m_bChildrenVisible);
				}

				if(m_bChildrenVisible)
					m_pExpander->Set(L"", math::Rect(96, 32, 8, 8));
				else
					m_pExpander->Set(L"", math::Rect(96, 24, 8, 8));
			}

			if(m_bChildrenVisible)
				height = GetTreeHeight();

			SigExpand(m_bChildrenVisible, this, height);
		}

		void TreeViewNode::OnExpandNode(bool bExand, TreeViewNode* pNode, int height)
		{
			if(height > 1)
			{
				int move;
				if(bExand)
					move = height - 1;
				else
					move = -((int)height - 1);

				if(!pNode->IsSelected() && pNode->HasSelectedNode())
					pNode->OnSelect();

				auto itr = std::find(m_vChildNodes.begin(), m_vChildNodes.end(), pNode);

				if(itr != m_vChildNodes.end())
				{
					itr++;
					while(itr != m_vChildNodes.end())
					{
						auto pNextNode = *itr;
						pNextNode->Move(move);

						itr++;
					}
				}

				SigHeightChanged(move, *this);
			}
		}

		void TreeViewNode::OnMoveChildSelection(int direction, TreeViewNode* pNode)
		{
			auto itr = std::find(m_vChildNodes.begin(), m_vChildNodes.end(), pNode);

			if(direction <= -1 && itr == m_vChildNodes.begin())
				OnSelect();
			else
			{
				itr += direction;

				if(itr != m_vChildNodes.end())
				{
					if(direction >= 1)
						(*itr)->OnSelect();
					else
						(*itr)->OnSelectLast();
				}
				else
					SigMoveSelection(direction, this);
			}
		}

		void TreeViewNode::OnHeightChanged(int dinstance, const TreeViewNode& node)
		{
			bool childFound = false;
			for(auto pChild : m_vChildNodes)
			{
				if(!childFound)
					childFound = pChild == &node;
				else
				{
					pChild->Move(dinstance);
				}
			}
		}

		void TreeViewNode::OnConfirmRename(std::wstring stName)
		{
			ACL_ASSERT(m_pCallback);
			m_pCallback->OnRename(stName);
			m_pRenameBox->OnFocus(false);
			m_pRenameBox->OnInvisible();
		}

		void TreeViewNode::OnRenameFocus(bool bFocus)
		{
			if(!bFocus)
			{
				m_pRenameBox->OnInvisible();
				OnFocus(true);
			}
		}

		void TreeViewNode::OnRenameKey(Keys key)
		{
			switch(key)
			{
			case Keys::ESC:
				m_pRenameBox->OnFocus(false);
				break;
			}
		}

	}
}