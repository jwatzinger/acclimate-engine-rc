#pragma once
#include "Widget.h"
#include "Textbox.h"

namespace acl
{
	namespace gui
	{

		class Icon;
		class ITreeNodeCallback;

		class TreeViewNode :
			public Widget
		{
			typedef std::vector<TreeViewNode*> ChildNodeVector;
		public:
			TreeViewNode(float y, float height, size_t depth, const std::wstring& stName, ITreeNodeCallback* pCallback);
			~TreeViewNode();

			void SetName(const std::wstring& stName);
			void SetIcon(const std::wstring& stIcon);
			void SetNodesVisible(bool bVisible);

			size_t GetTreeHeight(void) const;
			bool IsSelected(void) const;
			bool HasSelectedNode(void) const;
			const ITreeNodeCallback* GetCallback(void) const;

			TreeViewNode& AddNode(const std::wstring& stName, ITreeNodeCallback* pCallback);
			void Clear(void);
			void Move(int distance);

			void OnSelect(void);
			void OnSelectLast(void);
			void OnDeselect(void);
			void OnExpandNode(bool bExand, TreeViewNode* pNode, int height);
			void UpdateChildren(void) override;
			void OnRedraw(void) override;
			void OnClick(MouseType mouse) override;
			void OnRelease(MouseType mouse, bool bMouseOver) override;
			void OnVisible(void) override;
			void OnInvisible(void) override;
			void OnKeyDown(Keys key) override;

			core::Signal<TreeViewNode*> SigSelected;

		private:
#pragma warning( disable: 4251 )

			core::Signal<> SigSelectParent;
			core::Signal<int, TreeViewNode*> SigMoveSelection;
			core::Signal<int, const TreeViewNode&> SigHeightChanged;
			core::Signal<bool, TreeViewNode*, int> SigExpand;

			int GetTextOffset(void) const;
			
			void OnRename(void);
			void OnToggleNodes(void);
			void OnMoveChildSelection(int direction, TreeViewNode* pNode);
			void OnHeightChanged(int, const TreeViewNode& node);

			// rename widget
			void OnConfirmRename(std::wstring stName);
			void OnRenameFocus(bool bFocus);
			void OnRenameKey(Keys key);

			size_t m_depth;
			bool m_bSelected, m_bChildrenVisible;
			std::wstring m_stName;
			ITreeNodeCallback* m_pCallback;

			Icon* m_pExpander, *m_pIcon;
			Textbox<>* m_pRenameBox;
			ChildNodeVector m_vChildNodes;

#pragma warning( default: 4251 )
		};

	}
}


