#include "UserData.h"

namespace acl
{
	namespace gui
	{

		BaseUserData::Type BaseUserData::typeCount = 0;

		BaseUserData::Type BaseUserData::GenerateTypeId(void)
		{
			return typeCount++;
		}

	}
}

