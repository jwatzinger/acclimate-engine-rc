#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace gui
	{

		struct ACCLIMATE_API BaseUserData
		{
			typedef unsigned int Type;

			virtual ~BaseUserData(void) = 0 {}

			virtual Type GetType(void) const = 0;

		protected:
			static Type GenerateTypeId(void);

		private:
			static Type typeCount;
		};

		/*********************************
		* UserData
		**********************************/

		template <typename Derived>
		struct UserData :
			public BaseUserData
		{

			static Type type(void)
			{
				//increase base family count on first access
				static BaseUserData::Type Type = GenerateTypeId();
				return Type;
			}

			Type GetType(void) const override
			{
				return type();
			}

			virtual ~UserData(void) = 0 {};
		};

	}
}


