#include "Widget.h"
#include <algorithm>
#include "Module.h"
#include "BaseController.h"
#include "ContextMenu.h"
#include "Tooltip.h"
#include "IDragDropHandler.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace gui
	{

		Widget::Widget(float x, float y, float width, float height): m_x(x), m_y(y), m_width(width), m_height(height), m_aX(0), m_aY(0), m_absWidth(0),
			m_absHeight(0), m_bVisible(true), m_bDirty(true), m_bActive(false), m_bDisabled(false), m_bFocus(false), m_pParent(nullptr), 
			m_pParentController(nullptr), m_bClipping(false), m_focus(FocusState::DELEGATE_FOCUS), m_minW(16), m_maxW(0),
            m_minH(16), m_maxH(0), m_hAlign(HorizontalAlign::LEFT), m_vAlign(VerticalAlign::TOP), m_hCenter(HorizontalAlign::LEFT), 
            m_vCenter(VerticalAlign::TOP), m_padX(0), m_padY(0), m_padW(0), m_padH(0), m_borderRelation(BorderRelation::NONE), 
			m_pModule(nullptr), m_positionModeX(PositionMode::REL), m_positionModeY(PositionMode::REL), m_sizeModeWidth(PositionMode::REL), 
			m_sizeModeHeight(PositionMode::REL), m_bDirtyRect(true), m_aLastX(0), m_aLastY(0), m_absLastWidth(0), m_absLastHeight(0),
			m_pContextMenu(nullptr), m_pShortcut(nullptr), m_pTooltip(nullptr), m_order(0), m_borderRelationFactor(1.0f), m_pDragDrop(nullptr),
			m_isDragging(false), m_pUserData(nullptr)
		{
		}

		Widget::~Widget(void)
		{
			for(auto pWidget : m_vChildren)
			{
                pWidget->m_pParent = nullptr;
			}

            if(m_pParent)
                m_pParent->RemoveChild(*this);

            if(m_pParentController)
                m_pParentController->RemoveWidget(*this);

			if(m_pModule)
			{
				if(m_bVisible)
					m_pModule->AddDirtyRect(math::Rect(GetX(), GetY(), GetWidth(), GetHeight()));
				m_pModule->UnregisterWidget(*this);

				if(m_pShortcut)
				{
					m_pModule->UnregisterShortcut(*m_pShortcut);
					delete m_pShortcut;
				}
			}

			delete m_pTooltip;
			delete m_pUserData;
		}

		void Widget::AddChild(Widget& child)
		{
			Widget* pParent = child.GetParent();

			if(pParent)
				pParent->RemoveChild(child);

			child.m_pParent = this;
		
			if(child.m_pModule != m_pModule)
			{
				if(m_pModule)
					m_pModule->RegisterWidget(child);
				else
					child.Unregister();
			}

			if(m_pModule)
				child.OnParentUpdate();

			//add child to list & connect close
			for(auto ii = m_vChildren.begin(); ii != m_vChildren.end(); ++ii)
			{
				if((*ii)->GetOrder() > child.GetOrder())
				{
					m_vChildren.insert(ii, &child);
					return;
				}
			}
			m_vChildren.push_back(&child);
		}

		void Widget::RemoveChild(Widget& child)
		{
			WidgetVector::iterator ii = std::find(m_vChildren.begin(), m_vChildren.end(), &child);
			if (ii != m_vChildren.end())
            {
				m_vChildren.erase(ii);

				child.m_pParent = nullptr;
            }
		}

		void Widget::MarkDirtyRect(void)
		{
			if(m_bDirtyRect != true)
			{
				if(!m_bClipping)
				{
					for(auto pChild : m_vChildren)
					{
						pChild->MarkDirtyRect();
					}
				}
				m_bDirtyRect = true;
			}
		}

		bool Widget::Over(math::Vector2 vMousePos) const
		{
			return vMousePos.x >= GetX() && vMousePos.x <= GetX()+GetWidth() && vMousePos.y >= GetY() && vMousePos.y <= GetY()+GetHeight();
		}

		void Widget::Unregister(void)
		{
			if(m_pModule)
			{
				for(auto pChild : m_vChildren)
				{
					pChild->Unregister();
				}
			}

			m_pModule = nullptr;
		}

		void Widget::Free(void)
		{
			m_pModule->SetAsFree(*this);
		}

#pragma region getter/setter
		void Widget::SetX(float x)
		{
			m_x = x;
		}

		void Widget::SetY(float y)
		{
			m_y = y;
		}

		void Widget::SetWidth(float width)
		{
			m_width = width;
			m_bDirty = true;
		}

		void Widget::SetHeight(float height)
		{
			m_height = height;
			m_bDirty = true;
		}

		void Widget::SetOrder(int order)
		{
			if(order != m_order)
			{
				m_order = order;
				SortChildren();
			}
		}

		void Widget::SetParent(Widget* pParent)
		{
            if(pParent)
                pParent->AddChild(*this);
            else 
            {
                if(m_pParent)
                    m_pParent->RemoveChild(*this);
            }

			m_bDirty = true;
		}

        void Widget::SetParentController(BaseController* pController)
        {
            m_pParentController = pController;

			if(pController)
			{
				auto pModule = &pController->GetModule();
				if(m_pModule != pModule)
					pModule->RegisterWidget(*this);
			}
        }

		void Widget::SetFocusState(FocusState state)
        {
			m_focus = state;
        }

        void Widget::SetCapMetrics(int minW, int maxW, int minH, int maxH)
        {
            m_minW = minW;
            m_maxW = maxW;
            m_minH = minH;
            m_maxH = maxH;
        }

        void Widget::SetAlignement(HorizontalAlign hAlign, VerticalAlign vAlign)
        {
            m_hAlign = hAlign;
            m_vAlign = vAlign;
        }

        void Widget::SetCenter(HorizontalAlign hCenter, VerticalAlign vCenter)
        {
            m_hCenter = hCenter;
            m_vCenter = vCenter;
        }

        void Widget::SetPadding(int x, int y, int w, int h)
        {
            m_padX = x;
            m_padY = y;
            m_padW = w;
            m_padH = h;
        }

		void Widget::SetPadding(PaddingSide side, int value)
		{
			switch(side)
			{
			case PaddingSide::LEFT:
				m_padX = value;
				break;
			case PaddingSide::RIGHT:
				m_padW = value;
				break;
			case PaddingSide::TOP:
				m_padY = value;
				break;
			case PaddingSide::BOTTOM:
				m_padH = value;
				break;
			default:
				ACL_ASSERT(false);
			}
		}

        void Widget::SetBorderRelation(BorderRelation relation, float factor)
        {
			if(m_borderRelation != relation || m_borderRelationFactor != factor)
			{
				m_borderRelation = relation;
				m_borderRelationFactor = factor;
				m_bDirty = true;
			}
        }

		void Widget::SetBorderRelation(BorderRelation relation)
		{
			if(m_borderRelation != relation)
			{
				m_borderRelation = relation;
				m_bDirty = true;
			}
		}

		void Widget::SetClipping(bool bClip)
		{
			m_bDirty = m_bClipping != bClip;
			m_bClipping = bClip;
		}

		void Widget::SetPositionModes(PositionMode x, PositionMode y, PositionMode width, PositionMode height)
		{
			m_positionModeX = x;
			m_positionModeY = y;
			m_sizeModeWidth = width;
			m_sizeModeHeight = height;
			m_bDirty = true;
		}

		void Widget::SetPositionMode(Position pos, PositionMode mode)
		{
			switch(pos)
			{
			case Position::X:
				m_bDirty |= m_positionModeX != mode;
				m_positionModeX = mode;
				break;
			case Position::WIDTH:
				m_bDirty |= m_sizeModeWidth != mode;
				m_sizeModeWidth = mode;
				break;
			case Position::Y:
				m_bDirty |= m_positionModeY != mode;
				m_positionModeY = mode;
				break;
			case Position::HEIGHT:
				m_bDirty |= m_sizeModeHeight != mode;
				m_sizeModeHeight = mode;
				break;
			default:
				ACL_ASSERT(false);
			}
		}

		void Widget::SetVisible(bool bVisible)
		{
			if(bVisible && !m_bVisible)
				OnVisible();
			else if(!bVisible && m_bVisible)
				OnInvisible();
		}

		void Widget::SetContextMenu(ContextMenu* pMenu)
		{
			m_pContextMenu = pMenu;
		}

		void Widget::SetEnabled(bool bEnabled)
		{
			if(m_bDisabled && bEnabled)
				OnEnable();
			else if(!m_bDisabled && !bEnabled)
				OnDisable();
		}

		void Widget::SetTooltip(const std::wstring& stTooltip)
		{
			if(stTooltip.empty())
				delete m_pTooltip;
			else
			{
				if(!m_pTooltip)
				{
					m_pTooltip = new Tooltip(stTooltip);
					AddChild(*m_pTooltip);
				}
				else
					m_pTooltip->SetText(stTooltip);
			}
		}

		void Widget::SetDragDrop(IDragDropHandler* pHandler)
		{
			m_pDragDrop = pHandler;
		}

		void Widget::SetUserData(BaseUserData& userData)
		{
			if(m_pUserData != &userData)
			{
				delete m_pUserData;
				m_pUserData = &userData;
			}
		}

		int Widget::GetX(void) const
		{
			return m_aX;
		}

		int Widget::GetY(void) const
		{
			return m_aY;
		}

		int Widget::GetWidth(void) const
		{
			return m_absWidth;
		}

		int Widget::GetHeight(void) const
		{
			return m_absHeight;
		}

		int Widget::GetOrder(void) const
		{
			return m_order;
		}

		float Widget::GetRelX(void) const
		{
			return m_x;
		}

		float Widget::GetRelY(void) const
		{
			return m_y;
		}

		float Widget::GetRelWidth(void) const
		{
			return m_width;
		}

		float Widget::GetRelHeight(void) const
		{
			return m_height;
		}

		bool Widget::IsVisible(void) const
		{
			return m_bVisible;
		}

        bool Widget::IsClipping(void) const
        {
            return m_bClipping;
        }

		bool Widget::GetDisable(void) const
		{
			return m_bDisabled;
		}

		bool Widget::GetFocus(void) const
		{
			return m_bFocus;
		}

		bool Widget::HasUserData(void) const
		{
			return m_pUserData != nullptr;
		}

		bool VisibleChain(const Widget& widget)
		{
			if(widget.IsVisible())
			{
				auto pParent = widget.GetParent();
				if(pParent)
					return VisibleChain(*pParent);
				else
					return true;
			}
			else
				return false;
		}

		bool Widget::CanDoShortcut(void) const
		{
			if(m_pShortcut && !m_bDisabled)
			{
				switch(m_pShortcut->state)
				{
				case ShortcutState::ALWAYS:
					return true;
				case ShortcutState::VISIBLE:
					return VisibleChain(*this);
				case ShortcutState::FOCUS:
					return GetFocus();
				}
			}

			return false;
		}

		const std::vector<Widget*>& Widget::GetChildren(void) const
		{
			return m_vChildren;
		}

		Widget* Widget::GetParent(void) const
		{
			return m_pParent;
		}

		Module* Widget::GetModule(void) const
		{
			return m_pModule;
		}

		const Shortcut* Widget::GetShortcut(void) const
		{
			return m_pShortcut;
		}

		const std::vector<RenderData>& Widget::GetRenderData(void) const
		{
			return m_vRenderData;
		}

#pragma endregion

		void Widget::SetTop(Widget& child)
		{
			std::vector<Widget*>::const_iterator ii = std::find(m_vChildren.cbegin(), m_vChildren.cend(), &child);
			if(ii != m_vChildren.cend() && ii != m_vChildren.cend() - 1)
			{
				const int order = child.GetOrder();
				m_vChildren.erase(ii);
				for(unsigned int i = 0; i < m_vChildren.size(); i++)
				{
					if(order < m_vChildren[i]->GetOrder())
					{
						m_vChildren.insert(m_vChildren.begin() + i, &child);
						return;
					}
				}

				m_vChildren.push_back(&child);
			}
		}

		void Widget::SetBottom(Widget& child)
		{
			std::vector<Widget*>::const_iterator ii = std::find(m_vChildren.cbegin(), m_vChildren.cend(), &child);
			if(ii != m_vChildren.cend() && ii != m_vChildren.cbegin())
			{
				const int order = child.GetOrder();
				m_vChildren.erase(ii);

				for(int i = m_vChildren.size(); i >= 0; i--)
				{
					if(order >= m_vChildren[i]->GetOrder())
					{
						m_vChildren.insert(m_vChildren.begin() + i+1, &child);
						return;
					}
				}

				m_vChildren.insert(m_vChildren.begin(), &child);
			}
		}

		void Widget::SetShortcut(ComboKeys keys, char key, ShortcutState state)
		{
			if(m_pShortcut)
			{
				if(m_pModule)
					m_pModule->UnregisterShortcut(*m_pShortcut);
				delete m_pShortcut;
			}

			m_pShortcut = new Shortcut{ keys, key, state };

			if(m_pModule)
				m_pModule->RegisterShortcut(*m_pShortcut, *this);
		}

        void Widget::PushToTop(void)
        {
            if(m_pParent)
            {
                m_pParent->SetTop(*this);
                m_pParent->PushToTop();
            }
        }

		void Widget::PushToBottom(void)
        {
            if(m_pParent)
            {
                m_pParent->SetBottom(*this);
                m_pParent->PushToBottom();
            }
        }

		void Widget::OnClick(MouseType mouse)
		{
			if(mouse == MouseType::LEFT)
				SigClicked();

			OnActivate(true);
			AquireFocus();
		}

		void Widget::OnDoubleClick(void)
		{
		}

		void Widget::OnMoveOn(void)
		{
			SigChangeCursorState(CursorState::IDLE);

			SigMoveOn();

			if(m_pTooltip)
				m_pTooltip->Show();
		}

		void Widget::OnMoveOff(void)
		{
			SigMoveOff();

			if(m_pTooltip)
				m_pTooltip->Hide();
		}

        void Widget::OnHover(void)
        {
            SigHover();
        }

		void Widget::OnDrag(const math::Vector2& vDistance)
		{
			if(m_pDragDrop)
			{
				if(m_isDragging)
					m_pDragDrop->OnDrag(m_pModule->m_cursor.GetPosition());
				else
					m_pDragDrop->OnBegin(*this);

				m_isDragging = true;
			}
			SigDrag(vDistance);
		}
		
		void Widget::OnMove(const math::Vector2& vMousePos)
		{
			m_vLastMousePos = vMousePos;
			SigMouseMove(vMousePos - math::Vector2(GetX(), GetY()));
		}

		void Widget::OnRelease(MouseType mouse, bool bMouseOver)
		{
			if(m_pDragDrop && m_isDragging)
			{
				auto& vPosition = m_pModule->m_cursor.GetPosition();
				auto pWidget = m_pModule->GetWidget(vPosition, true);
				m_pDragDrop->OnDrop(*pWidget);
				m_isDragging = false;
			}

			if(!m_bActive)
				return;

			OnActivate(false);

			if(mouse == MouseType::LEFT)
				SigReleased();
			else if(mouse == MouseType::RIGHT && m_pContextMenu)
				m_pContextMenu->Execute(m_vLastMousePos);
		}

		void Widget::OnHold(bool bMouseOver)
		{
		}

		void Widget::OnWheelMove(int distance)
		{
			SigWheelMoved(distance);
		}

		void Widget::OnDisable(void)
		{
			m_bDisabled = true;
		}

		void Widget::OnEnable(void)
		{
			m_bDisabled = false;
		}

		void Widget::OnClose(void)
		{
			if(IsVisible())
			{
				OnActivate(false);
				OnInvisible();
				SigClose();
			}
		}

		void Widget::OnActivate(bool bActive)
		{
			if(m_bActive != bActive)
			{
				m_bActive = bActive;
				if(m_bActive)
					m_pModule->SetActiveWidget(this);
				else
					m_pModule->SetActiveWidget(nullptr);
				SigActivate(bActive);
			}
		}

		void Widget::OnFocus(bool bFocus)
		{
			if(m_bFocus != bFocus)
			{
				m_bFocus = bFocus;
				if(m_bFocus)
					m_pModule->SetFocusWidget(this);
				else
					m_pModule->SetFocusWidget(nullptr);

				SigFocus(m_bFocus);

				m_bDirty = true;
			}
		}

		void Widget::OnRedraw(void)
		{
		}

		void Widget::OnVisible(void)
		{
			if(!m_bVisible)
			{
				m_bVisible = true;
				MarkDirtyRect();
				SigVisible();
			}
		}

		void Widget::OnInvisible(void)
		{
			if(m_bVisible)
			{
				m_bVisible = false;
				MarkDirtyRect();
				OnActivate(false);
				SigInvisible();
			}
		}

		void Widget::UpdateChildren(void)
		{
			for(auto pWidget : m_vChildren)
            {
                pWidget->OnParentUpdate();
            }
		}

		void Widget::OnParentUpdate(void)
		{
			if(m_pParent)
				OnParentResize(m_pParent->GetX(), m_pParent->GetY(), m_pParent->GetWidth(), m_pParent->GetHeight());
			else
			{
				const math::Vector2& vScreenSize = m_pModule->GetScreenSize();
				OnParentResize(0, 0, vScreenSize.x, vScreenSize.y);
			}

			UpdateChildren();

			//skip if nothing changed
			if(IsVisible())
			{
				if(m_bDirty)
				{
					OnRedraw();
					m_bDirtyRect = true;
					m_bDirty = false;
				}

				if(m_bDirtyRect && (m_absWidth || m_absHeight))
				{
					m_pModule->AddDirtyRect(math::Rect(m_aX, m_aY, m_absWidth, m_absHeight));
					m_bDirtyRect = false;
				}
			}

		}

		void Widget::OnKeyStroke(WCHAR key)
		{
		}

		void Widget::OnKeyDown(Keys key)
		{
			SigKey(key);
		}

		void Widget::OnParentResize(int x, int y, int width, int height)
		{
			int posWidth = width, posHeight = height;

			const math::Vector2& vScreenSize = m_pModule->GetScreenSize(); 
			const math::Vector2& vReferenceSize = m_pModule->m_layout.GetReferenceSize(); 

			switch(m_positionModeX)
			{
			case PositionMode::SCREEN:
				{
					posWidth = vScreenSize.x;
					break;
				}
			case PositionMode::REF:
				{
					posWidth = vReferenceSize.x;
					break;
				}
			case PositionMode::ABS:
				{
					posWidth = 1;
					break;
				}
			}

			switch(m_positionModeY)
			{
			case PositionMode::SCREEN:
				{
					posHeight = vScreenSize.y;
					break;
				}
			case PositionMode::REF:
				{
					posHeight = vReferenceSize.y;
					break;
				}
			case PositionMode::ABS:
				{
					posHeight = 1;
					break;
				}
			}

			switch(m_sizeModeWidth)
			{
			case PositionMode::SCREEN:
				{
					width = vScreenSize.x;
					break;
				}
			case PositionMode::REF:
				{
					width = vReferenceSize.x;
					break;
				}
			case PositionMode::ABS:
				{
					width = 1;
					break;
				}
			}

			switch(m_sizeModeHeight)
			{
			case PositionMode::SCREEN:
				{
					height = vScreenSize.y;
					break;
				}
			case PositionMode::REF:
				{
					height = vReferenceSize.y;
					break;
				}
			case PositionMode::ABS:
				{
					height = 1;
					break;
				}
			}

            float xCenter = m_x;
            switch(m_hAlign)
            {
            case HorizontalAlign::CENTER:
				switch(m_positionModeX)
				{
				case PositionMode::REL:
					xCenter += 0.5f;
					break;
				case PositionMode::SCREEN:
					xCenter += m_pModule->GetScreenSize().x / 2.0f;
					break;
				case PositionMode::ABS:
					xCenter += m_pParent->GetWidth() / 2.0f;
					break;
				case PositionMode::REF:
					xCenter += m_pModule->m_layout.GetReferenceSize().x / 2.0f;
					break;
				}
                break;
            case HorizontalAlign::RIGHT:
				switch(m_positionModeX)
				{
				case PositionMode::REL:
					xCenter += 1.0f;
					break;
				case PositionMode::SCREEN:
					xCenter += m_pModule->GetScreenSize().x;
					break;
				case PositionMode::ABS:
					xCenter += m_pParent->GetWidth();
					break;
				case PositionMode::REF:
					xCenter += m_pModule->m_layout.GetReferenceSize().x;
					break;
				}
                break;
            }
            m_aX = (int)(xCenter * posWidth) + x + m_padX;

            float yCenter = m_y;
            switch(m_vAlign)
            {
            case VerticalAlign::CENTER:
				switch(m_positionModeY)
				{
				case PositionMode::REL:
					yCenter += 0.5f;
					break;
				case PositionMode::SCREEN:
					yCenter += m_pModule->GetScreenSize().y / 2.0f;
					break;
				case PositionMode::ABS:
					yCenter += m_pParent->GetHeight() / 2.0f;
					break;
				case PositionMode::REF:
					yCenter += m_pModule->m_layout.GetReferenceSize().y / 2.0f;
					break;
				}
                break;
            case VerticalAlign::BOTTOM:
				switch(m_positionModeY)
				{
				case PositionMode::REL:
					yCenter += 1.0f;
					break;
				case PositionMode::SCREEN:
					yCenter += m_pModule->GetScreenSize().y;
					break;
				case PositionMode::ABS:
					yCenter += m_pParent->GetHeight();
					break;
				case PositionMode::REF:
					yCenter += m_pModule->m_layout.GetReferenceSize().y;
					break;
				}
                break;
            }
            m_aY = (int)(yCenter * posHeight) + y + m_padY;

			m_absWidth = (int)(m_width * width) - m_padW;
			m_absHeight = (int)(m_height * height)  - m_padH;

			if(m_maxW)
				m_absWidth = max(m_minW, min(m_maxW, m_absWidth));
			else
				m_absWidth = max(m_minW, m_absWidth);

			if(m_maxH)
				m_absHeight = max(m_minH, min(m_maxH, m_absHeight));
			else
				m_absHeight = max(m_minH, m_absHeight);

            switch(m_borderRelation)
            {
            case BorderRelation::WIDTH_FROM_HEIGHT:
                m_absWidth = (int)(m_absHeight * m_borderRelationFactor);
                break;
            case BorderRelation::HEIGHT_FROM_WIDTH:
				m_absHeight = (int)(m_absWidth * m_borderRelationFactor);
                break;
            }

            switch(m_hCenter)
            {
            case HorizontalAlign::CENTER:
                m_aX -= m_absWidth / 2;
                break;
            case HorizontalAlign::RIGHT:
                m_aX -= m_absWidth;
                break;
            }

            switch(m_vCenter)
            {
            case VerticalAlign::CENTER:
                m_aY -= m_absHeight / 2;
                break;
            case VerticalAlign::BOTTOM:
                m_aY -= m_absHeight;
                break;
            }

			if(!m_pModule)
				return;

			bool bSizeChanged = m_absWidth != m_absLastWidth || m_absHeight != m_absLastHeight;
			if(bSizeChanged || m_aX != m_aLastX || m_aY != m_aLastY)
			{
				m_bDirty = m_bDirty | bSizeChanged;
				if(m_absLastWidth && m_absLastHeight)
					m_pModule->AddDirtyRect(math::Rect(m_aLastX, m_aLastY, m_absLastWidth, m_absLastHeight));
				m_pModule->AddDirtyRect(math::Rect(m_aX, m_aY, m_absWidth, m_absHeight));
				m_absLastWidth = m_absWidth;
				m_absLastHeight = m_absHeight;
				m_aLastX = m_aX;
				m_aLastY = m_aY;
			}
		}

		void Widget::OnRegister(Module& module)
		{
			if(m_pModule != &module)
			{
				if(m_pModule && m_pShortcut)
					m_pModule->UnregisterShortcut(*m_pShortcut);

				m_pModule = &module;

				if(m_pModule && m_pShortcut)
					m_pModule->RegisterShortcut(*m_pShortcut, *this);

				for(auto pChild : m_vChildren)
				{
					if(pChild->m_pModule != &module)
						m_pModule->RegisterWidget(*pChild);
				}			
			}
		}

		void Widget::OnExecute(void)
		{
		}

		void Widget::OnLayoutChanged(const Layout& layout)
		{
		}

		void Widget::OnShortcut(void)
		{
			SigShortcut();
		}

        void Widget::AquireFocus(void)
        {
			switch(m_focus)
			{
			case FocusState::DELEGATE_FOCUS:
				if(m_pParent)
					m_pParent->AquireFocus();
				break;
			case FocusState::KEEP_FOCUS:
				OnFocus(true);
				break;
			}
        }

		void Widget::SortChildren(void)
		{
			std::sort(m_vChildren.begin(), m_vChildren.end(), ChildrenSorter());
		}

	}
}