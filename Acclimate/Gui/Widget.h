#pragma once
#include <vector>
#include "Shortcut.h"
#include "Cursor.h"
#include "UserData.h"
#include "..\Core\Dll.h"
#include "..\Core\Signal.h"
#include "..\Gfx\Color.h"
#include "..\Math\Rect.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

	namespace gui
	{

		struct RenderData
		{
			RenderData(void): bCrop(true), stLabel(L""), stFileName(L""), pTexture(nullptr), labelFlags(DT_CENTER | DT_VCENTER),
				labelColor(255, 255, 255), textSize(0)
			{
			}

            //image crop flag
            bool bCrop;
			//texture-Src and destination-draw rect 
			math::Rect rSrcRect, rDestRect;
			//optional texture
			const gfx::ITexture* pTexture;
			//optional string
			std::wstring stLabel;
			unsigned int labelFlags, textSize;
            gfx::Color labelColor;
			//optional file name
			std::wstring stFileName;
		};

        enum class HorizontalAlign
        {
            LEFT, CENTER, RIGHT
        };

        enum class VerticalAlign
        {
            TOP, CENTER, BOTTOM
        };

        enum class BorderRelation
        {
            NONE, HEIGHT_FROM_WIDTH, WIDTH_FROM_HEIGHT
        };

		enum class PositionMode
		{
			REL, SCREEN, REF, ABS
		};

		enum class MouseType
		{
			LEFT, RIGHT
		};

		enum class FocusState
		{
			IGNORE_FOCUS,
			DELEGATE_FOCUS,
			KEEP_FOCUS
		};

		enum class PaddingSide
		{
			LEFT, RIGHT, TOP, BOTTOM
		};

		enum class Position
		{
			X, Y, WIDTH, HEIGHT
		};

		enum class Keys;

#pragma warning( disable: 4251 )
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<Keys>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<bool>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<int>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const math::Vector2&>;
		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<CursorState>;
#pragma warning( default: 4251 )
		EXPORT_TEMPLATE template class ACCLIMATE_API std::vector<RenderData>;

		class Widget;
		EXPORT_TEMPLATE template class ACCLIMATE_API std::vector<Widget*>;


		class Module;
		class Layout;
        class BaseController;
		class ContextMenu;
		class Tooltip;
		class IDragDropHandler;

		class ACCLIMATE_API Widget
		{
            typedef std::vector<Widget*> WidgetVector;

		public:
			Widget(float x, float y, float width, float height);
			virtual ~Widget(void);

			void MarkDirtyRect(void);
			bool Over(math::Vector2 vMousePosition) const;
			virtual void UpdateChildren(void);
			void Unregister(void);
			void Free(void);

			virtual void AddChild(Widget& child);
			virtual void RemoveChild(Widget& child);

			void SetX(float x);
			void SetY(float y);
			void SetWidth(float width);
			void SetHeight(float height);
			void SetOrder(int order);
			void SetParent(Widget* pParent);
			void SetTop(Widget& child);
			void SetBottom(Widget& child);
            void PushToTop(void);
			void PushToBottom(void);
            void SetParentController(BaseController* pController);
			void SetFocusState(FocusState state);
            void SetCapMetrics(int minW, int maxW, int minH, int maxH);
            void SetAlignement(HorizontalAlign hAlign, VerticalAlign vAlign);
            void SetCenter(HorizontalAlign hCenter, VerticalAlign vCenter);
            void SetPadding(int x, int y, int w, int h);
			void SetPadding(PaddingSide side, int value);
            void SetBorderRelation(BorderRelation relation, float factor);
			void SetBorderRelation(BorderRelation relation);
			virtual void SetClipping(bool bClip);
			void SetPositionModes(PositionMode x, PositionMode y, PositionMode width, PositionMode height);
			void SetPositionMode(Position pos, PositionMode mode);
			void SetVisible(bool bVisible);
			virtual void SetContextMenu(ContextMenu* pMenu);
			void SetEnabled(bool bEnabled);
			void SetShortcut(ComboKeys keys, char key, ShortcutState state);
			void SetTooltip(const std::wstring& stTooltip);
			void SetDragDrop(IDragDropHandler* pHandler);
			void SetUserData(BaseUserData& userData);

			int GetX(void) const;
			int GetY(void) const;
			int GetWidth(void) const;
			int GetHeight(void) const;
			int GetOrder(void) const;
			float GetRelX(void) const;
			float GetRelY(void) const;
			float GetRelWidth(void) const;
			float GetRelHeight(void) const;
			bool IsVisible(void) const;
            bool IsClipping(void) const;
			bool GetDisable(void) const;
			bool GetFocus(void) const;
			bool CanDoShortcut(void) const;
			Widget* GetParent(void) const;
			Module* GetModule(void) const;
			const Shortcut* GetShortcut(void) const;
			const std::vector<Widget*>& GetChildren(void) const;
			const std::vector<RenderData>& GetRenderData(void) const;

			template<typename DataType>
			DataType* GetUserData(void) const;
			bool HasUserData(void) const;

			//signals
			core::Signal<> SigClose;
			core::Signal<> SigClicked;
			core::Signal<> SigReleased;
			core::Signal<> SigVisible;
			core::Signal<> SigInvisible;
            core::Signal<> SigHover;
			core::Signal<> SigShortcut;
			core::Signal<> SigMoveOn;
			core::Signal<> SigMoveOff;
			core::Signal<Keys> SigKey;
			core::Signal<bool> SigActivate;
			core::Signal<bool> SigFocus;
			core::Signal<int> SigWheelMoved;
			core::Signal<const math::Vector2&> SigMouseMove; //projected mouse position
			core::Signal<const math::Vector2&> SigDrag;
			core::Signal<CursorState> SigChangeCursorState;

			//events
			virtual void OnClick(MouseType mouse);					//mouse clicked
			virtual void OnDoubleClick(void);                       //mouse double clicked
			virtual void OnHold(bool bMouseOver);					//mouse hold
			virtual void OnDrag(const math::Vector2& vDistance);	//mouse hold & moved
			virtual void OnMove(const math::Vector2& vMousePos);	//mouse moved
			virtual void OnRelease(MouseType mouse, bool bMouseOver); //mouse released
			virtual void OnMoveOn(void);							//mouse moved on
			virtual void OnMoveOff(void);							//mouse moved off
            virtual void OnHover(void);
			virtual void OnWheelMove(int distance);                 //mouse wheel moved
			virtual void OnKeyStroke(WCHAR key);					//literal key stroke
			virtual void OnKeyDown(Keys key);						//key down
			virtual void OnActivate(bool bActive);					//widget activity altered
			virtual void OnFocus(bool bFocus);						//widget keyboard focus altered
			virtual void OnClose(void);								//widget closed
			virtual void OnRedraw(void);							//widget needs to be redrawn
			virtual void OnDisable(void);							//widget gets disabled
			virtual void OnEnable(void);							//widget gets enabled
			virtual void OnVisible(void);
			virtual void OnInvisible(void);
			virtual void OnParentUpdate(void);		        //parent widget got update
			virtual void OnParentResize(int x, int y, int width, int height);	//parent widget got resized
			virtual void OnRegister(Module& module);
			virtual void OnExecute(void);
			virtual void OnLayoutChanged(const Layout& layout);
			virtual void OnShortcut(void);

		protected:

            void AquireFocus(void);
			void SortChildren(void);

			Module* m_pModule;

			// position & size attributes
			float m_x, m_y, m_width, m_height, m_borderRelationFactor;
			// absolute position
			int m_aX, m_aY, m_absWidth, m_absHeight, m_aLastX, m_aLastY, m_absLastWidth, m_absLastHeight;
            // caps
            int m_minW, m_maxW, m_minH, m_maxH;
            // padding
            int m_padX, m_padY, m_padW, m_padH;
			int m_order;
			// state attributes
			// todo: make flags
			bool m_bVisible, m_bActive, m_bDisabled, m_bDirty, m_bFocus, m_bClipping, m_bDirtyRect, m_isDragging;
			math::Vector2 m_vLastMousePos;
            // alignement
            HorizontalAlign m_hAlign, m_hCenter;
            VerticalAlign m_vAlign, m_vCenter;
            // border
			FocusState m_focus;
            BorderRelation m_borderRelation;
			// position
			PositionMode m_positionModeX, m_positionModeY, m_sizeModeWidth, m_sizeModeHeight;

			Widget* m_pParent;
            BaseController* m_pParentController;
			ContextMenu* m_pContextMenu;
			Tooltip* m_pTooltip;
			Shortcut* m_pShortcut;
			IDragDropHandler* m_pDragDrop;
			BaseUserData* m_pUserData;

			std::vector<RenderData> m_vRenderData;
			WidgetVector m_vChildren;

		private:

			struct ChildrenSorter
			{
				inline bool operator()(const Widget* pLeft, const Widget* pRight)
				{
					return pLeft->GetOrder() > pRight->GetOrder();
				}
			};

		};

		template<typename DataType>
		DataType* Widget::GetUserData(void) const
		{
			if(m_pUserData)
			{
				if(DataType::type() == m_pUserData->GetType())
					return (DataType*)m_pUserData;
				else
					return nullptr;
			}
			else
				return nullptr;
		}

	}
}