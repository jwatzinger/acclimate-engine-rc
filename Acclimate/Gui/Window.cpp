#include "Window.h"
#include "ToolButton.h"
#include "Icon.h"
#include "Module.h"
#include "Input.h"

namespace acl
{
	namespace gui
	{

		Window::Window(float x, float y, float width, float height, const std::wstring& stName) : BaseWindow(x, y, width, height, stName),
			m_topArea(0.0f, 0.0f, 1.0f, m_topBorder)
        {
			// area 
			m_topArea.SetClipping(false);
			m_topArea.SetPositionMode(Position::HEIGHT, PositionMode::ABS);
			m_topArea.SigDrag.Connect(this, &Window::OnDrag);
			m_topArea.SigMouseMove.Connect(this, &Window::OnMoveTop);
			m_topArea.SigReleased.Connect(this, &Window::OnTopRelease);
			Widget::AddChild(m_topArea);
			
	        //close button
	        Icon* pIcon = new Icon(0.0f, 0.0f, -1.0f, L"", math::Rect(56, 24, 8, 8));
			pIcon->SetCapMetrics(8, 8, 8, 8);
	        m_pClose = new ToolButton(1.0f, 0.0f, pIcon);
			m_pClose->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
			m_pClose->SetPadding(-36, 0, 0, 0);
			m_pClose->SetCapMetrics(32, 32, 14, 14);
	        m_pClose->SigClicked.Connect(this, &BaseWindow::OnClose);
			m_pClose->SetBorderRelation(gui::BorderRelation::WIDTH_FROM_HEIGHT, 2.0f);
	        m_topArea.AddChild(*m_pClose);

	        ////maximize button
	        pIcon = new Icon(0.0f, 0.0f, -1.0f, L"", math::Rect(56, 40, 8, 8));
			pIcon->SetCapMetrics(8, 8, 8, 8);
	        m_pMaximize = new ToolButton(1.0f, 0.0f, pIcon);
			m_pMaximize->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
			m_pMaximize->SetPadding(-64, 0, 0, 0);
			m_pMaximize->SetCapMetrics(14, 14, 14, 14);
	        m_pMaximize->SigClicked.Connect(this, &Window::OnMaximize);
	        this->SigMaximized.Connect(m_pMaximize, &ToolButton::OnDisable);
	        this->SigShrinked.Connect(m_pMaximize, &ToolButton::OnEnable);
	        this->SigMaximized.Connect(m_pMaximize, &ToolButton::OnInvisible);
	        this->SigShrinked.Connect(m_pMaximize, &ToolButton::OnVisible);
	        m_topArea.AddChild(*m_pMaximize);

	        ////shrink button
	        pIcon = new Icon(0.0f, 0.0f, 1.0f, L"", math::Rect(56, 32, 8, 8));
			pIcon->SetCapMetrics(8, 8, 8, 8);
	        m_pShrink = new ToolButton(1.0f, 0.0f, pIcon);
			m_pShrink->SetCenter(HorizontalAlign::RIGHT, VerticalAlign::TOP);
			m_pShrink->SetPadding(-64, 0, 0, 0);
			m_pShrink->SetCapMetrics(14, 14, 14, 14);
	        m_pShrink->OnDisable();
	        m_pShrink->OnInvisible();
	        m_pShrink->SigClicked.Connect(this, &Window::OnShrink);
	        this->SigMaximized.Connect(m_pShrink, &ToolButton::OnEnable);
	        this->SigShrinked.Connect(m_pShrink, &ToolButton::OnDisable);
	        this->SigMaximized.Connect(m_pShrink, &ToolButton::OnVisible);
	        this->SigShrinked.Connect(m_pShrink, &ToolButton::OnInvisible);
	        m_topArea.AddChild(*m_pShrink);

	        //initialize attributes
	        m_pIcon = nullptr;
			SetCapMetrics(256, 2560, 256, 1600);
        }

		Window::~Window(void)
		{
			delete m_pClose;
			delete m_pMaximize;
			delete m_pShrink;
			Widget::RemoveChild(*m_pIcon);
			delete m_pIcon;
		}

		void Window::SetIcon(const std::wstring& stFileName, int srcX, int srcY, int srcWidth, int srcHeight)
        {
	        //delete existing icon
			if(m_pIcon)
			{
				Widget::RemoveChild(*m_pIcon);
				delete m_pIcon;
			}

	        //create new icon
	        m_pIcon = new Icon(0.0f, 0.0f, 24.0f, stFileName, math::Rect(srcX, srcY, srcWidth, srcHeight));
			m_pIcon->SetPositionMode(Position::WIDTH, PositionMode::ABS);
			m_pIcon->SetPositionMode(Position::HEIGHT, PositionMode::ABS);
	        Widget::AddChild(*m_pIcon);
        }

        void Window::OnDoubleClick(void)
        {
	        Widget::OnDoubleClick();
	        if(m_vLastMousePos.y - GetY() <= 24)
	        {
		        if(HasStyleFlags(Maximized))
			        OnShrink();
		        else
			        OnMaximize();
	        }
        }

        void Window::OnShrink(void)
        {
			if(!HasStyleFlags(Maximized) || HasStyleFlags(NoResize))
		        return;

	        //unset maximized flag
	        RemoveStyleFlags(Maximized);
			m_bDirty = true;

			m_area.SetPadding(m_leftBorder, m_topBorder, m_leftBorder + m_rightBorder, m_topBorder + m_bottomBorder);
            
            SigShrinked();
        }

        void Window::OnMaximize(void)
        {
	        if(HasStyleFlags(Maximized) || HasStyleFlags(NoResize))
		        return;

	        //set style to "maximized"
	        SetStyleFlags(Maximized);
			m_bDirty = true;

			m_area.SetPadding(0, m_topBorder, 0, m_topBorder);

	        SigMaximized();
        }

        void Window::OnRedraw(void)
        {
			BaseWindow::OnRedraw();
			//move window name if window has an icon

			if(m_pIcon)
			{
				if(!HasStyleFlags(TopBar))
					m_pIcon->SetVisible(false);
				else if(!m_vRenderData.empty() && !m_stName.empty())
					m_vRenderData.rbegin()->rDestRect.x += 20;
			}

			m_pClose->SetVisible(!HasStyleFlags(NoClose));

			if ((HasStyleFlags(NoResizeH) || HasStyleFlags(NoResizeV)) || HasStyleFlags(NoMaximize))
			{
				m_pShrink->OnInvisible();
				m_pMaximize->OnInvisible();
			}
			else
			{
				if (HasStyleFlags(Maximized))
					m_pShrink->OnVisible();
				else
					m_pMaximize->OnVisible();
			}
        }

		void Window::OnParentResize(int x, int y, int width, int height)
		{
			if(HasStyleFlags(WindowStyleFlags::Maximized))
			{
				float oldX = m_x, oldY = m_y, oldWidth = m_width, oldHeight = m_height;
				auto vCenter = m_vCenter;
				auto hCenter = m_hCenter;

				m_x = 0.0f;
				m_y = 0.0f;
				switch(m_sizeModeWidth)
				{
				case PositionMode::ABS:
					m_width = (float)m_pParent->GetWidth();
					break;
				case PositionMode::SCREEN:
					m_width = m_pParent->GetWidth() / (float)m_pModule->GetScreenSize().x;
					break;
				case PositionMode::REF:
					m_width = m_pParent->GetWidth() / (float)m_pModule->m_layout.GetReferenceSize().x;
					break;
				default:
					m_width = 1.0f;
					break;
				}
				
				switch(m_sizeModeHeight)
				{
				case PositionMode::ABS:
					m_height = (float)m_pParent->GetHeight();
					break;
				case PositionMode::SCREEN:
					m_height = m_pParent->GetHeight() / (float)m_pModule->GetScreenSize().y;
					break;
				case PositionMode::REF:
					m_height = m_pParent->GetHeight() / (float)m_pModule->m_layout.GetReferenceSize().y;
					break;
				default:
					m_height = 1.0f;
					break;
				}
				m_vCenter = VerticalAlign::TOP;
				m_hCenter = HorizontalAlign::LEFT;

				BaseWindow::OnParentResize(x, y, width, height);

				m_x = oldX;
				m_y = oldY;
				m_width = oldWidth;
				m_height = oldHeight;
				m_vCenter = vCenter;
				m_hCenter = hCenter;
			}
			else
				BaseWindow::OnParentResize(x, y, width, height);
		}

		void Window::OnKeyDown(Keys key)
		{
			switch(key)
			{
			case Keys::ESC:
				OnClose();
				break;
			}

			if(m_bFocus)
				Widget::OnKeyDown(key);
		}

		void Window::ChangeArea(void)
		{
			BaseWindow::ChangeArea();

			m_topArea.SetHeight(m_topBorder);
		}

		void Window::OnTopRelease(void)
		{
			OnRelease(gui::MouseType::LEFT, true);
		}

		void Window::OnMoveTop(const math::Vector2& vMouse)
		{
			OnMove(math::Vector2(m_topArea.GetX(), m_topArea.GetY()) + vMouse);
		}

    }
}