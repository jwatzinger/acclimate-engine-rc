#pragma once
#include "BaseWindow.h"
#include "Area.h"

namespace acl
{
	namespace gui
	{

		class Icon;
		class ToolButton;

		class ACCLIMATE_API Window :
			public BaseWindow
		{
		public:
			Window(float x, float y, float width, float height, const std::wstring& stName = L"Untitled");
			~Window(void);

			void SetIcon(const std::wstring& stFileName, int srcX, int srcY, int srcWidth, int srcHeight);

			//events
			void OnShrink(void);
			void OnMaximize(void);
            void OnDoubleClick(void) override;
			void OnRedraw(void) override;
			void OnParentResize(int x, int y, int width, int height) override;
			void OnKeyDown(Keys key) override;
	
			core::Signal<> SigMaximized;
			core::Signal<> SigShrinked;

		private:
#pragma warning( disable: 4251 )
			void ChangeArea(void) override;

			void OnTopRelease(void);
			void OnMoveTop(const math::Vector2& vMouse);

			Icon* m_pIcon;
			Area m_topArea;
			ToolButton* m_pClose, *m_pShrink, *m_pMaximize;
#pragma warning( default: 4251 )
		};

	}
}
