#include "WindowProcHook.h"
#include "Module.h"

namespace acl
{
	namespace gui
	{

		void WindowProcHook::SetModule(Module& module)
		{
			m_pModule = &module;
		}

		void WindowProcHook::OnKeyStroke(wchar_t key)
		{
			m_pModule->OnKeyStroke(key);
		}

		void WindowProcHook::OnShortcut(wchar_t key)
		{
			m_pModule->OnShortcut(key);
		}

		bool WindowProcHook::PostCloseMessage(void)
		{
			return m_pModule->PostCloseMessage();
		}

		const math::Vector2& WindowProcHook::GetSize(void)
		{
			return m_pModule->GetScreenSize();
		}

		bool WindowProcHook::IsCursorVisible(void)
		{
			return m_pModule->m_cursor.IsVisible();
		}

		Module* WindowProcHook::m_pModule = nullptr;

	}
}
