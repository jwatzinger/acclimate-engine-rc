#pragma once

namespace acl
{
	namespace math
	{
		struct Vector2;
	}

	namespace gui
	{

		class Module;

		class WindowProcHook
		{
		public:
			
			static void SetModule(Module& module);

			static void OnKeyStroke(wchar_t key);
			static void OnShortcut(wchar_t key);
			static bool PostCloseMessage(void);
			static bool IsCursorVisible(void);
			static const math::Vector2& GetSize(void);

		private:

			static Module* m_pModule;
		};

	}
}


