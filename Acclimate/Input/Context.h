#pragma once

namespace acl
{
	namespace input
	{

		class Module;
		class ILoader;

		struct Context
		{
		public:
			Context(Module& module, ILoader& loader): module(module), loader(loader)
			{
			}

			Module& module;
			ILoader& loader;
		};

	}
}
