#include "Handler.h"

namespace acl
{
	namespace input
	{

		bool MappedInput::HasAction(unsigned int action) const
		{
			return m_actions.count(action) != 0;
		}

		bool MappedInput::HasState(unsigned int state) const
		{
			return m_states.count(state) != 0;
		}

		bool MappedInput::HasRange(unsigned int range, float& out) const
		{
			auto itr = m_ranges.find(range);
			if(itr != m_ranges.end())
			{
				out = itr->second;
				return true;
			}
			else
			{
				out = 0.0f;
				return false;
			}
		}

		bool MappedInput::HasValue(unsigned int range, float& out) const
		{
			auto itr = m_values.find(range);
			if(itr != m_values.end())
			{
				out = itr->second;
				return true;
			}
			else
			{
				out = 0.0f;
				return false;
			}
		}

		void Handler::SetButtonMappingAction(Keys button, unsigned int map)
		{
			m_mActions[button] = map;
		}

		void Handler::SetButtonMappingState(Keys button, unsigned int map)
		{
			m_mStates[button] = map;
		}

		void Handler::SetAxisMappingRange(Axis button, unsigned int map)
		{
			m_mRanges[button] = map;
		}

		void Handler::SetAxisMappingValue(Axis button, unsigned int map)
		{
			m_mValues[button] = map;
		}

		void Handler::MapButtonToAction(Keys button)
		{
			auto itr = m_mActions.find(button);
			if(itr != m_mActions.end())
				m_mapped.m_actions.insert(itr->second);

			auto itr2 = m_mStates.find(button);
			if(itr2 != m_mStates.end())
				m_toState.insert(itr2->second);
		}

		void Handler::MapButtonToState(Keys button)
		{
			auto itr = m_mStates.find(button);
			if(itr != m_mStates.end())
				m_mapped.m_states.insert(itr->second);
		}

		void Handler::MapAxisToRange(Axis axis, float value)
		{
			auto itr = m_mRanges.find(axis);
			if(itr != m_mRanges.end())
				m_mapped.m_ranges[itr->second] += value;
		}

		void Handler::MapAxisToValue(Axis axis, float value)
		{
			auto itr = m_mValues.find(axis);
			if(itr != m_mValues.end())
				m_mapped.m_values[itr->second] = value;
		}

		void Handler::RemoveButton(Keys button)
		{
			auto itr = m_mStates.find(button);
			if(itr != m_mStates.end())
			{
				m_mapped.m_states.erase(itr->second);
				m_toState.erase(itr->second);
			}
		}

		void Handler::Clear(void)
		{
			for(auto state : m_toState)
			{
				m_mapped.m_states.insert(state);
			}
			m_toState.clear();

			m_mapped.m_actions.clear();
			m_mapped.m_ranges.clear();
		}

		void Handler::Distribute(void)
		{
			SigProcessInput(m_mapped);
		}

	}
}