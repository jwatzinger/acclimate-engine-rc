/*
* Acclimate Engine : Input
*
* Created by Julian Watzinger
*/

#pragma once
#include <map>
#include <set>
#include "Keys.h"
#include "..\Core\Dll.h"
#include "..\Core\Signal.h"

namespace acl
{
	namespace input
	{

		struct ACCLIMATE_API MappedInput
		{
			bool HasAction(unsigned int action) const;
			bool HasState(unsigned int state) const;
			bool HasRange(unsigned int range, float& out) const;
			bool HasValue(unsigned int value, float& out) const;

			friend class Handler;
		private:
#pragma warning( disable: 4251 )
			std::set<unsigned int> m_actions;
			std::set<unsigned int> m_states;
			std::map<unsigned int, float> m_ranges;
			std::map<unsigned int, float> m_values;
#pragma warning( default: 4251 )
		};

		template<typename Actions = unsigned int, typename States = unsigned int, typename Ranges = unsigned int, typename Values = unsigned int>
		class MappedInputWrapper
		{
		public:

			MappedInputWrapper(const MappedInput& input) : m_input(input)
			{
			}

			bool HasAction(Actions action) const
			{
				return m_input.HasAction((unsigned int)action);
			}

			bool HasState(States state) const
			{
				return m_input.HasState((unsigned int)state);
			}

			bool HasRange(Ranges range, float& out) const
			{
				return m_input.HasRange((unsigned int)range, out);
			}

			bool HasValue(Values value, float& out) const
			{
				return m_input.HasValue((unsigned int)value, out);
			}

		private:

			const MappedInput& m_input;
		};

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const MappedInput&>;

		class ACCLIMATE_API Handler
		{
		public:

			void SetButtonMappingAction(Keys button, unsigned int map);
			void SetButtonMappingState(Keys button, unsigned int map);
			void SetAxisMappingRange(Axis axis, unsigned int map);
			void SetAxisMappingValue(Axis axis, unsigned int map);
		
			void MapButtonToAction(Keys button);
			void MapButtonToState(Keys button);
			void MapAxisToRange(Axis axis, float value);
			void MapAxisToValue(Axis axis, float value);
			void RemoveButton(Keys button);

			void Clear(void);
			void Distribute(void);

			core::Signal<const MappedInput&> SigProcessInput;

		private:
#pragma warning( disable: 4251 )
			std::map<Keys, unsigned int> m_mActions;
			std::map<Keys, unsigned int> m_mStates;
			std::map<Axis, unsigned int> m_mRanges;
			std::map<Axis, unsigned int> m_mValues;
			std::set<unsigned int> m_toState;

			MappedInput m_mapped;
#pragma warning( default: 4251 )
		};

	}
}
