/*
* Acclimate Engine : Input
*
* Created by Julian Watzinger
*/

#pragma once
#include <string>

namespace acl
{
	namespace input
	{

		class Handler;

		class ILoader
		{
		public:

			virtual ~ILoader(void) = 0 {}

			virtual Handler* Load(const std::wstring& stFilename) const = 0;
		};
	}
}