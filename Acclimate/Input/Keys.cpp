#include "Keys.h"
#include <Windows.h>

namespace acl
{
	namespace input
	{

		Keys mouseToKey(unsigned int mouseButton)
		{
			switch(mouseButton)
			{
			case 0:
				return Keys::LEFT_MOUSE;
			case 1:
				return Keys::RIGHT_MOUSE;
			case 2:
				return Keys::LEFT_DOUBLE;
			default:
				return Keys::UNKOWN;
			}
		}

		Keys winToKey(unsigned int key)
		{
			switch(key)
			{
			case VK_SHIFT:
				return Keys::SHIFT;
			case VK_SPACE:
				return Keys::SPACE;
			case VK_CONTROL:
				return Keys::CTRL;
			case VK_MENU:
				return Keys::ALT;
			case VK_RETURN:
				return Keys::ENTER;
			case VK_ESCAPE:
				return Keys::ESC;
			case VK_DELETE:
				return Keys::DEL;
			case 0x41:
				return Keys::A;
			case 0x42:
				return Keys::B;
			case 0x43:
				return Keys::C;
			case 0x44:
				return Keys::D;
			case 0x45:
				return Keys::E;
			case 0x46:
				return Keys::F;
			case 0x47:
				return Keys::G;
			case 0x48:
				return Keys::H;
			case 0x49:
				return Keys::I;
			case 0x4A:
				return Keys::J;
			case 0x4B:
				return Keys::K;
			case 0x4C:
				return Keys::L;
			case 0x4D:
				return Keys::M;
			case 0x4E:
				return Keys::N;
			case 0x4F:
				return Keys::O;
			case 0x50:
				return Keys::P;
			case 0x51:
				return Keys::Q;
			case 0x52:
				return Keys::R;
			case 0x53:
				return Keys::S;
			case 0x54:
				return Keys::T;
			case 0x55:
				return Keys::U;
			case 0x56:
				return Keys::V;	
			case 0x57:
				return Keys::W;
			case 0x58:
				return Keys::X;
			case 0x59:
				return Keys::Y;
			case 0x5A:
				return Keys::Z;
			case VK_LEFT:
				return Keys::LEFT_ARROW;
			case VK_RIGHT:
				return Keys::RIGHT_ARROW;
			case VK_UP:
				return Keys::UP_ARROW;
			case VK_DOWN:
				return Keys::DOWN_ARROW;
			default:
				return Keys::UNKOWN;
			}
		}

	}
}