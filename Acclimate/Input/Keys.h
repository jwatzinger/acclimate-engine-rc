/*
* Acclimate Engine : Input
*
* Created by Julian Watzinger
*/

#pragma once

namespace acl
{
	namespace input
	{

		enum class Axis
		{
			UNKOWN = -1,
			MOUSE_X = 0,
			MOUSE_Y,
			MOUSE_WHEEL,
			COUNT
		};

		enum class Keys
		{
			UNKOWN = -1,
			LEFT_MOUSE = 0,
			RIGHT_MOUSE,
			W, A, S, D,
			SHIFT, SPACE, CTRL, ALT, ENTER,
			DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW, UP_ARROW,
			LEFT_DOUBLE, ESC, DEL,
			B, C, E, F, G, H, I, J, K, L, M, N, O, P, Q, R,
			T, U, V, X, Y, Z, COUNT
		};

		Keys mouseToKey(unsigned int mouseButton);

		Keys winToKey(unsigned int key);

	}
}