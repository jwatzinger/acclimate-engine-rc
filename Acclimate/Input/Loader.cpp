#include "Loader.h"
#include "Module.h"
#include "Handler.h"
#include "..\System\Log.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace input
	{

		Loader::Loader(Module& module): m_pModule(&module)
		{
		}

		Handler* Loader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;

			try
			{
				doc.LoadFile(stFilename);
			}
			catch(...)
			{
				sys::log->Out(sys::LogModule::INPUT, sys::LogType::ERR, "Failed to load input handler from file", stFilename);
				return nullptr;
			}

			if(auto pRoot = doc.Root(L"Handler"))
			{
				const std::wstring& stName = pRoot->Attribute(L"name")->GetValue();

				auto pHandler = m_pModule->AddHandler(stName);

				// actions
				if(auto pActions = pRoot->FirstNode(L"Actions"))
				{
					if(auto pActionVector = pActions->Nodes(L"Action"))
					{
						for(auto pAction : *pActionVector)
						{
							const Keys key = static_cast<Keys>(pAction->Attribute(L"button")->AsInt());
							const unsigned int map = pAction->Attribute(L"map")->AsInt();

							if(key >= Keys::COUNT)
								sys::log->Out(sys::LogModule::INPUT, sys::LogType::WARNING, "tried to map action", map, "to invalid key", (unsigned int)key);
							else
								pHandler->SetButtonMappingAction(key, map);
						}
					}
				}
				
				// states
				if(auto pStates = pRoot->FirstNode(L"States"))
				{
					if(auto pStateVector = pStates->Nodes(L"State"))
					{
						for(auto pState : *pStateVector)
						{
							const Keys key = static_cast<Keys>(pState->Attribute(L"button")->AsInt());
							const unsigned int map = pState->Attribute(L"map")->AsInt();

							if(key >= Keys::COUNT)
								sys::log->Out(sys::LogModule::INPUT, sys::LogType::WARNING, "tried to map state", map, "to invalid key", (unsigned int)key);
							else
								pHandler->SetButtonMappingState(key, map);
						}
					}
				}

				// ranges
				if(auto pRanges = pRoot->FirstNode(L"Ranges"))
				{
					if(auto pRangeVector = pRanges->Nodes(L"Range"))
					{
						for(auto pRange : *pRangeVector)
						{
							const Axis axis = static_cast<Axis>(pRange->Attribute(L"axis")->AsInt());
							const unsigned int map = pRange->Attribute(L"map")->AsInt();

							if(axis >= Axis::COUNT)
								sys::log->Out(sys::LogModule::INPUT, sys::LogType::WARNING, "tried to map range", map, "to invalid axis", (unsigned int)axis);
							else
								pHandler->SetAxisMappingRange(axis, map);
						}
					}
				}

				// values
				if(auto pValues = pRoot->FirstNode(L"Values"))
				{
					if(auto pValuesVector = pValues->Nodes(L"Value"))
					{
						for(auto pValue : *pValuesVector)
						{
							Axis axis = static_cast<Axis>(pValue->Attribute(L"axis")->AsInt());
							unsigned int map = pValue->Attribute(L"map")->AsInt();

							if(axis >= Axis::COUNT)
								sys::log->Out(sys::LogModule::INPUT, sys::LogType::WARNING, "tried to map value", map, "to invalid axis", (unsigned int)axis);
							else
								pHandler->SetAxisMappingValue(axis, map);
						}
					}
				}

				return pHandler;
			}

			return nullptr;
		}


	}
}