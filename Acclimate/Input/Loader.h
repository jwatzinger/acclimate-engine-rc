/*
* Acclimate Engine : Input
*
* Created by Julian Watzinger
*/

#pragma once
#include "ILoader.h"

namespace acl
{
	namespace input
	{

		class Module;

		class Loader :
			public ILoader
		{
		public:
			Loader(Module& module);

			Handler* Load(const std::wstring& stFile) const override;

		private:

			Module* m_pModule;
		};

	}
}

