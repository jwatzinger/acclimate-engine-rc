#include "Module.h"
#include "Handler.h"

namespace acl
{
	namespace input
	{
		
		Module::~Module(void)
		{
			for(auto& handler : m_mHandler)
			{
				delete handler.second;
			}
		}

		void Module::SetRawButtonState(Keys key, bool bPressed, bool bPreviouslyPressed)
		{
			if(bPressed)
			{
				if(!bPreviouslyPressed)
				{
					for(auto& handler : m_mHandler)
					{
						handler.second->MapButtonToAction(key);
					}
				}
				else
				{
					for(auto& handler : m_mHandler)
					{
						handler.second->MapButtonToState(key);
					}
				}
			}
			else
			{
				for(auto& handler : m_mHandler)
				{
					handler.second->RemoveButton(key);
				}
			}
		}

		Handler* Module::AddHandler(const std::wstring& stName)
		{
			auto itr = m_mHandler.find(stName);
			if(itr == m_mHandler.end())
			{
				Handler* pHandler = new Handler();
				m_mHandler[stName] = pHandler;
				return pHandler;
			}
			else
				return itr->second;
		}

		Handler* Module::GetHandler(const std::wstring& stName)
		{
			auto itr = m_mHandler.find(stName);
			if(itr != m_mHandler.end())
				return itr->second;
			else
				return nullptr;
		}

		void Module::RemoveHandler(const std::wstring& stName)
		{
			auto itr = m_mHandler.find(stName);
			if(itr != m_mHandler.end())
			{
				delete itr->second;
				m_mHandler.erase(itr);
			}
		}

		void Module::RemoveHandler(const Handler& handler)
		{
			for(auto itr = m_mHandler.begin(); itr != m_mHandler.end(); ++itr)
			{
				if(itr->second == &handler)
				{
					delete itr->second;
					m_mHandler.erase(itr);
					return;
				}
			}
		}

		void Module::SetRawAxisValue(Axis axis, float value, bool bStoreForRange)
		{
			for(auto& handler : m_mHandler)
			{
				handler.second->MapAxisToValue(axis, value);
			}

			// calculate range
			if(m_mLastValues.count(axis))
			{
				auto& lastValue = m_mLastValues[axis];
				if(value != lastValue)
				{
					const float distance = lastValue - value;
					for(auto& handler : m_mHandler)
					{
						handler.second->MapAxisToRange(axis, distance);
					}
					lastValue = value;
				}
			}
			else
			{
				for(auto& handler : m_mHandler)
				{
					handler.second->MapAxisToRange(axis, value);
				}

				if(bStoreForRange)
					m_mLastValues[axis] = value;
			}		
		}

		void Module::SetAxisValueForced(Axis axis, float value, bool storeForRange)
		{
			for(auto& handler : m_mHandler)
			{
				handler.second->MapAxisToValue(axis, value);
			}

			// calculate range
			if(m_mLastValues.count(axis))
			{
				auto& lastValue = m_mLastValues[axis];
				if(value != lastValue)
				{
					for(auto& handler : m_mHandler)
					{
						handler.second->MapAxisToRange(axis, 0);
					}
					lastValue = value;
				}
			}
			else
			{
				for(auto& handler : m_mHandler)
				{
					handler.second->MapAxisToRange(axis, 0);
				}

				if(storeForRange)
					m_mLastValues[axis] = value;
			}
		}

		void Module::Distribute(void)
		{
			for(auto& handler : m_mHandler)
			{
				handler.second->Distribute();

				handler.second->Clear();
			}
		}

	}
}	