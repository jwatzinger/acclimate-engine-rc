/*
* Acclimate Engine : Input
*
* Created by Julian Watzinger
*/

#pragma once
#include <map>
#include <string>
#include "Keys.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace input
	{

		class Handler;

		class ACCLIMATE_API Module
		{
		public: 

			~Module(void);

			void SetRawButtonState(Keys key, bool bPressed, bool bPreviouslyPressed);
			void SetRawAxisValue(Axis axis, float value, bool bStoreForRange);
			void SetAxisValueForced(Axis axis, float value, bool storeForRange);

			Handler* AddHandler(const std::wstring& stName);

			Handler* GetHandler(const std::wstring& stName);

			void RemoveHandler(const std::wstring& stName);
			void RemoveHandler(const Handler& handler);

			void Distribute(void);

		private:
#pragma warning( disable: 4251 )
			std::map<std::wstring, Handler*> m_mHandler;

			std::map<Axis, float> m_mLastValues;
#pragma warning( default: 4251 )
		};

	}
}

