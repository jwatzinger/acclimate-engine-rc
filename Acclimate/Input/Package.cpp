#include "Package.h"
#include "WindowProcHook.h"

namespace acl
{
	namespace input
	{

		Package::Package(void) : m_loader(m_module), m_context(m_module, m_loader)
		{
			WindowProcHook::SetModule(m_module);
		}

		Package::~Package(void)
		{
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

		void Package::Update(void) 
		{
			m_module.Distribute();
		}

	}
}