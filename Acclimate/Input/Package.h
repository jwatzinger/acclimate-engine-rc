#pragma once
#include "Context.h"
#include "Module.h"
#include "Loader.h"

namespace acl
{
	namespace input
	{

		class Package
		{
		public:
			Package(void);
			~Package(void);

			const Context& GetContext(void) const;

			void Update(void);

		private:

			Module m_module;
			Loader m_loader;
			Context m_context;
		};

	}
}

