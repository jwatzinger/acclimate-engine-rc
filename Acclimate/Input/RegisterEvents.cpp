#include "RegisterEvents.h"
#include "..\Core\EventLoader.h"

namespace acl
{
	namespace input
	{

		void registerEvents(void)
		{
			// functions
			core::EventLoader::RegisterFunction<InputHasAction>();
			core::EventLoader::RegisterFunction<InputHasState>();

			// objects
			core::EventLoader::RegisterObject<MappedInput>(L"MappedInput");
		}

		/*****************************************
		* InputHasAction
		******************************************/

		void InputHasAction::OnCalculate(void)
		{
			auto& input = GetAttribute<MappedInput>(0);

			const bool hasAction = input.HasAction(GetAttribute<int>(1));
			ReturnValue(0, hasAction);
		}

		const core::FunctionDeclaration& InputHasAction::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"input", core::getObjectType<MappedInput>(), false },
				{ L"action", core::AttributeType::INT, false },
			};

			const core::AttributeVector vReturns =
			{
				{ L"", core::AttributeType::BOOL, false }
			};

			static const core::FunctionDeclaration decl(L"InputHasAction", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* InputHasState
		******************************************/

		void InputHasState::OnCalculate(void)
		{
			auto& input = GetAttribute<MappedInput>(0);

			const bool hasState = input.HasState(GetAttribute<int>(1));
			ReturnValue(0, hasState);
		}

		const core::FunctionDeclaration& InputHasState::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"input", core::getObjectType<MappedInput>(), false },
				{ L"state", core::AttributeType::INT, false },
			};

			const core::AttributeVector vReturns =
			{
				{ L"", core::AttributeType::BOOL, false }
			};

			static const core::FunctionDeclaration decl(L"InputHasState", vAttributes, vReturns);

			return decl;
		}

	}
}