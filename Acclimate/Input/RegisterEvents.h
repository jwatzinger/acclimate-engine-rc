#pragma once
#include "..\Core\EventFunction.h"
#include "Handler.h"

namespace acl
{
	namespace input
	{

		EXPORT_OBJECT(MappedInput);

		void registerEvents(void);

		/*****************************************
		* InputHasAction
		******************************************/

		class InputHasAction :
			public core::BaseEventFunction<InputHasAction>
		{
		public:

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* InputHasState
		******************************************/

		class InputHasState :
			public core::BaseEventFunction<InputHasState>
		{
		public:

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);
		};

	}
}