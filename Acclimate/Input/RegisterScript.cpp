#include "RegisterScript.h"
#include "Handler.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace input
	{

		void RegisterScript(script::Core& script)
		{
			auto mappedInput = script.RegisterReferenceType("MappedInput", script::REF_NOCOUNT);
			mappedInput.RegisterMethod("bool HasAction(uint) const", asMETHOD(MappedInput, HasAction));
			mappedInput.RegisterMethod("bool HasState(uint) const", asMETHOD(MappedInput, HasState));
			mappedInput.RegisterMethod("bool HasRange(uint, float& out) const", asMETHOD(MappedInput, HasRange));
			mappedInput.RegisterMethod("bool HasValue(uint, float& out) const", asMETHOD(MappedInput, HasValue));
		}

	}
}