#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace input
	{

		void RegisterScript(script::Core& core);

	}
}