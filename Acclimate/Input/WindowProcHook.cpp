#include "WindowProcHook.h"
#include "Module.h"

namespace acl
{
	namespace input
	{

		void WindowProcHook::SetModule(Module& module)
		{
			m_pModule = &module;
		}

		void WindowProcHook::SetRawButtonState(Keys button, bool bPressed, bool bPreviouslyPressed)
		{
			m_pModule->SetRawButtonState(button, bPressed, bPreviouslyPressed);
		}

		void WindowProcHook::SetRawAxisValue(Axis axis, float value, bool bStoreForRange)
		{
			m_pModule->SetRawAxisValue(axis, value, bStoreForRange);
		}

		void WindowProcHook::SetAxisValueForced(Axis axis, float value, bool bStoreForRange)
		{
			m_pModule->SetAxisValueForced(axis, value, bStoreForRange);
		}

		void WindowProcHook::SetMousePos(const math::Vector2& vMousePos)
		{
			m_vMousePos = vMousePos;
		}

		const math::Vector2& WindowProcHook::GetMousePos(void)
		{
			return m_vMousePos;
		}

		math::Vector2 WindowProcHook::m_vMousePos(0, 0);
		Module* WindowProcHook::m_pModule = nullptr;

	}
}
