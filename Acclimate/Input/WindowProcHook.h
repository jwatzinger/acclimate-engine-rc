#pragma once
#include "Keys.h"
#include "..\Math\Vector.h"

namespace acl
{
	namespace input
	{

		class Module;

		class WindowProcHook
		{
		public:
			
			static void SetModule(Module& module);

			static void SetRawButtonState(Keys button, bool bPressed, bool bPreviouslyPressed);
			static void SetRawAxisValue(Axis axis, float value, bool bStoreForRange = true);
			static void SetAxisValueForced(Axis axis, float value, bool bStoreForRange = true);

			static void SetMousePos(const math::Vector2& vMousePos);
			static const math::Vector2& GetMousePos(void);

		private:

			static math::Vector2 m_vMousePos;

			static Module* m_pModule;
		};

	}
}


