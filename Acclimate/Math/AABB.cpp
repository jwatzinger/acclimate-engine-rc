#include "AABB.h"
#include "Ray.h"
#include "Plane.h"
#include "Volumes.h"
#include <cmath>
#include <minmax.h>

namespace acl
{
	namespace math
	{

		AABB::AABB(void) : BaseVolume(0.0f, 0.0f, 0.0f, Volumes::AABB), m_vSize(0.0f, 0.0f, 0.0f)
		{
		}

		AABB::AABB(float x, float y, float z, float sizeX, float sizeY, float sizeZ) : BaseVolume(x, y, z, Volumes::AABB), m_vSize(sizeX, sizeY, sizeZ)
		{
		}

		AABB::AABB(const Vector3& vCenter, const Vector3& vSize) : BaseVolume(vCenter, Volumes::AABB), m_vSize(vSize)
		{
		}

		bool AABB::DotInside(const Vector3& dot) const
		{
			Vector3 vDistance = m_vCenter;
			vDistance -= dot;
			return (abs(vDistance.x) <= m_vSize.x && abs(vDistance.y) <= m_vSize.y && abs(vDistance.z) <= m_vSize.z);
		}

		bool AABB::Intersect(const Ray& ray, Vector3* vOut) const
		{
			Vector3 vMin(m_vCenter - m_vSize), vMax(m_vCenter + m_vSize);
			// r.dir is unit direction vector of ray
			// lb is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
			// ray.m_vOrigin is origin of ray
			float t1 = (vMin.x - ray.m_vOrigin.x)*ray.m_vDirFrac.x;
			float t2 = (vMax.x - ray.m_vOrigin.x)*ray.m_vDirFrac.x;
			float t3 = (vMin.y - ray.m_vOrigin.y)*ray.m_vDirFrac.y;
			float t4 = (vMax.y - ray.m_vOrigin.y)*ray.m_vDirFrac.y;
			float t5 = (vMin.z - ray.m_vOrigin.z)*ray.m_vDirFrac.z;
			float t6 = (vMax.z - ray.m_vOrigin.z)*ray.m_vDirFrac.z;

			float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
			float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));

			// if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behind us
			if (tmax < 0)
				return false;

			// if tmin > tmax, ray doesn't intersect AABB
			if (tmin > tmax)
				return false;

			if (vOut)
				*vOut = ray.m_vOrigin + ray.m_vDirection * tmin;

			return true;
		}

		bool AABB::InsidePlane(const Plane& plane) const
		{
			Vector3 vCenter = m_vCenter;
			const Vector3& vNormal = plane.GetNormal();

			if (vNormal.x >= 0.0f)
				vCenter.x += m_vSize.x;
			else
				vCenter.x -= m_vSize.x;

			if (vNormal.y >= 0.0f)
				vCenter.y += m_vSize.y;
			else
				vCenter.y -= m_vSize.y;

			if (vNormal.z >= 0.0f)
				vCenter.z += m_vSize.z;
			else
				vCenter.z -= m_vSize.z;

			float posDot = plane.Dot(vCenter);

			return posDot >= 0.0f;
		}

		const Vector3 AABB::GetMin() const
		{
			return Vector3(m_vCenter.x - m_vSize.x, m_vCenter.y - m_vSize.y, m_vCenter.z - m_vSize.z);
		}

		const Vector3 AABB::GetMax() const
		{
			return Vector3(m_vCenter.x + m_vSize.x, m_vCenter.y + m_vSize.y, m_vCenter.z + m_vSize.z);
		}
    }
}

