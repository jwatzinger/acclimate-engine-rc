#pragma once
#include "BaseVolume.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        class ACCLIMATE_API AABB : 
            public BaseVolume
        {
        public:

	        AABB(void);
	        AABB(float x, float y, float z, float sizeX, float sizeY, float sizeZ);
	        AABB(const Vector3& vCenter, const Vector3& vSize);

	        bool DotInside(const Vector3& dot) const;

	        bool Intersect(const Ray& ray, Vector3* pOut = nullptr) const;

	        inline float MaxExtents(void) const
	        {
		        return m_vSize.length();
	        }

			bool InsidePlane(const Plane& plane) const override;
			const Vector3 GetMin() const;
			const Vector3 GetMax() const;

            Vector3 m_vSize;
        };

    }
}

