#include "BaseFunctors.h"

namespace acl
{
	namespace math
	{

		/**************************************
		* Linear
		**************************************/

		float LinearFunctor::GetValue(float x) const
        {
            return x;
        }

		/**************************************
		* 2nd degree polynomal
		**************************************/

        Polynomal2::Polynomal2(float x2, float x, float d): m_x2(x2), m_x(x), m_d(d) 
		{
		}

        float Polynomal2::GetValue(float x) const
        {
            return pow(x, 2)*m_x2 + x*m_x + m_d;
        }

        /**************************************
		* 3rd degree polynomal
		**************************************/

        Polynomal3::Polynomal3(float x3, float x2, float x, float d): m_x3(x3), m_x2(x2), m_x(x), m_d(d) 
		{
		}

        float Polynomal3::GetValue(float x) const
        {
            return pow(x, 3)*m_x3 + pow(x, 2)*m_x2 + x*m_x + m_d;
        }

	}
}