#pragma once
#include <math.h>
#include "IFunctor.h"

namespace acl
{
	namespace math
	{

		/**************************************
		* Linear
		**************************************/

		class ACCLIMATE_API LinearFunctor : 
			public IFunctor
		{
        public:
			float GetValue(float x) const;
		};

		/**************************************
		* 2nd degree polynomal
		**************************************/

        class ACCLIMATE_API Polynomal2 : 
			public IFunctor
        {
        public:
            Polynomal2(float x2, float x, float d);

            inline float GetValue(float x) const;

        private:

            float m_x2, m_x, m_d;
        };

        /**************************************
		* 3rd degree polynomal
		**************************************/

        class ACCLIMATE_API Polynomal3 : 
			public IFunctor
        {
        public:
            Polynomal3(float x3, float x2, float x, float d);

            inline float GetValue(float x) const;

        private:

            float m_x3, m_x2, m_x, m_d;
        };

	}
}

