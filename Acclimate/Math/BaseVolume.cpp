#include "BaseVolume.h"
#include <algorithm>
#include "Collision.h"
#include "AABB.h"
#include "Sphere.h"
#include "OBB.h"
#include "HeightField.h"

namespace acl
{
    namespace math
    {

        BaseVolume::BaseVolume(float x, float y, float z, Volumes type): m_vOrigin(x, y, z), m_vCenter(x, y, z), m_type((unsigned int)type)
        {
        }

        BaseVolume::BaseVolume(const Vector3& vCenter, Volumes type): m_vCenter(vCenter), m_vOrigin(vCenter), m_type((unsigned int)type)
        {
        }

        void BaseVolume::SetCenter(const Vector3& vCenter)
        {
            m_vCenter = m_vOrigin + vCenter;
        }

        const Vector3& BaseVolume::GetCenter(void) const
        {
            return m_vCenter;
        }

		unsigned int BaseVolume::GetType(void) const
		{
			return m_type;
		}
		
#define HELP_CAST(volume, type, volume2, type2) *static_cast<const type*>(volume), *static_cast<const type2*>(volume2)

		bool BaseVolume::IsInside(const IVolume& volume) const
		{
			const IVolume* pVolume = this;
			const IVolume* pVolume2 = &volume;

			unsigned int type = m_type;
			unsigned int type2 = pVolume2->GetType();

			if(type > type2)
			{
				std::swap(type, type2);
				std::swap(pVolume, pVolume2);
			}

			switch(VolumeType(type, type2))
			{
			case VolumeType(Volumes::AABB, Volumes::AABB):
				return AABBVsAABB(HELP_CAST(pVolume, AABB, pVolume2, AABB));
			case VolumeType(Volumes::AABB, Volumes::HeightField):
				return AABBvsHeightfield(HELP_CAST(pVolume, AABB, pVolume2, Heightfield));
			case VolumeType(Volumes::AABB, Volumes::OBB):
				return AABBvsOBB(HELP_CAST(pVolume, AABB, pVolume2, OBB));
			case VolumeType(Volumes::AABB, Volumes::Sphere):
				return AABBvsSphere(HELP_CAST(pVolume, AABB, pVolume2, Sphere));
			case VolumeType(Volumes::OBB, Volumes::OBB):
				return OBBvsOBB(HELP_CAST(pVolume, OBB, pVolume2, OBB));
			case VolumeType(Volumes::OBB, Volumes::Sphere):
				return OBBvsSphere(HELP_CAST(pVolume, OBB, pVolume2, Sphere));
			case VolumeType(Volumes::Sphere, Volumes::Sphere):
				return SphereVsSphere(HELP_CAST(pVolume, Sphere, pVolume2, Sphere));
			default:
				return false;
			}
		}

#undef HELP_CAST

		bool BaseVolume::DotInside(const Vector3& dot) const
		{
			return false;
		}

		bool BaseVolume::Intersect(const Ray& ray, Vector3* pOut) const
		{
			return false;
		}

		bool BaseVolume::InsidePlane(const Plane& plane) const
		{
			return true;
		}

    }
}