#pragma once
#include "IVolume.h"
#include "Vector3.h"
#include "Volumes.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

		/// Volume base class
		/** This class implements the IVolume interface. It handles center and offset, so that a volume
		*	can be created at a certain position (offset) and later be moved via SetCenter, keeping this 
		*	relative position. Childs must still override most methods from the IVolume interface, 
		*	however the center getter/setter are \c final.*/
        class ACCLIMATE_API BaseVolume :
            public IVolume
        {
        public:

            BaseVolume(float x, float y, float z, Volumes type);
            BaseVolume(const Vector3& vCenter, Volumes type);
            virtual ~BaseVolume(void) = 0 {};

			/** 
			 *	@return Type
			 */
			unsigned int GetType(void) const override final;
			
			/**
			 * @param[in] vCenter
			 */
            void SetCenter(const Vector3& vCenter) override final;

			/**
			 * @return m_vCenter
			 */
            const Vector3& GetCenter(void) const override final;

			/** Checks for intersection with another volume.
			 *  This method checks if two volumes oversect in their current state.
			 *
			 *	@param[in] volume The volume to check for intersection.
			 *
			 *	@return Whether the two volumes intersected.
			 */
			bool IsInside(const IVolume& volume) const override final;

			virtual bool DotInside(const Vector3& dot) const override;

			virtual bool Intersect(const Ray& ray, Vector3* pOut = nullptr) const override;

			virtual bool InsidePlane(const Plane& plane) const override;

        protected:
            
            Vector3 m_vCenter,	///< Volume position
					m_vOrigin;	///< Volume offset to its position

			const unsigned int m_type;
        };


    }
}
