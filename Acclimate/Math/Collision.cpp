#include "Collision.h"
#include <math.h>
#include <minmax.h>
#include "Vector3.h"
#include "Sphere.h"
#include "AABB.h"
#include "OBB.h"
#include "HeightField.h"

namespace acl
{
    namespace math
    {

        bool AABBVsAABB(const AABB& left, const AABB& right)
        {
            Vector3 vDistance = left.GetCenter() - right.GetCenter();
		    Vector3 vMaxDistance = left.m_vSize + right.m_vSize;
		    return (abs(vDistance.x) <= vMaxDistance.x && abs(vDistance.y) <= vMaxDistance.y && abs(vDistance.z) <= vMaxDistance.z);
        }

		bool AABBvsHeightfield(const AABB& aabb, const Heightfield& field)
		{
			const size_t divX = (size_t)aabb.m_vSize.x;
			const size_t divZ = (size_t)aabb.m_vSize.z;

			const Vector3 vMin = aabb.GetCenter() - Vector3((float)divX, aabb.m_vSize.y, (float)divZ);

			for(size_t i = 0; i <= divX; i++)
			{
				for(size_t j = 0; j <= divZ; j++)
				{
					const Vector3 vDot = vMin + Vector3((float)i, 0, (float)j);
					if(field.DotInside(vDot))
						return true;
				}
			}

			return false;
		}

		bool AABBvsOBB(const AABB& left, const OBB& right)
        {
            Vector3 v;

            // Vectors for box extentens and seperate axis
            float a[3];
            float b[3];
            float T[3];

            // projection matrices
            float R[3][3];
            float abs_R[3][3];

            long i, k;
            float ra, rb, t;

            const Vector3& vExtent1 = right.GetExtent();
            a[0] = vExtent1.x;
            a[1] = vExtent1.y;
            a[2] = vExtent1.z;

            const Vector3& vExtent2 = left.m_vSize;
            b[0] = vExtent2.x;
            b[1] = vExtent2.y;
            b[2] = vExtent2.z;

            Vector3 vNormals[3];
            vNormals[0] = Vector3(1.0f, 0.0f, 0.0f);
            vNormals[1] = Vector3(0.0f, 1.0f, 0.0f);
            vNormals[2] = Vector3(0.0f, 0.0f, 1.0f);

            // Calculate projection coefficients

            for(i = 0; i < 3; i++)
            {
                for(k = 0; k < 3; k++)
                {
                    R[i][k] = right.GetNormal(i).Dot(vNormals[k]);
                    abs_R[i][k] = (float)fabs(R[i][k]);
                }
            }

            // retransformation of center point

            v = left.GetCenter() - right.GetCenter();
            T[0] = v.Dot(right.GetNormal(0));
            T[1] = v.Dot(right.GetNormal(1));
            T[2] = v.Dot(right.GetNormal(2));

            // test whether normal vectors are seperate axis

            for(i = 0; i < 3; i++)
            {
                ra = a[i];

                rb = b[0]*abs_R[i][0]+
                b[1]*abs_R[i][1]+
                b[2]*abs_R[i][2];

                t = (float)fabs(T[i]);

                if(t > ra + rb)
                    return false;
            }

            for(i = 0; i < 3; i++)
            {

                ra = a[0]*abs_R[0][i]+
                a[1]*abs_R[1][i]+
                a[2]*abs_R[2][i];

                rb = b[i];

                t = (float)fabs(T[0]*R[0][i]+T[1]*R[1][i]+T[2]*R[2][i]);

                if(t > ra + rb)
                    return false;
            }

            // test if vector-products build seperate axis

            ra = a[1]*abs_R[2][0] + a[2]*abs_R[1][0];
            rb = b[1]*abs_R[0][2] + b[2]*abs_R[0][1];
            t  = (float)fabs(T[2]*R[1][0] - T[1]*R[2][0]);

            if(t > ra + rb)
                return false;

            ra = a[1]*abs_R[2][1] + a[2]*abs_R[1][1];
            rb = b[0]*abs_R[0][2] + b[2]*abs_R[0][0];
            t  = (float)fabs(T[2]*R[1][1] - T[1]*R[2][1]);

            if(t > ra + rb)
                return false;

            ra = a[1]*abs_R[2][2] + a[2]*abs_R[1][2];
            rb = b[0]*abs_R[0][1] + b[1]*abs_R[0][0];
            t  = (float)fabs(T[2]*R[1][2] - T[1]*R[2][2]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][0] + a[2]*abs_R[0][0];
            rb = b[1]*abs_R[1][2] + b[2]*abs_R[1][1];
            t  = (float)fabs(T[0]*R[2][0] - T[2]*R[0][0]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][1] + a[2]*abs_R[0][1];
            rb = b[0]*abs_R[1][2] + b[2]*abs_R[1][0];
            t  = (float)fabs(T[0]*R[2][1] - T[2]*R[0][1]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][2] + a[2]*abs_R[0][2];
            rb = b[0]*abs_R[1][1] + b[1]*abs_R[1][0];
            t  = (float)fabs(T[0]*R[2][2] - T[2]*R[0][2]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][0] + a[1]*abs_R[0][0];
            rb = b[1]*abs_R[2][2] + b[2]*abs_R[2][1];
            t  = (float)fabs(T[1]*R[0][0] - T[0]*R[1][0]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][1] + a[1]*abs_R[0][1];
            rb = b[0]*abs_R[2][2] + b[2]*abs_R[2][0];
            t  = (float)fabs(T[1]*R[0][1] - T[0]*R[1][1]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][2] + a[1]*abs_R[0][2];
            rb = b[0]*abs_R[2][1] + b[1]*abs_R[2][0];
            t  = (float)fabs(T[1]*R[0][2] - T[0]*R[1][2]);

            if(t > ra + rb)
                return false;


            return true;
        }

		bool AABBvsSphere(const AABB& aabb, const Sphere& sphere)
		{
			const Vector3& vCenter = aabb.GetCenter();

			const Vector3 vMin = vCenter - aabb.m_vSize;
			const Vector3 vMax = vCenter + aabb.m_vSize;

			const Vector3& vSphereCenter = sphere.GetCenter();

			const Vector3 vClosest = vSphereCenter.Max(vMin).Min(vMax);
			
			float distanceSquared = (vClosest - vSphereCenter).squaredLength();

			return distanceSquared < (sphere.m_radius * sphere.m_radius);
		}

        /***********************************************
        * OBB vs OBB collision
        * orginally (slightly modified) from 
        * http://www.spieleprogrammierung.net/2010/06/orientierte-bounding-boxen-obb-obb-obb_7513.html
        ***********************************************/

        bool OBBvsOBB(const OBB& left, const OBB& right)
        {
            Vector3 v;

            // Vectors for box extentens and seperate axis
            float a[3];
            float b[3];
            float T[3];

            // projection matrices
            float R[3][3];
            float abs_R[3][3];

            long i, k;
            float ra, rb, t;

            const Vector3& vExtent1 = left.GetExtent();
            a[0] = vExtent1.x;
            a[1] = vExtent1.y;
            a[2] = vExtent1.z;

            const Vector3& vExtent2 = right.GetExtent();
            b[0] = vExtent2.x;
            b[1] = vExtent2.y;
            b[2] = vExtent2.z;

            // Calculate projection coefficients

            for(i = 0; i < 3; i++)
            {
                for(k = 0; k < 3; k++)
                {
                    R[i][k] = left.GetNormal(i).Dot(right.GetNormal(k));
                    abs_R[i][k] = (float)fabs(R[i][k]);
                }
            }

            // retransformation of center point

            v = right.GetCenter() - left.GetCenter();
            T[0] = v.Dot(left.GetNormal(0));
            T[1] = v.Dot(left.GetNormal(1));
            T[2] = v.Dot(left.GetNormal(2));

            // test whether normal vectors are seperate axis

            for(i = 0; i < 3; i++)
            {
                ra = a[i];

                rb = b[0]*abs_R[i][0]+
                b[1]*abs_R[i][1]+
                b[2]*abs_R[i][2];

                t = (float)fabs(T[i]);

                if(t > ra + rb)
                    return false;
            }

            for(i = 0; i < 3; i++)
            {

                ra = a[0]*abs_R[0][i]+
                a[1]*abs_R[1][i]+
                a[2]*abs_R[2][i];

                rb = b[i];

                t = (float)fabs(T[0]*R[0][i]+T[1]*R[1][i]+T[2]*R[2][i]);

                if(t > ra + rb)
                    return false;
            }

            // test if vector-products build seperate axis

            ra = a[1]*abs_R[2][0] + a[2]*abs_R[1][0];
            rb = b[1]*abs_R[0][2] + b[2]*abs_R[0][1];
            t  = (float)fabs(T[2]*R[1][0] - T[1]*R[2][0]);

            if(t > ra + rb)
                return false;

            ra = a[1]*abs_R[2][1] + a[2]*abs_R[1][1];
            rb = b[0]*abs_R[0][2] + b[2]*abs_R[0][0];
            t  = (float)fabs(T[2]*R[1][1] - T[1]*R[2][1]);

            if(t > ra + rb)
                return false;

            ra = a[1]*abs_R[2][2] + a[2]*abs_R[1][2];
            rb = b[0]*abs_R[0][1] + b[1]*abs_R[0][0];
            t  = (float)fabs(T[2]*R[1][2] - T[1]*R[2][2]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][0] + a[2]*abs_R[0][0];
            rb = b[1]*abs_R[1][2] + b[2]*abs_R[1][1];
            t  = (float)fabs(T[0]*R[2][0] - T[2]*R[0][0]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][1] + a[2]*abs_R[0][1];
            rb = b[0]*abs_R[1][2] + b[2]*abs_R[1][0];
            t  = (float)fabs(T[0]*R[2][1] - T[2]*R[0][1]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[2][2] + a[2]*abs_R[0][2];
            rb = b[0]*abs_R[1][1] + b[1]*abs_R[1][0];
            t  = (float)fabs(T[0]*R[2][2] - T[2]*R[0][2]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][0] + a[1]*abs_R[0][0];
            rb = b[1]*abs_R[2][2] + b[2]*abs_R[2][1];
            t  = (float)fabs(T[1]*R[0][0] - T[0]*R[1][0]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][1] + a[1]*abs_R[0][1];
            rb = b[0]*abs_R[2][2] + b[2]*abs_R[2][0];
            t  = (float)fabs(T[1]*R[0][1] - T[0]*R[1][1]);

            if(t > ra + rb)
                return false;

            ra = a[0]*abs_R[1][2] + a[1]*abs_R[0][2];
            rb = b[0]*abs_R[2][1] + b[1]*abs_R[2][0];
            t  = (float)fabs(T[1]*R[0][2] - T[0]*R[1][2]);

            if(t > ra + rb)
                return false;


            return true;
        }

		bool OBBvsSphere(const OBB& obb, const Sphere& sphere)
		{
			Vector3 vSphereCenter = sphere.GetCenter();
			float sphereRadius = sphere.m_radius;

			Vector3 vPoint = obb.ClosestPoint(vSphereCenter);
			Vector3 v = vPoint - vSphereCenter;

			return v.Dot(v) <= sphereRadius * sphereRadius;
		}
		
        bool SphereVsSphere(const Sphere& left, const Sphere& right)
        {
            Vector3 vDir = left.GetCenter();
		    vDir -= right.GetCenter();

		    float maxDistance = left.m_radius + right.m_radius;
		    float distance = vDir.length();

		    return distance <= maxDistance;
        }

    }
}