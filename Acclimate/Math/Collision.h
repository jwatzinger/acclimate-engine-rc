/** @file Collision.h
*	
*	Contains collision methods for base volumes.
*/

#pragma once

namespace acl
{
    namespace math
    {

        class Sphere;
        class AABB;
        class OBB;
		class Heightfield;

		/** Checks for intersection between two AABBs.
		 *
		 *	@param[in] left First AABB.
		 *	@param[in] right Second AABB.
		 *
		 *	@return Whether the two AABBs intersected.
		 */
        bool AABBVsAABB(const AABB& left, const AABB& right);

		/** Checks for intersection between an AABB and an OBB.
		 *
		 *	@param[in] left AABB.
		 *	@param[in] right OBB.
		 *
		 *	@return Whether the AABB and OBB intersected.
		 */
        bool AABBvsOBB(const AABB& left, const OBB& right);

		/** Checks for intersection between an AABB and a Heightfield.
		 *	Notifies intersection as long as any part of the box is under the heightfield, even if
		 *	it does not directly intersect it.
		 *
		 *	@param[in] aabb AABB.
		 *	@param[in] field field.
		 *
		 *	@return Whether the AABB intersects the heightfield.
		 */
		bool AABBvsHeightfield(const AABB& aabb, const Heightfield& field);

		/** Checks for intersection between an AABB and a Sphere.
		 *
		 *	@param[in] aabb AABB.
		 *	@param[in] sphere Sphere.
		 *
		 *	@return Whether the AABB intersects the sphere.
		 */
		bool AABBvsSphere(const AABB& aabb, const Sphere& sphere);

		/** Checks for intersection between two OBBs.
		 *
		 *	@param[in] left First OBB.
		 *	@param[in] right Second OBB.
		 *
		 *	@return Whether the two OBBs intersected.
		 */
        bool OBBvsOBB(const OBB& left, const OBB& right);

		/** Checks for intersection between an OBB and a Sphere.
		 *
		 *	@param[in] obb OBB.
		 *	@param[in] sphere Sphere.
		 *
		 *	@return Whether the OBB and the Sphere intersected.
		 */
        bool OBBvsSphere(const OBB& obb, const Sphere& sphere);
		
		/** Checks for intersection between two spheres.
		 *
		 *	@param[in] left First Sphere.
		 *	@param[in] right Second Sphere.
		 *
		 *	@return Whether the two spheres intersected.
		 */
        bool SphereVsSphere(const Sphere& left, const Sphere& right);

    }
}