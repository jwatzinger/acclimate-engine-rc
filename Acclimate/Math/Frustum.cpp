#include "Frustum.h"
#include "Matrix.h"
#include "IVolume.h"

namespace acl
{
    namespace math
    {

        void Frustum::CalculateFrustum(const Matrix& m)
        {
			m_frustums[0].Set(m.m13, m.m23, m.m33, m.m43); // near
			m_frustums[1].Set(m.m14 - m.m13, m.m24 - m.m23, m.m34 - m.m33, m.m44 - m.m43); // far
			m_frustums[2].Set(m.m14 + m.m11, m.m24 + m.m21, m.m34 + m.m31, m.m44 + m.m41); // left
			m_frustums[3].Set(m.m14 - m.m11, m.m24 - m.m21, m.m34 - m.m31, m.m44 - m.m41); // right
			m_frustums[4].Set(m.m14 + m.m12, m.m24 + m.m22, m.m34 + m.m32, m.m44 + m.m42); // bottom
			m_frustums[5].Set(m.m14 - m.m12, m.m24 - m.m22, m.m34 - m.m32, m.m44 - m.m42); // top
			for(unsigned int i = 0; i < 6; i++)
			{
				m_frustums[i].Normalize();
			}
        }

		const Plane* Frustum::GetPlanes(void) const
		{
			return m_frustums;
		}

        bool Frustum::TestVolume(const IVolume& volume) const
        {
	        for ( int i = 0; i < 6; i++ )
            {
                if ( !volume.InsidePlane(m_frustums[i]) )
			        return false;
            }
            return true;
        }

		bool Frustum::DotInside(const Vector3& vPosition, float offsetAllowance) const
		{
			for(int i = 0; i < 6; i++)
			{
				if (m_frustums[i].Dot(vPosition) < offsetAllowance)
					return false;
			}
			return true;
		}

    }
}