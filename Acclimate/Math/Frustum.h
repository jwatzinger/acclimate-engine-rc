#pragma once
#include <vector>
#include "Plane.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        class IVolume;
		struct Matrix;

        class ACCLIMATE_API Frustum
        {
        public:

			const Plane* GetPlanes(void) const;

	        void CalculateFrustum(const Matrix& mViewProjection);
	        bool TestVolume(const IVolume& volume) const;
			bool DotInside(const Vector3& vPosition, float offsetAllowance) const;

        private:
	
	        Plane m_frustums[6];
        };

    }
}