#include "Heightfield.h"
#include "Plane.h"
#include "Ray.h"
#include "Triangle.h"
#include "Vector.h"
#include "Volumes.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace math
    {

        Heightfield::Heightfield(const HeightVector& vHeights, unsigned int size): m_size(size), BaseVolume(0.0f, 0.0f, 0.0f, Volumes::HeightField)
        {
			m_vHeights = vHeights;

            // precaculate half size of heightfield
            const int halfSize = (signed)m_size/2;
            // add triangles to heightfield
            for(int i = 0; i < (signed)m_size-1; i++)
            {
                for(int j = 0; j < (signed)m_size-1; j++)
                {
                    // first triangle: 0/0 -> 0/1 -> 1/0
                    Vector3 vPos1((float)(j - halfSize), m_vHeights[i][j], (float)(i - halfSize));
                    Vector3 vPos2((float)(j - halfSize), m_vHeights[i+1][j], (float)(i + 1 - halfSize));
                    Vector3 vPos3((float)(j + 1 - halfSize), m_vHeights[i][j+1], (float)(i - halfSize));
                    
                    m_vTris.emplace_back(vPos1, vPos2, vPos3);

                    // second triangle: 1/0 -> 0/1 -> 1/1
					Vector3 vPos4((float)(j - halfSize), m_vHeights[i + 1][j], (float)(i + 1 - halfSize));
                    Vector3 vPos5((float)(j + 1 - halfSize), m_vHeights[i][j + 1], (float)(i - halfSize));
                    Vector3 vPos6((float)(j + 1- halfSize), m_vHeights[i + 1][j + 1], (float)(i + 1 - halfSize));
                    
					m_vTris.emplace_back(vPos4, vPos5, vPos6);
                }
            }
        }

		float Heightfield::Height(float x, float z) const
		{
			int gridZ = (int)(z - m_vCenter.z + m_size / 2);
			int gridX = (int)(x - m_vCenter.x + m_size / 2);

			const Ray heightRay(Vector3(x, 100.0f, z), Vector3(0.0f, -1.0f, 0.0f));

			Vector3 vPos;
			if(m_vTris[(gridZ*(m_size-1) + gridX)*2].Intersect(heightRay, &vPos))
				return vPos.y;
			else if(m_vTris[(gridZ*(m_size-1) + gridX)*2+1].Intersect(heightRay, &vPos))
				return vPos.y;
			else
			{
				ACL_ASSERT(false);
				return 0.0f;
			}
		}

        bool Heightfield::DotInside(const Vector3& vDot) const
        {
            Vector3 vPos = vDot - m_vCenter + Vector3((float)m_size/2, 0.0f, (float)m_size/2);
            Vector2 vGrid((int)(vPos.x)+1, (int)(vPos.z)+1);

			if(vGrid.x > m_vCenter.x && vGrid.x < m_vCenter.x + m_size &&
                vGrid.y > m_vCenter.z && vGrid.y < m_vCenter.z + m_size)
			{
				return vPos.y <= Height(vDot.x+1, vDot.z+1);
			}

            return false;
        }
        
        bool Heightfield::Intersect(const Ray& ray, Vector3* pOut) const
	    {
            if(pOut)
                *pOut = ray.m_vOrigin + ray.m_vDirection * 1000.0f;

            Vector3 vOut;

            Ray alignedRay(ray);
            alignedRay.m_vOrigin -= m_vCenter;

            bool bIntersect = false;
            for(const Triangle& tri : m_vTris)
            {
                if(tri.Intersect(alignedRay, &vOut))
                {
                    if(pOut)
                        if((alignedRay.m_vOrigin - vOut).length() < (alignedRay.m_vOrigin - *pOut).length())
                            *pOut = vOut + m_vCenter;

                    bIntersect = true;
                }
            }

		    return bIntersect;
	    }

        float Heightfield::MaxExtents(void) const
        {
            return (float)sqrt(m_size * m_size);
        }
    
    }
}
