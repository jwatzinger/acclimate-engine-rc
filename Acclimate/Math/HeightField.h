#pragma once
#include <vector>
#include "BaseVolume.h"
#include "Triangle.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        class ACCLIMATE_API Heightfield : 
            public BaseVolume
        {
            typedef std::vector<Triangle> TriangleVector;

        public:

			typedef std::vector<std::vector<float>> HeightVector;

            Heightfield(const HeightVector& vHeights, unsigned int size);

			float Height(float x, float z) const; 

            bool Intersect(const Ray& ray, Vector3* pOut = nullptr) const;

            bool DotInside(const Vector3& dot) const;

            float MaxExtents(void) const;

        private:
#pragma warning( disable: 4251 )
            unsigned int m_size;
            HeightVector m_vHeights;
            TriangleVector m_vTris;
#pragma warning( default: 4251 )
        };

    }
}

