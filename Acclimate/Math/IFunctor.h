#pragma once
#include <math.h>
#include "..\Core\Dll.h"

namespace acl
{
	namespace math
	{
		/// Interface for algebra functors
		/** This interface implements mathematical function objects, such as polynomals. */
		class ACCLIMATE_API IFunctor
		{
        public:

			virtual ~IFunctor(void) = 0 {}

			/** Returns the y value at x position.
			 *
			 *	@param[in] x Coordinate on x axis
			 *
			 *	@return Corresponding value on y axis
			 */
			virtual float GetValue(float x) const = 0;
		};

	}
}