#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace math
	{

		class Plane;
		struct Vector3;
		struct Ray;

		/// Interface for a 3d volume object
		/** This interface implementes a mathematical volumetric object, like an AABB or a Sphere. It is used
		*   with collision detection, and offers methods for direct collision checks between volumes. Also,
		*   ray and dot intersection is implemented. 
		*	Currently for each volume that should be supported for collision, a new virtual WithVolume-method 
		*	has to be declared in this interface, and be implemented by all volumes that support collision with 
		*	that new volume.*/
		class ACCLIMATE_API IVolume
		{
		public:

			virtual ~IVolume(void) {}

			/** Checks for intersection with another volume.
			 *  This method should be implemented calling the appropriate WithVolume-method of the passed volume
			 *	and returning its result.
			 *
			 *	@param[in] volume The volume to check for intersection.
			 *
			 *	@return Whether the two volumes intersected.
			 */
			virtual bool IsInside(const IVolume& volume) const = 0;

			/** Returns the maximal extent of the volume.
			 *  This method must return the maximal extent from the center to the farthest possible point on the
			 *	volume, so that e.g. a bounding sphere could be constructed around the volume using this value.
			 *
			 *	@return Length of the maximum extent of the volume.
			 */
			virtual float MaxExtents(void) const = 0;

			/**
			 * @param[in] vCenter
			 */
			virtual void SetCenter(const Vector3& vCenter) = 0;

			/**
			 * @return Center
			 */
			virtual const Vector3& GetCenter(void) const = 0;

			/** 
			 * @return Type
			 */
			virtual unsigned int GetType(void) const = 0;

			/** Checks if a dot is contained within the volume.
			 *
			 *  @param[in] dot The dot to check the volume for.
			 *
			 *	@return Whether the dot is inside the volume.
			 */
			virtual bool DotInside(const Vector3& dot) const = 0;

			/** Tests for intersection with a ray.
			 *	Performs a ray->volume intersection test. The passed output vector can be \c nullptr, otherwise
			 *	it shall receive the intersection position in case of intersection.
			 *
			 *  @param[in] ray The ray to check for intersection.
			 *	@param[out] pOut Intersection position, if intersection is found.
			 *
			 *	@return Whether the ray intersects the volume.
			 */
			virtual bool Intersect(const Ray& ray, Vector3* pOut = nullptr) const = 0;

			/** Tests for relation to a plane.
			 *	Used for frustum culling, determines if a volume is on the right side of a plane 
			 *	to be contained inside a frustum.
			 *
			 *  @param[in] plane The plane to check with.
			 *
			 *	@return Whether the volume is "inside" the plane.
			 */
			virtual bool InsidePlane(const Plane& plane) const = 0;
			// todo: change up for less case specifiy usage
		};

	}
}