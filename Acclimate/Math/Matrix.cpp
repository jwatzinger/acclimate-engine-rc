#include "Matrix.h"
#include <math.h>
#include "Vector3.h"
#include "Vector4.h"
#include "Plane.h"
#include "Rect.h"
#include "Utility.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace math
	{
		/**************************************************
		*	Member functions
		**************************************************/

		Matrix::Matrix(void)
		{
		}

		Matrix::Matrix(float c11, float c12, float c13, float c14,
				   float c21, float c22, float c23, float c24,
				   float c31, float c32, float c33, float c34,
				   float c41, float c42, float c43, float c44): m11(c11), m12(c12), m13(c13), m14(c14),
																m21(c21), m22(c22), m23(c23), m24(c24),
																m31(c31), m32(c32), m33(c33), m34(c34),
																m41(c41), m42(c42), m43(c43), m44(c44)
		{
		}

		Matrix::Matrix(const Matrix& matrix)
		{
			for(int i = 0; i < 4; i++)
			{
				for(int j = 0; j < 4; j++)
				{
					m[i][j] = matrix.m[i][j];
				}
			}
		}

		float& Matrix::operator() (int row, int column)
		{
			ACL_ASSERT(row <= 4 && column <= 4);

			return m[row-1][column-1];
		}

		float Matrix::operator() (int row, int column) const
		{
			ACL_ASSERT(row <= 4 && column <= 4);

			return m[row-1][column-1];
		}

		Matrix Matrix::operator* (const Matrix& mat) const
		{
			return Matrix(	m11 * mat.m11 + m12 * mat.m21 + m13 * mat.m31 + m14 * mat.m41, 
							m11 * mat.m12 + m12 * mat.m22 + m13 * mat.m32 + m14 * mat.m42,
							m11 * mat.m13 + m12 * mat.m23 + m13 * mat.m33 + m14 * mat.m43,
							m11 * mat.m14 + m12 * mat.m24 + m13 * mat.m34 + m14 * mat.m44,
							m21 * mat.m11 + m22 * mat.m21 + m23 * mat.m31 + m24 * mat.m41,
							m21 * mat.m12 + m22 * mat.m22 + m23 * mat.m32 + m24 * mat.m42,
							m21 * mat.m13 + m22 * mat.m23 + m23 * mat.m33 + m24 * mat.m43,
							m21 * mat.m14 + m22 * mat.m24 + m23 * mat.m34 + m24 * mat.m44,
							m31 * mat.m11 + m32 * mat.m21 + m33 * mat.m31 + m34 * mat.m41,
							m31 * mat.m12 + m32 * mat.m22 + m33 * mat.m32 + m34	* mat.m42,
							m31 * mat.m13 + m32 * mat.m23 + m33 * mat.m33 + m34 * mat.m43,
							m31 * mat.m14 + m32 * mat.m24 + m33 * mat.m34 + m34 * mat.m44,
							m41 * mat.m11 + m42 * mat.m21 + m43 * mat.m31 + m44 * mat.m41,
							m41 * mat.m12 + m42 * mat.m22 + m43 * mat.m32 + m44 * mat.m42,
							m41 * mat.m13 + m42 * mat.m23 + m43 * mat.m33 + m44 * mat.m43,
							m41 * mat.m14 + m42 * mat.m24 + m43 * mat.m34 + m44 * mat.m44);
		}

		Matrix& Matrix::operator*=(const Matrix& mat)
		{
			*this = *this*mat;

			return *this;
		}

		float Matrix::Det(void) const
		{
			Vector4 vMinor, v1, v2, v3;
			float det;
			v1.x = m11; v1.y = m21; v1.z = m31; v1.w = m41;
			v2.x = m12; v2.y = m22; v2.z = m32; v2.w = m42;
			v3.x = m13; v3.y = m23; v3.z = m33; v3.w = m43;
			vMinor = v1.Cross(v2, v3);
			det = -(m14 * vMinor.x + m24 * vMinor.y + m34 * vMinor.z + m44 * vMinor.w);
			return det;
		}

		void Matrix::Identity(void)
		{
			m11 = 1.0f; m12 = 0.0f; m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f; m22 = 1.0f; m23 = 0.0f; m24 = 0.0f;
			m31 = 0.0f; m32 = 0.0f; m33 = 1.0f; m34 = 0.0f;
			m41 = 0.0f; m42 = 0.0f; m43 = 0.0f; m44 = 1.0f;
		}

		void Matrix::Inverse(void)
		{
			float det = Det();
			if(!det)
				return;

			Vector4 vec[3];

			float mOld[4][4];
			memcpy(mOld, m, sizeof(float)* 16);

			for(int i=0; i<4; i++)
			{
				for (int j=0; j<4; j++)
				{
					if (j != i )
					{
						int a = j;
						if(j > i)
							a = a-1;
						vec[a].x = mOld[j][0];
						vec[a].y = mOld[j][1];
						vec[a].z = mOld[j][2];
						vec[a].w = mOld[j][3];
					}
				}
				Vector4 v = vec[0].Cross(vec[1], vec[2]);

				for(int j= 0; j<4; j++)
				{
					float cofactor;
					switch(j)
					{
						case 0: cofactor = v.x; break;
						case 1: cofactor = v.y; break;
						case 2: cofactor = v.z; break;
						case 3: cofactor = v.w; break;
					}
					m[j][i] = pow(-1.0f, i) * cofactor / det;
				}
			}
		}

		void Matrix::Translation(float x, float y, float z)
		{
			m11 = 1.0f; m12 = 0.0f; m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f; m21 = 1.0f; m23 = 0.0f; m24 = 0.0f;
			m31 = 0.0f; m32 = 0.0f; m33 = 1.0f; m34 = 0.0f;
			m41 = x;	m42 = y;	m43 = z;	m44 = 1.0f;
		}

		void Matrix::Translation(const Vector3& vT)
		{
			m11 = 1.0f; m12 = 0.0f; m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f; m22 = 1.0f; m23 = 0.0f; m24 = 0.0f;
			m31 = 0.0f; m32 = 0.0f; m33 = 1.0f; m34 = 0.0f;
			m41 = vT.x;	m42 = vT.y;	m43 = vT.z; m44 = 1.0f;
		}

		void Matrix::RotationX(float a)
		{
			m11 = 1.0f; m12 = 0.0f; m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f;							m24 = 0.0f;
			m31 = 0.0f;							m34 = 0.0f;
			m41 = 0.0f;							m44 = 1.0f;
			
			m22 = m33 = cos(a);
			m23 = sin(a);
			m32 = -m23;
		}

		void Matrix::RotationY(float a)
		{
						m12 = 0.0f;				m14 = 0.0f;
			m21 = 0.0f;	m22 = 1.0f;	m23 = 0.0f;	m24 = 0.0f;
						m32 = 0.0f;				m34 = 0.0f;
			m41 = 0.0f;	m42 = 0.0f;	m43 = 0.0f;	m44 = 1.0f;
			
			m11 = m33 = cos(a);
			m31 = sin(a);
			m13 = -m31;
		}

		void Matrix::RotationZ(float a)
		{
									m13 = 0.0f;	m14 = 0.0f;
									m23 = 0.0f;	m24 = 0.0f;
			m31 = 0.0f;	m32 = 0.0f;	m33 = 1.0f; m34 = 0.0f;
			m41 = 0.0f;	m42 = 0.0f;	m43 = 0.0f;	m44 = 1.0f;
			
			m11 = m22 = cos(a);
			m12 = sin(a);
			m21 = -m12;
		}
		
		void Matrix::Scale(float x, float y, float z)
		{
			m11 = x;	m12 = 0.0f;	m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f; m22 = y;	m23 = 0.0f; m24 = 0.0f;
			m31 = 0.0f; m32 = 0.0f; m33 = z;	m34 = 0.0f;
			m41 = 0.0f; m42 = 0.0f; m43 = 0.0f; m44 = 1.0f;
		}

		void Matrix::Scale(const Vector3& vS)
		{
			m11 = vS.x;	m12 = 0.0f;	m13 = 0.0f; m14 = 0.0f;
			m21 = 0.0f; m22 = vS.y;	m23 = 0.0f; m24 = 0.0f;
			m31 = 0.0f; m32 = 0.0f; m33 = vS.z;	m34 = 0.0f;
			m41 = 0.0f; m42 = 0.0f; m43 = 0.0f; m44 = 1.0f;
		}

		void Matrix::Axis(const Vector3& vX, const Vector3& vY, const Vector3& vZ)
		{
			m11 = vX.x; m12 = vX.y; m13 = vX.z; m14 = 0.0f;
			m21 = vY.x; m22 = vY.y; m23 = vY.z; m24 = 0.0f;
			m31 = vZ.x; m32 = vZ.y; m33 = vZ.z; m34 = 0.0f;
			m41 = 0.0f; m42 = 0.0f; m43 = 0.0f; m44 = 1.0f;
		}

		// vector manipulation

		Vector3& Matrix::TransformCoord(Vector3& vOut) const
		{
			float vX = vOut.x, vY = vOut.y, vZ = vOut.z;

			vOut.x = vX * m11 + vY * m21 + vZ * m31 + m41;
			vOut.y = vX * m12 + vY * m22 + vZ * m32 + m42;
			vOut.z = vX * m13 + vY * m23 + vZ * m33 + m43;

			const float  w = vX * m14 + vY * m24 + vZ * m34 + m44;

			if(w != 1.0f)
				vOut /= w;

			return vOut;
		}

		Vector3 Matrix::TransformCoord(const Vector3& vIn) const
		{
			Vector3 vOut;

			vOut.x = vIn.x * m11 + vIn.y * m21 + vIn.z * m31 + m41;
			vOut.y = vIn.x * m12 + vIn.y * m22 + vIn.z * m32 + m42;
			vOut.z = vIn.x * m13 + vIn.y * m23 + vIn.z * m33 + m43;

			const float  w = vIn.x * m14 + vIn.y * m24 + vIn.z * m34 + m44;

			if(w != 1.0f)
				vOut /= w;

			return vOut;
		}

		Vector3& Matrix::TransformNormal(Vector3& vOut) const
		{
			const float length = vOut.length();
			if(length == 0.0f)
				return vOut;

			const Matrix mNew = inverse().transpose();

			float vX = vOut.x, vY = vOut.y, vZ = vOut.z;

			vOut.x = vX * mNew.m11 + vY * mNew.m21 + vZ * mNew.m31;
			vOut.y = vX * mNew.m12 + vY * mNew.m22 + vZ * mNew.m32;
			vOut.z = vX * mNew.m13 + vY * mNew.m23 + vZ * mNew.m33;

			return vOut;
		}

		Vector3 Matrix::TransformNormal(const Vector3& vIn) const
		{
			const float length = vIn.length();
			if(length == 0.0f)
				return vIn;

			const Matrix mNew = inverse().transpose();

			return Vector3(	vIn.x * mNew.m11 + vIn.y * mNew.m21 + vIn.z * mNew.m31,
							vIn.x * mNew.m12 + vIn.y * mNew.m22 + vIn.z * mNew.m32,
							vIn.x * mNew.m13 + vIn.y * mNew.m23 + vIn.z * mNew.m33 );
		}

		Vector4 Matrix::TransformVector4(const Vector4& vIn) const
		{
			return Vector4( m11 * vIn.x + m21 * vIn.y + m31 * vIn.z + m41 * vIn.w,
							m12 * vIn.x + m22 * vIn.y + m32 * vIn.z + m42 * vIn.w,
							m13 * vIn.x + m23 * vIn.y + m33 * vIn.z + m43 * vIn.w,
							m14 * vIn.x + m24 * vIn.y + m34 * vIn.z + m44 * vIn.w);
		}

		Plane Matrix::TransformPlane(const Plane& plane) const
		{
			const Vector3& vNrm = plane.GetNormal();
			const float inD = plane.GetD();
			float a = m11 * vNrm.x + m21 * vNrm.y + m31 * vNrm.z + m41 * inD;
			float b = m12 * vNrm.x + m22 * vNrm.y + m32 * vNrm.z + m42 * inD;
			float c = m13 * vNrm.x + m23 * vNrm.y + m33 * vNrm.z + m43 * inD;
			float d = m14 * vNrm.x + m24 * vNrm.y + m34 * vNrm.z + m44 * inD;
			return Plane(a, b, c, d);
		}

		Matrix MatIdentity(void)
		{
			return Matrix(	1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 1.0f, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix Matrix::inverse(void) const
		{
			float det = Det();
			if(!det)
				return MatIdentity();

			Vector4 vec[3];

			Matrix mOut;
			for(int i=0; i<4; i++)
			{
				for (int j=0; j<4; j++)
				{
					if (j != i )
					{
						int a = j;
						if(j > i)
							a = a-1;
						vec[a].x = m[j][0];
						vec[a].y = m[j][1];
						vec[a].z = m[j][2];
						vec[a].w = m[j][3];
					}
				}
				Vector4 v = vec[0].Cross(vec[1], vec[2]);

				for(int j= 0; j<4; j++)
				{
					float cofactor;
					switch(j)
					{
						case 0: cofactor = v.x; break;
						case 1: cofactor = v.y; break;
						case 2: cofactor = v.z; break;
						case 3: cofactor = v.w; break;
					}
					mOut.m[j][i] = pow(-1.0f, i) * cofactor / det;
				}
			}

			return mOut;
		}

		Matrix Matrix::transpose(void) const
		{
			return Matrix(	m11, m21, m31, m41,
							m12, m22, m32, m42,
							m13, m23, m33, m43,
							m14, m24, m34, m44 );
		}

		/**************************************************
		* Transform matricies
		**************************************************/

		Matrix MatTranslation(const Vector3& vTranslation)
		{
			return Matrix(1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, 1.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 1.0f, 0.0f,
							vTranslation.x, vTranslation.y, vTranslation.z, 1.0f);
		}

		Matrix MatTranslation(float x, float y, float z)
		{
			return Matrix(1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				x, y, z, 1.0f);
		}

		Matrix MatRotationX(float a)
		{
			float c = cos(a);
			float s = sin(a);
			return Matrix (	1.0f, 0.0f, 0.0f, 0.0f,
							0.0f, c,    s,	  0.0f,
							0.0f, -s,	c,	  0.0f,
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatRotationY(float a)
		{
			float c = cos(a);
			float s = sin(a);
			return Matrix (	c,	  0.0f, -s,   0.0f,
							0.0f, 1.0f, 0.0f, 0.0f,
							s,    0.0f,	c,	  0.0f,
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatRotationZ(float a)
		{
			float c = cos(a);
			float s = sin(a);
			return Matrix (	c,	  s,	0.0f, 0.0f,
							-s,	  c,	0.0f, 0.0f,
							0.0f, 0.0f,	1.0f, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatYawPitchRoll(float y, float x, float z)
		{
			return MatRotationY(y) * MatRotationX(x) * MatRotationZ(z);
		}

		Matrix MatRotationAxis(const Vector3& vAxis, const float a)
		{
			const float s = sin(-a), c = cos(-a);
			const float oneMinusCos = 1.0f - c;

			const Vector3 vNrmAxis = vAxis.normal();

			return Matrix(	(vNrmAxis.x * vNrmAxis.x) * oneMinusCos + c,
							(vNrmAxis.x * vNrmAxis.y) * oneMinusCos - (vNrmAxis.z * s),
							(vNrmAxis.x * vNrmAxis.z) * oneMinusCos + (vNrmAxis.y * s), 0.0f,
							(vNrmAxis.y * vNrmAxis.x) * oneMinusCos + (vNrmAxis.z * s),
							(vNrmAxis.y * vNrmAxis.y) * oneMinusCos + c,
							(vNrmAxis.y * vNrmAxis.z) * oneMinusCos - (vNrmAxis.x * s), 0.0f,
							(vNrmAxis.z * vNrmAxis.x) * oneMinusCos - (vNrmAxis.y * s),
							(vNrmAxis.z * vNrmAxis.y) * oneMinusCos + (vNrmAxis.x * s),
							(vNrmAxis.z * vNrmAxis.z) * oneMinusCos + c, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatScale(float x, float y, float z)
		{
			return Matrix(	x,	  0.0f, 0.0f, 0.0f,
							0.0f, y,    0.0f, 0.0f,
							0.0f, 0.0f, z,    0.0f, 
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatScale(const Vector3& vScale)
		{
			return Matrix(	vScale.x,	  0.0f, 0.0f, 0.0f,
							0.0f, vScale.y,    0.0f, 0.0f,
							0.0f, 0.0f, vScale.z,    0.0f, 
							0.0f, 0.0f, 0.0f, 1.0f);
		}

		Matrix MatAxis(const Vector3& vX, const Vector3& vY, const Vector3& vZ)
		{
			return Matrix(	vX.x, vX.y, vX.z, 0.0f,
							vY.x, vY.y, vY.z, 0.0f,
							vZ.x, vZ.y, vZ.z, 0.0f,
							0.0f, 0.0f, 0.0f, 1.0f );
		}

		/**************************************************
		* Projection matricies
		**************************************************/

		Matrix MatPerspFovLH(float fovy, float aspect, float zn, float zf)
		{
			float yScale = cotan(fovy/2.0f);
			float xScale = yScale / aspect;

			return Matrix(	xScale, 0.0f,	0.0f,			0.0f,
							0.0f,	yScale,	0.0f,			0.0f,
							0.0f,	0.0f,	zf/(zf-zn),		1.0,
							0.0f,	0.0f,	-zn*zf/(zf-zn),	0.0f);
		}

		Matrix MatPerspectiveRH(float w, float h, float zn, float zf)
		{
			return Matrix(	2*zn/w, 0.0f,	0.0f,			0.0f,
							0.0f,	2*zn/h, 0.0f,			0.0f,
							0.0f,	0.0f,	zf/(zn-zf),		-1,
							0.0f,	0.0f,	zn*zf/(zn-zf),	0.0f );
		}

		Matrix MatOrthoLH(float w, float h, float zn, float zf)
		{
			return Matrix(	2.0f/w, 0.0f,	0.0f,			0.0f,
							0.0f,	2.0f/h, 0.0f,			0.0f,
							0.0f,	0.0f,	1/(zf-zn),		0.0f,

							0.0f,	0.0f,	zn/(zn-zf),		1.0f );
		}

		Matrix MatOrthoOffCenterLH(float l, float r, float b, float t, float zn, float zf)
		{
			return Matrix(2 / (r - l), 0.0f, 0.0f, 0.0f,
				0.0f, 2 / (t - b), 0.0f, 0.0f,
				0.0f, 0.0f, 1 / (zf - zn), 0.0f,
				(l + r) / (l - r), (t + b) / (b - t), zn / (zn - zf), 1.0f);
		}

		/**************************************************
		* View matrices
		**************************************************/

		Matrix MatLookAt(const Vector3& vEye, const Vector3& vAt, const Vector3& vUp)
		{
			Vector3 vZ = (vAt - vEye).normal();
			Vector3 vX = (vUp.Cross(vZ)).normal();
			Vector3 vY = vZ.Cross(vX);

			return Matrix(	vX.x,			vY.x,			vZ.x,			0.0f,
							vX.y,			vY.y,			vZ.y,			0.0f,
							vX.z,			vY.z,			vZ.z,			0.0f,
							-vX.Dot(vEye), -vY.Dot(vEye),	-vZ.Dot(vEye),	1.0f);
		}

		Matrix MatLookAtRH(const Vector3& vEye, const Vector3& vAt, const Vector3& vUp)
		{
			Vector3 vZ = (vEye - vAt).normal();
			Vector3 vX = (vUp.Cross(vZ)).normal();
			Vector3 vY = vZ.Cross(vX);

			return Matrix(	vX.x,			vY.x,			vZ.x,			0.0f,
							vX.y,			vY.y,			vZ.y,			0.0f,
							vX.z,			vY.z,			vZ.z,			0.0f,
							vX.Dot(vEye),   vY.Dot(vEye),	vZ.Dot(vEye),	1.0f);
		}

		Vector3 Vec3Unproject(const Vector3& vPoint, const Rect& viewport, const Matrix& mProjection, const Matrix& mView, const Matrix& mWorld)
		{
			Matrix m1, m2, m3;
			Vector3 vec;

			m1 = mWorld * mView;
			m2 = m1 * mProjection;
			m3 = m2.inverse();
			vec.x = 2.0f * ( vPoint.x - viewport.x ) / viewport.width - 1.0f;
			vec.y = 1.0f - 2.0f * ( vPoint.y - viewport.y ) / viewport.height;
			vec.z = ( vPoint.z - 0.0f) / ( 1.0f - 0.0f );//( vPoint.z - viewport.MinZ) / ( viewport.MaxZ - viewport.MinZ );
			return m3.TransformCoord(vec);
		}

		Vector3  Vec3Project(const Vector3& vPoint, const Rect& viewport, const Matrix& mProjection, const Matrix& mView, const Matrix& mWorld)
		{
			Matrix m1, m2;
			Vector3 vec;

			m1 = mWorld * mView;
			m2 = m1 * mProjection;
			vec = m2.TransformCoord(vPoint);
			vec.x = viewport.x +  ( 1.0f + vec.x ) * viewport.width / 2.0f;
			vec.y = viewport.y +  ( 1.0f - vec.y ) * viewport.height / 2.0f;
			vec.z = 0.0f + vec.z * ( 1.0f - 0.0f );//viewport.MinZ + vec.z * ( viewport.MaxZ - viewport.MinZ );
			return vec;
		}

	}
}