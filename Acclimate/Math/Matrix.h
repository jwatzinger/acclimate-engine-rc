#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace math
	{
		struct Vector3;
		struct Vector4;
		struct Rect;
		class Plane;

		struct ACCLIMATE_API Matrix
		{
			Matrix(void);
			Matrix(const Matrix& matrix);
			Matrix(float c11, float c12, float c13, float c14,
				   float c21, float c22, float c23, float c24,
				   float c31, float c32, float c33, float c34,
				   float c41, float c42, float c43, float c44);

			// matrix members
			union
			{
				struct
				{
					float m11, m12, m13, m14,
						  m21, m22, m23, m24,
						  m31, m32, m33, m34,
						  m41, m42, m43, m44;
				};

				float m[4][4];
			};

			float& operator() (int row, int column);
			float operator() (int row, int column) const;
			Matrix operator*(const Matrix& matrix) const;
			Matrix& operator*=(const Matrix& matrix);

			float Det(void) const;

			// special matrix creation
			void Identity(void);
			void Inverse(void);
			Matrix inverse(void) const;
			Matrix transpose(void) const;
			void Translation(float x, float y, float z);
			void Translation(const Vector3& vTranslation);
			void RotationX(float a);
			void RotationY(float a);
			void RotationZ(float a);
			void Scale(float x, float y, float z);
			void Scale(const Vector3& vScale);
			void Axis(const Vector3& vX, const Vector3& vY, const Vector3& vZ);

			// vector manipulation
			Vector3& TransformCoord(Vector3& vOut) const;
			Vector3 TransformCoord(const Vector3& vIn) const;
			Vector3& TransformNormal(Vector3& vOut) const;
			Vector3 TransformNormal(const Vector3& vIn) const;
			Vector4 TransformVector4(const Vector4& vIn) const;
			Plane TransformPlane(const Plane& plane) const;

		};

		ACCLIMATE_API Matrix MatIdentity(void);

		/**************************************************
		* Transform matricies
		**************************************************/

		ACCLIMATE_API Matrix MatTranslation(const Vector3& vTranslation);
		ACCLIMATE_API Matrix MatTranslation(float x, float y, float z);
		ACCLIMATE_API Matrix MatRotationX(float a);
		ACCLIMATE_API Matrix MatRotationY(float a);
		ACCLIMATE_API Matrix MatRotationZ(float a);
		ACCLIMATE_API Matrix MatYawPitchRoll(float y, float x, float z);
		ACCLIMATE_API Matrix MatRotationAxis(const Vector3& vAxis, const float a);
		ACCLIMATE_API Matrix MatScale(float x, float y, float z);
		ACCLIMATE_API Matrix MatScale(const Vector3& vScale);
		ACCLIMATE_API Matrix MatAxis(const Vector3& vX, const Vector3& vY, const Vector3& vZ);

		/**************************************************
		* Projection matricies
		**************************************************/

		ACCLIMATE_API Matrix MatPerspFovLH(float fovy, float aspect, float zn, float zf);
		ACCLIMATE_API Matrix MatPerspectiveRH(float w, float h, float zn, float zf);
		ACCLIMATE_API Matrix MatOrthoLH(float w, float h, float zn, float zf);
		ACCLIMATE_API Matrix MatOrthoOffCenterLH(float l, float r, float b, float t, float zn, float zf);

		/**************************************************
		* View matricies
		**************************************************/

		ACCLIMATE_API Matrix MatLookAt(const Vector3& vEye, const Vector3& vAt, const Vector3& vUp);
		ACCLIMATE_API Matrix MatLookAtRH(const Vector3& vEye, const Vector3& vAt, const Vector3& vUp);

		/**************************************************
		* Todo: Remove from here
		**************************************************/

		ACCLIMATE_API Vector3 Vec3Unproject(const Vector3& vPoint, const Rect& viewport, const Matrix& mProjection, const Matrix& mView, const Matrix& mWorld);

		ACCLIMATE_API Vector3 Vec3Project(const Vector3& vPoint, const Rect& viewport, const Matrix& mProjection, const Matrix& mView, const Matrix& mWorld);

	}
}

