#include "OBB.h"
#include "Volumes.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace math
    {

        OBB::OBB(const Vector3& vCenter, const Vector3& vNrm1, const Vector3& vNrm2, const Vector3& vNrm3): BaseVolume(vCenter, Volumes::OBB)
        {
            m_vNrm[0] = vNrm1.normal();
            m_vNrm[1] = vNrm2.normal();
            m_vNrm[2] = vNrm3.normal();

            m_vExtent.x = vNrm1.length();
            m_vExtent.y = vNrm2.length();
            m_vExtent.z = vNrm3.length();
        }

        float OBB::MaxExtents(void) const
        {
            return m_vExtent.length();
        }

        const Vector3& OBB::GetExtent(void) const
        {
            return m_vExtent;
        }

        const Vector3& OBB::GetNormal(size_t id) const
        {
            ACL_ASSERT(id < 3);

            return m_vNrm[id];
        }

		Vector3 OBB::ClosestPoint(const Vector3& point) const
		{
			Vector3 retPt;
			Vector3 d = point - m_vCenter;

			retPt = m_vCenter;

			const float* pExtents = (float*)&m_vExtent;

			for (int i = 0; i < 3; ++i)
			{
				float dist = d.Dot(m_vNrm[i]);

				if (dist > pExtents[i]) 
					dist = pExtents[i];
				if (dist < -pExtents[i]) 
					dist = -pExtents[i];

				retPt += dist * m_vNrm[i];
			}

			return retPt;
		}

    }
}
