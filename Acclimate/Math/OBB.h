#pragma once
#include "BaseVolume.h"

namespace acl
{
    namespace math
    {

        class OBB :
            public BaseVolume
        {
        public:
            OBB(const Vector3& vCenter, const Vector3& vNrm1, const Vector3& vNrm2, const Vector3& vNrm3);

            float MaxExtents(void) const;
            const Vector3& GetExtent(void) const;
            const Vector3& GetNormal(size_t id) const;

			Vector3 ClosestPoint(const Vector3& point) const;

        private:

            Vector3 m_vExtent;
            Vector3 m_vNrm[3];
        };

    }
}
