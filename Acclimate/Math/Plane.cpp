#include "Plane.h"
#include <math.h>
#include "Ray.h"
#include "Vector3.h"

namespace acl
{
    namespace math
    {

        Plane::Plane(void): m_d(0.0f)
        {
        }

        Plane::Plane(float a, float b, float c, float d): m_d(d), m_vNormal(a, b, c)
        {
			m_vNormal.Normalize();
        }

		Plane::Plane(const Vector3& vNormal, const Vector3& vPoint): m_vNormal(vNormal), m_d(-vPoint.Dot(vNormal))
		{
		}

        void Plane::Set(float a, float b, float c, float d)
        {
			m_vNormal.x = a;
			m_vNormal.y = b;
			m_vNormal.z = c;
			m_d = d;
        }

		void Plane::Set(const Vector3& vNormal, const Vector3& vPoint)
        {
			m_vNormal = vNormal;
			m_d = -vPoint.Dot(vNormal);
        }

		bool Plane::IntersectLine(const Vector3& vPoint1, const Vector3& vPoint2, Vector3* pOut) const
		{
			Vector3 vDirection(vPoint2 - vPoint1);
		    float dot = m_vNormal.Dot(vDirection);
		    if(!dot) 
				return false;

		    float temp = ( m_d + m_vNormal.Dot(vPoint1) ) / dot;
			if(pOut)
				*pOut = vPoint1 - vDirection * temp;
		    return true;
		}

        bool Plane::RayIntersect(const Ray& ray, Vector3* vOut) const
        {
            float dot = ray.m_vOrigin.Dot(m_vNormal);
            float nrmDot = ray.m_vDirection.Dot(m_vNormal); 

            if(abs(nrmDot) <= 0.001f)
                return false;

            float t = (m_d - dot) / nrmDot;

            Vector3 vIntersect;

            if(vOut)
                *vOut = ray.m_vOrigin + t * ray.m_vDirection;

            return true;
        }
		
		float Plane::Dot(const Vector3& v) const
		{
			return m_vNormal.x * v.x + m_vNormal.y * v.y + m_vNormal.z * v.z + m_d;
		}

		void Plane::Normalize(void)
		{   
			float norm = m_vNormal.length();
			if(norm)
			{
				m_vNormal /= norm;
				m_d /= norm;
			}
			else
			{
				m_vNormal = Vector3(0.0f, 0.0f, 0.0f);
				m_d = 0.0f;
			}
		}

    }
}