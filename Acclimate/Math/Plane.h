#pragma once
#include "Vector3.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        class AABB;

        struct Ray;

		/// Plance structure
		/** This structure represants a 3d infinite plane. Its 
		*	and a direction. */
        class ACCLIMATE_API Plane
        {
        public:
	        Plane(void);
	        Plane(float a, float b, float c, float d);
			Plane(const Vector3& vNormal, const Vector3& vPoint);

	        void Set(float a, float b, float c, float d);
			void Set(const Vector3& vNormal, const Vector3& vPoint);

			inline float GetD(void) const
			{
				return m_d;
			}

			inline const Vector3& GetNormal(void) const
			{
				return m_vNormal;
			}

			bool IntersectLine(const Vector3& vPoint1, const Vector3& vPoint2, Vector3* pOut = nullptr) const;
	        bool RayIntersect(const Ray& ray, Vector3* vOut = nullptr) const;

			float Dot(const Vector3& v) const;
			void Normalize(void);

        private:

	        Vector3 m_vNormal;
			float m_d;
        };

    }
}
