#include "Quaternion.h"
#include <math.h>
#include "Vector3.h"

namespace acl
{
	namespace math
	{

		Quaternion::Quaternion(void) : w(1.0f), x(0.0f), y(0.0f), z(0.0f)
		{
		}

		Quaternion::Quaternion(float w, float x, float y, float z) : w(w), x(x), y(y), z(z)
		{
		}

		Quaternion::Quaternion(float w, const Vector3& v) : w(w), x(v.x), y(v.y), z(v.z)
		{
		}

		Quaternion& Quaternion::operator+=(const Quaternion &q)
		{
			w += q.w;
			x += q.x;
			y += q.y;
			z += q.z;
			return *this;
		}

		Quaternion Quaternion::operator + (const Quaternion& q) const
		{
			return Quaternion(w + q.w, x + q.x, y + q.y, z + q.z);
		}

		Quaternion Quaternion::operator*(const Quaternion& q) const
		{
			return Quaternion(w*q.w - x*q.x - y*q.y - z*q.z,
				w*q.x + x*q.w + y*q.z - z*q.y,
				w*q.y - x*q.z + y*q.w + z*q.x,
				w*q.z + x*q.y - y*q.x + z*q.w);
		}

		Quaternion Quaternion::operator*(float value) const
		{
			return Quaternion(w*value, x*value, y*value, z*value);
		}

		Quaternion operator*(float s, const Quaternion &q)
		{
			return Quaternion(q.w*s, q.x*s, q.y*s, q.z*s);
		}

		void Quaternion::Identity(void)
		{
			w = 1.0f;
			x = 0.0f;
			y = 0.0f;
			z = 0.0f;
		}

		void Quaternion::Normalize(void)
		{
			const float length = Length();

			if(length == 0.0f)
			{
				w = 1.0f;
				x = 0.0f;
				y = 0.0f;
				z = 0.0f;
			}
			else
			{
				const float inv = 1.0f / length;
				x *= inv;
				y *= inv;
				z *= inv;
				w *= inv;
			}
		}

		float Quaternion::Dot(const Quaternion& q) const
		{
			return x * q.x + y * q.y + z * q.z + w * q.w;
		}

		float Quaternion::Yaw(void) const
		{
			return atan2(2.0f*x*w + 2.0f*y*z, 1 - 2.0f *(z*z + w*w));
		}

		float Quaternion::Pitch(void) const
		{
			return asin(2.0f * (x*z - w*y) );
		}

		float Quaternion::Roll(void) const
		{
			return atan2(2.0f * x * y + 2.0f * z * w, 1 - 2.0f * (y*y + z*z));
		}

		float Quaternion::Length(void) const
		{
			return sqrt(w*w + x*x + y*y + z*z);
		}

		Matrix Quaternion::Matrix(void) const
		{
			return math::Matrix(1.0f - 2 * (y * y + z * z), 2.0f * (x * y + z * w), 2.0f * (x * z - y * w), 0.0f,
								2.0f * (x * y - z * w), 1.0f - 2.0f * (x * x + z * z), 2.0f * (y * z + x * w), 0.0f,
								2.0f * (x * z + y * w), 2.0f * (y * z - x * w), 1.0f - 2.0f * (x * x + y * y), 0.0f,
								0.0f, 0.0f, 0.0f, 1.0f);
		}

		Quaternion interpolate(const Quaternion& a, const Quaternion& b, float t)
		{
			float dot, epsilon = 1.0f;

			dot = a.Dot(b);
			if(dot < 0.0f)
				epsilon = -1.0f;
			Quaternion out;
			out.x = (1.0f - t) * a.x + epsilon * t * b.x;
			out.y = (1.0f - t) * a.y + epsilon * t * b.y;
			out.z = (1.0f - t) * a.z + epsilon * t * b.z;
			out.w = (1.0f - t) * a.w + epsilon * t * b.w;
			return out;
		}

	}
}
