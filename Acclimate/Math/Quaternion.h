#pragma once
#include "Matrix.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace math
	{

		struct Vector3;

		struct ACCLIMATE_API Quaternion
		{
		public:
			Quaternion(void);
			Quaternion(float w, float x, float y, float z);
			Quaternion(float w, const Vector3& v);

			Quaternion& operator+=(const Quaternion& q);
			Quaternion operator+(const Quaternion& q) const;
			Quaternion operator*(const Quaternion& q) const;
			Quaternion operator*(float value) const;

			friend Quaternion operator*(float s, const Quaternion &q);

			void Identity(void);
			void Normalize(void);
			float Dot(const Quaternion& q) const;

			float Yaw(void) const;
			float Pitch(void) const;
			float Roll(void) const;
			float Length(void) const;
			Matrix Matrix(void) const;

			float w, x, y, z;
		};

		ACCLIMATE_API Quaternion interpolate(const Quaternion& a, const Quaternion& b, float alpha);

	}
}

