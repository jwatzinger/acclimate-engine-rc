#include "Ray.h"

namespace acl
{
    namespace math
    {

        Ray::Ray(const Vector3& vOrigin, const Vector3& vDirection): m_vOrigin(vOrigin), m_vDirection(vDirection), m_vDirFrac(1.0f, 1.0f, 1.0f)
	    {
            if(m_vDirection.x != 0.0f)
		        m_vDirFrac.x /= m_vDirection.x;
            if(m_vDirection.y != 0.0f)
		        m_vDirFrac.y /= m_vDirection.y;
            if(m_vDirection.z != 0.0f)
		        m_vDirFrac.z /= m_vDirection.z;
	    }

    }
}