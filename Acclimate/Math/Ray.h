#pragma once
#include "Vector3.h"

namespace acl
{
    namespace math
    {
		/// Directional ray struct
		/** This structure is the mathematical representation of a ray, consisting of a certain position 
		*	and a direction. */
        struct Ray 
        {
        public:

	        Ray(const Vector3& vOrigin, const Vector3& vDirection);

	        Vector3 m_vOrigin,		///< Ray origin
					m_vDirection,	///< Ray direction
					m_vDirFrac;		///< Inverse ray direction
        };

    }
}