#include "Rect.h"
#include "Vector.h"

namespace acl
{
    namespace math
    {

	    Rect::Rect(void): x(0), y(0), width(0), height(0)
	    {
	    }

	    Rect::Rect(int x, int y, int width, int height): x(x), y(y), width(width), height(height)
	    {
	    }

		bool Rect::operator!=(const Rect& r) const
		{
			return x != r.x || y != r.y || width != r.width || height == r.height;
		}

		Rect& Rect::operator+=(const Rect& r)
		{
			x += r.x;
			y += r.y;
			width += r.width;
			height += r.height;

			return *this;
		}
	
	    void Rect::Set(int x, int y, int width, int height)
	    {
		    this->x = x, this->y = y, this->width = width, this->height = height;
	    }

	    bool Rect::Inside(const Vector2& vPoint) const
	    {
		    int distanceX = vPoint.x - x;
		    int distanceY = vPoint.y - y;
		    return distanceX >= 0 && distanceX <= width && distanceY >= 0 && distanceY <= height;
	    }
		
		bool Rect::Inside(const Rect& rect) const
		{
		    int distanceX = rect.x - x;
		    int distanceY = rect.y - y;
		    return distanceX >= -rect.width && distanceX <= width && distanceY >= -rect.height && distanceY <= height;
		}

	    Rect Rect::Merge(const Rect& rect) const
	    {
		    int x = max(this->x, rect.x);
		    int y = max(this->y, rect.y);
		    int x2 = min(this->x+this->width, rect.x+rect.width);
		    int y2 = min(this->y+this->height, rect.y+rect.height);
		    return Rect(x, y, x2-x, y2-y);
	    }

	    RECT Rect::GetWinRect(void) const
	    {
		    RECT r = {x, y, width, height};
		    return r;
	    }

    }
}