#pragma once
#include "..\Core\Dll.h"
#include <Windows.h>


namespace acl
{
    namespace math
    {

		struct Vector2;
		/// 2D rectangle structure
		/** This struct represents a rectangle by its x/y positions and its extents, in pixel. */
        struct ACCLIMATE_API Rect
        {
	        Rect(void);
			Rect(int x, int y, int width, int height);

			bool operator!=(const Rect& r) const;
			Rect& operator+=(const Rect& r);
	
			/** Sets the rectangles metrics.
			 *
			 *	@param[in] x
			 *	@param[in] y
			 *	@param[in] width
			 *	@param[in] height
			 *
			 *	@return Whether the two spheres intersected.
			 */
	        void Set(int x, int y, int width, int height);

			/** Checks if point lies within the rectangle.
			 *
			 *	@param[in] vPoint Point of 2d coordinates
			 *
			 *	@return Whether the point is inside.
			 */
	        bool Inside(const Vector2& vPoint) const;

			/** Checks if the rects intersect
			 *
			 *	@param[in] rect
			 *
			 *	@return Whether the rect is inside.
			 */
	        bool Inside(const Rect& rect) const;

			/** Creates a new Rect as the intersection area of this and another rectangle.
			 *
			 *	@param[in] rect Rectangle to "merge"
			 *
			 *	@return Cut set of the two rectangles
			 */
	        Rect Merge(const Rect& rect) const;

			/** Creates windows rectangle off this.
			 *
			 *	@return Cut set of the two rectangles
			 */
	        RECT GetWinRect(void) const;

	        int x,			///< Rectangle x coordinate, in pixel
				y,			///< Rectangle y coordinate, in pixel
				width,		///< Rectangle width, in pixel
				height;		///< Rectangle height, in pixel
        };

    }
}