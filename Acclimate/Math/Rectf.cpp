#include "Rectf.h"
#include "Vector2f.h"

namespace acl
{
    namespace math
    {

	    Rectf::Rectf(void): x(0.0f), y(0.0f), width(0.0f), height(0.0f)
	    {
	    }

	    Rectf::Rectf(float x, float y, float width, float height): x(x), y(y), width(width), height(height)
	    {
	    }
	
	    void Rectf::Set(float x, float y, float width, float height)
	    {
		    this->x = x, this->y = y, this->width = width, this->height = height;
	    }

	    bool Rectf::Inside(const Vector2f& vPoint) const
	    {
		    float distanceX = vPoint.x - x;
		    float distanceY = vPoint.y - y;
		    return distanceX >= 0.0f && distanceX <= width && distanceY >= 0.0f && distanceY <= height;
	    }
		
		bool Rectf::Inside(const Rectf& rect) const
		{
		    float distanceX = rect.x - x;
		    float distanceY = rect.y - y;
		    return distanceX >= -rect.width && distanceX <= width && distanceY >= -rect.height && distanceY <= height;
		}

	    Rectf Rectf::Merge(const Rectf& rect) const
	    {
		    float x = max(this->x, rect.x);
		    float y = max(this->y, rect.y);
		    float x2 = min(this->x+this->width, rect.x+rect.width);
		    float y2 = min(this->y+this->height, rect.y+rect.height);
		    return Rectf(x, y, x2-x, y2-y);
	    }

	    RECT Rectf::GetWinRect(void) const
	    {
		    RECT r = {(int)x, (int)y, (int)width, (int)height};
		    return r;
	    }

    }
}