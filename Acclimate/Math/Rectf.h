#pragma once
#include "..\Core\Dll.h"
#include <Windows.h>

namespace acl
{
    namespace math
    {

		struct Vector2f;

		/// 2D rectangle structure
		/** This struct represents a rectangle by its x/y positions and its extents, in pixel. */
        struct ACCLIMATE_API Rectf
        {
	        Rectf(void);
	        Rectf(float x, float y, float width, float height);
	
			/** Sets the rectangles metrics.
			 *
			 *	@param[in] x
			 *	@param[in] y
			 *	@param[in] width
			 *	@param[in] height
			 *
			 *	@return Whether the two spheres intersected.
			 */
	        void Set(float x, float y, float width, float height);

			/** Checks if point lies within the rectangle.
			 *
			 *	@param[in] vPoint Point of 2d coordinates
			 *
			 *	@return Whether the point is inside.
			 */
	        bool Inside(const Vector2f& vPoint) const;

			/** Checks if the rects intersect
			 *
			 *	@param[in] rect
			 *
			 *	@return Whether the rect is inside.
			 */
	        bool Inside(const Rectf& rect) const;

			/** Creates a new Rectf as the intersection area of this and another rectangle.
			 *
			 *	@param[in] rect Rectfangle to "merge"
			 *
			 *	@return Cut set of the two rectangles
			 */
	        Rectf Merge(const Rectf& rect) const;

			/** Creates windows rectangle off this.
			 *
			 *	@return Cut set of the two rectangles
			 */
	        RECT GetWinRect(void) const;

	        float x,			///< Rectfangle x coordinate, in pixel
				y,			///< Rectfangle y coordinate, in pixel
				width,		///< Rectfangle width, in pixel
				height;		///< Rectfangle height, in pixel
        };

    }
}