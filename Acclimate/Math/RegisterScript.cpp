#include "RegisterScript.h"
#include <minmax.h>
#include <math.h>
#include "Vector.h"
#include "Vector2f.h"
#include "Vector3.h"
#include "Matrix.h"
#include "Ray.h"
#include "Utility.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace math
	{

		/*************************************
		* Vector2
		*************************************/

		void Vector2Construct(void *memory)
		{
			new(memory)Vector2f();
		}

		void Vector2Construct(float x, float y, void* memory)
		{
			new(memory)Vector2f(x, y);
		}

		void Vector2Construct(const Vector2f& vec, void* memory)
		{
			new(memory)Vector2f(vec);
		}

		/*************************************
		* Vector2i
		*************************************/

		void Vector2iConstruct(void *memory)
		{
			new(memory)Vector2();
		}

		void Vector2iConstruct(int x, int y, void* memory)
		{
			new(memory)Vector2(x, y);
		}

		/*************************************
		* Vector3
		*************************************/

		void Vector3Construct(void *memory)
		{
			new(memory) Vector3();
		}

		void Vector3Construct(float x, float y, float z, void* memory)
		{
			new(memory) Vector3(x, y, z);
		}

		void Vector3Construct(const Vector3& v, void* memory)
		{
			new(memory)Vector3(v);
		}

		void MatrixConstruct(void *memory)
		{
			new(memory) Matrix();
		}

		void RayConstruct(const Vector3& vOrigin, const Vector3& vDirection, void* memory)
		{
			new(memory) Ray(vOrigin, vDirection);
		}

		void RayCopyConstruct(const Ray& ray, void* memory)
		{
			new(memory) Ray(ray);
		}

		float maxWrapper(float a, float b)
		{
			return max(a, b);
		}

		float minWrapper(float a, float b)
		{
			return min(a, b);
		}

		float cap(float value, float minimum, float maximum)
		{
			return min(maximum, max(minimum, value));
		}

		void RegisterScript(script::Core& script)
		{
			// vector2
			auto vector2 = script.RegisterValueType<Vector2f>("Vector2", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR);
			vector2.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(Vector2Construct, (void*), void), asCALL_CDECL_OBJLAST);
			vector2.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(float, float)", asFUNCTIONPR(Vector2Construct, (float, float, void*), void), asCALL_CDECL_OBJLAST);
			vector2.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(float, float)", asFUNCTIONPR(Vector2Construct, (const Vector2f&, void*), void), asCALL_CDECL_OBJLAST);
			vector2.RegisterProperty("float x", asOFFSET(Vector2f, x));
			vector2.RegisterProperty("float y", asOFFSET(Vector2f, y));

			// vector2i
			auto vector2i = script.RegisterValueType<Vector2>("Vector2i", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR);
			vector2i.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(Vector2iConstruct, (void*), void), asCALL_CDECL_OBJLAST);
			vector2i.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(int, int)", asFUNCTIONPR(Vector2iConstruct, (int, int, void*), void), asCALL_CDECL_OBJLAST);
			vector2i.RegisterProperty("int x", asOFFSET(Vector2, x));
			vector2i.RegisterProperty("int y", asOFFSET(Vector2, y));

			// vector3
			auto vector3 = script.RegisterValueType<Vector3>("Vector3", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR);
			vector3.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(Vector3Construct, (void*), void), asCALL_CDECL_OBJLAST);
			vector3.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(float, float, float)", asFUNCTIONPR(Vector3Construct, (float, float, float, void*), void), asCALL_CDECL_OBJLAST);
			vector3.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(const Vector3& in)", asFUNCTIONPR(Vector3Construct, (const Vector3&, void*), void), asCALL_CDECL_OBJLAST);
			vector3.RegisterProperty("float x", asOFFSET(Vector3, x));
			vector3.RegisterProperty("float y", asOFFSET(Vector3, y));
			vector3.RegisterProperty("float z", asOFFSET(Vector3, z));
			vector3.RegisterMethod("Vector3 opAdd(const Vector3& in) const", asMETHOD(Vector3, operator+));
			vector3.RegisterMethod("Vector3 opSub(const Vector3& in) const", asMETHODPR(Vector3, operator-, (const Vector3&) const, Vector3));
			vector3.RegisterMethod("Vector3 opMul(float) const", asMETHODPR(Vector3, operator*, (float) const, Vector3));
			vector3.RegisterMethod("Vector3 opDiv(float) const", asMETHODPR(Vector3, operator/, (float) const, Vector3));
			vector3.RegisterMethod("Vector3 opNeg() const", asMETHODPR(Vector3, operator-, () const, Vector3));
			vector3.RegisterMethod("bool opEquals(const Vector3& in) const", asMETHOD(Vector3, operator==));
			vector3.RegisterMethod("Vector3& opAddAssign(const Vector3& in)", asMETHOD(Vector3, operator+=));
			vector3.RegisterMethod("Vector3& opSubAssign(const Vector3& in)", asMETHOD(Vector3, operator-=));
			vector3.RegisterMethod("Vector3& opMulAssign(float)", asMETHODPR(Vector3, operator*=, (float), Vector3&));
			vector3.RegisterMethod("Vector3& opDivAssign(float)", asMETHODPR(Vector3, operator/=, (float), Vector3&));
			vector3.RegisterMethod("Vector3 Cross(const Vector3& in) const", asMETHOD(Vector3, Cross));
			vector3.RegisterMethod("float Dot(const Vector3& in) const", asMETHOD(Vector3, Dot));
			vector3.RegisterMethod("Vector3& Normalize(void)", asMETHOD(Vector3, Normalize));
			vector3.RegisterMethod("Vector3 normal(void) const", asMETHOD(Vector3, normal));
			vector3.RegisterMethod("float length(void) const", asMETHOD(Vector3, length));

			// matrix
			auto matrix = script.RegisterValueType<Matrix>("Matrix", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR);
			matrix.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTIONPR(MatrixConstruct, (void*), void), asCALL_CDECL_OBJLAST);
			matrix.RegisterMethod("Vector3 TransformNormal(const Vector3& in) const", asMETHODPR(Matrix, TransformNormal, (const Vector3&) const, Vector3));

			script.RegisterGlobalFunction("Matrix MatRotationX(float)", asFUNCTION(MatRotationX));
			script.RegisterGlobalFunction("Matrix MatRotationY(float)", asFUNCTION(MatRotationY));
			script.RegisterGlobalFunction("Matrix MatRotationZ(float)", asFUNCTION(MatRotationZ));
			script.RegisterGlobalFunction("Matrix MatRotationAxis(const Vector3& in, float)", asFUNCTION(MatRotationAxis));
			script.RegisterGlobalFunction("float abs(float)", asFUNCTIONPR(std::abs, (float), float));
			script.RegisterGlobalFunction("float cap(float, float, float)", asFUNCTION(cap));
			script.RegisterGlobalFunction("float max(float, float)", asFUNCTION(maxWrapper));
			script.RegisterGlobalFunction("float min(float, float)", asFUNCTION(minWrapper));
			script.RegisterGlobalFunction("float acos(float)", asFUNCTIONPR(acos, (float), float));
			script.RegisterGlobalFunction("float atan2(float, float)", asFUNCTIONPR(atan2, (float, float), float));
			script.RegisterGlobalFunction("float degToRad(float)", asFUNCTION(degToRad));
			script.RegisterGlobalFunction("float radToDeg(float)", asFUNCTION(radToDeg));

			// ray
			auto ray = script.RegisterValueType<Ray>("Ray", script::VALUE_POD | script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR | script::VALUE_CLASS_COPY_CONSTRUCTOR);
			ray.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(const Vector3& in, const Vector3& in)", asFUNCTION(RayConstruct), asCALL_CDECL_OBJLAST);
			ray.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(const Ray& in)", asFUNCTION(RayCopyConstruct), asCALL_CDECL_OBJLAST);
			ray.RegisterProperty("Vector3 origin", asOFFSET(Ray, m_vOrigin));
			ray.RegisterProperty("Vector3 direction", asOFFSET(Ray, m_vDirection));
		}

	}
}
