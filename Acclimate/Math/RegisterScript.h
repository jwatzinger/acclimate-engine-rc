#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace math
	{

		void RegisterScript(script::Core& core);

	}
}