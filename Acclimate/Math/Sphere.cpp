#include "Sphere.h"
#include "Volumes.h"
#include "Plane.h"

namespace acl
{
    namespace math
    {

	    Sphere::Sphere(void) : BaseVolume(0.0f, 0.0f, 0.0f, Volumes::Sphere), m_radius(0.0f)
        { 
        }

	    Sphere::Sphere(float x, float y, float z, float radius) : BaseVolume(x, y, z, Volumes::Sphere), m_radius(radius)
        { 
        }

        Sphere::Sphere(const Vector3& vPos, float radius) : BaseVolume(vPos, Volumes::Sphere), m_radius(radius)
        { 
        }

		bool Sphere::InsidePlane(const Plane& plane) const
		{
			float dist = m_vCenter.Dot(plane.GetNormal()) - plane.GetD();

			return dist < m_radius && dist > 0.0f;
		}

    }
}