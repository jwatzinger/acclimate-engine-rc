#pragma once
#include "BaseVolume.h"

namespace acl
{
    namespace math
    {

        class Sphere : 
            public BaseVolume
        {
        public:

	        Sphere(void);
	        Sphere(float x, float y, float z, float radius);
            Sphere(const Vector3& vPos,  float radius);

	        float MaxExtents(void) const
	        {
		        return m_radius;
	        }

			bool InsidePlane(const Plane& plane) const;

	        float m_radius;
            
        };

    }
}

