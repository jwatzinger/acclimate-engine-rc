#include "Triangle.h"
#include "Ray.h"
#include "Volumes.h"
#include <cmath>

namespace acl
{
    namespace math
    {
        Triangle::Triangle(const Vector3& vP1, const Vector3& vP2, const Vector3& vP3): BaseVolume(0.0f, 0.0f, 0.0f, Volumes::Triangle), m_vP1(vP1), m_vP2(vP2), m_vP3(vP3)
        {
			math::Vector3 vAB = m_vP2 - m_vP1;
			math::Vector3 vAC = m_vP3 - m_vP1;
			m_vNormal = vAB.Cross(vAC).normal();
        }

		Triangle::Triangle() : BaseVolume(0.0f, 0.0f, 0.0f, Volumes::Triangle)
		{
		}

        bool Triangle::Intersect(const Ray& ray, Vector3* pOut) const
        {
            // calculate edges
            const Vector3 vEdge1(m_vP2 - m_vP1);
            const Vector3 vEdge2(m_vP3 - m_vP1);

            // needed for determinante and u
            const Vector3 vP( ray.m_vDirection.Cross(vEdge2) );

            // calculate determinante 
            const float det = vEdge1.Dot(vP);

            // perpendicular if determinante is near zero
            if(abs(det) < 0.0001f)
                return false;
            const float inv_det = 1.0f / det;

            // distance from first vertex to ray origin
            const Vector3 vT( ray.m_vOrigin - m_vP1 );

            // calculate u coordinate
            const float u = vT.Dot(vP) * inv_det;

            // test bounds
            if(u < 0.0f || u > 1.0f)
                return false;

            // used for v coordinate
            const Vector3 vQ = vT.Cross(vEdge1);

            // calculate v coordinated
            const float v = ray.m_vDirection.Dot(vQ) * inv_det;
            // test bounds
            if(v < 0.0f || u + v > 1.0f)
                return false;

            // calculate t coordinate (on ray)
            const float t = vEdge2.Dot(vQ) * inv_det;

            // store intersection component
            if(pOut)
                *pOut = ray.m_vOrigin + ray.m_vDirection * t;

            // intersected
            return true;
        }

		const Vector3& Triangle::GetP1() const
		{
			return m_vP1;
		}

		const Vector3& Triangle::GetP2() const
		{
			return m_vP2;
		}

		const Vector3& Triangle::GetP3() const
		{
			return m_vP3;
		}

		const Vector3& Triangle::GetNormal() const
		{
			return m_vNormal;
		}

    }
}
