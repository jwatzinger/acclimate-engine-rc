#pragma once
#include "BaseVolume.h"
#include "Vector3.h"

namespace acl
{
    namespace math
    {

        class Triangle :
            public BaseVolume
        {
        public:
            Triangle(const Vector3& vP1, const Vector3& vP2, const Vector3& vP3);
			Triangle();

            float MaxExtents(void) const
	        {
                return m_vP1.cabs().Max(m_vP2.cabs()).Max(m_vP3.cabs()).length();
	        }

			
            bool Intersect(const Ray& ray, Vector3* pOut = nullptr) const;

			const Vector3& GetP1() const;
			const Vector3& GetP2() const;
			const Vector3& GetP3() const;
			const Vector3& GetNormal() const;
		private:
			const Vector3 m_vP1, m_vP2, m_vP3;
			Vector3 m_vNormal;
        };

    }
}
