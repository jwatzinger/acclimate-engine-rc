#include "Utility.h"
#include <math.h>

namespace acl
{
    namespace math
    {

	    float degToRad(float degree) 
		{ 
		    return (float)detail::TO_RAD_FACTOR * (degree); 
	    }

		float radToDeg(float radians)
		{
			return (float)detail::TO_DEG_FACTOR * (radians);
		}

		float cotan(float degree)
		{
			return 1.0f/tan(degree);
		}

    }

}