/** @file Math\Utility.h
*	
*	Contains different mathematical helper functions.
*/
#pragma once
#include <math.h>
#include <type_traits>
#include "..\Core\Dll.h"

namespace acl
{
	namespace math
	{
		const float PI = (float)(atan(1.0)*4.0); ///< Const variable for PI, calculated on program execution

		/** Converts degree to radians.
		 *
		 *	@param[in] degree
		 *
		 *	@return radians
		 */
		ACCLIMATE_API float degToRad(float degree);

		/** Converts radians to degree.
		*
		*	@param[in] radians
		*
		*	@return degree
		*/
		ACCLIMATE_API float radToDeg(float radians);

		ACCLIMATE_API float cotan(float degree);

		/** Retrieves the sign from any numeric value.
		 *
		 *	@param[in] x Numeric value
		 *	@param[in] is_signed Reserved, used to pick the right template for some types
		 *
		 *	@tparam T Numeric type
		 *
		 *	@return Sign, eigther -1, 0 or 1
		 */
        template <typename T> inline
        int signum(T x, std::false_type is_signed) 
		{
            return T(0) < x;
        }

		/** Retrieves the sign from any numeric value.
		 *
		 *	@param[in] x Numeric value
		 *	@param[in] is_signed Reserved, used to pick the right template for some types
		 *
		 *	@tparam T Numeric type
		 *
		 *	@return Sign, eigther -1, 0 or 1
		 */
        template <typename T> inline
        int signum(T x, std::true_type is_signed) 
		{
            return (T(0) < x) - (x < T(0));
        }

		/** Retrieves the sign from any numeric value.
		 *
		 *	@param[in] x Numeric value
		 *
		 *	@tparam T Numeric type
		 *
		 *	@return Sign, eigther -1, 0 or 1
		 */
        template <typename T> inline
        int signum(T x) 
		{
            return signum(x, std::is_signed<T>());
        }

		namespace detail
		{
			const float TO_RAD_FACTOR = (PI / 180.0f); ///< Const conversion factor from degree to radians
			const float TO_DEG_FACTOR = (180.0f / PI); ///< Const conversion factor from radians to degree 
		}
	}
}
