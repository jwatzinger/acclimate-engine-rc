#include "Vector.h"
#include <math.h>

namespace acl
{
    namespace math
    {

        Vector2::Vector2(void) : x(0), y(0)
        {
        }

        Vector2::Vector2(int x, int y) : x(x), y(y)
        {
        }

		float Vector2::length(void) const
        {
			return sqrt((float)(x*x + y*y));
        }

        Vector2& Vector2::absolute(void)
        {
	        x = abs(x);
	        y = abs(y);
	        return *this;
        }

        Vector2& Vector2::operator/=(int devisor)
        {
	        x/=devisor;
	        y/=devisor;
	        return *this;
        }

		Vector2 Vector2::operator+(const Vector2 &v) const
        {
	        return Vector2(x+v.x, y+v.y);
        }

        Vector2 Vector2::operator-(const Vector2 &v) const
        {
	        return Vector2(x-v.x, y-v.y);
        }

        Vector2 Vector2::operator/(int devisor) const
        {
	        return Vector2(x/devisor, y/devisor);
        }

		Vector2 Vector2::operator / (float devisor) const
		{
			return Vector2((int)(x / devisor), (int)(y / devisor));
		}

        Vector2 Vector2::operator*(int multiplier) const
        {
            return Vector2(x*multiplier, y*multiplier);
        }

		Vector2 Vector2::operator*(float multiplier) const
        {
            return Vector2((int)(x*multiplier), (int)(y*multiplier));
        }

        Vector2 & Vector2::operator-=(const Vector2 &v)
        {
	        x -= v.x;
	        y -= v.y;
	        return *this;
        }

        Vector2 & Vector2::operator+=(const Vector2 &v)
        {
	        x += v.x;
	        y += v.y;
	        return *this;
        }

		bool Vector2::operator==(const Vector2& v) const
		{
			return x == v.x && y == v.y;
		}

		bool Vector2::operator!=(const Vector2& v) const
		{
			return x != v.x || y != v.y;
		}

    }
}