#pragma once
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        struct ACCLIMATE_API Vector2
        {
	        Vector2(void);
	        Vector2(int x, int y);
	
	        float length(void) const;
	        Vector2& absolute(void);

			Vector2 operator+(const Vector2& v) const;
	        Vector2 operator-(const Vector2& v) const;
	        Vector2 operator/(int devisor) const;
			Vector2 operator/(float devisor) const;
            Vector2 operator*(int multiplier) const;
			Vector2 operator*(float multiplier) const;

	        Vector2& operator+=(const Vector2& v);
	        Vector2& operator-=(const Vector2& v);

	        Vector2& operator/=(int devisor);

			bool operator==(const Vector2& v) const;
			bool operator!=(const Vector2& v) const;

	        int x, y;

        };

    }
}

