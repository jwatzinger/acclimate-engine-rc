#include "Vector2f.h"
#include <math.h>

namespace acl
{
    namespace math
    {

        Vector2f::Vector2f(void) : x(0), y(0)
        {
        }

        Vector2f::Vector2f(float x, float y) : x(x), y(y)
        {
        }

        float Vector2f::length(void) const
        {
	        return sqrt(x*x+y*y);
        }

        Vector2f& Vector2f::absolute(void)
        {
	        x = abs(x);
	        y = abs(y);
	        return *this;
        }

		Vector2f Vector2f::normal(void) const
		{
			const float len = length();

			if(len != 0.0f)
				return Vector2f(x / len, y / len);
			else
				return Vector2f(0.0f, 0.0f);
		}

		Vector2f& Vector2f::Normalize(void)
		{
			const float len = length();

			if(len != 0.0f)
			{
				x /= len;
				y /= len;
			}

			return *this;
		}

        Vector2f& Vector2f::operator/=(int devisor)
        {
	        x/=devisor;
	        y/=devisor;
	        return *this;
        }

		Vector2f Vector2f::operator + (const Vector2f& v) const
		{
			return Vector2f(x + v.x, y + v.y);
		}

        Vector2f Vector2f::operator-(const Vector2f &v) const
        {
	        return Vector2f(x-v.x, y-v.y);
        }

        Vector2f Vector2f::operator/(int devisor) const
        {
	        return Vector2f(x/devisor, y/devisor);
        }

		Vector2f Vector2f::operator/(float devisor) const
		{
			return Vector2f(x/devisor, y/devisor);
		}

		Vector2f Vector2f::operator/(double devisor) const
		{
			return Vector2f(x/(float)devisor, y/(float)devisor);
		}

        Vector2f Vector2f::operator*(int multiplier) const
        {
            return Vector2f(x*multiplier, y*multiplier);
        }

		Vector2f Vector2f::operator*(float multiplier) const
        {
            return Vector2f(x*multiplier, y*multiplier);
        }

		Vector2f Vector2f::operator*(double multiplier) const
        {
            return Vector2f(x*(float)multiplier, y*(float)multiplier);
        }

        Vector2f & Vector2f::operator-=(const Vector2f &v)
        {
	        x -= v.x;
	        y -= v.y;
	        return *this;
        }

        Vector2f & Vector2f::operator+=(const Vector2f &v)
        {
	        x += v.x;
	        y += v.y;
	        return *this;
        }

		bool Vector2f::operator==(const Vector2f& v) const
		{
			return x == v.x && y == v.y;
		}

		bool Vector2f::operator!=(const Vector2f& v) const
		{
			return x != v.x || y != v.y;
		}

    }
}