#pragma once
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {
		// todo: make generic templated vector2 maybe, but first figure out why
		// the default template thingy didn't work...
        struct ACCLIMATE_API Vector2f
        {
	        Vector2f(void);
	        Vector2f(float x, float y);
	
	        float length(void) const;
	        Vector2f& absolute(void);
			Vector2f normal(void) const;
			Vector2f& Normalize(void);
			
			Vector2f operator+(const Vector2f& v) const;
	        Vector2f operator-(const Vector2f& v) const;
	        Vector2f operator/(int devisor) const;
			Vector2f operator/(float devisor) const;
			Vector2f operator/(double devisor) const;
            Vector2f operator*(int multiplier) const;
			Vector2f operator*(float multiplier) const;
			Vector2f operator*(double multiplier) const;

	        Vector2f& operator+=(const Vector2f &v);
	        Vector2f& operator-=(const Vector2f &v);

	        Vector2f& operator/=(int devisor);

			bool operator==(const Vector2f& v) const;
			bool operator!=(const Vector2f& v) const;

	        float x, y;

        };

    }
}

