#include "Vector3.h"
#include <math.h>
#include <minmax.h>

namespace acl
{
    namespace math
    {

        Vector3::Vector3(void): x(0.0f), y(0.0f), z(0.0f)
        {
        }

        Vector3::Vector3(float x, float y, float z): x(x), y(y), z(z)
        {
        }

        float Vector3::length(void) const
        {
            return sqrt(x*x + y*y + z*z);
        }

		float Vector3::squaredLength(void) const
        {
            return x*x + y*y + z*z;
        }

		float Vector3::GetByIndex(unsigned int index) const
		{
			switch(index)
			{
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
			default:
				return 0.0f;
			}
		}

        Vector3 Vector3::cmin(const Vector3& v) const
        {
            return Vector3(min(x, v.x), min(y, v.y), min(z, v.z));
        }

        Vector3 Vector3::cmax(const Vector3& v) const
        {
            return Vector3(max(x, v.x), max(y, v.y), max(z, v.z));
        }

        Vector3 Vector3::cabs(void) const
        {
            return Vector3(abs(x), abs(y), abs(z));
        }

        float Vector3::Dot(const Vector3& v) const
        {
            float result = x * v.x;
            result += y * v.y;
            result += z * v.z;
            return result;
        }

        Vector3 Vector3::normal(void) const
        {
            const float len = length();
			if(len != 0.0f)
				return Vector3(x / len, y / len, z / len);
			else
				return Vector3(0.0f, 0.0f, 0.0f);
        }

        Vector3 Vector3::Cross(const Vector3& v) const
        {
            return Vector3( y * v.z - z * v.y,
                            z * v.x - x * v.z,
                            x * v.y - y * v.x);
        }

        Vector3& Vector3::Normalize(void)
        {
            const float len = length();

            if(len != 0.0f)
            {
                x /= len;
                y /= len;
                z /= len;
            }
            
            return *this;
        }

        Vector3& Vector3::Min(const Vector3& v)
        {
            x = min(x, v.x);
            y = min(y, v.y);
            z = min(z, v.z);

            return *this;
        }

        Vector3& Vector3::Max(const Vector3& v)
        {
            x = max(x, v.x);
            y = max(y, v.y);
            z = max(z, v.z);

            return *this;
        }

		Vector3 Vector3::Min(const Vector3& v) const
        {
            return Vector3(min(x, v.x), min(y, v.y), min(z, v.z));
        }

        Vector3 Vector3::Max(const Vector3& v) const
        {
			return Vector3(max(x, v.x), max(y, v.y), max(z, v.z));
        }

        Vector3& Vector3::Abs(void)
        {
            x = abs(x);
            y = abs(y);
            z = abs(z);

            return *this;
        }

		Vector3& Vector3::Floor(void)
		{
			x = floor(x);
			y = floor(y);
			z = floor(z);

			return *this;
		}

        Vector3 Vector3::operator+(const Vector3& v) const
        {
            return Vector3(x + v.x, y + v.y, z + v.z);
        }

		Vector3 Vector3::operator-(void) const
        {
            return Vector3(-x, -y, -z);
        }

        Vector3 Vector3::operator-(const Vector3& v) const
        {
            return Vector3(x - v.x, y - v.y, z - v.z);
        }

        Vector3 Vector3::operator*(float value) const
        {
            return Vector3(x * value, y * value, z * value);
        }

        Vector3 Vector3::operator/(float value) const
        {
            return Vector3(x / value, y / value, z / value);
        }

        Vector3 operator*(float value, const Vector3& v)
        {
            return Vector3(v.x * value, v.y * value, v.z * value);
        }

        Vector3& Vector3::operator+=(const Vector3& v)
        {
            x += v.x;
            y += v.y;
            z += v.z;

            return *this;
        }

		Vector3 Vector3::operator*(const Vector3& v) const
		{
			return Vector3(x * v.x, y * v.y, z * v.z);
		}

        Vector3& Vector3::operator-=(const Vector3& v)
        {
            x -= v.x;
            y -= v.y;
            z -= v.z;

            return *this;
        }

        Vector3& Vector3::operator*=(float value)
        {
            x *= value;
            y *= value;
            z *= value;

            return *this;
        }

		Vector3& Vector3::operator*=(const Vector3& v)
		{
			x *= v.x;
			y *= v.y;
			z *= v.z;

			return *this;
		}

        Vector3& Vector3::operator/=(float value)
        {
            x /= value;
            y /= value;
            z /= value;

            return *this;
        }

		Vector3& Vector3::operator/=(const Vector3& v)
		{
			x /= v.x;
			y /= v.y;
			z /= v.z;

			return *this;
		}

		bool Vector3::operator == (const Vector3& v) const
		{
			return x == v.x && y == v.y && z == v.z;
		}

		bool Vector3::operator != (const Vector3& v) const
		{
			return x != v.x || y != v.y || z != v.z;
		}

    }
}