#pragma once
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        struct ACCLIMATE_API Vector3
        {
        public:
            Vector3(void);
            Vector3(float x, float y, float z);

            float length(void) const;
			float squaredLength(void) const;
			float GetByIndex(unsigned int index) const;
            Vector3 normal(void) const;
            Vector3 cabs(void) const;
            Vector3 cmin(const Vector3& v) const;
            Vector3 cmax(const Vector3& v) const;

            float Dot(const Vector3& v) const;
            Vector3 Cross(const Vector3& v) const;
            Vector3& Normalize(void);

            Vector3& Min(const Vector3& v);
            Vector3& Max(const Vector3& v);
			Vector3 Min(const Vector3& v) const;
            Vector3 Max(const Vector3& v) const;
            Vector3& Abs(void);
			Vector3& Floor(void);

            Vector3 operator+(const Vector3& v) const;
			Vector3 operator-(void) const;
            Vector3 operator-(const Vector3& v) const;
            Vector3 operator*(float value) const;
			Vector3 operator*(const Vector3& v) const;
            Vector3 operator/(float value) const;
            
            friend ACCLIMATE_API Vector3 operator*(float, const Vector3& v);

            Vector3& operator+=(const Vector3& v);
            Vector3& operator-=(const Vector3& v);
            Vector3& operator*=(float value);
			Vector3& operator*=(const Vector3& v);
            Vector3& operator/=(float value);
			Vector3& operator/=(const Vector3& v);
			bool operator==(const Vector3& v) const;
			bool operator!=(const Vector3& v) const;

            float x, y, z;
        };

    }
}

