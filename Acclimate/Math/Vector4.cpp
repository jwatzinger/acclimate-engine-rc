#include "Vector4.h"
#include <math.h>
#include <minmax.h>

namespace acl
{
    namespace math
    {

        Vector4::Vector4(void): x(0.0f), y(0.0f), z(0.0f), w(0.0f)
        {
        }

        Vector4::Vector4(float x, float y, float z, float w): x(x), y(y), z(z), w(w)
        {
        }

		Vector4 Vector4::Cross(const Vector4& vec2, const Vector4& vec3) const
		{
			Vector4 vOut;
			vOut.x = y * (vec2.z * vec3.w - vec3.z * vec2.w) - z * (vec2.y * vec3.w - vec3.y * vec2.w) + w * (vec2.y * vec3.z - vec2.z *vec3.y);
			vOut.y = -(x * (vec2.z * vec3.w - vec3.z * vec2.w) - z * (vec2.x * vec3.w - vec3.x * vec2.w) + w * (vec2.x * vec3.z - vec3.x * vec2.z));
			vOut.z = x * (vec2.y * vec3.w - vec3.y * vec2.w) - y * (vec2.x *vec3.w - vec3.x * vec2.w) + w * (vec2.x * vec3.y - vec3.x * vec2.y);
			vOut.w = -(x * (vec2.y * vec3.z - vec3.y * vec2.z) - y * (vec2.x * vec3.z - vec3.x *vec2.z) + z * (vec2.x * vec3.y - vec3.x * vec2.y));
			return vOut;
		}

		Vector4& Vector4::Normalize(void)
		{
			const float length = sqrt(x*x + y*y + z*z + w*w);
			
			if(length != 0.0f)
			{
				x /= length;
				y /= length;
				z /= length;
				w /= length;
			}

			return *this;
		}

		Vector4 Vector4::normal(void) const
		{
			const float length = sqrt(x*x + y*y + z*z + w*w);
			return Vector4(x/length, y/length, z/length, w/length);
		}

		Vector4 Vector4::operator+(const Vector4& vector) const
		{
			return Vector4(x + vector.x, y + vector.y, z + vector.z, w + vector.w);
		}

		Vector4 Vector4::operator*(float value) const
		{
			return Vector4(x * value, y * value, z * value, w * value);
		}

		Vector4 Vector4::operator-(void) const
		{
			return Vector4(-x, -y, -z, -w);
		}

		Vector4& Vector4::operator-=(const Vector4& vector)
		{
			x -= vector.x;
			y -= vector.y;
			z -= vector.z;
			w -= vector.w;
			return *this;
		}

		Vector4& Vector4::operator*=(float value)
		{
			x *= value;
			y *= value;
			z *= value;
			w *= value;
			return *this;
		}

    }
}