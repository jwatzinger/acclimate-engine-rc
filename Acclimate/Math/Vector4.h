#pragma once
#include "..\Core\Dll.h"

namespace acl
{
    namespace math
    {

        struct ACCLIMATE_API Vector4
        {
        public:
            Vector4(void);
            Vector4(float x, float y, float z, float w);

			Vector4 Cross(const Vector4& vec2, const Vector4& vec3) const;
			Vector4& Normalize(void);
			Vector4 normal(void) const;

			Vector4 operator+(const Vector4& vector) const;
			Vector4 operator*(float value) const;
			Vector4 operator-(void) const;

			Vector4& operator-=(const Vector4& vector);
			Vector4& operator*=(float value);

            float x, y, z, w;
        };

    }
}

