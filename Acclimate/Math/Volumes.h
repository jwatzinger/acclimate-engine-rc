#pragma once

namespace acl
{
	namespace math
	{

		enum class Volumes
		{
			AABB = 0,
			HeightField = 1,
			OBB = 2,
			Sphere = 3,
			Triangle = 4
		};

		#define VolumeType(a, b) (((unsigned int)a << 16) | (unsigned int)b)
	}
}