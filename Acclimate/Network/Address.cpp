#include "Address.h"
#include <vector>
#include "..\System\Convert.h"

namespace acl
{
	namespace network
	{

		Address::Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port) :
			m_port(port)
		{
			m_address = (a << 24) | (b << 16) | (c << 8) | d;
		}

		Address::Address(const Address& address, unsigned short port) : m_address(address.m_address), m_port(port)
		{
		}

		unsigned int Address::GetAddress(void) const
		{
			return m_address;
		}

		unsigned int Address::GetPort(void) const
		{
			return m_port;
		}

		std::wstring Address::ToString(void) const
		{
			return std::wstring();
		}

		Address addressFromString(const std::wstring& stAddress)
		{
			std::vector<unsigned int> vParts;
			std::wstring strPart;
			for(auto c : stAddress)
			{
				if(c != '.')
					strPart += c;
				else
				{
					unsigned int temp;
					conv::FromString(&temp, strPart.c_str());
					vParts.push_back(temp);
					strPart.clear();
				}

				if(vParts.size() == 4)
					break;
			}

			unsigned int temp;
			conv::FromString(&temp, strPart.c_str());
			vParts.push_back(temp);

			if(vParts.size() != 4)
				return Address(127, 0, 0, 1, 0);
			else
				return Address(vParts[0], vParts[1], vParts[2], vParts[3], 0);
		}

	}
}

