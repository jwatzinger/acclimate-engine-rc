#pragma once
#include <string>

namespace acl
{
	namespace network
	{

		class Address
		{
		public:

			Address(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port);
			Address(const Address& address, unsigned short port);

			unsigned int GetAddress(void) const;
			unsigned int GetPort(void) const;

			std::wstring ToString(void) const;

		private:

			unsigned int m_address;
			unsigned int m_port;
		};

		Address addressFromString(const std::wstring& stAddress);

	}
}
