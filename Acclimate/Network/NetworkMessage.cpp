#include "NetworkMessage.h"

namespace acl
{
	namespace network
	{

		BaseNetworkMessage::ID BaseNetworkMessage::id_count = 1;

		BaseNetworkMessage::ID BaseNetworkMessage::GenerateID(void)
		{
			return id_count++;
		}

	}
}

