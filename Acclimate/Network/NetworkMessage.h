#pragma once
#include <string>
#include "NetworkStream.h"
#include "Address.h"

namespace acl
{
	namespace network
	{

		/*******************************
		* BaseMessage
		*******************************/

		struct BaseNetworkMessage
		{
		protected:
			typedef unsigned int ID;

			static ID GenerateID(void);

		private:
			static ID id_count;
		};

		/*******************************
		* Message
		*******************************/

		template <typename Derived>
		struct NetworkMessage :
			public BaseNetworkMessage
		{
		public:

			static ID MessageID(void)
			{
				static BaseNetworkMessage::ID id = BaseNetworkMessage::GenerateID();
				return id;
			}
		};

	}
}



