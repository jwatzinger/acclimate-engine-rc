#include "NetworkStream.h"
#include <memory>
#include <assert.h>

namespace acl
{
	namespace network
	{

		unsigned int helpSize(const std::wstring& stVar)
		{
			return stVar.size()*sizeof(wchar_t)+sizeof(char);
		}

		unsigned int helpSize(void)
		{
			return 0;
		}

		/***********************************
		* OUT
		***********************************/

		NetworkOutStream::NetworkOutStream(Socket& socket) : m_pSocket(&socket)
		{
		}

		void NetworkOutStream::WriteStream(const char* pStream, size_t& offset)
		{
		}

		void NetworkOutStream::WriteStream(const char* pStream, size_t& offset, const std::wstring& stVar)
		{
			wchar_t* pVariable = (wchar_t*)(pStream + offset);
			const size_t length = stVar.size()*sizeof(wchar_t);
			memcpy(pVariable, stVar.c_str(), length);
			(pVariable + length / sizeof(wchar_t))[0] = '\0';
			offset += length + sizeof(char);
		}

		/***********************************
		* MsgPacket
		***********************************/

		MsgPacket::MsgPacket(void) : m_id(0), m_size(0), m_pStream(nullptr)
		{
		}

		MsgPacket::MsgPacket(unsigned int id, size_t size, const char* pStream) : m_id(id), m_size(size)
		{
			m_pStream = new char[size];
			memcpy(m_pStream, pStream, size);
		}

		MsgPacket::MsgPacket(MsgPacket&& packet) : m_id(packet.m_id), m_size(packet.m_size),
			m_pStream(packet.m_pStream)
		{
			packet.m_id = 0;
			packet.m_pStream = nullptr;
			packet.m_size = 0;
		}

		MsgPacket::MsgPacket(const MsgPacket& packet) : m_id(packet.m_id), m_size(packet.m_size)
		{
			m_pStream = new char[m_size];
			memcpy(m_pStream, packet.m_pStream, m_size);
		}

		void MsgPacket::operator = (const MsgPacket& packet)
		{
			delete m_pStream;
			m_size = packet.m_size;
			m_id = packet.m_id;
			m_pStream = new char[m_size];
			memcpy(m_pStream, packet.m_pStream, m_size);
		}

		void MsgPacket::operator = (MsgPacket&& packet)
		{
			delete m_pStream;
			m_size = packet.m_size;
			m_id = packet.m_id;
			m_pStream = packet.m_pStream;

			packet.m_id = 0;
			packet.m_size = 0;
			packet.m_pStream = nullptr;
		}

		MsgPacket::~MsgPacket(void)
		{
			delete[] m_pStream;
		}

		unsigned int MsgPacket::GetId(void) const
		{
			return m_id;
		}

		void MsgPacket::ReadStream(const char* pStream, size_t& offset, std::wstring& var) const
		{
			auto pTest = (const wchar_t*)(pStream + offset);
			var = pTest;
			offset += var.size()*sizeof(wchar_t)+sizeof(char);
		}

		void MsgPacket::ReadStream(const char* pStream, size_t& offset) const
		{
		}

		/***********************************
		* IN
		***********************************/

		const size_t MAX_PACKET = 512;

		NetworkInStream::NetworkInStream(const Socket& socket) : m_pSocket(&socket)
		{
		}

		MsgPacket NetworkInStream::ReceiveMsgId(void)
		{
			char stream[MAX_PACKET];
			if(auto size = m_pSocket->Receive((void*)stream, 4))
			{
				assert(size == 4);

				const auto expectedSize = *(size_t*)(stream);

				size_t dataSize = 0;
				while(!(dataSize = m_pSocket->Receive((void*)stream, expectedSize)))
				{
				}

				assert(dataSize == expectedSize);

				auto id = ((unsigned int*)stream)[0];

				return MsgPacket(id, dataSize, stream + sizeof(unsigned int));

			}
			else
				return MsgPacket();
		}


	}
}