#pragma once
#include <new>
#include <string>
#include <vector>
#include "Socket.h"
#include "Address.h"

namespace acl
{
	namespace network
	{

		template <typename Head, typename Var, typename... Tail>
		unsigned int helpSize(const Head & head, const Var& var, const Tail &... tail)
		{
			return helpSize(head) + helpSize(var) + helpSize(tail...);
		}

		template<typename Type>
		unsigned int helpSize(const Type& type)
		{
			return sizeof(type);
		}

		template<typename Type>
		unsigned int helpSize(const std::vector<Type>& type)
		{
			return sizeof(Type)*type.size() + sizeof(size_t);
		}
		unsigned int helpSize(const std::wstring& stVar);
		unsigned int helpSize(void);

		/***********************************
		* OUT
		***********************************/

		class NetworkOutStream
		{
		public:
			NetworkOutStream(Socket& socket);

			template<typename Message, typename Variable, typename Variable2, typename... Variables>
			void Write(Variable var, Variable2 var2, Variables... vars);
			template<typename Message, typename Variable>
			void Write(Variable var);
			template<typename Message>
			void Write(void);

		private:

			template<typename Var1, typename Var2, typename... Variables>
			void WriteStream(const char* pStream, size_t& offset, Var1& var1, Var2& var2, Variables&... vars);
			template<typename Variable>
			void WriteStream(const char* pStream, size_t& offset, Variable var);
			template<typename Variable>
			void WriteStream(const char* pStream, size_t& offset, const std::vector<Variable>& vVar);
			void WriteStream(const char* pStream, size_t& offset, const std::wstring& stVar);
			void WriteStream(const char* pStream, size_t& offset);

			Socket* m_pSocket;
		};

		template<typename Message, typename Variable, typename Variable2, typename... Variables>
		void NetworkOutStream::Write(Variable var, Variable2 var2, Variables... vars)
		{
			const unsigned int size = helpSize(var, var2, vars...) + sizeof(unsigned int)* 2;
			char stream[512];

			size_t offset = 0;
			WriteStream(stream, offset, size - 4);
			WriteStream(stream, offset, Message::MessageID());
			WriteStream(stream, offset, var);
			WriteStream(stream, offset, var2);
			WriteStream(stream, offset, vars...);

			m_pSocket->Send((void*)stream, size);
		}

		template<typename Message, typename Variable>
		void NetworkOutStream::Write(Variable var)
		{
			const size_t size = helpSize(var) + sizeof(unsigned int)* 2;
			char stream[512];

			size_t offset = 0;
			WriteStream(stream, offset, size - 4);
			WriteStream(stream, offset, Message::MessageID());
			WriteStream(stream, offset, var);

			m_pSocket->Send((void*)stream, size);
		}

		template<typename Message>
		void NetworkOutStream::Write(void)
		{
			const size_t size = sizeof(unsigned int)* 2;
			char stream[size];

			size_t offset = 0;
			WriteStream(stream, offset, size - 4);
			WriteStream(stream, offset, Message::MessageID());

			m_pSocket->Send((void*)stream, size);
		}

		template<typename Var1, typename Var2, typename... Variables>
		void NetworkOutStream::WriteStream(const char* pStream, size_t& offset, Var1& var1, Var2& var2, Variables&... vars)
		{
			WriteStream(pStream, offset, var1);
			WriteStream(pStream, offset, var2);
			WriteStream(pStream, offset, vars...);
		}

		template<typename Variable>
		void NetworkOutStream::WriteStream(const char* pStream, size_t& offset, Variable var)
		{
			Variable* pVariable = (Variable*)(pStream + offset);
			new(pVariable)Variable(var);
			offset += sizeof(Variable);
		}

		template<typename Variable>
		void NetworkOutStream::WriteStream(const char* pStream, size_t& offset, const std::vector<Variable>& vVar)
		{
			size_t* pId = (size_t*)(pStream + offset);
			new(pId)size_t(vVar.size());
			offset += sizeof(size_t);

			Variable* pVariable = (Variable*)(pStream + offset);
			for(auto& var : vVar)
			{
				new(pVariable)Variable(var);
				pVariable++;
				offset += sizeof(Variable);
			}
		}

		/***********************************
		* MsgPacket
		***********************************/

		class MsgPacket
		{
		public:

			MsgPacket(void);
			MsgPacket(unsigned int id, size_t size, const char* pStream);
			MsgPacket(MsgPacket&& packet);
			MsgPacket(const MsgPacket& packet);
			~MsgPacket(void);

			void operator=(const MsgPacket& packet);
			void operator=(MsgPacket&& packet);

			unsigned int GetId(void) const;

			template<typename Var1, typename Var2, typename... Vars>
			void ReadBody(Var1& var1, Var2& var2, Vars&... vars) const;

			template<typename Var>
			void ReadBody(Var& var) const;

		private:

			template<typename Var1, typename Var2, typename... Variables>
			void ReadStream(const char* pStream, size_t& offset, Var1& var1, Var2& var2, Variables&... vars) const;
			template<typename Variable>
			void ReadStream(const char* pStream, size_t& offset, Variable& var) const;
			template<typename Variable>
			void ReadStream(const char* pStream, size_t& offset, std::vector<Variable>& var) const;
			void ReadStream(const char* pStream, size_t& offset, std::wstring& var) const;
			void ReadStream(const char* pStream, size_t& offset) const;

			unsigned int m_id;
			size_t m_size;
			char* m_pStream;
		};

		template<typename Var1, typename Var2, typename... Vars>
		void MsgPacket::ReadBody(Var1& var1, Var2& var2, Vars&... vars)  const
		{
			size_t offset = 0;
			ReadStream(m_pStream, offset, var1);
			ReadStream(m_pStream, offset, var2);
			ReadStream(m_pStream, offset, vars...);
		}

		template<typename Var>
		void MsgPacket::ReadBody(Var& var) const
		{
			size_t offset = 0;
			ReadStream(m_pStream, offset, var);
		}

		template<typename Var1, typename Var2, typename... Variables>
		void MsgPacket::ReadStream(const char* pStream, size_t& offset, Var1& var1, Var2& var2, Variables&... vars) const
		{
			ReadStream(m_pStream, offset, var1);
			ReadStream(m_pStream, offset, var2);
			ReadStream(m_pStream, offset, vars...);
		}

		template<typename Variable>
		void MsgPacket::ReadStream(const char* pStream, size_t& offset, Variable& var) const
		{
			var = ((Variable*)(pStream + offset))[0];
			const size_t size = sizeof(Variable);
			offset += size;
		}

		template<typename Variable>
		void MsgPacket::ReadStream(const char* pStream, size_t& offset, std::vector<Variable>& var) const
		{
			const size_t size = ((size_t*)(pStream + offset))[0];
			var.reserve(size);
			offset += sizeof(size);

			Variable* pVariable = ((Variable*)(pStream + offset));

			for(size_t i = 0; i < size; i++)
			{
				var.push_back(*pVariable);
				pVariable++;
				offset += sizeof(Variable);
			}
		}

		/***********************************
		* IN
		***********************************/

		class NetworkInStream
		{
		public:
			NetworkInStream(const Socket& socket);

			MsgPacket ReceiveMsgId(void);

		private:

			const Socket* m_pSocket;
		};

	}
}