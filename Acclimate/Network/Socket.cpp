#include "Socket.h"
#include <WinSock2.h>
#include <assert.h>
#include "Address.h"

#pragma comment(lib, "wsock32.lib")

namespace acl
{
	namespace network
	{

		void Socket::InitLayer(void)
		{
			WSADATA WsaData;
			auto test = WSAStartup(MAKEWORD(2, 2), &WsaData);
			assert(test == NO_ERROR);
		}

		void Socket::ShutDownLayer(void)
		{
			WSACleanup();
		}

		Socket::Socket(void) : m_port(0), m_handle(0), m_inSocket(0)
		{
		}

		void Socket::Connect(const Address& address, unsigned long inAddress)
		{
			sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = inAddress;
			addr.sin_port = htons(address.GetPort());

			while(connect(m_handle, (SOCKADDR*)&addr, sizeof(SOCKADDR)) < 0)
			{
			}
		}

		void Socket::Connect(const Address& address)
		{
			sockaddr_in addr;
			addr.sin_family = AF_INET;
			addr.sin_addr.s_addr = htonl(address.GetAddress());
			addr.sin_port = htons(address.GetPort());

			while(connect(m_handle, (SOCKADDR*)&addr, sizeof(SOCKADDR)) < 0)
			{
			}
		}

		void Socket::Open(unsigned int port)
		{
			m_port = port;

			// create socket
			m_handle = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			assert(m_handle > 0);
		}

		void Socket::Close(void)
		{
			closesocket(m_handle);
		}

		unsigned long Socket::Accept(void)
		{
			sockaddr_in address;
			address.sin_family = AF_INET;
			address.sin_addr.s_addr = INADDR_ANY;
			address.sin_port = htons((unsigned short)m_port);

			// bind handle to port
			if(bind(m_handle, (const sockaddr*)&address, sizeof(sockaddr_in)) < 0)
				assert(false);

			if(listen(m_handle, 10) < 0)
				assert(false);

			int size = sizeof(sockaddr_in);
			sockaddr_in inAddress;
			if((m_inSocket = accept(m_handle, (sockaddr*)&inAddress, &size)) == INVALID_SOCKET)
				assert(false);

			DWORD nonBlocking = 1;
			if(ioctlsocket(m_inSocket, FIONBIO, &nonBlocking) != 0)
				assert(false);

			return inAddress.sin_addr.s_addr;
		}

		void Socket::Send(const void * data, int size)
		{
			int sent_bytes = send(m_handle, (const char*)data, size, 0);

			if(sent_bytes != -1)
				assert(sent_bytes == size);
		}

		unsigned int Socket::Receive(void * data, int size) const
		{
			int bytes = recv(m_inSocket, (char*)data, size, 0);

			if(bytes > 0)
				return bytes;
			else
				return 0;
		}

	}
}