#pragma once

namespace acl
{
	namespace network
	{

		class Address;

		class Socket
		{
		public:

			static void InitLayer(void);
			static void ShutDownLayer(void);

			Socket(void);

			void Open(unsigned int port);
			unsigned long Accept(void);
			void Connect(const Address& address);
			void Connect(const Address& address, unsigned long inAddress);
			void Close(void);

			void Send(const void * data, int size);
			unsigned int Receive(void * data, int size) const;

		private:

			int m_handle, m_inSocket;
			int m_port;
		};

	}
}

