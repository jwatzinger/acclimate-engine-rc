#include "BoxShape.h"
#include "Transform.h"

namespace acl
{
	namespace physics
	{

		BoxShape::BoxShape(const math::Vector3& vSize) : CollisionShape(ShapeType::BOX),
			m_vSize(vSize)
		{
		}

		BoxShape::BoxShape(float x, float y, float z) : CollisionShape(ShapeType::BOX),
			m_vSize(x, y, z)
		{
		}

		float BoxShape::ComputeVolume(void) const
		{
			return m_vSize.x * m_vSize.y * m_vSize.z;
		}

		math::Vector3 BoxShape::CalculateInertia(const float mass) const
		{
			return math::Vector3(0.0f, 0.0f, 0.0f);
		}

		math::AABB BoxShape::ComputeAABB(const Transform& transform) const
		{
			return math::AABB(transform.vPosition, m_vSize);
		}

	}
}

