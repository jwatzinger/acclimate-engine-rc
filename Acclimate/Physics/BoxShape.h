#pragma once
#include "CollisionShape.h"

namespace acl
{
	namespace physics
	{

		class ACCLIMATE_API BoxShape :
			public CollisionShape
		{
		public:
			BoxShape(const math::Vector3& vSize);
			BoxShape(float x, float y, float z);

			float ComputeVolume(void) const override;
			math::Vector3 CalculateInertia(const float mass) const override;
			math::AABB ComputeAABB(const Transform& transform) const override;

			math::Vector3 m_vSize;
		};

	}
}

