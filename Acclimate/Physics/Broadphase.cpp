#include "Broadphase.h"
#include "..\Math\IVolume.h"
#include <algorithm>
#include <iterator>

namespace acl
{
	namespace physics
	{
		
		Broadphase::RigidBodyPairVector Broadphase::IntersectionSweep(BodyVector& vBodies, Axis axis)
		{
			BodyVector vActive;
			RigidBodyPairVector vIntersections;

			vActive.push_back(vBodies[0]);
			for (const auto bodyA : vBodies)
			{
				bool didCollide = false;

				for (auto bodyActive = vActive.begin(); bodyActive != vActive.end(); )
				{
					auto pActiveBody = *bodyActive;
					if(pActiveBody == bodyA)
					{
						bodyActive++;
						continue;
					}
						
					const auto vMin = bodyA->GetAABB().GetMin();
					const auto vMax = pActiveBody->GetAABB().GetMax();

					bool doesCollide;
					switch(axis)
					{
					case Axis::X:
						doesCollide = vMin.x <= vMax.x;
						break;
					case Axis::Y:
						doesCollide = vMin.y <= vMax.y;
						break;
					case Axis::Z:
						doesCollide = vMin.z <= vMax.z;
						break;
					}

					if(doesCollide)
					{
						// only store intersection if any of both objects is non-static we do have 
						// to perform this check here and still store the other body as "active",
						// since other bodies might collide with those
						if(pActiveBody->GetMotionType() != MotionType::STATIC || bodyA->GetMotionType() != MotionType::STATIC)
							vIntersections.emplace_back(bodyA, pActiveBody);

						bodyActive++;

						didCollide = true;
					}
					else
						bodyActive = vActive.erase(bodyActive);
				}

				if(didCollide)
					vActive.push_back(bodyA);
			}
			return vIntersections;
		}

		void Broadphase::GeneratePairs(BodyVector& vBodies)
		{
			m_vPairs.clear();
			m_vBroadphasePairs.clear();

			if(vBodies.size() < 2)
				return;

			std::sort(m_vBodyX.begin(), m_vBodyX.end(), Broadphase::CompareX());
			std::sort(m_vBodyZ.begin(), m_vBodyZ.end(), Broadphase::CompareZ());

			//Threading
			RigidBodyPairVector vBroadphasePairsX = IntersectionSweep(m_vBodyX, Broadphase::Axis::X);
			RigidBodyPairVector vBroadphasePairsY = IntersectionSweep(m_vBodyZ, Broadphase::Axis::Z);
			
			if ((vBroadphasePairsX.size()>0 && vBroadphasePairsY.size()>0))
				m_vBroadphasePairs = GetCommonRigidBodyPairs(vBroadphasePairsX, vBroadphasePairsY);

			//Narrowphase:
			for (auto pair : m_vBroadphasePairs) //RigidBodyPairVector
			{
				//in broadcast nehmen
				math::AABB& aabb1 = pair.rigidBody1->GetAABB();
				math::AABB& aabb2 = pair.rigidBody2->GetAABB();

				if (abs(aabb1.GetCenter().y - aabb2.GetCenter().y) <= aabb1.m_vSize.y + aabb2.m_vSize.y)
				{
					std::vector<Manifold> vManifolds = m_collision.CollisionGenerateManifold(*(pair.rigidBody1), *(pair.rigidBody2));
					for (auto manifold : vManifolds)
					{
						if (manifold.IsValid())
						{
							pair.rigidBody1->OnCollision(*pair.rigidBody2);
							if (pair.rigidBody1->GetCollisionType() != CollisionType::GHOST)
								m_vPairs.push_back(manifold);
						}
					}
				}
			}
		}

		const Broadphase::ManifoldVector& Broadphase::GetPairs(void) const
		{
			return m_vPairs;
		}

		void Broadphase::AddBody(RigidBody& rigidbody)
		{
			m_vBodyX.push_back(&rigidbody);
			m_vBodyZ.push_back(&rigidbody);
		}

		void Broadphase::RemoveBody(RigidBody& rigidbody)
		{
			auto itrX = std::find(m_vBodyX.begin(), m_vBodyX.end(), &rigidbody);
			m_vBodyX.erase(itrX);
			auto itrZ = std::find(m_vBodyZ.begin(), m_vBodyZ.end(), &rigidbody);
			m_vBodyZ.erase(itrZ);
		}

		Broadphase::RigidBodyPairVector Broadphase::GetCommonRigidBodyPairs(const RigidBodyPairVector& pairs1, const RigidBodyPairVector& pairs2)
		{
			RigidBodyPairVector common;
			for (auto pair1 : pairs1)
			{
				for (auto pair2 : pairs2)
				{
					if (((pair1.rigidBody1 == pair2.rigidBody1) && (pair1.rigidBody2 == pair2.rigidBody2)) ||
						((pair1.rigidBody1 == pair2.rigidBody2) && (pair1.rigidBody2 == pair2.rigidBody1)))
						common.push_back(pair2);
				}
			}
			return common;
		}
	}
}
