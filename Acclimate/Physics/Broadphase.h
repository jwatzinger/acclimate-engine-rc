#pragma once
#include <vector>
#include "RigidBody.h"
#include "Manifold.h"
#include "Collision.h"

namespace acl
{
	namespace physics
	{

		// TODO: optimize, currently AABB is generated for each sorting and while comparing values
		// generate only once per frame/GeneratePairs-call, possibly store in a struct alongside RigidBody here
		// or cache it already in RigidBody
		class Broadphase
		{
			enum Axis
			{
				X = 0, Y, Z
			};

			struct CompareX
			{
				bool operator()(const RigidBody* rigidbody1, const RigidBody* rigidbody2) const
				{
					return (rigidbody1->GetAABB().GetMin().x < rigidbody2->GetAABB().GetMin().x);
				}
			};

			struct CompareY
			{
				bool operator()(const RigidBody* rigidbody1, const RigidBody* rigidbody2) const
				{
					return (rigidbody1->GetAABB().GetMin().y < rigidbody2->GetAABB().GetMin().y);
				}
			};

			struct CompareZ
			{
				bool operator()(const RigidBody* rigidbody1, const RigidBody* rigidbody2) const
				{
					return (rigidbody1->GetAABB().GetMin().z < rigidbody2->GetAABB().GetMin().z);
				}
			};

			struct RigidBodyPair 
			{
				RigidBody* rigidBody1;
				RigidBody* rigidBody2;

				RigidBodyPair(RigidBody* rigidbody1, RigidBody* rigidbody2) : rigidBody1(rigidbody1), rigidBody2(rigidbody2)
				{
				}
			};

			typedef std::vector<RigidBody*> BodyVector;
			typedef std::vector<RigidBodyPair> RigidBodyPairVector;
		public:

			typedef std::vector<Manifold> ManifoldVector;
			const ManifoldVector& GetPairs(void) const;

			void GeneratePairs(BodyVector& vBodies);
			void AddBody(RigidBody& rigidbody);
			void RemoveBody(RigidBody& rigidbody);
		private:
			BodyVector m_vBodyX;
			BodyVector m_vBodyZ;
			
			Collision m_collision;
			ManifoldVector m_vPairs;
			RigidBodyPairVector m_vBroadphasePairs;

			RigidBodyPairVector IntersectionSweep(BodyVector& vBodies, Axis axis);
			RigidBodyPairVector GetCommonRigidBodyPairs(const RigidBodyPairVector& pairs1, const RigidBodyPairVector& pairs2);
		};
	}
}
