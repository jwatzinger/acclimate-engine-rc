#include "CapsuleShape.h"
#include "Transform.h"
#include "..\Math\Utility.h"

namespace acl
{
	namespace physics
	{
		CapsuleShape::CapsuleShape(const float height, const float radius) : CollisionShape(ShapeType::CAPSULE),
			m_height(height), m_radius(radius)
		{

		}

		float CapsuleShape::ComputeVolume(void) const 
		{
			return m_radius * m_radius * math::PI * m_height + (m_radius * m_radius * m_radius * math::PI * (4.0f / 3.0f));
		}

		math::Vector3 CapsuleShape::CalculateInertia(const float mass) const
		{
			const float x = 1.0f / 12.0f * mass * (2 * m_radius * m_radius + ((m_height + m_radius) * (m_height + m_radius)));
			const float z = (mass * m_radius * m_radius) / 2.0f;

			return math::Vector3(x, x, z);
		}

		//AABB from the Object Bounding Sphere
		math::AABB CapsuleShape::ComputeAABB(const Transform& transform) const
		{
			return math::AABB(transform.vPosition, math::Vector3(m_radius, m_height + m_radius, m_radius));
		}

		math::Vector3 CapsuleShape::ComputeBeginPoint(const Transform& transform) const
		{
			return math::Vector3(transform.vPosition.x, transform.vPosition.y - m_height, transform.vPosition.z);
		}

		math::Vector3 CapsuleShape::ComputeEndPoint(const Transform& transform) const
		{
			return math::Vector3(transform.vPosition.x, transform.vPosition.y + m_height, transform.vPosition.z);
		}
	}
}
