#pragma once
#include "CollisionShape.h"

namespace acl
{
	namespace physics
	{

		class ACCLIMATE_API CapsuleShape :
			public CollisionShape
		{
		public:
			CapsuleShape(const float height, const float radius);

			float ComputeVolume(void) const override;
			math::Vector3 CalculateInertia(const float mass) const override;
			math::AABB ComputeAABB(const Transform& transform) const override;

			math::Vector3 ComputeBeginPoint(const Transform& transform) const;
			math::Vector3 ComputeEndPoint(const Transform& transform) const;

			float m_height;
			float m_radius;
		};

	}
}

