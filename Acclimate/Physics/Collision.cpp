#include "Collision.h"
#include <minmax.h>
#include "RigidBody.h"
#include "SphereShape.h"
#include "BoxShape.h"
#include "CapsuleShape.h"
#include "TerrainShape.h"
#include <algorithm>
#include "../System/Log.h"
#include "../Math/Ray.h"

namespace acl
{
	namespace physics
	{

		math::Vector3 ClosestPtPointTriangle(math::Vector3 p, const math::Vector3& a, const math::Vector3& b, const math::Vector3& c)
		{
			math::Vector3 ab = b - a;
			math::Vector3 ac = c - a;
			math::Vector3 bc = c - b;
			// Compute parametric position s for projection P� of P on AB,
			// P� = A + s*AB, s = snom/(snom+sdenom)
			float snom = (p-a).Dot(ab), sdenom = (p-b).Dot(a - b);
			// Compute parametric position t for projection P� of P on AC,
			// P� = A + t*AC, s = tnom/(tnom+tdenom)
			float tnom = (p-a).Dot(ac), tdenom = (p-c).Dot(a - c);
			if (snom <= 0.0f && tnom <= 0.0f) return a; // Vertex region early out
			// Compute parametric position u for projection P� of P on BC,
			// P� = B + u*BC, u = unom/(unom+udenom)
			float unom = (p - b).Dot(bc), udenom = (p - c).Dot(b - c);
			if (sdenom <= 0.0f && unom <= 0.0f) return b; // Vertex region early out
			if (tdenom <= 0.0f && udenom <= 0.0f) return c; // Vertex region early out
			// P is outside (or on) AB if the triple scalar product [N PA PB] <= 0
			math::Vector3 n = (b - a).Cross(c - a);
			float vc = n.Dot((a-p).Cross(b - p));
			// If P outside AB and within feature region of AB,
			// return projection of P onto AB
			if (vc <= 0.0f && snom >= 0.0f && sdenom >= 0.0f)
				return a + snom / (snom + sdenom) * ab;
			// P is outside (or on) BC if the triple scalar product [N PB PC] <= 0
			float va = n.Dot((b - p).Cross(c - p));
			// If P outside BC and within feature region of BC,
			// return projection of P onto BC
			if (va <= 0.0f && unom >= 0.0f && udenom >= 0.0f)
				return b + unom / (unom + udenom) * bc;
			// P is outside (or on) CA if the triple scalar product [N PC PA] <= 0
			float vb = n.Dot((c - p).Cross(a - p));
			// If P outside CA and within feature region of CA,
			// return projection of P onto CA
			if (vb <= 0.0f && tnom >= 0.0f && tdenom >= 0.0f)
				return a + tnom / (tnom + tdenom) * ac;
			// P must project inside face region. Compute Q using barycentric coordinates
			float u = va / (va + vb + vc);
			float v = vb / (va + vb + vc);
			float w = 1.0f - u - v; // = vc / (va + vb + vc)
			return u * a + v * b + w * c;
		}

		// Given segment ab and point c, computes closest point d on ab.
		// Also returns t for the position of d, d(t) = a + t*(b - a)
		void ClosestPointToSegment(const math::Vector3& c, const math::Vector3& a, const math::Vector3& b, float &t, math::Vector3& d)
		{
			const math::Vector3 ab = b - a;
			// Project c onto ab, computing parameterized position d(t) = a + t*(b � a)
			t = (c-a).Dot(ab) / ab.Dot(ab);
			// If outside segment, clamp t (and therefore d) to the closest endpoint
			t = min(1.0f, max(0.0f, t));
			// Compute projected position from the clamped t
			d = a + t * ab;
		}

		// Returns the squared distance between point c and segment ab
		float SquaredDistancePointToSegment(const math::Vector3& vA, const math::Vector3& vB, const math::Vector3& vC)
		{
			const math::Vector3 ab = vB - vA;
			const math::Vector3 ac = vC - vA;
			const math::Vector3 bc = vC - vB;
			const float e = ac.Dot(ab);
			// c projects outside ab
			if (e <= 0.0f) 
				return ac.Dot(ac);
			const float f = ab.Dot(ab);
			if (e >= f) 
				return bc.Dot(bc);
			// c projects onto ab
			return ac.Dot(ac) - e * e / f;
		}

		// Clamp n to lie within the range [min, max]
		float Clamp(const float n, const float minValue, const float maxValue)
		{
			return max(minValue, min(maxValue, n));
		}

		// Computes closest points C1 and C2 of Segment1 S1 and Segment S2(t), returning s and t. 
		// Function result is squared distance between between S1(s) and S2(t)
		float ClosestPtSegmentSegment(const math::Vector3& p1, const math::Vector3& q1, const math::Vector3& p2, const math::Vector3& q2,
			float &s, float &t, math::Vector3 &c1, math::Vector3 &c2)
		{
			const float epsilon = 0.0001f;
			const math::Vector3 d1 = q1 - p1; // Direction vector of segment S1
			const math::Vector3 d2 = q2 - p2; // Direction vector of segment S2
			const math::Vector3 r = p1 - p2;
			const float a = d1.Dot(d1); // Squared length of segment S1, always nonnegative
			const float e = d2.Dot(d2); // Squared length of segment S2, always nonnegative

			// Check if either or both segments degenerate into points
			if(a <= epsilon && e <= epsilon)
			{
				// Both segments degenerate into points
				s = t = 0.0f;
				c1 = p1;
				c2 = p2;
				return r.Dot(r);
			}
			else
			{
				const float f = d2.Dot(r);

				if (a <= epsilon) 
				{
					// First segment degenerates into a point
					s = 0.0f;
					t = f / e; // s = 0 => t = (b*s + f) / e = f / e
					t = Clamp(t, 0.0f, 1.0f);
				}
				else 
				{
					const float c = d1.Dot(r);
					if (e <= epsilon) 
					{
						// Second segment degenerates into a point
						t = 0.0f;
						s = Clamp(-c / a, 0.0f, 1.0f); // t = 0 => s = (b*t - c) / a = -c / a
					}
					else 
					{
						// The general nondegenerate case starts here
						float b = d1.Dot(d2);
						float denom = a*e - b*b; // Always nonnegative
						// If segments not parallel, compute closest point on L1 to L2 and
						// clamp to segment S1. Else pick arbitrary s (here 0)
						if (denom != 0.0f) 
							s = Clamp((b*f - c*e) / denom, 0.0f, 1.0f);
						else 
							s = 0.0f;
						// Compute point on L2 closest to S1(s) using 
						// t = Dot((P1 + D1*s) - P2,D2) / Dot(D2,D2) = (b*s + f) / e
						t = (b*s + f) / e;
						// If t in [0,1] done. Else clamp t, recompute s for the new value
						// of t using s = Dot((P2 + D2*t) - P1,D1) / Dot(D1,D1)= (t*b - c) / a and clamp s to [0, 1]
						if (t < 0.0f) 
						{
							t = 0.0f;
							s = Clamp(-c / a, 0.0f, 1.0f);
						}
						else if (t > 1.0f) 
						{
							t = 1.0f;
							s = Clamp((b - c) / a, 0.0f, 1.0f);
						}
					}
				}

				c1 = p1 + d1 * s;
				c2 = p2 + d2 * t;
				return (c1 - c2).Dot(c1 - c2);
			}
		}

		float SquaredDistancePointToAABB(const math::Vector3& p, const math::AABB& b)
		{
			const math::Vector3 min = b.GetCenter() - b.m_vSize;
			const math::Vector3 max = b.GetCenter() + b.m_vSize;
			float sqDist = 0.0f; 
			
			float v = 0;
			// For each axis count any excess distance outside box extents
			
			//x-axis
			v = p.x;
			if (v < min.x) 
				sqDist += (min.x - v) * (min.x - v);
			if (v > max.x) 
				sqDist += (v - max.x) * (v - max.x);

			//y-axis
			v = p.y;
			if (v < min.y) 
				sqDist += (min.y - v) * (min.y - v);
			if (v > max.y) 
				sqDist += (v - max.y) * (v - max.y);

			//z-axis
			v = p.z;
			if (v < min.z) 
				sqDist += (min.z - v) * (min.z - v);
			if (v > max.z) 
				sqDist += (v - max.z) * (v - max.z);

			return sqDist;
		}

		Manifold SphereVsSphere(RigidBody& bodyA, RigidBody& bodyB, const SphereShape& shape, const SphereShape& shape2)
		{
			math::Vector3 vNormal = bodyB.m_transform.vPosition - bodyA.m_transform.vPosition;

			float maxRadius = shape2.m_radius + shape.m_radius;

			const float squaredLenght = vNormal.squaredLength();
			if(squaredLenght > maxRadius * maxRadius)
				return Manifold();

			const float distance = sqrt(squaredLenght);

			if(distance != 0.0f)
			{
				vNormal /= distance;
				return Manifold(bodyA, bodyB, maxRadius - distance, vNormal, vNormal * shape.m_radius + bodyA.m_transform.vPosition);
			}
			else
				return Manifold(bodyA, bodyB, shape.m_radius, math::Vector3(1.0f, 0.0f, 0.0f), bodyA.m_transform.vPosition);
		}

		bool SphereVSBoxTest(const math::Vector3& positionSphere, const math::Vector3& positionBox, const float radius, const BoxShape& shapeBox, float& penetration, math::Vector3& vContact, math::Vector3& vN)
		{
			const math::Vector3 vNormal = positionBox - positionSphere;

			math::Vector3 vClosest = vNormal;
			vClosest.Min(shapeBox.m_vSize).Max(-shapeBox.m_vSize);

			bool isInside = false;

			if (vNormal == vClosest)
			{
				isInside = true;

				if (abs(vNormal.x) > abs(vNormal.y))
				{
					if (abs(vNormal.z) > abs(vNormal.x))
					{
						if (vClosest.z > 0)
							vClosest.z = shapeBox.m_vSize.z;
						else
							vClosest.z = -shapeBox.m_vSize.z;
					}
					else
					{
						if (vClosest.x > 0)
							vClosest.x = shapeBox.m_vSize.x;
						else
							vClosest.x = -shapeBox.m_vSize.x;
					}
				}
				else
				{
					if (abs(vNormal.z) > abs(vNormal.y))
					{
						if (vClosest.z > 0)
							vClosest.z = shapeBox.m_vSize.z;
						else
							vClosest.z = -shapeBox.m_vSize.z;
					}
					else
					{
						if (vClosest.y > 0)
							vClosest.y = shapeBox.m_vSize.y;
						else
							vClosest.y = -shapeBox.m_vSize.y;
					}
				}
			}

			vN = vNormal - vClosest;
			float d = vN.squaredLength();

			if (d > radius*radius && !isInside)
				return false;

			d = sqrt(d);
			vN /= d;

			if (isInside)
			{
				penetration = radius;
				vContact = positionSphere;
				vN = -vN;
			}
			else
			{
				penetration = radius - d;
				vContact = vN * radius + positionSphere;
			}
			
			return true;
		}
		
		Manifold SphereVsBox(RigidBody& bodySphere, RigidBody& bodyBox, const SphereShape& shapeSphere, const BoxShape& shapeBox)
		{
			math::Vector3 vContact, vNormal;
			float penetration = 0.0f, d = 0.0f;

			if (SphereVSBoxTest(bodySphere.m_transform.vPosition, bodyBox.m_transform.vPosition, shapeSphere.m_radius, shapeBox, penetration, vContact, vNormal))
				return Manifold(bodySphere, bodyBox, penetration, vNormal, vContact);
			else 
				return Manifold();
		}

		// TODO: calculate real contact points / BoxVsBox
		Manifold BoxVsBox(RigidBody& bodyA, RigidBody& bodyB, const BoxShape& shape, const BoxShape& shape2)
		{
			const math::Vector3 vNormal = bodyB.m_transform.vPosition - bodyA.m_transform.vPosition;

			const float overlapX = shape.m_vSize.x + shape2.m_vSize.x - abs(vNormal.x);

			if(overlapX > 0.0f)
			{
				const float overlapY = shape.m_vSize.y + shape2.m_vSize.y - abs(vNormal.y);

				if(overlapY > 0.0f)
				{
					const float overlapZ = shape.m_vSize.z + shape2.m_vSize.z - abs(vNormal.z);

					if(overlapZ > 0.0f)
					{
						if(overlapX > overlapY)
						{
							if(overlapZ > overlapX)
							{
								if(vNormal.z < 0.0f)
									return Manifold(bodyA, bodyB, overlapZ, math::Vector3(0.0f, 0.0f, -1.0f), math::Vector3());
								else
									return Manifold(bodyA, bodyB, overlapZ, math::Vector3(0.0f, 0.0f, 1.0f), math::Vector3());
							}
							else
							{
								if(vNormal.x < 0.0f)
									return Manifold(bodyA, bodyB, overlapX, math::Vector3(-1.0f, 0.0f, 0.0f), math::Vector3());
								else
									return Manifold(bodyA, bodyB, overlapX, math::Vector3(1.0f, 0.0f, 0.0f), math::Vector3());
							}
						}
						else
						{
							if(overlapZ > overlapY)
							{
								if(vNormal.z < 0.0f)
									return Manifold(bodyA, bodyB, overlapZ, math::Vector3(0.0f, 0.0f, -1.0f), math::Vector3());
								else
									return Manifold(bodyA, bodyB, overlapZ, math::Vector3(0.0f, 0.0f, 1.0f), math::Vector3());
							}
							else
							{
								if(vNormal.y < 0.0f)
									return Manifold(bodyA, bodyB, overlapY, math::Vector3(0.0f, -1.0f, 0.0f), math::Vector3());
								else
									return Manifold(bodyA, bodyB, overlapY, math::Vector3(0.0f, 1.0f, 0.0f), math::Vector3());
							}
						}
					}
				}
			}

			return Manifold();
		}

		Manifold BoxVsCapsule(RigidBody& boxBody, RigidBody& capsuleBody, const BoxShape& box, const CapsuleShape& capsule)
		{
			const math::Vector3 Position1 = capsule.ComputeBeginPoint(capsuleBody.m_transform);
			const math::Vector3 Position2 = capsule.ComputeEndPoint(capsuleBody.m_transform);

			float t = 0.0f, penetration = 0.0f;
			math::Vector3 vContact, vN, ClosestPoint;

			ClosestPointToSegment(boxBody.m_transform.vPosition, Position1, Position2, t, ClosestPoint);
			if (SphereVSBoxTest(ClosestPoint, boxBody.m_transform.vPosition, capsule.m_radius, box, penetration, vContact, vN))
				return Manifold(capsuleBody, boxBody, penetration, vN, vContact);
			else
				return Manifold();
		}

		Manifold SphereVsCapsule(RigidBody& capsuleBody, RigidBody& sphereBody, const SphereShape& sphere, const CapsuleShape& capsule)
		{
			const math::Vector3 vCapsuleA = capsule.ComputeBeginPoint(capsuleBody.m_transform); 
			const math::Vector3 vCapsuleB = capsule.ComputeEndPoint(capsuleBody.m_transform);

			const float distance = SquaredDistancePointToSegment(vCapsuleA, vCapsuleB, sphereBody.m_transform.vPosition); 
			const float radius = sphere.m_radius + capsule.m_radius;
			if (distance <= (radius * radius))
			{
				const float depth = sqrt(distance) - radius;
				math::Vector3 vNormal = sphereBody.m_transform.vPosition - capsuleBody.m_transform.vPosition;
				vNormal.Normalize();
				return Manifold(capsuleBody, sphereBody, depth, vNormal, vNormal * depth + sphereBody.m_transform.vPosition);
			}
			return Manifold();
		}

		Manifold CapsuleVsCapsule(RigidBody& capsuleBodyA, RigidBody& capsuleBodyB, const CapsuleShape& capsule1, const CapsuleShape& capsule2)
		{
			const math::Vector3 vCapsuleABeginn = capsule1.ComputeBeginPoint(capsuleBodyA.m_transform);
			const math::Vector3 vCapsuleAEnd = capsule1.ComputeEndPoint(capsuleBodyA.m_transform);
			
			const math::Vector3 vCapsuleBBeginn = capsule2.ComputeBeginPoint(capsuleBodyB.m_transform);
			const math::Vector3 vCapsuleBEnd = capsule2.ComputeEndPoint(capsuleBodyB.m_transform);

			float s, t;
			math::Vector3 c1, c2;
			float distance = ClosestPtSegmentSegment(vCapsuleABeginn, vCapsuleAEnd, vCapsuleBBeginn, vCapsuleBEnd, s, t, c1, c2);
			const float radius = capsule1.m_radius + capsule2.m_radius;
			if (distance <= (radius * radius))
			{
				const float depth = sqrt(abs(distance)) - radius;
				const math::Vector3 nearPoint0 = (1.0f - s)*vCapsuleABeginn + s*vCapsuleAEnd;
				const math::Vector3 nearPoint1 = (1.0f - t)*vCapsuleBBeginn + t*vCapsuleBEnd;

				math::Vector3 vNormal = nearPoint1 - nearPoint0;
				vNormal.Normalize();
				const math::Vector3 vContact = vNormal * depth + capsuleBodyA.m_transform.vPosition;
				return Manifold(capsuleBodyA, capsuleBodyB, depth, vNormal, vContact);
			}	
			return Manifold();
		}

		Manifold SphereVsTerrain(RigidBody& sphereBody, RigidBody& terrainBody, const SphereShape& sphere, const TerrainShape& terrain)
		{

			return Manifold();
		}

		Manifold BoxVsTerrain(RigidBody& boxBody, RigidBody& terrainBody, const BoxShape& box, const TerrainShape& terrain)
		{
			return Manifold();
		}
		
		bool PointVsTerrain(const math::Vector3 vPoint, RigidBody& capsuleBody, RigidBody& terrainBody, const CapsuleShape& capsule, const TerrainShape& terrain, float& penetration, math::Vector3& vNormal, math::Vector3& vContact)
		{
			const math::Triangle* t = terrain.GetTriangle(vPoint.x, vPoint.z);
			if (t == nullptr)
				return false;

			const math::Ray ray(vPoint, math::Vector3(0, -1, 0));
			vContact = ClosestPtPointTriangle(vPoint, t->GetP1(), t->GetP2(), t->GetP3());

			const math::Vector3 v = vPoint - vContact;
			if (v.Dot(v) <= capsule.m_radius * capsule.m_radius)
			{
				penetration = v.length() - capsule.m_radius;
				//if (vContact.y > vPoint.y + capsule.m_radius) penetration += capsule.m_radius;
				vNormal = t->GetNormal();
				return true;
			}
			else return false;
		}

		std::vector<Manifold> CapsuleVsTerrain(RigidBody& capsuleBody, RigidBody& terrainBody, const CapsuleShape& capsule, const TerrainShape& terrain)
		{
			std::vector<Manifold> result;

			float penetration;
			math::Vector3 vNormal;
			math::Vector3 vContact;
			math::Vector3 vPosition = capsule.ComputeBeginPoint(capsuleBody.m_transform);
			float factor = capsule.m_radius;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.x += factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.z -= factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.x -= factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.x -= factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.z += factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.z += factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.x += factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			vPosition.x += factor;
			if (PointVsTerrain(vPosition, capsuleBody, terrainBody, capsule, terrain, penetration, vNormal, vContact))
				result.push_back(Manifold(capsuleBody, terrainBody, penetration, vNormal, vContact));
			
			return result;
		}

#define VolumeType(a, b) (((unsigned int)a << 16) | (unsigned int)b)
#define HELP_CAST(volume, type, volume2, type2) *static_cast<const type*>(volume), *static_cast<const type2*>(volume2)

		std::vector<Manifold> Collision::CollisionGenerateManifold(RigidBody& bodyA, RigidBody& bodyB) const
		{

			auto* pShape = &bodyA.GetShape();
			auto* pShape2 = &bodyB.GetShape();
			auto* pBody = &bodyA;
			auto* pBody2 = &bodyB;

			auto type = pShape->GetType();
			auto type2 = pShape2->GetType();

			if (type > type2)
			{
				std::swap(type, type2);
				std::swap(pShape, pShape2);
				std::swap(pBody, pBody2);
			}

			std::vector<Manifold> result;

			switch (VolumeType(type, type2))
			{
			case VolumeType(ShapeType::SPHERE, ShapeType::TERRAIN):
				result.push_back(SphereVsTerrain(*pBody, *pBody2, HELP_CAST(pShape, SphereShape, pShape2, TerrainShape)));
				break;
			case VolumeType(ShapeType::BOX, ShapeType::TERRAIN):
				result.push_back(BoxVsTerrain(*pBody, *pBody2, HELP_CAST(pShape, BoxShape, pShape2, TerrainShape)));
				break;
			case VolumeType(ShapeType::CAPSULE, ShapeType::TERRAIN):
			{
				std::vector<Manifold> manifolds = CapsuleVsTerrain(*pBody, *pBody2, HELP_CAST(pShape, CapsuleShape, pShape2, TerrainShape));
				for (auto m : manifolds) 
					result.push_back(m);
				break;
			}
			case VolumeType(ShapeType::SPHERE, ShapeType::SPHERE):
				result.push_back(SphereVsSphere(*pBody, *pBody2, HELP_CAST(pShape, SphereShape, pShape2, SphereShape)));
				break;
			case VolumeType(ShapeType::SPHERE, ShapeType::BOX):
				result.push_back(SphereVsBox(*pBody, *pBody2, HELP_CAST(pShape, SphereShape, pShape2, BoxShape)));
				break;
			case VolumeType(ShapeType::BOX, ShapeType::BOX):
				result.push_back(BoxVsBox(*pBody, *pBody2, HELP_CAST(pShape, BoxShape, pShape2, BoxShape)));
				break;
			case VolumeType(ShapeType::BOX, ShapeType::CAPSULE):
				result.push_back(BoxVsCapsule(*pBody, *pBody2, HELP_CAST(pShape, BoxShape, pShape2, CapsuleShape)));
				break;
			case VolumeType(ShapeType::SPHERE, ShapeType::CAPSULE):
				result.push_back(SphereVsCapsule(*pBody, *pBody2, HELP_CAST(pShape, SphereShape, pShape2, CapsuleShape)));
				break;
			case VolumeType(ShapeType::CAPSULE, ShapeType::CAPSULE):
				result.push_back(CapsuleVsCapsule(*pBody, *pBody2, HELP_CAST(pShape, CapsuleShape, pShape2, CapsuleShape)));
				break;
			default:
				result.push_back(Manifold());
			}
			return result;
		}

		bool Collision::CheckBoundingBoxes(const RigidBody& bodyA, const RigidBody& bodyB) const
		{
			return bodyA.GetAABB().IsInside(bodyB.GetAABB());
		}

#undef VolumeType
#undef HELP_CAST

	}
}
