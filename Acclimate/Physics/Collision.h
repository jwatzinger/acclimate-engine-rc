#pragma once
#include "Manifold.h"
#include <vector>

namespace acl
{
	namespace physics
	{

		class RigidBody;

		class Collision
		{
		public:

			std::vector<Manifold> CollisionGenerateManifold(RigidBody& bodyA, RigidBody& bodyB) const;
			bool CheckBoundingBoxes(const RigidBody& bodyA, const RigidBody& bodyB) const;
		};

	}
}


