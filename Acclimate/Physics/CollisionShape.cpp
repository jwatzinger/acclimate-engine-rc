#include "CollisionShape.h"

namespace acl
{
	namespace physics
	{

		CollisionShape::CollisionShape(ShapeType type) : m_type(type)
		{
		}

		CollisionShape::~CollisionShape(void)
		{
		}
	
		ShapeType CollisionShape::GetType(void) const
		{
			return m_type;
		}

	}
}


