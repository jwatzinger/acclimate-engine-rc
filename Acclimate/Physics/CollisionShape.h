#pragma once
#include "..\Core\Dll.h"
#include "..\Math\AABB.h"

namespace acl
{
	namespace physics
	{

		enum class ShapeType
		{
			SPHERE, BOX, CAPSULE, TERRAIN
		};

		struct Transform;

		class ACCLIMATE_API CollisionShape
		{
		public:

			CollisionShape(ShapeType type);
			virtual ~CollisionShape(void);
				
			ShapeType GetType(void) const;

			virtual float ComputeVolume(void) const = 0;
			virtual math::Vector3 CalculateInertia(const float mass) const = 0;
			virtual math::AABB ComputeAABB(const Transform& transform) const = 0;
		private:

			ShapeType m_type;
		};
		
	}
}


