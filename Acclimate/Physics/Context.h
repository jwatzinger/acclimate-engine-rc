#pragma once
#include "..\Core\Dll.h"
#include "Materials.h"
#include "Shapes.h"

namespace acl
{
	namespace physics
	{

		class World;
		class IPhysicsLoader;

		struct ACCLIMATE_API Context
		{
			Context(World& world, IPhysicsLoader& loader, Materials& materials, Shapes& shapes) : world(world), loader(loader),
				materials(materials), shapes(shapes)
			{
			}

			World& world;
			IPhysicsLoader& loader;
			Materials& materials;
			Shapes& shapes;
		};

	}
}