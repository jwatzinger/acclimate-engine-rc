#pragma once
#include <string>

namespace acl
{
	namespace physics
	{

		class IPhysicsLoader
		{
		public: 
			
			virtual ~IPhysicsLoader(void) = 0 {}

			virtual void Load(const std::wstring& stName) const = 0;

		};

	}
}