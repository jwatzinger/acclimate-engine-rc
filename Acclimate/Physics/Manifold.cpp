#include "Manifold.h"
#include <minmax.h>
#include <math.h>
#include "RigidBody.h"
#include "Material.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace physics
	{

		Manifold::Manifold(void) : m_pBodyA(nullptr), m_pBodyB(nullptr), m_bValid(false), m_penetration(0.0f), m_energy(0.0f), m_staticFriction(0.0f),
			m_dynamicFriction(0.0f)
		{
		}

		Manifold::Manifold(RigidBody& bodyA, RigidBody& bodyB, float penetration, const math::Vector3& vNormal, const math::Vector3& vContact) : m_pBodyA(&bodyA), m_pBodyB(&bodyB),
			m_vNormal(vNormal), m_penetration(penetration), m_bValid(true), m_vContact(vContact), m_energy(0.0f)
		{
			const auto& materialA = bodyA.GetMaterial();
			const auto& materialB = bodyB.GetMaterial();
			m_staticFriction = sqrt(materialA.staticFriction * materialB.staticFriction);
			m_dynamicFriction = sqrt(materialA.dynamicFriction * materialB.dynamicFriction);

			const math::Vector3 vRelVelocity = CalculateRelativeVelocity();

			if(vRelVelocity.squaredLength() >= 0.5f)
				m_energy = min(m_pBodyA->GetMaterial().restitution, m_pBodyB->GetMaterial().restitution);
		}

		bool Manifold::IsValid(void) const
		{
			return m_bValid;
		}

		void Manifold::ResolveCollision(void) const
		{
			ACL_ASSERT(m_pBodyA && m_pBodyB);

			const math::Vector3 vRelA = m_vContact - m_pBodyA->m_transform.vPosition;
			const math::Vector3 vRelB = m_vContact - m_pBodyB->m_transform.vPosition;

			const math::Vector3 vRelVelocity = CalculateRelativeVelocity();

			const float alongNormal = vRelVelocity.Dot(m_vNormal);

			if(alongNormal > 0.0f)
				return;

			const float AcrossN = vRelA.Cross(m_vNormal).length();
			const float BcrossN = vRelB.Cross(m_vNormal).length();

			// TODO: check if this is the correct way to apply inertia here
			const auto vInertiaMass = m_pBodyA->GetInvInertia() * sqrt(AcrossN) + sqrt(BcrossN) * m_pBodyB->GetInvInertia();
			const float invMassSum = m_pBodyA->GetInvMass() + m_pBodyB->GetInvMass() + vInertiaMass.x + vInertiaMass.y + vInertiaMass.z;
			float impulse = -(1.0f + m_energy) * alongNormal;
			impulse /= invMassSum;

			const math::Vector3 vImpulse = impulse * m_vNormal;

			m_pBodyA->ApplyImpulse(-vImpulse, vRelA);
			m_pBodyB->ApplyImpulse(vImpulse, vRelB);

			/********************************************
			* Friction
			*********************************************/

			const math::Vector3 vRelVelocityAfter = CalculateRelativeVelocity();

				// Solve for the tangent vector
			math::Vector3 vTangent = vRelVelocityAfter - vRelVelocityAfter.Dot(m_vNormal) * m_vNormal;
			vTangent.Normalize();

				// Solve for magnitude to apply along the friction vector
			float jt = -vRelVelocityAfter.Dot(vTangent);
			jt /= invMassSum;

			if(jt == 0.0f)
				return;

				// Clamp magnitude of friction and create impulse vector
			math::Vector3 vFrictionImpulse;
			if(abs(jt) < impulse * m_staticFriction)
				vFrictionImpulse = jt * vTangent;
			else
				vFrictionImpulse = -impulse * vTangent * m_dynamicFriction;

			// Apply
			m_pBodyA->ApplyImpulse(-vFrictionImpulse, vRelA);
			m_pBodyB->ApplyImpulse(vFrictionImpulse, vRelB);
		}

		void Manifold::PositionCorrection(void) const
		{
			const float percent = 0.4f; // usually 20% to 80%
			const float slop = 0.05f; // usually 0.01 to 0.1
			math::Vector3 vCorrection = max(m_penetration - slop, 0.0f) / (m_pBodyA->GetInvMass() + m_pBodyB->GetInvMass()) * percent * m_vNormal;
			m_pBodyA->m_transform.vPosition -= m_pBodyA->GetInvMass() * vCorrection;
			m_pBodyB->m_transform.vPosition += m_pBodyB->GetInvMass() * vCorrection;
		}

		math::Vector3 Manifold::CalculateRelativeVelocity(void) const
		{
			const math::Vector3 vRelA = m_vContact - m_pBodyA->m_transform.vPosition;
			const math::Vector3 vRelB = m_vContact - m_pBodyB->m_transform.vPosition;

			return m_pBodyB->GetRealVelocity() + m_pBodyB->m_vAngularVelocity.Cross(vRelB) -
				m_pBodyA->GetRealVelocity() - m_pBodyA->m_vAngularVelocity.Cross(vRelA);
		}

	}
}

