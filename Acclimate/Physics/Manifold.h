#pragma once
#include "..\Math\Vector3.h"

namespace acl
{
	namespace physics
	{

		class RigidBody;

		class Manifold
		{
		public:

			Manifold(void);
			Manifold(RigidBody& bodyA, RigidBody& bodyB, float penetration, const math::Vector3& vNormal, const math::Vector3& vContact);

			bool IsValid(void) const;

			void ResolveCollision(void) const;
			void PositionCorrection(void) const;

		private:

			math::Vector3 CalculateRelativeVelocity(void) const;

			RigidBody* m_pBodyA;
			RigidBody* m_pBodyB;

			bool m_bValid;
			float m_penetration, m_staticFriction, m_dynamicFriction, m_energy;
			math::Vector3 m_vNormal, m_vContact;
		};

	}
}