#pragma once

namespace acl
{
	namespace physics
	{

		struct Material
		{
			Material(float density, float restitution, float staticFriction, float dynamicFriction) : density(density), restitution(restitution),
				staticFriction(staticFriction), dynamicFriction(dynamicFriction)
			{
			}

			float density, restitution, staticFriction, dynamicFriction;
		};

	}
}