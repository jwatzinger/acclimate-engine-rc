#pragma once
#include "Material.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
	namespace physics
	{

		typedef core::Resources<std::wstring, Material> Materials;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, Material>;

	}
}