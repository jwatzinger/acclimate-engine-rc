#include "Package.h"

namespace acl
{
	namespace physics
	{

		Package::Package(void) : m_loader(m_materials, m_shapes), m_context(m_world, m_loader, m_materials, m_shapes)
		{
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

		void Package::Update(double dt)
		{
			m_world.Update(dt);
		}

	}
}
