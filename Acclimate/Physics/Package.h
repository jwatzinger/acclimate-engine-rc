#pragma once
#include "Context.h"
#include "World.h"
#include "Materials.h"
#include "Shapes.h"
#include "PhysicsLoaderXML.h"

namespace acl
{
	namespace physics
	{
		
		class Package
		{
		public:
			Package(void);

			const Context& GetContext(void) const;

			void Update(double dt);

		private:

			World m_world;
			Materials m_materials;
			Shapes m_shapes;
			PhysicsLoaderXML m_loader;
			Context m_context;
		};

	}
}


