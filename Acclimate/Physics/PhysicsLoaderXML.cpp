#include "PhysicsLoaderXML.h"
#include "SphereShape.h"
#include "BoxShape.h"
#include "CapsuleShape.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace physics
	{

		PhysicsLoaderXML::PhysicsLoaderXML(Materials& materials, Shapes& shapes) : m_pMaterials(&materials),
			m_pShapes(&shapes)
		{
		}

		void PhysicsLoaderXML::Load(const std::wstring& stName) const
		{
			xml::Doc doc;
			doc.LoadFile(stName);

			if(auto pRoot = doc.Root(L"Physics"))
			{
				if(auto pMaterialNode = pRoot->FirstNode(L"Materials"))
				{
					if(auto pMaterials = pMaterialNode->Nodes(L"Material"))
					{
						for(auto pMaterial : *pMaterials)
						{
							auto& material = *new Material(pMaterial->Attribute(L"density")->AsFloat(), pMaterial->Attribute(L"restitution")->AsFloat(), pMaterial->Attribute(L"staticFriction")->AsFloat(), pMaterial->Attribute(L"dynamicFriction")->AsFloat());
							m_pMaterials->Add(pMaterial->GetValue(), material);
						}
					}
				}

				if(auto pShapes = pRoot->FirstNode(L"Shapes"))
				{
					if(auto pSpheres = pShapes->Nodes(L"Sphere"))
					{
						for(auto pSphere : *pSpheres)
						{
							m_pShapes->Add(pSphere->GetValue(), *new SphereShape(pSphere->Attribute(L"radius")->AsFloat()));
						}
					}

					if(auto pBoxes = pShapes->Nodes(L"Box"))
					{
						for(auto pBox : *pBoxes)
						{
							m_pShapes->Add(pBox->GetValue(), *new BoxShape(pBox->Attribute(L"x")->AsFloat(), pBox->Attribute(L"y")->AsFloat(), pBox->Attribute(L"z")->AsFloat()));
						}
					}

					if (auto pCapsule = pShapes->Nodes(L"Capsule"))
					{
						for (auto pCapsule : *pCapsule)
						{
							m_pShapes->Add(pCapsule->GetValue(), *new CapsuleShape(pCapsule->Attribute(L"height")->AsFloat(), pCapsule->Attribute(L"radius")->AsFloat()));
						}
					}
				}
			}
		}
	}
}
