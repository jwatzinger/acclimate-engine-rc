#pragma once
#include "IPhysicsLoader.h"
#include "Materials.h"
#include "Shapes.h"

namespace acl
{
	namespace physics
	{

		class PhysicsLoaderXML :
			public IPhysicsLoader
		{
		public:
			PhysicsLoaderXML(Materials& materials, Shapes& shapes);

			void Load(const std::wstring& stName) const override;

		private:

			Materials* m_pMaterials;
			Shapes* m_pShapes;
		};

	}
}


