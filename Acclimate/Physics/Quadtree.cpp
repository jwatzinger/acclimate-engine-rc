#include "Quadtree.h"
#include "RigidBody.h"

namespace acl
{
	namespace physics
	{

		Quadtree::Quadtree(const float x, const float y, const float width, const float height, const unsigned int level, const unsigned int maxLevel) :
			m_xPos(x), m_yPos(y), m_width(width), m_height(height), m_level(level), m_maxLevel(maxLevel)
		{
			if (m_level == m_maxLevel)
				return;

			m_NW = new Quadtree(x, y, width / 2.0f, height / 2.0f, level + 1, maxLevel);
			m_NE = new Quadtree(x + width / 2.0f, y, width / 2.0f, height / 2.0f, level + 1, maxLevel);
			m_SW = new Quadtree(x, y + height / 2.0f, width / 2.0f, height / 2.0f, level + 1, maxLevel);
			m_SE = new Quadtree(x + width / 2.0f, y + height / 2.0f, width / 2.0f, height / 2.0f, level + 1, maxLevel);
		}

		Quadtree::~Quadtree()
		{
			if (m_level == m_maxLevel)
				return;

			delete m_SE;
			delete m_SW;
			delete m_NE;
			delete m_NW;
		}

		void Quadtree::AddObject(RigidBody *body) 
		{
			if (m_level == m_maxLevel) 
			{
				m_vBodies.push_back(body);
			}
			else if (contains(m_NW, body))
			{
				m_NW->AddObject(body);
			}
			else if (contains(m_NE, body)) 
			{
				m_NE->AddObject(body); 
				return;
			}
			else if (contains(m_SW, body)) 
			{
				m_SW->AddObject(body); 
				return;
			}
			else if (contains(m_SE, body)) 
			{
				m_SE->AddObject(body); 
				return;
			}
			else if(contains(this, body))
			{
				m_vBodies.push_back(body);
			}
		}

		const std::vector<RigidBody*>& Quadtree::GetObjectsAt(const float x, const float y) const 
		{
			if (m_level == m_maxLevel)
				return m_vBodies;

			std::vector<RigidBody*> returnObjects, childReturnObjects;
			if (!m_vBodies.empty()) 
				returnObjects = m_vBodies;
			
			if (x > m_xPos + m_width / 2.0f && x < m_xPos + m_width) 
			{
				if (y > m_yPos + m_height / 2.0f && y < m_yPos + m_height) 
				{
					childReturnObjects = m_SE->GetObjectsAt(x, y);
					returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
					return returnObjects;
				}
				else if (y > m_yPos && y <= m_yPos + m_height / 2.0f) 
				{
					childReturnObjects = m_NE->GetObjectsAt(x, y);
					returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
					return returnObjects;
				}
			}
			else if (x > m_xPos && x <= m_xPos + m_width / 2.0f) 
			{
				if (y > m_yPos + m_height / 2.0f && y < m_yPos + m_height) 
				{
					childReturnObjects = m_SW->GetObjectsAt(x, y);
					returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
					return returnObjects;
				}
				else if (y > m_yPos && y <= m_yPos + m_height / 2.0f) 
				{
					childReturnObjects = m_NW->GetObjectsAt(x, y);
					returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
					return returnObjects;
				}
			}

			return returnObjects;
		}

		void Quadtree::Clear() 
		{
			if (m_level == m_maxLevel) 
			{
				m_vBodies.clear();
				return;
			}
			else 
			{
				m_NW->Clear();
				m_NE->Clear();
				m_SW->Clear();
				m_SE->Clear();
			}
			if (!m_vBodies.empty()) 
			{
				m_vBodies.clear();
			}
		}

		bool Quadtree::contains(const Quadtree *child, const RigidBody *body) const
		{
			//body->GetShape()->GetAABB();
			const float width = 100; //TODO calculate width and height
			const float height = 100;
			return !(body->m_transform.vPosition.x < child->m_xPos ||
					 body->m_transform.vPosition.y < child->m_yPos ||
					 body->m_transform.vPosition.x > child->m_xPos + child->m_width ||
					 body->m_transform.vPosition.y > child->m_yPos + child->m_height ||
					 body->m_transform.vPosition.x + width < child->m_xPos ||
					 body->m_transform.vPosition.y + height < child->m_yPos ||
					 body->m_transform.vPosition.x + width > child->m_xPos + child->m_width ||
					 body->m_transform.vPosition.y + height > child->m_yPos + child->m_height);
		}
	}
}

