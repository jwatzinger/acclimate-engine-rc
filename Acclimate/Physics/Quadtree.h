#pragma once

#include <vector>

namespace acl
{
	namespace physics
	{
		class RigidBody;

		//noch nicht verwendet und deshalb auch noch nicht getestet
		class Quadtree
		{
		public:
			Quadtree(const float x, const float y, const float width, const float height, const unsigned int level, const unsigned int maxLevel);
			~Quadtree();

			void AddObject(RigidBody *body);
			const std::vector<RigidBody*>& GetObjectsAt(const float x, const float y) const;
			void Clear();
		private:
			float m_xPos;
			float m_yPos;
			float m_width;
			float m_height;
			unsigned int m_level;
			const unsigned int m_maxLevel;
			unsigned int m_maxObjects;
			std::vector<RigidBody*> m_vBodies;

			Quadtree* m_parent;
			Quadtree* m_NW;
			Quadtree* m_NE;
			Quadtree* m_SW;
			Quadtree* m_SE;
			bool contains(const Quadtree *child, const RigidBody *rigidbody) const;
		};
	}
}
