#include "RegisterScript.h"
#include "RigidBody.h"
#include "SoftBody.h"
#include "Material.h"
#include "World.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace physics
	{
		namespace detail
		{
			World* _pWorld;
			script::Core* _pScript = nullptr;
		}

		const math::Vector3& getPosition(const RigidBody* pBody)
		{
			return pBody->m_transform.vPosition;
		}

		void connectOnCollision(RigidBody* rigidBody, asIScriptFunction* pFunction)
		{
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			rigidBody->OnCollision.Connect(pFunc, &script::Function::Call<void, const RigidBody&>);
		}
		
		void setWind(const math::Vector3& vWind)
		{
			detail::_pWorld->SetWind(vWind);
		}

		void RegisterScript(script::Core& script, World& world)
		{
			detail::_pScript = &script; 
			detail::_pWorld = &world;

			// rigidBody
			auto rigidBody = script.RegisterReferenceType("RigidBody", script::REF_NOCOUNT);
			rigidBody.RegisterMethod("void ApplyImpulse(const Vector3& in, const Vector3& in)", asMETHOD(RigidBody, ApplyImpulse));
			rigidBody.RegisterMethod("void SetLinearVelocity(const Vector3& in)", asMETHOD(RigidBody, SetLinearVelocity));
			rigidBody.RegisterMethod("void SetDamping(float)", asMETHOD(RigidBody, SetDamping));
			rigidBody.RegisterMethod("void SetAngularFactors(float, float, float)", asMETHODPR(RigidBody, SetAngularFactors, (float, float, float), void));
			rigidBody.RegisterMethod("void SetAngularFactors(const Vector3& in)", asMETHODPR(RigidBody, SetAngularFactors, (const math::Vector3&), void));
			rigidBody.RegisterMethod("void StopMovement()", asMETHOD(RigidBody, StopMovement));
			rigidBody.RegisterMethod("const Vector3& GetPosition() const", asFUNCTION(getPosition), asCALL_CDECL_OBJFIRST);
			rigidBody.RegisterMethod("const Vector3& GetVelocity() const", asMETHOD(RigidBody, GetVelocity));
			rigidBody.RegisterMethod("bool IsFalling() const", asMETHOD(RigidBody, IsFalling));

			// softBody
			auto softBody = script.RegisterReferenceType("SoftBody", script::REF_NOCOUNT);
			softBody.RegisterMethod("void SetMass(uint, float)", asMETHODPR(SoftBody, SetMass, (unsigned int, float), void));
			softBody.RegisterMethod("void SetMass(uint, uint, float)", asMETHODPR(SoftBody, SetMass, (unsigned int, unsigned int, float), void));
			softBody.RegisterMethod("const Vector2i& GetSize(void) const", asMETHOD(SoftBody, GetSize));
			softBody.RegisterMethod("void AddConstraint(uint, SoftBody@, uint)", asMETHODPR(SoftBody, AddConstraint, (unsigned int, SoftBody&, unsigned int), void));
			softBody.RegisterMethod("void MoveNode(uint, const Vector3& in)", asMETHODPR(SoftBody, MoveNode, (unsigned int, const math::Vector3&), void));

			// material
			auto material = script.RegisterReferenceType("Material", script::REF_NOCOUNT);

			// collision shape
			auto collisionShape = script.RegisterReferenceType("CollisionShape", script::REF_NOCOUNT);
			
			// globals
			script.RegisterGlobalFunction("void setWind(const Vector3& in)", asFUNCTION(setWind));

			//signals
			script.RegisterFuncdef("void OnCollisionCallback(const RigidBody@)");
			rigidBody.RegisterMethod("void ConnectOnCollision(OnCollisionCallback@)", asFUNCTION(connectOnCollision), asCALL_CDECL_OBJFIRST);
		}

	}
}
