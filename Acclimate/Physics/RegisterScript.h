#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace physics
	{
		class World;

		void RegisterScript(script::Core& core, World& world);

	}
}