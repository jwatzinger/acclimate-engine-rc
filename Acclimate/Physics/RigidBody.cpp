#include "RigidBody.h"
#include <math.h>
#include <minmax.h>
#include "SphereShape.h"
#include "BoxShape.h"
#include "Material.h"

#include "..\System\Log.h"

namespace acl
{
	namespace physics
	{

		RigidBody::RigidBody(Material& material, CollisionShape& shape, const Transform& transform, const MotionType motiontype, const CollisionType collision, float damping) : m_pMaterial(&material),
			m_pShape(&shape), m_transform(transform), m_invMass(0.0f), m_MotionType(motiontype), m_CollisionType(collision), m_damping(damping), m_vAngularFactors(1.0f, 1.0f, 1.0f)
		{
			CalculateMass();
		}

		void RigidBody::SetLinearVelocity(const math::Vector3& vMovement)
		{
			m_vVelocity += vMovement;
		}

		void RigidBody::SetDamping(float damping)
		{
			m_damping = max(min(1.0f, 1.0f - damping), 0.0f);
		}

		void RigidBody::SetAngularFactors(float x, float y, float z)
		{
			m_vAngularFactors.x = x;
			m_vAngularFactors.y = y;
			m_vAngularFactors.z = z;
		}

		void RigidBody::SetAngularFactors(const math::Vector3& vFactors)
		{
			m_vAngularFactors = vFactors;
		}

		MotionType RigidBody::GetMotionType(void) const
		{
			return m_MotionType;
		}

		CollisionType RigidBody::GetCollisionType(void) const
		{
			return m_CollisionType;
		}

		float RigidBody::GetInvMass(void) const
		{
			return m_invMass;
		}

		const math::Vector3& RigidBody::GetInvInertia(void) const
		{
			return m_invInertia;
		}

		const math::Vector3& RigidBody::GetVelocity(void) const
		{
			return m_vVelocity;
		}

		const CollisionShape& RigidBody::GetShape(void) const
		{
			return *m_pShape;
		}

		const Material& RigidBody::GetMaterial(void) const
		{
			return *m_pMaterial;
		}

		math::AABB RigidBody::GetAABB(void) const
		{
			return m_pShape->ComputeAABB(m_transform);
		}

		math::Vector3 RigidBody::GetRealVelocity(void) const
		{
			return m_vVelocity + m_vGravity;
		}

		bool RigidBody::IsFalling(void) const
		{
			// TODO: fix for other directions of gravity
			// also, fix or generalize gravity bias
			return m_vGravity.y <= -0.001f;
		}

		void RigidBody::ApplyImpulse(const math::Vector3& vImpulse, const math::Vector3& vContact)
		{
			auto vMassImpulse = m_invMass * vImpulse;
			// TODO: fix for gravity in other direction
			if(m_vGravity.y < 0.0f && vMassImpulse.y > 0.0f)
			{
				if(vMassImpulse.y >= abs(m_vGravity.y))
				{
					vMassImpulse.y += m_vGravity.y;
					m_vGravity.y = 0.0f;
				}
				else
				{
					m_vGravity.y += vMassImpulse.y;
					vMassImpulse.y = 0.0f;
				}
			}

			m_vVelocity += vMassImpulse;
			m_vAngularVelocity += m_invInertia * vContact.Cross(vImpulse) * m_vAngularFactors;
		}

		void RigidBody::StopMovement(void)
		{
			m_vVelocity = math::Vector3(0, 0, 0);
		}

		void RigidBody::IntegrateVelocities(const double dt)
		{
			if (!CanMove())
				return;

			// TODO: fix out how to resolve velocity vs gravity in a less hackish way
			if(m_vVelocity.y == abs(m_vGravity.y))
			{
				m_vVelocity.y = 0.0f;
				m_vGravity.y = 0.0f;
			}

			// Update linear position
			m_transform.vPosition += m_vVelocity * dt;
			m_transform.vPosition += m_vGravity * dt;
			m_transform.orientation += dt * (0.5f * math::Quaternion(0, m_vAngularVelocity) * m_transform.orientation);
			m_transform.orientation.Normalize();

			// apply damping
			const float dampFactor = pow(m_damping, (float)dt);
			m_vVelocity *= dampFactor;
			m_vAngularVelocity *= dampFactor;
		}

		void RigidBody::IntegrateForce(const double dt, const math::Vector3& vGravity)
		{
			if(m_MotionType != MotionType::DYNAMIC || m_invMass == 0.0f)
				return;

			m_vGravity += vGravity * dt;
			m_vVelocity += (m_vForce * GetInvMass())* dt;
			m_vAngularVelocity += m_vTorque * GetInvInertia() * dt * m_vAngularFactors;
		}

		bool RigidBody::CanMove() const
		{
			return m_MotionType != MotionType::STATIC && ((m_MotionType == MotionType::KINETIC) || (m_invMass != 0.0f));
		}

		void RigidBody::CalculateMass(void)
		{
			const float mass = m_pMaterial->density * m_pShape->ComputeVolume();

			if(mass <= 0.0f)
				m_invMass = 0.0f;
			else
				m_invMass = 1.0f / mass;

			CalculateInertia(mass);
		}

		void RigidBody::CalculateInertia(float mass)
		{
			//anschauen
			const math::Vector3 inertia = m_pShape->CalculateInertia(mass);

			if(inertia.x <= 0.0f)
				m_invInertia.x = 0.0f;
			else
				m_invInertia.x = 1.0f / inertia.x;
			if(inertia.y <= 0.0f)
				m_invInertia.y = 0.0f;
			else
				m_invInertia.y = 1.0f / inertia.y;
			if(inertia.z <= 0.0f)
				m_invInertia.z = 0.0f;
			else
				m_invInertia.z = 1.0f / inertia.z;
		}

	}
}
