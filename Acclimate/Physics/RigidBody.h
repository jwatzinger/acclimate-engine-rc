#pragma once
#include "Transform.h"
#include "..\Core\Dll.h"
#include "..\Math\AABB.h"
#include "..\Core\Signal.h"

namespace acl
{
	namespace physics
	{
		//�berall wo die masse verwendet wird auf kinetic pr�fen, ansonsten dynamic
		//static: �berpr�fung ob beide k�rper masse null haben (stattdessen)
		enum class MotionType
		{
			//normal, static (), kinetic (masse = 0)
			DYNAMIC = 0, STATIC, KINETIC, SIZE
		};

		//is used for ghost objects
		enum class CollisionType
		{
			STANDARD = 0, GHOST, SIZE
		};

		struct Material;
		class CollisionShape;

		class RigidBody;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Signal<const RigidBody&>;

		class ACCLIMATE_API RigidBody
		{
		public:
			
			RigidBody(Material& material, CollisionShape& shape, const Transform& transform, const MotionType motiontype, const CollisionType collisiontype, float damping);
			
			void SetLinearVelocity(const math::Vector3& vMovement);
			void SetDamping(float damping);
			void SetAngularFactors(float x, float y, float z);
			void SetAngularFactors(const math::Vector3& vFactors);

			MotionType GetMotionType(void) const;
			CollisionType GetCollisionType(void) const; 
			float GetInvMass(void) const;
			const math::Vector3& GetInvInertia(void) const;
			const math::Vector3& GetVelocity(void) const;
			const CollisionShape& GetShape(void) const;
			const Material& GetMaterial(void) const;
			math::AABB GetAABB(void) const;
			math::Vector3 GetRealVelocity(void) const;
			bool IsFalling(void) const;

			void ApplyImpulse(const math::Vector3& vImpulse, const math::Vector3& vContact);
			void StopMovement(void);
			void IntegrateVelocities(const double dt);
			void IntegrateForce(const double dt, const math::Vector3& vGravity);

			Transform m_transform;
			math::Vector3 m_vForce;
			math::Vector3 m_vAngularVelocity, m_vTorque, m_vAngularFactors;

			core::Signal<const RigidBody&> OnCollision;
		private:
			bool CanMove() const;
			void CalculateMass(void);
			void CalculateInertia(float mass);

			float m_invMass, m_damping;
			math::Vector3 m_invInertia;
			math::Vector3 m_vGravity, m_vVelocity;
			
			Material* m_pMaterial;
			CollisionShape* m_pShape;
			MotionType m_MotionType;
			CollisionType m_CollisionType;
		};

	}
}

