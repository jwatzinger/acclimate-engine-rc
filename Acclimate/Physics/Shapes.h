#pragma once
#include "CollisionShape.h"
#include "..\Core\Dll.h"
#include "..\Core\Resources.h"

namespace acl
{
	namespace physics
	{

		typedef core::Resources<std::wstring, CollisionShape> Shapes;

		EXPORT_TEMPLATE template class ACCLIMATE_API core::Resources<std::wstring, CollisionShape>;

	}
}