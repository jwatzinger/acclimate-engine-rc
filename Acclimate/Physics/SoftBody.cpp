#include "SoftBody.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace physics
	{

		SoftBody::SoftBody(float mass, float damping, unsigned int width, unsigned int height, const math::Vector3& vTranslation, float sizeX, float sizeY, const math::Vector3& vDir) :
			m_vSize(width, height), m_numParticles(m_vSize.x*m_vSize.y), m_particles(m_numParticles)
		{
			float invMass;
			if(mass != 0.0f)
				invMass = 1.0f / mass;
			else
				invMass = 0.0f;

			const float scaleX = sizeX / width;
			const float scaleY = sizeY / height;

			const auto vNormalDir = vDir.normal();

			for(int i = 0; i < m_vSize.x; i++)
			{
				for(int j = 0; j < m_vSize.y; j++)
				{
					const unsigned int index = j * m_vSize.x + i;

					const auto vPos = vTranslation + vNormalDir * i * scaleX;
					const auto vNrm = vNormalDir.Cross(math::Vector3(0.0f, -1.0f, 0.0f));
					const auto vPosZ = vNrm * j * scaleY;
					const auto vFinalPos = vPos + vPosZ;

					m_particles.x[index] = vFinalPos.x;
					m_particles.y[index] = vFinalPos.y;
					m_particles.z[index] = vFinalPos.z;
					m_particles.oldX[index] = m_particles.x[index];
					m_particles.oldY[index] = m_particles.y[index];
					m_particles.oldZ[index] = m_particles.z[index];
					m_particles.accX[index] = 0.0f;
					m_particles.accY[index] = 0.0f;
					m_particles.accZ[index] = 0.0f;
					m_particles.invMass[index] = invMass;
					m_particles.damp[index] = damping;
				}
				
			}

			/**********************************
			* Constraints
			**********************************/

			// cloth
			if(m_vSize.x > 1 && m_vSize.y > 1)
			{
				// structural & shearing constraints
				for(int x = 0; x<m_vSize.x; x++)
				{
					for(int y = 0; y<m_vSize.y; y++)
					{
						if(x<m_vSize.x - 1)
							CreateConstraint(x, y, x + 1, y);
						if(y<m_vSize.y - 1)
							CreateConstraint(x, y, x, y + 1);
						if(x<m_vSize.x - 1 && y<m_vSize.y - 1)
							CreateConstraint(x, y, x + 1, y + 1);
						if(x<m_vSize.x - 1 && y<m_vSize.y - 1)
							CreateConstraint(x + 1, y, x, y + 1);
					}
				}

				// bending contraints
				for(int x = 0; x<m_vSize.x; x++)
				{
					for(int y = 0; y<m_vSize.y; y++)
					{
						if(x<m_vSize.x - 2)
							CreateConstraint(x, y, x + 2, y);
						if(y<m_vSize.y - 2)
							CreateConstraint(x, y, x, y + 2);
					}
				}
			}
			// rope
			else
			{
				for(unsigned int i = 0; i<m_numParticles-1; i++)
				{
					CreateConstraint(i, 0, i + 1, 0);
					if(i < m_numParticles - 2)
						CreateConstraint(i, 0, i + 2, 0);
				}
			}
		}

		void SoftBody::SetMass(unsigned int node, float mass)
		{
			if(node < m_numParticles)
			{
				if(mass != 0.0f)
					m_particles.invMass[node] = 1.0f / mass;
				else
					m_particles.invMass[node] = 0.0f;
			}
		}

		void SoftBody::SetMass(unsigned int x, unsigned int y, float mass)
		{
			const unsigned int node = y * m_vSize.x + x;
			SetMass(node, mass);
		}

		const math::Vector2& SoftBody::GetSize(void) const
		{
			return m_vSize;
		}

		const SoftParticle& SoftBody::GetParticles(void) const
		{
			return m_particles;
		}

		math::Vector3 SoftBody::GetNormal(unsigned int node) const
		{
			return math::Vector3();
		}

		math::Vector3 SoftBody::GetNormal(unsigned int x, unsigned int y) const
		{
			if((int)x < m_vSize.x - 1 && (int)y < m_vSize.y - 1)
				return CalculateNormal(y*m_vSize.x + x, y*m_vSize.x + x + 1, (y + 1)*m_vSize.x + x);
			else
				return math::Vector3();
		}

		void SoftBody::AddGravity(const math::Vector3& vGravity)
		{
			for(unsigned int i = 0; i < m_numParticles; i++)
			{
				m_particles.accX[i] += vGravity.x;
				m_particles.accY[i] += vGravity.y;
				m_particles.accZ[i] += vGravity.z;
			}
		}

		void SoftBody::AddForce(const math::Vector3& vForce)
		{
			for(unsigned int i = 0; i < m_numParticles; i++)
			{
				m_particles.accX[i] += vForce.x * m_particles.invMass[i];
				m_particles.accY[i] += vForce.y * m_particles.invMass[i];
				m_particles.accZ[i] += vForce.z * m_particles.invMass[i];
			}
		}
		
		void SoftBody::AddWind(const math::Vector3& vWind)
		{
			// cloth
			if(m_vSize.x > 1 && m_vSize.y > 1)
			{
				for(int x = 0; x<m_vSize.x - 1; x++)
				{
					for(int y = 0; y<m_vSize.y - 1; y++)
					{
						AddWindForce(vWind, x + 1, y, x, y, x, y + 1);
						AddWindForce(vWind, x + 1, y + 1, x + 1, y, x, y + 1);
					}
				}
			}
			// rope
			else
			{
				for(unsigned int i = 1; i < m_numParticles-1; i++)
				{
					AddWindForce(vWind, i - 1, 0, i, 0, i + 1, 0);
				}
			}
		}

		void SoftBody::AddConstraint(unsigned int node, SoftBody& targetBody, unsigned int targetNode)
		{
			if(node < m_numParticles && targetNode < targetBody.m_numParticles)
			{
				targetBody.m_particles.x[targetNode] = m_particles.x[node];
				targetBody.m_particles.y[targetNode] = m_particles.y[node];
				targetBody.m_particles.z[targetNode] = m_particles.z[node];
				m_vConstraints.emplace_back(m_particles, node, targetBody.m_particles, targetNode);
			}
		}

		void SoftBody::MoveNode(unsigned int node, const math::Vector3& vPosition)
		{
			if(node < m_numParticles)
			{
				m_particles.x[node] = vPosition.x;
				m_particles.y[node] = vPosition.y;
				m_particles.z[node] = vPosition.z;
				m_particles.oldX[node] = m_particles.x[node];
				m_particles.oldY[node] = m_particles.y[node];
				m_particles.oldZ[node] = m_particles.z[node];
			}
		}

		void SoftBody::Integrate(double dt)
		{
			// TODO: SSE
			for(unsigned int i = 0; i < m_numParticles; i++)
			{
				if(m_particles.invMass[i] <= 0.00001f)
					continue;

				const float tempX = m_particles.x[i];
				m_particles.x[i] += (m_particles.x[i] - m_particles.oldX[i])*(1.0f - m_particles.damp[i]) + m_particles.accX[i] * dt;
				m_particles.oldX[i] = tempX;
				m_particles.accX[i] = 0.0f;

				const float tempY = m_particles.y[i];
				m_particles.y[i] += (m_particles.y[i] - m_particles.oldY[i])*(1.0f - m_particles.damp[i]) + m_particles.accY[i] * dt;
				m_particles.oldY[i] = tempY;
				m_particles.accY[i] = 0.0f;

				const float tempZ = m_particles.z[i];
				m_particles.z[i] += (m_particles.z[i] - m_particles.oldZ[i])*(1.0f - m_particles.damp[i]) + m_particles.accZ[i] * dt;
				m_particles.oldZ[i] = tempZ;
				m_particles.accZ[i] = 0.0f;
			}
		}

		void SoftBody::ResolveConstraints(void)
		{
			for(auto& constraint : m_vConstraints)
			{
				constraint.Resolve();
			}
		}

		void SoftBody::CreateConstraint(unsigned int pX1, unsigned int pY1, unsigned int pX2, unsigned int pY2)
		{
			m_vConstraints.emplace_back(m_particles, pY1*m_vSize.x + pX1, m_particles, pY2*m_vSize.x + pX2);
		}

		void SoftBody::AddWindForce(const math::Vector3& vWind, unsigned int pX1, unsigned int pY1, unsigned int pX2, unsigned int pY2, unsigned int pX3, unsigned int pY3)
		{
			// triangle normal

			const unsigned int index1 = pY1*m_vSize.x + pX1;
			const unsigned int index2 = pY2*m_vSize.x + pX2;
			const unsigned int index3 = pY3*m_vSize.x + pX3;

			const math::Vector3 vPos1 = math::Vector3(m_particles.x[index1], m_particles.y[index1], m_particles.z[index1]);

			const math::Vector3 vDistance1 = math::Vector3(m_particles.x[index2], m_particles.y[index2], m_particles.z[index2]) - vPos1;
			const math::Vector3 vDistance2 = math::Vector3(m_particles.x[index3], m_particles.y[index3], m_particles.z[index3]) - vPos1;

			// TODO: find out which method is faster/produces better results
			//const math::Vector3 vNormal = vDistance1.Cross(vDistance2);
			//const math::Vector3 vForce = vNormal * vNormal.normal().Dot(vWind);

			const math::Vector3 vNormal = CalculateNormal(index1, index2, index3);
			const math::Vector3 vForce = vNormal * vNormal.Dot(vWind.normal()) * vWind.length();

			m_particles.accX[index1] += vForce.x * m_particles.invMass[index1];
			m_particles.accY[index1] += vForce.y * m_particles.invMass[index1];
			m_particles.accZ[index1] += vForce.z * m_particles.invMass[index1];

			m_particles.accX[index2] += vForce.x * m_particles.invMass[index2];
			m_particles.accY[index2] += vForce.y * m_particles.invMass[index2];
			m_particles.accZ[index2] += vForce.z * m_particles.invMass[index2];

			m_particles.accX[index3] += vForce.x * m_particles.invMass[index3];
			m_particles.accY[index3] += vForce.y * m_particles.invMass[index3];
			m_particles.accZ[index3] += vForce.z * m_particles.invMass[index3];
		}

		math::Vector3 SoftBody::CalculateNormal(unsigned int index1, unsigned int index2, unsigned int index3) const
		{
			const math::Vector3 vPos1 = math::Vector3(m_particles.x[index1], m_particles.y[index1], m_particles.z[index1]);

			const math::Vector3 vDistance1 = math::Vector3(m_particles.x[index2], m_particles.y[index2], m_particles.z[index2]) - vPos1;
			const math::Vector3 vDistance2 = math::Vector3(m_particles.x[index3], m_particles.y[index3], m_particles.z[index3]) - vPos1;

			return vDistance1.Cross(vDistance2).normal();
		}

	}
}

