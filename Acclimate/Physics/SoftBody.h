#pragma once
#include <vector>
#include "SoftParticle.h"
#include "SoftConstraint.h"
#include "..\Core\Dll.h"
#include "..\Math\Vector.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace physics
	{

		class ACCLIMATE_API SoftBody
		{
			typedef std::vector<SoftConstraint> ContraintVector;
		public:
			SoftBody(float mass, float damping, unsigned int width, unsigned int height, const math::Vector3& vTranslation, float sizeX, float sizeY, const math::Vector3& vDir);

			void SetMass(unsigned int node, float mass);
			void SetMass(unsigned int x, unsigned int y, float mass);

			const math::Vector2& GetSize(void) const;
			// todo: improve encapsulation
			const SoftParticle& GetParticles(void) const;
			math::Vector3 GetNormal(unsigned int node) const;
			math::Vector3 GetNormal(unsigned int x, unsigned int y) const;

			void AddGravity(const math::Vector3& vGravity);
			void AddForce(const math::Vector3& vForce);
			void AddWind(const math::Vector3& vWind);
			void AddConstraint(unsigned int node, SoftBody& targetBody, unsigned int targetNode);
			void MoveNode(unsigned int node, const math::Vector3& vPosition);

			void Integrate(double dt);
			void ResolveConstraints(void);

		private:
#pragma warning( disable: 4251 )
			void CreateConstraint(unsigned int pX1, unsigned int pY1, unsigned int pX2, unsigned int pY2);
			void AddWindForce(const math::Vector3& vWind, unsigned int pX1, unsigned int pY1, unsigned int pX2, unsigned int pY2, unsigned int pX3, unsigned int pY3);
			math::Vector3 CalculateNormal(unsigned int index1, unsigned int index2, unsigned int index3) const;

			math::Vector2 m_vSize;
			unsigned int m_numParticles;
			SoftParticle m_particles;

			ContraintVector m_vConstraints;
#pragma warning( default: 4251 )
		};

	}
}


