#include "SoftConstraint.h"
#include "SoftParticle.h"
#include "..\Math\Vector3.h"

namespace acl
{
	namespace physics
	{

		SoftConstraint::SoftConstraint(SoftParticle& particles1, unsigned int p1, SoftParticle& particles2, unsigned int p2) :
			m_pParticles1(&particles1), m_pParticles2(&particles2), m_p1(p1), m_p2(p2)
		{	
			m_restDistance = math::Vector3(particles1.x[p1] - particles2.x[p2], particles1.y[p1] - particles2.y[p2], particles1.z[p1] - particles2.z[p2]).length();
		}

		void SoftConstraint::Resolve(void)
		{
			const math::Vector3 vDist = math::Vector3(m_pParticles2->x[m_p2] - m_pParticles1->x[m_p1], m_pParticles2->y[m_p2] - m_pParticles1->y[m_p1], m_pParticles2->z[m_p2] - m_pParticles1->z[m_p1]);
			const math::Vector3 vCorrection = vDist *(1 - m_restDistance / vDist.length());
			//const math::Vector3 vCorrectionHalf = vCorrection * 0.5f;

			const float massSum = m_pParticles1->invMass[m_p1] + m_pParticles2->invMass[m_p2];
	
			if(m_pParticles1->invMass[m_p1] != 0.0f)
			{
				const math::Vector3 vCorrectionHalf = vCorrection * (m_pParticles1->invMass[m_p1] / massSum);

				m_pParticles1->x[m_p1] += vCorrectionHalf.x;
				m_pParticles1->y[m_p1] += vCorrectionHalf.y;
				m_pParticles1->z[m_p1] += vCorrectionHalf.z;
			}
			
			if(m_pParticles2->invMass[m_p2] != 0.0f)
			{
				const math::Vector3 vCorrectionHalf = vCorrection * (m_pParticles2->invMass[m_p2] / massSum);

				m_pParticles2->x[m_p2] -= vCorrectionHalf.x;
				m_pParticles2->y[m_p2] -= vCorrectionHalf.y;
				m_pParticles2->z[m_p2] -= vCorrectionHalf.z;
			}
		}

	}
}

