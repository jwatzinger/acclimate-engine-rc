#pragma once

namespace acl
{
	namespace physics
	{

		struct SoftParticle;

		class SoftConstraint
		{
		public:
			SoftConstraint(SoftParticle& particles1, unsigned int p1, SoftParticle& particles2, unsigned int p2);

			void Resolve(void);

		private:

			SoftParticle* m_pParticles1, *m_pParticles2;
			unsigned int m_p1, m_p2;
			float m_restDistance;
		};

	}
}


