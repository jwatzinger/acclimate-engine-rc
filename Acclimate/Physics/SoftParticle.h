#pragma once
#include <malloc.h>
#include "..\Core\Dll.h"

namespace acl
{
	namespace physics
	{
		struct ACCLIMATE_API SoftParticle
		{
#define BOUNDARY_ALIGNMENT 16

			SoftParticle(unsigned int count)
			{
				x = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				y = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				z = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				oldX = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				oldY = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				oldZ = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				accX = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				accY = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				accZ = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				invMass = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				damp = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
			}

			~SoftParticle(void)
			{
				_mm_free(x);
				_mm_free(y);
				_mm_free(z);
				_mm_free(oldX);
				_mm_free(oldY);
				_mm_free(oldZ);
				_mm_free(accX);
				_mm_free(accY);
				_mm_free(accZ);
				_mm_free(invMass);
				_mm_free(damp);
			}

			float	*x,     ///< x position array
					*y,     ///< y position array
					*z;     ///< z position array
			float   *oldX,  ///< old x velocity array
					*oldY,  ///< old y velocity array
					*oldZ;  ///< old z velocity array
			float   *accX,  ///< x acceleration array
					*accY,  ///< y acceleration array
					*accZ;  ///< z acceleration array
			float	*invMass;	///< mass array
			float	*damp;	///< damping array
		};
	}
}