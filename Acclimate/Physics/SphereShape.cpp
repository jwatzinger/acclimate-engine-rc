#include "SphereShape.h"
#include "Transform.h"
#include "..\Math\Utility.h"

namespace acl
{
	namespace physics
	{

		SphereShape::SphereShape(float radius) : CollisionShape(ShapeType::SPHERE),
			m_radius(radius)
		{
		}

		float SphereShape::ComputeVolume(void) const
		{
			return m_radius * m_radius * m_radius * math::PI * (4.0f / 3.0f);
		}

		math::Vector3 SphereShape::CalculateInertia(const float mass) const
		{
			float v = (2.0f / 5.0f) * (mass * m_radius*m_radius);
			return math::Vector3(v, v, v);
		}

		math::AABB SphereShape::ComputeAABB(const Transform& transform) const
		{
			return math::AABB(transform.vPosition, math::Vector3(m_radius, m_radius, m_radius));
		}

	}
}

