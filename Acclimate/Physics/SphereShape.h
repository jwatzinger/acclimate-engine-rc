#pragma once
#include "CollisionShape.h"

namespace acl
{
	namespace physics
	{

		class ACCLIMATE_API SphereShape :
			public CollisionShape
		{
		public:
			SphereShape(float radius);

			float ComputeVolume(void) const override;
			math::Vector3 CalculateInertia(const float mass) const override;
			math::AABB ComputeAABB(const Transform& transform) const override;

			float m_radius;

		};

	}
}


