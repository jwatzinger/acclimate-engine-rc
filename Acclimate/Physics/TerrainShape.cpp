#include "TerrainShape.h"
#include "Transform.h"
#include "../System/Assert.h"
#include <math.h> 
#include <minmax.h>

namespace acl
{
	namespace physics
	{
		TerrainShape::TerrainShape(const HeightMapVector& heightmap, const float scalingfactor, const math::Vector3& position) :
			CollisionShape(ShapeType::TERRAIN),
			m_minHeightValue(-1),
			m_maxHeightValue(-1),
			m_width(0),
			m_height(0),
			m_scalingfactor(scalingfactor),
			m_vPosition(position)
		{
			Init(heightmap);
		}

		TerrainShape::~TerrainShape()
		{
		}

		void TerrainShape::Init(const HeightMapVector& heightmap)
		{
			m_width = heightmap.size() * m_scalingfactor;
			m_height = heightmap[0].size();

			int rowcount = heightmap[0].size();
			int colcount = heightmap.size();
			int row = 0;

			for (auto& vRow : heightmap)
			{
				ACL_ASSERT(rowcount == vRow.size());
				int col = 0;

				for (auto& height : vRow)
				{
					if (row < (rowcount - 1) && col < (colcount - 1))
						m_vTrianglePairs.emplace_back(
							m_vPosition + math::Vector3(row * m_scalingfactor, height, m_scalingfactor * col),
							m_vPosition + math::Vector3((row + 1) * m_scalingfactor, heightmap[row + 1][col], m_scalingfactor * col),
							m_vPosition + math::Vector3((row + 1) * m_scalingfactor, heightmap[row + 1][col + 1], m_scalingfactor * (col + 1)),
							m_vPosition + math::Vector3(row * m_scalingfactor, heightmap[row][col + 1], m_scalingfactor * (col + 1)));

					if ((m_minHeightValue == -1) || (m_minHeightValue>height))
						m_minHeightValue = height;
					if ((m_maxHeightValue == -1) || (m_maxHeightValue<height))
						m_maxHeightValue = height;
					col++;
				}
				row++;
			}
		}

		void TerrainShape::ResetHeightMap(const HeightMapVector& heightmap, const float scalingfactor, const math::Vector3& position)
		{
			m_scalingfactor = scalingfactor;
			m_vPosition = position;
			Init(heightmap);
		}

		float TerrainShape::ComputeVolume(void) const
		{
			return 0;
		}

		math::Vector3 TerrainShape::CalculateInertia(const float mass) const
		{
			return math::Vector3(0, 0, 0);
		}

		math::AABB TerrainShape::ComputeAABB(const Transform& transform) const
		{
			math::Vector3 position = transform.vPosition;
			if (m_minHeightValue > 0)
				position.y += m_minHeightValue;
			return math::AABB(position, math::Vector3(m_width, max(m_maxHeightValue - m_minHeightValue, 0.001f), m_height));
		}

		const math::Triangle* TerrainShape::GetTriangle(float xPos, float zPos) const
		{
			const TerrainShape::TrianglePair* pair = GetTrianglePair(xPos, zPos);
			if (pair == nullptr)
				return nullptr;
			else
				return &pair->GetTriangle(xPos, zPos);
		}

		const TerrainShape::TrianglePair* TerrainShape::GetTrianglePair(float xPos, float zPos) const
		{
			//Calculate the matching TrianglePair
			const int iy = (int)trunc((xPos - m_vPosition.x) / m_scalingfactor);
			const int ix = (int)trunc((zPos - m_vPosition.y) / m_scalingfactor);

			if ((ix < 0) || (iy < 0) || (ix > m_width) || (iy > m_height))
				return nullptr;
			else 
				return &m_vTrianglePairs[ix + ((int)trunc(m_width / m_scalingfactor) - 1) *iy];
		}

		const float TerrainShape::GetScaleFactor() const
		{
			return m_scalingfactor;
		}
	}
}
