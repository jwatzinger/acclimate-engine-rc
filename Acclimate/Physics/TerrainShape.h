#pragma once
#include "CollisionShape.h"
#include <vector>
#include "../Math/Triangle.h"
#include "../System/Assert.h"

namespace acl
{
	namespace physics
	{

		class ACCLIMATE_API TerrainShape : 
			public CollisionShape
		{
		public:
			struct TrianglePair
			{
				/*---------------------------
									
					  ________________
				  P2  |             * | P1/P3
					  |  T1      *    |
					  |        *      |
					  |      *        |
					  |    *     T2   |
					  |  *            |
				P1/P3 |_______________| P2
					         

				---------------------------*/

			public:
				math::Triangle triangle2;
				math::Triangle triangle1;
			
				TrianglePair(const math::Vector3& P1, const math::Vector3& P2, const math::Vector3& P3, const math::Vector3& P4):
					triangle1(P1, P2, P3), triangle2(P3, P4, P1)
				{
				}

				const math::Triangle& GetTriangle(float xPos, float zPos) const
				{

					if (triangle1.GetP2().x - xPos > triangle2.GetP2().z - zPos)
						return triangle1;
					else 
						return triangle2;
				}

				void operator=(const TrianglePair& pair) {}
			};

			typedef std::vector<std::vector<float>> HeightMapVector;
			typedef std::vector<TrianglePair> TrianglePairVector;

			TerrainShape(const HeightMapVector& heightmap, const float scalingfactor, const math::Vector3& position);
			~TerrainShape();

			void ResetHeightMap(const HeightMapVector& heightmap, const float scalingfactor, const math::Vector3& position);
			float ComputeVolume(void) const override;
			math::Vector3 CalculateInertia(const float mass) const override;
			math::AABB ComputeAABB(const Transform& transform) const override;
			void GetVertex(int x, int y, math::Vector3& vertex) const;
			const math::Triangle* GetTriangle(float xPos, float zPos) const;
			const float GetScaleFactor() const;
		private:
#pragma warning (disable : 4251)
			math::Vector3 m_vPosition;
			float m_width;
			float m_height;

			float m_scalingfactor;
			float m_minHeightValue;
			float m_maxHeightValue;
			TrianglePairVector m_vTrianglePairs;
			
			void Init(const HeightMapVector& m_vHeightMap);
			const TrianglePair* GetTrianglePair(float xPos, float yPos) const;
		};
#pragma warning (default : 4251)
	}
}

