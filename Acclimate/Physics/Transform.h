#pragma once
#include "..\Math\Vector3.h"
#include "..\Math\Quaternion.h"

namespace acl
{
	namespace physics
	{

		struct ACCLIMATE_API Transform
		{
			Transform(const math::Vector3& vPosition, const math::Quaternion& orientation) : vPosition(vPosition), orientation(orientation)
			{
			}
			
			math::Vector3 vPosition;
			math::Quaternion orientation;
		};

	}
}