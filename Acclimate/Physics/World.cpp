#include "World.h"
#include <minmax.h>
#include "RigidBody.h"
#include "SoftBody.h"

namespace acl
{
	namespace physics
	{

		World::World(void) : m_vGravity(0.0f, -10.0f, 0.0f), m_vWind(0.0f, 0.0f, 0.0f)
		{
		}

		World::~World(void)
		{
			for(auto pBody : m_vBodies)
			{
				delete pBody;
			}

			for(auto pSoftBody : m_vSoftBodies)
			{
				delete pSoftBody;
			}
		}

		void World::SetWind(const math::Vector3& vWind)
		{
			m_vWind = vWind;
		}

		RigidBody& World::AddRigidBody(Material& material, CollisionShape& shape, const Transform& transform, MotionType motion, CollisionType collision, float damping)
		{
			auto rigidbody = new RigidBody(material, shape, transform, motion, collision, damping);
			m_vBodies.push_back(rigidbody);
			m_broadphase.AddBody(*rigidbody);
			return *rigidbody;
		}

		void World::RemoveRigidBody(RigidBody& body)
		{
			auto itr = std::find(m_vBodies.begin(), m_vBodies.end(), &body);
			if(itr != m_vBodies.end())
			{
				delete *itr;
				m_broadphase.RemoveBody(body);
				m_vBodies.erase(itr);
			}
		}

		SoftBody& World::AddSoftBody(float mass, float damping, unsigned int width, unsigned int height, const math::Vector3& vTranslation, float sizeX, float sizeY, const math::Vector3& vDir)
		{
			m_vSoftBodies.push_back(new SoftBody(mass, damping, width, height, vTranslation, sizeX, sizeY, vDir));
			return **m_vSoftBodies.rbegin();
		}

		void World::RemoveSoftBody(SoftBody& body)
		{
			auto itr = std::find(m_vSoftBodies.begin(), m_vSoftBodies.end(), &body);
			if(itr != m_vSoftBodies.end())
			{
				delete *itr;
				m_vSoftBodies.erase(itr);
			}
		}

		void World::Update(double dt)
		{	
			IntegrateForces(dt);
 			ResolveCollisions();
			IntegrateVelocities(dt);

			CorrectPositions();
		}

		void World::ResolveCollisions(void)
		{
			m_broadphase.GeneratePairs(m_vBodies);

			auto& vContacts = m_broadphase.GetPairs();

			for(unsigned int i = 0; i < 20; i++)
			{
				for(auto& manifold : vContacts)
				{
					manifold.ResolveCollision();
				}
			}
		}

		void World::CorrectPositions(void)
		{
			auto& vContacts = m_broadphase.GetPairs();

			for(auto& manifold : vContacts)
			{
				manifold.PositionCorrection();
			}
		}

		void World::IntegrateVelocities(double dt)
		{
			for(auto pBody : m_vBodies)
			{
				pBody->IntegrateVelocities(dt);
			}
		}

		void World::IntegrateForces(double dt)
		{
			for(auto pBody : m_vBodies)
			{
				IntegrateForce(*pBody, dt);
			}
			
			for(auto pSoftBody : m_vSoftBodies)
			{
				pSoftBody->AddGravity(m_vGravity*dt);
				pSoftBody->AddWind(m_vWind*dt);
				pSoftBody->Integrate(dt);
			}

			for(unsigned int i = 0; i < 10; i++)
			{
				for(auto pSoftBody : m_vSoftBodies)
				{
					pSoftBody->ResolveConstraints();
				}
			}
		}

		void World::IntegrateForce(RigidBody& body, double dt) const
		{
			body.IntegrateForce(dt, m_vGravity); 
		}

		void World::ClearForces(void) const
		{
			static const math::Vector3 vIdentity(0.0f, 0.0f, 0.0f);

			for(auto pBody : m_vBodies)
			{
				pBody->m_vForce = vIdentity;
				pBody->m_vTorque = vIdentity;
			}
		}

	}
}

