#pragma once
#include <vector>
#include "Broadphase.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace physics
	{

		class RigidBody;
		class SoftBody;

		class ACCLIMATE_API World
		{
			typedef std::vector<RigidBody*> BodyVector;
			typedef std::vector<SoftBody*> SoftBodyVector;
		public:
			World(void);
			~World(void);

			void SetWind(const math::Vector3& vWind);

			RigidBody& AddRigidBody(Material& material, CollisionShape& shape, const Transform& transform, MotionType motion, CollisionType collision, float damping);
			void RemoveRigidBody(RigidBody& body);

			SoftBody& AddSoftBody(float mass, float damping, unsigned int width, unsigned int height, const math::Vector3& vTranslation, float sizeX, float sizeY, const math::Vector3& vDir);
			void RemoveSoftBody(SoftBody& body);

			void Update(double dt);

		private:
#pragma warning( disable: 4251 )

			math::Vector3 m_vGravity, m_vWind;

			void ResolveCollisions(void);
			void IntegrateVelocities(double dt);
			void IntegrateForces(double dt);
			void IntegrateForce(RigidBody& body, double dt) const;
			void CorrectPositions(void);
			void ClearForces(void) const;

			BodyVector m_vBodies;
			SoftBodyVector m_vSoftBodies;

			Broadphase m_broadphase;
#pragma warning( default: 4251 )
		};

	}
}


