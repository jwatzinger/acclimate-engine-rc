#include "AxmLoader.h"
#include "IRenderer.h"
#include "IStage.h"
#include "..\Gfx\Color.h"
#include "..\Gfx\Screen.h"
#include "..\Math\Rect.h"
#include "..\XML\Doc.h"
#include "..\System\Log.h"

namespace acl
{
    namespace render
    {

		AxmLoader::AxmLoader(IRenderer& renderer, const gfx::Screen& screen, const gfx::Textures& textures, const gfx::ZBuffers& zBuffers) : m_pRenderer(&renderer),
			m_pTextures(&textures), m_pZBuffers(&zBuffers), m_pScreen(&screen)
        {
        }

        void AxmLoader::Load(const std::wstring& stFilename) const
        {
            xml::Doc doc;
            doc.LoadFile( stFilename.c_str() );

            if( const xml::Node* pRenderer = doc.Root(L"Renderer") )
            {
				LoadGroups(*pRenderer);

				if(auto pStages = pRenderer->Nodes(L"Stage"))
				{
					for(auto pStage : *pStages)
					{
						const std::wstring& stGroup = pStage->Attribute(L"group")->GetValue();

						size_t queueId = 0;
						if(auto pQueue = pStage->Attribute(L"queue"))
							queueId = pQueue->AsInt();
						else
							queueId = m_pRenderer->AddQueue(stGroup);

						CreateStage(*pStage, stGroup, queueId);
					}
				}
            }
        }

		void AxmLoader::LoadGroups(const xml::Node& rendererNode) const
		{
			if( const xml::Node::NodeVector* pGroups = rendererNode.Nodes(L"Group") )
            {
                for( auto& pGroup : *pGroups )
                {
                    const std::wstring& stGroup = pGroup->Attribute(L"name")->GetValue();

					if(!m_pRenderer->HasGroup(stGroup))
					{
						if(auto pBefore = pGroup->Attribute(L"before"))
							m_pRenderer->CreateGroupBefore(stGroup, *pBefore);
						else if(auto pAfter = pGroup->Attribute(L"after"))
							m_pRenderer->CreateGroupAfter(stGroup, *pAfter);
						else
							m_pRenderer->CreateGroup(stGroup);
					}

                    if( const xml::Node::NodeVector* pQueueVector = pGroup->Nodes(L"Queue") )
                    {
                        for( const xml::Node* pQueue : *pQueueVector)
                        {
                            size_t queueId = m_pRenderer->AddQueue( stGroup );
                            
                            if( const xml::Node::NodeVector* pStages = pQueue->Nodes(L"Stage") )
                            {
                                for( const xml::Node* pStage : *pStages)
                                {
									CreateStage(*pStage, stGroup, queueId);
                                }
                            }

                            queueId++;
                        }
                    }
                }
            }
		}

		void AxmLoader::CreateStage(const xml::Node& stageNode, const std::wstring& stGroup, size_t queueId) const
		{
			const std::wstring stName = stageNode.Attribute(L"name")->GetValue();

			// load flags
			unsigned int stageFlags = 0;
			if(stageNode.Attribute(L"clear")->AsBool())
				stageFlags |= CLEAR;
			if(auto pMip = stageNode.Attribute(L"mip"))
			{
				if(pMip->AsBool())
					stageFlags |= GENERATE_MIPS;
			}
			if(stageNode.FirstNode(L"AllowEmptyExecute"))
				stageFlags |= ALLOW_EXECUTE_EMPTY;


			// load zbuffer
			std::wstring stZBufferName = L"";
			if( const xml::Node* pZBuffer = stageNode.FirstNode(L"ZBuffer") )
			{
				stZBufferName = pZBuffer->Attribute(L"name")->GetValue();

				if(pZBuffer->Attribute(L"clear")->AsBool())
					stageFlags |= CLEAR_Z;
			}

			auto* pStage = m_pRenderer->AddStage(stName, stGroup, queueId, stageFlags);

			if(auto pClearColor = stageNode.FirstNode(L"ClearColor"))
			{
				pStage->SetClearColor(gfx::FColor(pClearColor->Attribute(L"r")->AsFloat(),
					pClearColor->Attribute(L"g")->AsFloat(),
					pClearColor->Attribute(L"b")->AsFloat(),
					pClearColor->Attribute(L"a")->AsFloat()));
			}
            
			// load render targets
            size_t id = 0;
            if( const xml::Node::NodeVector* pTargets = stageNode.Nodes(L"Target") )
            {
                for( const xml::Node* pTarget : *pTargets )
                {
                    const gfx::ITexture* pTexture = nullptr;
                    const std::wstring& stTextureName = pTarget->Attribute(L"name")->GetValue();
                    if( !stTextureName.empty() )
					{
                        pTexture = m_pTextures->Get( stTextureName );
						if(!pTexture)
							sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "set missing texture", stTextureName, "as target", id, "to stage", stName);
					}

                    pStage->SetRenderTarget( id, pTexture );  

                    id++;
                }
            }

			// set clear color
			if(auto pClearColor = stageNode.FirstNode(L"ClearColor"))
			{
				gfx::FColor color(pClearColor->Attribute(L"r")->AsFloat(), pClearColor->Attribute(L"g")->AsFloat(), pClearColor->Attribute(L"b")->AsFloat(), pClearColor->Attribute(L"a")->AsFloat());
				pStage->SetClearColor(color);
			}

			// set zbuffer
			if(!stZBufferName.empty())
			{
				gfx::IZBuffer* pZbuffer = m_pZBuffers->Get(stZBufferName);
				if(pZbuffer)
					pStage->SetDepthBuffer(pZbuffer);
				else
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Tried to assign missing Z-Buffer", stZBufferName, "to stage", stName);
			}

			// viewport
			math::Rect rViewport = m_pScreen->GetRect();

			if(auto pView = stageNode.FirstNode(L"Viewport"))
			{
				const int mode = pView->Attribute(L"mode")->AsInt();
				switch(mode)
				{
				case 1:
					rViewport.width = 0;
					rViewport.height = 0;
					break;
				case 2:
					pStage->SetViewportFromTarget();
					return;
				}
			}

			pStage->SetViewport(rViewport);
		}

    }
}