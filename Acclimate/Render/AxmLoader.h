#pragma once
#include "ILoader.h"

#include "..\Gfx\Textures.h"
#include "..\Gfx\ZBuffers.h"

namespace acl
{
	namespace gfx
	{
		class Screen;
	}

	namespace xml
	{
		class Node;
	}

    namespace render
    {
        class IRenderer;

        class AxmLoader :
            public ILoader
        {
        public:
			AxmLoader(IRenderer& renderer, const gfx::Screen& screen, const gfx::Textures& textures, const gfx::ZBuffers& zBuffers);

            void Load(const std::wstring& stFilename) const;

        private:

			void LoadGroups(const xml::Node& rendererNode) const;
			void CreateStage(const xml::Node& stageNode, const std::wstring& stGroup, size_t queueId) const;

            IRenderer* m_pRenderer;
			const gfx::Screen* m_pScreen;
            const gfx::Textures* m_pTextures;
			const gfx::ZBuffers* m_pZBuffers;
        };

    }
}

