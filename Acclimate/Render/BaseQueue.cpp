#include "BaseQueue.h"

#include <algorithm>
#include "StateGroup.h"

namespace acl
{
	namespace render
	{

		BaseQueue::BaseQueue(void)
		{
			m_vTempInstances.reserve(1024); // todo: undo
		}

		size_t BaseQueue::GetNumSubmitted(void) const
		{
			return m_vInstances.size();
		}

		void BaseQueue::SubmitInstance(const Instance& instance)
		{
			m_vInstances.push_back({ &instance, instance.sortKey });
		}

		void BaseQueue::SubmitTempInstance(Instance&& instance)
		{
			m_vTempInstances.emplace_back(std::move(instance));

			auto pInstance = &m_vTempInstances[m_vTempInstances.size() - 1];
			m_vInstances.push_back({ pInstance, pInstance->sortKey });
		}

		void BaseQueue::Sort(void)
		{
			//sort instance vector
			std::stable_sort(m_vInstances.begin(), m_vInstances.end(), QueueSorter());

			//iteratre through render instances
			OnExecuteSorted(m_vInstances);

			//clear instance list
			m_vInstances.clear();
			m_vTempInstances.clear();
		}

	}
}

#undef EXECUTE_GUARDED