#pragma once
#include "IQueue.h"
#include <vector>
#include "Instance.h"

namespace acl
{
	namespace render
	{

		class BaseQueue :
			public IQueue
		{
			typedef std::vector<const Instance> TempInstanceVector;
		public:

			BaseQueue(void);

			size_t GetNumSubmitted(void) const override final;

			void SubmitInstance(const Instance& instance) override final;
			void SubmitTempInstance(Instance&& instance) override final;

			void Sort(void) override final;
		
		protected:

			typedef std::pair<const Instance*, Key64> SubmittedInstance;
			typedef std::vector<SubmittedInstance> InstanceVector;

			virtual void OnExecuteSorted(const InstanceVector& vInstances) const = 0;

		private:

			InstanceVector m_vInstances;
			TempInstanceVector m_vTempInstances;

			struct QueueSorter
			{
				inline bool operator()(const SubmittedInstance& left, const SubmittedInstance& right)
				{
					return left.second.bits > right.second.bits;
				}
			};
		};

	}
}