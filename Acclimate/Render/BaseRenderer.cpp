#include "BaseRenderer.h"
#include "IQueue.h"
#include "IStage.h"
#include "ImmediateRenderer.h"
#include "..\Math\Rect.h"
#include "..\Math\Vector.h"
#include "..\System\Log.h"

namespace acl
{
    namespace render
    {

		BaseRenderer::~BaseRenderer(void)
        {
	        for(auto& group : m_mQueues)
            {
                for(auto& queue : group.second)
                {
		            delete queue;
                }
            }

            for(auto& stage : m_mStages)
            {
                delete stage.second;
            }
        }

		void BaseRenderer::CreateGroup(const std::wstring& stGroup)
        {
			if(!HasGroup(stGroup))
				m_vGroups.push_back(stGroup);
			else
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create group", stGroup, ". Group already exists.");
		}

		void BaseRenderer::CreateGroupBefore(const std::wstring& stGroup, const std::wstring& stBeforeGroup)
		{
			if(!HasGroup(stGroup))
			{
				auto itr = std::find(m_vGroups.begin(), m_vGroups.end(), stBeforeGroup);
				if(itr != m_vGroups.end())
					m_vGroups.insert(itr, stGroup);
				else
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create group", stGroup, ". Group should be inserted before group", stBeforeGroup, ", which does not exist.");
			}
			else
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create group", stGroup, ". Group already exists.");
		}

		void BaseRenderer::CreateGroupAfter(const std::wstring& stGroup, const std::wstring& stAfterGroup)
		{
			if(!HasGroup(stGroup))
			{
				auto itr = std::find(m_vGroups.begin(), m_vGroups.end(), stAfterGroup);
				if(itr != m_vGroups.end())
					m_vGroups.insert(itr+1, stGroup);
				else
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create group", stGroup, ". Group should be inserted after group", stAfterGroup, ", which does not exist.");
			}
			else
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create group", stGroup, ". Group already exists.");
		}

		ImmediateRenderer& BaseRenderer::CreateImmediate(void)
		{
			auto& queue = OnCreateQueue();
			auto& stage = OnCreateStage(0, queue, 0);
			return *new ImmediateRenderer(stage, queue);
		}

		void BaseRenderer::RemoveGroup(const std::wstring& stGroup)
		{
			auto itr = std::find(m_vGroups.begin(), m_vGroups.end(), stGroup);
			if(itr != m_vGroups.end())
			{
				ClearGroup(stGroup);
				m_vGroups.erase(itr);
			}
		}

		void BaseRenderer::ClearGroup(const std::wstring& stGroup)
		{
			auto itr = m_mQueues.find(stGroup);
			if(itr != m_mQueues.end())
			{
				for(auto queue : itr->second)
				{
					delete queue;
				}

				auto stageItr = m_mStageGroup.find(stGroup);
				if(stageItr != m_mStageGroup.end())
				{
					for(auto stage : stageItr->second)
					{
						delete m_mStages[stage];
						m_mStages.erase(stage);
					}
					itr->second.clear();
				}
			}
		}

		size_t BaseRenderer::AddQueue(const std::wstring& stGroup)
        {
			if(!HasGroup(stGroup))
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to create queue in group", stGroup, ". Group doesn't exist.");
				return INVALID_QUEUE;
			}

			IQueue* pQueue = &OnCreateQueue();
		    m_mQueues[stGroup].push_back(pQueue);
            m_mStageCounts[stGroup].push_back(0);

            return m_mQueues[stGroup].size()-1;
        }

		IStage* BaseRenderer::AddStage(const std::wstring& stName, const std::wstring& stGroup, unsigned int queueId, unsigned int flags)
        {
            if( m_mQueues.count(stGroup) == 0 || m_mQueues[stGroup].size() <= queueId)
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "tried to add stage", stName, "to non existing queue", queueId, "in group", stGroup);
                return nullptr;
			}

            if( m_mStages.count(stName) == 0 )
            {
                IStage* pStage = &OnCreateStage(m_mStageCounts[stGroup][queueId]++, *m_mQueues[stGroup][queueId], flags);
                m_mStages[stName] = pStage;
				m_mStageGroup[stGroup].push_back(stName);
				return pStage;
            }
			else
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "tried to add another stage with already taken name", stName, "to queue", queueId, "in group", stGroup);
				return nullptr;
			}
        }

		void BaseRenderer::RemoveStage(const std::wstring& stName, bool bRemoveQueueIfEmpty)
		{
			auto itr = m_mStages.find(stName);
			// check if stage exists
			if(itr != m_mStages.end())
			{
				// check for groups
				for(auto& group : m_mStageGroup)
				{
					auto grpItr = std::find(group.second.begin(), group.second.end(), itr->first);
					if(grpItr != group.second.end())
					{
						auto pQueue = itr->second->GetQueue();

						// get queue id
						for(auto& vQueues : m_mQueues)
						{
							auto queueItr = std::find(vQueues.second.begin(), vQueues.second.end(), pQueue);
							if(queueItr != vQueues.second.end())
							{
								size_t queueId = queueItr - vQueues.second.begin();

								m_mStageCounts[group.first][queueId]--;

								if(bRemoveQueueIfEmpty && !m_mStageCounts[group.first][queueId])
								{
									m_mStageCounts[group.first].erase(m_mStageCounts[group.first].begin()+queueId);
									m_mQueues[group.first].erase(m_mQueues[group.first].begin()+queueId);
								}

								break;
							}
						}

						group.second.erase(grpItr);

						delete itr->second;
						m_mStages.erase(itr);

						break;
					}
				}
			}
		}

		void BaseRenderer::RemoveStage(const IStage& stage, bool bRemoveQueueIfEmpty)
		{
			for(auto& element : m_mStages)
			{
				if(element.second == &stage)
				{
					RemoveStage(element.first, bRemoveQueueIfEmpty);
					return;
				}
			}
		}

        IStage* BaseRenderer::GetStage(const std::wstring& stName) const
        {
			if(m_mStages.find(stName) != m_mStages.end())
				return m_mStages.at(stName);
			else
				return nullptr;
        }

		bool BaseRenderer::HasGroup(const std::wstring& stName) const
		{
			return std::find(m_vGroups.begin(), m_vGroups.end(), stName) != m_vGroups.end();
		}

		void BaseRenderer::Execute(void)
		{
			for(auto& stage : m_mStages)
			{
				stage.second->Submit();
			}

			for(auto& groups : m_vGroups)
			{
				for(auto& queue : m_mQueues[groups])
				{
					queue->Sort();
				}
			}

			OnExecute();
		}

		void BaseRenderer::OnResizeScreen(const math::Vector2& vOldScreenSize, const math::Vector2& vNewScreenSize)
		{
			for(auto& stage : m_mStages)
			{
				if(stage.second->IsViewportFromTarget())
					stage.second->SetViewportFromTarget();
				else
				{
					auto& viewport = stage.second->GetViewport();
					if(viewport.width == vOldScreenSize.x && viewport.height == vOldScreenSize.y)
						stage.second->SetViewport(math::Rect(0, 0, vNewScreenSize.x, vNewScreenSize.y));
				}
			}
		}
        
    }
}