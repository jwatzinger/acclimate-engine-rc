#pragma once
#include "IRenderer.h"
#include <map>
#include <string>
#include <vector>

namespace acl
{
	namespace render
	{
		class IQueue;

		class BaseRenderer :
			public IRenderer
		{
			typedef std::map<std::wstring, std::vector<IQueue*>> QueueMap;
            typedef std::vector<std::wstring> GroupVector;
            typedef std::map<std::wstring, IStage*> StageMap;
            typedef std::map<std::wstring, std::vector<int>> StageCounts;
			typedef std::map<std::wstring, std::vector<std::wstring>> StagesByGroup;

		public:
			~BaseRenderer(void);

            void CreateGroup(const std::wstring& stGroup) override final;
			void CreateGroupBefore(const std::wstring& stGroup, const std::wstring& stBeforeGroup) override final;
			void CreateGroupAfter(const std::wstring& stGroup, const std::wstring& stAfterGroup) override final;
			ImmediateRenderer& CreateImmediate(void) override final;
			void ClearGroup(const std::wstring& stGroup) override final;
			void RemoveGroup(const std::wstring& stGroup) override final;
			size_t AddQueue(const std::wstring& stGroup) override final;
			IStage* AddStage(const std::wstring& stName, const std::wstring& stGroup, unsigned int queueId, unsigned int flags) override final;
			void RemoveStage(const std::wstring& stName, bool bRemoveQueueIfEmpty) override final;
			void RemoveStage(const IStage& stage, bool bRemoveQueueIfEmpty) override final;

			IStage* GetStage(const std::wstring& stName) const override final;
			bool HasGroup(const std::wstring& stName) const override final;

			void Execute(void) override final;

			void OnResizeScreen(const math::Vector2& vOldScreenSize, const math::Vector2& vNewScreenSize) override final;

		protected:

			virtual IStage& OnCreateStage(unsigned int id, IQueue& queue, unsigned int flags) const = 0;
			virtual IQueue& OnCreateQueue(void) const = 0;

			virtual void OnExecute(void) = 0;

		private:

			QueueMap m_mQueues;
            GroupVector m_vGroups;
            StageMap m_mStages;
            StageCounts m_mStageCounts;
			StagesByGroup m_mStageGroup;

		};

	}
}