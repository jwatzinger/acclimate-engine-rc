#include "BaseStage.h"
#include <algorithm>
#include "IQueue.h"
#include "Instance.h"
#include "States.h"
#include "..\Math\Vector.h"
#include "..\Gfx\ITexture.h"
#include "..\Gfx\ZbufferInternalAccessor.h"
#include "..\Gfx\TextureInternalAccessor.h"
#include "..\Gfx\ICbufferLoader.h"
#include "..\Gfx\ICbuffer.h"
#include "..\System\Log.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace render
    {

#pragma warning (disable: 4351)
		BaseStage::BaseStage(unsigned int id, IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader) : m_pQueue(&queue),
			m_id(id), m_flags(flags), m_clearColor(-0.01f, -0.01f, -0.01f, 1.0f), m_pDraw(nullptr),
			m_pDepthBuffer(nullptr), m_pTextures(), m_isVCbound(false), m_isPCbound(false), m_isViewportFromTarget(false)
#ifndef ACL_API_DX9
			, m_isGCbound(false)
#endif
        {
			m_pVCbuffer = &loader.Create(16);
			m_pPCbuffer = &loader.Create(16);
#ifndef ACL_API_DX9
			m_pGCbuffer = &loader.Create(16);
#endif
        }
#pragma warning (default: 4351)

		BaseStage::~BaseStage(void)
        {
			delete m_pVCbuffer;
			delete m_pPCbuffer;
#ifndef ACL_API_DX9
			delete m_pGCbuffer;
#endif
	        delete m_pDraw;
        }

        void BaseStage::SubmitInstance(Instance& instance) const
        {
            instance.sortKey.Stage = 7 - m_id;
	        m_pQueue->SubmitInstance(instance);
        }

		void BaseStage::SubmitTempInstance(Instance&& instance) const
		{
			instance.sortKey.Stage = 7 - m_id;
			m_pQueue->SubmitTempInstance(std::move(instance));
		}

		void BaseStage::SetVertexConstant(unsigned int index, const float* constArr, unsigned int numConsts)
		{
			m_pVCbuffer->Write(index, constArr, numConsts);

			if(!m_isVCbound)
			{
				m_pVCbuffer->Bind(m_stageStates, gfx::CbufferType::VERTEX, gfx::CbufferSlot::STAGE);
				m_isVCbound = true;
			}
		}

		void BaseStage::SetPixelConstant(unsigned int index, const float* constArr, unsigned int numConsts)
		{
			m_pPCbuffer->Write(index, constArr, numConsts);

			if(!m_isPCbound)
			{
				m_pPCbuffer->Bind(m_stageStates, gfx::CbufferType::PIXEL, gfx::CbufferSlot::STAGE);
				m_isPCbound = true;
			}
		}

		void BaseStage::SetGeometryConstant(unsigned int index, const float* constArr, unsigned int numConsts)
		{
#ifndef ACL_API_DX9
			m_pGCbuffer->Write(index, constArr, numConsts);

			if(!m_isGCbound)
			{
				m_pGCbuffer->Bind(m_stageStates, gfx::CbufferType::GEOMETRY, gfx::CbufferSlot::STAGE);
				m_isGCbound = true;
			}
#else
			sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Failed to set geometry constant. Geometry shader is not supported in DX9-mode, please check and modify your implementation.");
#endif
		}

        void BaseStage::SetRenderTarget(unsigned int index, const gfx::ITexture* pTexture)
        {
			if(index >= MAX_RENDERTARGETS)
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "tried to set render target for stage at invalid slot", index, "(ony slots up to ", MAX_RENDERTARGETS, "supported)");
				return;
			}

			if(m_pTextures[index] != pTexture)
			{
				m_pTextures[index] = pTexture;

				OnChangeRenderTarget(index, gfx::getTexture(pTexture));
			}
        }

        void BaseStage::SetDepthBuffer(const gfx::IZBuffer* pZBuffer)
        {
			if(pZBuffer != m_pDepthBuffer)
			{
				m_pDepthBuffer = pZBuffer;
				OnChangeDepthBuffer(gfx::getDepthBuffer(pZBuffer));
			}
        }

		void BaseStage::SetViewportFromTarget(void)
		{
			if(auto pTexture = m_pTextures[0])
			{
				const math::Vector2& vSize = pTexture->GetSize();
				SetViewport(math::Rect(0, 0, vSize.x, vSize.y));
				m_isViewportFromTarget = true;
			}
			else
				SetViewport(math::Rect(0, 0, 0, 0));
		}

		void BaseStage::SetClearColor(const gfx::FColor& color)
		{
			if(color != m_clearColor)
			{
				m_clearColor = color;
				OnChangeClearColor(color);
			}
		}

		void BaseStage::SetViewport(const math::Rect& rViewport)
		{
			if(m_rViewport != rViewport)
			{
				m_rViewport = rViewport;
				m_isViewportFromTarget = false;
				OnChangeViewport(rViewport);
			}
		}

		const IQueue* BaseStage::GetQueue(void) const
		{
			return m_pQueue;
		}

		const math::Rect& BaseStage::GetViewport(void) const
		{
			return m_rViewport;
		}

		bool BaseStage::IsViewportFromTarget(void) const
		{
			return m_isViewportFromTarget;
		}

		void BaseStage::Submit(void) const
		{
			if(!(m_flags & ALLOW_EXECUTE_EMPTY) && !m_pQueue->GetNumSubmitted())
				return;

			Key64 key;
			key.bits = 0xffffffffffffffff;
			key.Stage = 7 - m_id;

			const StateGroup* groups[1] = { &m_stageStates };
			Instance stageInstance(key, groups, 1);
			if(m_pDraw)
				stageInstance.SetCall(*m_pDraw);

			m_pQueue->SubmitTempInstance(std::move(stageInstance));

			// mipmap generation

			if(m_flags & GENERATE_MIPS)
			{
				Key64 mipKey;
				mipKey.bits = 0x0000000000000000;
				mipKey.Stage = 7 - m_id;

				Instance mipInstance(key, nullptr, 0);
				if(OnGenerateMips(mipInstance))
				{
					m_pQueue->SubmitTempInstance(std::move(mipInstance));
				}
			}
		}

		void BaseStage::SetDrawCall(BaseDrawCall& call)
		{
			delete m_pDraw;
			m_pDraw = &call;
		}

		std::vector<const AclTexture*> BaseStage::GetTextures(void) const
		{
			std::vector<const AclTexture*> vTextures;
			for(unsigned int i = 0; i < MAX_RENDERTARGETS; i++)
			{
				vTextures.push_back(gfx::getTexture(m_pTextures[i]));
			}
			return vTextures;
		}

		const AclDepth* BaseStage::GetDepthbuffer(void) const
		{
			return gfx::getDepthBuffer(m_pDepthBuffer);
		}

		unsigned int BaseStage::GetFlags(void) const
		{
			return m_flags;
		}

		const gfx::FColor& BaseStage::GetClearColor(void) const
		{
			return m_clearColor;
		}

    }
}