#pragma once
#include <vector>
#include "IStage.h"
#include "StateGroup.h"
#include "Defines.h"
#include "..\ApiDef.h"
#include "..\Gfx\Color.h"
#include "..\Math\Rect.h"

namespace acl
{
	namespace gfx
	{
		class ICbuffer;
		class ICbufferLoader;
	}

	namespace render
	{

		class BaseDrawCall;

		class BaseStage :
			public IStage
		{
		public:
			BaseStage(unsigned int id, IQueue& queue, unsigned int flags, const gfx::ICbufferLoader& loader);
			~BaseStage(void);

			void SetVertexConstant(unsigned int index, const float* constArr, unsigned int numConsts) override final;
			void SetPixelConstant(unsigned int index, const float* constArr, unsigned int numConsts) override final;
			void SetGeometryConstant(unsigned int index, const float* constArr, unsigned int numConsts) override final;
			void SetRenderTarget(unsigned int index, const gfx::ITexture* pTexture) override final;
			void SetDepthBuffer(const gfx::IZBuffer* pZBuffer) override final;
			void SetViewportFromTarget(void) override final;
			void SetClearColor(const gfx::FColor& color) override final;
			void SetViewport(const math::Rect& rViewport) override final;

			const IQueue* GetQueue(void) const override final;
			const math::Rect& GetViewport(void) const override final;
			bool IsViewportFromTarget(void) const override final;

			void Submit(void) const override final;
			void SubmitInstance(Instance& instance) const override final;
			void SubmitTempInstance(Instance&& instance) const override final;

		protected:

			void SetDrawCall(BaseDrawCall& call);

			std::vector<const AclTexture*> GetTextures(void) const;
			const AclDepth* GetDepthbuffer(void) const;

			unsigned int GetFlags(void) const;
			const gfx::FColor& GetClearColor(void) const;

			StateGroup m_stageStates;

		private:

			virtual void OnChangeDepthBuffer(const AclDepth* pDepth) = 0;
			virtual void OnChangeRenderTarget(unsigned int slot, const AclTexture* pTexture) = 0;
			virtual void OnChangeClearColor(const gfx::FColor& color) = 0;
			virtual bool OnGenerateMips(Instance& instance) const = 0;
			virtual void OnChangeViewport(const math::Rect& rViewport) = 0;
	
			BaseStage(const BaseStage& stage) {};

			unsigned int m_id, m_flags;

			const gfx::ITexture* m_pTextures[MAX_RENDERTARGETS];
			const gfx::IZBuffer* m_pDepthBuffer;
			gfx::FColor m_clearColor;
			math::Rect m_rViewport;
			bool m_isViewportFromTarget;

			BaseDrawCall* m_pDraw;
			IQueue* m_pQueue;

			gfx::ICbuffer* m_pVCbuffer, *m_pPCbuffer;
			bool m_isVCbound, m_isPCbound;
#ifndef ACL_API_DX9
			gfx::ICbuffer* m_pGCbuffer;
			bool m_isGCbound;
#endif

		};

	}
}