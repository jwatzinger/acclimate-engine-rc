#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace render
	{
		
		class IRenderer;
		class ILoader;
		class Postprocessor;

		struct ACCLIMATE_API Context
		{
			Context(IRenderer& renderer, ILoader &loader, Postprocessor& postprocess): renderer(renderer), loader(loader),
				postprocess(postprocess)
			{
			}

			IRenderer& renderer;
			ILoader& loader;
			Postprocessor& postprocess;
		};

	}
}