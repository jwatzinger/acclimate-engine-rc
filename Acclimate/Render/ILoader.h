#pragma once
#include <string>

namespace acl
{
    namespace render
    {

        class ILoader
        {
        public:

			virtual ~ILoader(void) {};

            virtual void Load(const std::wstring& stFilename) const = 0;

        };

    }
}