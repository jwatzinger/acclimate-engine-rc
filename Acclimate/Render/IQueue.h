#pragma once
#include "Instance.h"

namespace acl
{
	namespace render
	{
		class IQueue
		{
		public:

			virtual ~IQueue(void) {};

			virtual size_t GetNumSubmitted(void) const = 0;

			virtual void SubmitInstance(const Instance& instance) = 0;
			virtual void SubmitTempInstance(Instance&& instance) = 0;
			
			virtual void Sort(void) = 0;
		};

	}

}