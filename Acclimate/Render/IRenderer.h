#pragma once
#include <string>

namespace acl
{
	namespace math
	{
		struct Vector2;
	}

	namespace render
	{

		class IStage;
		class RendererInternalAccessor;
		class ImmediateRenderer;

		static const size_t INVALID_QUEUE = -1;

		class IRenderer
		{
		public:

			virtual ~IRenderer(void) {};

			virtual void GetDevice(RendererInternalAccessor& accessor) const = 0;

            virtual void CreateGroup(const std::wstring& stGroup) = 0;
			virtual void CreateGroupBefore(const std::wstring& stGroup, const std::wstring& stBeforeGroup) = 0;
			virtual void CreateGroupAfter(const std::wstring& stGroup, const std::wstring& stBeforeGroup) = 0;
			virtual ImmediateRenderer& CreateImmediate(void) = 0;
			virtual void ClearGroup(const std::wstring& stGroup) = 0;
			virtual void RemoveGroup(const std::wstring& stGroup) = 0;
            virtual size_t AddQueue(const std::wstring& stGroup) = 0;
            virtual IStage* AddStage(const std::wstring& stName, const std::wstring& stGroup, unsigned int queueId, unsigned int StageFlags) = 0;
			virtual void RemoveStage(const std::wstring& stName, bool bRemoveQueueIfEmpty) = 0;
			virtual void RemoveStage(const IStage& stage, bool bRemoveQueueIfEmpty) = 0;
            
            virtual IStage* GetStage(const std::wstring& stName) const = 0;
			virtual bool HasGroup(const std::wstring& stName) const = 0;

            virtual void Prepare(void) = 0;
            virtual void Execute(void) = 0;
			virtual void Finish(void) = 0;

			virtual void OnResizeScreen(const math::Vector2& vOldScreenSize, const math::Vector2& vNewScreenSize) = 0;

		};
	}
}