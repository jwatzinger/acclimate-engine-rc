#pragma once

namespace acl
{
	namespace gfx
	{
		class ITexture;
        class IZBuffer;
		struct FColor;
	}

	namespace math
	{
		struct Rect;
	}

	namespace render
	{

		enum StageFlags
		{
			CLEAR = 1,
			CLEAR_Z = 2,
			GENERATE_MIPS = 4,
			ALLOW_EXECUTE_EMPTY = 8,
		};

		struct Instance;
		class IQueue;

		class IStage
		{
		public:

			virtual ~IStage(void) = 0 {};

			virtual void SetRenderTarget(unsigned int index, const gfx::ITexture* pTexture) = 0;
            virtual void SetDepthBuffer(const gfx::IZBuffer* pZBuffer) = 0;
			virtual void SetVertexConstant(unsigned int index, const float* constArr, unsigned int numConsts) = 0;
			virtual void SetPixelConstant(unsigned int index, const float* constArr, unsigned int numConsts) = 0;
			virtual void SetGeometryConstant(unsigned int index, const float* constArr, unsigned int numConsts) = 0;
			virtual void SetViewport(const math::Rect& rect) = 0;
			virtual void SetViewportFromTarget(void) = 0;
			virtual const math::Rect& GetViewport(void) const = 0;
			virtual bool IsViewportFromTarget(void) const = 0;
			virtual void SetClearColor(const gfx::FColor& color) = 0;

			virtual const IQueue* GetQueue(void) const = 0;

			virtual void Submit(void) const = 0;
			virtual void SubmitInstance(Instance& instance) const = 0;
			virtual void SubmitTempInstance(Instance&& instance) const = 0;
		};

	}
}

