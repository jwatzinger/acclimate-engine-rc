#include "ImmediateRenderer.h"
#include "IStage.h"
#include "IQueue.h"

namespace acl
{
	namespace render
	{

		ImmediateRenderer::ImmediateRenderer(IStage& stage, IQueue& queue):
			m_pStage(&stage), m_pQueue(&queue)
		{
		}

		ImmediateRenderer::~ImmediateRenderer(void)
		{
			delete m_pStage;
			delete m_pQueue;
		}

		IStage& ImmediateRenderer::GetStage(void)
		{
			return *m_pStage;
		}

		void ImmediateRenderer::Execute(void)
		{
			m_pStage->Submit();
			m_pQueue->Sort();
		}

	}
}