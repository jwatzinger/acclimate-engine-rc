#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace render
	{

		class IStage;
		class IQueue;

		class ACCLIMATE_API ImmediateRenderer
		{
		public:
			ImmediateRenderer(IStage& stage, IQueue& queue);
			~ImmediateRenderer(void);

			IStage& GetStage(void);

			void Execute(void);

		private:

			IStage* m_pStage;
			IQueue* m_pQueue;
		};

	}
}

