#include "Instance.h"
#include "States.h"
#include "StateGroup.h"
#include "..\System\Assert.h"

namespace acl
{
    namespace render
    {

        Instance::Instance(Key64 sortKey, const StateGroup** ppGroup, size_t numStates): pStateGroups(nullptr), cStateGroups(numStates), sortKey(sortKey), pCall(nullptr)
        {
			if(!ppGroup)
				return;

			size_t streamSize = 0;
			for(unsigned int i = 0; i < numStates; i++)
			{
				if(auto count = ppGroup[i]->GetCommandCount())
					streamSize += count*STATE_SIZE+sizeof(unsigned int);
				else
					cStateGroups -= 1;
			}
            pStateGroups = new char[streamSize];

			unsigned int offset = 0;
			for(unsigned int i = 0; i < numStates; i++)
			{
				if(auto count = ppGroup[i]->GetCommandCount())
				{
					char* pStateGroup = reinterpret_cast<char*>(pStateGroups)+offset;
					*reinterpret_cast<unsigned int*>(pStateGroup) = ppGroup[i]->GetCommandCount();
					const size_t size = count*STATE_SIZE;
					memcpy(pStateGroup+sizeof(unsigned int), ppGroup[i]->GetCommands(), size);
					offset += size+sizeof(unsigned int);
				}
			}
        }

        Instance::Instance(const Instance& instance): cStateGroups(instance.cStateGroups), sortKey(instance.sortKey),
			pStateGroups(nullptr)
        {
			if(instance.pCall)
				pCall = &instance.pCall->Clone();

			ACL_ASSERT(false); // unimplemented
        }

		Instance::Instance(Instance&& instance) : cStateGroups(instance.cStateGroups), sortKey(instance.sortKey), pCall(instance.pCall),
			pStateGroups(instance.pStateGroups)
		{
			instance.cStateGroups = 0;
			instance.pStateGroups = nullptr;
			instance.pCall = nullptr;
		}

		Instance::~Instance(void)
		{
			delete[] pStateGroups;

			delete pCall;
		}

		void Instance::operator=(const Instance& instance)
		{
			cStateGroups = instance.cStateGroups;
			sortKey = instance.sortKey;
			if(instance.pCall)
				pCall = &instance.pCall->Clone();

			ACL_ASSERT(false); // unimplemented
		}

		void Instance::operator=(Instance&& instance)
		{
			if(&instance != this)
			{
				cStateGroups = instance.cStateGroups;
				sortKey = instance.sortKey;
				pStateGroups = instance.pStateGroups;
				pCall = instance.pCall;

				instance.pCall = nullptr;
				instance.pStateGroups = nullptr;
				instance.cStateGroups = 0;
			}
		}

        const void* Instance::GetStateStream(void) const
        {
	        return pStateGroups;
        }

        void Instance::SetCall(const BaseDrawCall& call)
        {
			delete pCall;
	        pCall = &call.Clone();
        }

        const BaseDrawCall* Instance::GetDrawCall(void) const
        {
	        return pCall;
        }

    }
}