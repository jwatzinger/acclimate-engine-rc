#pragma once
#include "Key.h"

namespace acl
{
	namespace render
	{

		class StateGroup;
		class BaseDrawCall;

		/*******************************************
		* Render instance
		********************************************/
		struct Instance
		{
			Instance(Key64 sortKey, const StateGroup** ppGroup, size_t numStates);
            Instance(const Instance& instance);
			Instance(Instance&& instance);
			~Instance(void);

			void operator=(const Instance& instance);
			void operator=(Instance&& instance);

			template<typename Call, typename... Args>
			void CreateCall(Args&&... args);

			void SetCall(const BaseDrawCall& call);

			inline unsigned int GetStateCount(void) const
			{
				return cStateGroups;
			}

			const void* GetStateStream(void) const;
			const BaseDrawCall* GetDrawCall(void) const;

			Key64 sortKey;

		private:

			const BaseDrawCall* pCall;
			void* pStateGroups;
			unsigned int cStateGroups;
		};

		template<typename Call, typename... Args>
		void Instance::CreateCall(Args&&... args)
		{
			delete pCall;
			pCall = new Call(args...);
		}

	}
}