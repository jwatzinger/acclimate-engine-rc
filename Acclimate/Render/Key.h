#pragma once

namespace acl
{
	namespace render
	{

		/*******************************
		* 64 bit sort key
		*******************************/

		union Key64
		{
			unsigned __int64 bits;
			struct
			{
				unsigned long long Material : 16;
				unsigned long long Depth : 32;
				unsigned long long Unused : 13;
				unsigned long long Stage : 3;
			};
		};

	}
}