#include "PostprocessEffect.h"
#include "IStage.h"
#include "Instance.h"
#include "..\Gfx\ITexture.h"
#include "..\Gfx\IEffect.h"
#include "..\Gfx\IMesh.h"
#include "..\Gfx\ICbuffer.h"

namespace acl
{
	namespace render
	{

		PostprocessEffect::PostprocessEffect(IStage& stage, const gfx::IMesh& mesh, const gfx::IEffect& effect, gfx::ICbuffer* pCbuffer, const TextureVector& vTextures) :
			m_pStage(&stage), m_pCbuffer(pCbuffer)
		{
			// bind resources

			unsigned int slot = 0;
			for(auto pTexture : vTextures)
			{
				pTexture->Bind(m_states, gfx::ITexture::BindTarget::PIXEL, slot);
				slot++;
			}

			effect.BindEffect(m_states);

			if(m_pCbuffer)
				m_pCbuffer->Bind(m_states, gfx::CbufferType::PIXEL, gfx::CbufferSlot::INSTANCE);

			// setup instance
			const Key64 key = { 0 };

			const StateGroup* groups[3] = {0};

			groups[2] = mesh.GetStates()[0];
			groups[1] = &effect.GetState();
			groups[0] = &m_states;

			m_pInstance = new Instance(key, groups, 3);
			m_pInstance->SetCall(mesh.GetDraw(0));
		}

		PostprocessEffect::~PostprocessEffect()
		{
			delete m_pCbuffer;
			delete m_pInstance;
			// TODO: remove stage, possibly inside postprocessor
		}

		void PostprocessEffect::SetRenderTarget(const gfx::ITexture* pTexture)
		{
			m_pStage->SetRenderTarget(0, pTexture);
		}

		void PostprocessEffect::SetShaderConstant(unsigned int index, const float* constArray, size_t numConsts)
		{
			if(m_pCbuffer)
				m_pCbuffer->Write(index, constArray, numConsts);
		}

		const IStage& PostprocessEffect::GetStage(void) const
		{
			return *m_pStage;
		}

		void PostprocessEffect::Render(void) const
		{
			m_pStage->SubmitInstance(*m_pInstance);
		}

	}
}
