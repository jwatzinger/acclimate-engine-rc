#pragma once
#include <vector>
#include "StateGroup.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
		class IMesh;
		class IEffect;
		class ICbuffer;
	}
	namespace render
	{

		class IStage;
		struct Instance;

		class ACCLIMATE_API PostprocessEffect
		{
		public:
			typedef std::vector<const gfx::ITexture*> TextureVector;

			PostprocessEffect(IStage& stage, const gfx::IMesh& mesh, const gfx::IEffect& effect, gfx::ICbuffer* pCbuffer, const TextureVector& vTextures);
			~PostprocessEffect(void);

			void SetRenderTarget(const gfx::ITexture* pTexture);
			void SetShaderConstant(unsigned int index, const float* constArray, size_t numConsts);

			const IStage& GetStage(void) const;

			void Render(void) const;

		private:
#pragma warning( disable: 4251 )
			Instance* m_pInstance;
			IStage* m_pStage;
			StateGroup m_states;

			gfx::ICbuffer* m_pCbuffer;
#pragma warning( default: 4251 )
		};

	}
}


