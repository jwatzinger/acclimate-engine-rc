#include "Postprocessor.h"
#include "PostprocessEffect.h"
#include "..\Gfx\ITextureLoader.h"
#include "..\Gfx\Screen.h"
#include "..\Gfx\EffectInternalAccessor.h"
#include "..\Gfx\CbufferInternalAccessor.h"
#include "..\Gfx\Defines.h"
#include "..\Render\IRenderer.h"
#include "..\Render\IStage.h"
#include "..\System\Log.h"

namespace acl
{
	namespace render
	{

		static const std::wstring stScene1 = L"PostprocessScene1";
		static const std::wstring stScene2 = L"PostprocessScene2";

		static const std::wstring stSceneHDR1 = L"PostprocessSceneHDR1";
		static const std::wstring stSceneHDR2 = L"PostprocessSceneHDR2";

		Postprocessor::Postprocessor(gfx::Textures& textures, const gfx::ITextureLoader& loader, const gfx::Effects& effects, const gfx::Screen& screen, const gfx::ICbufferLoader& cbuffers, IRenderer& renderer) : 
			m_pTextures(&textures), m_pSceneIn(nullptr), m_pPositions(nullptr), m_pNormals(nullptr), m_pScene1(nullptr), m_pScene2(nullptr), m_pLoader(&loader), m_pEffects(&effects), m_isSceneSwitched(false), 
			m_pSceneHDR1(nullptr), m_pSceneHDR2(nullptr), m_isSceneSwitchedHDR(nullptr), m_pMesh(nullptr), m_pCbufferLoader(&cbuffers), m_pRenderer(&renderer), m_block(textures)
		{
			const auto& vScreenSize = screen.GetSize();

			m_block.Begin();

			m_pScene1 = loader.Create(stScene1, vScreenSize, gfx::TextureFormats::X32, gfx::LoadFlags::RENDER_TARGET);
			m_pScene2 = loader.Create(stScene2, vScreenSize, gfx::TextureFormats::X32, gfx::LoadFlags::RENDER_TARGET);

			m_pSceneHDR1 = loader.Create(stSceneHDR1, vScreenSize, gfx::TextureFormats::A16F, gfx::LoadFlags::RENDER_TARGET);
			m_pSceneHDR2 = loader.Create(stSceneHDR2, vScreenSize, gfx::TextureFormats::A16F, gfx::LoadFlags::RENDER_TARGET);

			m_block.End();
		}

		Postprocessor::~Postprocessor(void)
		{
			m_block.Clear();

			for(auto pEffect : m_vEffects)
			{
				m_pRenderer->RemoveStage(pEffect->GetStage(), true);
				delete pEffect;
			}

			for(auto pEffectHDR : m_vEffectsHDR)
			{
				m_pRenderer->RemoveStage(pEffectHDR->GetStage(), true);
				delete pEffectHDR;
			}
		}

		bool Postprocessor::HasEnabledHDR(void) const
		{
			return !m_stGroupHDR.empty();
		}

		const std::wstring& Postprocessor::GetOutput(void) const
		{
			if(m_vEffects.empty())
			{
				if(HasEnabledHDR())
					return m_stSceneHDR;
				else
					return m_stScene;
			}
			else
			{
				if(m_isSceneSwitched)
					return stScene1;
				else
					return stScene2;
			}
		}

		const std::wstring& Postprocessor::GetPreHDROutput(void) const
		{
			if(HasEnabledHDR())
			{
				if(!m_vEffectsHDR.empty())
				{
					if(m_isSceneSwitchedHDR)
						return stSceneHDR1;
					else
						return stSceneHDR2;
				}
				else
				{
					return m_stScene;
				}
			}
			else
			{
				static const std::wstring stEmpty = L"";
				return stEmpty;
			}
		}

		void Postprocessor::SelectRenderGroup(const std::wstring& stGroup)
		{
			if(m_stGroup != stGroup)
			{
				if(!m_pRenderer->HasGroup(stGroup))
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Render-group", stGroup, "cannot be selected for postprocessor (group doesn't exist).");
				else
					m_stGroup = stGroup;
			}
		}

		void Postprocessor::SetupHDR(const std::wstring& stGroup, const std::wstring& stSceneHDR)
		{
			if(m_stSceneHDR != stSceneHDR)
			{
				if(auto pSceneInHDR = m_pTextures->Get(stSceneHDR))
					m_pSceneInHDR = pSceneInHDR;
				else
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Scene-texture", stSceneHDR, "not found for postprocessor.");

				m_stSceneHDR = stSceneHDR;
			}

			if(m_stGroupHDR != stGroup)
			{
				if(!m_pRenderer->HasGroup(stGroup))
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "HDR-Render-group", stGroup, "cannot be selected for postprocessor (group doesn't exist).");
				else
					m_stGroupHDR = stGroup;
			}
		}

		void Postprocessor::SelectInputTextures(const std::wstring& stScene, const std::wstring& stPositions, const std::wstring& stNormals)
		{
			if(m_stScene != stScene)
			{
				if(auto pScene = m_pTextures->Get(stScene))
					m_pSceneIn = pScene;
				else
					sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Scene-texture", stScene, "not found for postprocessor.");

				m_stScene = stScene;
			}

			if(auto pPosition = m_pTextures->Get(stPositions))
				m_pPositions = pPosition;
			else
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Position-texture", stPositions, "not found for postprocessor.");

			if(auto pNormals = m_pTextures->Get(stNormals))
				m_pNormals = pNormals;
			else
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Normal-texture", stNormals, "not found for postprocessor.");
		}

		void Postprocessor::SelectMesh(const gfx::IMesh& mesh)
		{
			m_pMesh = &mesh;
		}

		PostprocessEffect* Postprocessor::AppendEffect(const std::wstring& stEffect, InputTextures textures, bool useHDR)
		{
			if(useHDR && m_stGroupHDR.empty())
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Cannot create HDR-postprocessing-effect", stEffect, ": No render group selected for HDR (might not be supported by currently active plugins).");
				return nullptr;
			}
			else if(m_stGroup.empty())
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Cannot create postprocessing-effect", stEffect, ": No render group selected (might not be supported by currently active plugins).");
				return nullptr;
			}

			if(!m_pMesh)
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Cannot create postprocessing-effect", stEffect, ": No mesh is selected for rendering the fullscreen pass.");
				return nullptr;
			}

			auto pEffect = m_pEffects->Get(stEffect);
			if(!pEffect)
			{
				sys::log->Out(sys::LogModule::RENDER, sys::LogType::WARNING, "Cannot create postprocessing-effect", stEffect, ": Effect (shader) was not found.");
				return nullptr;
			}

			/*********************************
			* Setup gfx-resources
			**********************************/

			auto pAclCbuffer = gfx::clonePCbuffer(*pEffect, gfx::CBUFFER_INSTANCE_ID, 0);
			auto pCbuffer = gfx::createCbuffer(*m_pCbufferLoader, pAclCbuffer);

			PostprocessEffect::TextureVector vTextures;

			if(useHDR)
				vTextures.push_back(m_pSceneIn);
			else
				vTextures.push_back(m_pSceneInHDR);

			switch(textures)
			{
			case InputTextures::SCENE:
				break;
			case InputTextures::SCENE_NORMAL:
				vTextures.push_back(m_pNormals);
				break;
			case InputTextures::SCENE_NORMAL_POS:
				vTextures.push_back(m_pNormals);
				vTextures.push_back(m_pPositions);
				break;
			case InputTextures::SCENE_POS:
				vTextures.push_back(m_pPositions);
				break;
			}

			/*********************************
			* Setup render-resources
			**********************************/

			std::wstring* pGroupToUse = nullptr;
			if(useHDR)
				pGroupToUse = &m_stGroupHDR;
			else
				pGroupToUse = &m_stGroup;

			const auto queue = m_pRenderer->AddQueue(*pGroupToUse);

			auto pStage = m_pRenderer->AddStage(stEffect, *pGroupToUse, queue, 0);

			gfx::ITexture* pTarget = nullptr;
			if(useHDR)
			{
				if(m_isSceneSwitchedHDR)
					pTarget = m_pSceneHDR2;
				else
					pTarget = m_pSceneHDR1;
			}
			else
			{
				if(m_isSceneSwitched)
					pTarget = m_pScene2;
				else
					pTarget = m_pScene1;
			}

			pStage->SetRenderTarget(0, pTarget);
			pStage->SetViewportFromTarget();

			/*********************************
			* Create posteffect
			**********************************/

			auto pPostEffect = new PostprocessEffect(*pStage, *m_pMesh, *pEffect, pCbuffer, vTextures);

			// the final output texture is switched with every effect added
			if(useHDR)
			{
				m_vEffectsHDR.push_back(pPostEffect);
				m_isSceneSwitchedHDR = !m_isSceneSwitchedHDR;
			}
			else
			{
				m_vEffects.push_back(pPostEffect);
				m_isSceneSwitched = !m_isSceneSwitched;
			}

			return pPostEffect;
		}

		bool removeEffect(PostprocessEffect& effect, IRenderer& renderer, std::vector<PostprocessEffect*>& vEffects, const gfx::ITexture& scene1, const gfx::ITexture& scene2, bool& isSceneSwitched)
		{
			auto itr = std::find(vEffects.begin(), vEffects.end(), &effect);

			if(itr != vEffects.end())
			{
				auto itr2 = itr + 1;
				auto offset = itr2 - vEffects.begin();
				while(itr2 != vEffects.end())
				{
					// inverse logic, since afterwards we are going to remove that once texture
					const gfx::ITexture* pTarget = nullptr;
					if(offset % 2)
						pTarget = &scene1;
					else
						pTarget = &scene2;
					(*itr2)->SetRenderTarget(pTarget);
				}

				vEffects.erase(itr);

				renderer.RemoveStage(effect.GetStage(), true);
				delete &effect;

				isSceneSwitched = !isSceneSwitched;

				return true;
			}
			else
				return false;
		}

		void Postprocessor::RemoveEffect(PostprocessEffect& effect)
		{
			// handle non-hdr-effect
			if(removeEffect(effect, *m_pRenderer, m_vEffects, *m_pScene1, *m_pScene2, m_isSceneSwitched))
				return;

			// handle hdr-effects
			removeEffect(effect, *m_pRenderer, m_vEffectsHDR, *m_pSceneHDR1, *m_pSceneHDR2, m_isSceneSwitchedHDR);
		}

		void Postprocessor::Execute(void) const
		{
			for(auto pEffect : m_vEffectsHDR)
			{
				pEffect->Render();
			}

			for(auto pEffect : m_vEffects)
			{
				pEffect->Render();
			}
		}

	}
}

