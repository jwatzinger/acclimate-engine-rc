#pragma once
#include "..\Core\Dll.h"
#include "..\Gfx\Textures.h"
#include "..\Gfx\Effects.h"

namespace acl
{
	namespace gfx
	{
		class ITextureLoader;
		class Screen;
		class IMesh;
		class ICbufferLoader;
	}

	namespace render
	{

		enum class InputTextures
		{
			SCENE,
			SCENE_NORMAL,
			SCENE_NORMAL_POS,
			SCENE_POS
		};

		class PostprocessEffect;
		class IRenderer;

		class ACCLIMATE_API Postprocessor
		{
			typedef std::vector<PostprocessEffect*> EffectVector;
		public:
			Postprocessor(gfx::Textures& textures, const gfx::ITextureLoader& loader, const gfx::Effects& effects, const gfx::Screen& screen, const gfx::ICbufferLoader& cbuffers, IRenderer& renderer);
			~Postprocessor(void);

			bool HasEnabledHDR(void) const;
			const std::wstring& GetOutput(void) const;
			const std::wstring& GetPreHDROutput(void) const;

			void SelectRenderGroup(const std::wstring& stGroup);
			void SetupHDR(const std::wstring& stGroup, const std::wstring& stSceneHDR);
			void SelectInputTextures(const std::wstring& stScene, const std::wstring& stPositions, const std::wstring& stNormals);
			void SelectMesh(const gfx::IMesh& mesh);

			PostprocessEffect* AppendEffect(const std::wstring& stEffect, InputTextures textures, bool useHDR);
			void RemoveEffect(PostprocessEffect& effect);

			void Execute(void) const;

		private:
#pragma warning( disable: 4251 )
			gfx::Textures* m_pTextures;
			gfx::Textures::Block m_block;
			const gfx::ITextureLoader* m_pLoader;
			const gfx::Effects* m_pEffects;
			const gfx::ICbufferLoader* m_pCbufferLoader;
			IRenderer* m_pRenderer;

			bool m_isSceneSwitched, m_isSceneSwitchedHDR;
			std::wstring m_stGroup, m_stGroupHDR, m_stScene, m_stSceneHDR;
			gfx::ITexture* m_pSceneIn, *m_pSceneInHDR, *m_pPositions, *m_pNormals, *m_pScene1, *m_pScene2, *m_pSceneHDR1, *m_pSceneHDR2;
			const gfx::IMesh* m_pMesh;

			EffectVector m_vEffects, m_vEffectsHDR;
#pragma warning( default: 4251 )
		};

	}
}

