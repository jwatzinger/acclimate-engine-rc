#include "RendererInternalAccessor.h"
#include "IRenderer.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace render
	{

		RendererInternalAccessor::RendererInternalAccessor(void) : m_pDevice(nullptr)
		{
		}

		void RendererInternalAccessor::SetDevice(const AclDevice* pDevice)
		{
			m_pDevice = pDevice;
		}

		const AclDevice* RendererInternalAccessor::GetDevice(void) const
		{
			return m_pDevice;
		}

		const AclDevice& getDevice(const IRenderer& renderer)
		{
			RendererInternalAccessor accessor;
			renderer.GetDevice(accessor);
			ACL_ASSERT(accessor.GetDevice());
			return *accessor.GetDevice();
		}

		const AclDevice* getDevice(const IRenderer* pRenderer)
		{
			if(pRenderer)
				return &getDevice(*pRenderer);
			else
				return nullptr;
		}


	}
}

