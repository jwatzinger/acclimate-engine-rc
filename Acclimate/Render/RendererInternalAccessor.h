#pragma once
#include "..\ApiDef.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace render
	{

		class IRenderer;

		class RendererInternalAccessor
		{
		public:

			RendererInternalAccessor(void);

			void SetDevice(const AclDevice* pDevice);
			const AclDevice* GetDevice(void) const;

		private:

			const AclDevice* m_pDevice;
		};

		ACCLIMATE_API const AclDevice& getDevice(const IRenderer& Renderer);
		ACCLIMATE_API const AclDevice* getDevice(const IRenderer* pRenderer);

	}
}


