#include "StateGroup.h"
#include "..\Render\States.h"

namespace acl
{
    namespace render
    {

        StateGroup::StateGroup(void): pStates(nullptr), cCommands(0) {}

		StateGroup::StateGroup(const StateGroup& group): cCommands(group.cCommands)
		{
			pStates = new char*[cCommands*STATE_SIZE];
			memcpy(pStates, group.pStates, cCommands*STATE_SIZE);
		}

		StateGroup& StateGroup::operator=(const StateGroup& group)
		{
			if(this != &group)
			{
				if(cCommands != group.cCommands)
				{
					delete[] pStates;
					cCommands = group.cCommands;
					pStates = new char*[cCommands*STATE_SIZE];
				}

				memcpy(pStates, group.pStates, cCommands*STATE_SIZE);
			}
			return *this;
		}

        StateGroup::~StateGroup(void)
        {
			delete[] pStates;
        }

    }
}