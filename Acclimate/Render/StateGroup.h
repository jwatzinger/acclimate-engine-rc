#pragma once

namespace acl
{
	namespace render
	{

		const unsigned int STATE_SIZE = 32;
		class BaseState;

		/********************************
		* State group
		********************************/
		class StateGroup
		{
		public:	

			StateGroup(void);
			StateGroup(const StateGroup& group);
			~StateGroup(void);

			StateGroup& operator=(const StateGroup& group);
	
			template<typename S, typename... Args>
			S& Add(Args&& ... args);

			template<typename S>
			void Remove(void);

			inline unsigned int GetCommandCount(void) const
			{
				return cCommands;
			}

			inline const void* GetCommands(void) const
			{
				return pStates; 
			}

		private:	
	
			void* pStates;
			unsigned int cCommands;
		};

		template<typename S, typename... Args>
		S& StateGroup::Add(Args&& ... args) 
		{
			const unsigned int type = S::type();
			for(unsigned int i = 0; i < cCommands; i ++)
			{
				const unsigned int id = *reinterpret_cast<unsigned int*>(reinterpret_cast<char*>(pStates) + STATE_SIZE*i);
				if(id == type)
				{
					S* pTempStates = reinterpret_cast<S*>(reinterpret_cast<char*>(pStates) + STATE_SIZE*i+sizeof(unsigned int));
					pTempStates->~S();
					new (pTempStates) S(args...);
					return *pTempStates;
				}
			}

			const unsigned int cmdSize = cCommands*STATE_SIZE;
			cCommands++;
			void* pNewStates = new char*[cmdSize+STATE_SIZE];
			memcpy(pNewStates, pStates, cmdSize);

			delete[] pStates;
			pStates = pNewStates;

			*reinterpret_cast<unsigned int*>(reinterpret_cast<char*>(pStates)+cmdSize) = type;
			S* pTempStates = reinterpret_cast<S*>(&reinterpret_cast<char*>(pStates)[cmdSize + sizeof(unsigned int)]);
			new (pTempStates) S(args...);

			return *pTempStates;
		}

		template<typename S>
		void StateGroup::Remove(void)
		{
			auto itr = mCommands.find(S::type());
			if(itr != mCommands.end())
			{
				reinterpret_cast<S*>(reinterpret_cast<char*>(pStates) + itr->second)->~S();
				
				if(cCommands-- > 1)
				{
					void** pNewStates = new char*[cmdSize];

					memcpy(pNewStates, pStates, i*sizeof(BaseState*));

					if(i != cCommands)
						memcpy(pNewStates+i+1, pStates+i+1, cCommands-i-1);

					delete[] pStates;

					pStates = pNewStates;
				}
			}
		}

	}
}