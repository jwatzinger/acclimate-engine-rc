#pragma once

namespace acl
{
	namespace render
	{

		class StateGuard
		{
		public:

			virtual ~StateGuard(void) = 0 {};
		};

	}
}