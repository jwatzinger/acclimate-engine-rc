#pragma once
#include <string.h>

namespace acl
{
	namespace render
	{

		/****************************************
		* Draw call
		****************************************/

		class BaseDrawCall
		{
		public:

            virtual ~BaseDrawCall(void) = 0 {}

			virtual BaseDrawCall& Clone(void) const = 0;

			typedef unsigned int Type;

			Type GetType(void) const { return m_type; }

		protected:

			static Type type_count;
			Type m_type;
		};

		template <typename Derived>
		class DrawCall : 
			public BaseDrawCall 
		{
		public:

			DrawCall(void) { m_type = type(); }
            virtual ~DrawCall(void) = 0 {};

			static BaseDrawCall::Type type(void) 
			{
				static BaseDrawCall::Type Type = type_count++;
				return Type;
			};
		};

		/****************************************
		* Render States
		****************************************/

		class BaseState
		{
		public:

			typedef unsigned int Type;

		protected:

			static Type type_count;
		};

		template <typename Derived>
		class State : 
			public BaseState 
		{
		public:

			static BaseState::Type type(void) 
			{
				static BaseState::Type Type = type_count++;
				return Type;
			};
		};
	}
}