#pragma once

namespace acl
{

    #define SAFE_RELEASE(p) { if ( (p) ) { (p)->Release(); } }

}
