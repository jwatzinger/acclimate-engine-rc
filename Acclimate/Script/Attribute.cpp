#include "Attribute.h"
#include "Core.h"
#include "..\System\Convert.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace script
	{

		Attribute::Attribute(void) : m_type(Type::UNKNOWN), m_pData(nullptr)
		{
		}

		Attribute::Attribute(const std::string& stName, Type type, void* pData) :
			m_stName(stName), m_type(type), m_pData(pData)
		{
		}

		const std::string& Attribute::GetName(void) const
		{
			return m_stName;
		}

		Attribute::Type Attribute::GetType(void) const
		{
			return m_type;
		}

		void Attribute::EnumerateTypes(const Core& core)
		{
			m_mTypes.clear();

			static const std::vector<std::pair<Type, std::string>> vTypes = 
			{
				{ Type::INT, "int" }, { Type::FLOAT, "float" }, { Type::BOOL, "bool" }, { Type::UINT, "uint" }
			};

			ACL_ASSERT(vTypes.size() == (size_t)Type::SIZE);

			for(auto& type : vTypes)
			{
				const int id = core.GetTypeId(type.second.c_str());
				if(id >= 0)
					m_mTypes.emplace(id, type.first);
				else
					sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::WARNING, "failed to retrieve typeid for attribute type", type.second, ", reflection won't work for this type.");
			}
		}

		Attribute::Type Attribute::TypeFromId(unsigned int id)
		{
			auto itr = m_mTypes.find(id);
			if(itr != m_mTypes.end())
				return itr->second;
			else
				return Type::UNKNOWN;
		}

		Attribute::TypeMap Attribute::m_mTypes = {};

	}
}
