#pragma once
#include <unordered_map>
#include "angelscript.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{
		
		class Core;

		class ACCLIMATE_API Attribute
		{
		public:
			enum class Type
			{
				UNKNOWN = -1, INT = 0, BOOL, FLOAT, UINT, SIZE
			};

			Attribute(void);
			Attribute(const std::string& stName, Type type, void* pData);

			const std::string& GetName(void) const;
			Type GetType(void) const;

			template<typename Ty>
			void ModifyData(Ty&& value);

			template<typename Ty>
			const Ty* GetData(void) const;

			static void EnumerateTypes(const Core& core);
			static Type TypeFromId(unsigned int id);

		private:
#pragma warning( disable: 4251 )
			typedef std::unordered_map<unsigned int, Type> TypeMap;

			std::string m_stName;
			Type m_type;
			void* m_pData;

			static TypeMap m_mTypes;
#pragma warning( default: 4251 )
		};

		template<typename Ty>
		void Attribute::ModifyData(Ty&& value)
		{
			(*(Ty*)m_pData) = value;
		}

		template<typename Ty>
		const Ty* Attribute::GetData(void) const
		{
			return (Ty*)m_pData;
		}

	}
}


