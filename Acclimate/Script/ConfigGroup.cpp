#include "ConfigGroup.h"
#include "angelscript.h"

namespace acl
{
	namespace script
	{

		ConfigGroup::ConfigGroup(void) : m_pEngine(nullptr)
		{
		}

		ConfigGroup::ConfigGroup(const char* group, asIScriptEngine& engine) : m_stName(group), m_pEngine(&engine)
		{
		}

		ConfigGroup::ConfigGroup(ConfigGroup&& group) : m_pEngine(group.m_pEngine), m_stName(std::move(group.m_stName))
		{
			group.m_stName.clear();
			group.m_pEngine = nullptr;
		}

		ConfigGroup::~ConfigGroup(void)
		{
			if(!m_stName.empty())
				m_pEngine->RemoveConfigGroup(m_stName.c_str());
		}

		void ConfigGroup::operator=(ConfigGroup&& group)
		{
			m_stName = std::move(group.m_stName);
			m_pEngine = group.m_pEngine;
			group.m_pEngine = nullptr;
		}

		void ConfigGroup::Begin(void)
		{
			if(!m_stName.empty())
				m_pEngine->BeginConfigGroup(m_stName.c_str());
		}

		void ConfigGroup::End(void)
		{
			m_pEngine->EndConfigGroup();
		}

		void ConfigGroup::Remove(void)
		{
			if(!m_stName.empty())
			{
				m_pEngine->RemoveConfigGroup(m_stName.c_str());
				m_stName.clear();
			}
		}

	}
}

