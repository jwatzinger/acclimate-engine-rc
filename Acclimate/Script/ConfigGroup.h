#pragma once
#include <string>
#include "..\Core\Dll.h"

class asIScriptEngine;

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API ConfigGroup
		{
		public:
			ConfigGroup(void);
			ConfigGroup(const char* group, asIScriptEngine& engine);
			ConfigGroup(ConfigGroup&& group);
			~ConfigGroup(void);

			void operator=(ConfigGroup&& group);

			void Begin(void);
			void Remove(void);
			void End(void);

		private:
#pragma warning( disable: 4251 )
			std::string m_stName;

			asIScriptEngine* m_pEngine;
#pragma warning( default: 4251 )
		};

	}
}


