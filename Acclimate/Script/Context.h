#pragma once
#include "Resources.h"
#include "Loader.h"

namespace acl
{
	namespace script
	{

		class Core;

		struct Context
		{
			Context(Core& core, Scripts& scripts) : core(core), scripts(scripts), loader(core, scripts) {}

			Core& core;
			Scripts& scripts;
			const Loader loader;
		};

	}
}