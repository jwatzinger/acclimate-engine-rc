#include "Core.h"
#include "String.h"
#include "Extern\scriptarray.h"
#include "..\System\Log.h"

#ifdef _DEBUG
#pragma comment(lib, "angelscriptd.lib")
#else
#pragma comment(lib, "angelscript.lib")
#endif

namespace acl
{
	namespace script
	{

		void MessageCallback(const asSMessageInfo *msg, void *param)
		{
			sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::ERR, "<", msg->row, msg->col, ">", msg->message);
		}

		Core::Core(const Scripts& scripts) : m_pEngine(asCreateScriptEngine(ANGELSCRIPT_VERSION)),
			m_pScripts(&scripts)
		{
			sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::INFO, "Initializing angelscript core.");

			m_pEngine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);
			m_pContext = m_pEngine->CreateContext();

			RegisterScriptArray(m_pEngine, true);

			m_pEngine->SetEngineProperty(asEP_STRING_ENCODING, 1);
		}

		Core::~Core(void)
		{
			m_pContext->Release();
			m_pEngine->Release();
		}

		ConfigGroup Core::GetConfigGroup(const char* groupName) const
		{
			return ConfigGroup(groupName, *m_pEngine);
		}

		std::vector<Type> Core::GetTypesWithInterface(const char* interfaceName) const
		{
			std::vector<Type> vTypes;

			auto pModule = m_pEngine->GetModule("module", asGM_ONLY_IF_EXISTS);
			if(!pModule)
				return vTypes;

			auto numTypes = pModule->GetObjectTypeCount();

			for(unsigned int i = 0; i < numTypes; i++)
			{
				auto pType = pModule->GetObjectTypeByIndex(i);
				auto numInterfaces = pType->GetInterfaceCount();
				for(unsigned int j = 0; j < numInterfaces; j++)
				{
					if(!strcmp(pType->GetInterface(j)->GetName(), interfaceName))
						vTypes.emplace_back(*pType, *m_pContext);
				}
			}

			return vTypes;
		}

		Function Core::GetGlobalFunction(const char* name) const
		{
			asIScriptFunction* pFunction = m_pEngine->GetModule("module")->GetFunctionByDecl(name);

			if(pFunction)
				return Function(*pFunction, *m_pContext);
			else
				return Function();
		}

		asIScriptContext& Core::GetContext(void) const
		{
			return *m_pContext;
		}

		int Core::GetTypeId(const char* declaration) const
		{
			return m_pEngine->GetTypeIdByDecl(declaration);
		}

		void Core::Reload(void)
		{
			// Create a new script module
			asIScriptModule *mod = m_pEngine->GetModule("module", asGM_CREATE_IF_NOT_EXISTS);

			for(auto& script : m_pScripts->Map())
			{
				mod->AddScriptSection("script", script.second->GetData().c_str());
			}

			// Build the module
			int r = mod->Build();
			if(r < 0)
			{
				sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::ERR, "Failed to build scripts.");
				return;
			}
		}

		void Core::DiscardModule(void)
		{
			m_pEngine->GarbageCollect(asGC_FULL_CYCLE);
			m_pEngine->DiscardModule("module");
		}

		void Core::RegisterGlobalFunction(const char* name, const asSFuncPtr& func)
		{
			m_pEngine->RegisterGlobalFunction(name, func, asCALL_CDECL);
		}

		Enum Core::RegisterEnum(const char* name)
		{
			m_pEngine->RegisterEnum(name);
			return Enum(name, *m_pEngine);
		}

		Object Core::RegisterReferenceType(const char* name, int flags)
		{
			m_pEngine->RegisterObjectType(name, 0, asOBJ_REF | flags);
			return Object(name, *m_pEngine);
		}

		Interface Core::RegisterInterface(const char* name)
		{
			m_pEngine->RegisterInterface(name);
			return Interface(name, *m_pEngine);
		}

		void Core::RegisterStringFactory(const char* datatype, const asSFuncPtr& func)
		{
			m_pEngine->RegisterStringFactory(datatype, func, asCALL_CDECL);
		}

		void Core::RegisterFuncdef(const char* declaration)
		{
			m_pEngine->RegisterFuncdef(declaration);
		}

	}
}