#pragma once
#include <string>
#include <vector>
#include <type_traits>
#include <unordered_map>
#include "angelscript.h"
#include "Interface.h"
#include "Object.h"
#include "Type.h"
#include "Enum.h"
#include "Resources.h"
#include "ConfigGroup.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{

		enum ReferenceFlags
		{
			REF_NONE = 0,
			REF_GC = asOBJ_GC,
			REF_NOCOUNT = asOBJ_NOCOUNT
		};

		enum ValueFlags
		{
			VALUE_NONE = 0,
			VALUE_POD = asOBJ_POD,
			VALUE_CLASS = asOBJ_APP_CLASS,
			VALUE_CLASS_CONSTRUCTOR = asOBJ_APP_CLASS_CONSTRUCTOR,
			VALUE_CLASS_DESTRUCTOR = asOBJ_APP_CLASS_DESTRUCTOR,
			VALUE_CLASS_COPY_CONSTRUCTOR = asOBJ_APP_CLASS_COPY_CONSTRUCTOR
		};

		class Instance;

		class ACCLIMATE_API Core
		{
		public:
			Core(const Scripts& scripts);
			~Core(void);

			std::vector<Type> GetTypesWithInterface(const char* interfaceName) const;
			ConfigGroup GetConfigGroup(const char* groupName) const;
			Function GetGlobalFunction(const char* name) const;
			// TODO: improve encapsulation
			asIScriptContext& GetContext(void) const;
			int GetTypeId(const char* declaration) const;

			void Reload(void);
			void DiscardModule(void);
			template<typename... Args>
			Instance* CreateInstanceFromInterface(const char* interfaceName, const char* className, const char* constructorElements, Args&&... args) const;
			
			void RegisterGlobalFunction(const char* name, const asSFuncPtr& func);
			Enum RegisterEnum(const char* name);
			Object RegisterReferenceType(const char* name, int flags);
			template<typename Type>
			Object RegisterValueType(const char* name, int flags);
			Interface RegisterInterface(const char* name);
			void RegisterStringFactory(const char* datatype, const asSFuncPtr& func);
			void RegisterFuncdef(const char* declaration);

		private:
#pragma warning( disable: 4251 )
			const Scripts* m_pScripts;

			asIScriptEngine*  m_pEngine;
			asIScriptContext* m_pContext;
#pragma warning( default: 4251 )
		};

		template<typename Type>
		Object Core::RegisterValueType(const char* name, int flags)
		{
			m_pEngine->RegisterObjectType(name, sizeof(Type), asOBJ_VALUE | flags);
			return Object(name, *m_pEngine);
		}

		template<typename... Args>
		Instance* Core::CreateInstanceFromInterface(const char* interfaceName, const char* className, const char* constructorElements, Args&&... args) const
		{
			auto vTypes = GetTypesWithInterface(interfaceName);
			for(auto& type : vTypes)
			{
				if(type.GetName() == className)
					return type.CreateInstance(constructorElements, args...);
			}

			return nullptr;
		}

	}
}

