#include "Enum.h"
#include "angelscript.h"

namespace acl
{
	namespace script
	{

		Enum::Enum(const char* name, asIScriptEngine& engine) : m_stName(name),
			m_pEngine(&engine)
		{
		}

		void Enum::RegisterValue(const char* name, int value)
		{
			m_pEngine->RegisterEnumValue(m_stName.c_str(), name, value);
		}

		void Enum::RegisterConcurrentHelp(int& value, const char* name)
		{
			RegisterValue(name, value++);
		}

		void Enum::RegisterConcurrentHelp(int& value)
		{
		}

	}
}

