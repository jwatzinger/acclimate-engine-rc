#pragma once
#include <string>
#include "..\Core\Dll.h"

class asIScriptEngine;

namespace acl
{
	namespace script
	{
		
		class ACCLIMATE_API Enum
		{
		public:
			Enum(const char* name, asIScriptEngine& engine);

			void RegisterValue(const char* name, int value);

			template<typename Arg1, typename Arg2, typename... Args>
			void RegisterConcurrent(int start, Arg1&& arg, Arg2&& arg2, Args&&... args);

		private:
#pragma warning( disable: 4251 )

			template<typename Arg1, typename Arg2, typename... Args>
			void RegisterConcurrentHelp(int& start, Arg1&& arg, Arg2&& arg2, Args&&... args);
			void RegisterConcurrentHelp(int& value, const char* name);
			void RegisterConcurrentHelp(int& value);
			
			std::string m_stName;

			asIScriptEngine* m_pEngine;
#pragma warning( default: 4251 )
		};

		template<typename Arg1, typename Arg2, typename... Args>
		void Enum::RegisterConcurrent(int start, Arg1&& arg, Arg2&& arg2, Args&&... args)
		{
			int current = start;
			RegisterConcurrentHelp(current, arg, arg2, args...);
		}

		template<typename Arg1, typename Arg2, typename... Args>
		void Enum::RegisterConcurrentHelp(int& current, Arg1&& arg, Arg2&& arg2, Args&&... args)
		{
			RegisterConcurrentHelp(current, arg);
			RegisterConcurrentHelp(current, arg2);
			RegisterConcurrentHelp(current, args...);
		}

	}
}


