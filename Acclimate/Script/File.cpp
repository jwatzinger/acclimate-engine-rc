#include "File.h"

namespace acl
{
	namespace script
	{

		File::File(const std::string& stScript, const std::wstring& stFile) :
			m_stScript(stScript), m_stFile(stFile)
		{
		}

		const std::string& File::GetData(void) const
		{
			return m_stScript;
		}

		const std::wstring& File::GetFile(void) const
		{
			return m_stFile;
		}

	}
}
