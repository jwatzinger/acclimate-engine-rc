#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API File
		{
		public:
			File(const std::string& stScript, const std::wstring& stFile);

			const std::string& GetData(void) const;
			const std::wstring& GetFile(void) const;

		private:
#pragma warning( disable: 4251 )
			std::string m_stScript;
			std::wstring m_stFile;
#pragma warning( default: 4251 )
		};

	}
}

