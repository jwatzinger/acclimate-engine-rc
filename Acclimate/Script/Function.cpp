#include "Function.h"

namespace acl
{
	namespace script
	{

		/*************************************************
		* BASEFUNCTION
		*************************************************/

		BaseFunction::BaseFunction(void) : m_pContext(nullptr), m_pFunction(nullptr), m_isValid(false)
		{
		}

		BaseFunction::BaseFunction(asIScriptFunction& function, asIScriptContext& context) : m_pContext(&context),
			m_pFunction(&function), m_isValid(true)
		{
		}

		void BaseFunction::PushArguments(unsigned int& numArg, void* pObject)
		{
			m_pContext->SetArgObject(numArg++, pObject);
		}

		void BaseFunction::PushArguments(unsigned int& numArg, int arg)
		{
			m_pContext->SetArgDWord(numArg++, arg);
		}

		void BaseFunction::PushArguments(unsigned int& numArg, float arg)
		{
			m_pContext->SetArgFloat(numArg++, arg);
		}

		void BaseFunction::PushArguments(unsigned int& numArg, double arg)
		{
			m_pContext->SetArgDouble(numArg++, arg);
		}

		void BaseFunction::PushArguments(unsigned int& numArg, bool arg)
		{
			m_pContext->SetArgByte(numArg++, arg);
		}

		void BaseFunction::PushArguments(unsigned int& numArg)
		{
		}

		template<>
		void BaseFunction::ReturnArgument(void)
		{
		}

		bool BaseFunction::IsValid(void) const
		{
			return m_isValid;
		}

		/*************************************************
		* FUNCTION
		*************************************************/

		Function::Function(void) : BaseFunction()
		{
		}

		Function::Function(asIScriptFunction& function, asIScriptContext& context) : BaseFunction(function, context)
		{
		}

		/*************************************************
		* METHOD
		*************************************************/

		Method::Method(void) : BaseFunction(), m_pObject(nullptr)
		{
		}

		Method::Method(asIScriptFunction& function, asIScriptContext& context, asIScriptObject& object) : BaseFunction(function, context),
			m_pObject(&object)
		{
		}

	}
}

