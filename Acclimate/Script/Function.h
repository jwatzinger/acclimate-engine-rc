#pragma once
#include "angelscript.h"
#include "..\Core\Dll.h"
#include "..\System\Log.h"

namespace acl
{
	namespace script
	{

		/******************************************
		* BaseFunction
		******************************************/

		class ACCLIMATE_API BaseFunction
		{
		protected:

			BaseFunction(void);
			BaseFunction(asIScriptFunction& function, asIScriptContext& context);

			template<typename Arg1, typename Arg2, typename... Args>
			void PushArguments(unsigned int& numArg, Arg1&& arg1, Arg2&& arg2, Args&&... args);
			template<typename Object>
			void PushArguments(unsigned int& numArg, Object&& object);
			template<typename Object>
			void PushArguments(unsigned int& numArg, Object* pObject);
			void PushArguments(unsigned int& numArg, void* pObject);
			void PushArguments(unsigned int& numArg, int arg);
			void PushArguments(unsigned int& numArg, float arg);
			void PushArguments(unsigned int& numArg, double arg);
			void PushArguments(unsigned int& numArg, bool arg);
			void PushArguments(unsigned int& numArg);

			template<typename Type>
			Type ReturnArgument(void);
			template<>
			void ReturnArgument(void);

			asIScriptFunction* m_pFunction;
			asIScriptContext* m_pContext;

		public:

			bool IsValid(void) const;

		private:

			bool m_isValid;
		};

		template<typename Arg1, typename Arg2, typename... Args>
		void BaseFunction::PushArguments(unsigned int& numArg, Arg1&& arg1, Arg2&& arg2, Args&&... args)
		{
			PushArguments(numArg, arg1);
			PushArguments(numArg, arg2);
			PushArguments(numArg, args...);
		}

		template<typename Object>
		void BaseFunction::PushArguments(unsigned int& numArg, Object&& object)
		{
			PushArguments(numArg, (void*)&object);
		}

		template<typename Object>
		void BaseFunction::PushArguments(unsigned int& numArg, Object* pObject)
		{
			PushArguments(numArg, (void*)pObject);
		}

		template<typename Type>
		Type BaseFunction::ReturnArgument(void)
		{
			return *static_cast<Type*>(m_pContext->GetAddressOfReturnValue());
		}

		/******************************************
		* Function
		******************************************/

		class ACCLIMATE_API Function : 
			public BaseFunction
		{
		public:
			Function(void);
			Function(asIScriptFunction& function, asIScriptContext& context);

			template<typename Return, typename... Args>
			Return Call(Args&&... args);
			template<typename Return, typename... Args>
			Return CallValues(Args... args);
		};

		template<typename Return, typename... Args>
		Return Function::Call(Args&&... args)
		{
			if(!IsValid())
				return Return();

			m_pContext->Prepare(m_pFunction);

			unsigned int numArg = 0;
			PushArguments(numArg, args...);

			int r = m_pContext->Execute();
			if(r == asEXECUTION_FINISHED)
				return ReturnArgument<Return>();
			else
				return Return();
		}

		template<typename Return, typename... Args>
		Return Function::CallValues(Args... args)
		{
			if(!IsValid())
				return Return();

			m_pContext->Prepare(m_pFunction);

			unsigned int numArg = 0;
			PushArguments(numArg, args...);

			int r = m_pContext->Execute();
			if(r == asEXECUTION_FINISHED)
				return ReturnArgument<Return>();
			else
				return Return();
		}

		/******************************************
		* Method
		******************************************/

		class ACCLIMATE_API Method : 
			public BaseFunction
		{
		public:
			Method(void);
			Method(asIScriptFunction& function, asIScriptContext& context, asIScriptObject& object);

			template<typename Return, typename... Args>
			Return Call(Args&&... args);

		private:

			asIScriptObject* m_pObject;
		};

		template<typename Return, typename... Args>
		Return Method::Call(Args&&... args)
		{
			if(!IsValid())
				return Return();

			m_pContext->Prepare(m_pFunction);
			m_pContext->SetObject(m_pObject);

			unsigned int numArg = 0;
			PushArguments(numArg, args...);

			int r = m_pContext->Execute();
			m_pContext->SetObject(nullptr);
			if(r == asEXECUTION_FINISHED)
				return ReturnArgument<Return>();
			else
			{
				if(r == asEXECUTION_EXCEPTION)
				{
					sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::ERR, "Script method call failed:", m_pContext->GetExceptionString(), "in line", m_pContext->GetExceptionLineNumber());
				}
				return Return();
			}
				
		}

	}
}


