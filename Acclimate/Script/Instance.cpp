#include "Instance.h"
#include "..\System\Convert.h"

namespace acl
{
	namespace script
	{

		Instance::Instance(asIScriptObject& object, asIObjectType& type, asIScriptContext& context) : m_pObject(&object), m_pType(&type),
			m_pContext(&context)
		{
			m_pObject->AddRef();

			const size_t numAttributes = type.GetPropertyCount();
			for(unsigned int i = 0; i < numAttributes; i++)
			{
				const char* pName = nullptr;
				int id;
				bool isPrivate, isReference;
				type.GetProperty(i, &pName, &id, &isPrivate, nullptr, &isReference, nullptr);

				if(!isPrivate && !isReference)
				{
					const Attribute::Type attribType = Attribute::TypeFromId(id);
					if(attribType != Attribute::Type::UNKNOWN)
					{
						void* pData = object.GetAddressOfProperty(i);

						m_mAttributes[conv::ToW(pName)] = Attribute(pName, attribType, pData);
					}
				}
			}
		}

		Instance::~Instance(void)
		{
			m_pObject->Release();
		}

		Attribute* Instance::GetAttribute(const std::wstring& stName)
		{
			auto itr = m_mAttributes.find(stName);
			if(itr == m_mAttributes.end())
				return nullptr;
			else
				return &itr->second;
		}

		const Attribute* Instance::GetAttribute(const std::wstring& stName) const
		{
			auto itr = m_mAttributes.find(stName);
			if(itr == m_mAttributes.end())
				return nullptr;
			else
				return &itr->second;
		}

		Method Instance::GetMethod(const char* declaration) const
		{
			auto pMethod = m_pType->GetMethodByDecl(declaration);
			if(pMethod)
			{
				Method method(*pMethod, *m_pContext, *m_pObject);
				return method;
			}
			else
				return Method();
		}

	}
}

