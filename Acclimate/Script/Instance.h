#pragma once
#include "angelscript.h"
#include "Function.h"
#include "Attribute.h"

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API Instance
		{
			typedef std::unordered_map<std::wstring, Attribute> AttributeMap;
		public:
			Instance(asIScriptObject& object, asIObjectType& type, asIScriptContext& context);
			~Instance(void);

			template<typename Return, typename... Args>
			Return CallMethod(const char* declaration, Args&&... args);

			Attribute* GetAttribute(const std::wstring& stName);
			const Attribute* GetAttribute(const std::wstring& stName) const;
			Method GetMethod(const char* declaration) const;

		private:
#pragma warning( disable: 4251 )
			asIScriptObject* m_pObject;
			asIObjectType* m_pType;
			asIScriptContext* m_pContext;

			AttributeMap m_mAttributes;
#pragma warning( default: 4251 )
		};

		template<typename Return, typename... Args>
		Return Instance::CallMethod(const char* declaration, Args&&... args)
		{
			auto method = GetMethod(declaration);
			return method.Call<Return>(args...);
		}

	}
}


