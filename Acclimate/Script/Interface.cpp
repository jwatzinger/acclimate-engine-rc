#include "Interface.h"
#include "angelscript.h"

namespace acl
{
	namespace script
	{

		Interface::Interface(const char* name, asIScriptEngine& engine) : 
			m_stName(name), m_pEngine(&engine)
		{
		}

		void Interface::RegisterMethod(const char* declaration)
		{
			m_pEngine->RegisterInterfaceMethod(m_stName.c_str(), declaration);
		}

	}
}
