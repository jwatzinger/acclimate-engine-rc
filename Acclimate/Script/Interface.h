#pragma once
#include <string>
#include "..\Core\Dll.h"

class asIScriptEngine;

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API Interface
		{
		public:
			Interface(const char* name, asIScriptEngine& engine);

			void RegisterMethod(const char* declaration);

		private:
#pragma warning( disable: 4251 )
			std::string m_stName;

			asIScriptEngine* m_pEngine;
#pragma warning( default: 4251 )
		};

	}
}

