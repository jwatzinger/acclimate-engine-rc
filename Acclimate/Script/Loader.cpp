#include "Loader.h"
#include "Core.h"
#include "..\File\File.h"
#include "..\System\Convert.h"
#include "..\System\Exception.h"
#include "..\System\Log.h"
#include "..\System\WorkingDirectory.h"
#include "..\XML\Doc.h"

namespace acl
{
	namespace script
	{

		Loader::Loader(Core& core, Scripts& scripts) : m_pScripts(&scripts), m_pCore(&core)
		{
		}

		void Loader::FromConfig(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			sys::WorkingDirectory dir(stFilename, true);

			auto pRoot = doc.Root(L"Scripts");

			sys::WorkingDirectory dir2;
			if(auto pPath = pRoot->Attribute(L"path"))
				dir2 = sys::WorkingDirectory(*pPath);

			if(auto pScripts = pRoot->Nodes(L"Script"))
			{
				for(auto pScript : *pScripts)
				{
					const std::wstring& stName = pScript->Attribute(L"name")->GetValue();
					const std::wstring& stFile = pScript->Attribute(L"file")->GetValue();

					try
					{
						FromFile(stName, stFile);
					}
					catch(fileException&)
					{
						sys::log->Out(sys::LogModule::SCRIPT, sys::LogType::ERR, "Failed to load script", stName, "from file", stFile);
					}	
				}
			}

			dir2.Restore();
			dir.Restore();
		}

		void Loader::FromFile(const std::wstring& stName, const std::wstring& stFilename) const
		{
			if(m_pScripts->Has(stName))
				m_pScripts->Delete(stName);

			const std::string stScript = file::LoadFileA(stFilename);
			File* pFile = new File(stScript, file::FullPath(stFilename));

			m_pScripts->Add(stName, *pFile);

			m_pCore->Reload();
		}

	}
}
