#pragma once
#include "Resources.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{

		class Core;

		class ACCLIMATE_API Loader
		{
		public:
			Loader(Core& core, Scripts& scripts);

			void FromConfig(const std::wstring& stFilename) const;
			void FromFile(const std::wstring& stName, const std::wstring& stFilename) const;

		private:

			Core* m_pCore;
			Scripts* m_pScripts;
		};

	}
}

