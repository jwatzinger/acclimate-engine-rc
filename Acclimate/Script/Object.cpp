#include "Object.h"

namespace acl
{
	namespace script
	{

		Object::Object(const char* name, asIScriptEngine& engine) : m_stName(name), m_pEngine(&engine)
		{
		}

		void Object::RegisterMethod(const char* function, const asSFuncPtr& func, asDWORD callConv)
		{
			m_pEngine->RegisterObjectMethod(m_stName.c_str(), function, func, callConv);
		}

		void Object::RegisterProperty(const char* declaration, int offset)
		{
			m_pEngine->RegisterObjectProperty(m_stName.c_str(), declaration, offset);
		}

		void Object::RegisterBehaviour(asEBehaviours behaviour, const char* declaration, const asSFuncPtr& ptr, asDWORD callConv)
		{
			m_pEngine->RegisterObjectBehaviour(m_stName.c_str(), behaviour, declaration, ptr, callConv, nullptr);
		}

	}
}
