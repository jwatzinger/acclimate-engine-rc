#pragma once
#include <string>
#include "angelscript.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API Object
		{
		public:
			Object(const char* name, asIScriptEngine& engine);

			void RegisterMethod(const char* declaration, const asSFuncPtr& func, asDWORD callConv = asCALL_THISCALL);
			void RegisterProperty(const char* declaration, int offset);
			void RegisterBehaviour(asEBehaviours behaviour, const char* declaration, const asSFuncPtr& ptr, asDWORD callConv = asCALL_THISCALL);

		private:
#pragma warning( disable: 4251 )
			std::string m_stName;

			asIScriptEngine* m_pEngine;
#pragma warning( default: 4251 )
		};

	}
}
