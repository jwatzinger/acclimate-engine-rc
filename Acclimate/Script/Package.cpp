#include "Package.h"
#include "Attribute.h"

namespace acl
{
	namespace script
	{

		Package::Package(void) : m_core(m_scripts), m_context(m_core, m_scripts)
		{
		}

		const Context& Package::GetContext(void) const
		{
			return m_context;
		}

		void Package::FinishSDKRegistration(void)
		{
			Attribute::EnumerateTypes(m_core);
		}

	}
}