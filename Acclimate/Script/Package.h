#pragma once
#include "Core.h"
#include "Resources.h"
#include "Context.h"

namespace acl
{
	namespace script
	{

		class Package
		{
		public:
			Package(void);

			const Context& GetContext(void) const;

			void FinishSDKRegistration(void);

		private:

			Scripts m_scripts;
			Core m_core;
			Context m_context;
		};

	}
}

