#pragma once
#include "File.h"
#include "..\Core\Resources.h"

namespace acl
{
	namespace script
	{

		typedef core::Resources<std::wstring, File> Scripts;

	}
}