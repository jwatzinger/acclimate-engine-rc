#include "Type.h"
#include "Instance.h"

namespace acl
{
	namespace script
	{

		Type::Type(asIObjectType& type, asIScriptContext& context) : m_pType(&type),
			m_pContext(&context)
		{
			m_pType->AddRef();
		}

		Type::Type(const Type& type) : m_pType(type.m_pType),
			m_pContext(type.m_pContext)
		{
			m_pType->AddRef();
		}

		Type::Type(Type&& type) : m_pType(type.m_pType),
			m_pContext(type.m_pContext)
		{
			type.m_pType = nullptr;
			type.m_pContext = nullptr;
		}

		Type::~Type(void)
		{
			if(m_pType)
				m_pType->Release();
		}

		std::string Type::GetName(void) const
		{
			return m_pType->GetName();
		}

	}
}
