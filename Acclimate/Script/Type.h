#pragma once
#include <string>
#include "Function.h"
#include "Instance.h"
#include "..\Core\Dll.h"

namespace acl
{
	namespace script
	{

		class ACCLIMATE_API Type
		{
		public:
			Type(asIObjectType& type, asIScriptContext& context);
			Type(const Type& type);
			Type(Type&& type);
			~Type(void);

			std::string GetName(void) const;

			template<typename... Args>
			Instance* CreateInstance(const char* params, Args&&... args);

		private:
#pragma warning( disable: 4251 )
			asIObjectType* m_pType;
			asIScriptContext* m_pContext;
#pragma warning( default: 4251 )
		};

		template<typename... Args>
		Instance* Type::CreateInstance(const char* params, Args&&... args)
		{
			const std::string stName = m_pType->GetName();
			const std::string stFactory = stName + "@ " + stName + "(" + params + ")";
			auto pFunction = m_pType->GetFactoryByDecl(stFactory.c_str());
			if(pFunction)
			{
				Function function(*pFunction, *m_pContext);
				auto pScriptObject = function.Call<asIScriptObject*>(args...);
				if(pScriptObject)
					return new Instance(*pScriptObject, *m_pType, *m_pContext);
			}

			return nullptr;
				
		}

	}
}

