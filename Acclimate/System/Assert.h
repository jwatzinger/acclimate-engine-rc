#pragma once

#ifdef _DEBUG

#include <assert.h>
#define ACL_ASSERT(x) assert(x)

#else

#define ACL_ASSERT(x) ((void)sizeof(x))

#endif