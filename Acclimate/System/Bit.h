#pragma once
#include <type_traits>

namespace acl
{
	namespace bit
	{
		/***********************
		* Bit checks
		***********************/

		template <typename T>
		bool contains(T x, T y, std::true_type) {
		  //cast x and y to their primitive type (int, ...)
	      //then check if y's bits are set for x
		  return (static_cast<typename std::underlying_type<T>::type>(x)
				  & static_cast<typename std::underlying_type<T>::type>(y))
			== static_cast<typename std::underlying_type<T>::type>(y);
		}

		template <typename T>
		bool contains(T x, T y, std::false_type) {
		  //check if y's bits are set for x
		  return (x & y) == y;
		}

		template <typename T>
		bool contains(T x, T y) {
		  //call respective contains function whether T is a primitive type or not
		  return contains(x, y, std::integral_constant<bool, std::is_enum<T>::value>());
		}

		/***********************
		* Logical xor
		***********************/

		#define log_xor !=0==! 
	}
}