#include "Clipboard.h"

namespace acl
{
	namespace sys
	{

		Clipboard::Clipboard(void) : m_pData(nullptr)
		{
		}

		Clipboard::~Clipboard(void)
		{
			delete m_pData;
		}

	}
}
