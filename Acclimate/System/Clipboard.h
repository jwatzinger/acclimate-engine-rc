#pragma once
#include "ClipboardData.h"

namespace acl
{
	namespace sys
	{

		class ACCLIMATE_API Clipboard
		{
		public:
			Clipboard(void);
			~Clipboard(void);

			template<typename Type, typename... Args>
			void AddData(Args&&... args)
			{
				static_assert(std::is_base_of<BaseClipboardData, Type>::value, "Type must be a descendant of BaseClipboardData");

				delete m_pData;
				m_pData = new Type(args...);
			}

			template<typename Type>
			Type* GetCurrentData(void) const
			{
				static_assert(std::is_base_of<BaseClipboardData, Type>::value, "Type must be a descendant of BaseClipboardData");
				if(m_pData && m_pData->GetType() == Type::Type())
					return (Type*)m_pData;
				else
					return nullptr;
			}

		private:

			BaseClipboardData* m_pData;

		};

	}
}

