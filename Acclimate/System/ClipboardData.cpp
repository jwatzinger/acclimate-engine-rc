#include "ClipboardData.h"

namespace acl
{
	namespace sys
	{

		unsigned int BaseClipboardData::typeCount = 0;

		unsigned int BaseClipboardData::GenerateTypeId(void)
		{
			return typeCount++;
		}

	}
}
