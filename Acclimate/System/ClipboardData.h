#pragma once
#include "..\Core\Dll.h"

namespace acl
{
	namespace sys
	{

		class ACCLIMATE_API BaseClipboardData
		{
		public:
			virtual ~BaseClipboardData(void) = 0 {}

			virtual unsigned int GetType(void) const = 0;

		protected:
			static unsigned int GenerateTypeId(void);

		private:
			static unsigned int typeCount;
		};

		/*********************************
		* ClipboardData
		**********************************/

		template <typename Derived>
		class ClipboardData :
			public BaseClipboardData
		{
		public:
			static unsigned int Type(void)
			{
				//increase base family count on first access
				static unsigned int type = GenerateTypeId();
				return type;
			}

			unsigned int GetType(void) const override
			{
				return Type();
			}

			virtual ~ClipboardData(void) = 0 {};
		};

#define USE_CLIPBOARD_DATA(Type) EXPORT_TEMPLATE template class ACCLIMATE_API acl::sys::ClipboardData<Type>;

	}
}

