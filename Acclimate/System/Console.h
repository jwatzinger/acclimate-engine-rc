/** @file System\Console.h
*	
*	Handles a seperate console window for output.
*/
#pragma once

#ifdef _DEBUG

namespace acl
{
    namespace sys
    {
		/** Setups a console window.
		 *	Creates an console window in additon to an already existing application window. std functions like
		 *	cin, cout are redirected to this consolse.
		 */
        void redirectIOToConsole(void);

		void freeConsole(void);
    }
}

#endif
