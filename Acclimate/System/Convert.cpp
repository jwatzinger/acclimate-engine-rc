#include "Convert.h"

namespace acl
{
    namespace conv
    {
        /************************************************
        * POD to String
        ************************************************/

        std::wstring ToString(float value, unsigned int precision) 
		{
			wchar_t* buffer = new wchar_t[64];
			std::wstring stFormat = L"%.";
			stFormat += std::to_wstring(precision);
			stFormat += L"f";
			std::swprintf(buffer, 64, stFormat.c_str(), value);
			std::wstring stTemp(buffer);
			unsigned int i = stTemp.size()-1;
			while (true)
			{
				if (i >= 1 && stTemp[i] == '0' && stTemp[i - 1] != '.')
					i--;
				else
					break;
			}
	        return stTemp.substr(0, i+1);
        };

        std::wstring ToString(int value) 
		{
	        return std::to_wstring(value);
        };

#ifdef _WIN64
		std::wstring ToString(unsigned int value) 
		{
	        return std::to_wstring(value);
        };
#endif

		std::wstring ToString(size_t value)
		{
			return std::to_wstring(value);
		};

        const std::wstring& ToString(const std::wstring& stValue) 
		{
	        return stValue;
        };

		std::string ToStringA(size_t value)
		{
			return std::to_string(value);
		}

		std::string ToStringA(int value)
		{
			return std::to_string(value);
		}

		std::string ToStringA(float value, unsigned int precision)
		{
			char* buffer = new char[64];
			std::string stFormat = "%.";
			stFormat += std::to_string(precision);
			stFormat += "f";
			sprintf_s(buffer, 64, stFormat.c_str(), value);
			std::string stTemp(buffer);
			unsigned int i = stTemp.size() - 1;
			while(true)
			{
				if(i >= 1 && stTemp[i] == '0' && stTemp[i - 1] != '.')
					i--;
				else
					break;
			}
			return stTemp.substr(0, i + 1);
		}

        /************************************************
        * String to POD
        ************************************************/

		void FromString(unsigned char* pOut, const wchar_t* lpValue)
		{
			*pOut = (unsigned char)_wtoi(lpValue);
		}

		void FromString(short* pOut, const wchar_t* lpValue)
        {
	        *pOut = (short)_wtoi(lpValue);
        }

		void FromString(unsigned short* pOut, const wchar_t* lpValue)
        {
	        *pOut = (unsigned short)_wtoi(lpValue);
        }

		void FromString(int* pOut, const wchar_t* lpValue)
        {
	        *pOut = _wtoi(lpValue);
        }

        void FromString(unsigned int* pOut, const wchar_t* lpValue)
        {
	        *pOut = _wtoi(lpValue);
        }

        void FromString(float* pOut, const wchar_t* lpValue)
        {
	        *pOut = (float)_wtof(lpValue);
        }

        void FromString(std::wstring* pOut, const wchar_t* lpValue)
        {
	        *pOut = lpValue;
        }

        void FromString(const wchar_t** lpOut, const wchar_t* lpValue)
        {
	        *lpOut = lpValue;
        }

		void FromString(bool* pOut, const wchar_t* lpValue)
		{
			*pOut = _wtoi(lpValue) != 0;
		}

		void FromString(unsigned char* pOut, const char* lpValue)
		{
			*pOut = (unsigned char)atoi(lpValue);
		}

		void FromString(short* pOut, const char* lpValue)
        {
	        *pOut = (short)atoi(lpValue);
        }

		void FromString(unsigned short* pOut, const char* lpValue)
        {
	        *pOut = (unsigned short)atoi(lpValue);
        }

		void FromString(int* pOut, const char* lpValue)
        {
	        *pOut = atoi(lpValue);
        }

		void FromString(unsigned int* pOut, const char* lpValue)
        {
	        *pOut = atoi(lpValue);
        }

        void FromString(float* pOut, const char* lpValue)
        {
	        *pOut = (float)atof(lpValue);
        }

        void FromString(std::string* pOut, const char* lpValue)
        {
	        *pOut = lpValue;
        }

        void FromString(const char** lpOut, const char* lpValue)
        {
	        *lpOut = lpValue;
        }

#ifdef _WIN64
		void FromString(size_t* pOut, const char* lpValue)
		{
			*pOut = atoi(lpValue);
		}
#endif

		void FromString(bool* pOut, const char* lpValue)
		{
			*pOut = atoi(lpValue) != 0;
		}

		/************************************************
        * Char to POD
        ************************************************/

		void FromChar(unsigned int* pOut, char value)
		{
			*pOut = value - '0';
		}
                
        /************************************************
        * UTF8 <-> Ansii
        ************************************************/

        std::string ToA(const std::wstring& stUnicode)
        {
            return std::string(stUnicode.begin(), stUnicode.end());
        }

        std::wstring ToW(const std::string& stAnsi)
        {
            return std::wstring(stAnsi.begin(), stAnsi.end());
        }

    }
}