#pragma once
#include "..\Core\Dll.h"
#include <string>

namespace acl
{
	namespace conv
	{

        /************************************************
        * POD to String
        ************************************************/

		ACCLIMATE_API std::wstring ToString(float value, unsigned int precision = 5);

#ifdef _WIN64
		ACCLIMATE_API std::wstring ToString(unsigned int value);
#endif

		ACCLIMATE_API std::wstring ToString(size_t value);

		ACCLIMATE_API std::wstring ToString(int value);

		ACCLIMATE_API std::string ToStringA(size_t value);

		ACCLIMATE_API std::string ToStringA(int value);

		ACCLIMATE_API std::string ToStringA(float value, unsigned int precision = 5);

		ACCLIMATE_API const std::wstring& ToString(const std::wstring& stValue);

        /************************************************
        * String to POD
        ************************************************/

		ACCLIMATE_API void FromString(unsigned char* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(short* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(unsigned short* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(int* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(unsigned int* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(float* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(std::wstring* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(const wchar_t** lpOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(bool* pOut, const wchar_t* lpValue);

		ACCLIMATE_API void FromString(unsigned char* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(short* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(unsigned short* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(int* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(unsigned int* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(float* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(std::string* pOut, const char* lpValue);

		ACCLIMATE_API void FromString(const char** lpOut, const char* lpValue);

#ifdef _WIN64
		ACCLIMATE_API void FromString(size_t* pOut, const char* lpValue);
#endif

		ACCLIMATE_API void FromString(bool* pOut, const char* lpValue);

		template<typename T>
		T FromString(const wchar_t* lpValue)
		{
			T data;
			FromString(&data, lpValue);
			return data;
		}

		template<typename T>
		T FromString(const std::wstring& stValue)
		{
			T data;
			FromString(&data, stValue.c_str());
			return data;
		}

		template<typename T>
		T FromString(const std::string& stValue)
		{
			T data;
			FromString(&data, stValue.c_str());
			return data;
		}

		template<typename T>
		T FromString(const char* lpValue)
		{
			T data;
			FromString(&data, lpValue);
			return data;
		}

		/************************************************
        * Char to POD
        ************************************************/

		ACCLIMATE_API void FromChar(unsigned int* pOut, char value);

        /************************************************
        * UTF8 <-> Ansii
        ************************************************/

        ACCLIMATE_API std::string ToA(const std::wstring& stUnicode);

        ACCLIMATE_API std::wstring ToW(const std::string& stAnsi);

	}
}