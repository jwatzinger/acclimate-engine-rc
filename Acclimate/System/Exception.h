#pragma once
#include <exception>
#include <string>

namespace acl
{

	class AclException : public std::exception 
	{
	};

	//low level exeption

	class llException : public AclException
	{
	};

	class apiException : public llException
	{
	};

	//mid level exceptions

	class mlException : public AclException
	{
	};

	class resourceException : public mlException
	{
	};

	class resourceLoadException : public resourceException
	{
	public: 
		class resourceLoadException(const std::wstring& fileName): m_string(fileName)
		{
		};

	private:
		std::wstring m_string;
	};

	class audioException : public mlException
	{
	};

	//high level exceptions

	class hlException : public AclException
	{
	};

	class fileException : public hlException
	{
	};

}