#include "Int128.h"

namespace acl
{
	namespace sys
	{

		Int128::Int128(void) : m_left(0), m_right(0)
		{
		}

		Int128::Int128(unsigned long long left, unsigned long long right) : m_left(left), m_right(right)
		{
		}

		bool Int128::operator==(const Int128& other) const
		{
			return m_left == other.m_left && m_right == other.m_right;
		}

		bool Int128::operator<(const Int128& other) const
		{
			return m_right < other.m_right || ((m_right <=other.m_right) && m_left < other.m_left);
		}

		bool Int128::operator>=(const Int128& other) const
		{
			return m_right >= other.m_right || m_left >= other.m_left;
		}

		void Int128::Add(unsigned int pos, unsigned int length, unsigned long long value)
		{
			for(unsigned int i = 0; i < length; i++)
			{
				const unsigned int realPos = i + pos;

				unsigned long long* pValue;
				if(realPos < 64)
					pValue = &m_left;
				else
					pValue = &m_right;

				const bool bit = (value & (1ULL << i)) != 0;
				if(bit)
					*pValue |= 1ULL << (realPos % 64);
				else
					*pValue &= ~(1ULL << (realPos % 64));
			}
		}

	}
}