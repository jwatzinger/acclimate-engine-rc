#pragma once

namespace acl
{
	namespace sys
	{

		class Int128
		{
		public:

			Int128(void);
			Int128(unsigned long long left, unsigned long long right);

			bool operator==(const Int128& other) const;

			bool operator<(const Int128& other) const;
			bool operator>=(const Int128& other) const;

			void Add(unsigned int pos, unsigned int length, unsigned long long value);

		private:

			unsigned long long m_left, m_right;
		};

	}
}

