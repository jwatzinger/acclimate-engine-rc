#include "Log.h"
#include "Console.h"
#include "Convert.h"

namespace acl
{
    namespace sys
    {
        Log::Log(void)
        {
            m_file.open(L"../Output.txt", std::ios::out | std::ios::trunc);
            m_file << "Initializing application!\n";

#ifdef _DEBUG
            redirectIOToConsole();
#endif
        }

        Log::~Log(void)
        {
#ifdef _DEBUG
			freeConsole();
#endif
            m_file.close();
        }

		void Log::OutLog(const std::wstring& out)
		{
			OutLog(conv::ToA(out));
		}

		Log* Log::m_pInstance = nullptr;
    }
}
