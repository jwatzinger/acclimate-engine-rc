#pragma once
#include <fstream>
#include <iostream>
#include "..\Core\Dll.h"

namespace acl
{
    namespace sys
    {

#define log Log::instance()

		enum class LogType
		{
			NONE,
			INFO,
			WARNING,
			ERR,
		};

		enum class LogModule
		{
			API = 0, AUDIO, CORE, ENTITY, FILE, GFX, GUI, INPUT, MATH, NETWORK, PHYSICS, RENDER, SCRIPT, SYSTEM, XML, PLUGIN, EDITOR, CUSTOM
		};

		/// Global logger class.
		/** This class acts as a singleton logger. Depending on the selected compile mode, it can output
		*	information to a console window and/or a text file. It will create a console window on first
		*	use itself. */
        class ACCLIMATE_API Log
        {
        public:
            ~Log(void);

			/** Access singleton instance.
			 *	Returns the only available instance of the logger class. This instance is initiated on first call
			 *	of this function.
			 *
			 *	@return Pointer to the Log instance.
			 */
			inline static Log* instance(void)
			{
			   static Guard g;
			   if (!m_pInstance)
				  m_pInstance = new Log;
			   return m_pInstance;
			}

			/** Outputs a number of values to the logger.
             *  Requires at least one value to be passed, while an arbitrary number of arguments
			 *	is accepted. Input arguments may be of any type, however they must be able to being
			 *	converted to string using an overload of the conv::ToString()-function.
			 *
             *  @param[in] out First and required value to log.
			 *
			 *	@tparam OutType Type of the first output value.
			 *	@tparam Args Varidic template for optional further logging values.
			 */
            template<typename OutType, typename... Args>
			void Out(LogModule module, LogType type, const OutType& out, Args&&... args);

        private:
#pragma warning( disable: 4251 )
            Log(void);
            Log(const Log&) {}
            Log & operator=(const Log &) { return * this;} 
            
            template<typename OutType, typename OutType2, typename... Args>
            void OutLog(const OutType& out, const OutType2& out2, Args&&... args);

            template<typename OutType>
            void OutLog(const OutType& out);
			void OutLog(const std::wstring& out);

			static Log* m_pInstance;

            std::ofstream m_file;

			class Guard
			{
			public:
			   ~Guard(void)
			   {
				  if(Log::m_pInstance)
				  {
					 delete Log::m_pInstance;
					 Log::m_pInstance = nullptr;
				  }
			   }
			};
			friend class Guard;
#pragma warning( default: 4251 )
        };

        template<typename OutType, typename... Args>
		void Log::Out(LogModule module, LogType type, const OutType& out, Args&&... args)
        {
			const char* pModules[] = { "API", "AUDIO", "CORE", "ENTITY", "FILE", "GFX", "GUI", "INPUT", "MATH", "NETWORK", "PHYSICS", "RENDER", "SCRIPT", "SYSTEM", "XML", "PLUGIN", "EDITOR", "" };

			const char* pType = nullptr;
			switch(type)
			{
			case LogType::NONE:
				pType = "";
				break;
			case LogType::INFO:
				pType = "Info:";
				break;
			case LogType::WARNING:
				pType = "Warning:";
				break;
			case LogType::ERR:
				pType = "Error:";
				break;
			}

			OutLog(pModules[(int)module], pType, out, args...);
            OutLog("\n");
        }

        template<typename OutType, typename OutType2, typename... Args>
        void Log::OutLog(const OutType& out, const OutType2& out2, Args&&... args)
        {
            OutLog(out);
            OutLog(" ");
            OutLog(out2, args...);
        }

        template<typename OutType>
        void Log::OutLog(const OutType& out)
        {
#ifdef _DEBUG
            std::cout << out;
#endif
			m_file << out << std::flush;
        }

    }
}