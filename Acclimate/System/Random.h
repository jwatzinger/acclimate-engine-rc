#pragma once
#include <random>

namespace acl
{
	namespace rnd
	{
		/************************************
		* FLOAT
		*************************************/

		//returns a random float in range of min to max
		inline float fRange(float min, float max)
		{
			return min + (rand()/(float)RAND_MAX) * (max - min);
		}

		//returns a random float in range of min to min + dist
		inline float fDist(float min, float dist)
		{
			return min + (rand()/(float)RAND_MAX) * dist;
		}

		//returns a random float from range 0.0f to max
		inline float fMax(float max)
		{
			return (rand()/(float)RAND_MAX) * max;
		}

		/************************************
		* INTEGER
		*************************************/

		//returns a random int from range 0 to max
		inline unsigned int iMax(unsigned int max)
		{
			return rand() % (max+1);
		}

		//returns a random int from range 0 to max
		inline int iRange(int min, int max)
		{
			return min + iMax(max-min);
		}

		/************************************
		* BOOL
		*************************************/

		//returns eigther true or false
		inline bool boolean(void) 
		{
			return (rand() % 2) != 0;
		}
	}
}

