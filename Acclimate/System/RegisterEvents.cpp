#include "RegisterEvents.h"
#include "Convert.h"
#include "Log.h"
#include "Random.h"
#include "..\Core\EventLoader.h"
#include "..\Core\EventDeclaration.h"

namespace acl
{
	namespace sys
	{

		/*****************************************
		* Condition
		******************************************/

		void registerEvents(void)
		{
			// events
			core::EventLoader::RegisterEvent<LogInt>();
			core::EventLoader::RegisterEvent<LogString>();

			// functions
			core::EventLoader::RegisterFunction<RandomInt>();
			core::EventLoader::RegisterFunction<RandomFloat>();
			core::EventLoader::RegisterFunction<RandomBool>();
		}

		/*****************************************
		* ----------------------------------------
		* EVENTS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* LogInt
		******************************************/

		void LogInt::Reset(void)
		{
		}

		void LogInt::OnExecute(double dt)
		{
			sys::log->Out(sys::LogModule::CUSTOM, sys::LogType::INFO, conv::ToString(GetAttribute<int>(0)));

			CallOutput(0);
		}

		const core::EventDeclaration& LogInt::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"", core::AttributeType::INT, false }
			};

			static const core::EventDeclaration decl(L"LogInt", vAttributes, core::AttributeVector());

			return decl;
		}

		/*****************************************
		* LogInt
		******************************************/

		void LogString::Reset(void)
		{
		}

		void LogString::OnExecute(double dt)
		{
			sys::log->Out(sys::LogModule::CUSTOM, sys::LogType::INFO, conv::ToA(GetAttribute<std::wstring>(0)));

			CallOutput(0);
		}

		const core::EventDeclaration& LogString::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"", core::AttributeType::STRING, false }
			};

			static const core::EventDeclaration decl(L"LogString", vAttributes, core::AttributeVector());

			return decl;
		}

		/*****************************************
		* ----------------------------------------
		* FUNCTIONS
		*-----------------------------------------
		******************************************/


		/*****************************************
		* RandomInt
		******************************************/

		void RandomInt::OnCalculate(void)
		{
			ReturnValue<int>(0, rnd::iRange(GetAttribute<int>(0), GetAttribute<int>(1)));
		}

		const core::FunctionDeclaration& RandomInt::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"min", core::AttributeType::INT, false },
				{ L"max", core::AttributeType::INT, false }
			};

			const core::AttributeVector vReturns =
			{
				{ L"", core::AttributeType::INT, false }
			};

			static const core::FunctionDeclaration decl(L"RandomInt", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* RandomFloat
		******************************************/

		void RandomFloat::OnCalculate(void)
		{
			ReturnValue<float>(0, rnd::fRange(GetAttribute<float>(0), GetAttribute<float>(1)));
		}

		const core::FunctionDeclaration& RandomFloat::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"min", core::AttributeType::FLOAT, false },
				{ L"max", core::AttributeType::FLOAT, false }
			};

			const core::AttributeVector vReturns =
			{
				{ L"", core::AttributeType::FLOAT, false }
			};

			static const core::FunctionDeclaration decl(L"RandomFloat", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* RandomBool
		******************************************/

		void RandomBool::OnCalculate(void)
		{
			ReturnValue<bool>(0, rnd::boolean());
		}

		const core::FunctionDeclaration& RandomBool::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
			};

			const core::AttributeVector vReturns =
			{
				{ L"", core::AttributeType::BOOL, false }
			};

			static const core::FunctionDeclaration decl(L"RandomBool", vAttributes, vReturns);

			return decl;
		}

	}
}