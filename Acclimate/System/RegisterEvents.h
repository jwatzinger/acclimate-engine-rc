#pragma once
#include "..\Core\Event.h"
#include "..\Core\EventFunction.h"

namespace acl
{
	namespace sys
	{

		void registerEvents(void);

		/*****************************************
		* ----------------------------------------
		* EVENTS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* LogInt
		******************************************/

		class LogInt :
			public core::BaseEvent<LogInt>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* LogString
		******************************************/

		class LogString :
			public core::BaseEvent<LogString>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* ----------------------------------------
		* FUNCTIONS
		*-----------------------------------------
		******************************************/

		/*****************************************
		* RandomInt
		******************************************/

		class RandomInt :
			public core::BaseEventFunction<RandomInt>
		{
		public:

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* RandomFloat
		******************************************/

		class RandomFloat :
			public core::BaseEventFunction<RandomFloat>
		{
		public:

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* RandomBool
		******************************************/

		class RandomBool :
			public core::BaseEventFunction<RandomBool>
		{
		public:

			void OnCalculate(void) override;

			static const core::FunctionDeclaration& GenerateDeclaration(void);
		};

	}
}