#include "RegisterScript.h"
#include <string>
#include "Random.h"
#include "Convert.h"
#include "..\Script\Core.h"

namespace acl
{
	namespace sys
	{
		
		/*****************************************
		* String
		*****************************************/

		std::wstring StringFactory(unsigned int byteLength, const wchar_t *s)
		{
			return std::wstring(s, byteLength / 2);
		}

		void ConstructString(void* memory)
		{
			new(memory)std::wstring();
		}

		void ConstructStringCopy(const std::wstring& string, void* memory)
		{
			new(memory)std::wstring(string);
		}

		void DestructString(void* memory)
		{
			((std::wstring*)memory)->~basic_string();
		}

		std::wstring& AssignString(const std::wstring& stRight, std::wstring* stLeft)
		{
			return *stLeft = stRight;
		}

		std::wstring AddString(const std::wstring& stRight, const std::wstring* stLeft)
		{
			return *stLeft + stRight;
		}

		bool compare(const std::wstring* stRight, const std::wstring& stLeft) //*JW
		{
			return (*stRight==stLeft);
		}

		bool contains(const std::wstring* stRight, const std::wstring& stLeft)
		{
			return stRight->find(stLeft) != std::wstring::npos;
		}

		/*****************************************
		* Random
		*****************************************/

		float random(void)
		{
			return rnd::fMax(1.0f);
		}

		/*****************************************
		* Log
		*****************************************/

		void scriptLog(int out)
		{
			log->Out(LogModule::CUSTOM, LogType::NONE, out);
		}

		void scriptLog(float out)
		{
			log->Out(LogModule::CUSTOM, LogType::NONE, out);
		}

		void scriptLog(const std::wstring &stOut)
		{
			log->Out(LogModule::CUSTOM, LogType::NONE, conv::ToA(stOut));
		}

		void RegisterScript(script::Core& script)
		{
			// string
			auto string = script.RegisterValueType<std::wstring>("string", script::VALUE_CLASS | script::VALUE_CLASS_CONSTRUCTOR | script::VALUE_CLASS_COPY_CONSTRUCTOR | script::VALUE_CLASS_DESTRUCTOR);

			script.RegisterStringFactory("string", asFUNCTION(StringFactory));

			string.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f()", asFUNCTION(ConstructString), asCALL_CDECL_OBJLAST);
			string.RegisterBehaviour(asBEHAVE_CONSTRUCT, "void f(const string& in)", asFUNCTION(ConstructStringCopy), asCALL_CDECL_OBJLAST);
			string.RegisterBehaviour(asBEHAVE_DESTRUCT, "void f()", asFUNCTION(DestructString), asCALL_CDECL_OBJLAST);
			string.RegisterMethod("string& opAssign(const string& in)", asFUNCTION(AssignString), asCALL_CDECL_OBJLAST);
			string.RegisterMethod("string opAdd(const string& in) const", asFUNCTION(AddString), asCALL_CDECL_OBJLAST);
			string.RegisterMethod("string& opAddAssign(const string& in)", asMETHODPR(std::wstring, operator+=, (const std::wstring&), std::wstring&));
			string.RegisterMethod("bool opEquals(const string& in) const", asFUNCTION(compare), asCALL_CDECL_OBJFIRST);
			string.RegisterMethod("bool contains(const string& in) const", asFUNCTION(contains), asCALL_CDECL_OBJFIRST);

			// random
			script.RegisterGlobalFunction("uint rand(uint)", asFUNCTION(rnd::iMax));
			script.RegisterGlobalFunction("float rand()", asFUNCTION(random));
			script.RegisterGlobalFunction("float randRange(float, float)", asFUNCTION(rnd::fRange));

			// convert
			script.RegisterGlobalFunction("string toString(int)", asFUNCTIONPR(conv::ToString, (int), std::wstring));
			script.RegisterGlobalFunction("string toString(float, uint)", asFUNCTIONPR(conv::ToString, (float, unsigned int), std::wstring));

			// log
			script.RegisterGlobalFunction("void log(int)", asFUNCTIONPR(scriptLog, (int), void));
			script.RegisterGlobalFunction("void log(float)", asFUNCTIONPR(scriptLog, (float), void));
			script.RegisterGlobalFunction("void log(const string& in)", asFUNCTIONPR(scriptLog, (const std::wstring&), void));

			script.RegisterGlobalFunction("void quit()", asFUNCTION(exit));
		}

	}
}


