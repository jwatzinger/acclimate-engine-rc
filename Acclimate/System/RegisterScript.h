#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace sys
	{

		void RegisterScript(script::Core& script);

	}
}


