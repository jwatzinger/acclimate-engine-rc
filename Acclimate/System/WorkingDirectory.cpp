#include "WorkingDirectory.h"
#include <Windows.h>
#include "Log.h"
#include "..\File\File.h"

namespace acl
{
	namespace sys
	{

		const unsigned int BUFFER_SIZE = 512;

		WorkingDirectory::WorkingDirectory(void)
		{
			const auto& stDirectory = getDirectory();
			m_stDirectory = stDirectory;
			m_stOldDirectory = stDirectory;
		}

		WorkingDirectory::WorkingDirectory(const std::wstring& stTargetDir, bool isFilePath)
		{
			m_stOldDirectory = getDirectory();

			if(isFilePath)
				m_stDirectory = file::FullPath(file::FilePath(stTargetDir));
			else
				m_stDirectory = file::FullPath(stTargetDir);
			setDirectory(m_stDirectory.c_str());
		}

		WorkingDirectory::WorkingDirectory(WorkingDirectory&& directory):
			m_stDirectory(std::move(directory.m_stDirectory)),
			m_stOldDirectory(std::move(directory.m_stOldDirectory))
		{

		}

		WorkingDirectory::~WorkingDirectory(void)
		{
			if(!m_stOldDirectory.empty())
				setDirectory(m_stOldDirectory.c_str());
		}

		WorkingDirectory& WorkingDirectory::operator=(WorkingDirectory&& directory)
		{
			if(&directory != this)
			{
				m_stDirectory = std::move(directory.m_stDirectory);
				directory.m_stOldDirectory.clear();
			}

			return *this;
		}

		void WorkingDirectory::Restore(void)
		{
			if(!m_stOldDirectory.empty())
			{
				setDirectory(m_stOldDirectory.c_str());
				m_stOldDirectory.clear();
			}
		}

		bool WorkingDirectory::IsActive(void) const
		{
			return !m_stOldDirectory.empty();
		}

		const std::wstring& WorkingDirectory::GetOldDirectory(void) const
		{
			return m_stOldDirectory;
		}

		const std::wstring& WorkingDirectory::GetDirectory(void) const
		{
			return m_stDirectory;
		}

		bool setDirectory(const std::wstring& stPath)
		{
			if(SetCurrentDirectory(stPath.c_str()))
				return true;
			else
			{
				log->Out(LogModule::SYSTEM, LogType::WARNING, "Failed to set current working directory to", stPath, ".");
				return false;
			}
		}

		std::wstring getDirectory(void)
		{
			wchar_t dir[512];
			GetCurrentDirectory(512, dir);
			return dir;
		}


	}
}