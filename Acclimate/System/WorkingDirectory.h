#pragma once
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
	namespace sys
	{

		class ACCLIMATE_API WorkingDirectory
		{
		public:

			WorkingDirectory(void);
			WorkingDirectory(const std::wstring& stTargetDir, bool isFilePath = false);
			WorkingDirectory(WorkingDirectory&& directory);
			~WorkingDirectory(void);

			WorkingDirectory& operator=(WorkingDirectory&& directory);

			void Restore(void);

			bool IsActive(void) const;
			const std::wstring& GetOldDirectory(void) const;
			const std::wstring& GetDirectory(void) const;

		private:
#pragma warning( disable: 4251 )

			WorkingDirectory(const WorkingDirectory& directory) {};
			WorkingDirectory& operator=(const WorkingDirectory& directory) { return *this; }

			std::wstring m_stDirectory, m_stOldDirectory;

#pragma warning( default: 4251 )
		};

		ACCLIMATE_API bool setDirectory(const std::wstring& stPath);
		ACCLIMATE_API std::wstring getDirectory(void);

	}
}