#pragma once
#include <map>
#include "Math\AABB.h"
#include "Math\Vector3.h"

namespace acl
{
    namespace util
    {

        template<typename T>
        class Node
        {
	        typedef std::map<math::IVolume*, T> dataMap;

        public:
	
	        static const int MAX_DEPTH = 5;

	        Node(const math::AABB& box, int depth, Node* pParent) : m_box(box), m_looseBox(box.m_vCenter, box.m_size), m_depth(depth), m_pParent(pParent), 
                m_pNodes(nullptr), m_bSplit(false), m_numActiveNodes(0) 
	        { 
                //only create children up to maximum depth
		        if(m_depth < MAX_DEPTH)
			        CreateChildren();
	        }

	        ~Node(void) {
		        if(m_pNodes)
		        {
			        //free memory
			        for(unsigned int i = 0; i<8; i++)
			        {
				        delete m_pNodes[i];
			        }
			        delete[] m_pNodes;
			        m_pNodes = nullptr;
		        }
	        }

	        void InsertData(math::IVolume& volume, T data)
	        {
		        m_mData[&volume] = data;
		        //check if node needs to be split
		        if(m_mData.size() > 1 && m_depth < MAX_DEPTH)
		        {
			        //set status to split
			        m_bSplit = true;
			        //todo: make existing data be checked for needing to get inserted deeper once a node is split
		        }
		        //check if node has become active
		        else if(m_mData.size() == 1)
		        {
			        //expand border
			        m_looseBox.m_size = m_box.m_size*2;
			        //tell parent node is active
			        if(m_pParent)
				        m_pParent->AddActiveNode();
		        }
	        }

	        void RemoveData(math::IVolume& volume)
	        {
		        m_mData.erase(m_mData.find(&volume));
		        CheckNodeStatus();
	        }

	        void Intersect(const math::Ray& ray, dataMap& mData)
	        {
		        bool bIntersect = m_looseBox.Intersect(ray);

		        //skip if no intersection with box
		        if(!bIntersect)
			        return;

		        //store data if node has any
		        if(!m_mData.empty())
			        mData.insert(m_mData.begin(), m_mData.end());

		        if(IsSplit())
		        {
			        //test for child node intersection
			        for(unsigned int i = 0; i<8; i++)
			        {
				        m_pNodes[i]->Intersect(ray, mData);
			        }
		        }
	        }

	        bool IsSplit(void) const
	        {
		        return m_bSplit;
	        }

	        Node** Children(void) const
	        {
		        return m_pNodes;
	        }

	        const math::AABB& GetBox(void) const
	        {
		        return m_box;
	        }

	        const math::AABB& GetLooseBox(void) const
	        {
		        return m_looseBox;
	        }

	        const dataMap& GetData(void) const
	        {
		        return m_mData;
	        }

	        void Clear(void) 
	        {
                //remove data
		        m_mData.clear();
                //reset loose box border
		        m_looseBox.m_size = m_box.m_size;
		        if(IsSplit())
		        {
                    //clear all child nodes
			        for(int i = 0; i < 8; i++)
			        {
				        m_pNodes[i]->Clear();
			        }
                    //set to leaf
			        m_bSplit = false;
		        }
	        }
        private:

	        void CreateChildren(void) 
	        {
		        //create child Node array
		        m_pNodes = new Node*[8];
			
		        //child bounding box size
		        float halfsize = m_box.m_size / 2;
		        //center of the first new bounding box
		        const math::Vector3 vNewCenter(m_box.m_vCenter.x - halfsize, m_box.m_vCenter.y - halfsize, m_box.m_vCenter.z - halfsize);
		        //create first bounding box
		        math::AABB octBox(vNewCenter, halfsize);
			
		        //create 8 child nodes
		        for(unsigned int i = 0; i<8; i++)
		        {
			        m_pNodes[i] = new Node<T>(GetOct(octBox, i, halfsize*2), m_depth+1, this);
		        }
	        }

	        void AddActiveNode(void)
	        {
                //increment active nodes
		        m_numActiveNodes += 1;
	        }

	        void RemoveActiveNode(void)
	        {
                //decrement active node
		        m_numActiveNodes -= 1;
                //check for new status of node
		        CheckNodeStatus();
	        }

	        void CheckNodeStatus(void)
	        {
		        //check if node still needs to be split
		        if(m_mData.size() <= 1 && m_numActiveNodes == 0)
		        {
			        //check if node still contains any data
			        if(m_mData.empty())
			        {
				        //reset loose borders
				        m_looseBox.m_size = m_box.m_size;
				        //tell parent node becomes inactive
				        if(m_pParent)
					        m_pParent->RemoveActiveNode();
			        }
			        m_bSplit = false;
		        }
	        }

	        //recursively constructs the num child leaves bounding box
	        const math::AABB& GetOct(math::AABB& targetBox, int num, float size) const
	        {
		        switch(num)
		        {
		        case 1:
			        targetBox.m_vCenter.x += size;
			        break;
		        case 2:
			        targetBox.m_vCenter.x -= size;
			        targetBox.m_vCenter.z += size;
			        break;
		        case 3:
			        targetBox.m_vCenter.x += size;
			        break;
		        case 4:
			        targetBox.m_vCenter.x -= size;
			        targetBox.m_vCenter.z -= size;
			        targetBox.m_vCenter.y += size;
			        break;
		        case 5:
			        targetBox.m_vCenter.x += size;
			        break;
		        case 6:
			        targetBox.m_vCenter.x -= size;
			        targetBox.m_vCenter.z += size;
			        break;
		        case 7:
			        targetBox.m_vCenter.x += size;
			        break;
		        }

		        return targetBox;
	        }

	        int m_depth, m_numActiveNodes;
	        bool m_bSplit;

	        math::AABB m_box;
	        math::AABB m_looseBox;
	        Node* m_pParent;
	        dataMap m_mData;
	        Node** m_pNodes;

        };

    }
}