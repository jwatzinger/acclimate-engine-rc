#pragma once
#include "Node.h"
#include <algorithm>
#include "Math\IVolume.h"
#include "Math\Vector3.h"

namespace acl
{
    namespace util
    {

        template<typename T>
        class Octree
        {
	        typedef std::map<math::IVolume*, Node<T>*> nodeMap;

        public:
	        Octree(float size): m_size(size), m_node(AABB(0.0f, 0.0f, 0.0f, m_size, m_size, m_size), 0, nullptr) {};

	        void InsertNode(math::IVolume& volume, T data){
		        //acccess volume and octree max size
		        float volumeSize = volume.MaxExtents(), treeSize = m_size;
		        int depth = 0;

		        //until node box is smaller than the volume
		        while(treeSize > volumeSize) 
		        {
			        treeSize /= 2;
			        depth += 1;
			        //break on max depth
			        if(depth > Node<T>::MAX_DEPTH)
				        break;
		        }
		        //adjust depth and size
		        depth -= 1;
		        treeSize *= 2;

		        //find node
		        Node<T>* pNode = FindFittingNode(volume, treeSize, depth);

				if(pNode)
				{
					//insert data into node
					pNode->InsertData(volume, data);

					//link volume and node for update
					m_mNodeData[&volume] = pNode;
				}
 	        };

	        void RemoveData(math::IVolume& volume)
	        {
		        m_mNodeData[&volume]->RemoveData(volume);
		        m_mNodeData.erase(&volume);
	        }

	        void UpdateData(math::IVolume& volume, T data)
	        {
		        m_mNodeData[&volume]->RemoveData(volume);
		        InsertNode(volume, data);
	        }

	        void Intersect(const math::Ray& ray, std::map<math::IVolume*, T>& mData)
	        {
		        //intersection with parent box
		        m_node.Intersect(ray, mData);
	        }

	        const Node<T>& GetRoot(void) const
	        {
		        return m_node;
	        }

	        void Clear(void)
	        {
                //remove nodes
		        m_node.Clear();
                //remove linked data
		        m_mNodeData.clear();
	        }

        private:

	        Node<T>* FindFittingNode(const math::IVolume& volume, float treeSize, unsigned int depth)
	        {
		        math::Vector3 vCenter(volume.m_vCenter.x + m_size, volume.m_vCenter.y + m_size, volume.m_vCenter.z + m_size);

				if(abs(vCenter.x) > treeSize || abs(vCenter.y) > treeSize || abs(vCenter.z) > treeSize)
					return nullptr;

		        //calculate absolut position
		        unsigned int x = (unsigned int)(vCenter.x / treeSize);
		        unsigned int y = (unsigned int)(vCenter.y / treeSize);
		        unsigned int z = (unsigned int)(vCenter.z / treeSize);
		        Node<T>* pNode = &m_node;
		        //iteratively choose next child node
		        for(unsigned int i = 0; i != depth; i++)
		        {
			        //pick node if it isn't split
			        if(!pNode->IsSplit())
				        break;

			        unsigned int currentDepth = depth - i;
			        //calculate position at current depth
			        unsigned int currentDepthX = x >> currentDepth;
                    unsigned int currentDepthY = y >> currentDepth;
                    unsigned int currentDepthZ = z >> currentDepth;
			        //calculate childs index
                    unsigned int childIndex = currentDepthX + (currentDepthZ << 1) + (currentDepthY << 2);
			        //get new node
			        pNode = pNode->Children()[childIndex];
			        //modify absolut positions to new child
			        x -= currentDepthX << currentDepth;
			        y -= currentDepthY << currentDepth;
			        z -= currentDepthZ << currentDepth;																																												
		        }

		        return pNode;
	        }

	        float m_size;
	        Node<T> m_node;
	        nodeMap m_mNodeData;
	
        };

    }
}
