#pragma once
#include <vector>
#include "..\Gfx\Camera.h"
#include "..\Math\AABB.h"
#include "..\Math\Rectf.h"
#include "..\Math\Vector2f.h"
#include "..\System\Assert.h"

namespace acl
{
	namespace util
	{

		template<typename Data>
		class Quadtree
		{
		public:

			typedef std::vector<Data> DataVector;

#pragma warning( disable: 4351 )
			Quadtree(math::Vector2f position, int size, int minSize) : m_boundingBox(),
				m_children(), m_boundingRect(position.x, position.y, (float)size, (float)size)
			{
				const int halfSize = size / 2;

				const math::Vector3 center(position.x + halfSize, 0.0f, position.y + halfSize);
				m_boundingBox.SetCenter(center);

				// todo: eigther caculate correct "height" of each quadtree element, or write specific class for 2d-camera culling
				m_boundingBox.m_vSize = math::Vector3((float)halfSize, 9999.0f, (float)halfSize);

				if(halfSize >= minSize)
				{
					m_children[0] = new Quadtree(position, halfSize, minSize);

					math::Vector2f newPos;
					newPos.x = position.x + halfSize;
					newPos.y = position.y;
					m_children[1] = new Quadtree(newPos, halfSize, minSize);

					newPos.y = position.y + halfSize;
					m_children[2] = new Quadtree(newPos, halfSize, minSize);

					newPos.x = position.x;
					m_children[3] = new Quadtree(newPos, halfSize, minSize);
				}

				ACL_ASSERT(m_boundingBox.m_vSize.x == m_boundingBox.m_vSize.z);
			}
#pragma warning( default: 4351)

			~Quadtree(void)
			{
				for(unsigned int i = 0; i < 4; i++)
				{
					delete m_children[i];
				}
			}

			bool Insert(Data data, const math::Rectf& rect)
			{
				if(m_boundingRect.Inside(rect))
				{
					// insert child if this is the last node in chain,
					// or the item to be inserted is too big to be fit into the children
					if(IsLeave() ||
						rect.width > m_boundingBox.m_vSize.x || rect.height > m_boundingBox.m_vSize.z)
					{
						m_vData.push_back(data);
						return true;
					}

					for(unsigned int i = 0; i < 4; i++)
					{
						if(m_children[i]->Insert(data, rect))
							return true;
					}

					return false;
				}
				else
					return false;
			}

			bool Remove(Data data)
			{
				auto itr = std::find(m_vData.begin(), m_vData.end(), data);
				if(itr != m_vData.end())
				{
					m_vData.erase(itr);
					return true;
				}
				else
				{
					for(unsigned int i = 0; i < 4; i++)
					{
						if(m_children[i]->Remove(data))
							return true;
					}

					return false;
				}
			}

			void GetVisibleData(const gfx::Camera& camera, DataVector& vOutData) const
			{
				if(camera.GetFrustum().TestVolume(m_boundingBox))
				{
					if(!m_vData.empty())
						vOutData.insert(vOutData.end(), m_vData.begin(), m_vData.end());
					
					if(m_children[0])
					{
						for(unsigned int i = 0; i < 4; i++)
						{
							m_children[i]->GetVisibleData(camera, vOutData);
						}
					}
				}
			}

		private:

			bool IsLeave(void) const
			{
				return m_vData.size() > 0 || !m_children[0];
			}

			DataVector m_vData;

			math::AABB m_boundingBox;
			math::Rectf m_boundingRect;

			Quadtree* m_children[4];
		};

	}
}