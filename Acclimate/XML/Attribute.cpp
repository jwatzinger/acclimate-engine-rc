#include "Attribute.h"
#include <codecvt>

namespace acl
{
    namespace xml
    {
		namespace
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;
		}

        Attribute::Attribute(void)
        {
        }

        Attribute::Attribute(const std::wstring& stName, const std::wstring& stValue): m_stName(stName), m_stValue(stValue)
        {
        }

		bool Attribute::operator==(const Attribute& attribute) const
		{
			return m_stName == attribute.m_stName && m_stValue == attribute.m_stValue;
		}

		bool Attribute::operator!=(const Attribute& attribute) const
		{
			return m_stName != attribute.m_stName || m_stValue != attribute.m_stValue;
		}

        void Attribute::Save(std::fstream& fOut) const
        {
	        fOut.put(' ');
			const auto& stName = conversion.to_bytes(m_stName);
			fOut.write(stName.c_str(), stName.size());
	        fOut.put('=');
	        fOut.put('"');
			const auto& stValue = conversion.to_bytes(m_stValue);
			fOut.write(stValue.c_str(), stValue.size());
	        fOut.put('"');
        }

        void Attribute::SetValue(const std::wstring& stValue)
        {
	        m_stValue = stValue;
        }

        const std::wstring& Attribute::GetName(void) const
        {
	        return m_stName;
        }

        const std::wstring& Attribute::GetValue(void) const
        {
	        return m_stValue;
        }

        bool Attribute::AsBool(void) const
        {
            return AsInt() != 0;
        }

        int Attribute::AsInt(void) const
        {
	        return _wtoi(m_stValue.c_str());
        }

        float Attribute::AsFloat(void) const
        {
	        return (float)_wtof(m_stValue.c_str());
        }

        double Attribute::AsDouble(void) const
        {
	        return _wtof(m_stValue.c_str());
        }

		Attribute::operator const std::wstring&(void) const
		{
			return m_stValue;
		}

    }
}