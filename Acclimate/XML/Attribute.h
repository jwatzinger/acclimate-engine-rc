/*
* Acclimate Engine: XML
*
* Created by Julian Watzinger
*/

#pragma once
#include <fstream>
#include <string>
#include "..\Core\Dll.h"

namespace acl
{
    namespace xml
    {
		/// The xml attribute class
		/** Represents an attribute from a XML node in a DOM tree. It has a name and a value,
			and can be serialized. */
        class ACCLIMATE_API Attribute
        {
        public:
	        Attribute(void);
	        Attribute(const std::wstring& stName, const std::wstring& stValue);

			bool operator==(const Attribute& attribute) const;
			bool operator!=(const Attribute& attribute) const;

			/** Serializes the attribute to a file stream.
			 *
             *  @param[in] fOut Filestream to write to.
			 */
	        void Save(std::fstream& fOut) const;

			/** 
			 * Set value
			 */
	        void SetValue(const std::wstring& stValue);

			/** 
			 * Get name
			 */
	        const std::wstring& GetName(void) const;
			/** 
			 * Get value
			 */
	        const std::wstring& GetValue(void) const;
	
			/** 
			 * Get value as bool
			 */
            bool AsBool(void) const;

			/** 
			 * Get value as int
			 */
	        int AsInt(void) const;

			/** 
			 * Get value as float
			 */
	        float AsFloat(void) const;

			/** 
			 * Get value as double
			 */
            double AsDouble(void) const;

			template<typename Type>
			Type AsType(void) const
			{
				return (Type)AsInt();
			}

			operator const std::wstring&(void) const;

        private:
#pragma warning( disable: 4251 )
	        std::wstring m_stName,	///< The attributes name
						 m_stValue; ///< The attributes value, as string
#pragma warning( default: 4251 )
        };

    }
}

