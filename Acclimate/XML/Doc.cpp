#include "Doc.h"
#include "Exception.h"
#include "Parser.h"

namespace acl
{
    namespace xml
    {
	
		Doc::Doc(void): m_rootNode(L"")
		{
		}

        const Node* Doc::Root(const std::wstring& stName) const
        {
	        if(m_rootNode.GetName() != stName)
		        throw Exception();

	        return &m_rootNode;
        }

        void Doc::LoadFile(const std::wstring& stFileName)
        {
			Parser parser(stFileName);

			parser.Parse(*this);
        }

        void Doc::SaveFile(const std::wstring& stFileName) const
        {
	        std::fstream fOut(stFileName, std::ios::out);

	        if(!fOut.is_open())
		        throw ParseException(stFileName);

	        m_rootNode.Save(fOut, 0);
        }

        Node& Doc::InsertNode(const std::wstring& stName)
        {
	        m_rootNode.SetName(stName);
	        return m_rootNode;
        }

    }
}