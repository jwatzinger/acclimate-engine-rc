/*
* Acclimate Engine: XML
*
* Created by Julian Watzinger
*/

#pragma once
#include "Node.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace xml
    {
		class Node;

		/// The xml document class
		/** Represents an XML document. Can be serialized in both directions. After loading it from a file,
		*	it will hold the root node of an XML-tree. A node can also be inserted into an empty document, 
		*	which can be saved to a file afterwards.*/
        class ACCLIMATE_API Doc
        {
        public:

			Doc(void);

			/** Aquires the root of the document.
             *  Returns a pointer to the root,  if it fits the passed name, otherwise it
			 *	will return a \c nullptr.
			 *
             *  @param[in] stName Expected name of the root.
			 *
			 *	@return Pointer to the documents root node.
			 */
	        const Node* Root(const std::wstring& stName) const;

			/** Loads an XML-document from a file.
             *  This method overwrites everything currently in the file.
			 *
             *  @param[in] stDocumentName Name of the document to load.
			 */
	        void LoadFile(const std::wstring& stDocumentName);

			/** Saves the XML-document to a file.
			 *	Will overwrite it silently if the file already exists.
			 *
             *  @param[in] stDocumentName Name of the document to write to.
			 */
	        void SaveFile(const std::wstring& stDocumentName) const;

			/** Sets a new root node to the document.
             *  Throws away the whole current tree if the document isn't already empty.
			 *
             *  @param[in] stName Name of the new root.
			 *
			 *	@return The new root node.
			 */
	        Node& InsertNode(const std::wstring& stName);

        private:
#pragma warning( disable: 4251 )
	        Node m_rootNode; /**< The documents root node.
				* If the document is empty, this node will be namelass, and hold
				* no attributes nor child nodes. */
#pragma warning( default: 4251 )
			
        };

    }
}
