/*
* Acclimate Engine: XML
*
* Created by Julian Watzinger
*/

#pragma once
#include "..\System\Exception.h"

namespace acl
{
	namespace xml
	{

		class Exception : 
			public llException
		{
		};

		class ParseException : 
			public Exception
		{
		public: 
			ParseException(const std::wstring& fileName): m_string(fileName)
			{
			}

			const std::wstring m_string;
		};

		class SyntaxException : 
			public ParseException
		{
		public:

			SyntaxException(const std::string& token, const std::wstring& stFilename, unsigned int line): ParseException(stFilename), m_token(token), m_line(line)
			{
			}

			const unsigned int m_line;
			const std::string m_token;
		};

	}
}