#include "Node.h"
#include <codecvt>
#include "..\System\Assert.h"

namespace acl
{
    namespace xml
    {
		namespace
		{
			std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;
		}

        Node::Node(const std::wstring& stName) : m_stName(stName),
			m_stValue(L"")
        {
        }

		Node::Node(const Node& node) : m_stName(node.m_stName), m_stValue(node.m_stValue),
			m_mAttributes(node.m_mAttributes)
		{
			for(auto pNode : node.m_vNodes)
			{
				auto pNewNode = new Node(*pNode);
				m_mNodes[pNewNode->GetName()].push_back(pNewNode);
				m_vNodes.push_back(pNewNode);
			}
		}

		Node::~Node(void)
		{
			for(auto pNode: m_vNodes)
			{
				delete pNode;
			}
		}

		bool Node::operator==(const Node& node) const
		{
			if(&node == this)
				return true;

			if(m_stName != node.m_stName || m_stValue != node.m_stValue)
				return false;

			if(m_mNodes.size() != node.m_mNodes.size() || m_mAttributes.size() != m_mAttributes.size())
				return false;

			for(auto& nodeVectors : m_mNodes)
			{
				auto itr = node.m_mNodes.find(nodeVectors.first);
				if(itr == node.m_mNodes.end())
					return false;
				else
				{
					if(nodeVectors.second.size() != itr->second.size())
						return false;
					else
					{
						unsigned int id = 0;
						for(auto pNode : nodeVectors.second)
						{
							if(*pNode != *itr->second[id])
								return false;
							else
								id++;
						}
					}
				}
			}

			for(auto& attribute : m_mAttributes)
			{
				auto itr = node.m_mAttributes.find(attribute.first);
				if(itr == node.m_mAttributes.end() || attribute.second != itr->second)
					return false;
			}

			return true;
		}

		bool Node::operator!=(const Node& node) const
		{
			return !operator==(node);
		}

		void Node::Save(std::fstream& fOut, unsigned int deep) const
        {
	        for(unsigned int i = 0; i < deep; i++)
		        fOut.put('\t');

	        fOut.put('<');
			const auto& stName = conversion.to_bytes(m_stName);
			fOut.write(stName.c_str(), stName.size());

	        for(auto& attribute : m_mAttributes)
	        {
		        attribute.second.Save(fOut);
	        }

	        if(!m_mNodes.empty())
	        {
		        fOut.put('>');
		        fOut.put('\n');
			    for(auto& pNode : m_vNodes)
			    {
				    pNode->Save(fOut, deep+1);
			    }
		        for(unsigned int i = 0; i < deep; i++)
			        fOut.put('\t');

		        fOut.put('<');
		        fOut.put('/');
				fOut.write(stName.c_str(), stName.size());
		        fOut.put('>');
		        fOut.put('\n');
	        }
	        else
	        {
				if(m_stValue.empty())
				{
					fOut.put('/');
					fOut.put('>');
					fOut.put('\n');
				}
				else
				{
					fOut.put('>');
					const auto& stValue = conversion.to_bytes(m_stValue);
					fOut.write(stValue.c_str(), stValue.size());
					fOut.put('<');
					fOut.put('/');
					fOut.write(stName.c_str(), stName.size());
					fOut.put('>');
					fOut.put('\n');
				}
	        }
        }

		void Node::SetName(const std::wstring& stName)
		{
			m_stName = stName;
		}

		void Node::SetValue(const std::wstring& stValue)
		{
			m_stValue = stValue;
		}

		Node::NodeVector* Node::Nodes(const std::wstring& stName)
        {
			auto& itr = m_mNodes.find(stName);
			if(itr == m_mNodes.end())
		        return nullptr;

			return &itr->second;
        }

        Node* Node::FirstNode(const std::wstring& stName)
        {
	        NodeVector* pNodes = Nodes(stName);

	        if(!pNodes || pNodes->empty())
		        return nullptr;

	        return *pNodes->begin();
        }

		void Node::RemoveNode(const Node& node)
		{
			auto itr = std::find(m_vNodes.begin(), m_vNodes.end(), &node);

			if(itr != m_vNodes.end())
			{
				const size_t id = itr - m_vNodes.begin();
				m_vNodes.erase(itr);

				auto vNodes = m_mNodes[node.GetName()];
				ACL_ASSERT(vNodes[id] == &node);
				vNodes.erase(vNodes.begin() + id);

				delete &node;
			}
		}

        Attribute* Node::Attribute(const std::wstring& stName)
        {
			auto& itr = m_mAttributes.find(stName);
			if(itr == m_mAttributes.end())
		        return nullptr;

	        return &itr->second;
        }

        const std::wstring& Node::GetName(void) const
        {
	        return m_stName;
        }

		const std::wstring& Node::GetValue(void) const
		{
			return m_stValue;
		}

		const Node::NodeVector& Node::GetNodes(void) const
		{
			return m_vNodes;
		}

        const Node::NodeVector* Node::Nodes(const std::wstring& stName) const
        {
			auto& itr = m_mNodes.find(stName);
	        if(itr == m_mNodes.end())
		        return nullptr;

			return &itr->second;
        }

        const Node* Node::FirstNode(const std::wstring& stName) const
        {
	        const NodeVector* pNodes = Nodes(stName);

	        if(!pNodes || pNodes->empty())
		        return nullptr;

	        return (*pNodes)[0];
        }

        const Attribute* Node::Attribute(const std::wstring& stName) const
        {
			auto& itr = m_mAttributes.find(stName);
	        if(itr == m_mAttributes.end())
		        return nullptr;

			return &itr->second;
        }

        Node& Node::InsertNode(const std::wstring& stName)
        {
	        Node* pNode = new Node(stName);
	        m_mNodes[stName].push_back(pNode);
			m_vNodes.push_back(pNode);

	        return *pNode;
        }

        void Node::ModifyAttribute(const std::wstring& stName, const std::wstring& stValue)
        {
	        if(!m_mAttributes.count(stName))
	        {
		        xml::Attribute attribute(stName, stValue);
		        m_mAttributes[stName] = attribute;
	        }
	        else
		        m_mAttributes[stName].SetValue(stValue);
        }

        void Node::RemoveAttribute(const std::wstring& stName)
        {
			m_mAttributes.erase(stName);
        }

    }
}