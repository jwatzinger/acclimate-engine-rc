/*
* Acclimate Engine: XML
*
* Created by Julian Watzinger
*/

#pragma once
#include <unordered_map>
#include <vector>
#include "Attribute.h"
#include "..\Core\Dll.h"

namespace acl
{
    namespace xml
    {

		/// The xml node class
		/** Represents a node in an XML document. It may contain any number of child-nodes, 
		*	as well as any number of attributes, both referencable by name. New nodes can be
		*	inserted, and attributes can both be modified and removed.*/
        class ACCLIMATE_API Node
        {
        public:
			typedef std::vector<Node*> NodeVector;

	        Node(const std::wstring& stName);
			Node(const Node& node);
			~Node(void);

			bool operator==(const Node& node) const;
			bool operator!=(const Node& node) const;

			/** Serializes the node to a file stream.
             *  Writes the current node and all of its subnodes plus attributes to the file stream.
			 *
             *  @param[in] fOut Filestream to write to.
			 *	@param[in] deep The depth of the current node, used for tab-formatting
			 */
	        void Save(std::fstream& fOut, unsigned int deep) const;

			/** 
			 * Set name
			 */
			void SetName(const std::wstring& stName);

			/**
			* Set value
			*/
			void SetValue(const std::wstring& stValue);

			/** 
			 * Get name
			 */
	        const std::wstring& GetName(void) const;

			/**
			* Get value
			*/
			const std::wstring& GetValue(void) const;

			/** Returns all child nodes.
			 *	Use this method to access all child nodes, sorted by tree order.
			 *
			 *	@return Reference to the map of nodes.
			 */
			const NodeVector& GetNodes(void) const;

			/** Returns a vector of all nodes with the specified name.
			 *
             *  @param[in] stName Name for checking the nodes
			 *	
			 *	@return Vector of nodes, if none are found, returns \c nullptr
			 */
			NodeVector* Nodes(const std::wstring& stName); 

			/** Returns the first node of the specified name.
			 *
             *  @param[in] stName Name of the node
			 *	
			 *	@return Pointer to the node, if none is found, returns \c nullptr
			 */
			Node* FirstNode(const std::wstring& stName);

			void RemoveNode(const Node& node);

			/** Returns the attribute of the given name.
			 *
             *  @param[in] stName Name of the attribute
			 *	
			 *	@return Pointer to the attribute, \c nullptr if none is found
			 */
			Attribute* Attribute(const std::wstring& stName);

			/** 
			 *	const overload of Nodes
			 */
			const NodeVector* Nodes(const std::wstring& stName) const; 

			/** 
			 *	const overload of FirstNode
			 */
			const Node* FirstNode(const std::wstring& stName) const;

			/** 
			 *	const overload of Attribute
			 */
			const xml::Attribute* Attribute(const std::wstring& stName) const;

			/** Inserts a node to the node-tree.
			 *	Puts a new node at the end of the nodes children list. If there are already
			 *	some nodes of this name already, it gets inserted as the end node.
			 *
             *  @param[in] stName Name of the new node
			 *	
			 *	@return Reference to the newly inserted node.
			 */
	        Node& InsertNode(const std::wstring& stName);

			/** Sets the value of an attribute of the node.
			 *	If the attribute doesn't already exist, it gets inserted.
			 *
             *  @param[in] stName Name of the attribute
			 *	@param[in] stValue New value for the attribute
			 */
	        void ModifyAttribute(const std::wstring& stName, const std::wstring& stValue);

			/** Removes an attribute from the node.
			 *	Does nothing if the attribute doesn't exist.
			 *
             *  @param[in] stName Name of the attribute to remove.
			 */
	        void RemoveAttribute(const std::wstring& stName);

        private:
#pragma warning( disable: 4251 )
	        typedef std::unordered_map<std::wstring, xml::Attribute> AttributeMap;
			typedef std::unordered_map<std::wstring, NodeVector> NodeMap;

	        std::wstring m_stName; ///< Name of the node
			std::wstring m_stValue; ///< Value of the node

	        AttributeMap m_mAttributes; ///< Map for all of the nodes attributes
	        NodeMap m_mNodes; ///< Container for the nodes, sorted by name
			NodeVector m_vNodes; ///< Container to stored nodes by insertion order.
#pragma warning( default: 4251 )
        };
    }
}

