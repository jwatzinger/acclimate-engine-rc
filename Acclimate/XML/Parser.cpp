#include "Parser.h"
#include <string>
#include <ctype.h>
#include "Doc.h"
#include "Exception.h"
#include "..\File\File.h"
#include "..\System\Assert.h"
#include "..\System\Log.h"

#include <codecvt>

namespace acl
{
	namespace xml
	{

		std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;

		Parser::Parser(const std::wstring& stFilename): m_stream(stFilename.c_str(), std::ios::in), m_line(1), m_nodes(0), m_stFilename(stFilename),
			m_pDoc(nullptr)
		{
		}

		void Parser::Parse(Doc& doc)
		{
			if(!m_stream.is_open())
			{
				sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "Failed to open AXM file", file::FullPath(m_stFilename));
		        throw ParseException(m_stFilename);
			}

			m_pDoc = &doc;

			SearchNode(nullptr);

			m_pDoc = nullptr;
		}

		void Parser::SearchNode(Node* pParentNode)
		{
			while(m_stream)
			{
				char c = m_stream.peek();
				if(c != '<' && c != '\n' && c != '\t' && c != ' ')
				{
					ParseValue(*pParentNode);
					break;
				}

				m_stream.get(c);

				if(c == '<')
				{
					m_nodes++;
					ParseNode(pParentNode);
					break;
				}
				else if(c == '\n')
					m_line++;
			}
		}

		void Parser::ParseNode(Node* pParentNode)
		{		
			int currentNodes = m_nodes;
			bool bEnd = false;
			char c;
			std::wstring stName;

			Node* pNode = nullptr;
			while(m_stream)
			{
				m_stream.get(c);

				if(c == ' ' || c == '\t')
				{
					if(stName.empty()|| bEnd)
					{
						sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
						throw ParseException(m_stFilename);
					}
					else
					{
						ACL_ASSERT(!pNode);
						if(pParentNode)
							pNode = &pParentNode->InsertNode(stName.c_str());
						else
							pNode = &m_pDoc->InsertNode(stName.c_str());
						SearchAttributes(*pNode);
					}
				}
				else if(c == '\n')
				{
					sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
					throw ParseException(m_stFilename);
				}
				else if(c == '>')
				{
					if((stName.empty() && !bEnd) || m_nodes < 0)
					{
						sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
						throw ParseException(m_stFilename);
					}
					if(!stName.empty())
					{
						if(!pNode)
						{
							if(pParentNode)
								pNode = &pParentNode->InsertNode(stName.c_str());
							else
								pNode = &m_pDoc->InsertNode(stName.c_str());
						}
						while(m_nodes >= currentNodes)
						{
							if(!m_stream.good())
							{
								sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, ". end of file reached before node", pNode->GetName(), "was being closed.");
								throw ParseException(m_stFilename);
							}

							SearchNode(pNode);
						}
					}
					break;
				}
				else if(c == '/')
				{
					if(stName.empty())
						m_nodes-=2;
					else 
						m_nodes--;
					bEnd = true;
				}
				else if(!bEnd)
					stName += c;
			}
		}

		void Parser::SearchAttributes(Node& node)
		{
			while(m_stream)
			{
				char c = m_stream.peek();

				if(c == '/' || c == '>')
					break;
				else if(isalpha(c))
					ParseAttributeName(node);
				else if(c != ' ' && c != '\t')
				{
					sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
					throw ParseException(m_stFilename);
				}
				else
					m_stream.get(c);
			}
		}

		void Parser::ParseAttributeName(Node& node)
		{
			char c;

			std::string stName;
			std::wstring stValue;

			while(m_stream)
			{
				m_stream.get(c);

				if(isalnum(c))
					stName += c;
				else if(c == '=')
				{
					m_stream.get(c);
					if(c == '\"')
						stValue = ParseAttributeValue();
					else
					{
						sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
						throw ParseException(m_stFilename);
					}

					break;
				}
				else if(c != ' ' && c != '\t')
				{
					sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "expected node name identifier.");
					throw ParseException(m_stFilename);
				}

			}

			const std::wstring stNameW = conversion.from_bytes(stName);
			node.ModifyAttribute(stNameW, stValue);
		}

		std::wstring Parser::ParseAttributeValue(void)
		{
			char c;

			std::string stValue;

			while(m_stream)
			{
				m_stream.get(c);

				if(c == '\"')
					break;
				else if(c == '\n' || c == '\t')
				{
					sys::log->Out(sys::LogModule::XML, sys::LogType::ERR, "AXM syntax error reading file", m_stFilename, "in line", m_line, ": unexpected token", c, "while parsing attribute.");
					throw ParseException(m_stFilename);
				}
				else
					stValue += c;
			}

			return conversion.from_bytes(stValue);
		}

		template<class Facet>
		struct deletable_facet : Facet
		{
			template<class ...Args>
			deletable_facet(Args&& ...args) : Facet(std::forward<Args>(args)...) {}
			~deletable_facet() {}
		};

		void Parser::ParseValue(Node& node)
		{
			std::string stValue("");

			// parse value until next token is encountered
			while(m_stream)
			{
				char c = m_stream.peek();
				if(c == '<')
					break;

				m_stream.get(c);

				if(c == '\n')
					m_line++;
				else
					stValue += c;
			}

			const auto stValueW = conversion.from_bytes(stValue);

			node.SetValue(stValueW);
		}
			
	}
}