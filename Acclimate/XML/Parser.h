/*
* Acclimate Engine: XML
*
* Created by Julian Watzinger
*/

#pragma once
#include <fstream>

namespace acl
{
	namespace xml
	{

		class Doc;
		class Node;

		/// XML parser
		/** This class is responsible for parsing the content of an XML file. Can output the input to
		*	an XML document class. */
		class Parser
		{
		public:

			Parser(const std::wstring& stFilename);

			/** Parses the content of the parsers file.
			 *
             *  @param[out] doc Document reference to write to.
			 */
			void Parse(Doc& doc);

		private:

			void SearchNode(Node* pParentNode);
			void ParseNode(Node* pParentNode);

			void SearchAttributes(Node& node);
			void ParseAttributeName(Node& node);
			std::wstring ParseAttributeValue(void);

			void ParseValue(Node& node);

			int m_line;
			int m_nodes;

			std::fstream m_stream;
			std::wstring m_stFilename;

			Doc* m_pDoc;

		};

	}
}

