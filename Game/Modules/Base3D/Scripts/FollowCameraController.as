class FollowCameraController : IGameScript
{
	FollowCameraController(const Entity& in ent)
	{
		entity = ent;
		@cam = GetCamera(entity);
	}
	
	void OnInit()
	{
	}
	
	void OnUpdate(double dt)
	{
		Position@ position = GetPosition(entity);
		if(position !is null)
		{
			const Vector3 vPos = position.GetVec();
			Vector3 vCameraPos = vPos + Vector3(0.0f, 5.0f, 0.0f);
			
			Direction@ direction = GetDirection(entity);
			if(direction !is null)
				vCameraPos -= direction.GetVec()*10.0f;
			else
				vCameraPos -= Vector3(10.0f, 0.0f, 0.0f);

			cam.camera.SetPosition(vCameraPos);
			cam.camera.SetLookAt(vPos);
			cam.dirty = true;
		}
	}
	
	void OnProcessInput(const MappedInput@ input)
	{
	}
	
	private Entity entity;
	private Cam@ cam;
}