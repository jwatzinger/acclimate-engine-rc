class FreeCameraController : IGameScript
{
	FreeCameraController(const Entity& in entity)
	{
		@cam = GetCamera(entity);
		
		rotateX = 0.0001f;
		rotateY = 0.0001f;
		angleX = 0.0f;
		angleY = 0.0f;
		vPos = Vector3(100.0f, 5.0f, 5.0f);
		vOldPos = vPos;
		vDirection = Vector3(1.0f, 0.0f, 0.0f);
		isInit = true;
	}
	
	void OnInit()
	{
	}
	
	void OnUpdate(double dt)
	{
		const Vector3 vUp = cam.camera.GetUp();
		
		bool hasMoved = false;
		
		if(vOldPos != vPos)
		{
			const float speed = 100.0f;
			
			Vector3 vDist = vPos - vOldPos;
			
			const float length = vDist.length();
			vDist.Normalize();
			vDist *= dt * speed;
			const float newLength = vDist.length();
			
			if(newLength >= length)
			{
				vOldPos = vPos;
			}
			else
			{
				vOldPos += vDist;
			}
			
			hasMoved = true;
		}
		
		if(rotateX != 0.0f || rotateY != 0.0f)
		{
			angleX += rotateX;
			angleY += rotateY;
			
			vDirection = Vector3(1.0f, 0.0f, 0.0f);
			const Matrix mRotation = MatRotationY(angleX);
			vDirection = mRotation.TransformNormal(vDirection);
			vDirection.Normalize();
			
			const Vector3 vRight = vDirection.Cross(vUp).normal();
			const Matrix mRotationAxis = MatRotationAxis(vRight, angleY);
			vDirection = mRotationAxis.TransformNormal(vDirection);
			vDirection.Normalize();
			
			rotateX = 0.0f;
			rotateY = 0.0f;
			
			hasMoved = true;
		}

		if(isInit || hasMoved)
		{
			cam.camera.SetPosition(vOldPos);
			cam.camera.SetLookAt(vOldPos + vDirection);
			
			isInit = false;
			cam.dirty = true;
		}
	}
	
	void OnProcessInput(const MappedInput@ input)
	{
		if(input.HasState(TURN_HOLD))
		{
			float rotX = 0.0f;
			float rotY = 0.0f;
			bool rotatedX = input.HasRange(MOVE_X, rotX);
			bool rotatedY = input.HasRange(MOVE_Y, rotY);
			
			if(rotatedX || rotatedY)
			{
				rotateX = -rotX*0.01f;
				rotateY = rotY*0.01f;
			}
		}

		const Vector3 vUp = cam.camera.GetUp();
		const float step = 2.0f;
		
		Vector3 vMove;
		if(input.HasState(MOVE_FORWARD))
			vMove = vDirection * step;
		else if(input.HasState(MOVE_BACKWARD))
			vMove = -vDirection * step;
			
		const Vector3 vRight = vUp.Cross(vDirection);
		
		if(input.HasState(MOVE_RIGHT))
			vMove += vRight * step;
		else if(input.HasState(MOVE_LEFT))
			vMove -= vRight * step;
			
		vPos = vOldPos + vMove;
	}
	
	float angleX, angleY;
	Vector3 vPos;

	private bool isInit;
	private Cam@ cam;
	private Vector3 vOldPos, vDirection;
	private float rotateX, rotateY;
}