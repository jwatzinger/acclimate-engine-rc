class TerrainCameraController : IGameScript
{
	TerrainCameraController(const Entity& in entity)
	{
		@cam = GetCamera(entity);
		moveX = 0.01f;
		moveZ = 0.01f;
		vLook = Vector3(-5.5f, 30.0f, -17.5);
		y = targetY = 10.0f;
	}
	
	void OnInit()
	{
	}
	
	void OnUpdate(double dt)
	{
		bool moved = false;
		
		// update camera height
		
		if(targetY != y)
		{
			float dist = targetY - y;
			if(dist > 0.0f)
				y += cap(dist*dt, 0.5f, dist);
			else
				y -= cap(-dist*dt, 0.5f, -dist);
			moved = true;
		}
		
		// update camera position
		
		if(moveX != 0.0f || moveZ != 0.0f)
		{
			vLook.x += moveX * dt;
			vLook.z += moveZ * dt;
			
			vLook.x = cap(vLook.x, -64.0f, 64.0f);
			vLook.z = cap(vLook.z, -64.0f, 64.0f);
				
			Ray ray(vLook, Vector3(0.0f, 1.0f, 0.0f));
			PlacePositionQuery query(ray);
			
			if(queryPlacePosition(query))
			{	
				vLook = query.position;
				
				vLook.y = max(30.0f, vLook.y);
			}
							
			moved = true;
		}
		
		if(moved)
		{
			// calculate up-vector
			const Vector3 vRight = Vector3(0.0f, 0.0f, 1.0f);
			const Vector3 vUp = vRight.Cross(cam.camera.GetDirection());
			
			cam.camera.SetUp(vUp);
			cam.camera.SetLookAt(vLook);
			cam.camera.SetPosition(vLook + Vector3(-10.0f, y, 0.0f));
			
			cam.dirty = true;
		}
	}
	
	void OnProcessInput(const MappedInput@ input)
	{
		const Vector2i screenSize = cam.camera.GetScreenSize();
		const int border = 16;
		const float speed = 15.0f;
		
		float zoom = 0.0f;
		if(input.HasRange(ZOOM, zoom))
		{
			const float min = 5.0f;
			const float max = 20.0f;
			
			targetY += zoom / 50.0f;
			targetY = cap(targetY, min, max);
		}
		
		float posX = 0.0f;
		if(input.HasRange(MOVE_X, 0) && input.HasValue(MOUSE_X, posX))
		{
			if(posX < border)
				moveZ = speed;
			else if(posX > screenSize.x - border*2)
				moveZ = -speed;
			else
				moveZ = 0.0f;
		}
		
		float posY = 0.0f;
		if(input.HasRange(MOVE_Y, 0) && input.HasValue(MOUSE_Y, posY))
		{
			if(posY < border)
				moveX = speed;
			else if(posY > screenSize.y - border*2)
				moveX = -speed;
			else
				moveX = 0.0f;
		}
	}
	
	private Cam@ cam;
	Vector3 vLook;
	float moveX, moveZ;
	float y, targetY;
}