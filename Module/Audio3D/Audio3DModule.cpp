#include "Audio3DModule.h"
#include "AudioSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "RegisterScript.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"

namespace acl
{
	namespace modules
	{

		Audio3DModule::Audio3DModule(void): m_pSystems(nullptr)
		{
		}

		void Audio3DModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			m_pSystems->AddSystem<ecs::AudioSystem>(context.audio.device, context.audio.files);

			// io
			ecs::Saver::RegisterSubRoutine<ecs::AudioSaveRoutine>(L"Audio3D");
			ecs::Loader::RegisterSubRoutine<ecs::AudioLoadRoutine>(L"Audio3D");

			audio3d::registerScript(context.script.core);
		}

		void Audio3DModule::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<ecs::AudioSystem>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Audio3D");
			ecs::Loader::UnregisterSubRoutine(L"Audio3D");

			m_pSystems = nullptr;
		}

		void Audio3DModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::AudioSystem>(dt);
		}

	}
}
