#include "AudioSystem.h"
#include "Components.h"
#include "Audio\Device.h"
#include "Audio\Emitter.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace ecs
	{

		AudioSystem::AudioSystem(audio::Device& device, const audio::Files& files): m_pDevice(&device), m_files(files)
		{
			m_pDevice->SetListener(&m_listener);
		}

		void AudioSystem::Init(MessageManager& messageManager)
		{
			messageManager.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}
		
		void AudioSystem::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<AudioSource>();

			for(auto& entity: vEntities)
			{
				AudioSource* pSource = entity->GetComponent<AudioSource>();
				
				if(!pSource->pEmitter)
				{
					if(auto pFile = m_files[pSource->stFile])
					{
						pSource->pEmitter = new audio::Emitter(*m_pDevice, *pFile, pSource->bLoop, pSource->volume);
						pSource->pEmitter->Play();
					}
				}
				else
				{
					if(pSource->bRemoveOnFinish && !pSource->pEmitter->IsPlaying())
					{
						m_pEntities->RemoveEntity(entity);
						continue;
					}
					else
						pSource->pEmitter->Update(dt);

					if(auto pPosition = entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Position")))
						pSource->pEmitter->SetPosition(pPosition->GetAttribute<math::Vector3>(L"Vector"));
					else
						pSource->pEmitter->SetPosition(nullptr);
				}
			}
		}

		void AudioSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				auto pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
				m_listener.Update(pCamera->GetPosition(), pCamera->GetDirection());
			}
		}

	}
}
