#pragma once
#include "Entity\System.h"
#include "Audio\Files.h"
#include "Audio\Listener.h"

namespace acl
{
	namespace audio
	{
		class Device;
	}

	namespace ecs
	{

		class AudioSystem :
			public ecs::System<AudioSystem>
		{
		public:
			AudioSystem(audio::Device& device, const audio::Files& files);

			void Init(MessageManager& messageManager) override;
			void Update(double dt) override;
			void ReceiveMessage(const BaseMessage& message) override;

		private:

			audio::Device* m_pDevice;
			audio::Listener m_listener;
			const audio::Files& m_files;
		};

	}
}

