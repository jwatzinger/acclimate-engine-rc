#include "Components.h"
#include "Audio\Emitter.h"

namespace acl
{
	namespace ecs
	{

		AudioSource::AudioSource(const std::wstring& stFile, bool bLoop, bool bRemoveOnFinish, float volume) : stFile(stFile), bLoop(bLoop), bRemoveOnFinish(bRemoveOnFinish),
			pEmitter(nullptr), volume(volume)
		{
		}

		AudioSource::~AudioSource(void)
		{
			delete pEmitter;
		}

	}
}