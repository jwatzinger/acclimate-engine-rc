#pragma once
#include <string>
#include "Entity\Component.h"

namespace acl
{
	namespace audio
	{
		class Emitter;
	}

	namespace ecs
	{

		struct AudioSource :
			public Component<AudioSource>
		{
			AudioSource(const std::wstring& stFile, bool bLoop, bool bRemoveOnFinish, float volume);
			~AudioSource(void);

			std::wstring stFile;
			bool bLoop, bRemoveOnFinish;
			float volume;
			audio::Emitter* pEmitter;
		};

	}
}