#include "Export.h"
#include "Audio3DModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::Audio3DModule();
}