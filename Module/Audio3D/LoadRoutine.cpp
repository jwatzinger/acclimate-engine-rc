#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void AudioLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pSource = entityNode.FirstNode(L"AudioSource"))
			{
				const std::wstring& stFile = pSource->Attribute(L"file")->GetValue();
				const bool bLoop = pSource->Attribute(L"loop")->AsBool();
				const bool bRemove = pSource->Attribute(L"remove")->AsBool();
				const float volume = pSource->Attribute(L"volume")->AsFloat();

				entity.AttachComponent<AudioSource>(stFile, bLoop, bRemove, volume);
			}
		}

	}
}
