#include "RegisterScript.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace audio3d
	{
		/*******************************************
		* Event
		*******************************************/

		ecs::AudioSource* AttachAudio(ecs::EntityHandle* entity, const std::wstring& stFile, bool loop, bool removedOnFinish)
		{
			return (*entity)->AttachComponent<ecs::AudioSource>(stFile, loop, removedOnFinish, 1.0f);
		}

		void RemoveEvent(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::AudioSource>();
		}

		ecs::AudioSource* GetEvent(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::AudioSource>();
		}

		void registerScript(script::Core& script)
		{
			auto eventComp = script.RegisterReferenceType("AudioSource", asOBJ_NOCOUNT);

			// position
			script.RegisterGlobalFunction("AudioSource@ AttachAudio(Entity& in, const string& in, bool, bool)", asFUNCTION(AttachAudio));
			script.RegisterGlobalFunction("void RemoveAudio(Entity& in)", asFUNCTION(RemoveEvent));
			script.RegisterGlobalFunction("AudioSource@ GetAudio(const Entity& in)", asFUNCTION(GetEvent));
		}

	}
}
