#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace audio3d
	{

		void registerScript(script::Core& script);
	}
}
