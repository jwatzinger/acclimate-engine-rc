#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void AudioSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(AudioSource* pSource = entity.GetComponent<AudioSource>())
			{
				auto& node = entityNode.InsertNode(L"AudioSource");
				node.ModifyAttribute(L"file", pSource->stFile);
				node.ModifyAttribute(L"loop", conv::ToString(pSource->bLoop));
				node.ModifyAttribute(L"remove", conv::ToString(pSource->bRemoveOnFinish));
				node.ModifyAttribute(L"volume", conv::ToString(pSource->volume));
			}
		}

	}
}
