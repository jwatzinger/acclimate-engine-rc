#include "ActorRenderSystem.h"
#include "Messages.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "Gfx\ModelInstance.h"
#include "Math\Frustum.h"
#include "Render\IStage.h"
#include "Render\IRenderer.h"

namespace acl
{
    namespace ecs
    {

        ActorRenderSystem::ActorRenderSystem(const render::IRenderer& renderer): m_pCamera(nullptr), m_pLitStage(renderer.GetStage(L"world")), 
			m_pUnlitStage(renderer.GetStage(L"nolight"))
        {
        }

        void ActorRenderSystem::Init(MessageManager& messageManager)
        {
	        messageManager.Subscribe<UpdateCamera>(*this);
			messageManager.Subscribe<RenderScene>(*this);
        }

        void ActorRenderSystem::Update(double dt)
        {
			if(!m_pCamera)
				return;

	        const math::Frustum* pFrustum = &m_pCamera->GetFrustum();

	        auto vEntities = m_pEntities->EntitiesWithComponents<Actor>();

            //render actors
	        for(auto& entity : vEntities)
	        {
				const Actor* pActor = entity->GetComponent<Actor>();
                if(!pActor->bVisible)
                    continue;

                if(auto* pModel = pActor->pModel)
				{
					// frustum culling
					Bounding* pBounding = entity->GetComponent<Bounding>();
					if(pBounding && !pFrustum->TestVolume(*pBounding->pVolume))
						continue;

					// depth sorting
					if(auto pPosition = entity->GetComponent<ecs::Position>())
					{
						math::Vector3 vPos(pPosition->v);
						const math::Matrix& mView = m_pCamera->GetViewMatrix();
						vPos = mView.TransformCoord(vPos);
						pModel->SetDepth((1.0f-vPos.z)/m_pCamera->GetFar());
					}

					pModel->Draw(*m_pLitStage, 0); // lit pass
					pModel->Draw(*m_pUnlitStage, 3); // unlit pass
				}
	        }
	        //
        }

        void ActorRenderSystem::ReceiveMessage(const BaseMessage& message)
        {
	        if(const UpdateCamera* pUpdate = message.Convert<UpdateCamera>())
	        {
		        m_pCamera = &pUpdate->camera;

				m_pLitStage->SetVertexConstant(0, (float*)&m_pCamera->GetViewProjectionMatrix(), 4);

				m_pUnlitStage->SetVertexConstant(0, (float*)&m_pCamera->GetViewProjectionMatrix(), 4);
				m_pUnlitStage->SetVertexConstant(4, (float*)&m_pCamera->GetPosition(), 1);
				m_pUnlitStage->SetVertexConstant(5, (float*)&m_pCamera->GetViewMatrix(), 4);

				// TODO: move to sky-module

				m_pUnlitStage->SetGeometryConstant(0, (float*)&m_pCamera->GetViewProjectionMatrix(), 4);
				m_pUnlitStage->SetGeometryConstant(4, (float*)&m_pCamera->GetPosition(), 1);
				m_pUnlitStage->SetGeometryConstant(5, (float*)&m_pCamera->GetUp(), 1);
	        }
			else if(const RenderScene* pRender = message.Convert<RenderScene>())
			{
				const math::Frustum* pFrustum = &pRender->camera.GetFrustum();

				auto vEntities = m_pEntities->EntitiesWithComponents<Actor>();

				//render actors
				for(auto& entity : vEntities)
				{
					const Actor* pActor = entity->GetComponent<Actor>();
					if(!pActor->bVisible)
						continue;

					if(auto* pModel = pActor->pModel)
					{
						// frustum culling
						Bounding* pBounding = entity->GetComponent<Bounding>();
						if(pBounding && !pFrustum->TestVolume(*pBounding->pVolume))
							continue;

						// depth sorting
						if(auto pPosition = entity->GetComponent<ecs::Position>())
						{
							math::Vector3 vPos(pPosition->v);
							const math::Matrix& mView = pRender->camera.GetViewMatrix();
							vPos = mView.TransformCoord(vPos);
							pModel->SetDepth((1.0f - vPos.z) / pRender->camera.GetFar());
						}

						pModel->Draw(pRender->stage, pRender->pass); // choose pass
					}
				}				
			}
        }

    }
}