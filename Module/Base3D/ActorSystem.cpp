#include "ActorSystem.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"

#include "Gfx\IMesh.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\AnimationController.h"

namespace acl
{
	namespace ecs
	{
		
		ActorSystem::ActorSystem(const gfx::Models& models): m_pModels(&models)
		{
		}

		void ActorSystem::Update(double dt)
		{
	        auto vEntities = m_pEntities->EntitiesWithComponents<Actor>();

            // set actor models
	        for(auto& entity : vEntities)
	        {
				Actor& actor = *entity->GetComponent<Actor>();

				if(actor.bDirty)
				{
					if (auto pModel = m_pModels->Get(actor.stName))
					{
						if(actor.pModel)
							delete actor.pModel;

						actor.pModel = &pModel->CreateInstance();
						actor.bDirty = false;
					}
				}

				if(actor.pModel)
				{
					if(const Transform* pTransform = entity->GetComponent<Transform>())
						actor.pModel->SetVertexConstant(0, (float*)&pTransform->mTransform, 4);

					if(Animation* pAnimation = entity->GetComponent<Animation>())
					{
						if(pAnimation->pController)
						{
							const auto& vMatrices = pAnimation->pController->GetMatrices();
							if(!vMatrices.empty())
								actor.pModel->SetVertexConstant(4, (float*)&vMatrices[0], vMatrices.size() * 4);
						}
					}	
				}

			}
		}

	}
}