#pragma once
#include "Entity\System.h"
#include "Gfx\Models.h"

namespace acl
{
    namespace ecs
    {

        class ActorSystem : 
	        public System<ActorSystem>
        {
        public:

	        ActorSystem(const gfx::Models& models);

	        void Update(double dt);

        private:

            const gfx::Models* m_pModels;
        };

    }
}