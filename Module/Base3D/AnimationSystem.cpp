#include "AnimationSystem.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"

#include "Gfx\IMesh.h"
#include "Gfx\IModel.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\AnimationController.h"

namespace acl
{
	namespace ecs
	{
		
		AnimationSystem::AnimationSystem(const gfx::Animations& animations) : m_pAnimations(&animations)
		{
		}

		void AnimationSystem::Update(double dt)
		{
	        auto vEntities = m_pEntities->EntitiesWithComponents<Animation, Actor>();

            // set actor models
	        for(auto& entity : vEntities)
	        {
				auto& animation = *entity->GetComponent<Animation>();
				auto& actor = *entity->GetComponent<Actor>();

				if(!animation.pController)
				{
					if(actor.pModel)
					{
						auto pSkeleton = actor.pModel->GetParent()->GetMesh()->GetSkeleton();
						auto pSet = m_pAnimations->Get(animation.stAnimationSet);
						if(pSkeleton && pSet)
						{
							animation.pController = new gfx::AnimationController(*pSkeleton, *pSet);
							animation.pController->ActivateAnimation(animation.stDefaultAnimation, 0.0f);
						}
					}
				}
				else if(animation.isPlaying)
				{
					if(!animation.stNextAnimation.empty())
					{
						animation.pController->ActivateAnimation(animation.stNextAnimation, 0.5f);
						animation.stNextAnimation.clear();
					}

					animation.pController->SetSpeed(animation.speed);
					animation.pController->Run(dt);
				}
			}
		}

	}
}