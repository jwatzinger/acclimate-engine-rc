#pragma once
#include "Entity\System.h"
#include "Gfx\Animations.h"

namespace acl
{
    namespace ecs
    {

        class AnimationSystem : 
			public System<AnimationSystem>
        {
        public:

			AnimationSystem(const gfx::Animations& animations);

	        void Update(double dt);

        private:

            const gfx::Animations* m_pAnimations;
        };

    }
}