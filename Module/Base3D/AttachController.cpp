#include "AttachController.h"
#include "IEditor.h"
#include "Components.h"
#include "Gui\CheckBox.h"

namespace acl
{
	namespace base3d
	{
		
		/***************************************
		* POSITION
		***************************************/

		PositionAttachController::PositionAttachController(gui::Module& module) : 
			BaseController(module, L"Editor/AttachPosition.axm")
		{
			m_pX = GetWidgetByName<PositionBox>(L"X");
			m_pY = GetWidgetByName<PositionBox>(L"Y");
			m_pZ = GetWidgetByName<PositionBox>(L"Z");
		}

		editor::IComponentController& PositionAttachController::Clone(void) const
		{
			return *new PositionAttachController(*m_pModule);
		}

		void PositionAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Position>(m_pX->GetContent(), m_pY->GetContent(), m_pZ->GetContent());
		}

		gui::BaseController& PositionAttachController::GetController(void)
		{
			return *this;
		}

		size_t PositionAttachController::GetComponentId(void) const
		{
			return ecs::Position::family();
		}

		/***************************************
		* Rotation
		***************************************/

		RotationAttachController::RotationAttachController(gui::Module& module) :
			BaseController(module, L"Editor/AttachPosition.axm")
		{
			m_pX = GetWidgetByName<RotationBox>(L"X");
			m_pY = GetWidgetByName<RotationBox>(L"Y");
			m_pZ = GetWidgetByName<RotationBox>(L"Z");
		}

		editor::IComponentController& RotationAttachController::Clone(void) const
		{
			return *new RotationAttachController(*m_pModule);
		}

		void RotationAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Rotation>(m_pX->GetContent(), m_pY->GetContent(), m_pZ->GetContent());
		}

		gui::BaseController& RotationAttachController::GetController(void)
		{
			return *this;
		}

		size_t RotationAttachController::GetComponentId(void) const
		{
			return ecs::Rotation::family();
		}

		/***************************************
		* Scale
		***************************************/

		ScaleAttachController::ScaleAttachController(gui::Module& module) :
			BaseController(module, L"Editor/AttachScale.axm")
		{
			m_pX = GetWidgetByName<ScaleBox>(L"X");
			m_pY = GetWidgetByName<ScaleBox>(L"Y");
			m_pZ = GetWidgetByName<ScaleBox>(L"Z");
		}

		editor::IComponentController& ScaleAttachController::Clone(void) const
		{
			return *new ScaleAttachController(*m_pModule);
		}

		void ScaleAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Scale>(m_pX->GetContent(), m_pY->GetContent(), m_pZ->GetContent());
		}

		gui::BaseController& ScaleAttachController::GetController(void)
		{
			return *this;
		}

		size_t ScaleAttachController::GetComponentId(void) const
		{
			return ecs::Scale::family();
		}

		/***************************************
		* TRANSFORM
		***************************************/

		TransformAttachController::TransformAttachController(gui::Module& module) :
			BaseController(module)
		{
		}

		editor::IComponentController& TransformAttachController::Clone(void) const
		{
			return *new TransformAttachController(*m_pModule);
		}

		void TransformAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Transform>();
		}

		gui::BaseController& TransformAttachController::GetController(void)
		{
			return *this;
		}

		size_t TransformAttachController::GetComponentId(void) const
		{
			return ecs::Transform::family();
		}

		/***************************************
		* Actor
		***************************************/

		ActorAttachController::ActorAttachController(gui::Module& module, const editor::IEditor& editor) :
			BaseController(module, L"Editor/AttachActor.axm"),
			m_pEditor(&editor)
		{
			m_pBox = GetWidgetByName<gui::Textbox<>>(L"Name");
			m_pBox->SigReleased.Connect(this, &ActorAttachController::OnPick);
		}

		editor::IComponentController& ActorAttachController::Clone(void) const
		{
			return *new ActorAttachController(*m_pModule, *m_pEditor);
		}

		void ActorAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Actor>(m_pBox->GetContent());
		}

		gui::BaseController& ActorAttachController::GetController(void)
		{
			return *this;
		}

		size_t ActorAttachController::GetComponentId(void) const
		{
			return ecs::Actor::family();
		}

		void ActorAttachController::OnPick(void)
		{
			std::wstring stName;
			if (m_pEditor->PickModel(nullptr, &stName))
				m_pBox->SetText(stName);
		}

		/***************************************
		* Node
		***************************************/

		NodeAttachController::NodeAttachController(gui::Module& module, const editor::IEditor& editor) :
			BaseController(module, L"Editor/AttachNode.axm"), m_pEditor(&editor)
		{
			m_pBox = GetWidgetByName<gui::Textbox<>>(L"Name");
			m_pBox->SigReleased.Connect(this, &NodeAttachController::OnPick);

			m_pLink = GetWidgetByName<gui::CheckBox>(L"Link");
		}

		editor::IComponentController& NodeAttachController::Clone(void) const
		{
			return *new NodeAttachController(*m_pModule, *m_pEditor);
		}

		void NodeAttachController::Attach(ecs::Entity& entity) const
		{
			if(m_entity.IsValid())
				entity.AttachComponent<ecs::Node>(m_entity, m_pLink->IsChecked());
		}

		gui::BaseController& NodeAttachController::GetController(void)
		{
			return *this;
		}

		size_t NodeAttachController::GetComponentId(void) const
		{
			return ecs::Node::family();
		}

		void NodeAttachController::OnPick(void)
		{
			ecs::EntityHandle entity;
			std::wstring stName = L"";
			if(m_pEditor->PickEntity(&entity, &stName))
			{
				m_pBox->SetText(stName);
				m_entity = entity;
			}	
		}

		/***************************************
		* Camera
		***************************************/

		CameraAttachController::CameraAttachController(gui::Module& module, const editor::IEditor& editor) :
			BaseController(module, L"Editor/AttachCamera.axm"), m_pEditor(&editor)
		{
			m_pName = GetWidgetByName<gui::Textbox<>>(L"Name");
		}

		editor::IComponentController& CameraAttachController::Clone(void) const
		{
			return *new CameraAttachController(*m_pModule, *m_pEditor);
		}

		void CameraAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<ecs::Camera>(m_pName->GetContent());
		}

		gui::BaseController& CameraAttachController::GetController(void)
		{
			return *this;
		}

		size_t CameraAttachController::GetComponentId(void) const
		{
			return ecs::Camera::family();
		}

	}
}

