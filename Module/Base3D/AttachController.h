#pragma once
#include "IComponentController.h"
#include "Gui\Textbox.h"
#include "Gui\BaseController.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace gui
	{
		class CheckBox;
	}

	namespace base3d
	{

		/***************************************
		* Position
		***************************************/

		class PositionAttachController:
			public editor::IComponentController, gui::BaseController
		{
			typedef gui::Textbox<float> PositionBox;
		public:
			PositionAttachController(gui::Module& module);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			PositionBox* m_pX, *m_pY, *m_pZ;
		};

		/***************************************
		* Rotation
		***************************************/

		class RotationAttachController :
			public editor::IComponentController, gui::BaseController
		{
			typedef gui::Textbox<float> RotationBox;
		public:
			RotationAttachController(gui::Module& module);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			RotationBox* m_pX, *m_pY, *m_pZ;
		};

		/***************************************
		* Scale
		***************************************/

		class ScaleAttachController :
			public editor::IComponentController, gui::BaseController
		{
			typedef gui::Textbox<float> ScaleBox;
		public:
			ScaleAttachController(gui::Module& module);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			ScaleBox* m_pX, *m_pY, *m_pZ;
		};

		/***************************************
		* Transform
		***************************************/

		class TransformAttachController :
			public editor::IComponentController, gui::BaseController
		{
		public:
			TransformAttachController(gui::Module& module);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;
		};

		/***************************************
		* Actor
		***************************************/

		class ActorAttachController :
			public editor::IComponentController, gui::BaseController
		{
		public:
			ActorAttachController(gui::Module& module, const editor::IEditor& editor);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			void OnPick(void);

			const editor::IEditor* m_pEditor;

			gui::Textbox<>* m_pBox;
		};

		/***************************************
		* Node
		***************************************/

		class NodeAttachController :
			public editor::IComponentController, gui::BaseController
		{
		public:
			NodeAttachController(gui::Module& module, const editor::IEditor& editor);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			void OnPick(void);

			gui::Textbox<>* m_pBox;
			gui::CheckBox* m_pLink;

			ecs::EntityHandle m_entity;
			const editor::IEditor* m_pEditor;
		};

		/***************************************
		* Camera
		***************************************/

		class CameraAttachController :
			public editor::IComponentController, gui::BaseController
		{
		public:
			CameraAttachController(gui::Module& module, const editor::IEditor& editor);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			gui::Textbox<>* m_pName;

			const editor::IEditor* m_pEditor;
		};

	}
}


