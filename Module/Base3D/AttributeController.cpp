#include "AttributeController.h"
#include "Components.h"
#include "IEditor.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace base3d
	{

		AttributeController::AttributeController(void) : m_pEntity(nullptr)
		{
			m_pX = new PositionBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pX->SigContentSet.Connect(this, &AttributeController::OnChangeX);
			m_pY = new PositionBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pY->SigContentSet.Connect(this, &AttributeController::OnChangeY);
			m_pZ = new PositionBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pZ->SigContentSet.Connect(this, &AttributeController::OnChangeZ);
		}

		AttributeController::~AttributeController(void)
		{
			delete m_pX;
			delete m_pY;
			delete m_pZ;
		}

		bool AttributeController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Position>() != nullptr;
		}

		void AttributeController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pPosition = entity.GetComponent<ecs::Position>())
			{
				m_pX->SetText(conv::ToString(pPosition->v.x, 3));
				m_pY->SetText(conv::ToString(pPosition->v.y, 3));
				m_pZ->SetText(conv::ToString(pPosition->v.z, 3));
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void AttributeController::OnChangeX(float x)
		{
			if(m_pEntity)
			{
				auto pPosition = m_pEntity->GetComponent<ecs::Position>();
				pPosition->v.x = x;
				pPosition->bDirty = true;
			}
		}

		void AttributeController::OnChangeY(float y)
		{
			if(m_pEntity)
			{
				auto pPosition = m_pEntity->GetComponent<ecs::Position>();
				pPosition->v.y = y;
				pPosition->bDirty = true;
			}
		}

		void AttributeController::OnChangeZ(float z)
		{
			if(m_pEntity)
			{
				auto pPosition = m_pEntity->GetComponent<ecs::Position>();
				pPosition->v.z = z;
				pPosition->bDirty = true;
			}
		}

		void AttributeController::OnFillTable(TableMap& mTable)
		{
			mTable[L"X"] = m_pX;
			mTable[L"Y"] = m_pY;
			mTable[L"Z"] = m_pZ;
		}

		void AttributeController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pPosition = m_pEntity->GetComponent<ecs::Position>())
				{
					m_pX->SetText(conv::ToString(pPosition->v.x, 3));
					m_pY->SetText(conv::ToString(pPosition->v.y, 3));
					m_pZ->SetText(conv::ToString(pPosition->v.z, 3));
				}
			}
		}

		/*************************************
		* SCALE
		*************************************/

		ScaleController::ScaleController(void) : m_pEntity(nullptr)
		{
			m_pX = new ScaleBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pX->SigContentSet.Connect(this, &ScaleController::OnChangeX);
			m_pY = new ScaleBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pY->SigContentSet.Connect(this, &ScaleController::OnChangeY);
			m_pZ = new ScaleBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pZ->SigContentSet.Connect(this, &ScaleController::OnChangeZ);
		}

		ScaleController::~ScaleController(void)
		{
			delete m_pX;
			delete m_pY;
			delete m_pZ;
		}

		bool ScaleController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Scale>() != nullptr;
		}

		void ScaleController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pScale = entity.GetComponent<ecs::Scale>())
			{
				m_pX->SetText(conv::ToString(pScale->v.x, 3));
				m_pY->SetText(conv::ToString(pScale->v.y, 3));
				m_pZ->SetText(conv::ToString(pScale->v.z, 3));
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void ScaleController::OnChangeX(float x)
		{
			if(m_pEntity)
			{
				auto pScale = m_pEntity->GetComponent<ecs::Scale>();
				pScale->v.x = x;
				pScale->bDirty = true;
			}
		}

		void ScaleController::OnChangeY(float y)
		{
			if(m_pEntity)
			{
				auto pScale = m_pEntity->GetComponent<ecs::Scale>();
				pScale->v.y = y;
				pScale->bDirty = true;
			}
		}

		void ScaleController::OnChangeZ(float z)
		{
			if(m_pEntity)
			{
				auto pScale = m_pEntity->GetComponent<ecs::Scale>();
				pScale->v.z = z;
				pScale->bDirty = true;
			}
		}

		void ScaleController::OnFillTable(TableMap& mTable)
		{
			mTable[L"X"] = m_pX;
			mTable[L"Y"] = m_pY;
			mTable[L"Z"] = m_pZ;
		}

		void ScaleController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pScale = m_pEntity->GetComponent<ecs::Scale>())
				{
					m_pX->SetText(conv::ToString(pScale->v.x, 3));
					m_pY->SetText(conv::ToString(pScale->v.y, 3));
					m_pZ->SetText(conv::ToString(pScale->v.z, 3));
				}
			}
		}

		/*************************************
		* SCALE
		*************************************/

		RotationController::RotationController(void) : m_pEntity(nullptr)
		{
			m_pX = new RotationBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pX->SigContentSet.Connect(this, &RotationController::OnChangeX);
			m_pY = new RotationBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pY->SigContentSet.Connect(this, &RotationController::OnChangeY);
			m_pZ = new RotationBox(0.0f, 0.0f, 1.0f, 1.0f, 0.0f);
			m_pZ->SigContentSet.Connect(this, &RotationController::OnChangeZ);
		}

		RotationController::~RotationController(void)
		{
			delete m_pX;
			delete m_pY;
			delete m_pZ;
		}

		bool RotationController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Rotation>() != nullptr;
		}

		void RotationController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pRotation = entity.GetComponent<ecs::Rotation>())
			{
				m_pX->SetText(conv::ToString(pRotation->x, 3));
				m_pY->SetText(conv::ToString(pRotation->y, 3));
				m_pZ->SetText(conv::ToString(pRotation->z, 3));
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void RotationController::OnChangeX(float x)
		{
			if(m_pEntity)
			{
				auto pRotation = m_pEntity->GetComponent<ecs::Rotation>();
				pRotation->x = x;
				pRotation->bDirty = true;
			}
		}

		void RotationController::OnChangeY(float y)
		{
			if(m_pEntity)
			{
				auto pRotation = m_pEntity->GetComponent<ecs::Rotation>();
				pRotation->y = y;
				pRotation->bDirty = true;
			}
		}

		void RotationController::OnChangeZ(float z)
		{
			if(m_pEntity)
			{
				auto pRotation = m_pEntity->GetComponent<ecs::Rotation>();
				pRotation->z = z;
				pRotation->bDirty = true;
			}
		}

		void RotationController::OnFillTable(TableMap& mTable)
		{
			mTable[L"X"] = m_pX;
			mTable[L"Y"] = m_pY;
			mTable[L"Z"] = m_pZ;
		}

		void RotationController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pRotation = m_pEntity->GetComponent<ecs::Rotation>())
				{
					m_pX->SetText(conv::ToString(pRotation->x, 3));
					m_pY->SetText(conv::ToString(pRotation->y, 3));
					m_pZ->SetText(conv::ToString(pRotation->z, 3));
				}
			}
		}

		/*************************************
		* ACTOR
		*************************************/

		ActorController::ActorController(const editor::IEditor& editor) : m_pEntity(nullptr),
			m_pEditor(&editor)
		{
			m_pModel = new ModelBox(0.0f, 0.0f, 1.0f, 1.0f, L"");
			m_pModel->SetEditable(false);
			m_pModel->SigReleased.Connect(this, &ActorController::OnBoxClicked);
		}

		ActorController::~ActorController(void)
		{
			delete m_pModel;
		}

		bool ActorController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Actor>() != nullptr;
		}

		void ActorController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pModel = entity.GetComponent<ecs::Actor>())
			{
				m_pModel->SetText(pModel->stName);
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void ActorController::OnBoxClicked(void)
		{
			if(m_pEntity)
			{
				std::wstring stName;
				if(m_pEditor->PickModel(m_pModel->GetContent(), nullptr, &stName))
				{
					auto pModel = m_pEntity->GetComponent<ecs::Actor>();
					if(pModel->stName != stName)
					{
						m_pModel->SetText(stName);
						pModel->stName = stName;
						pModel->bDirty = true;
					}
				}
			}
		}

		void ActorController::OnFillTable(TableMap& mTable)
		{
			mTable[L"Model"] = m_pModel;
		}

		void ActorController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pActor = m_pEntity->GetComponent<ecs::Actor>())
					m_pModel->SetText(pActor->stName);
			}
		}

		/*************************************
		* Node
		*************************************/

		NodeController::NodeController(const editor::IEditor& editor) : m_pEntity(nullptr),
			m_pEditor(&editor)
		{
			m_pParent = new EntityBox(0.0f, 0.0f, 1.0f, 1.0f, L"");
			m_pParent->SigReleased.Connect(this, &NodeController::OnBoxClicked);
		}

		NodeController::~NodeController(void)
		{
			delete m_pParent;
		}

		bool NodeController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Node>() != nullptr;
		}

		void NodeController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pNode = entity.GetComponent<ecs::Node>())
			{
				m_pParent->SetText(pNode->parent->GetName());
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void NodeController::OnBoxClicked(void)
		{
			if(m_pEntity)
			{
				ecs::EntityHandle entity;
				if(m_pEditor->PickEntity(m_pParent->GetContent(), &entity, nullptr))
				{
					auto pNode = m_pEntity->GetComponent<ecs::Node>();
					m_pParent->SetText(entity->GetName());
					pNode->parent = entity;
				}
			}
		}

		void NodeController::OnFillTable(TableMap& mTable)
		{
			mTable[L"Parent"] = m_pParent;
		}

		void NodeController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pParent = m_pEntity->GetComponent<ecs::Node>())
				{
					if(pParent->parent.IsValid())
						m_pParent->SetText(pParent->parent->GetName());
				}
			}
		}

		/*************************************
		* Camera
		*************************************/

		CameraController::CameraController(const editor::IEditor& editor) : m_pEditor(&editor),
			m_pEntity(nullptr)
		{
			m_pName = new gui::Textbox<>(0.0f, 0.0f, 1.0f, 1.0f, L"");
			m_pName->SigContentSet.Connect(this, &CameraController::OnChangeName);
		}

		CameraController::~CameraController(void)
		{
			delete m_pName;
		}

		bool CameraController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<ecs::Camera>() != nullptr;
		}

		void CameraController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pCamera = entity.GetComponent<ecs::Camera>())
			{
				m_pName->SetText(pCamera->stName);
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void CameraController::OnFillTable(TableMap& mTable)
		{
			mTable[L"Name"] = m_pName;
		}

		void CameraController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pCamera = m_pEntity->GetComponent<ecs::Camera>())
				{
					m_pName->SetText(pCamera->stName);
				}
			}
		}

		void CameraController::OnChangeName(std::wstring stName)
		{
			auto& camera = *m_pEntity->GetComponent<ecs::Camera>();
			camera.stName = stName;
		}

	}
}
