#pragma once
#include "IComponentAttribute.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace base3d
	{

		/**************************************
		* POSITION
		***************************************/

		class AttributeController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<float> PositionBox;
		public:

			AttributeController(void);
			~AttributeController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnChangeX(float x);
			void OnChangeY(float y);
			void OnChangeZ(float z);

			const ecs::Entity* m_pEntity;
			PositionBox* m_pX, *m_pY, *m_pZ;
		};

		/**************************************
		* POSITION
		***************************************/

		class ScaleController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<float> ScaleBox;
		public:

			ScaleController(void);
			~ScaleController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnChangeX(float x);
			void OnChangeY(float y);
			void OnChangeZ(float z);

			const ecs::Entity* m_pEntity;
			ScaleBox* m_pX, *m_pY, *m_pZ;
		};

		/**************************************
		* Rotation
		***************************************/

		class RotationController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<float> RotationBox;
		public:

			RotationController(void);
			~RotationController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnChangeX(float x);
			void OnChangeY(float y);
			void OnChangeZ(float z);

			const ecs::Entity* m_pEntity;
			RotationBox* m_pX, *m_pY, *m_pZ;
		};

		/**************************************
		* Actor
		***************************************/

		class ActorController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<> ModelBox;
		public:

			ActorController(const editor::IEditor& editor);
			~ActorController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnBoxClicked(void);

			const ecs::Entity* m_pEntity;
			const editor::IEditor* m_pEditor;
			ModelBox* m_pModel;
		};

		/**************************************
		* Node
		***************************************/

		class NodeController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<> EntityBox;
		public:

			NodeController(const editor::IEditor& editor);
			~NodeController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnBoxClicked(void);

			const ecs::Entity* m_pEntity;
			const editor::IEditor* m_pEditor;
			EntityBox* m_pParent;
		};

		/**************************************
		* Camera
		***************************************/

		class CameraController :
			public editor::IComponentAttribute
		{
		public:

			CameraController(const editor::IEditor& editor);
			~CameraController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnChangeName(std::wstring stName);

			const ecs::Entity* m_pEntity;
			const editor::IEditor* m_pEditor;
			gui::Textbox<>* m_pName;
		};

	}
}

