#include "Base3DModule.h"

#include "ActorSystem.h"
#include "ActorRenderSystem.h"
#include "TransformSystem.h"
#include "CameraSystem.h"
#include "BoundingSystem.h"
#include "SceneGraphSystem.h"
#include "AnimationSystem.h"
#include "RegisterScript.h"

#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "Messages.h"
#include "Core\BaseContext.h"
#include "Core\SettingLoader.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\MessageRegistry.h"
#include "Entity\MessageFactory.h"
#include "Entity\QueryRegistry.h"
#include "Entity\QueryFactory.h"
#include "Gfx\Context.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\Screen.h"
#include "Input\Loader.h"
#include "Render\ILoader.h"
#include "Render\IRenderer.h"
#include "Render\Postprocessor.h"
#include "Script\Core.h"

namespace acl
{
	namespace modules
	{

		Base3DModule::Base3DModule(void): m_pSystems(nullptr)
		{
		}

		void Base3DModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void Base3DModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
			context.postprocess.SelectRenderGroup(L"PostProcess");
		}

		void Base3DModule::OnInit(const core::GameStateContext& context)
		{
			context.gfx.effect.SetMesh(L"screen quad");
			context.render.postprocess.SelectMesh(*context.gfx.resources.meshes[L"screen quad"]);

			m_pSystems = &context.ecs.systems;

			// load settings
			context.core.settingLoader.Load(L"Settings.axm");

			// load game view camera
			context.input.loader.Load(L"Input.axm");

			// system
			m_pSystems->AddSystem<ecs::AnimationSystem>(context.gfx.resources.animations);
			m_pSystems->AddSystem<ecs::ActorSystem>(context.gfx.resources.models);
			m_pSystems->AddSystem<ecs::ActorRenderSystem>(context.render.renderer);
			m_pSystems->AddSystem<ecs::TransformSystem>();
			m_pSystems->AddSystem<ecs::CameraSystem>(context.gfx.screen.GetSize(), context.core.settings);
			m_pSystems->AddSystem<ecs::BoundingSystem>();
			m_pSystems->AddSystem<ecs::SceneGraphSystem>();

			// io
			ecs::Saver::RegisterSubRoutine<ecs::Base3DSaveRoutine>(L"Base3D");
			ecs::Loader::RegisterSubRoutine<ecs::Base3DLoadRoutine>(L"Base3D");

			// components
			ecs::ComponentRegistry::RegisterComponent<ecs::Position>(L"Position");
			context.ecs.componentFactory.Register<ecs::Position, float, float ,float>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Scale>(L"Scale");
			context.ecs.componentFactory.Register<ecs::Scale, float, float ,float>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Rotation>(L"Rotation");
			context.ecs.componentFactory.Register<ecs::Rotation, float, float, float>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Direction>(L"Direction");
			context.ecs.componentFactory.Register<ecs::Direction, float, float ,float>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Transform>(L"Transform");
			context.ecs.componentFactory.Register<ecs::Transform>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Actor>(L"Actor");
			context.ecs.componentFactory.Register<ecs::Actor, const wchar_t*>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Camera>(L"Camera");
			context.ecs.componentFactory.Register<ecs::Camera, const wchar_t*>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Bounding>(L"Bounding");
			context.ecs.componentFactory.Register<ecs::Bounding, math::IVolume&>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Node>(L"Node");
			context.ecs.componentFactory.Register<ecs::Node, const ecs::EntityHandle&, bool>();

			// messages
			ecs::MessageRegistry::RegisterMessage<ecs::UpdateCamera>(L"UpdateCamera");
			ecs::MessageRegistry::RegisterMessage<ecs::RenderScene>(L"RenderScene");
			context.ecs.messageFactory.Register<ecs::RenderScene, const gfx::Camera&, render::IStage&, unsigned int>();
			ecs::MessageRegistry::RegisterMessage<ecs::ActivateCamera>(L"ActivateCamera");
			context.ecs.messageFactory.Register<ecs::ActivateCamera, const wchar_t*>();

			// load scripts
			m_scriptGroup = context.script.core.GetConfigGroup("Base3D");
			m_scriptGroup.Begin();
			base3d::RegisterScript(context.ecs.messages, context.script.core);
			m_scriptGroup.End();

			context.script.loader.FromFile(L"FreeCameraController", L"Scripts/FreeCameraController.as");
			context.script.loader.FromFile(L"FollowCameraController", L"Scripts/FollowCameraController.as");
		}

		void Base3DModule::OnUninit(const core::GameStateContext& context)
		{
			m_scriptGroup.Remove();

			// render
			context.render.renderer.RemoveGroup(L"Scene");
			context.render.renderer.RemoveGroup(L"Prelight");
			context.render.renderer.RemoveGroup(L"Light");
			context.render.renderer.RemoveGroup(L"Postlight");
			context.render.renderer.RemoveGroup(L"Nolight");
			context.render.renderer.RemoveGroup(L"PreProcess");
			context.render.renderer.RemoveGroup(L"PostProcess");
			context.render.renderer.RemoveGroup(L"NoProcess");
			context.render.renderer.RemoveGroup(L"PreGui");
			context.render.renderer.RemoveGroup(L"Gui");

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Position");
			context.ecs.componentFactory.Unregister<ecs::Position>();
			ecs::ComponentRegistry::UnregisterComponent(L"Scale");
			context.ecs.componentFactory.Unregister<ecs::Scale>();
			ecs::ComponentRegistry::UnregisterComponent(L"Rotation");
			context.ecs.componentFactory.Unregister<ecs::Rotation>();
			ecs::ComponentRegistry::UnregisterComponent(L"Direction");
			context.ecs.componentFactory.Unregister<ecs::Direction>();
			ecs::ComponentRegistry::UnregisterComponent(L"Transform");
			context.ecs.componentFactory.Unregister<ecs::Transform>();
			ecs::ComponentRegistry::UnregisterComponent(L"Actor");
			context.ecs.componentFactory.Unregister<ecs::Actor>();
			ecs::ComponentRegistry::UnregisterComponent(L"Camera");
			context.ecs.componentFactory.Unregister<ecs::Camera>();
			ecs::ComponentRegistry::UnregisterComponent(L"Bounding");
			context.ecs.componentFactory.Unregister<ecs::Bounding>();
			ecs::ComponentRegistry::UnregisterComponent(L"Node");
			context.ecs.componentFactory.Unregister<ecs::Node>();

			// messages
			ecs::MessageRegistry::UnregisterMessage(L"UpdateCamera");
			ecs::MessageRegistry::UnregisterMessage(L"RenderScene");
			context.ecs.messageFactory.Unregister<ecs::RenderScene>();
			ecs::MessageRegistry::UnregisterMessage(L"ActivateCamera");
			context.ecs.messageFactory.Unregister<ecs::ActivateCamera>();

			// system
			m_pSystems->RemoveSystem<ecs::AnimationSystem>();
			m_pSystems->RemoveSystem<ecs::CameraSystem>();
			m_pSystems->RemoveSystem<ecs::ActorSystem>();
			m_pSystems->RemoveSystem<ecs::ActorRenderSystem>();
			m_pSystems->RemoveSystem<ecs::TransformSystem>();
			m_pSystems->RemoveSystem<ecs::BoundingSystem>();
			m_pSystems->RemoveSystem<ecs::SceneGraphSystem>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Base3D");
			ecs::Loader::UnregisterSubRoutine(L"Base3D");

			m_pSystems = nullptr;
		}

		void Base3DModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::CameraSystem>(dt);
			m_pSystems->UpdateSystem<ecs::SceneGraphSystem>(dt);
			m_pSystems->UpdateSystem<ecs::TransformSystem>(dt);
			m_pSystems->UpdateSystem<ecs::AnimationSystem>(dt);
			m_pSystems->UpdateSystem<ecs::ActorSystem>(dt);
			m_pSystems->UpdateSystem<ecs::BoundingSystem>(dt);
		}

		void Base3DModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::ActorRenderSystem>(0.0f);
		}

	}
}
