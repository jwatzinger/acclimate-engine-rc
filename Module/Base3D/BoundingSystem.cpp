#include "BoundingSystem.h"
#include "Components.h"
#include "Queries.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Entity\EntityManager.h"
#include "Math\Ray.h"

namespace acl
{
    namespace ecs
    {

        void BoundingSystem::Init(MessageManager& messageManager)
        {
            messageManager.RegisterQuery<CollideQuery>(*this);
            messageManager.RegisterQuery<PickQuery>(*this);
        }

        void BoundingSystem::Update(double dt)
        {
	        auto vEntities = m_pEntities->EntitiesWithComponents<Bounding, Position>();

	        //loop through those entities to check for collision
	        for(auto& entity: vEntities)
            {
                Bounding& bounding = *entity->GetComponent<Bounding>();
				const Position& position = *entity->GetComponent<Position>();

                bounding.pVolume->SetCenter(position.v);
            }
        }

        bool BoundingSystem::HandleQuery(BaseQuery& query) const
        {
            // collision request
            if( const CollideQuery* pCollide = query.Convert<CollideQuery>() )
            {
                //get entities with collision component
	            auto vEntities = m_pEntities->EntitiesWithComponents<Bounding>();

	            //loop through those entities to check for collision
	            for(auto& entity: vEntities)
                {
					const Bounding& bounding = *entity->GetComponent<Bounding>();

                    if(bounding.pVolume->IsInside(pCollide->volume))
                    {
                        return true;
                    }
                }

                return false;
            }
            // pick request
            else if( PickQuery* pPick = query.Convert<PickQuery>() )
            {
                //get entities with collision component
	            auto vEntities = m_pEntities->EntitiesWithComponents<Bounding>();

                math::Vector3 vIntersect;
                float cameraDistance = 5000.0f;
                //loop through those entities to check for collision
	            for(auto& entity: vEntities)
                {
                    const Bounding& bounding = *entity->GetComponent<Bounding>();

                    if(bounding.pVolume->Intersect(pPick->ray, &vIntersect))
                    {
                        float distance = (vIntersect - pPick->ray.m_vOrigin).length();

                        if(distance <= cameraDistance)
                        {
                            pPick->entity = entity;
                            cameraDistance = distance;
                        }
                    }
                }

                if(pPick->entity.IsValid())
                    return true;
                else
                    return false;
            }

	        return false;
        }

    }
}