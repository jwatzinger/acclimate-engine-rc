#pragma once
#include "Entity\System.h"

namespace acl
{
    namespace ecs
    {

        class BoundingSystem :
            public System<BoundingSystem>
        {
        public:

            void Init(MessageManager& messageManager);

            void Update(double dt);

            bool HandleQuery(BaseQuery& query) const;
        };

    }
}

