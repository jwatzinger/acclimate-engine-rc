#include "CameraSystem.h"
#include "Components.h"
#include "Messages.h"
#include "Queries.h"
#include "Core\SettingModule.h"
#include "Entity\MessageManager.h"
#include "Entity\EntityManager.h"
#include "Gfx\Camera.h"
#include "Math\Utility.h"

namespace acl
{
	namespace ecs
	{

		CameraSystem::CameraSystem(const math::Vector2& vScreenSize, core::SettingModule& settings): m_vScreenSize(vScreenSize),
			m_viewDistance(1000.0f)
		{
			settings.SigUpdateSettings.Connect(this, &CameraSystem::OnHandleSettings);
		}

		void CameraSystem::Init(MessageManager& messageManager)
		{
			messageManager.Subscribe<ActivateCamera>(*this);
			messageManager.Subscribe<ResolutionChange>(*this);

			messageManager.RegisterQuery<ActiveCameraQuery>(*this);
		}

		void CameraSystem::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<Camera>();

			for(auto& entity : vEntities)
			{
				Camera* pCamera = entity->GetComponent<Camera>();

				if(!pCamera->pCamera)
				{
					pCamera->pCamera = new gfx::Camera(m_vScreenSize, math::Vector3(0.0f, 0.0f, 0.0f), math::PI / 4.0f);
					pCamera->pCamera->SetClipPlanes(0.5f, m_viewDistance);
				}
			}

			if(m_camera.IsValid())
			{
				if(auto pCamera = m_camera->GetComponent<ecs::Camera>())
				{
					if(pCamera->pCamera && pCamera->bDirty)
					{
						m_pMessages->DeliverMessage<UpdateCamera>(*pCamera->pCamera);
						pCamera->bDirty = false;
					}
				}
			}
		}

		void CameraSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(const ActivateCamera* pActivate = message.Convert<ActivateCamera>())
			{
				auto vEntities = m_pEntities->EntitiesWithComponents<Camera>();

				for(auto& entity : vEntities)
				{
					Camera* pCamera = entity->GetComponent<Camera>();
					
					if(pCamera->stName == pActivate->stName)
					{
						if(m_camera.IsValid())
						{
							if(auto pCamera = m_camera->GetComponent<Camera>())
								pCamera->isActive = false;
						}
							
						pCamera->bDirty = true;
						pCamera->isActive = true;
						m_camera = entity;
					}
				}
			}
			else if(const ResolutionChange* pChange = message.Convert<ResolutionChange>())
			{
				m_vScreenSize = pChange->vSize;

				auto vEntities = m_pEntities->EntitiesWithComponents<Camera>();

				for(auto& entity : vEntities)
				{
					const Camera* pCamera = entity->GetComponent<Camera>();

					if(pCamera->pCamera)
						pCamera->pCamera->SetScreenSize(pChange->vSize);
				}
			}
		}

		bool CameraSystem::HandleQuery(BaseQuery& query) const
		{
			if(auto pCamera = query.Convert<ActiveCameraQuery>())
			{
				if(m_camera.IsValid())
				{
					pCamera->camera = m_camera;
					return true;
				}
				else
					return false;
			}

			return false;
		}

		void CameraSystem::OnHandleSettings(const core::SettingModule& settings)
		{
			auto pSetting = settings.GetSetting(L"viewDistance");
			ACL_ASSERT(pSetting);

			m_viewDistance = pSetting->GetData<float>();

			auto vEntities = m_pEntities->EntitiesWithComponents<Camera>();

			for(auto& entity : vEntities)
			{
			}
		}

	}
}