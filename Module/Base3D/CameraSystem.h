#pragma once
#include <map>
#include "Entity\System.h"
#include "Entity\Entity.h"
#include "Math\Vector.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace ecs
	{
		struct Camera;

		class CameraSystem : 
			public System<CameraSystem>
		{
		public:
			CameraSystem(const math::Vector2& vScreenSize, core::SettingModule& settings);

			void Init(MessageManager& messageManager);
			void Update(double dt);
			void ReceiveMessage(const BaseMessage& message);
			bool HandleQuery(BaseQuery& query) const;

		protected:

			void OnHandleSettings(const core::SettingModule& settings);

			EntityHandle m_camera;

			float m_viewDistance;
			math::Vector2 m_vScreenSize;
		};
	
	}
}