#include "Components.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\AnimationController.h"
#include "Script\Instance.h"

namespace acl
{
    namespace ecs
    {
        /*******************************************
		* Position
		********************************************/

		Position::Position(const math::Vector3& vPos): v(vPos), bDirty(true)
		{
		}

        Position::Position(float x, float y, float z): v(x, y, z), bDirty(true) 
        {
        }

		void* Position::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Vector")
			{
				*type = &typeid(v);
				return &v;
			}
			else if(stName == L"Dirty")
			{
				*type = &typeid(bDirty);
				return &bDirty;
			}

			return nullptr;
		}

        /*******************************************
		* Direction
		********************************************/

	    Direction::Direction(float x, float y, float z): v(x, y, z), bDirty(true)
        {
        }

        Direction::Direction(const math::Vector3& vDir): v(vDir), bDirty(true)
        {
        }

		void* Direction::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Vector")
			{
				*type = &typeid(v);
				return &v;
			}

			return nullptr;
		}

        /*******************************************
		* Rotation
		********************************************/

	    Rotation::Rotation(float x, float y, float z): x(x), y(y), z(z), bDirty(true)
        {
        }

		void* Rotation::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"X")
			{
				*type = &typeid(x);
				return &x;
			}
			else if(stName == L"Y")
			{
				*type = &typeid(y);
				return &y;
			}
			else if(stName == L"Z")
			{
				*type = &typeid(z);
				return &z;
			}
			else if(stName == L"Quaternion")
			{
				*type = &typeid(quaternion);
				return &quaternion;
			}
			else if(stName == L"Dirty")
			{
				*type = &typeid(bDirty);
				return &bDirty;
			}

			return nullptr;
		}

        /*******************************************
		* Scale
		********************************************/

	    Scale::Scale(float x, float y, float z): v(x, y, z), bDirty(true) 
        {
		}

		Scale::Scale(const math::Vector3& vScale): v(vScale), bDirty(true) 
        {
		}

		/*******************************************
		* Transform
		********************************************/

		void* Transform::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Matrix")
			{
				*type = &typeid(mTransform);
				return &mTransform;
			}

			return nullptr;
		}

        /*******************************************
		* Bounding
		********************************************/

	    Bounding::Bounding(math::IVolume& volume) : pVolume(&volume) 
        {
        }

		/*******************************************
		* Node
		********************************************/

		Node::Node(const EntityHandle& parent, bool bLinkLifeTime): parent(parent), bLinkLifeTime(bLinkLifeTime)
		{
		}

        /************************************************
        * Actor 
        ************************************************/

	    Actor::Actor(const std::wstring& stName): stName(stName), bDirty(true), pModel(nullptr), bVisible(true)
        {
        }

		Actor::~Actor(void)
		{
			delete pModel;
		}

		void* Actor::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Model")
			{
				*type = &typeid(gfx::ModelInstance);
				return pModel;
			}

			return nullptr;
		}

		/************************************************
        * Camera
        ************************************************/

		Camera::Camera(const std::wstring& stName): stName(stName), pCamera(nullptr),
			bDirty(true), isActive(false)
		{
		}

		Camera::~Camera(void)
		{
			delete pCamera;
		}

		/************************************************
		* Animation
		************************************************/

		Animation::Animation(const std::wstring& stAnimationSet, const std::wstring& stDefaultAnimation) : pController(nullptr),
			stAnimationSet(stAnimationSet), stDefaultAnimation(stDefaultAnimation), isPlaying(true), speed(1.0f)
		{
		}

		Animation::~Animation(void)
		{
			delete pController;
		}

    }
}
