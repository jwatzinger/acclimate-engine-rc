#pragma once
#include <string>
#include <map>
#include "Gfx\Camera.h"
#include "Entity\Component.h"
#include "Entity\Entity.h"
#include "Math\IVolume.h"
#include "Math\Vector3.h"
#include "Math\Matrix.h"
#include "Math\Quaternion.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
		class ITexture;
		class AnimationController;
	}

	namespace script
	{
		class Instance;
	}

    namespace ecs
    {
        /*******************************************
		* Position
		********************************************/

        struct Position : Component<Position>
        {
			Position(const math::Vector3& vPos);
	        Position(float x, float y, float z);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

			math::Vector3 v;
			math::Matrix mTranslation;
	        bool bDirty;
        };

        /*******************************************
		* Direction
		********************************************/

        struct Direction : Component<Direction>
        {
	        Direction(float x, float y, float z);
            Direction(const math::Vector3& vDir);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

	        math::Vector3 v;
            math::Matrix mRotation;
            bool bDirty;
        };

        /*******************************************
		* Rotation
		********************************************/

        struct Rotation : Component<Rotation>
        {
	        Rotation(float x, float y, float z);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

	        float x, y, z;
            math::Matrix mRotation;
			math::Quaternion quaternion;
            bool bDirty;
        };

        /*******************************************
		* Scale
		********************************************/

        struct Scale : Component<Scale>
        {
	        Scale(float x, float y, float z);
			Scale(const math::Vector3& v);

	        math::Vector3 v;
			math::Matrix mScaling;
			bool bDirty;
        };

		/*******************************************
		* Transform
		********************************************/

		struct Transform : Component<Transform>
		{
			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

			math::Matrix mTransform;
			math::Vector3 vOffset;
		};

		/*******************************************
		* Node
		********************************************/

		class Entity;

		struct Node : Component<Node>
		{
			Node(const EntityHandle& parent, bool bLinkLifeTime);

			bool bLinkLifeTime;
			EntityHandle parent;
		};

        /*******************************************
		* Bounding
		********************************************/

        struct Bounding : Component<Bounding>
        {
	        Bounding(math::IVolume& box);

	        math::IVolume* pVolume;
        };

        /************************************************
        * Actor 
        ************************************************/

        struct Actor : Component<Actor>
        {
	        Actor(const std::wstring& stName);
			~Actor(void);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

			bool bDirty, bVisible;
			std::wstring stName;
			gfx::ModelInstance* pModel;
        };

		/************************************************
        * Camera
        ************************************************/

		struct Camera : Component<Camera>
		{
			Camera(const std::wstring& stName);
			~Camera(void);

			std::wstring stName;
			gfx::Camera* pCamera;
			bool bDirty, isActive;
		};

		/************************************************
		* Animation
		************************************************/

		struct Animation : Component<Animation>
		{
			Animation(const std::wstring& stAnimationSet, const std::wstring& stDefaultAnimation);
			~Animation(void);

			std::wstring stAnimationSet, stDefaultAnimation, stNextAnimation;
			gfx::AnimationController* pController;
			bool isPlaying;
			float speed;
		};
    }
}