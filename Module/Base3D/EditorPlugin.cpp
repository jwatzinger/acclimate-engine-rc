#include "EditorPlugin.h"
#include "GameViewController.h"
#include "IComponentRegistry.h"
#include "IEditor.h"
#include "AttachController.h"
#include "AttributeController.h"

namespace acl
{
	namespace base3d
	{

		EditorPlugin::EditorPlugin(void): m_pGameView(nullptr)
		{
		}

		void EditorPlugin::OnInit(editor::IEditor& editor)
		{
			m_pEditor = &editor;

			auto& module = editor.GetModule();
			auto& registry = editor.GetComponentRegistry();
			registry.AddComponentAttachController(L"Position", *new PositionAttachController(module));
			registry.AddComponentAttachController(L"Rotation", *new RotationAttachController(module));
			registry.AddComponentAttachController(L"Scale", *new ScaleAttachController(module));
			registry.AddComponentAttachController(L"Transform", *new TransformAttachController(module));
			registry.AddComponentAttachController(L"Actor", *new ActorAttachController(module, editor));
			registry.AddComponentAttachController(L"Node", *new NodeAttachController(module, editor));
			registry.AddComponentAttachController(L"Camera", *new CameraAttachController(module, editor));
			// attriutes
			registry.AddComponentAtttributeController(L"Position", *new AttributeController);
			registry.AddComponentAtttributeController(L"Scale", *new ScaleController);
			registry.AddComponentAtttributeController(L"Rotation", *new RotationController);
			registry.AddComponentAtttributeController(L"Actor", *new ActorController(editor));
			registry.AddComponentAtttributeController(L"Node", *new NodeController(editor));
			registry.AddComponentAtttributeController(L"Camera", *new CameraController(editor));

			m_pGameView = new GameViewController(module, editor);
			editor.SetGameView(m_pGameView);
		}
		
		void EditorPlugin::OnUpdate(void)
		{
			m_pGameView->Update();
		}

		void EditorPlugin::OnUninit(void)
		{
			m_pEditor->SetGameView(nullptr);
			delete m_pGameView;

			auto& registry = m_pEditor->GetComponentRegistry();
			registry.RemoveComponentAttachController(L"Position");
			registry.RemoveComponentAttachController(L"Rotation");
			registry.RemoveComponentAttachController(L"Scale");
			registry.RemoveComponentAttachController(L"Transform");
			registry.RemoveComponentAttachController(L"Actor");
			registry.RemoveComponentAttachController(L"Node");
			registry.RemoveComponentAttachController(L"Camera");
			// attriutes
			registry.RemoveComponentAtttributeController(L"Position");
			registry.RemoveComponentAtttributeController(L"Scale");
			registry.RemoveComponentAtttributeController(L"Rotation");
			registry.RemoveComponentAtttributeController(L"Actor");
			registry.RemoveComponentAtttributeController(L"Node");
			registry.RemoveComponentAtttributeController(L"Camera");
		}

		void EditorPlugin::OnBeginTest(void)
		{
		}

		void EditorPlugin::OnEndTest(void)
		{
		}

	}
}

