#include "Export.h"
#include "Base3DModule.h"
#include "EditorPlugin.h"

core::IModule& CreateModule(void)
{
	return *new modules::Base3DModule();
}

editor::IPlugin& CreatePlugin(core::IModule& module)
{
	return *new base3d::EditorPlugin();
}