#include "GameViewController.h"
#include "Messages.h"
#include "Components.h"
#include "IEditor.h"
#include "Entity\MessageManager.h"
#include "Math\Utility.h"
#include "Gfx\Context.h"
#include "Gfx\ModelInstance.h"
#include "Gui\ContextMenu.h"
#include "Gui\Dock.h"
#include "Gui\Input.h"
#include "Render\IRenderer.h"

namespace acl
{
	namespace base3d
	{

		GameViewController::GameViewController(gui::Module& module, editor::IEditor& editor) : BaseController(module, L"Editor/GameView.axm"), 
			m_camera(math::Vector2(0, 0), math::Vector3(5.0f, 0, 0.0f), math::PI/4.0f), m_angleX(0.0f), m_angleY(0.0f), m_pMaterial(nullptr), 
			m_pInstance(nullptr), m_pStage(nullptr), m_gizmo(GizmoType::TRANSLATE), m_pEditor(&editor)
		{
			m_camera.SetLookAt(math::Vector3(0.0f, 0.0f, 0.0f));

			m_pView = GetWidgetByName(L"View");
			m_pView->SetFocusState(gui::FocusState::KEEP_FOCUS);
			m_pView->SigKey.Connect(this, &GameViewController::OnKeyPress);
			m_pView->SigDrag.Connect(this, &GameViewController::OnRotate);
			m_pView->SigWheelMoved.Connect(this, &GameViewController::OnZoom);

			auto& context = AddWidget<gui::ContextMenu>(0.02f);
			context.AddItem(L"Add entity");
			context.InsertSeperator();

			// translate
			auto& translate = context.AddItem(L"Translate");
			translate.SigReleased.Connect(this, &GameViewController::OnSetTranslate);
			translate.SigShortcut.Connect(this, &GameViewController::OnSetTranslate);
			translate.SetShortcut(gui::ComboKeys::NONE, 'Q', gui::ShortcutState::ALWAYS);

			// rotate
			auto& rotate = context.AddItem(L"Rotate");
			rotate.SigReleased.Connect(this, &GameViewController::OnSetRotate);
			rotate.SigShortcut.Connect(this, &GameViewController::OnSetRotate);
			rotate.SetShortcut(gui::ComboKeys::NONE, 'W', gui::ShortcutState::ALWAYS);

			// scale
			auto& scale = context.AddItem(L"Scale");
			scale.SigReleased.Connect(this, &GameViewController::OnSetScale);
			scale.SigShortcut.Connect(this, &GameViewController::OnSetScale);
			scale.SetShortcut(gui::ComboKeys::NONE, 'E', gui::ShortcutState::ALWAYS);

			m_pView->SetContextMenu(&context);
		}

		gui::Dock& GameViewController::GetDock(void)
		{
			return *GetWidgetByName<gui::Dock>(L"MainWindow");
		}

		void GameViewController::OnKeyPress(gui::Keys key)
		{
			switch(key)
			{
			case gui::Keys::LEFT_ARROW:
				PerformGizmo(GizmoAxis::Z, -1);
				return;
			case gui::Keys::RIGHT_ARROW:
				PerformGizmo(GizmoAxis::Z, 1);
				return;
			case gui::Keys::UP_ARROW:
				PerformGizmo(GizmoAxis::X, -1);
				return;
			case gui::Keys::DOWN_ARROW:
				PerformGizmo(GizmoAxis::X, 1);
				return;
			}
		}

		void GameViewController::PerformGizmo(GizmoAxis axis, int direction)
		{
			if(m_entity.IsValid())
			{
				switch(m_gizmo)
				{
				case GizmoType::TRANSLATE:
					if(auto pPosition = m_entity->GetComponent<ecs::Position>())
					{
						const float moveDistance = 0.1f * direction;
						switch(axis)
						{
						case GizmoAxis::X:
							pPosition->v.x += moveDistance;
							pPosition->bDirty = true;
							break;
						case GizmoAxis::Y:
							pPosition->v.y += moveDistance;
							pPosition->bDirty = true;
							break;
						case GizmoAxis::Z:
							pPosition->v.z += moveDistance;
							pPosition->bDirty = true;
							break;
						}
					}
					break;
				case GizmoType::ROTATE:
					if(auto pRotation = m_entity->GetComponent<ecs::Rotation>())
					{
						const float rotateDistance = 1.0f * direction;
						switch(axis)
						{
						case GizmoAxis::X:
							pRotation->z -= rotateDistance;
							pRotation->bDirty = true;
							break;
						case GizmoAxis::Y:
							pRotation->y += rotateDistance;
							pRotation->bDirty = true;
							break;
						case GizmoAxis::Z:
							pRotation->x += rotateDistance;
							pRotation->bDirty = true;
							break;
						}
					}
					break;
				case GizmoType::SCALE:
					if(auto pScale = m_entity->GetComponent<ecs::Scale>())
					{
						const float scaleDistance = 0.01f * direction;
						switch(axis)
						{
						case GizmoAxis::X:
							pScale->v.x += scaleDistance;
							pScale->bDirty = true;
							break;
						case GizmoAxis::Y:
							pScale->v.y += scaleDistance;
							pScale->bDirty = true;
							break;
						case GizmoAxis::Z:
							pScale->v.z += scaleDistance;
							pScale->bDirty = true;
							break;
						}
					}
					break;
				}

				m_pEditor->OnEntityChanged();
			}
		}

		void GameViewController::OnInit(const gfx::Context& gfx, const render::IRenderer& renderer)
		{
			m_pMaterial = gfx.resources.materials[L"MeshPreview"];
			m_pStage = renderer.GetStage(L"world");
		}

		void GameViewController::OnSelectEntity(const ecs::EntityHandle* pEntity)
		{
			if(pEntity)
				m_entity = *pEntity;
			else
				m_entity.Invalidate();
		}

		void GameViewController::OnGotoEntity(const ecs::Entity& entity)
		{
			if(auto pPosition = entity.GetComponent<ecs::Position>())
			{
				const math::Vector3 vDir = m_camera.GetDirection() * m_camera.GetDistance();
				m_camera.SetLookAt(pPosition->v);
				m_camera.SetPosition(pPosition->v - vDir);
				m_pView->OnFocus(true);
			}
		}

		ecs::EntityHandle GameViewController::OnPickEntity(const ecs::EntityManager& entities, const math::Vector2& vPosition)
		{
			return ecs::EntityHandle();
		}

		void GameViewController::Update(void)
		{
			m_camera.SetScreenSize(math::Vector2(m_pView->GetWidth(), m_pView->GetHeight()));
			m_pEditor->GetMessageManager().DeliverMessage<ecs::UpdateCamera>(m_camera);
			m_pView->MarkDirtyRect();

			if(m_entity.IsValid())
			{
				if(auto pActor = m_entity->GetComponent<ecs::Actor>())
				{
					if(auto pTransform = m_entity->GetComponent<ecs::Transform>())
					{
						delete m_pInstance;
						if(pActor->pModel)
						{
							auto& model = pActor->pModel->GetParent()->Clone();
							model.SetMaterial(0, m_pMaterial);

							m_pInstance = &model.MakeUniqueInstance();

							m_pInstance->SetVertexConstant(0, (float*)&pTransform->mTransform, 4);
							m_pInstance->SetPixelConstant(0, (float*)&gfx::FColor(1.0f, 0.0f, 0.0f), 1);
						}
						else
							m_pInstance = nullptr;
					}
				}
			}
		}

		void GameViewController::OnRender(void) const
		{
			if(m_pInstance)
				m_pInstance->Draw(*m_pStage, 0);
		}

		void GameViewController::OnRotate(const math::Vector2& vDistance)
		{	
			//access camerea attributes
			const math::Matrix mView = m_camera.GetViewMatrix().inverse();

			if(m_pModule->ComboKeysActive(gui::ComboKeys::CTRL))
			{
				const float dist = m_camera.GetDistance();
				//screen space drag vector
				math::Vector3 vDrag(-(float)vDistance.x, (float)vDistance.y, 0.0f);

				//transform drag vector to world space
				mView.TransformNormal(vDrag);
				vDrag.Normalize();
				vDrag *= max(1.0f, sqrt(dist)) / 2.0f;
				
				const math::Vector3 vPos = m_camera.GetPosition() + vDrag;
				const math::Vector3 vLook = m_camera.GetLookAt() + vDrag;
				m_camera.SetPosition(vPos);
				m_camera.SetLookAt(vLook);
			}
			else
			{
				//setup vectors
				math::Vector3 vDir(m_camera.GetDirection()), vLookAt(m_camera.GetLookAt()), vUp(m_camera.GetUp());

				math::Vector3 vDrag((float)vDistance.x, (float)vDistance.y, 0.0f);

				//transform mouse drag vector to object space
				mView.TransformNormal(vDrag);
				vDrag.Normalize();

				math::Vector3 vAxis = vDrag.Cross(vDir);
				vAxis.Normalize();

				math::Matrix mRotation;
				//rotate around axis
				mRotation = math::MatRotationAxis(vAxis, math::degToRad(vDistance.length()));
				//transform direction and up
				mRotation.TransformNormal(vDir);
				//recalculate position
				const float cameraDist = m_camera.GetDistance();

				m_camera.SetPosition(vLookAt - vDir*cameraDist);

				//recalculate up
				math::Vector3 vUpAxis = vUp.Cross(vDir);
				//calculate "rotation" axis for up vector
				vUpAxis.y = 0.0f;
				vUpAxis.Normalize();

				//cross direction and up axis for up vector
				vUp = vDir.Cross(vUpAxis);
				m_camera.SetUp(vUp);
			}
		}

		void GameViewController::OnZoom(int distance)
		{
			const float distanceFactor = 0.05f;

			if(m_camera.GetDistance() <= -(float)distance*distanceFactor)
				return;

			const math::Vector3& vDir(m_camera.GetDirection());
			const math::Vector3 vPos = m_camera.GetPosition() - vDir*(float)distance*distanceFactor;

			m_camera.SetPosition(vPos);
		}

		void GameViewController::OnSetTranslate(void)
		{
			if(m_entity.IsValid() && m_entity->GetComponent<ecs::Position>())
				m_gizmo = GizmoType::TRANSLATE;
		}

		void GameViewController::OnSetRotate(void)
		{
			if(m_entity.IsValid() && m_entity->GetComponent<ecs::Rotation>())
				m_gizmo = GizmoType::ROTATE;
		}

		void GameViewController::OnSetScale(void)
		{
			if(m_entity.IsValid() && m_entity->GetComponent<ecs::Scale>())
				m_gizmo = GizmoType::SCALE;
		}

		core::Signal<>& GameViewController::GetClickedSignal(void)
		{
			return m_pView->SigClicked;
		}

	}
}
