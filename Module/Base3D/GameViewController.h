#pragma once
#include "Entity\Entity.h"
#include "Gfx\Camera.h"
#include "Gui\BaseController.h"
#include "IGameView.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace gfx
	{
		class IMaterial;
		class IModel;
		class ModelInstance;
	}

	namespace render
	{
		class IStage;
	}
	
	namespace base3d
	{

		enum class GizmoType
		{
			TRANSLATE, ROTATE, SCALE
		};
		
		enum class GizmoAxis
		{
			X, Y, Z
		};

		class GameViewController:
			public gui::BaseController, public editor::IGameView
		{
		public:
			GameViewController(gui::Module& module, editor::IEditor& editor);

			gui::Dock& GetDock(void) override;

			void OnKeyPress(gui::Keys key) override;
			void OnInit(const gfx::Context& gfx, const render::IRenderer& renderer) override;
			void OnSelectEntity(const ecs::EntityHandle* pEntity) override;
			void OnGotoEntity(const ecs::Entity& entity) override;
			ecs::EntityHandle OnPickEntity(const ecs::EntityManager& entities, const math::Vector2& vPosition) override;
			core::Signal<>& GetClickedSignal(void) override;
			
			void Update(void) override;

			void OnRender(void) const override;

		private:

			void OnRotate(const math::Vector2& vDistance);
			void OnZoom(int distance);
			void OnSetTranslate(void);
			void OnSetRotate(void);
			void OnSetScale(void);

			void PerformGizmo(GizmoAxis axis, int direction);

			float m_angleX, m_angleY;
			GizmoType m_gizmo;

			editor::IEditor* m_pEditor;
			gfx::IMaterial* m_pMaterial;
			gfx::ModelInstance *m_pInstance;
			gfx::Camera m_camera;
			gui::Widget* m_pView;
			render::IStage* m_pStage;
			ecs::EntityHandle m_entity;
		};

	}
}

