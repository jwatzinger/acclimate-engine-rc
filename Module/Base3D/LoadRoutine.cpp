#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Math\AABB.h"
#include "Math\Utility.h"
#include "System\Convert.h"
#include "XML\Node.h"


namespace acl
{
	namespace ecs
	{

		void Base3DLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			/*******************************************
			* Position
			********************************************/

			if(const xml::Node* pPosition = entityNode.FirstNode(L"Position"))
				entity.AttachComponent<Position>(pPosition->Attribute(L"x")->AsFloat(), pPosition->Attribute(L"y")->AsFloat(), pPosition->Attribute(L"z")->AsFloat());

			/*******************************************
			* Direction
			********************************************/

			if(const xml::Node* pDirection = entityNode.FirstNode(L"Direction"))
			{
                math::Vector3 vDir(pDirection->Attribute(L"x")->AsFloat(), pDirection->Attribute(L"y")->AsFloat(), pDirection->Attribute(L"z")->AsFloat());
                vDir.Normalize();
				entity.AttachComponent<Direction>(vDir);
			}

			/*******************************************
			* Actor
			********************************************/

			if(const xml::Node* pActor = entityNode.FirstNode(L"Actor"))
			{
				entity.AttachComponent<Actor>(pActor->Attribute(L"model")->GetValue().c_str());
			}

			/*******************************************
			* Rotation
			********************************************/
				    
			if(const xml::Node* pRotation = entityNode.FirstNode(L"Rotation"))
				entity.AttachComponent<Rotation>(pRotation->Attribute(L"x")->AsFloat(), pRotation->Attribute(L"y")->AsFloat(), pRotation->Attribute(L"z")->AsFloat());

			/*******************************************
			* Scale
			********************************************/
				    
			if(const xml::Node* pScale = entityNode.FirstNode(L"Scale"))
				entity.AttachComponent<Scale>(pScale->Attribute(L"x")->AsFloat(), pScale->Attribute(L"y")->AsFloat(), pScale->Attribute(L"z")->AsFloat());

			/*******************************************
			* Transform
			********************************************/

			if(const xml::Node* pTransform = entityNode.FirstNode(L"Transform"))
				entity.AttachComponent<Transform>();

			/*******************************************
			* Bounding
			********************************************/

			if(const xml::Node* pBounding = entityNode.FirstNode(L"Bounding"))
				entity.AttachComponent<Bounding>(*new math::AABB(pBounding->Attribute(L"x")->AsFloat(), pBounding->Attribute(L"y")->AsFloat(), pBounding->Attribute(L"z")->AsFloat(), 
					pBounding->Attribute(L"sx")->AsFloat(), pBounding->Attribute(L"sy")->AsFloat(), pBounding->Attribute(L"sz")->AsFloat()));

			/*******************************************
			* Camera
			********************************************/

			if(const xml::Node* pCamera = entityNode.FirstNode(L"Camera"))
				entity.AttachComponent<Camera>(pCamera->Attribute(L"name")->GetValue());

			/*******************************************
			* Animation
			********************************************/

			if(const xml::Node* pAnimation = entityNode.FirstNode(L"Animation"))
				entity.AttachComponent<Animation>(pAnimation->Attribute(L"set")->GetValue(), pAnimation->Attribute(L"default")->GetValue());
		}

	}
}
