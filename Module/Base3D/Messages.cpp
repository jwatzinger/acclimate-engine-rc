#include "Messages.h"

namespace acl
{
	namespace ecs
	{
        /***********************************************
        * UpdateCamera
        ***********************************************/

		UpdateCamera::UpdateCamera(const gfx::Camera& camera): camera(camera)
		{
		}

		const void* UpdateCamera::GetParam(const std::wstring& stName, const std::type_info** type) const
		{
			if(stName == L"Camera")
			{
				*type = &typeid(camera);
				return &camera;
			}

			return nullptr;
		}

		/***********************************************
        * RenderScene
        ***********************************************/

		RenderScene::RenderScene(const gfx::Camera& camera, render::IStage& stage, unsigned int pass): camera(camera), stage(stage), pass(pass) 
		{
		}

		const void* RenderScene::GetParam(const std::wstring& stName, const std::type_info** type) const
		{
			if(stName == L"Stage")
			{
				*type = &typeid(stage);
				return &stage;
			}
			else if(stName == L"Pass")
			{
				*type = &typeid(pass);
				return &pass;
			}

			return nullptr;
		}

	}
}