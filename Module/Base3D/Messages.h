#pragma once
#include <string>
#include "Entity\Message.h"
#include "Math\Vector.h"

namespace acl
{
    namespace gfx
    {
        class Camera;
    }

	namespace render
	{
		class IStage;
	}

    namespace ecs
    {

        /***********************************************
        * UpdateCamera
        ***********************************************/

        struct UpdateCamera : 
			public ecs::Message<UpdateCamera>
        {
	        UpdateCamera(const gfx::Camera& camera);

			const void* GetParam(const std::wstring& stName, const std::type_info** type) const override;

	        const gfx::Camera& camera;
        };

		/***********************************************
        * ActivateCamera
        ***********************************************/

		struct ActivateCamera : 
			public ecs::Message<ActivateCamera>
        {
	        ActivateCamera(const std::wstring& stName): stName(stName) {};

	        const std::wstring stName;
        };

		/***********************************************
        * ResolutionChange
        ***********************************************/

		struct ResolutionChange: 
			public ecs::Message<ResolutionChange>
		{
			ResolutionChange(const math::Vector2& vSize): vSize(vSize) {};

			const math::Vector2 vSize;
		};

		/***********************************************
        * RenderScene
        ***********************************************/

        struct RenderScene : 
			public ecs::Message<RenderScene>
        {
	        RenderScene(const gfx::Camera& camera, render::IStage& stage, unsigned int pass);

			const void* GetParam(const std::wstring& stName, const std::type_info** type) const override;

			unsigned int pass;

	        const gfx::Camera& camera;

			render::IStage& stage;
		};

    }
}