#pragma once
#include "Entity\Query.h"
#include "Entity\Entity.h"

namespace acl
{

    namespace math
    {
        struct Ray;
        class IVolume;
    }

    namespace ecs
    {
        /****************************
        * Collide
        ****************************/

        struct CollideQuery : public Query<CollideQuery>
        {
            CollideQuery(const math::IVolume& volume): volume(volume) {}

            const math::IVolume& volume;
        };

        /****************************
        * Pick
        ****************************/

        struct PickQuery : public Query<PickQuery>
        {
            PickQuery(const math::Ray& ray): ray(ray) {}

            ecs::EntityHandle entity;
            const math::Ray& ray;
        };

		/****************************
		* ActiveCamera
		****************************/

		struct ActiveCameraQuery : public Query<ActiveCameraQuery>
		{
			ecs::EntityHandle camera;
		};

    }
}