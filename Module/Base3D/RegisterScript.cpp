#include "RegisterScript.h"
#include "Components.h"
#include "Messages.h"
#include "Queries.h"
#include "Entity\MessageManager.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace base3d
	{
		namespace detail
		{
			ecs::MessageManager* _pMessages = nullptr;
		}

		/*******************************************
		* Position
		*******************************************/

		ecs::Position* AttachPosition(ecs::EntityHandle* entity, float x, float y, float z)
		{
			return (*entity)->AttachComponent<ecs::Position>(x, y, z);
		}

		ecs::Position* AttachPosition(ecs::EntityHandle* entity, const math::Vector3& vPos)
		{
			return (*entity)->AttachComponent<ecs::Position>(vPos);
		}

		void RemovePosition(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Position>();
		}

		ecs::Position* GetPosition(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Position>();
		}

		const math::Vector3& GetPositionVec(const ecs::Position* pos)
		{
			return pos->v;
		}

		void SetPositionVec(const math::Vector3& vPos, ecs::Position* pos)
		{
			pos->v = vPos;
			pos->bDirty = true;
		}

		/*******************************************
		* Rotation
		*******************************************/

		ecs::Rotation* AttachRotation(ecs::EntityHandle* entity, float x, float y, float z)
		{
			return (*entity)->AttachComponent<ecs::Rotation>(x, y, z);
		}

		void RemoveRotation(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Rotation>();
		}

		ecs::Rotation* GetRotation(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Rotation>();
		}

		float GetRotationX(const ecs::Rotation* rotation)
		{
			return rotation->x;
		}

		float GetRotationY(const ecs::Rotation* rotation)
		{
			return rotation->y;
		}

		float GetRotationZ(const ecs::Rotation* rotation)
		{
			return rotation->z;
		}

		void SetRotationX(float x, ecs::Rotation* rotation)
		{
			rotation->x = x;
			rotation->bDirty = true;
		}

		void SetRotationY(float y, ecs::Rotation* rotation)
		{
			rotation->y = y;
			rotation->bDirty = true;
		}

		void SetRotationZ(float z, ecs::Rotation* rotation)
		{
			rotation->z = z;
			rotation->bDirty = true;
		}

		/*******************************************
		* Scale
		*******************************************/

		ecs::Scale* AttachScale(ecs::EntityHandle* entity, float x, float y, float z)
		{
			return (*entity)->AttachComponent<ecs::Scale>(x, y, z);
		}

		void RemoveScale(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Scale>();
		}

		ecs::Scale* GetScale(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Scale>();
		}

		const math::Vector3& GetScaleVector(const ecs::Scale* scale)
		{
			return scale->v;
		}

		void SetScaleVector(const math::Vector3& vScale, ecs::Scale* scale)
		{
			scale->v = vScale;
			scale->bDirty = true;
		}

		/*******************************************
		* Actor
		*******************************************/

		ecs::Actor* AttachActor(ecs::EntityHandle* entity, const std::wstring& stModel)
		{
			return (*entity)->AttachComponent<ecs::Actor>(stModel);
		}

		void RemoveActor(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Actor>();
		}

		ecs::Actor* GetActor(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Actor>();
		}

		void SetActorModel(const std::wstring& stModel, ecs::Actor* actor)
		{
			actor->stName = stModel;
			actor->bDirty = true;
		}

		const std::wstring& GetActorModel(const ecs::Actor* actor)
		{
			return actor->stName;
		}

		/*******************************************
		* Direction
		*******************************************/

		ecs::Direction* AttachDirection(ecs::EntityHandle* entity, float x, float y, float z)
		{
			return (*entity)->AttachComponent<ecs::Direction>(x, y, z);
		}

		ecs::Direction* AttachDirection(ecs::EntityHandle* entity, const math::Vector3& vPos)
		{
			return (*entity)->AttachComponent<ecs::Direction>(vPos);
		}

		void RemoveDirection(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Direction>();
		}

		ecs::Direction* GetDirection(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Direction>();
		}

		const math::Vector3& GetDirectionVec(const ecs::Direction* dir)
		{
			return dir->v;
		}

		void SetDirectionVec(const math::Vector3& vDir, ecs::Direction* dir)
		{
			dir->v = vDir;
			dir->bDirty = true;
		}

		/*******************************************
		* Transform
		*******************************************/

		ecs::Transform* AttachTransform(ecs::EntityHandle* entity)
		{
			return (*entity)->AttachComponent<ecs::Transform>();
		}

		void RemoveTransform(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Transform>();
		}

		ecs::Transform* GetTransform(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Transform>();
		}

		/*******************************************
		* Node
		*******************************************/

		ecs::Node* AttachNode(ecs::EntityHandle* entity, const ecs::EntityHandle& parent, bool bLinkLifetime)
		{
			return (*entity)->AttachComponent<ecs::Node>(parent, bLinkLifetime);
		}

		void RemoveNode(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Node>();
		}

		ecs::Node* GetNode(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Node>();
		}

		/*******************************************
		* Camera
		*******************************************/

		ecs::Camera* AttachCamera(ecs::EntityHandle* entity, const std::wstring& stName)
		{
			return (*entity)->AttachComponent<ecs::Camera>(stName);
		}

		void RemoveCamera(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Camera>();
		}

		ecs::Camera* GetCamera(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Camera>();
		}

		/*******************************************
		* Animation
		*******************************************/

		ecs::Animation* AttachAnimation(ecs::EntityHandle* entity, const std::wstring& stName, const std::wstring& stDefault)
		{
			return (*entity)->AttachComponent<ecs::Animation>(stName, stDefault);
		}

		void RemoveAnimation(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Animation>();
		}

		ecs::Animation* GetAnimation(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Animation>();
		}

		void changeAnimation(ecs::Animation& animation, const std::wstring& stName)
		{
			animation.stNextAnimation = stName;
		}

		/*******************************************
		* SetCamera
		*******************************************/

		void ActivateCamera(const std::wstring& stName)
		{
			detail::_pMessages->DeliverMessage<ecs::ActivateCamera>(stName);
		}

		bool getActiveCamera(ecs::EntityHandle& entity)
		{
			ecs::ActiveCameraQuery query;
			if(detail::_pMessages->Query(query))
			{
				entity = query.camera;
				return true;
			}
			else
				return false;
		}

		void RegisterScript(ecs::MessageManager& messages, script::Core& script)
		{
			detail::_pMessages = &messages;

			auto states = script.RegisterEnum("State");
			states.RegisterConcurrent(0, "TURN_HOLD", "MOVE_LEFT", "MOVE_RIGHT", "MOVE_FORWARD", "MOVE_BACKWARD");
			auto ranges = script.RegisterEnum("Ranges");
			ranges.RegisterConcurrent(0, "MOVE_X", "MOVE_Y", "ZOOM");
			auto values = script.RegisterEnum("Values");
			values.RegisterConcurrent(0, "MOUSE_X", "MOUSE_Y");

			auto position = script.RegisterReferenceType("Position", asOBJ_NOCOUNT);
			position.RegisterMethod("const Vector3& GetVec() const", asFUNCTION(GetPositionVec), asCALL_CDECL_OBJLAST);
			position.RegisterMethod("void SetVec(const Vector3& in)", asFUNCTION(SetPositionVec), asCALL_CDECL_OBJLAST);

			auto direction = script.RegisterReferenceType("Direction", asOBJ_NOCOUNT);
			direction.RegisterMethod("const Vector3& GetVec() const", asFUNCTION(GetDirectionVec), asCALL_CDECL_OBJLAST);
			direction.RegisterMethod("void SetVec(const Vector3& in)", asFUNCTION(SetDirectionVec), asCALL_CDECL_OBJLAST);

			auto rotation = script.RegisterReferenceType("Rotation", asOBJ_NOCOUNT);
			rotation.RegisterMethod("float GetX() const", asFUNCTION(GetRotationX), asCALL_CDECL_OBJLAST);
			rotation.RegisterMethod("float GetY() const", asFUNCTION(GetRotationY), asCALL_CDECL_OBJLAST);
			rotation.RegisterMethod("float GetZ() const", asFUNCTION(GetRotationZ), asCALL_CDECL_OBJLAST);
			rotation.RegisterMethod("void SetX(float)", asFUNCTION(SetRotationX), asCALL_CDECL_OBJLAST);
			rotation.RegisterMethod("void SetY(float)", asFUNCTION(SetRotationY), asCALL_CDECL_OBJLAST);
			rotation.RegisterMethod("void SetZ(float)", asFUNCTION(SetRotationZ), asCALL_CDECL_OBJLAST);

			auto scale = script.RegisterReferenceType("Scale", asOBJ_NOCOUNT);
			scale.RegisterMethod("const Vector3& GetVec() const", asFUNCTION(GetScaleVector), asCALL_CDECL_OBJLAST);
			scale.RegisterMethod("void SetVec(const Vector3& in)", asFUNCTION(SetScaleVector), asCALL_CDECL_OBJLAST);

			auto actor = script.RegisterReferenceType("Actor", asOBJ_NOCOUNT);
			actor.RegisterProperty("bool visible", asOFFSET(ecs::Actor, bVisible));
			actor.RegisterMethod("void SetModel(const string& in)", asFUNCTION(SetActorModel), asCALL_CDECL_OBJLAST);
			actor.RegisterMethod("const string& GetModel(void) const", asFUNCTION(GetActorModel), asCALL_CDECL_OBJLAST);

			auto transform = script.RegisterReferenceType("Transform", asOBJ_NOCOUNT);
			transform.RegisterProperty("Vector3 offset", asOFFSET(ecs::Transform, vOffset));

			auto node = script.RegisterReferenceType("Node", asOBJ_NOCOUNT);
			node.RegisterProperty("bool linkLife", asOFFSET(ecs::Node, bLinkLifeTime));
			node.RegisterProperty("Entity parent", asOFFSET(ecs::Node, parent));

			auto camera = script.RegisterReferenceType("Cam", asOBJ_NOCOUNT);
			camera.RegisterProperty("string name", asOFFSET(ecs::Camera, stName));
			camera.RegisterProperty("Camera@ camera", asOFFSET(ecs::Camera, pCamera));
			camera.RegisterProperty("bool dirty", asOFFSET(ecs::Camera, bDirty));
			camera.RegisterProperty("bool active", asOFFSET(ecs::Camera, isActive));

			auto animation = script.RegisterReferenceType("Animation", asOBJ_NOCOUNT);
			animation.RegisterProperty("bool isPlaying", asOFFSET(ecs::Animation, isPlaying));
			animation.RegisterProperty("float speed", asOFFSET(ecs::Animation, speed));
			animation.RegisterMethod("void SetAnimation(const string& in)", asFUNCTION(changeAnimation), asCALL_CDECL_OBJFIRST);

			// position
			script.RegisterGlobalFunction("Position@ AttachPosition(Entity& in, float, float, float)", asFUNCTIONPR(AttachPosition, (ecs::EntityHandle*, float, float, float), ecs::Position*));
			script.RegisterGlobalFunction("Position@ AttachPosition(Entity& in, const Vector3& in)", asFUNCTIONPR(AttachPosition, (ecs::EntityHandle*, const math::Vector3&), ecs::Position*));
			script.RegisterGlobalFunction("void RemovePosition(Entity& in)", asFUNCTION(RemovePosition));
			script.RegisterGlobalFunction("Position@ GetPosition(const Entity& in)", asFUNCTION(GetPosition));
			// rotation
			script.RegisterGlobalFunction("Rotation@ AttachRotation(Entity& in, float, float, float)", asFUNCTION(AttachRotation));
			script.RegisterGlobalFunction("void RemoveRotation(Entity& in)", asFUNCTION(RemoveRotation));
			script.RegisterGlobalFunction("Rotation@ GetRotation(const Entity& in)", asFUNCTION(GetRotation));
			// scale
			script.RegisterGlobalFunction("Scale@ AttachScale(Entity& in, float, float, float)", asFUNCTION(AttachScale));
			script.RegisterGlobalFunction("void RemoveScale(Entity& in)", asFUNCTION(RemoveScale));
			script.RegisterGlobalFunction("Scale@ GetScale(const Entity& in)", asFUNCTION(GetScale));
			// direction
			script.RegisterGlobalFunction("Direction@ AttachDirection(Entity& in, float, float, float)", asFUNCTIONPR(AttachDirection, (ecs::EntityHandle*, float, float, float), ecs::Direction*));
			script.RegisterGlobalFunction("Direction@ AttachDirection(Entity& in, const Vector3& in)", asFUNCTIONPR(AttachDirection, (ecs::EntityHandle*, const math::Vector3&), ecs::Direction*));
			script.RegisterGlobalFunction("void RemoveDirection(Entity& in)", asFUNCTION(RemoveDirection));
			script.RegisterGlobalFunction("Direction@ GetDirection(const Entity& in)", asFUNCTION(GetDirection));
			// actor
			script.RegisterGlobalFunction("Actor@ AttachActor(Entity& in, const string& in)", asFUNCTION(AttachActor));
			script.RegisterGlobalFunction("void RemoveActor(Entity& in)", asFUNCTION(RemoveActor));
			script.RegisterGlobalFunction("Actor@ GetActor(const Entity& in)", asFUNCTION(GetActor));
			// transform
			script.RegisterGlobalFunction("Transform@ AttachTransform(Entity& in)", asFUNCTION(AttachTransform));
			script.RegisterGlobalFunction("void RemoveTransform(Entity& in)", asFUNCTION(RemoveTransform));
			script.RegisterGlobalFunction("Transform@ GetTansform(const Entity& in)", asFUNCTION(GetTransform));
			// node
			script.RegisterGlobalFunction("Node@ AttachNode(Entity& in, const Entity& in, bool link)", asFUNCTION(AttachNode));
			script.RegisterGlobalFunction("void RemoveNode(Entity& in)", asFUNCTION(RemoveNode));
			script.RegisterGlobalFunction("Node@ GetNode(const Entity& in)", asFUNCTION(GetNode));
			// camera
			script.RegisterGlobalFunction("Cam@ AttachCamera(Entity& in, const string& in)", asFUNCTION(AttachCamera));
			script.RegisterGlobalFunction("void RemoveCamera(Entity& in)", asFUNCTION(RemoveCamera));
			script.RegisterGlobalFunction("Cam@ GetCamera(const Entity& in)", asFUNCTION(GetCamera));
			// animation
			script.RegisterGlobalFunction("Animation@ AttachAnimation(Entity& in, const string& in, const string& in)", asFUNCTION(AttachAnimation));
			script.RegisterGlobalFunction("void RemoveAnimation(Entity& in)", asFUNCTION(RemoveAnimation));
			script.RegisterGlobalFunction("Animation@ GetAnimation(const Entity& in)", asFUNCTION(GetAnimation));

			// set camera
			script.RegisterGlobalFunction("void activateCamera(const string& in)", asFUNCTION(ActivateCamera));
			script.RegisterGlobalFunction("bool getActiveCamera(Entity& out)", asFUNCTION(getActiveCamera));
		}

	}
}
