#pragma once

namespace acl
{
	namespace ecs
	{
		class MessageManager;
	}

	namespace script
	{
		class Core;
	}

	namespace base3d
	{

		void RegisterScript(ecs::MessageManager& messages, script::Core& script);

	}
}
