#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void Base3DSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			/*******************************************
			* Position
			********************************************/

			if(Position* pPosition = entity.GetComponent<Position>())
			{
				auto& component = entityNode.InsertNode(L"Position");
				component.ModifyAttribute(L"x", conv::ToString(pPosition->v.x).c_str());
				component.ModifyAttribute(L"y", conv::ToString(pPosition->v.y).c_str());
				component.ModifyAttribute(L"z", conv::ToString(pPosition->v.z).c_str());
			}

			/*******************************************
			* Direction
			********************************************/

			if(Direction* pDirection = entity.GetComponent<Direction>())
			{
				auto& component = entityNode.InsertNode(L"Direction");
				component.ModifyAttribute(L"x", conv::ToString(pDirection->v.x).c_str());
				component.ModifyAttribute(L"y", conv::ToString(pDirection->v.y).c_str());
				component.ModifyAttribute(L"z", conv::ToString(pDirection->v.z).c_str());
			}

			/*******************************************
			* Actor
			********************************************/

			if(Actor* pActor = entity.GetComponent<Actor>())
			{
				auto& component = entityNode.InsertNode(L"Actor");
				component.ModifyAttribute(L"model", pActor->stName.c_str());
			}

			/*******************************************
			* Rotation
			********************************************/

			if(Rotation* pRotation = entity.GetComponent<Rotation>())
			{
				auto& component = entityNode.InsertNode(L"Rotation");
				component.ModifyAttribute(L"x", conv::ToString(pRotation->x).c_str());
				component.ModifyAttribute(L"y", conv::ToString(pRotation->y).c_str());
				component.ModifyAttribute(L"z", conv::ToString(pRotation->z).c_str());
			}

			/*******************************************
			* Scale
			********************************************/

			if(Scale* pScale = entity.GetComponent<Scale>())
			{
				auto& component = entityNode.InsertNode(L"Scale");
				component.ModifyAttribute(L"x", conv::ToString(pScale->v.x).c_str());
				component.ModifyAttribute(L"y", conv::ToString(pScale->v.y).c_str());
				component.ModifyAttribute(L"z", conv::ToString(pScale->v.z).c_str());
			}

			/*******************************************
			* Transform
			********************************************/

			if(Transform* pTransform = entity.GetComponent<Transform>())
			{
				entityNode.InsertNode(L"Transform");
			}

			/*******************************************
			* Camera
			********************************************/

			if(Camera* pCamera = entity.GetComponent<Camera>())
			{
				auto& camera = entityNode.InsertNode(L"Camera");
				camera.ModifyAttribute(L"name", pCamera->stName);
			}

			//todo: IMPORTANT -> think about how bounding could still be serialized
			/*if(Bounding* pBounding = entity->GetComponent<Bounding>())
			{
				auto& component = entity.InsertNode(L"Component");
				component.ModifyAttribute(L"type", L"Bounding");
				component.ModifyAttribute(L"x", conv::ToString(pBounding->box.vOrigin.x).c_str());
				component.ModifyAttribute(L"y", conv::ToString(pBounding->box.vOrigin.y).c_str());
				component.ModifyAttribute(L"z", conv::ToString(pBounding->box.vOrigin.z).c_str());
				component.ModifyAttribute(L"s", conv::ToString(pBounding->box.size).c_str());
			}*/

			/*******************************************
			* Animation
			********************************************/

			if(Animation* pAnimation = entity.GetComponent<Animation>())
			{
				auto& animation = entityNode.InsertNode(L"Animation");
				animation.ModifyAttribute(L"set", pAnimation->stAnimationSet);
				animation.ModifyAttribute(L"default", pAnimation->stDefaultAnimation);
			}
		}

	}
}
