#include "SceneGraphSystem.h"
#include "Components.h"
#include "Queries.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace ecs
	{

		void SceneGraphSystem::Update(double dt)
		{
	        auto vEntities = m_pEntities->EntitiesWithComponents<Node>();

	        for(auto& entity : vEntities)
	        {
				Node* pNode = entity->GetComponent<Node>();

				/*******************************************
				* Parent lifety check
				********************************************/

				if(!pNode->parent.IsValid())
				{
					if(pNode->bLinkLifeTime)
						m_pEntities->RemoveEntity(entity);
					else
						entity->DetachComponent<Node>();

					continue;
				}

				// todo: account for differences in attributes, so that childs can modify their 
				// components without the parent overriding it

				/*******************************************
				* Position modification
				********************************************/
				
				if(Position* pChildPos = entity->GetComponent<Position>())
				{
					if(const Position* pParentPos = pNode->parent->GetComponent<Position>())
						pChildPos->v = pParentPos->v;
				}

				/*******************************************
				* Rotation modification
				********************************************/
				
				if(Rotation* pChildRot = entity->GetComponent<Rotation>())
				{
					if(const Rotation* pParentRot = pNode->parent->GetComponent<Rotation>())
					{
						pChildRot->x = pParentRot->x;
						pChildRot->y = pParentRot->y;
						pChildRot->z = pParentRot->z;
					}
				}

				/*******************************************
				* Scale modification
				********************************************/
				
				if(Scale* pChildScale = entity->GetComponent<Scale>())
				{
					if(const Scale* pParentScale = pNode->parent->GetComponent<Scale>())
						pChildScale->v = pParentScale->v;
				}

			}
		}

	}
}