#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace ecs
	{

		class SceneGraphSystem final :
			public ecs::System<SceneGraphSystem>
		{
		public:

			void Update(double dt) override;
		};

	}
}
