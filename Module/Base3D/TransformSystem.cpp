#include "TransformSystem.h"
#include "Messages.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Math\Utility.h"

namespace acl
{
    namespace ecs
    {

        void TransformSystem::Update(double dt)
        {
	        //get entities with transform component
	        auto vEntities = m_pEntities->EntitiesWithComponents<Transform>();

	        //loop through those entities to update model component
	        for(auto& entity: vEntities)
	        {
				auto pTransform = entity->GetComponent<Transform>();
				math::Matrix& mTransform = pTransform->mTransform;
                mTransform.Identity();

				if(Scale* pScale = entity->GetComponent<Scale>())
                {
                    //check if we need to update scalation
			        if(pScale->bDirty)
                    {
				        pScale->mScaling.Scale(pScale->v);

                        pScale->bDirty = false;
                    }

			        mTransform *= pScale->mScaling;
                }

				if(Rotation* pRotation = entity->GetComponent<Rotation>())
                {
                    //check if we need to update scalation
                    if(pRotation->bDirty)
                    {
						pRotation->mRotation = pRotation->quaternion.Matrix();
			            //perform matrix translation
						pRotation->mRotation *= math::MatYawPitchRoll(math::degToRad(pRotation->y), math::degToRad(pRotation->x), math::degToRad(pRotation->z));

                        //mark position as done
                        pRotation->bDirty = false;
                    }

			        //set the world transform constant to the model
			        mTransform *= pRotation->mRotation;
                }

				//check if we need to update rotation
				if(Direction* pDirection = entity->GetComponent<Direction>())
				{
					if(pDirection->bDirty)
					{
						pDirection->v.Normalize();

						const math::Vector3 vUp(0.0f, 1.0f, 0.0f);
						math::Vector3 vRight = pDirection->v.Cross(vUp);
						vRight.Normalize();

						const math::Vector3 vRealUp = vRight.Cross(pDirection->v).normal();

						pDirection->mRotation.Axis(pDirection->v, vRealUp, vRight);

						pDirection->bDirty = false;
					}

					mTransform *= pDirection->mRotation;
				}

				if(Position* pPosition = entity->GetComponent<Position>())
		        {
                    //check if we need to update transform
                    if(pPosition->bDirty)
                    {
						pPosition->mTranslation.Translation(pPosition->v + pTransform->vOffset);

			            //mark position as done
                        pPosition->bDirty = false;
                    }

                    mTransform *= pPosition->mTranslation;
		        }

	        }
		
        }

    }
}