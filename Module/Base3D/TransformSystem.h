#pragma once
#include "Entity\System.h"

namespace acl
{ 
    namespace ecs
    {

        class TransformSystem : 
	        public System<TransformSystem>
        {
        public:

	        void Update(double dt);

        };

    }
}