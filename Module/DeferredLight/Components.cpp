#include "Components.h"
#include "Gfx\IModel.h"
#include "Gfx\ModelInstance.h"

namespace acl
{
	namespace ecs
	{
        /***************************************************
        * Light
        ****************************************************/

	    Light::Light(bool bVisible): bVisible(bVisible), bDirty(true),
			pModelInstance(nullptr)
        {
        }

		Light::~Light(void)
		{
			delete pModelInstance;
		}

		void* Light::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Model")
			{
				*type = &typeid(gfx::ModelInstance);
				return pModelInstance;
			}

			return nullptr;
		}

        /************************************************
        * Ambient
        ************************************************/

	    Ambient::Ambient(const gfx::FColor& ambient): ambient(ambient) 
        {
        }

        /************************************************
        * Diffuse
        ************************************************/

	    Diffuse::Diffuse(const gfx::FColor& diffuse): diffuse(diffuse) 
        {
        }

        /************************************************
        * Specular
        ************************************************/

	    Specular::Specular(const gfx::FColor& specular): specular(specular) 
        {
        }

        /************************************************
        * Attenuation
        ************************************************/

	    Attenuation::Attenuation(float range, float attenuation, float attenuation1, float attenuation2): range(range), attenuation(attenuation), attenuation1(attenuation1), attenuation2(attenuation2) 
        {
        }

        /************************************************
        * Fallout
        ************************************************/

	    Falloff::Falloff(float phi, float theta, float falloff): phi(phi), theta(theta), falloff(falloff) 
        {
        }

	}
}