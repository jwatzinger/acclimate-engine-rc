#pragma once
#include <string>
#include "Entity\Component.h"
#include "Gfx\Color.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
	}

	namespace ecs
	{
        /***************************************************
        * Light
        ****************************************************/

        struct Light : Component<Light>
        {
	        Light(bool bVisible);
			~Light(void);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

			bool bVisible, bDirty;
			gfx::ModelInstance* pModelInstance;
        };

        /************************************************
        * Ambient
        ************************************************/

        struct Ambient : Component<Ambient>
        {
	        Ambient(const gfx::FColor& ambient);

	        gfx::FColor ambient;
        };

        /************************************************
        * Diffuse
        ************************************************/

        struct Diffuse : Component<Diffuse>
        {
	        Diffuse(const gfx::FColor& diffuse);

	        gfx::FColor diffuse;
        };

        /************************************************
        * Specular
        ************************************************/

        struct Specular : Component<Specular>
        {
			Specular(const gfx::FColor& specular);

	        gfx::FColor specular;
        };

        /************************************************
        * Attenuation
        ************************************************/

        struct Attenuation : Component<Attenuation>
        {
	        Attenuation(float range, float attenuation, float attenuation1, float attenuation2);

	        float range, attenuation, attenuation1, attenuation2;
        };

        /************************************************
        * Fallout
        ************************************************/

        struct Falloff : Component<Falloff>
        {
	        Falloff(float phi, float theta, float falloff);

	        float phi, theta, falloff;
        };

	}
}