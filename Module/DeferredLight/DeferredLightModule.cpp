#include "DeferredLightModule.h"
#include "LightSystem.h"
#include "LightRenderSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\IResourceLoader.h"
#include "Render\ILoader.h"
#include "Render\Postprocessor.h"

namespace acl
{
	namespace modules
	{

		DeferredLightModule::DeferredLightModule(void): m_pSystems(nullptr)
		{
		}

		void DeferredLightModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void DeferredLightModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void DeferredLightModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			context.render.postprocess.SelectInputTextures(L"final scene", L"deferred pos", L"deferred nrm");

			// system
			m_pSystems->AddSystem<ecs::LightSystem>(context.gfx.load.models);
			m_pSystems->AddSystem<ecs::LightRenderSystem>(context.gfx.resources.materials, context.gfx.load.materials, context.render.renderer);

			// io
			ecs::Saver::RegisterSubRoutine<ecs::LightSaveRoutine>(L"DeferredLight");
			ecs::Loader::RegisterSubRoutine<ecs::LightLoadRoutine>(L"DeferredLight");

			// components
			ecs::ComponentRegistry::RegisterComponent<ecs::Light>(L"Light");
			context.ecs.componentFactory.Register<ecs::Light, bool>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Ambient>(L"Ambient");
			context.ecs.componentFactory.Register<ecs::Ambient, gfx::FColor>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Diffuse>(L"Diffuse");
			context.ecs.componentFactory.Register<ecs::Diffuse, gfx::FColor>();
			ecs::ComponentRegistry::RegisterComponent<ecs::Specular>(L"Specular");
			context.ecs.componentFactory.Register<ecs::Specular, gfx::FColor>();
		}

		void DeferredLightModule::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<ecs::LightSystem>();
			m_pSystems->RemoveSystem<ecs::LightRenderSystem>();

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Light");
			context.ecs.componentFactory.Unregister<ecs::Light>();
			ecs::ComponentRegistry::UnregisterComponent(L"Ambient");
			context.ecs.componentFactory.Unregister<ecs::Ambient>();
			ecs::ComponentRegistry::UnregisterComponent(L"Diffuse");
			context.ecs.componentFactory.Unregister<ecs::Diffuse>();
			ecs::ComponentRegistry::UnregisterComponent(L"Specular");
			context.ecs.componentFactory.Unregister<ecs::Specular>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"DeferredLight");
			ecs::Loader::UnregisterSubRoutine(L"DeferredLight");

			m_pSystems = nullptr;
		}

		void DeferredLightModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::LightSystem>(dt);
		}

		void DeferredLightModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::LightRenderSystem>(0.0f);
		}

	}
}
