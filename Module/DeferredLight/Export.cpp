#include "Export.h"
#include "DeferredLightModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::DeferredLightModule();
}