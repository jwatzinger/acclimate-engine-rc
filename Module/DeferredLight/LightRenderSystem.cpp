#include "LightRenderSystem.h"
#include "Components.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\Message.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Gfx\IModel.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\IMaterialLoader.h"
#include "Math\Matrix.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"
#include "System\Convert.h"

namespace acl
{
	namespace ecs
	{

		LightRenderSystem::LightRenderSystem(const gfx::Materials& materials, const gfx::IMaterialLoader& materialLoader, const render::IRenderer& renderer): m_pStage(renderer.GetStage(L"light")), m_pMaterials(&materials),
			m_pMaterialLoader(&materialLoader), m_bRenderShadows(false)
		{
		}

		void LightRenderSystem::Init(MessageManager& messages)
		{
			messages.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		std::wstring LightTypeToString(size_t lightType)
		{
			switch(lightType)
			{
			case 1:
				return L"Directional";
			case 2:
				return L"Point";
			case 3:
				return L"Spot";
			}

			return L"Ambient";
		}

		std::wstring LightRenderSystem::GenerateResources(size_t lightType, const std::wstring& stId) const
		{
			std::wstring stType = LightTypeToString(lightType);

			if(stType != L"Ambient")
			{
				const std::wstring& stLight = L"Light" + stType + stId;
				
				if(!m_pMaterials->Has(stLight))
				{
					gfx::TextureVector vTextures;
					vTextures.push_back(L"deferred mat");
					vTextures.push_back(L"deferred nrm");
					vTextures.push_back(L"deferred pos");
					m_pMaterialLoader->Load(stLight, L"Light" + stType, 0, vTextures);
				}

				return stLight;
			}
			else
				return L"LightAmbient";
		}

		void LightRenderSystem::Update(double dt)
		{
	        auto vEntities = m_pEntities->EntitiesWithComponents<Light>();

			size_t count = 0;
	        for(auto& entity : vEntities)
	        {
				const Light& light = *entity->GetComponent<Light>();

				if( light.pModelInstance && light.bVisible )
				{
					size_t type = 0;
					// check type
					if(entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Position")))
						type += 2;
					if(entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Direction")))
						type += 1;
							
                    if(type != 0)
					    light.pModelInstance->GetParent()->SetMaterial(2, m_pMaterials->Get(GenerateResources(type, conv::ToString(count))));

					light.pModelInstance->Draw(*m_pStage, 2); // lighting pass

					count++;
				}
			}
		}

		void LightRenderSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pStage->SetPixelConstant(0, (float*)&pUpdate->GetParam<gfx::Camera>(L"Camera")->GetPosition(), 1);
		}
		
	}
}