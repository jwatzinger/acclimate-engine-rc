#pragma once
#include "Entity\System.h"
#include "Gfx\Materials.h"

namespace acl
{
	namespace gfx
	{
		class IMaterialLoader;
	}

	namespace render
	{
		class IStage;
		class IRenderer;
	}

	namespace ecs
	{

		class LightRenderSystem final :
			public ecs::System<LightRenderSystem>
		{
		public:
			LightRenderSystem(const gfx::Materials& materials, const gfx::IMaterialLoader& materialLoader, const render::IRenderer& renderer);

			void Init(MessageManager& messages) override;

			void Update(double dt) override;

			void ReceiveMessage(const BaseMessage& message) override;

		private:

			std::wstring  GenerateResources(size_t lightType, const std::wstring& stId) const;

			bool m_bRenderShadows;

			render::IStage* m_pStage;

			const gfx::Materials* m_pMaterials;
			const gfx::IMaterialLoader* m_pMaterialLoader;
		};

	}
}

