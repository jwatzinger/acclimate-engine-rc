#include "LightSystem.h"
#include "Components.h"
#include "ApiInclude.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\ModelInstance.h"
#include "Math\Utility.h"
#include "System\Assert.h"

namespace acl
{
    namespace ecs
    {
		LightSystem::LightSystem(const gfx::IModelLoader& models): m_pModels(&models)
		{
		}

        void LightSystem::Update(double dt)
        {
	        auto vEntities = m_pEntities->EntitiesWithComponents<Light>();

	        for(auto& entity : vEntities)
	        {
				Light* pLight = entity->GetComponent<Light>();

		        if(pLight->bDirty)
		        {
					if(!pLight->pModelInstance)
					{
						const gfx::IModelLoader::PassVector vPasses = { { 2, L"LightAmbient" } };
						pLight->pModelInstance = &m_pModels->LoadInstance(L"screen quad", vPasses);

						continue;
					}

					if(const Ambient* pAmbient = entity->GetComponent<Ambient>())
			        {
				        const float colorArr[4] = { pAmbient->ambient.r, pAmbient->ambient.g, pAmbient->ambient.b, pAmbient->ambient.a };
			
				        pLight->pModelInstance->SetPixelConstant(0, colorArr, 1);
			        }
			        else 
			        {
						unsigned int type = 0;

						auto pDirection = entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Direction"));
						
						math::Vector3 vDirection;
						if(pDirection)
						{
							vDirection = pDirection->GetAttribute<math::Vector3>(L"Vector")->normal();
							type += 1;
						}

						auto pPosition = entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Position"));
						math::Vector3 vPosition;
						if(pPosition)
						{
							vPosition = *pPosition->GetAttribute<math::Vector3>(L"Vector");
							type += 2;
						}

						ACL_ASSERT(type != 0);

						const gfx::FColor& diffuse = entity->GetComponent<Diffuse>()->diffuse;
						const gfx::FColor& specular = entity->GetComponent<Specular>()->specular;

						switch(type)
						{
						case 1:
							{
								float consts[3][4] = 
								{
									{ -vDirection.x, -vDirection.y, -vDirection.z, 32.0f}, //direction, w = shine
									{ diffuse.r, diffuse.g, diffuse.b, diffuse.a }, //diffuse
									{ specular.r, specular.g, specular.b, specular.a }, //specular
								};

								pLight->pModelInstance->SetPixelConstant(0, (float*)consts, 3);

								break;
							}
						case 2:
							{
								const Attenuation& atten = *entity->GetComponent<Attenuation>();

								float consts[4][4] = 
								{
									{ vPosition.x, vPosition.y, vPosition.z, 32.0f}, //direction, w = shine
									{ diffuse.r, diffuse.g, diffuse.b, diffuse.a }, //diffuse
									{ specular.r, specular.g, specular.b, specular.a }, //specular
									{ atten.attenuation, atten.attenuation1, atten.attenuation2, atten.range } //attenuation
								};

								if(getUsedAPI() == GraphicAPI::GL4)
								{
									consts[0][0] = -consts[0][0];
									consts[0][1] = -consts[0][1];
									consts[0][2] = -consts[0][2];
								}

								pLight->pModelInstance->SetPixelConstant(0, (float*)consts, 4);

								break;
							}
						case 3:
							{
								const Attenuation& atten = *entity->GetComponent<Attenuation>();
								const Falloff& falloff = *entity->GetComponent<Falloff>();

								ACL_ASSERT(falloff.phi >= falloff.theta); 

								float consts[6][4] = 
								{
									{ -vDirection.x, -vDirection.y, -vDirection.z, 32.0f}, //direction, w = shine
									{ vPosition.x, vPosition.y, vPosition.z, 0.0f}, //direction, w = shine
									{ diffuse.r, diffuse.g, diffuse.b, diffuse.a }, //diffuse
									{ specular.r, specular.g, specular.b, specular.a }, //specular
									{ atten.attenuation, atten.attenuation1, atten.attenuation2, atten.range }, //attenuation
									{ math::degToRad(falloff.phi), math::degToRad(falloff.theta), falloff.falloff, 0.0f} // spot components
								};

								if(getUsedAPI() == GraphicAPI::GL4)
								{
									consts[0][0] = -consts[0][0];
									consts[0][1] = -consts[0][1];
									consts[0][2] = -consts[0][2];
								}

								pLight->pModelInstance->SetPixelConstant(0, (float*)consts, 6);

								break;
							}
						}
			        }

			        pLight->bDirty = false;
		        }
	        }
        }

    }
}