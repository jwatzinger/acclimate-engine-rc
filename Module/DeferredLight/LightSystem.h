#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace gfx
	{
		class IModelLoader;
	}

    namespace ecs
    {

        class LightSystem : 
            public System<LightSystem>
        {
        public:

			LightSystem(const gfx::IModelLoader& models);

	        void Update(double dt);

		private:
			
			const gfx::IModelLoader* m_pModels;
        };

    }
}