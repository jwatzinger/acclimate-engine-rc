#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void LightLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			/*******************************************
			* Light
			********************************************/

			if(const xml::Node* pLight = entityNode.FirstNode(L"Light"))
			{
				entity.AttachComponent<Light>(pLight->Attribute(L"visible")->AsBool());
			}

			/*******************************************
			* Ambient
			********************************************/

			if(const xml::Node* pAmbient = entityNode.FirstNode(L"Ambient"))
			{
				entity.AttachComponent<Ambient>(gfx::FColor(pAmbient->Attribute(L"r")->AsFloat(), pAmbient->Attribute(L"g")->AsFloat(), pAmbient->Attribute(L"b")->AsFloat(), pAmbient->Attribute(L"i")->AsFloat()));
			}

			/*******************************************
			* Diffuse
			********************************************/

			if(const xml::Node* pDiffuse = entityNode.FirstNode(L"Diffuse"))
			{
				entity.AttachComponent<Diffuse>(gfx::FColor(pDiffuse->Attribute(L"r")->AsFloat(), pDiffuse->Attribute(L"g")->AsFloat(), pDiffuse->Attribute(L"b")->AsFloat(), pDiffuse->Attribute(L"i")->AsFloat()));
			}

			/*******************************************
			* Specular
			********************************************/
				    
			if(const xml::Node* pSpecular = entityNode.FirstNode(L"Specular"))
			{
				entity.AttachComponent<Specular>(gfx::FColor(pSpecular->Attribute(L"r")->AsFloat(), pSpecular->Attribute(L"g")->AsFloat(), pSpecular->Attribute(L"b")->AsFloat(), pSpecular->Attribute(L"i")->AsFloat()));
			}

			/*******************************************
			* Falloff
			********************************************/
                    
			if(const xml::Node* pFalloff = entityNode.FirstNode(L"Falloff"))
			{
				entity.AttachComponent<Falloff>(pFalloff->Attribute(L"ph")->AsFloat(), pFalloff->Attribute(L"th")->AsFloat(), pFalloff->Attribute(L"off")->AsFloat());
			}

			/*******************************************
			* Attenuation
			********************************************/

			if(const xml::Node* pAttenunation = entityNode.FirstNode(L"Attenuation"))
			{
				entity.AttachComponent<Attenuation>(pAttenunation->Attribute(L"rng")->AsFloat(), pAttenunation->Attribute(L"at")->AsFloat(), pAttenunation->Attribute(L"at1")->AsFloat(), pAttenunation->Attribute(L"at2")->AsFloat());
			}
		}

	}
}
