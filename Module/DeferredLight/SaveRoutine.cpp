#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void LightSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			/*******************************************
			* Light
			********************************************/

			if(Light* pLight = entity.GetComponent<Light>())
			{
				auto& component = entityNode.InsertNode(L"Light");
				component.ModifyAttribute(L"visible", conv::ToString(pLight->bVisible));
			}

			/*******************************************
			* Ambient
			********************************************/

			if(Ambient* pAmbient = entity.GetComponent<Ambient>())
			{
				auto& component = entityNode.InsertNode(L"Ambient");
				component.ModifyAttribute(L"r", conv::ToString(pAmbient->ambient.r));
				component.ModifyAttribute(L"g", conv::ToString(pAmbient->ambient.g));
				component.ModifyAttribute(L"b", conv::ToString(pAmbient->ambient.b));
				component.ModifyAttribute(L"i", conv::ToString(pAmbient->ambient.a));
			}

			/*******************************************
			* Diffuse
			********************************************/

			if(Diffuse* pDiffuse = entity.GetComponent<Diffuse>())
			{
				auto& component = entityNode.InsertNode(L"Diffuse");
				component.ModifyAttribute(L"r", conv::ToString(pDiffuse->diffuse.r));
				component.ModifyAttribute(L"g", conv::ToString(pDiffuse->diffuse.g));
				component.ModifyAttribute(L"b", conv::ToString(pDiffuse->diffuse.b));
				component.ModifyAttribute(L"i", conv::ToString(pDiffuse->diffuse.a));
			}

			/*******************************************
			* Specular
			********************************************/

			if(Specular* pSpecular = entity.GetComponent<Specular>())
			{
				auto& component = entityNode.InsertNode(L"Specular");
				component.ModifyAttribute(L"r", conv::ToString(pSpecular->specular.r));
				component.ModifyAttribute(L"g", conv::ToString(pSpecular->specular.g));
				component.ModifyAttribute(L"b", conv::ToString(pSpecular->specular.b));
				component.ModifyAttribute(L"i", conv::ToString(pSpecular->specular.a));
			}

			/*******************************************
			* Attenuation
			********************************************/

			if(Attenuation* pAttenuation = entity.GetComponent<Attenuation>())
			{
				auto& component = entityNode.InsertNode(L"Attenuation");
				component.ModifyAttribute(L"rng", conv::ToString(pAttenuation->range));
				component.ModifyAttribute(L"at", conv::ToString(pAttenuation->attenuation));
				component.ModifyAttribute(L"at1", conv::ToString(pAttenuation->attenuation1));
				component.ModifyAttribute(L"at2", conv::ToString(pAttenuation->attenuation2));
			}

			/*******************************************
			* Falloff
			********************************************/

			if(Falloff* pFalloff = entity.GetComponent<Falloff>())
			{
				auto& component = entityNode.InsertNode(L"Falloff");
				component.ModifyAttribute(L"off", conv::ToString(pFalloff->falloff));
				component.ModifyAttribute(L"ph", conv::ToString(pFalloff->phi));
				component.ModifyAttribute(L"th", conv::ToString(pFalloff->theta));
			}
		}

	}
}
