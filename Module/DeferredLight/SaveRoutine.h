#pragma once
#include "Entity\ISubSaveRoutine.h"

namespace acl
{
	namespace ecs
	{

		class LightSaveRoutine : 
			public ISubSaveRoutine
		{
		public:
			
			void Execute(const ecs::Entity& entity, xml::Node& entityNode) const override;
		};

	}
}

