#include "AttachController.h"
#include "IEditor.h"
#include "Component.h"

namespace acl
{
	namespace event
	{
		
		/***************************************
		* Event
		***************************************/

		EventAttachController::EventAttachController(gui::Module& module) :
			BaseController(module, L"Editor/AttachEvent.axm")
		{
			m_pName = GetWidgetByName<NameBox>(L"Name");
		}

		editor::IComponentController& EventAttachController::Clone(void) const
		{
			return *new EventAttachController(*m_pModule);
		}

		void EventAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<Event>(m_pName->GetContent());
		}

		gui::BaseController& EventAttachController::GetController(void)
		{
			return *this;
		}

		size_t EventAttachController::GetComponentId(void) const
		{
			return Event::family();
		}

	}
}

