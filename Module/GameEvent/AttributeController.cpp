#include "AttributeController.h"
#include "Component.h"
#include "IEditor.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace event
	{

		AttributeController::AttributeController(void) : m_pEntity(nullptr)
		{
			m_pName = new NameBox(0.0f, 0.0f, 1.0f, 1.0f, L"");
			m_pName->SigContentSet.Connect(this, &AttributeController::OnChangeName);
		}

		AttributeController::~AttributeController(void)
		{
			delete m_pName;
		}

		bool AttributeController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<Event>() != nullptr;
		}

		void AttributeController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pEvent = entity.GetComponent<Event>())
			{
				m_pName->SetText(pEvent->stName);
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void AttributeController::OnChangeName(std::wstring name)
		{
			if(m_pEntity)
			{
				auto pEvent = m_pEntity->GetComponent<Event>();
				pEvent->stName = name;
			}
		}

		void AttributeController::OnFillTable(TableMap& mTable)
		{
			mTable[L"Name"] = m_pName;
		}

		void AttributeController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pEvent = m_pEntity->GetComponent<Event>())
					m_pName->SetText(pEvent->stName);
			}
		}

	}
}
