#pragma once
#include "IComponentAttribute.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace event
	{

		/**************************************
		* POSITION
		***************************************/

		class AttributeController :
			public editor::IComponentAttribute
		{
			typedef gui::Textbox<> NameBox;
		public:

			AttributeController(void);
			~AttributeController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnChangeName(std::wstring name);

			const ecs::Entity* m_pEntity;
			NameBox* m_pName;
		};

	}
}

