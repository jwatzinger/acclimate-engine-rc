#include "Component.h"
#include "Core\EventInstance.h"
#include "Core\EventTriggerHandler.h"
#include "Script\Instance.h"

namespace acl
{
	namespace event
	{

		Event::Event(const std::wstring& stName) : stName(stName),
			pInstance(nullptr), pHandler(nullptr)
		{
		}

		Event::~Event(void)
		{
			if(pInstance)
			{
				ACL_ASSERT(pHandler);
				pHandler->RemoveInstance(*pInstance);

				delete pInstance;
			}
		}

		void* Event::GetAttribute(const std::wstring& stName, const std::type_info** type)
		{
			if(stName == L"Instance")
			{
				*type = &typeid(*pInstance);
				return pInstance;
			}

			return nullptr;
		}

	}
}

