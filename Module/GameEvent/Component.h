#pragma once
#include "Entity\Component.h"

namespace acl
{
	namespace core
	{
		class EventInstance;
		class EventTriggerHandler;
	}

	namespace event
	{

		class Instance;

		class Event :
			public ecs::Component<Event>
		{
		public:

			Event(const std::wstring& stName);
			~Event(void);

			void* GetAttribute(const std::wstring& stName, const std::type_info** type) override;

			std::wstring stName;
			core::EventInstance* pInstance;
			core::EventTriggerHandler* pHandler;
		};

	}
}


