#include "EditorPlugin.h"
#include "IComponentRegistry.h"
#include "IEditor.h"
#include "AttachController.h"
#include "AttributeController.h"

namespace acl
{
	namespace event
	{

		void EditorPlugin::OnInit(editor::IEditor& editor)
		{
			m_pEditor = &editor;

			auto& module = editor.GetModule();
			auto& registry = editor.GetComponentRegistry();
			registry.AddComponentAttachController(L"Event", *new EventAttachController(module));
			// attriutes
			registry.AddComponentAtttributeController(L"Event", *new AttributeController);

		}
		
		void EditorPlugin::OnUpdate(void)
		{
		}

		void EditorPlugin::OnUninit(void)
		{
			auto& registry = m_pEditor->GetComponentRegistry();
			registry.RemoveComponentAttachController(L"Event");
			// attriutes
			registry.RemoveComponentAtttributeController(L"Event");
		}

		void EditorPlugin::OnBeginTest(void)
		{
		}

		void EditorPlugin::OnEndTest(void)
		{
		}

	}
}

