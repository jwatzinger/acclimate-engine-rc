#include "EventTrigger.h"
#include "Core\TriggerDeclaration.h"
#include "Core\EventAttribute.h"
#include "Input\Handler.h"

namespace acl
{
	namespace event
	{

		USE_OBJECT(input::MappedInput);

		const core::TriggerDeclaration& EventStartTrigger::GenerateDeclaration(void)
		{
			static const core::TriggerDeclaration declaration(L"EventStartTrigger", core::ReturnVector());
			
			return declaration;
		}

		const core::TriggerDeclaration& EventTickTrigger::GenerateDeclaration(void)
		{
			const core::ReturnVector vReturns =
			{
				{ L"timestep", core::AttributeType::FLOAT, false }
			};

			static const core::TriggerDeclaration declaration(L"EventTickTrigger", vReturns);

			return declaration;
		}

		/*****************************************
		* EventInputTrigger
		******************************************/

		const core::TriggerDeclaration& EventInputTrigger::GenerateDeclaration(void)
		{
			const core::ReturnVector vReturns =
			{
				{ L"input", core::getObjectType<input::MappedInput>(), false }
			};

			static const core::TriggerDeclaration decl(L"EventInputTrigger", vReturns);

			return decl;
		}

	}
}