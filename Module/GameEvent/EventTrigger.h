#pragma once
#include "Core\EventTrigger.h"

namespace acl
{
	namespace event
	{

		/*****************************************
		* EventStartTrigger
		******************************************/

		class EventStartTrigger :
			public core::BaseTrigger<EventStartTrigger>
		{
		public:
			static const core::TriggerDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* EventTickTrigger
		******************************************/

		class EventTickTrigger :
			public core::BaseTrigger<EventTickTrigger>
		{
		public:
			static const core::TriggerDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* EventInputTrigger
		******************************************/

		class EventInputTrigger :
			public core::BaseTrigger<EventInputTrigger>
		{
		public:

			static const core::TriggerDeclaration& GenerateDeclaration(void);
		};

	}
}