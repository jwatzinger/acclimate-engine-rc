#include "Export.h"
#include "Module.h"
#include "EditorPlugin.h"

core::IModule& CreateModule(void)
{
	return *new event::Module();
}

editor::IPlugin& CreatePlugin(core::IModule& module)
{
	return *new event::EditorPlugin();
}