#include "LoadRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"


namespace acl
{
	namespace event
	{

		void LoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(auto* pEvent = entityNode.FirstNode(L"Event"))
				auto pScriptComp = entity.AttachComponent<Event>(pEvent->Attribute(L"name")->GetValue());
		}

	}
}
