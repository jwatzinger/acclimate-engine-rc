#include "Module.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"

#include "Component.h"
#include "System.h"
#include "RegisterScript.h"
#include "RegisterEvents.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"

namespace acl
{
	namespace event
	{

		Module::Module(void) : m_pSystems(nullptr)
		{
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			m_pSystems->AddSystem<System>(context.core.events);

			// components
			ecs::ComponentRegistry::RegisterComponent<Event>(L"Event");
			context.ecs.componentFactory.Register<Event, const wchar_t*>();

			// io
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Event");
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Event");

			registerScript(context.script.core);
			registerEvents(context.ecs.entities);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<System>();

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Event");
			context.ecs.componentFactory.Unregister<Event>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Event");
			ecs::Loader::UnregisterSubRoutine(L"Event");

			m_pSystems = nullptr;
		}

		void Module::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<System>(dt);
		}

	}
}
