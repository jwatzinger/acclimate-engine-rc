#include "RegisterEvents.h"
#include "Component.h"
#include "EventTrigger.h"
#include "Core\EventLoader.h"
#include "Core\EventDeclaration.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"

namespace acl
{
	namespace event
	{
		namespace detail
		{
			ecs::EntityManager* _pEntities;
		}

		USE_OBJECT(ecs::EntityHandle);

		void registerEvents(ecs::EntityManager& entities)
		{
			detail::_pEntities = &entities;

			// events
			core::EventLoader::RegisterEvent<CreateEventEntity>();
			core::EventLoader::RegisterEvent<AttachEventComponent>();

			// trigger
			core::EventLoader::RegisterTrigger<EventStartTrigger>();
			core::EventLoader::RegisterTrigger<EventTickTrigger>();
			core::EventLoader::RegisterTrigger<EventInputTrigger>();
		}

		/*****************************************
		* CreateEventEntity
		******************************************/

		void CreateEventEntity::Reset(void)
		{
		}

		void CreateEventEntity::OnExecute(double dt)
		{
			auto& entity = detail::_pEntities->CreateEntity(L"Event");

			entity->AttachComponent<event::Event>(GetAttribute<std::wstring>(0));
			CallOutput(0);
		}

		const core::EventDeclaration& CreateEventEntity::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"event", core::AttributeType::STRING, false }
			};

			const core::AttributeVector vReturns =
			{
				{ L"entity", core::getObjectType<ecs::EntityHandle>(), false }
			};

			static const core::EventDeclaration decl(L"CreateEventEntity", vAttributes, vReturns);

			return decl;
		}

		/*****************************************
		* AttachEventComponent
		******************************************/

		void AttachEventComponent::Reset(void)
		{
		}

		void AttachEventComponent::OnExecute(double dt)
		{
			auto& entity = GetAttribute<ecs::EntityHandle>(1);
			entity->AttachComponent<event::Event>(GetAttribute<std::wstring>(0));
			CallOutput(0);
		}

		const core::EventDeclaration& AttachEventComponent::GenerateDeclaration(void)
		{
			const core::AttributeVector vAttributes =
			{
				{ L"event", core::AttributeType::STRING, false },
				{ L"entity", core::getObjectType<ecs::EntityHandle>(), false },
			};

			static const core::EventDeclaration decl(L"AttachEventComponent", vAttributes, core::AttributeVector());

			return decl;
		}


	}
}