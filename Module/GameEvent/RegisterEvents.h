#pragma once
#include "Core\Event.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
	}

	namespace event
	{

		void registerEvents(ecs::EntityManager& entities);

		/*****************************************
		* CreateEventEntity
		******************************************/

		class CreateEventEntity :
			public core::BaseEvent<CreateEventEntity>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;

			static const core::EventDeclaration& GenerateDeclaration(void);
		};

		/*****************************************
		* AttachEventComponent
		******************************************/

		class AttachEventComponent :
			public core::BaseEvent<AttachEventComponent>
		{
		public:

			void Reset(void) override;
			void OnExecute(double dt) override;


			static const core::EventDeclaration& GenerateDeclaration(void);
		};

	}
}