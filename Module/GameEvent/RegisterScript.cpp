#include "RegisterScript.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace event
	{
		/*******************************************
		* Event
		*******************************************/

		Event* AttachEvent(ecs::EntityHandle* entity, const std::wstring& stName)
		{
			return (*entity)->AttachComponent<Event>(stName);
		}

		void RemoveEvent(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<Event>();
		}

		Event* GetEvent(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<Event>();
		}

		void registerScript(script::Core& script)
		{
			auto eventComp = script.RegisterReferenceType("Event", asOBJ_NOCOUNT);

			// position
			script.RegisterGlobalFunction("Event@ AttachEvent(Entity& in, const string& in)", asFUNCTION(AttachEvent));
			script.RegisterGlobalFunction("void RemoveEvent(Entity& in)", asFUNCTION(RemoveEvent));
			script.RegisterGlobalFunction("Event@ GetEvent(const Entity& in)", asFUNCTION(GetEvent));
		}

	}
}
