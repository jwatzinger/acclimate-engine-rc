#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace event
	{

		void registerScript(script::Core& script);
	}
}
