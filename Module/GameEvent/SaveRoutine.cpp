#include "SaveRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace event
	{

		void SaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(auto* pEvent = entity.GetComponent<Event>())
			{
				auto& component = entityNode.InsertNode(L"Event");
				component.ModifyAttribute(L"name", pEvent->stName);
			}
		}

	}
}
