#include "System.h"
#include "Component.h"
#include "EventTrigger.h"
#include "Core\Events.h"
#include "Core\EventContext.h"
#include "Core\EventTriggerHandler.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Input\Handler.h"
#include "System\Convert.h"
#include "System\Log.h"

namespace acl
{
	namespace event
	{

		USE_OBJECT(input::MappedInput);

		System::System(const core::EventContext& events) : 
			m_pEvents(&events)
		{
		}

		void System::Init(ecs::MessageManager& messageManager)
		{
			messageManager.Subscribe(ecs::MessageRegistry::GetMessageId(L"ProcessInput"), *this);
		}

		void System::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<Event>();

			m_pEvents->trigger.TriggerAll<EventTickTrigger>((float)dt);

			for(auto& entity : vEntities)
			{
				auto& event = *entity->GetComponent<Event>();

				if(!event.pInstance)
				{
					auto pInstance = m_pEvents->events.Get(event.stName);
					if(pInstance)
					{
						event.pInstance = new core::EventInstance(*pInstance);
						event.pInstance->SetEntity(entity);
						m_pEvents->trigger.AddInstance(*event.pInstance);
						event.pHandler = &m_pEvents->trigger;

						event.pInstance->Trigger<EventStartTrigger>();
					}
				}
				else
					event.pInstance->Run(dt);
			}
		}

		void System::ReceiveMessage(const ecs::BaseMessage& message)
		{
			static const auto inputId = ecs::MessageRegistry::GetMessageId(L"ProcessInput");
			if(message.Check(inputId))
			{
				auto pEntity = message.GetParam<ecs::Entity>(L"Entity");
				auto pInput = message.GetParam<input::MappedInput>(L"Input");

				if(auto pEvent = pEntity->GetComponent<Event>())
				{
					if(pEvent->pInstance)
						core::EventTriggerHandler::TriggerInstance<EventInputTrigger>(*pEvent->pInstance, *pInput);
				}
			}
		}

	}
}

