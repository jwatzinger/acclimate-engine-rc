#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace core
	{
		struct EventContext;
	}

	namespace event
	{

		class System : 
			public ecs::System<System>
		{
		public:
			System(const core::EventContext& events);

			void Init(ecs::MessageManager& messageManager);
			void Update(double dt);
			void ReceiveMessage(const ecs::BaseMessage& message);

		private:

			const core::EventContext* m_pEvents;
		};

	}
}


