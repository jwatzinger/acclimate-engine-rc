#include "AttachController.h"
#include "IEditor.h"
#include "Component.h"

namespace acl
{
	namespace gameInput
	{
		
		/***************************************
		* Input
		***************************************/

		InputAttachController::InputAttachController(gui::Module& module) :
			BaseController(module, L"Editor/AttachInput.axm")
		{
			m_pName = GetWidgetByName<NameBox>(L"Name");
		}

		editor::IComponentController& InputAttachController::Clone(void) const
		{
			return *new InputAttachController(*m_pModule);
		}

		void InputAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<Input>(m_pName->GetContent());
		}

		gui::BaseController& InputAttachController::GetController(void)
		{
			return *this;
		}

		size_t InputAttachController::GetComponentId(void) const
		{
			return Input::family();
		}

	}
}

