#pragma once
#include "IComponentController.h"
#include "Gui\Textbox.h"
#include "Gui\BaseController.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace gameInput
	{

		/***************************************
		* Input
		***************************************/

		class InputAttachController :
			public editor::IComponentController, gui::BaseController
		{
			typedef gui::Textbox<> NameBox;
		public:
			InputAttachController(gui::Module& module);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			NameBox* m_pName;
		};



	}
}


