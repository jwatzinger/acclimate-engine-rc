#include "Component.h"
#include "Input\Handler.h"

namespace acl
{
	namespace gameInput
	{

		Input::Input(const std::wstring& stHandler) : stHandler(stHandler),
			pHandler(nullptr)
		{
		}

		Input::~Input(void)
		{
			if(pHandler)
				pHandler->SigProcessInput.Disconnect(this, &Input::OnProcessInput);
		}

		void Input::OnProcessInput(const input::MappedInput& input)
		{
			SigProcessInput(GetParent(), input);
		}

	}
}

