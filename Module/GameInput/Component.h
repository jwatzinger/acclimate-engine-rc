#pragma once
#include "Core\Signal.h"
#include "Entity\Component.h"

namespace acl
{
	namespace input
	{
		class Handler;
		class Module;
		struct MappedInput;
	}

	namespace gameInput
	{

		class Input :
			public ecs::Component<Input>
		{
		public:

			Input(const std::wstring& stHandler);
			~Input(void);

			std::wstring stHandler;
			input::Handler* pHandler;

			core::Signal<const ecs::Entity&, const input::MappedInput&> SigProcessInput;

			void OnProcessInput(const input::MappedInput& input);
		};

	}
}


