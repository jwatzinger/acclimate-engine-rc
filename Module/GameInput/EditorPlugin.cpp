#include "EditorPlugin.h"
#include "IComponentRegistry.h"
#include "IEditor.h"
#include "AttachController.h"
#include "AttributeController.h"

namespace acl
{
	namespace gameInput
	{

		void EditorPlugin::OnInit(editor::IEditor& editor)
		{
			m_pEditor = &editor;

			auto& module = editor.GetModule();
			auto& registry = editor.GetComponentRegistry();
			registry.AddComponentAttachController(L"Input", *new InputAttachController(module));
			// attriutes
			registry.AddComponentAtttributeController(L"Input", *new AttributeController);

		}
		
		void EditorPlugin::OnUpdate(void)
		{
		}

		void EditorPlugin::OnUninit(void)
		{
			auto& registry = m_pEditor->GetComponentRegistry();
			registry.RemoveComponentAttachController(L"Input");
			// attriutes
			registry.RemoveComponentAtttributeController(L"Input");
		}

		void EditorPlugin::OnBeginTest(void)
		{
		}

		void EditorPlugin::OnEndTest(void)
		{
		}

	}
}

