#include "LoadRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace gameInput
	{

		void LoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(auto* pEvent = entityNode.FirstNode(L"Input"))
				auto pInputComponent = entity.AttachComponent<Input>(pEvent->Attribute(L"handler")->GetValue());
		}

	}
}
