#include "Messages.h"

namespace acl
{
	namespace gameInput
	{
		
		ProcessInput::ProcessInput(const ecs::Entity& entity, const input::MappedInput& input) : pEntity(&entity),
			pInput(&input)
		{
		}

		const void* ProcessInput::GetParam(const std::wstring& stName, const std::type_info** type) const
		{
			if(stName == L"Entity")
			{
				*type = &typeid(*pEntity);
				return pEntity;
			}
			else if(stName == L"Input")
			{
				*type = &typeid(*pInput);
				return pInput;
			}
			else
				return nullptr;
		}

		SetInputState::SetInputState(bool takeInput) : takeInput(takeInput)
		{
		}

	}
}
