#pragma once
#include "Entity\Entity.h"
#include "Entity\Message.h"

namespace acl
{
	namespace input
	{
		struct MappedInput;
	}

	namespace gameInput
	{

		struct ProcessInput :
			public ecs::Message<ProcessInput>
		{
			ProcessInput(const ecs::Entity& entity, const input::MappedInput& input);

			const void* GetParam(const std::wstring& stName, const std::type_info** type) const override;

			const ecs::Entity* pEntity;
			const input::MappedInput* pInput;
		};

		struct SetInputState :
			public ecs::Message<SetInputState>
		{
			SetInputState(bool takeInput);

			bool takeInput;
		};

	}
}


