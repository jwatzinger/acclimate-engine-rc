#include "Module.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Messages.h"
#include "Component.h"
#include "System.h"
#include "RegisterEvents.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\MessageFactory.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"

namespace acl
{
	namespace gameInput
	{

		Module::Module(void) : m_pSystems(nullptr)
		{
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			m_pSystems->AddSystem<System>(context.input.module);

			// component
			ecs::ComponentRegistry::RegisterComponent<Input>(L"Input");
			context.ecs.componentFactory.Register<Input, const wchar_t*>();

			// io
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Input");
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Input");

			// messages
			ecs::MessageRegistry::RegisterMessage<ProcessInput>(L"ProcessInput");
			ecs::MessageRegistry::RegisterMessage<SetInputState>(L"SetInputState");
			context.ecs.messageFactory.Register<SetInputState, bool>();

			registerEvents();
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<System>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Input");
			ecs::Loader::UnregisterSubRoutine(L"Input");

			// component
			ecs::ComponentRegistry::UnregisterComponent(L"Input");
			context.ecs.componentFactory.Unregister<Input>();

			// messages
			ecs::MessageRegistry::UnregisterMessage(L"ProcessInput");

			m_pSystems = nullptr;
		}

		void Module::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<System>(dt);
		}

	}
}
