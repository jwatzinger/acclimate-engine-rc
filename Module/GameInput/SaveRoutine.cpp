#include "SaveRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace gameInput
	{

		void SaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(auto* pEvent = entity.GetComponent<Input>())
			{
				auto& component = entityNode.InsertNode(L"Input");
				component.ModifyAttribute(L"handler", pEvent->stHandler);
			}
		}

	}
}
