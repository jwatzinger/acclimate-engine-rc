#pragma once
#include "Entity\ISubSaveRoutine.h"

namespace acl
{
	namespace gameInput
	{

		class SaveRoutine : 
			public ecs::ISubSaveRoutine
		{
		public:
			
			void Execute(const ecs::Entity& entity, xml::Node& entityNode) const override;
		};

	}
}

