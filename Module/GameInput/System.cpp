#include "System.h"
#include "Component.h"
#include "Messages.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\SystemManager.h"
#include "Input\Module.h"
#include "Input\Handler.h"

namespace acl
{
	namespace gameInput
	{

		System::System(input::Module& input) :
			m_pInput(&input), m_takeInput(true)
		{
		}

		void System::Init(ecs::MessageManager& messageManager)
		{
			messageManager.Subscribe<SetInputState>(*this);
		}

		void System::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<Input>();

			for(auto& entity : vEntities)
			{
				auto& event = *entity->GetComponent<Input>();

				if(!event.pHandler)
				{
					auto pHandler = m_pInput->GetHandler(event.stHandler);
					if(pHandler)
					{
						event.pHandler = pHandler;
						event.pHandler->SigProcessInput.Connect(&event, &Input::OnProcessInput);
						event.SigProcessInput.Connect(this, &System::OnProcessInput);
					}
				}
			}
		}

		void System::OnProcessInput(const ecs::Entity& entity, const input::MappedInput& input)
		{
			if(m_takeInput)
				m_pMessages->DeliverMessage<ProcessInput>(entity, input);
		}

		void System::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(auto pInput = message.Convert<SetInputState>())
			{
				m_takeInput = pInput->takeInput;
			}
		}

	}
}

