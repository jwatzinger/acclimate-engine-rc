#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace ecs
	{
		class Entity;
	}

	namespace input
	{
		class Module;
		struct MappedInput;
	}

	namespace gameInput
	{

		class System : 
			public ecs::System<System>
		{
		public:
			System(input::Module& input);

			void Init(ecs::MessageManager& messageManager);
			void Update(double dt);
			void ReceiveMessage(const ecs::BaseMessage& message);

		private:

			void OnProcessInput(const ecs::Entity& entity, const input::MappedInput& input);

			bool m_takeInput;

			input::Module* m_pInput;
		};

	}
}


