#include "Module.h"

#include "ApiInclude.h"
#include "Core\BaseContext.h"
#include "Gfx\Context.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\IEffect.h"
#include "Gui\Renderer.h"
#include "Gui\InputHandler.h"
#include "Gui\LayoutLoader.h"
#include "Input\Loader.h"
#include "Render\Context.h"
#include "Render\ILoader.h"
#include "Render\IRenderer.h"

namespace acl
{
	namespace modules
	{

		void GuiModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void GuiModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void GuiModule::OnInit(const core::GameStateContext& context)
		{
			gfx::IEffect* pEffect = nullptr;
			if(getUsedAPI() == GraphicAPI::DX11)
				pEffect = context.gfx.resources.effects.Get(L"SpriteGUIGeometry");
			else
				pEffect = context.gfx.resources.effects.Get(L"SpriteGUI");

			context.gui.input.SetHandler(*context.input.loader.Load(L"Input.axm"));
			context.gui.loader.Load(L"Menu/Default.axl");
			context.gui.pRenderer->SetEffect(*pEffect);
			context.gui.pRenderer->SetStage(*context.render.renderer.GetStage(L"gui"));
		}

	}
}
