#pragma once
#include "Core\IModule.h"

namespace acl
{
	namespace modules
	{

		class GuiModule final :
			public core::IModule
		{
		public:

			void OnLoadResources(const gfx::LoadContext& context) override;
			void OnLoadRender(const render::Context& context) override;
			void OnInit(const core::GameStateContext& context) override;
			void OnUninit(const core::GameStateContext& context) {}

			void OnUpdate(double dt) {}
			void OnRender(void) const {}

		};

	}
}

