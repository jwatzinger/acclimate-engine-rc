#include "Export.h"
#include "HDRModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::HDRModule();
}