#include "HDRModule.h"
#include "HDRRenderSystem.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Gfx\Screen.h"
#include "Gfx\IResourceLoader.h"
#include "Render\ILoader.h"
#include "Render\Context.h"
#include "Render\Postprocessor.h"

namespace acl
{
	namespace modules
	{

		HDRModule::HDRModule(void): m_pSystems(nullptr)
		{
		}

		void HDRModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void HDRModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
			context.postprocess.SetupHDR(L"PostProcessHDR", L"final scene hdr");
		}

		void HDRModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			m_pSystems->AddSystem<ecs::HDRRenderSystem>(context.gfx.screen.GetSize(), context.gfx.resources.textures, context.gfx.effect, context.gfx.load.textures, context.gfx.load.materials, context.render.postprocess);
		}

		void HDRModule::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<ecs::HDRRenderSystem>();

			m_pSystems = nullptr;
		}

		void HDRModule::OnUpdate(double dt)
		{
			m_dt = dt;
		}

		void HDRModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::HDRRenderSystem>(0.16f);
		}

	}
}
