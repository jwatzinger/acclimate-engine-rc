#pragma once
#include "Core\IModule.h"

namespace acl
{
	namespace ecs
	{
		class SystemManager;
	}

	namespace modules
	{

		class HDRModule final :
			public core::IModule
		{
		public:
		
			HDRModule(void);

			void OnLoadResources(const gfx::LoadContext& context) override;
			void OnLoadRender(const render::Context& context) override;
			void OnInit(const core::GameStateContext& context) override;
			void OnUninit(const core::GameStateContext& contex) override;

			void OnUpdate(double dt) override;
			void OnRender(void) const override;

		private:

			double m_dt; // todo: remove hack, implement correct accounting for dt

			ecs::SystemManager* m_pSystems;

		};

	}
}

