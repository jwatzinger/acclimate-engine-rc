#include "HDRRenderSystem.h"
#include "Messages.h"
#include "Entity\Message.h"
#include "Entity\MessageManager.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Math\Vector.h"
#include "Math\Rect.h"
#include "System\Convert.h"
#include "Render\Postprocessor.h"

namespace acl
{
	namespace ecs
	{

		HDRRenderSystem::HDRRenderSystem(const math::Vector2& vScreenSize, gfx::Textures& textures, const gfx::FullscreenEffect& effect, const gfx::ITextureLoader& loader, const gfx::IMaterialLoader& materialLoader, const render::Postprocessor& postprocess) :
			m_pTextures(&textures), m_bSwitchAdaptation(false), m_bEnabled(true), m_pTextureLoader(&loader), m_pMaterialLoader(&materialLoader), m_pPostprocess(&postprocess)
		{
			/************************************************
			* Luminance convertion
			************************************************/

			m_pLuminance = &effect.CreateInstance(L"Luminance", L"PreProcess", L"luminance", 0);

			/************************************************
			* Luminance downsampling for average lum
			************************************************/

			unsigned int numMipLevelsW = static_cast<int>((logf(static_cast<float>(vScreenSize.x)) / logf(2.0f))) + 1;
			unsigned int numMipLevelsH = static_cast<int>((logf(static_cast<float>(vScreenSize.y)) / logf(2.0f))) + 1;
			m_numLuminanceMips = numMipLevelsH > numMipLevelsW ? numMipLevelsH : numMipLevelsW;

			float scale = 2.0f;
			unsigned int lastMipWidth = vScreenSize.x, lastMipHeight = vScreenSize.y;
			for(size_t i = 0; i < m_numLuminanceMips - 1; ++i)
			{
				/************************************************
				* Create mip textures
				************************************************/

				unsigned int mipHeight = vScreenSize.y >> (i + 1);
				mipHeight = (mipHeight == 0) ? 1 : mipHeight;
				unsigned int mipWidth = vScreenSize.x >> (i + 1);
				mipWidth = (mipWidth == 0) ? 1 : mipWidth;

				const bool losesInformation = (i >= m_numLuminanceMips / 2 ) && lastMipWidth % 2 || lastMipHeight % 2;

				std::wstring stName = L"Luminance";

				if(i == m_numLuminanceMips - 2 || losesInformation)
					stName += L"Last";
				else
				{
					stName += conv::ToString(i);

					m_pTextureLoader->Create(stName, math::Vector2(mipWidth, mipHeight), gfx::TextureFormats::R16F, gfx::LoadFlags::RENDER_TARGET);
				}

				/************************************************
				* Filter material and model
				************************************************/
				if(i == 0)
					m_pMaterialLoader->Load(stName, L"HDRLumFilter", 0, L"luminance");
				else
				{
					const std::wstring& stLastName = L"Luminance" + conv::ToString(i - 1);
					if(losesInformation)
					{
						m_pMaterialLoader->Load(stName, L"HDRSumLuminance", 0, stLastName);

						auto pFilter = &effect.CreateInstance(stName, L"PreProcess", stName, false);
						const float arr[4] = { (float)lastMipWidth, (float)lastMipHeight, 1.0f / lastMipWidth, 1.0f / lastMipHeight };
						pFilter->SetShaderConstant(0, arr, 1);

						m_vFilterModels.push_back(pFilter);
						break;
					}
					else
						m_pMaterialLoader->Load(stName, L"HDRLumFilter", 0, stLastName);
				}

				auto pFilter = &effect.CreateInstance(stName, L"PreProcess", stName, false);

				const float arr[4] = { 1.0f / lastMipWidth, 1.0f / lastMipHeight, 0.0f, 0.0f };
				pFilter->SetShaderConstant(0, arr, 1);

				lastMipWidth = mipWidth;
				lastMipHeight = mipHeight;

				m_vFilterModels.push_back(pFilter);
			}

			/************************************************
			* Adaptation
			************************************************/

			m_pAdaptation = &effect.CreateInstance(L"Adaptation", L"PreProcess", L"currentlum", 0);

			/************************************************
			* Bloom threshold filter
			************************************************/

			m_pBloom = &effect.CreateInstance(L"BloomFilter", L"PreProcess", L"bloomthresh", 0);

			const float thresh[4] = {1500.0f, 0.0f, 0.0f, 0.0f};
			m_pBloom->SetShaderConstant(0, thresh, 1);

			/************************************************
			* Bloom blur
			************************************************/

			m_pBlur = &effect.CreateInstance(L"BloomBlur", L"PreProcess", L"bloomthresh", 0);

			const float size[4] = {1.0f /vScreenSize.x, 1.0f/vScreenSize.y, 0, 0};
			m_pBlur->SetShaderConstant(0, size, 1);

			/************************************************
			* Tonemap
			************************************************/

			m_pTonemap = &effect.CreateInstance(L"Tonemap", L"PreProcess", L"final scene hdr", 0);
		}

		HDRRenderSystem::~HDRRenderSystem(void)
		{
			delete m_pLuminance;

			delete m_pBlur;
			delete m_pTonemap;
			delete m_pAdaptation;
			delete m_pBloom;

			for(auto pFilter : m_vFilterModels)
			{
				delete pFilter;
			}
		}

		void HDRRenderSystem::Init(ecs::MessageManager& messages)
		{
			messages.Subscribe<SetHDRState>(*this);
		}

		void HDRRenderSystem::Update(double dt)
		{
			if(!m_bEnabled)
				return;

			const auto& stProcessedScene = m_pPostprocess->GetPreHDROutput();

			auto pScene = m_pTextures->Get(stProcessedScene);
			m_pLuminance->SetTexture(0, pScene);
			m_pTonemap->SetTexture(1, pScene);

			/************************************************
			* Luminance convertion
			************************************************/

			m_pLuminance->Draw();

			/************************************************
			* Luminance downsampling for average lum
			************************************************/

			for(auto pFilter : m_vFilterModels)
			{
				pFilter->Draw();
			}

			/************************************************
			* Adaptation
			************************************************/

			if(m_bSwitchAdaptation)
			{
				m_pAdaptation->SetTexture(1, m_pTextures->Get(L"currentlum"));
				m_pAdaptation->SetRenderTarget(0, m_pTextures->Get(L"previouslum"));
			}
			else
			{
				m_pAdaptation->SetTexture(1, m_pTextures->Get(L"previouslum"));
				m_pAdaptation->SetRenderTarget(0, m_pTextures->Get(L"currentlum"));
			}

			const float arr[4] = {min(1.0f, max(0.0f, (float)dt)), 0.0f, 0.0f, 0.0f};
			m_pAdaptation->SetShaderConstant(0, arr, 1);
			m_pAdaptation->Draw();

			/************************************************
			* Bloom
			************************************************/

			//m_pBloom->Draw(); // filter pass
			//m_pBlur->Draw(); // blur pass

			/************************************************
			* Tonemap
			************************************************/

			if(m_bSwitchAdaptation)
				m_pTonemap->SetTexture(0, m_pTextures->Get(L"previouslum"));
			else
				m_pTonemap->SetTexture(0, m_pTextures->Get(L"currentlum"));

			m_pTonemap->Draw();

			m_bSwitchAdaptation = !m_bSwitchAdaptation;
		}

		void HDRRenderSystem::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(const SetHDRState* pHDRState = message.Convert<SetHDRState>())
				m_bEnabled = pHDRState->bEnable;
		}

	}
}