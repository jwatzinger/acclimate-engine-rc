#pragma once
#include "Core\Dll.h"
#include "Entity\System.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gfx
	{
		class FxInstance;
		class FullscreenEffect;
		class ITextureLoader;
		class IMaterialLoader;
	}

	namespace render
	{
		class Postprocessor;
	}

	namespace ecs
	{

		class HDRRenderSystem:
			public System<HDRRenderSystem>
		{
			typedef std::vector<gfx::FxInstance*> FilterVector;
		public:
			HDRRenderSystem(const math::Vector2& vScreenSize, gfx::Textures& textures, const gfx::FullscreenEffect& effect, const gfx::ITextureLoader& loader, const gfx::IMaterialLoader& materialLoader, const render::Postprocessor& postprocess);
			~HDRRenderSystem(void);

			void Init(ecs::MessageManager& messages) override;

			void Update(double dt) override;

			void ReceiveMessage(const ecs::BaseMessage& message) override;

		private:

			unsigned int m_numLuminanceMips;
			bool m_bSwitchAdaptation, m_bEnabled;

			gfx::Textures* m_pTextures;
			const gfx::ITextureLoader* m_pTextureLoader;
			const gfx::IMaterialLoader* m_pMaterialLoader;
			const render::Postprocessor* m_pPostprocess;

			gfx::FxInstance* m_pLuminance, *m_pTonemap, *m_pAdaptation, *m_pBloom, *m_pBlur;

			FilterVector m_vFilterModels;
		};

	}
}
