#pragma once
#include "Entity\Message.h"

namespace acl
{
	namespace ecs
	{

		class SetHDRState :
			public ecs::Message<SetHDRState>
		{
		public:
			SetHDRState(bool bEnable): bEnable(bEnable) {}

			const bool bEnable;
		};

	}
}