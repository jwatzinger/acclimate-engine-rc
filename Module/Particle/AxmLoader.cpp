#include "AxmLoader.h"
#include "Effect.h"
#include "Emitter.h"
#include "BaseSpawner.h"
#include "Renderer.h"
#include "Particles.h"
#include "ApiInclude.h"
#include "Gfx\Color.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\IModel.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\IMaterial.h"
#include "Math\Vector.h"
#include "Math\BaseFunctors.h"
#include "System\Convert.h"
#include "System\Log.h"
#include "XML\Doc.h"

namespace acl
{
    namespace particle
    {

		AxmLoader::AxmLoader(const gfx::ITextureLoader& loader, const gfx::IMeshLoader& meshLoader, const gfx::Models& models, const gfx::IMaterialLoader& materials) :
			m_meshLoader(meshLoader), m_materials(materials), m_loader(loader), m_pModel(models[L"particle"])
        {
        }

		/** Converts a spawner from an xml node.
         *  This method takes an xml node and translates it into a spawner. All known
         *  base spawner types are supported.
         *
         *  @param[in] node Const reference to the node containing the spawner information.
         *
         *  @return A reference to the created ISpawner object.
         */
        ISpawner& ConvertSpawner(const xml::Node& node)
        {
            const std::wstring& stSpawnerType = node.Attribute(L"type")->GetValue();

			if(stSpawnerType == L"sphere")
				return *new SphereSpawner(node.Attribute(L"minrad")->AsFloat(), node.Attribute(L"maxrad")->AsFloat());
			else if(stSpawnerType == L"cone")
			{
				math::Vector3 vOrientation(0.0f, 1.0f, 0.0f);
				if(const xml::Node* pOrientation = node.FirstNode(L"orientation"))
					vOrientation = math::Vector3( pOrientation->Attribute(L"x")->AsFloat(), pOrientation->Attribute(L"y")->AsFloat(), pOrientation->Attribute(L"z")->AsFloat() );
        
				return *new ConeSpawner( vOrientation, node.Attribute(L"minlen")->AsFloat(), node.Attribute(L"maxlen")->AsFloat(), node.Attribute(L"minrad")->AsFloat(), node.Attribute(L"maxrad")->AsFloat() );
			}
			else if(stSpawnerType == L"ring")
			{
				math::Vector3 vOrientation(0.0f, 1.0f, 0.0f);
				if(const xml::Node* pOrientation = node.FirstNode(L"orientation"))
					vOrientation = math::Vector3( pOrientation->Attribute(L"x")->AsFloat(), pOrientation->Attribute(L"y")->AsFloat(), pOrientation->Attribute(L"z")->AsFloat() );

				return *new RingSpawner(vOrientation, node.Attribute(L"minrad")->AsFloat(), node.Attribute(L"maxrad")->AsFloat(), node.Attribute(L"height")->AsFloat());
			}
            else if(stSpawnerType == L"box")
            {
                return *new BoxSpawner(node.Attribute(L"sx")->AsFloat(), node.Attribute(L"sy")->AsFloat(), node.Attribute(L"sz")->AsFloat());
            }
			else
				return *new PointSpawner(0.0f, 0.0f, 0.0f);
        }

        std::shared_ptr<math::IFunctor> LoadFunctor(const xml::Node& node)
        {
            const std::wstring& stFunctorType = node.Attribute(L"type")->GetValue();
			if(stFunctorType == L"poly3")
				return std::make_shared<math::Polynomal3>(node.Attribute(L"x3")->AsFloat(), node.Attribute(L"x2")->AsFloat(), node.Attribute(L"x")->AsFloat(), node.Attribute(L"d")->AsFloat());
			else if(stFunctorType == L"poly2")
				return std::make_shared<math::Polynomal2>(node.Attribute(L"x2")->AsFloat(), node.Attribute(L"x")->AsFloat(), node.Attribute(L"d")->AsFloat());
			else
				return std::make_shared<math::LinearFunctor>();
        }

        void AxmLoader::Load(const std::wstring& stFilename) const
        {
			xml::Doc doc;
			doc.LoadFile(stFilename.c_str());

			const xml::Node* pEffects = doc.Root(L"Effects");
	
			if(const xml::Node::NodeVector* pEffect = pEffects->Nodes(L"Effect"))
			{
				unsigned int count = 0;
                unsigned int cEmitters = 0;
				for(auto& effect : *pEffect)
				{
                    Effect& Effect = *new particle::Effect();
					AddResource(count, Effect);
					if( const xml::Node::NodeVector* pEmitter = effect->Nodes(L"Emitter") )
					{
						for(auto& emitter : *pEmitter)
						{
							EmitterParams params;
							//read attributes
							const std::wstring& stTextureName(emitter->Attribute(L"texture")->GetValue());
							params.cParticles = emitter->Attribute(L"count")->AsInt();
							params.cSpawnRate = emitter->Attribute(L"spawn")->AsInt();
							params.bRespawn = emitter->Attribute(L"respawn")->AsBool();
							params.energyMin = emitter->Attribute(L"energymin")->AsFloat();
							params.energyMax = emitter->Attribute(L"energymax")->AsFloat();
							params.scaleMin = emitter->Attribute(L"minscale")->AsFloat();
							params.scaleMax = emitter->Attribute(L"maxscale")->AsFloat();
							params.scaleSpeed = emitter->Attribute(L"scalespeed")->AsFloat();
							params.gravity = emitter->Attribute(L"gravity")->AsFloat();
							const std::wstring& stBillboard(emitter->Attribute(L"billboard")->GetValue());
							const std::wstring& stBlendType(emitter->Attribute(L"blend")->GetValue());

							//choose blend type
							if(stBlendType == L"normal")
								params.blendType = BlendType::NORMAL;
							else if(stBlendType == L"blend")
								params.blendType = BlendType::BLEND;
							else if(stBlendType == L"add")
								params.blendType = BlendType::ADD;
							else if(stBlendType == L"sub")
								params.blendType = BlendType::SUB;

							//color
							gfx::FColor startColor(1.0f, 1.0f, 1.0f, 1.0f);
							if(const xml::Node* pNode = emitter->FirstNode(L"Color"))
								startColor = gfx::FColor(pNode->Attribute(L"r")->AsFloat(), pNode->Attribute(L"g")->AsFloat(), pNode->Attribute(L"b")->AsFloat(), pNode->Attribute(L"a")->AsFloat());

							//end color
							gfx::FColor endColor(startColor);
							if(const xml::Node* pNode = emitter->FirstNode(L"Endcolor"))
								endColor = gfx::FColor(pNode->Attribute(L"r")->AsFloat(), pNode->Attribute(L"g")->AsFloat(), pNode->Attribute(L"b")->AsFloat(), pNode->Attribute(L"a")->AsFloat());

							//particle emitter
							std::shared_ptr<IEmitter> pParticleEmitter;
							//spawner
							if(const xml::Node* pSpawner = emitter->FirstNode(L"Spawner"))
								pParticleEmitter = std::make_shared<Emitter>(ConvertSpawner(*pSpawner));
							else
								pParticleEmitter = std::make_shared<Emitter>(*new PointSpawner(0.0f, 0.0f, 0.0f));

							//accelerator
							if(const xml::Node* pAccelerator = emitter->FirstNode(L"Accelerator"))
							{
								const math::Vector3 vOff(pAccelerator->Attribute(L"ox")->AsFloat(), pAccelerator->Attribute(L"oy")->AsFloat(), pAccelerator->Attribute(L"ox")->AsFloat());
								if(const xml::Node* pSpawner = pAccelerator->FirstNode(L"Spawner"))
									pParticleEmitter->SetAccelerator(vOff, ConvertSpawner(*pSpawner));
							}

							//create interpolator
							const std::wstring stInterpolator = L"Interpolator" + conv::ToString(cEmitters);

							gfx::ITexture& texture = *m_loader.Create(stInterpolator, math::Vector2(256, 1), gfx::TextureFormats::R32, gfx::LoadFlags::CPU_WRITE);

							//choose billboard type
							BillboardType billType;
							unsigned int permutation = 0;
							if(stBillboard == L"screen")
								billType = BillboardType::SCREEN;
							else if(stBillboard == L"axis")
							{
								billType = BillboardType::AXIS;
								permutation = 1;
							}
							else if(stBillboard == L"floor")
							{
								billType = BillboardType::FLOOR;
								permutation = 2;
							}
							else if(stBillboard == L"dir")
								billType = BillboardType::DIRECTION;

							std::wstring stMeshName;
							gfx::IGeometry::AttributeVector vAttributes;
							gfx::IMaterial* pMaterial = nullptr;

							if(getUsedAPI() != GraphicAPI::DX9)
							{
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 4, 1, 1);
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 2, gfx::AttributeType::FLOAT, 1, 1, 1);
								stMeshName = L"ParticlePoint";
								pMaterial = m_materials.Load(stTextureName + L"Particle", L"ParticleGeometry", permutation, stTextureName);
							}
							else
							{
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 3, 1, 1);
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 2, gfx::AttributeType::FLOAT, 3, 1, 1);
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 3, gfx::AttributeType::FLOAT, 3, 1, 1);
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 4, gfx::AttributeType::FLOAT, 3, 1, 1);
								vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 5, gfx::AttributeType::FLOAT, 1, 1, 1);
								stMeshName = L"particle";
								pMaterial = m_materials.Load(stTextureName + L"Particle", L"ParticleInstAdd", 0, stTextureName);
							}

							pMaterial->SetVertexTexture(0, &texture);
							gfx::IInstancedMesh& mesh = *m_meshLoader.Instanced(stMeshName, params.cParticles, vAttributes);

							// functor
							params.pFunctor = LoadFunctor(*emitter->FirstNode(L"Functor"));
								
							// model
							gfx::IModel& model = m_pModel->Clone();
							model.SetMaterial(0, pMaterial);
							auto& modelInstance = model.MakeUniqueInstance();

							Renderer* pRenderer = new Renderer(modelInstance, mesh, params.blendType, billType, startColor, endColor);
       
                            Effect.AddEmitter(params, pParticleEmitter, texture, *pRenderer);

                            cEmitters++;
						}
					}
					count++;
				}
			}
        }

    }
}
