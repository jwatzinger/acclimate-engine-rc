#pragma once
#include "Effect.h"
#include "Core\ResourceSet.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace gfx
	{
		class IMeshLoader;
        class ITextureLoader;
		class IModelLoader;
		class IMaterialLoader;
	}

    namespace xml
    {
        class Node;
    }

    namespace particle
    { 
        class ISpawner;

        /// Loads particle effects from a serialized XML-file
		/** This class is used to read in particle effects and emitters to a certain
		*   particle cache from a file in XML. */
        class AxmLoader :
			public core::ResourceLoader<unsigned int, Effect>
        {
        public:
            AxmLoader(const gfx::ITextureLoader& loader, const gfx::IMeshLoader& meshLoader, const gfx::Models& models, const gfx::IMaterialLoader& materials);

            /** Loads particle effects from a file.
             *  This method loads the particle effects consiting of
             *  multiple emitters from the specified XML file.
             *
             *  @param[in] stFilename Name of the specified file.
             */
            void Load(const std::wstring& stFilename) const override;

        private:

            const gfx::ITextureLoader& m_loader; ///< Const reference to the texture loader used for generating the interpolator textures
            const gfx::IMeshLoader& m_meshLoader; ///< Const reference to the mesh loader used to create the emitters instance buffers
			const gfx::IMaterialLoader& m_materials; ///< Const reference to the material cache used for accessing the particle materials

			const gfx::IModel* m_pModel; ///< Prototype model for all particles to render

        };

    }
}

