#include "BaseSpawner.h"
#include "Math\Utility.h"
#include "System\Random.h"

namespace acl
{
    namespace particle
    {

        /************************************
        * POINT
        *************************************/

        PointSpawner::PointSpawner(float x, float y, float z): m_vPoint(x, y, z) {}

        math::Vector3 PointSpawner::GetPoint(void) const
        {
	        return m_vPoint;
        }

        /************************************
        * LINE
        *************************************/

        LineSpawner::LineSpawner(float x, float y, float z, float lenght): m_vDirection(x, y, z), m_length(lenght)
        {
        }

        math::Vector3 LineSpawner::GetPoint(void) const
        {
	        float len = rnd::fMax(m_length);

	        return len*m_vDirection;
        }

        /***********************************
        * SPHERE
        ***********************************/

        SphereSpawner::SphereSpawner(float minRadius, float maxRadius) : m_minRadius(minRadius), m_radiusDist(maxRadius - minRadius) {}

        math::Vector3 SphereSpawner::GetPoint(void) const
        {
	        const float length = rnd::fDist(m_minRadius, m_radiusDist);
	        const float angleX = rnd::fMax(360.f);
	        const float angleY = rnd::fMax(360.f);

	        const float r = length*sin(math::degToRad(90 - angleY));

	        math::Vector3 Temp(r*sin(math::degToRad(90-angleX)), length*sin(math::degToRad(angleY)), r*sin(math::degToRad(angleX)));

	        return Temp;
        }

        /***********************************
        * BOX
        ***********************************/

        BoxSpawner::BoxSpawner(float sx, float sy, float sz): m_vPosition(-sx, -sy, -sz), m_vSize(sx*2, sy*2, sz*2) {}

        math::Vector3 BoxSpawner::GetPoint(void) const
        {
	        const float x = rnd::fDist(m_vPosition.x, m_vSize.x);
	        const float y = rnd::fDist(m_vPosition.y, m_vSize.y);
	        const float z = rnd::fDist(m_vPosition.z, m_vSize.z);

	        return math::Vector3(x, y, z);
        }

        /***********************************
        * CONE
        ***********************************/

        ConeSpawner::ConeSpawner(const math::Vector3& vDirection, float minLength, float maxLength, float minRadius, float maxRadius): m_minLength(minLength), m_lengthDist(maxLength - minLength), m_minRadius(minRadius), m_radiusDist(maxRadius-minRadius) 
        {
	        //normalize direction for angle calculation
	        math::Vector3 vNormDirection(vDirection.z, vDirection.y, -vDirection.x);
	        vNormDirection.Normalize();

	        //original orientation axis
	        const math::Vector3 vUp(0.0f, 0.0f, -1.0f);

	        //create rotation axis by orientation x z-axis
	        math::Vector3 vAxis = vNormDirection.Cross(vUp);
	        vAxis.Normalize();

	        //calculate angle between axis and direction vector via dot product
	        const float angle = acos(vUp.Dot(vNormDirection));

	        //set rotation matrix
	        m_mRotation = math::MatRotationAxis(vAxis, angle);
        }

        math::Vector3 ConeSpawner::GetPoint(void) const
        {
	        const float radiusLength = rnd::fDist(m_minRadius, m_radiusDist);
	        const float randAngle = rnd::fMax(360.f);

	        math::Vector3 vTemp = math::Vector3(radiusLength*sin(math::degToRad(randAngle)), radiusLength*sin(math::degToRad(90-randAngle)), m_minLength + m_lengthDist);

	        const float length = rnd::fDist(m_minLength, m_lengthDist);

	        vTemp.Normalize();
	        vTemp *= length;

	        return m_mRotation.TransformCoord(vTemp);
        }

        /**************************************
        * RING
        **************************************/

        RingSpawner::RingSpawner(const math::Vector3& vOrientation, float minRadius, float maxRadius, float height): m_minRadius(minRadius), m_radiusDist(maxRadius - minRadius), m_height(height)
        {
	        //normalize direction for angle calculation
	        math::Vector3 vNormDirection(vOrientation.z, vOrientation.y, -vOrientation.x);
	        vNormDirection.Normalize();

	        //original orientation axis
	        const math::Vector3 vUp(0.0f, 0.0f, -1.0f);

	        //create rotation axis by orientation x z-axis
	        math::Vector3 vAxis = vNormDirection.Cross(vUp);
	        vAxis.Normalize();

	        //calculate angle between axis and direction vector via dot product
	        const float angle = acos(vUp.Dot(vNormDirection));

	        m_mRotation = math::MatRotationAxis(vAxis, -angle);
        }

        math::Vector3 RingSpawner::GetPoint(void) const
        {
	        float randAngle = rnd::fMax(360.0f);
	        float height = rnd::fRange(-m_height, m_height);
	        float radius = rnd::fDist(m_minRadius, m_radiusDist);

	        math::Vector3 vTemp = math::Vector3(radius*sin(math::degToRad(randAngle)), radius*sin(math::degToRad(90-randAngle)), height);

	        return m_mRotation.TransformCoord(vTemp);
        }

        /**************************************
        * TORUS
        **************************************/

        TorusSpawner::TorusSpawner(const math::Vector3& vOrientation, float outerRadius, float innerRadius): m_outerRadius(outerRadius), m_innerRadius(innerRadius)
        {
	        //normalize direction for angle calculation
	        math::Vector3 vNormDirection(vOrientation.z, vOrientation.y, -vOrientation.x);
	        vNormDirection.Normalize();

	        //original orientation axis
	        const math::Vector3 vUp(0.0f, 0.0f, -1.0f);

	        //create rotation axis by orientation x z-axis
	        math::Vector3 vAxis = vNormDirection.Cross(vUp);
	        vAxis.Normalize();

	        //calculate angle between axis and direction vector via dot product
	        const float angle = acos(vUp.Dot(vNormDirection));

	        //set rotation matrix
	        m_mRotation = math::MatRotationAxis(vAxis, -angle);
        }

        math::Vector3 TorusSpawner::GetPoint(void) const
        {
	        //get random position using angle around up axis and
	        //random height
	        const float randAngle = rnd::fMax(360.0f);
	        const float height = rnd::fRange(-m_innerRadius, m_innerRadius);

	        //determine whether the particle lies outside or inside of outer radius
	        const bool bIn = rnd::boolean();
	        //store radius as temp
	        float radius = m_outerRadius;
	        //add or sub inverse height to correctly choose the z-position depending on where pericle lies
	        if(bIn)
		        radius += m_innerRadius - abs(height);
	        else
		        radius -= m_innerRadius - abs(height);

	        //create position vector from angle and sin interpolated height
	        math::Vector3 vTemp = math::Vector3( radius * sin( math::degToRad(randAngle) ), radius * sin( math::degToRad(90 - randAngle) ), sin( height * (math::PI / 4) ) );

	        //rotate torus to fit orientation
	        return m_mRotation.TransformCoord(vTemp);
        }

        /**************************************
        * TRI
        **************************************/

        TriSpawner::TriSpawner(const math::Vector3& p1, const math::Vector3& p2, const math::Vector3& p3): m_p1(p1), m_p2(p2), m_p3(p3)
        {
        }

        math::Vector3 TriSpawner::GetPoint(void) const
        {
	        math::Vector3 Temp = m_p2 - m_p1;
	        Temp *= rnd::fMax(1.0f);
	        Temp += m_p1;
	        Temp -= m_p3;
	        Temp *= rnd::fMax(1.0f);
	        Temp += m_p3;

	        return Temp;
        }

        /**************************************
        * MESH
        **************************************/

        MeshSpawner::MeshSpawner(const math::Vector3* pVertices, unsigned int cVertices): m_cVertices(cVertices/3)
        {
	        m_pTris = new TriSpawner*[m_cVertices];
	        for(unsigned int i = 0; i<m_cVertices; i++)
	        {
		        m_pTris[i] = new TriSpawner(pVertices[i*3], pVertices[i*3+1], pVertices[i*3+2]);
	        }
        }

        MeshSpawner::~MeshSpawner(void)
        {
	        for(unsigned int i = 0; i < m_cVertices; i++)
	        {
		        delete m_pTris[i];
	        }
	        delete[] m_pTris;
        }

        math::Vector3 MeshSpawner::GetPoint(void) const
        {
	        const unsigned int id = rnd::iMax(m_cVertices);

	        return m_pTris[id]->GetPoint();
        }

    }
}