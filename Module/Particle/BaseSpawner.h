#pragma once
#include "ISpawner.h"
#include "Math\Matrix.h"

namespace acl
{
	namespace particle
	{
		/************************************
		* POINT
		*************************************/

		class PointSpawner : 
            public ISpawner
		{
		public:
			PointSpawner(float x, float y, float z);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Vector3 m_vPoint;
		};

		/************************************
		* LINE
		*************************************/

		class LineSpawner : 
            public ISpawner
		{
		public:
			LineSpawner(float x, float y, float z, float length);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Vector3 m_vDirection;
			float m_length;
		};

		/************************************
		* SPHERE
		*************************************/

		class SphereSpawner :
            public ISpawner
		{
		public:
			SphereSpawner(float minRadius, float maxRadius);

			math::Vector3 GetPoint(void) const override;

		private:

			float m_minRadius, m_radiusDist;
		};

		/************************************
		* BOX
		*************************************/

		class BoxSpawner : 
            public ISpawner
		{
		public:
			BoxSpawner(float sx, float sy, float sz);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Vector3 m_vPosition;
			math::Vector3 m_vSize;
		};

		/************************************
		* CONE
		*************************************/

		class ConeSpawner : 
            public ISpawner
		{
		public:

			ConeSpawner(const math::Vector3& vDirection, float minLength, float maxLength, float minRadius, float maxRadius);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Matrix m_mRotation;
			float m_minLength, m_lengthDist, m_minRadius, m_radiusDist;

		};

		/************************************
		* RING
		*************************************/

		class RingSpawner : 
            public ISpawner
		{
		public:

			RingSpawner(const math::Vector3& vOrientation, float minRadius, float maxRadius, float height);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Matrix m_mRotation;
			float m_minRadius, m_radiusDist, m_height;
		};

		/************************************
		* TORUS
		*************************************/

		class TorusSpawner : 
            public ISpawner
		{
		public:

			TorusSpawner(const math::Vector3& vOrientation, float outerRadius, float innerRadius);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Matrix m_mRotation;
			float m_outerRadius, m_innerRadius;
		};

		/************************************
		* TRI
		*************************************/

		class TriSpawner : 
            public ISpawner
		{
		public:

			TriSpawner(const math::Vector3 &P1, const math::Vector3 &P2, const math::Vector3 &P3);

			math::Vector3 GetPoint(void) const override;

		private:

			math::Vector3 m_p1, m_p2, m_p3;
		};

		/************************************
		* MESH
		*************************************/

		class MeshSpawner : 
            public ISpawner
		{
		public:

			MeshSpawner(const math::Vector3* pVertices, unsigned int cVertices);
			~MeshSpawner(void);

			math::Vector3 GetPoint(void) const override;

		private:

			unsigned int m_cVertices;
			TriSpawner** m_pTris;
		};

	}
}

