#include "Components.h"
#include "Effect.h"

namespace acl
{
	namespace ecs
	{
		
	    Particle::Particle(unsigned int id, bool bRemoveOnFinish): id(id), bRemoveOnFinish(bRemoveOnFinish), pEffect(nullptr)
        {
        }

		Particle::~Particle(void)
		{
			delete pEffect;
		}

	}
}