#pragma once
#include "Entity\Component.h"

namespace acl
{
	namespace particle
	{
		class Effect;
	}

	namespace ecs
	{
	
        struct Particle : 
			Component<Particle>
        {
	        Particle(unsigned int id, bool removeOnFinish);
            ~Particle(void);

	        unsigned int id;
			const bool bRemoveOnFinish;
			particle::Effect* pEffect;
        };

	}
}