#pragma once

namespace acl
{
    namespace particle
    {

        /// Used for sorting particles by depth
		/** The ParticleDepth structure stores an particle index in correclation
        *   with a certain camera depth. It is later used to perform an index sort on
        *   the particles, sorting them back to front by depth. */
		struct ParticleDepth
		{
			int index;		///< Particle index
			float depth;	///< Particle camera depth
		};

    }
}