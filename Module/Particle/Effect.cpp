#include "Effect.h"

namespace acl
{
    namespace particle
    {
        Effect::Effect(void)
        {
        }

		Effect::Effect(const Effect& effect)
		{
            //delete all emitter on this effect
            for(auto pEmitter : effect.m_vEmitter)
            {
                m_vEmitter.push_back( new ParticleEmitter(*pEmitter) );
            }
		}

        Effect::~Effect(void)
        {
            //delete all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                delete pEmitter;
            }
        }

		void Effect::SetPosition(const math::Vector3& vPosition)
		{
            //update all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                pEmitter->SetPosition(vPosition);
            }
		}

        bool Effect::IsDone(void) const
        {
            //update all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                if( !pEmitter->IsDead() )
                    return false;
            }

            return true;
        }

        void Effect::Update(const gfx::Camera& camera, double dt)
        {
            //update all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                pEmitter->Update(camera, dt);
            }
        }

        void Effect::Render(const render::IStage& stage) const
        {
            //render all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                pEmitter->Render(stage);
            }
        }

        void Effect::Reset(void)
        {
            //reset all emitter on this effect
            for(auto pEmitter : m_vEmitter)
            {
                pEmitter->Reset();
            }
        }
    }
}
