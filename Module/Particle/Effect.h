#pragma once
#include "ParticleEmitter.h"

namespace acl
{
    namespace particle
    {
		/// A particle effect.
		/** Made up of multiple particle emitter. Its position is applied to each emitter. */
        class Effect
        {
            typedef std::vector<ParticleEmitter*> EmitterVector;

        public:

            Effect(void);
			Effect(const Effect& effect);
            ~Effect(void);

			/** Constructs a new emitter.
             *  Creates an ParticleEmitter for the effect and stores it internally.
             *
             *  @param[in] args Constructor arguments for the emitter.
             */
            template<typename... Args>
            void AddEmitter(Args&&... args);

			/** 
             *  @param[in] vPosition Effect position
             */
			void SetPosition(const math::Vector3& vPosition);

			/** Checks whether the whole effect is done.
			 *	Can never reach \c true if at least one particle effect is set to
			 *	respawn.
			 *
             *  @return Indicates whether every effect is dead.
             */
            bool IsDone(void) const;

			/** Performs an effect update.
			 *	Updates every ParticleEmitter of the effect.
			 *
             *  @param[in] camera The camera for particle sorting and billboard effects.
			 *	@param[in] dt The delta timestep.
             */
            void Update(const gfx::Camera& camera, double dt);

			/** Renders the particle effect.
			 *	Tells every ParticleEmitter of the effect to render.
			 *
             *  @param[in] camera The stage to render the effect to.
             */
            void Render(const render::IStage& stage) const;

			/** Resets the effect.
			 *	Sets every ParticleEmitter of the effect to its initial state.
             */
            void Reset(void);

        private:

            EmitterVector m_vEmitter; ///< Vector of ParticleEffect
        };

        template<typename... Args>
        void Effect::AddEmitter(Args&&... args)
        {
            //push back emitter
            m_vEmitter.push_back( new ParticleEmitter(args...) );
        }

    }
}