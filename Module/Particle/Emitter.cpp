#include "Emitter.h"
#include "ISpawner.h"
#include "Particle.h"

namespace acl
{
	namespace particle
	{

		/**************************************
		* ACCELERATOR
		**************************************/

		Accelerator::Accelerator(const math::Vector3& vOffset, const ISpawner& spawner): m_vOffset(vOffset), m_pSpawner(&spawner)
		{
		}

		Accelerator::~Accelerator(void)
		{
            //delete spawner
			delete m_pSpawner;
		}

		math::Vector3 Accelerator::GetStartVelocity(void) const
		{
            //get starting velocity by random point on spawner plus offset
			return m_pSpawner->GetPoint() + m_vOffset;
		}

		/**************************************
		* EMITTER
		**************************************/
		
		Emitter::Emitter(const ISpawner& spawner): m_pSpawner(&spawner), m_pAccelerator(nullptr)
		{
		}

		Emitter::~Emitter(void)
		{
            //delete spawner and accelerator
			delete m_pSpawner;
			delete m_pAccelerator;
		}

		void Emitter::SetAccelerator(const math::Vector3& vOffset, ISpawner& spawner)
		{
            //delete old accelerator
			delete m_pAccelerator;
            //create new accelerator with offset and spawner
			m_pAccelerator = new Accelerator(vOffset, spawner);
		}

		void Emitter::InitParticle(unsigned int id, Particle& particle) const
		{
            //get a random point from spawner
            const math::Vector3& vSpawn(m_pSpawner->GetPoint());
            //set particle coordinates
			particle.x[id] = vSpawn.x;
			particle.y[id] = vSpawn.y;
			particle.z[id] = vSpawn.z;

            //velocity
			math::Vector3 vVelocity(0.0f, 0.0f, 0.0f);
            //get velocity from accelerator if there is any
			if(m_pAccelerator)
				vVelocity = m_pAccelerator->GetStartVelocity();
            //set velocity
			particle.vx[id] = vVelocity.x;
			particle.vy[id] = vVelocity.y;
			particle.vz[id] = vVelocity.z;
		}

	}
}
