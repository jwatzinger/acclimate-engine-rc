#pragma once
#include "IEmitter.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace particle
	{
		/**************************************
		* ACCELERATOR
		**************************************/

		struct Accelerator
		{
			Accelerator(const math::Vector3& vOffset, const ISpawner& spawner);
			~Accelerator(void);

			math::Vector3 GetStartVelocity(void) const;
			
		private:

			math::Vector3 m_vOffset;
			const ISpawner* m_pSpawner;
		};

		/**************************************
		* EMITTER
		**************************************/

		class Emitter :
			public IEmitter
		{
		public:
			Emitter(const ISpawner& spawner);
			~Emitter(void);

			void SetAccelerator(const math::Vector3& vOffset, ISpawner& spawner);

			void InitParticle(unsigned int id, Particle& particles) const;

		private:
			
			const ISpawner* m_pSpawner;
			const Accelerator* m_pAccelerator;
		};
	}
}

