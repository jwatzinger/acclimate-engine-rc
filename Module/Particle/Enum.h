#pragma once

namespace acl
{
	namespace particle
	{

		/**	The blend type the emitters particle will be using. This value decides which
        *   alpha blend function is being used. Used to achieve different transparency effects: 
        *   An explosion might use additive blending, while a fog effect might look better with 
        *   just alpha blending. */
		enum class BlendType
		{
			NORMAL, ///< No alpha blending
			BLEND,	///< Standard alpha blending
			ADD,	///< Additive blending
			SUB,	///< Subtractive blending
		};

        /** The type of billboard the emitters particle will be using. Determines the 
        *   function used to align the particles to a certain direction. This might 
        *   be the screen, the floor, the particles travel direction or a fixed axis. 
        *   Used to create different types of emitter effects. */
		enum class BillboardType
		{
			AXIS,		///< Alignement along the y-axi 
			SCREEN,		///< Alignement towards the screen
			FLOOR,		///< Aligmenent towards the floor
			DIRECTION	///< Aligement towards the traveling direction
		};

	}
}