#include "ExSpawner.h"

namespace acl
{
    namespace particle
    {

        /************************************
        * CUBE
        *************************************/

        CubeSpawner::CubeSpawner(float size): BoxSpawner(size, size, size) 
        {
        }

        /************************************
        * DISC
        *************************************/

        DiscSpawner::DiscSpawner(const math::Vector3& vOrientation, float radius): RingSpawner(vOrientation, 0.0f, radius, 0.0f)
        {
        }
    }
}
