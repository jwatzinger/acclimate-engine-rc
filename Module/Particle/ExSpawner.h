#pragma once
#include "BaseSpawner.h"

namespace acl
{
	namespace particle
	{
		/************************************
		* CUBE
		*************************************/

		class CubeSpawner : public BoxSpawner
		{
		public:
			CubeSpawner(float size);
		};

		/************************************
		* DISC
		*************************************/

		class DiscSpawner : public RingSpawner
		{
		public:
			DiscSpawner(const math::Vector3& vOrientation, float radius);
		};
	}
}