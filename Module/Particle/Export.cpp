#include "Export.h"
#include "ParticleModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::ParticleModule();
}