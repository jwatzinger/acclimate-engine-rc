#pragma once

namespace acl
{
    namespace math
    {
        struct Vector3;
    }

	namespace particle
	{
        class ISpawner;
        struct Particle;

		/// Emitter interface.
		/** This interface must be implemented to create a new emitter functor. It shall 
		*	use an ISpawner implementation to calculate the initial velocity of a newly 
		*	spawned particle. */
		class IEmitter
		{
		public:
			virtual ~IEmitter(void) {};

			virtual void SetAccelerator(const math::Vector3& vOffset, ISpawner& spawner) = 0;

			virtual void InitParticle(unsigned int id, Particle& particle) const = 0;
		};
	}
}