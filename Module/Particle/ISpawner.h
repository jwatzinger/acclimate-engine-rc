#pragma once
#include "Math\Vector3.h"

namespace acl
{
	namespace particle
	{
		/// Particle spawner interface.
		/** This interface must be implemented to create a new partice spawner. The spawner shall 
		*	return the spawning point for new particles, be it from a random function, or some 
		*	equations making up a virtual mathematical body. */
		class ISpawner
		{
		public:

			virtual ~ISpawner(void) = 0 {};
	
			/** Returns a point to spawn a new particle at. This method should 
             *  be implemented returning a new spawning point for a particle every
			 *	time it is called. Use eigther a random function or a counter to
			 *	maintain different spawning positions.
             *
             *  @return The position for the new particle
			 */
			virtual math::Vector3 GetPoint(void) const = 0;
		};
	}
}

