#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void ParticleLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pParticle = entityNode.FirstNode(L"Particle"))
			{
				entity.AttachComponent<Particle>(pParticle->Attribute(L"id")->AsInt(), pParticle->Attribute(L"remove")->AsBool());
			}
		}

	}
}
