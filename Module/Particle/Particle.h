#pragma once
#include <malloc.h>

namespace acl
{
	namespace particle
	{
        /// Container for a set of particles
		/** The Particle struct represents a variable number of particles using
        *   SoA-pattern. It creates its particle member information arrays
        *   SIMD-optimized, to allow cache-friendly processing in 
        *   to them, as well as methods for checking for keys and resources.
        *   It also offers the possibility to selectively clear certain
        *   resources. */
		struct Particle
		{
			#define BOUNDARY_ALIGNMENT 16

			Particle(unsigned int count)
			{
				x  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				y  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				z  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				vx  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				vy  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				vz  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				e = (float*)_mm_malloc(count* sizeof(float), BOUNDARY_ALIGNMENT);
                max = (float*)_mm_malloc(count* sizeof(float), BOUNDARY_ALIGNMENT);
				ax  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				ay  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
				az  = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
                s = (float*)_mm_malloc(count * sizeof(float), BOUNDARY_ALIGNMENT);
			}

			~Particle(void)
			{
				_mm_free(x);
				_mm_free(y);
				_mm_free(z);
				_mm_free(vx);
				_mm_free(vy);
				_mm_free(vz);
				_mm_free(e);
                _mm_free(max);
				_mm_free(ax);
				_mm_free(ay);
				_mm_free(az);
                _mm_free(s);
			}

			float   *x,     ///< x position array
                    *y,     ///< y position array
                    *z;     ///< z position array
			float   *vx,    ///< x velocity array
                    *vy,    ///< y velocity array
                    *vz;    ///< z velocity array
			float   *ax,    ///< x acceleration array
                    *ay,    ///< y acceleration array
                    *az;    ///< z acceleration array
			float   *e,     ///< energy array
                    *max;   ///< max energy array
            float   *s;     ///< scalation array
		};

	}
}