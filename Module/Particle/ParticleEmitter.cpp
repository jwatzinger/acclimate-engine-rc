#include "ParticleEmitter.h"
#include <algorithm>
#include "BaseSpawner.h"
#include "Emitter.h"
#include "Effect.h"
#include "Renderer.h"
#include "Gfx\Camera.h"
#include "Gfx\IMaterial.h"
#include "Gfx\ITexture.h"
#include "Gfx\TextureAccessors.h"
#include "System\Random.h"

namespace acl
{
    namespace particle
    {
        ParticleEmitter::ParticleEmitter(const EmitterParams& params, const std::shared_ptr<IEmitter>& emitter, gfx::ITexture& interpolator, Renderer& renderer): m_cMaxParticles(params.cParticles), 
			m_cSpawnRate(params.cSpawnRate), m_cParticles(0), m_particles(params.cParticles), m_blendType(params.blendType), m_bRespawn(params.bRespawn), m_pEmitter(emitter), 
            m_scaleSpeed(params.scaleSpeed), m_energyMin(params.energyMin), m_energyMax(params.energyMax), m_vDepth(params.cParticles), m_minScale(params.scaleMin), 
            m_maxScale(params.scaleMax), m_gravity(params.gravity), m_bDead(false), m_pInterpolator(&interpolator), m_pRenderer(&renderer), m_pCamera(nullptr)
        {
            Init();

			if(params.pFunctor)
				ApplyFunctor(*params.pFunctor);
        }

		ParticleEmitter::ParticleEmitter(const ParticleEmitter& emitter): m_cMaxParticles(emitter.m_cMaxParticles), m_cSpawnRate(emitter.m_cSpawnRate), 
			m_cParticles(0), m_particles(emitter.m_cMaxParticles), m_blendType(emitter.m_blendType), m_bRespawn(emitter.m_bRespawn), m_pEmitter(emitter.m_pEmitter), 
            m_scaleSpeed(emitter.m_scaleSpeed), m_energyMin(emitter.m_energyMin), m_energyMax(emitter.m_energyMax), m_vDepth(emitter.m_cMaxParticles), 
            m_minScale(emitter.m_minScale), m_maxScale(emitter.m_maxScale), m_gravity(emitter.m_gravity), m_bDead(false), m_pInterpolator(emitter.m_pInterpolator),
			m_pCamera(emitter.m_pCamera)
		{
            m_pRenderer = new Renderer(*emitter.m_pRenderer);

            Init();
		}

        ParticleEmitter::~ParticleEmitter(void)
        {
			delete m_pRenderer;
        }

        void ParticleEmitter::Init(void)
        {
	        //initialize particles
	        for(unsigned int i = 0; i < m_cMaxParticles; i++)
	        {
		        //set initial index in depth buffer
		        m_vDepth[i].index = i;
	        }     
        }

        bool ParticleEmitter::IsDead(void) const
        {
            return m_bDead;
        }

        void ParticleEmitter::ApplyFunctor(const math::IFunctor& functor)
        {
			gfx::TextureAccessorR32 accessor(*m_pInterpolator, false);

            for(unsigned int i = 0; i < 256; i++)
            {
                accessor.SetPixel(i, 0, min(1.0f, max(0.0f, functor.GetValue(i/256.0f))));
            }
        }

        void ParticleEmitter::Update(const gfx::Camera& camera, double dt)
        {	
			if(m_bDead)
				return;

			size_t oldParticles = (size_t)m_cParticles;

			if(!m_cSpawnRate)
                m_cParticles = m_cMaxParticles;
			else
			{
				m_cParticles += m_cSpawnRate * dt;
				m_cParticles = min(m_cParticles, m_cMaxParticles);
			}

			if(oldParticles != (size_t)m_cParticles)
			{
				for(size_t i = oldParticles; i < m_cParticles; i++)
				{
					SetParticleDefaults(i);
				}
			}

	        UpdatePhysicData(dt);

	        if(m_blendType != BlendType::ADD && m_blendType != BlendType::SUB)
		        SortParticles(camera.GetPosition());

			m_pCamera = &camera;
        }

        void ParticleEmitter::SortParticles(const math::Vector3& vCameraPos)
        {
			if(!m_cParticles)
				return;

			//iterate through particles for depth sorting
	        for(auto& depth : m_vDepth)
	        {
		        //get index of currect particle to keep temporal coherency
		        const unsigned int index = depth.index;
		        depth.depth = abs(m_particles.x[index] - vCameraPos.x);
		        depth.depth += abs(m_particles.y[index] - vCameraPos.y);
		        depth.depth += abs(m_particles.z[index] - vCameraPos.z);
	        }

	        std::sort(m_vDepth.begin(), m_vDepth.end(), ParticleSorter());
        }

        void ParticleEmitter::UpdatePhysicData(double dt)
        {
	        //initialize registers
	        __m128 energy, velocityX, velocityY, velocityZ;
	        __m128 time = _mm_set_ps1((float)dt);
	        const __m128 ZERO = _mm_setzero_ps();

			unsigned int remainder = (unsigned int)(m_cParticles) % 4;

            size_t count = 0;

            unsigned int i = 0;
	        //perform physics update on all particles
	        for(; i < (unsigned int)m_cParticles - remainder; i+=4)
	        {
                //load energy
		        energy = _mm_load_ps(&m_particles.e[i]);

                // check if all four particles are dead?
				if(_mm_movemask_ps(_mm_cmple_ps(energy, ZERO)) != 15)
				{
					//load position to SSE registers and add velocity
					velocityX = _mm_add_ps(_mm_load_ps(&m_particles.vx[i]), _mm_mul_ps(time, _mm_load_ps(&m_particles.ax[i])));
					velocityY = _mm_add_ps(_mm_load_ps(&m_particles.vy[i]), _mm_mul_ps(time, _mm_load_ps(&m_particles.ay[i])));
					velocityZ = _mm_add_ps(_mm_load_ps(&m_particles.vz[i]), _mm_mul_ps(time, _mm_load_ps(&m_particles.az[i])));
					_mm_store_ps(&m_particles.x[i], _mm_add_ps(_mm_load_ps(&m_particles.x[i]), _mm_mul_ps(velocityX, time)));
					_mm_store_ps(&m_particles.y[i], _mm_add_ps(_mm_load_ps(&m_particles.y[i]), _mm_mul_ps(velocityY, time)));
					_mm_store_ps(&m_particles.z[i], _mm_add_ps(_mm_load_ps(&m_particles.z[i]), _mm_mul_ps(velocityZ, time)));

					_mm_store_ps(&m_particles.vx[i], velocityX);
					_mm_store_ps(&m_particles.vy[i], velocityY);
					_mm_store_ps(&m_particles.vz[i], velocityZ);

					_mm_store_ps(&m_particles.s[i], _mm_add_ps(_mm_load_ps(&m_particles.s[i]), _mm_mul_ps(_mm_set1_ps(m_scaleSpeed), time)));

					//substract timestep from energy
					_mm_store_ps(&m_particles.e[i], _mm_sub_ps(energy, time));
				}

		        //check what particles are still alive
                if(m_bRespawn)
                {
		            switch ( _mm_movemask_ps( _mm_cmple_ps(energy, ZERO) ) )
                    {
                    case 0: // f f f f
                      // do nothing; all 4 particles are alive
                      break;
                    case 1: // f f f t
                      // particle [i] is dead
                      SetParticleDefaults(i);
                      break;
                    case 2: // f f t f
                      // particle [i+1] is dead
                      SetParticleDefaults(i+1);
                      break;
                    case 3: // f f t t
                      // particles [i] and [i+1] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+1);
                      break;
                    case 4: // f t f f
                      // particle [i+2] is dead
                      SetParticleDefaults(i+2);
                      break;
                    case 5: // f t f t
                      // particles [i] and [i+2] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+2);
                      break;
                    case 6: // f t t f
                      // particles [i+1] and [i+2] are dead
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+2);
                      break;
                    case 7: // f t t t
                      // particles [i] and [i+1] and [i+2] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+2);
                      break;
                    case 8: // t f f f
                      // particle [i+3] is dead
                      SetParticleDefaults(i+3);
                      break;
                    case 9: // t f f t
                      // particles [i] and [i+3] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+3);
                      break;
                    case 10: // t f t f
                      // particles [i+1] and [i+3] are dead
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+3);
                      break;
                    case 11: // t f t t
                      // particles [i] and [i+1] and [i+3] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+3);
                      break;
                    case 12: // t t f f
                      // particles [i+2] and [i+3] are dead
                      SetParticleDefaults(i+2);
                      SetParticleDefaults(i+3);
                      break;
                    case 13: // t t f t
                      // particles [i] and [i+2] and [i+3] are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+2);
                      SetParticleDefaults(i+3);
                      break;
                    case 14: // t t t f
                      // particles [i+1] and [i+2] and [i+3] are dead
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+2);
                      SetParticleDefaults(i+3);
                      break;
                    case 15: // t t t t
                      // all 4 particles are dead
                      SetParticleDefaults(i);
                      SetParticleDefaults(i+1);
                      SetParticleDefaults(i+2);
                      SetParticleDefaults(i+3);
                      break;
		            }
                }

                count++;
	        }

            // handle last 0 to 3 particles
			for(unsigned int j = i; j < (unsigned int)m_cParticles; j++)
			{
                if(m_particles.e[j] <= 0.0f)
                    continue;

                // handle speed by adding acceleration to velocity
				m_particles.vx[j] += (float)(m_particles.ax[j] * dt);
				m_particles.vy[j] += (float)(m_particles.ay[j] * dt);
				m_particles.vz[j] += (float)(m_particles.az[j] * dt);

                // move particle by adding velocity to position
				m_particles.x[j] += (float)(m_particles.vx[j] * dt);
				m_particles.y[j] += (float)(m_particles.vy[j] * dt);
				m_particles.z[j] += (float)(m_particles.vz[j] * dt);
				
                // scale
				m_particles.s[j] += (float)(m_scaleSpeed * dt);

                // decrement energy
				m_particles.e[j] -= (float)dt;

                count++;
			}

            // if no particles where updated, but there were particles spawned ->
            // mark emitter as dead
            if(!count && (int)m_cParticles)
               m_bDead = true;
        }

        void ParticleEmitter::SetParticleDefaults(unsigned int index)
        {
	        //reset particle to default parameters
	        //plus get new random from spawner
	        m_pEmitter->InitParticle(index, m_particles);
			m_particles.x[index] += m_vPosition.x;
			m_particles.y[index] += m_vPosition.y;
			m_particles.z[index] += m_vPosition.z;
            //acceleration
	        m_particles.ax[index] = 0.0f;
	        m_particles.ay[index] = -m_gravity;
	        m_particles.az[index] = 0.0f;
            //energy
            float energy = rnd::fRange(m_energyMin, m_energyMax);
            m_particles.max[index] = energy;
            m_particles.e[index] = energy;
            //rotation
            m_particles.s[index] = rnd::fRange(m_minScale, m_maxScale);
        }

		void ParticleEmitter::SetPosition(const math::Vector3& vPosition)
		{
			m_vPosition = vPosition;
		}

        void ParticleEmitter::Render(const render::IStage& stage)
        {
            // don't draw if emitter is dead
			if(m_bDead || !m_pCamera || !m_cParticles)
				return;

			m_pRenderer->UpdateParticleData((size_t)m_cParticles, *m_pCamera, m_vDepth, m_particles);  

	        m_pRenderer->Render(stage);
        }

        void ParticleEmitter::Reset(void)
        {
            // reset all particles
            for(unsigned int i = 0; i < m_cMaxParticles; i++)
            {
                SetParticleDefaults(i);

                // instant or timed spawn?
                if(m_cSpawnRate)
                    m_cParticles = 0;
                else
                    m_cParticles = m_cMaxParticles;
            }

            // unset dead flag
			m_bDead = false;
        }
    }
}