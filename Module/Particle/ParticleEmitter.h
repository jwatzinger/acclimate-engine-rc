/** @file */

#pragma once
#include <memory>
#include <vector>
#include "Particle.h"
#include "IEmitter.h"
#include "Enum.h"
#include "Depth.h"
#include "Math\Vector3.h"
#include "Math\IFunctor.h"

namespace acl
{

    namespace gfx
    {
        class ITexture;
        class InstancedMesh;
        class Camera;
    }

    namespace render
    {
        class IStage;
    }

	namespace particle
	{

		/// Parameters needed for the construction of a emitter
        /** The EmitterParams structure stores all parameters needed to initialize a single 
        *   emitter. There is no default values, all paramters have to be set in order for
        *   the emitter to function. */
        struct EmitterParams 
        {
            unsigned int cParticles;    ///< The maximum number of particles at any point in time.
            unsigned int cSpawnRate;    ///< The spawn rate per second (0 for instant spawn)
            bool bRespawn;              ///< Indicates whether the emitter should respawn
            float scaleMin;             ///< The minimum initial scale size of a particle
            float scaleMax;             ///< The maximum initial scale size on a particle
            float scaleSpeed;           ///< The speed by which particles scale
            float energyMin;            ///< The minimum intial energy with which a particle spawns
            float energyMax;            ///< The maximum intial energy with which a particle spawns
            //todo: replace with force field mechanic
            float gravity;              ///< The gravitational pull that accelerates the particles
			BlendType blendType;		///< The particles blend type
			std::shared_ptr<math::IFunctor> pFunctor;
        };

		class Renderer;

        /// Emits a certain kind of particles
        /** The ParticleEmitter class handles the spawning, physics, movement and rendering
        *   of a bunch of particles. Its paramters are set via the EmitterParams structure.
        *   It also needs an IEmitter implementation which decides the spawning conditions
        *   of each particle. An 1D interpolator texture as a function lookup table is passed
        *   for the interpolation, currently only between start and end color values.
        *   As specified in the params, the emitter will eigther continously respawn particles,
        *   or stop interaction after the last particle died.
        *   the emitter to function. */
		class ParticleEmitter final
        {
            typedef std::vector<ParticleDepth> DepthVector;
		public:

			ParticleEmitter(const EmitterParams& params, const std::shared_ptr<IEmitter>& emitter, gfx::ITexture& interpolator, Renderer& renderer);
			ParticleEmitter(const ParticleEmitter& emitter);
			~ParticleEmitter(void);

            /** Creates and adds a new spawner to the emitter. This method constructs 
             *  a new Spawner structure using the passed arguments  and attaches it
             *  to the emitter, replacing the old one.
             *
             *  @param[in] args The constructor arguments for the spawner being created.
             *
             *  @tparam SpawnerType Type of the spawner to construct.
			 */
			template<typename SpawnerType, typename... Args>
			void SetSpawner(Args&&... args);

            /**
             *  @param[in] vPosition Vector describing the target spawner position
			 */
			void SetPosition(const math::Vector3& vPosition);

            /**
             *  @return Indicates whether the emitter is dead
			 */
            bool IsDead(void) const;

            /** Applies an interpolation function. 
             *  This method writes the values from 0.0f to 1.0f of an IFunctor implementation
             *  to the interpolator texture used by this emitter. This works as a function lookup
             *  table, which allows even complex functions to be used for interpolation without 
             *  any performance penalties.
             *
             *  @param[in] functor Const reference to the functor being used for the interpolation function.
			 */
            void ApplyFunctor(const math::IFunctor& functor);

            /** Performs an emitter update.
             *  This method spawns new particles until the limit is reached, using the specified
             *  spawn rate, and then sorts all active particles based on their blend type. It also 
             *  class UpdatePhysicsData() and UpdateInstanceData(). Update does nothing if the emitter 
             *  is marked dead, therefore it has to manually be reset or destroyed once it reaches 
             *  this state.
             *
             *  @param[in] camera Const reference to the camera being used for particle sorting.
             *  @param[in] dt The delta timestep.
			 */
			void Update(const gfx::Camera& camera, double dt);

            /** Submits the particles render instance to a render stage.
             *  Doesn't submit anything if the emitter is marked dead.
             *
             *  @param[in] stage Const reference to the stage the particles are rendered at.
			 */
            void Render(const render::IStage& stage);

            /** Resets the emitters state.
             *  Marks emitter as alive, clears all active particles and starts respawning them.
             *  Does not reset interpolator and spawner to their original state, if they have
             *  been modified meanwhile. */
            void Reset(void);

		private:

            void Init(void);

			void SetParticleDefaults(unsigned int index);
			void SortParticles(const math::Vector3& vCameraPos);
			void UpdatePhysicData(double dt);

			bool m_bDead;
			BlendType m_blendType;
            bool m_bRespawn;
            float m_energyMax;
            float m_energyMin;
            float m_minScale;
            float m_maxScale;
            float m_scaleSpeed;
            float m_gravity;
			double m_cParticles;
            unsigned int m_cMaxParticles;
            unsigned int m_cSpawnRate;

			math::Vector3 m_vPosition;

            gfx::ITexture* m_pInterpolator;
			const gfx::Camera* m_pCamera;

			Particle m_particles;
			DepthVector m_vDepth;

			Renderer* m_pRenderer;
			std::shared_ptr<IEmitter> m_pEmitter;

		};

        template<typename SpawnerType, typename... Args>
		void SetSpawner(Args&&... args)
		{
			delete m_pSpawner;
			m_pSpawner = new SpawnerType(args);
		}
		
		/// Used for particle sorting
        /** Sorting structure used to allow the compiler to inline the sort function for
        *   faster sorting. Used for index sorting based on depth.*/
		struct ParticleSorter
		{
			/** Compares two ParticleDepth depth values.
			 *  
			 *	@param[in] left First particle to compare.
			 *	@param[in] right Second particle to compare.
			 */
			inline bool operator() (const ParticleDepth& left, const ParticleDepth& right)
			{
				return left.depth > right.depth;
			}
		};
	}
}
 