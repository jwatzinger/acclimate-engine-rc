#include "ParticleModule.h"
#include "ParticleSystem.h"
#include "ParticleRenderSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "RegisterScript.h"
#include "AxmLoader.h"
#include "ApiInclude.h"
#include "Core\BaseContext.h"
#include "Core\ResourceManager.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\ComponentFactory.h"
#include "Gfx\IResourceLoader.h"
#include "Render\Context.h"
#include "Render\ILoader.h"
#include "Script\Core.h"

#include "Gfx\IMeshLoader.h"

namespace acl
{
	namespace modules
	{

		ParticleModule::ParticleModule(void): m_pSystems(nullptr)
		{
		}

		void ParticleModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");

			if(getUsedAPI() != GraphicAPI::DX9)
			{
				gfx::IGeometry::AttributeVector vAttributes;
				vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 1); // position

				const gfx::IMeshLoader::VertexVector vVertices = { 0.0f };
				const gfx::IMeshLoader::IndexVector vIndices = { 0 };

				context.meshes.Custom(L"ParticlePoint", vAttributes, vVertices, vIndices, gfx::PrimitiveType::POINT);
			}
		}

		void ParticleModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void ParticleModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// resources
			auto& loader = *new particle::AxmLoader(context.gfx.load.textures, context.gfx.load.meshes, context.gfx.resources.models, context.gfx.load.materials);
			auto& set = context.core.resources.AddSet<unsigned int, particle::Effect>(L"Particle", L"Particle.axm", loader);

			// system
			m_pSystems->AddSystem<ecs::ParticleSystem>(set.GetResources());
			m_pSystems->AddSystem<ecs::ParticleRenderSystem>(context.render.renderer);

			// io
			ecs::Saver::RegisterSubRoutine<ecs::ParticleSaveRoutine>(L"Particle");
			ecs::Loader::RegisterSubRoutine<ecs::ParticleLoadRoutine>(L"Particle");

			// component
			ecs::ComponentRegistry::RegisterComponent<ecs::Particle>(L"Particle");
			context.ecs.componentFactory.Register<ecs::Particle, unsigned int, bool>();

			m_scriptGroup = context.script.core.GetConfigGroup("Particle");
			m_scriptGroup.Begin();
			particle::RegisterScript(context.script.core);
			m_scriptGroup.End();
		}

		void ParticleModule::OnUninit(const core::GameStateContext& context)
		{
			m_scriptGroup.Remove();

			// system
			m_pSystems->RemoveSystem<ecs::ParticleSystem>();
			m_pSystems->RemoveSystem<ecs::ParticleRenderSystem>();

			// component
			ecs::ComponentRegistry::UnregisterComponent(L"Particle");
			context.ecs.componentFactory.Unregister<ecs::Particle>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Particle");
			ecs::Loader::UnregisterSubRoutine(L"Particle");

			m_pSystems = nullptr;
		}

		void ParticleModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::ParticleSystem>(dt);
		}

		void ParticleModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::ParticleRenderSystem>(0.0f);
		}

	}
}
