#include "ParticleRenderSystem.h"
#include "Components.h"
#include "Effect.h"
#include "ApiInclude.h"
#include "Entity\Entity.h"
#include "Entity\Message.h"
#include "Entity\MessageRegistry.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Gfx\Camera.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

namespace acl
{
    namespace ecs
    {

        ParticleRenderSystem::ParticleRenderSystem(render::IRenderer& renderer): m_pStage(renderer.GetStage(L"particles"))
        {
        }

		void ParticleRenderSystem::Init(MessageManager& messageManager)
        {
			messageManager.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
        }

        void ParticleRenderSystem::Update(double dt)
        {
	        //get entities with particle component
	        auto vEntities = m_pEntities->EntitiesWithComponents<Particle>();

	        //loop through those entities to update particle
	        for(auto& entity: vEntities)
	        {
		        const Particle* pParticle = entity->GetComponent<Particle>();
				
				if(pParticle->pEffect)
					pParticle->pEffect->Render(*m_pStage);
	        }
        }

		void ParticleRenderSystem::ReceiveMessage(const BaseMessage& message)
        {
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				auto pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
				m_pStage->SetVertexConstant(0, (float*)&pCamera->GetViewProjectionMatrix(), 4);
				if(getUsedAPI() != GraphicAPI::DX9)
				{
					m_pStage->SetGeometryConstant(0, (float*)&pCamera->GetViewProjectionMatrix(), 4);
					m_pStage->SetGeometryConstant(4, (float*)&pCamera->GetPosition(), 1);
					m_pStage->SetGeometryConstant(5, (float*)&pCamera->GetUp(), 1);
				}
			} 
        }

    }
}