#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace render
	{
		class IRenderer;
		class IStage;
	}

    namespace ecs
    {

        class ParticleRenderSystem :
	        public System<ParticleRenderSystem>
        {
        public:

	        ParticleRenderSystem(render::IRenderer& renderer);

			void Init(MessageManager& messageManager);
	        void Update(double dt);
			void ReceiveMessage(const BaseMessage& message);

        private:

	        render::IStage* m_pStage;
        };

    }
}
