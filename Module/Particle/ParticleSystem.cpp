#include "ParticleSystem.h"
#include "Components.h"
#include "Effect.h"
#include "Entity\Entity.h"
#include "Entity\Message.h"
#include "Entity\MessageRegistry.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\ComponentRegistry.h"

namespace acl
{
    namespace ecs
    {

		ParticleSystem::ParticleSystem(const particle::Particles& particles) :
			m_pCamera(nullptr), m_pParticles(&particles)
        {
        }

        void ParticleSystem::Init(MessageManager& messageManager)
        {
            messageManager.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
        }

        void ParticleSystem::Update(double dt)
        {
			if(!m_pCamera)
				return;

	        //get entities with particle component
	        auto vEntities = m_pEntities->EntitiesWithComponents(Particle::family(), ecs::ComponentRegistry::GetComponentId(L"Position"));

	        //loop through those entities to update particle
	        for(auto& entity: vEntities)
	        {
				Particle* pParticles = entity->GetComponent<Particle>();

		        if(!pParticles->pEffect)
					pParticles->pEffect = new particle::Effect(*m_pParticles->Get(pParticles->id));

		        particle::Effect* pEffect = pParticles->pEffect;

				pEffect->SetPosition(*entity->GetComponent(ecs::ComponentRegistry::GetComponentId(L"Position"))->GetAttribute<math::Vector3>(L"Vector"));

		        pEffect->Update(*m_pCamera, dt);

                if(pParticles->bRemoveOnFinish && pEffect->IsDone())
					m_pEntities->RemoveEntity(entity);

	        }
        }

        void ParticleSystem::ReceiveMessage(const BaseMessage& message)
        {
            if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
		        m_pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
        }

    }
}
