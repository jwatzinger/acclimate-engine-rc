#pragma once
#include "Particles.h"
#include "Entity\System.h"

namespace acl
{
	namespace gfx
	{
		class Camera;
		class IMeshLoader;
	}

    namespace ecs
    {

        class ParticleSystem :
	        public System<ParticleSystem>
        {
        public:

	        ParticleSystem(const particle::Particles& particles);

            void Init(MessageManager& messageManager);
	        void Update(double dt);
            void ReceiveMessage(const BaseMessage& message);

        private:

	        const gfx::Camera* m_pCamera;

	        const particle::Particles* m_pParticles;
        };

    }
}
