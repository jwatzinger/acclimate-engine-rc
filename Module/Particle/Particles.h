#pragma once
#include "Core\Resources.h"
#include "Effect.h"

namespace acl
{
    namespace particle
    {

        typedef core::Resources<unsigned int, Effect> Particles;

    }
}