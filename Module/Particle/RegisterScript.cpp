#include "RegisterScript.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Script\Core.h"

namespace acl
{
	namespace particle
	{
		/*******************************************
		* Particle component
		*******************************************/

		ecs::Particle* AttachParticle(ecs::EntityHandle* entity, unsigned int id, bool bRemove)
		{
			return (*entity)->AttachComponent<ecs::Particle>(id, bRemove);
		}

		void RemoveParticle(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Particle>();
		}

		ecs::Particle* GetParticle(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Particle>();
		}

		void RegisterScript(script::Core& script)
		{
			// particle component
			auto particle = script.RegisterReferenceType("Particle", asOBJ_NOCOUNT);
			particle.RegisterProperty("uint id", asOFFSET(ecs::Particle, id));
			particle.RegisterProperty("bool remove", asOFFSET(ecs::Particle, bRemoveOnFinish));

			script.RegisterGlobalFunction("Particle@ AttachParticle(Entity& in, uint id, bool remove)", asFUNCTION(AttachParticle));
			script.RegisterGlobalFunction("void RemoveParticle(Entity& in)", asFUNCTION(RemoveParticle));
			script.RegisterGlobalFunction("Particle@ GetParticle(const Entity& in)", asFUNCTION(GetParticle));
		}

	}
}
