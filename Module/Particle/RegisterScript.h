#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace particle
	{

		void RegisterScript(script::Core& script);

	}
}
