#include "Renderer.h"

#include "Particle.h"
#include "ApiInclude.h"
#include "Gfx\IModel.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\Camera.h"
#include "Gfx\InstanceBufferAccessor.h"
#include "Math\Vector4.h"

namespace acl
{
	namespace particle
	{

		struct INSTANCEDATA
		{
			math::Vector3 r1;
			math::Vector3 r2;
			math::Vector3 r3;
			math::Vector3 r4;
			float time;
		};

		struct INSTANCEDATA_GEOMETRY
		{
			math::Vector4 vTranslation;
			float time;
		};

		Renderer::Renderer(gfx::ModelInstance& instance, gfx::IInstancedMesh& instanced, particle::BlendType blend, particle::BillboardType billboard, gfx::FColor& startColor, gfx::FColor& endColor): 
			m_blendType(blend), m_billType(billboard), m_pParticleMesh(&instanced), m_startColor(startColor), m_endColor(endColor), m_pInstance(&instance)
		{
            InitModel();
		}

        Renderer::Renderer(const Renderer& renderer): m_blendType(renderer.m_blendType), m_startColor(renderer.m_startColor), 
			m_endColor(renderer.m_endColor), m_billType(renderer.m_billType)
        {
			m_pInstance = &renderer.m_pInstance->Clone();
			
            m_pParticleMesh = &renderer.m_pParticleMesh->Clone();

            InitModel();
        }

        Renderer::~Renderer(void)
        {
			delete m_pInstance;
			delete m_pParticleMesh;
        }

		void Renderer::Render(const render::IStage& stage)
		{
			m_pInstance->Draw(stage, 0);
		}

        void Renderer::UpdateParticleData(size_t cParticles, const gfx::Camera& camera, const DepthVector& vDepth, const particle::Particle& particles) const
        {
	        //set up vector to choose type of billboard
	        //standard:: y axis aligned
	        //todo: allow another axis

			size_t cActiveParticles = 0;
			
			if(getUsedAPI() != GraphicAPI::DX9)
			{
				gfx::InstanceBufferAccessor<INSTANCEDATA_GEOMETRY> accessor(*m_pParticleMesh);
				INSTANCEDATA_GEOMETRY* pBuffer = accessor.GetData();

				for(auto& depth : vDepth)
				{
					//get index
					const unsigned int index = depth.index;

					if(particles.e[index] <= 0.0f)
						continue;

					//set local transform matrix
					pBuffer->vTranslation.x = particles.x[index];
					pBuffer->vTranslation.y = particles.y[index];
					pBuffer->vTranslation.z = particles.z[index];
					pBuffer->vTranslation.w = particles.s[index];

					pBuffer->time = 1.0f - particles.e[index] / particles.max[index];

					//get next buffer element
					pBuffer++;
					cActiveParticles++;
				}
			}
			else
			{
				gfx::InstanceBufferAccessor<INSTANCEDATA> accessor(*m_pParticleMesh);
				INSTANCEDATA* pBuffer = accessor.GetData();

				math::Vector3 vUp(0.0f, 0.0f, 0.0f);
				math::Vector3 vDir(0.0f, 0.0f, 0.0f);
				switch(m_billType)
				{
				case particle::BillboardType::AXIS:
					vDir = camera.GetDirection();
					vDir.y = 0.0f;
					vUp.y = 1.0f;
					break;
				case particle::BillboardType::SCREEN:
					vDir = camera.GetDirection();
					vUp = camera.GetUp();
					break;
				case particle::BillboardType::FLOOR:
					vDir.y = 1.0f;
					vUp.x = 1.0f;
					break;
				case particle::BillboardType::DIRECTION:
					vDir = camera.GetDirection();
					break;
				}

				//calculate right vector
				math::Vector3 vRight(0.0f, 0.0f, 0.0f);
				vRight = vDir.Cross(vUp);
				vRight.Normalize();

				if(m_billType == particle::BillboardType::SCREEN)
					vUp = vDir.Cross(vRight);

				for(auto& depth : vDepth)
				{
					//get index
					const unsigned int index = depth.index;

					if(particles.e[index] <= 0.0f)
						continue;

					if(m_billType == particle::BillboardType::DIRECTION)
					{
					    vRight.x = -particles.vx[index];
					    vRight.y = -particles.vy[index];
					    vRight.z = -particles.vz[index];
					    vRight.Normalize();

					    vUp = vRight.Cross(vDir);
					    vUp.Normalize();

					    vRight *= 5.0f;
					}

					//set local transform matrix
					const float scale = particles.s[index];
					pBuffer->r1 = vDir;
					pBuffer->r2 = vUp * scale;
					pBuffer->r3 = vRight * scale;

					//set translation
					pBuffer->r4.x = particles.x[index];
					pBuffer->r4.y = particles.y[index];
					pBuffer->r4.z = particles.z[index];

					pBuffer->time = 1.0f - particles.e[index] / particles.max[index];

					//get next buffer element
					pBuffer++;
					cActiveParticles++;
				}
			}

	        m_pParticleMesh->SetInstanceCount(cActiveParticles);
        }

        void Renderer::InitModel(void)
        {
			m_pInstance->GetParent()->SetMesh(*m_pParticleMesh);
            //color
			const gfx::FColor dist = m_endColor - m_startColor;
			float color[2][4] = { 
				{ m_startColor.r, m_startColor.g, m_startColor.b, m_startColor.a },
				{ dist.r, dist.g, dist.b, dist.a }
			};

			m_pInstance->SetPixelConstant(0, (float*)color, 2);
        }

	}
}