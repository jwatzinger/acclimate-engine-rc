#pragma once
#include <vector>
#include "Enum.h"
#include "Depth.h"
#include "Gfx\Color.h"

namespace acl
{
	namespace gfx
	{
		class IInstancedMesh;
		class ModelInstance;
        class Camera;
	}

    namespace math
    {
        struct Vector3;
    }

	namespace render
	{
		class IStage;
	}

	namespace particle
	{
        struct Particle;

		/// Instanced particle renderer
        /** Implements the IRenderer interface. 
        *   It should at least set up an renderable instance and a bunch of state groups
		*	(though for standard use cases it might be sufficient to use the Model class). */
		class Renderer
		{
            typedef std::vector<ParticleDepth> DepthVector;

		public:
			Renderer(gfx::ModelInstance& instance, gfx::IInstancedMesh& instanced, particle::BlendType blend, particle::BillboardType billboard, gfx::FColor& startColor, gfx::FColor& endColor);
            Renderer(const Renderer& renderer);
            ~Renderer(void);
            
            /** Sets all particles to render. This method applies the set billboard type
             *  to the passed set of particles, and sets all alive particles in the pre-
             *  sorted order as instance data.
             *
             *  @param[in] cParticles Count of particles alive
			 *  @param[in] camera Const reference to the camera used for billboard effects.
             *  @param[in] vDepth Vector of particle indicies, probably sorted by depth
             *  @param[in] particles The particle group whose data is being used
			 */
            void UpdateParticleData(size_t cParticles, const gfx::Camera& camera, const DepthVector& vDepth, const particle::Particle& particles) const;

			/** Submits the particles render instance to a render stage.
             *
             *  @param[in] stage Const reference to the stage the particles are rendered at.
			 */
            void Render(const render::IStage& stage);

        private:

            void InitModel(void);

			gfx::IInstancedMesh* m_pParticleMesh;
			gfx::ModelInstance* m_pInstance;

			particle::BlendType m_blendType;
            particle::BillboardType m_billType;

			gfx::FColor m_startColor, m_endColor;
		};

	}
}