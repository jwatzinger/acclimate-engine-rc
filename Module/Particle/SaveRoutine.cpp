#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void ParticleSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Particle* pParticle = entity.GetComponent<Particle>())
			{
				auto& component = entityNode.InsertNode(L"Particle");
				component.ModifyAttribute(L"id", conv::ToString(pParticle->id));
                component.ModifyAttribute(L"remove", conv::ToString(pParticle->bRemoveOnFinish));
			}
		}

	}
}
