#include "ClothMesh.h"
#include "Gfx\IMesh.h"
#include "Gfx\VertexBufferAccessor.h"
#include "Math\Vector2f.h"
#include "Math\Vector3.h"
#include "Physics\SoftBody.h"

namespace acl
{
	namespace physics3d
	{

		struct ClothVertex
		{
			float x, y, z;
			float u, v;
			float nx, ny, nz;
		};

		struct RopeVertex
		{
			float x, y, z;
		};

		ClothMesh::ClothMesh(const physics::SoftBody& body, gfx::IMesh& mesh):
			m_pBody(&body), m_pMesh(&mesh)
		{
		}

		ClothMesh::~ClothMesh()
		{
		}

		void ClothMesh::Update(void)
		{
			const auto& particles = m_pBody->GetParticles();
			const auto& vSize = m_pBody->GetSize();

			// clothing
			if(vSize.x > 1 && vSize.y > 1)
			{
				const math::Vector2f vTexcoord(1.0f / vSize.x, 1.0f / vSize.y);

				gfx::VertexBufferAccessor<ClothVertex> accessor(*m_pMesh);

				auto pData = accessor.GetData();
				for(int i = 0; i < vSize.x; i++)
				{
					for(int j = 0; j < vSize.y; j++)
					{
						const unsigned int index = j * vSize.x + i;
						pData[index].x = particles.x[index];
						pData[index].y = particles.y[index];
						pData[index].z = particles.z[index];
						pData[index].u = i * vTexcoord.x;
						pData[index].v = j * vTexcoord.y;
						const math::Vector3& vNormal = m_pBody->GetNormal(i, j);
						pData[index].nx = vNormal.x;
						pData[index].ny = vNormal.y;
						pData[index].nz = vNormal.z;
					}
				}
			}
			// rope
			else
			{
				gfx::VertexBufferAccessor<RopeVertex> accessor(*m_pMesh);

				auto pData = accessor.GetData();
				for(int i = 0; i < vSize.x; i++)
				{
					for(int j = 0; j < vSize.y; j++)
					{
						const unsigned int index = j * vSize.x + i;
						pData[index].x = particles.x[index];
						pData[index].y = particles.y[index];
						pData[index].z = particles.z[index];
					}
				}
			}

		}

	}
}

