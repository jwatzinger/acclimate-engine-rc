#pragma once

namespace acl
{
	namespace gfx
	{
		class IMesh;
	}

	namespace physics
	{
		class SoftBody;
	}

	namespace physics3d
	{

		class ClothMesh
		{
		public:
			ClothMesh(const physics::SoftBody& body, gfx::IMesh& mesh);
			~ClothMesh();

			void Update(void);

		private:

			const physics::SoftBody* m_pBody;

			gfx::IMesh* m_pMesh;
		};

	}
}



