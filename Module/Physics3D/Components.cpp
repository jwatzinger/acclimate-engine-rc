#include "Components.h"
#include "Gfx\ModelInstance.h"
#include "System\Assert.h"
#include "Physics\Material.h"
#include "Physics\World.h"
#include "ClothMesh.h"

namespace acl
{
	namespace physics3d
	{

		Body::Body(const std::wstring& stMaterial, const std::wstring& stShape, int motion, int collision, float damping) : stMaterial(stMaterial), stShape(stShape),
			pBody(nullptr), pWorld(nullptr), pDebugModel(nullptr), motion(motion), collision(collision), damping(damping)
		{
		}

		Body::~Body(void)
		{
			delete pDebugModel;

			if(pBody)
			{
				ACL_ASSERT(pWorld);
				pWorld->RemoveRigidBody(*pBody);
			}
		}

		void Body::OnCollision(const physics::RigidBody& body)
		{
			SigHandleCollision(*this, body);
		}

		SoftBody::SoftBody(float mass, float damping, unsigned int width, unsigned int height, float sizeX, float sizeY, const math::Vector3& vDir) : mass(mass), damping(damping),
			width(width), height(height), pBody(nullptr), pWorld(nullptr), pMesh(nullptr), sizeX(sizeX), sizeY(sizeY), vDir(vDir)
		{
		}

		SoftBody::~SoftBody(void)
		{
			if(pBody)
			{
				ACL_ASSERT(pWorld);
				pWorld->RemoveSoftBody(*pBody);
			}

			delete pMesh;
		}

	}
}