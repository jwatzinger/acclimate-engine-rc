#pragma once
#include "Core\Signal.h"
#include "Entity\Component.h"
#include "Entity\Entity.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
	}

	namespace physics
	{
		struct Material;
		class CollisionShape;
		class RigidBody;
		class SoftBody;
		class World;
	}

	namespace physics3d
	{

		struct Body :
			public ecs::Component<Body>
		{
			Body(const std::wstring& stMaterial, const std::wstring& stShape, int motion, int collision, float damping);
			~Body(void);

			std::wstring stMaterial, stShape;
			physics::RigidBody* pBody;
			physics::World* pWorld;

			gfx::ModelInstance* pDebugModel;
			int motion;
			int collision;
			float damping;

			void OnCollision(const physics::RigidBody& body);

			core::Signal<Body&, const physics::RigidBody&> SigHandleCollision;
			core::Signal<ecs::EntityHandle&> SigCollision;
		};

		class ClothMesh;

		struct SoftBody :
			public ecs::Component<SoftBody>
		{
			SoftBody(float mass, float damping, unsigned int width, unsigned int height, float sizeX, float sizeY, const math::Vector3& vDir);
			~SoftBody(void);

			float mass, damping;
			unsigned int width, height;
			float sizeX, sizeY;
			physics::SoftBody* pBody;
			physics::World* pWorld;
			ClothMesh* pMesh;
			math::Vector3 vDir;
		};

	}
}