#include "DebugDraw.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\IMesh.h"
#include "Math\Utility.h"

namespace acl
{
	namespace physics3d
	{

		void generateDebugCube(const gfx::IMeshLoader& loader)
		{
			gfx::IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 3);

			const gfx::IMeshLoader::VertexVector vVertices =
			{
				1.0f, 1.0f, 1.0f,
				-1.0f, 1.0f, 1.0f,
				-1.0f, 1.0f, -1.0f,
				1.0f, 1.0f, -1.0f,
				1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f, -1.0f,
				-1.0f, -1.0f, 1.0f,
				1.0f, -1.0f, 1.0f
			};

			const gfx::IMeshLoader::IndexVector vIndices =
			{
				0, 1, 1, 2, 2, 3, 3, 0, 0, 7, 7, 6, 6, 5, 5, 4, 4, 7, 6, 1, 5, 2, 4, 3
			};

			loader.Custom(L"RigidbodyDebugCube", vAttributes, vVertices, vIndices, gfx::PrimitiveType::LINE);
		}

		struct QuarterCircle
		{
			QuarterCircle(float x, float y) : x(x), y(y)
			{
			}

			float x, y;
		};

		void generateDebugSphere(const gfx::IMeshLoader& loader)
		{
			gfx::IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 3);

			/********************************
			* Create quater circle
			********************************/
			const unsigned int numSubsets = 45;

			std::vector<QuarterCircle> vCirclePoints;
			vCirclePoints.reserve(numSubsets);

			const float conversion = 90 / numSubsets;

			// create points on cirle segment
			static const float quaterAngle = math::degToRad(90);
			for(unsigned int i = 0; i < numSubsets; i++)
			{
				const float angle = math::degToRad(i*conversion);
				vCirclePoints.emplace_back(sin(angle), sin(quaterAngle - angle));
			}

			const unsigned int numPerCircle = 4;
			const unsigned int numCircles = 3;

			gfx::IMeshLoader::VertexVector vVertices;
			vVertices.reserve(numSubsets * numPerCircle * numCircles * 3);

			/*********************************
			* First circle
			*********************************/

			// first half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(point.x);
				vVertices.push_back(point.y);
				vVertices.push_back(0);
			}

			// second half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(point.y);
				vVertices.push_back(-point.x);
				vVertices.push_back(0);
			}

			// third half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(-point.x);
				vVertices.push_back(-point.y);
				vVertices.push_back(0);
			}

			// fourth half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(-point.y);
				vVertices.push_back(point.x);
				vVertices.push_back(0);
			}

			/*********************************
			* Second circle
			*********************************/

			// first half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(0);
				vVertices.push_back(point.y);
				vVertices.push_back(point.x);
			}

			// second half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(0);
				vVertices.push_back(-point.x);
				vVertices.push_back(point.y);
			}

			// third half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(0);
				vVertices.push_back(-point.y);
				vVertices.push_back(-point.x);
			}

			// fourth half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(0);
				vVertices.push_back(point.x);
				vVertices.push_back(-point.y);
			}

			/*********************************
			* Third circle
			*********************************/

			// first half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(point.y);
				vVertices.push_back(0);
				vVertices.push_back(point.x);
			}

			// second half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(-point.x);
				vVertices.push_back(0);
				vVertices.push_back(point.y);
			}

			// third half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(-point.y);
				vVertices.push_back(0);
				vVertices.push_back(-point.x);
			}

			// fourth half
			for(auto& point : vCirclePoints)
			{
				vVertices.push_back(point.x);
				vVertices.push_back(0);
				vVertices.push_back(-point.y);
			}

			gfx::IMeshLoader::IndexVector vIndices;

			for(unsigned int j = 0; j < numCircles; j++)
			{
				const unsigned int offset = j * numSubsets * numPerCircle;

				for(unsigned int i = 0; i < numSubsets * numPerCircle; i++)
				{
					vIndices.push_back(offset + i);
					vIndices.push_back(offset + i + 1);
				}

				// circle should close
				*vIndices.rbegin() = offset;
			}

			loader.Custom(L"RigidbodyDebugSphere", vAttributes, vVertices, vIndices, gfx::PrimitiveType::LINE);
		}

		void generateDebugCapsule(const gfx::IMeshLoader& loader)
		{
			gfx::IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 3);

			/********************************
			* Create quater circle
			********************************/
			const unsigned int numSubsets = 45;

			std::vector<QuarterCircle> vCirclePoints;
			vCirclePoints.reserve(numSubsets);

			const float conversion = 90 / numSubsets;

			// create points on cirle segment
			static const float quaterAngle = math::degToRad(90);
			for (unsigned int i = 0; i < numSubsets; i++)
			{
				const float angle = math::degToRad(i*conversion);
				vCirclePoints.emplace_back(sin(angle), sin(quaterAngle - angle));
			}

			const unsigned int numPerCircle = 4;
			const unsigned int numCircles = 5;

			gfx::IMeshLoader::VertexVector vVertices;
			vVertices.reserve(numSubsets * numPerCircle * numCircles * 3);

			/*********************************
			* Circles
			*********************************/

			for (float i = 1.0f; i >= -1.0f; i -= 0.5f)
			{
				for (auto& point : vCirclePoints)
				{
					vVertices.push_back(point.y);
					vVertices.push_back(i);
					vVertices.push_back(point.x);
				}

				// second half
				for (auto& point : vCirclePoints)
				{
					vVertices.push_back(-point.x);
					vVertices.push_back(i);
					vVertices.push_back(point.y);
				}

				// third half
				for (auto& point : vCirclePoints)
				{
					vVertices.push_back(-point.y);
					vVertices.push_back(i);
					vVertices.push_back(-point.x);
				}

				// fourth half
				for (auto& point : vCirclePoints)
				{
					vVertices.push_back(point.x);
					vVertices.push_back(i);
					vVertices.push_back(-point.y);
				}
			}

			/*********************************
			* Lines
			*********************************/

			const float pointsX[] = { -1.0f, 0.0f, 1.0f, 0.0f };
			const float pointsZ[] = { 0.0f, -1.0f, 0.0f, 1.0f };

			for(unsigned int j = 0; j < 4; j++)
			{
				for(float y = -1.0f; y <= 1; y+=2.0f)
				{
					vVertices.push_back(pointsX[j]);
					vVertices.push_back(y);
					vVertices.push_back(pointsZ[j]);
				}
			}

			gfx::IMeshLoader::IndexVector vIndices;

			for (unsigned int j = 0; j < numCircles; j++)
			{
				const unsigned int offset = j * numSubsets * numPerCircle;

				for (unsigned int i = 0; i < numSubsets * numPerCircle; i++)
				{
					vIndices.push_back(offset + i);
					vIndices.push_back(offset + i + 1);
				}

				// circle should close
				*vIndices.rbegin() = offset;
			}

			const unsigned int offset = numCircles * numSubsets * numPerCircle;
			for(unsigned int j = 0; j < 4; j++)
			{
				const auto index = offset + j*2;
				vIndices.push_back(index);
				vIndices.push_back(index + 1);
			}

			loader.Custom(L"RigidbodyDebugCapsule", vAttributes, vVertices, vIndices, gfx::PrimitiveType::LINE);
		}

		void generateDebugMeshes(const gfx::IMeshLoader& loader)
		{
			generateDebugCube(loader);
			generateDebugSphere(loader);
			generateDebugCapsule(loader);
		}

	}
}