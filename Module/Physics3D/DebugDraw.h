#pragma once

namespace acl
{
	namespace gfx
	{
		class IMeshLoader;
	}

	namespace physics3d
	{

		void generateDebugMeshes(const gfx::IMeshLoader& loader);

	}
}