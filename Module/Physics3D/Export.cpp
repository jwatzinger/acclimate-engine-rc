#include "Export.h"
#include "Module.h"

core::IModule& CreateModule(void)
{
	return *new physics3d::Module();
}