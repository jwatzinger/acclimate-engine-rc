#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"
#include "Physics\RigidBody.h"
#include "System\Log.h"

namespace acl
{
	namespace physics3d
	{

		void LoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pBody = entityNode.FirstNode(L"Body"))
			{
				physics::CollisionShape* pShape = nullptr;

				int collision = 0;
				if (auto pCollision = pBody->Attribute(L"collision"))
				{
					collision = pCollision->AsInt();
					if ((collision >= (int)physics::CollisionType::SIZE) || (collision < 0))
					{
						collision = 0;
						sys::log->Out(sys::LogModule::PLUGIN, sys::LogType::WARNING, "Invalid collision type for rigid body:", collision);
					}
				}

				int motion = 0;
				if(auto pMotion = pBody->Attribute(L"motion"))
				{
					motion = pMotion->AsInt();
					if((motion >= (int)physics::MotionType::SIZE) || (motion < 0))
					{
						motion = 0;
						sys::log->Out(sys::LogModule::PLUGIN, sys::LogType::WARNING, "Invalid motion type for rigid body:", motion);
					}			
				}

				float damping = 0;
				if(auto pDamping = pBody->Attribute(L"damping"))
				{
					damping = pDamping->AsFloat();
					if(damping < 0.0f || damping > 1.0f)
					{
						damping = 0.0f;
						sys::log->Out(sys::LogModule::PLUGIN, sys::LogType::WARNING, "Invalid damping for rigid body:", damping, "(must be between 0.0f and 1.0f");
					}
				}
				entity.AttachComponent<Body>(pBody->Attribute(L"material")->GetValue(), pBody->Attribute(L"shape")->GetValue(), motion, collision, damping);
			}

			if(const xml::Node* pSoftBody = entityNode.FirstNode(L"SoftBody"))
			{
				math::Vector3 vDir = math::Vector3(1.0f, 0.0f, 0.0f);

				entity.AttachComponent<SoftBody>(pSoftBody->Attribute(L"mass")->AsFloat(), 
												pSoftBody->Attribute(L"damping")->AsFloat(),
												pSoftBody->Attribute(L"width")->AsInt(),
												pSoftBody->Attribute(L"height")->AsInt(),
												pSoftBody->Attribute(L"sizeX")->AsFloat(),
												pSoftBody->Attribute(L"sizeY")->AsFloat(),
												vDir);
			}
		}

	}
}
