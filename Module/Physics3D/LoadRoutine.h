#pragma once
#include "Entity\ISubLoadRoutine.h"

namespace acl
{
	namespace physics3d
	{

		class LoadRoutine : 
			public ecs::ISubLoadRoutine
		{
		public:
			
			void Execute(ecs::Entity& entity, const xml::Node& entityNode) const override;
		};

	}
}

