#include "Module.h"
#include "System.h"
#include "RenderSystem.h"
#include "Components.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "RegisterScript.h"
#include "DebugDraw.h"
#include "Core\BaseContext.h"
#include "Entity\Loader.h"
#include "Entity\Saver.h"
#include "Entity\SystemManager.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\ComponentFactory.h"
#include "Gfx\IResourceLoader.h"
#include "Render\IRenderer.h"
#include "Render\ILoader.h"

namespace acl
{
	namespace physics3d
	{

		Module::Module(void) : m_pSystems(nullptr)
		{
		}

		void Module::OnLoadResources(const gfx::LoadContext& context)
		{
			generateDebugMeshes(context.meshes);

			context.pLoader->Load(L"Resources.axm");
		}

		void Module::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			m_pSystems->AddSystem<System>(context.physics);
			m_pSystems->AddSystem<RenderSystem>(context.gfx.load.meshes, context.gfx.resources.models, *context.render.renderer.GetStage(L"PhysicsDebug"));

			// io
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Physics");
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Physics");

			// component
			ecs::ComponentRegistry::RegisterComponent<Body>(L"Body");
			context.ecs.componentFactory.Register<Body, const wchar_t*, const wchar_t*, int, int, float>();
			ecs::ComponentRegistry::RegisterComponent<SoftBody>(L"SoftBody");
			context.ecs.componentFactory.Register<SoftBody, float, float, unsigned int, unsigned int, float, float, const math::Vector3&>();

			registerScript(context.script.core);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// render

			ecs::Saver::UnregisterSubRoutine(L"Physics");
			ecs::Loader::UnregisterSubRoutine(L"Physics");

			m_pSystems->RemoveSystem<System>();
			m_pSystems->RemoveSystem<RenderSystem>();

			ecs::ComponentRegistry::UnregisterComponent(L"Body");
			context.ecs.componentFactory.Unregister<Body>();
			ecs::ComponentRegistry::UnregisterComponent(L"SoftBody");
			context.ecs.componentFactory.Unregister<SoftBody>();

			m_pSystems = nullptr;
		}

		void Module::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<System>(dt);
			m_pSystems->UpdateSystem<RenderSystem>(dt);
		}

	}
}
