#pragma once
#include "Core\IModule.h"
#include "Script\ConfigGroup.h"

namespace acl
{
	namespace ecs
	{
		class SystemManager;
	}

	namespace physics3d
	{

		class Module final :
			public core::IModule
		{
		public:

			Module(void);

			void OnLoadResources(const gfx::LoadContext& context) override;
			void OnLoadRender(const render::Context& context) override;
			void OnInit(const core::GameStateContext& context) override;
			void OnUninit(const core::GameStateContext& context) override;
			void OnUpdate(double dt) override;
			void OnRender(void) const override {};

		private:

			ecs::SystemManager* m_pSystems;
			script::ConfigGroup m_scriptGroup;

		};

	}
}

