#include "RegisterScript.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Script\Core.h"

namespace acl
{
	namespace math
	{
		struct Vector3;
	}

	namespace physics3d
	{
		namespace detail
		{
			script::Core* _pScript = nullptr;
		}

		/*******************************************
		* Body component
		*******************************************/

		Body* AttachBody(ecs::EntityHandle* entity, const std::wstring& stMaterial, const std::wstring& stShape, int motiontype, int collisiontype, float damping)
		{
			return (*entity)->AttachComponent<Body>(stMaterial, stShape, motiontype, collisiontype, damping);
		}

		void RemoveBody(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<Body>();
		}

		Body* GetBody(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<Body>();
		}

		void connectOnCollision(Body* body, asIScriptFunction* pFunction)
		{
			auto pFunc = new script::Function(*pFunction, detail::_pScript->GetContext());
			body->SigCollision.Connect(pFunc, &script::Function::Call<void, ecs::EntityHandle&>);
		}

		/*******************************************
		* SoftBody component
		*******************************************/

		SoftBody* AttachSoftBody(ecs::EntityHandle* entity, float mass, float damping, unsigned int width, unsigned int height, float sizeX, float sizeY, const math::Vector3& vDir)
		{
			return (*entity)->AttachComponent<SoftBody>(mass, damping, width, height, sizeX, sizeY, vDir);
		}

		void RemoveSoftBody(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<SoftBody>();
		}

		SoftBody* GetSoftBody(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<SoftBody>();
		}

		void registerScript(script::Core& script)
		{
			detail::_pScript = &script;

			// body component
			auto body = script.RegisterReferenceType("Body", asOBJ_NOCOUNT);
			body.RegisterProperty("RigidBody@ body", asOFFSET(Body, pBody));

			script.RegisterGlobalFunction("Body@ AttachBody(Entity& in, const string& in, const string& in, int, float)", asFUNCTION(AttachBody));
			script.RegisterGlobalFunction("void RemoveBody(Entity& in)", asFUNCTION(RemoveBody));
			script.RegisterGlobalFunction("Body@ GetBody(const Entity& in)", asFUNCTION(GetBody));
			//signals
			script.RegisterFuncdef("void OnBodyCollisionCallback(Entity& in)");
			body.RegisterMethod("void ConnectOnCollision(OnBodyCollisionCallback@)", asFUNCTION(connectOnCollision), asCALL_CDECL_OBJFIRST);

			// softbody component
			auto softBody = script.RegisterReferenceType("SoftBodyComp", asOBJ_NOCOUNT);
			softBody.RegisterProperty("SoftBody@ body", asOFFSET(SoftBody, pBody));

			script.RegisterGlobalFunction("SoftBodyComp@ AttachSoftBody(Entity& in, float, float, uint, uint, float, float, const Vector3& in)", asFUNCTION(AttachSoftBody));
			script.RegisterGlobalFunction("void RemoveSoftBody(Entity& in)", asFUNCTION(RemoveSoftBody));
			script.RegisterGlobalFunction("SoftBodyComp@ GetSoftBody(const Entity& in)", asFUNCTION(GetSoftBody));
		}

	}
}
