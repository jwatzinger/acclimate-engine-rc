#pragma once

namespace acl
{
	namespace script
	{
		class Core;
	}

	namespace physics3d
	{

		void registerScript(script::Core& script);

	}
}
