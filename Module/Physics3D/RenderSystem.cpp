#include "RenderSystem.h"
#include "Components.h"
#include "ClothMesh.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\Message.h"
#include "Entity\MessageRegistry.h"
#include "Entity\MessageManager.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\IModel.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\Camera.h"
#include "Render\IStage.h"
#include "System\Convert.h"
#include "System\Assert.h"

#include "Physics\RigidBody.h"
#include "Physics\SphereShape.h"
#include "Physics\BoxShape.h"
#include "Physics\CapsuleShape.h"

namespace acl
{
    namespace physics3d
    {

		RenderSystem::RenderSystem(const gfx::IMeshLoader& meshLoader, gfx::Models& models, render::IStage& debugStage) :
			m_pMeshLoader(&meshLoader), m_pModels(&models), m_pStage(&debugStage)
		{
		}

		void RenderSystem::Init(ecs::MessageManager& messages)
		{
			messages.Subscribe(ecs::MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void RenderSystem::Update(double dt)
        {
			const unsigned int actorId = ecs::ComponentRegistry::GetComponentId(L"Actor");

			/*********************************************
			* Rigid body debug draw
			*********************************************/

			/*auto vBodies = m_pEntities->EntitiesWithComponents<Body>();

			for(auto& entity : vBodies)
			{
				auto& body = *entity->GetComponent<Body>();
				auto& shape = body.pBody->GetShape();

				if(!body.pDebugModel)
				{
					std::wstring stModel;

					// switch on shape type for correct model
					switch(shape.GetType())
					{
					case physics::ShapeType::SPHERE:
						stModel = L"RigidbodyDebugSphere";
						break;
					case physics::ShapeType::BOX:
						stModel = L"RigidbodyDebugCube";
						break;
					case physics::ShapeType::CAPSULE:
						stModel = L"RigidbodyDebugCapsule";
						break;
					case physics::ShapeType::TERRAIN:
						stModel = L"RigidbodyDebugTerrain";
						break;
					default:
						ACL_ASSERT(false);
					}

					body.pDebugModel = &m_pModels->Get(stModel)->CreateInstance();
				}
				else
				{
					math::Matrix mTransform = body.pBody->m_transform.orientation.Matrix();
					
					math::Vector3 vScale(1.0f, 1.0f, 1.0f);

					// switch on shape type for scale
					switch(shape.GetType())
					{
					case physics::ShapeType::SPHERE:
						{
							auto* pSphere = (physics::SphereShape*)&shape;
							const float scale = pSphere->m_radius;

							vScale = math::Vector3(scale, scale, scale);
							break;
						}
					case physics::ShapeType::BOX:
						{
							auto* pBox = (physics::BoxShape*)&shape;

							vScale = pBox->m_vSize;
							break;
						}
					case physics::ShapeType::CAPSULE:
						{
							auto* pCapsule = (physics::CapsuleShape*)&shape;
							vScale = math::Vector3(pCapsule->m_radius, pCapsule->m_height + pCapsule->m_radius, pCapsule->m_radius);
							break;
						}
					case physics::ShapeType::TERRAIN:
					{
							//set the scale of the debug mesh of the terrain
							break;
					}
					default:
						ACL_ASSERT(false);
					}

					mTransform *= body.pBody->m_transform.orientation.Matrix();
					mTransform *= math::MatScale(vScale);
					mTransform *= math::MatTranslation(body.pBody->m_transform.vPosition);

					body.pDebugModel->SetVertexConstant(0, (float*)&mTransform, 4);
					body.pDebugModel->Draw(*m_pStage, 0);
				}
			}*/

			/**********************************************
			* SoftBody
			**********************************************/

			auto vEntities = m_pEntities->EntitiesWithComponents(actorId, SoftBody::family());

			unsigned int id = 0;
			for(auto& entity : vEntities)
			{
				auto& body = *entity->GetComponent<SoftBody>();

				if(body.pBody)
				{
					if(!body.pMesh)
					{
						auto& actor = *entity->GetComponent(actorId);
						auto pModel = actor.GetAttribute<gfx::ModelInstance>(L"Model");

						if(pModel)
						{
							gfx::IMesh* pMesh;
							if(body.width > 1 && body.height > 1)
								pMesh = &m_pMeshLoader->Grid(L"Softbody" + conv::ToString(id), body.width, body.height);
							else
							{
								unsigned int segments;
								if(body.width > body.height)
									segments = body.width - 1;
								else
									segments = body.height - 1;

								pMesh = &m_pMeshLoader->Line(L"Rope" + conv::ToString(id), segments);
							}

							auto& newModel = pModel->GetParent()->Clone();
							newModel.SetMesh(*pMesh);
							pModel->SetParent(newModel);
							body.pMesh = new ClothMesh(*body.pBody, *pMesh);
							
						}
					}
					else
						body.pMesh->Update();
				}

				id++;
			}
        }

		void RenderSystem::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(auto pUpdate = message.Check(ecs::MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pStage->SetVertexConstant(0, (float*)&pUpdate->GetParam<gfx::Camera>(L"Camera")->GetViewProjectionMatrix(), 4);
		}

    }
}