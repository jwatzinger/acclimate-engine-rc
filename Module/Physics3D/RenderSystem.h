#pragma once
#include "Entity\System.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace gfx
	{
		class IMeshLoader;
	}

	namespace render
	{
		class IStage;
	}

    namespace physics3d
    {

        class RenderSystem : 
			public ecs::System<RenderSystem>
        {
        public:

			RenderSystem(const gfx::IMeshLoader& meshLoader, gfx::Models& models, render::IStage& debugStage);

			void Init(ecs::MessageManager& messages) override;
			void Update(double dt) override;
			void ReceiveMessage(const ecs::BaseMessage& message) override;

		private:

			const gfx::IMeshLoader* m_pMeshLoader;
			gfx::Models* m_pModels;

			render::IStage* m_pStage;
        };

    }
}