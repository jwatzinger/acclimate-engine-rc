#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace physics3d
	{

		void SaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Body* pBody = entity.GetComponent<Body>())
			{
				auto& component = entityNode.InsertNode(L"Body");

				component.ModifyAttribute(L"shape", pBody->stShape);
				component.ModifyAttribute(L"material", pBody->stMaterial);
				component.ModifyAttribute(L"damping", conv::ToString(pBody->damping));
				component.ModifyAttribute(L"motion", conv::ToString(pBody->motion));
				component.ModifyAttribute(L"collision", conv::ToString(pBody->collision));
			}
		}

	}
}
