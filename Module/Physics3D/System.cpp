#include "System.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\ComponentRegistry.h"
#include "Physics\Context.h"
#include "Physics\World.h"
#include "Physics\IPhysicsLoader.h"

namespace acl
{
    namespace physics3d
    {

		System::System(const physics::Context& physics) : m_pPhysics(&physics)
		{
		}

        void System::Update(double dt)
        {
			const unsigned int positionId = ecs::ComponentRegistry::GetComponentId(L"Position");
			const unsigned int rotationId = ecs::ComponentRegistry::GetComponentId(L"Rotation");

			/**********************************************
			* RigidBody
			**********************************************/

			auto vEntities = m_pEntities->EntitiesWithComponents<Body>();

			for(auto& entity : vEntities)
			{
				auto& body = *entity->GetComponent<Body>();

				if(!body.pBody)
				{
					if(auto pPosition = entity->GetComponent(positionId))
					{
						auto pVector = pPosition->GetAttribute<math::Vector3>(L"Vector");

						if(auto pMaterial = m_pPhysics->materials[body.stMaterial])
						{
							if(auto pShape = m_pPhysics->shapes[body.stShape])
							{
								const physics::Transform transform(*pVector, math::Quaternion());
								body.pBody = &m_pPhysics->world.AddRigidBody(*pMaterial, *pShape, transform, (physics::MotionType)body.motion, (physics::CollisionType)body.collision, body.damping);
								body.pBody->OnCollision.Connect(&body, &Body::OnCollision);
								body.SigHandleCollision.Connect(this, &System::OnHandleCollision);
								body.pWorld = &m_pPhysics->world;
							}
						}
					}
				}
				else
				{
					if(auto pPosition = entity->GetComponent(positionId))
					{
						auto pVector = pPosition->GetAttribute<math::Vector3>(L"Vector");
						*pVector = body.pBody->m_transform.vPosition;
						auto pDirty = pPosition->GetAttribute<bool>(L"Dirty");
						*pDirty = true;
					}

					if(auto pRotation = entity->GetComponent(rotationId))
					{
						auto quat = pRotation->GetAttribute<math::Quaternion>(L"Quaternion");
						*quat = body.pBody->m_transform.orientation;
						auto pDirty = pRotation->GetAttribute<bool>(L"Dirty");
						*pDirty = true;
					}
						
				}
			}

			/**********************************************
			* SoftBody
			**********************************************/

			auto vSoftEntities = m_pEntities->EntitiesWithComponents<SoftBody>();

			for(auto& entity : vSoftEntities)
			{
				auto& body = *entity->GetComponent<SoftBody>();

				if(!body.pBody)
				{
					math::Vector3 vTranslation;
					if(auto pPosition = entity->GetComponent(positionId))
					{
						auto pVector = pPosition->GetAttribute<math::Vector3>(L"Vector");
						vTranslation = *pVector;
					}

					body.pBody = &m_pPhysics->world.AddSoftBody(body.mass, body.damping, body.width, body.height, vTranslation, body.sizeX, body.sizeY, body.vDir);
					body.pWorld = &m_pPhysics->world;
				}
			}
        }


		void System::OnHandleCollision(Body& body, const physics::RigidBody& rigidBody)
		{
			ACL_ASSERT(body.pBody != &rigidBody);

			auto vEntities = m_pEntities->EntitiesWithComponents<Body>();

			for(auto& entity : vEntities)
			{
				auto& otherbody = *entity->GetComponent<Body>();

				if(otherbody.pBody == &rigidBody)
				{
					body.SigCollision(entity);
					return;
				}
			}
		}

    }
}