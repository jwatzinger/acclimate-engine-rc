#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace physics
	{
		struct Context;
		class RigidBody;
	}

    namespace physics3d
    {

		struct Body;

        class System : 
			public ecs::System<System>
        {
        public:

			System(const physics::Context& physics);

			void Update(double dt);

		private:

			void OnHandleCollision(Body& body, const physics::RigidBody& rigidBody);

			const physics::Context* m_pPhysics;

        };

    }
}