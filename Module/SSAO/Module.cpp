#include "Module.h"
#include "RenderSystem.h"
#include "Core\BaseContext.h"
#include "Core\SettingLoader.h"
#include "Entity\SystemManager.h"
#include "Gfx\IResourceLoader.h"

#include "ParameterController.h"

namespace acl
{
	namespace ssao
	{

		Module::Module(void): m_pSystems(nullptr), m_pCtrl(nullptr)
		{
		}
		
		void Module::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// load settings
			context.core.settingLoader.Load(L"Settings.axm");

			// system
			m_pSystems->AddSystem<RenderSystem>(context.gfx.resources.materials, context.gfx.resources.textures, context.gfx.resources.effects, context.gfx.effect, context.core.settings);

			m_pCtrl = new ssao::ParameterController(context.gui.module, context.core.settings);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<RenderSystem>();
			m_pSystems = nullptr;

			delete m_pCtrl;
		}

		void Module::OnRender(void) const
		{
			m_pSystems->UpdateSystem<RenderSystem>(0.0f);
		}

	}
}
	