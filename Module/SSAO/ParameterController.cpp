#include "ParameterController.h"
#include "Core\SettingModule.h"
#include "Entity\MessageManager.h"
#include "Gui\Sliderbar.h"
#include "Gui\Label.h"
#include "System\Convert.h"

namespace acl
{
	namespace ssao
	{

		ParameterController::ParameterController(gui::Module& module, core::SettingModule& settings) : BaseController(module, L"Menu/Parameter.axm"),
			m_pSettings(&settings), m_scale(1.0f), m_bias(0.045f), m_intensity(2.0f), m_radius(0.5f)
		{
			// scale
			auto pScale = GetWidgetByName<gui::SliderBar>(L"Scale");
			auto& scale = pScale->GetSlider();
			scale.SigValueChanged.Connect(this, &ParameterController::OnChangeScale);
			scale.SetValue(m_scale);

			scale.SetShortcut(gui::ComboKeys::CTRL_ALT, 'S', gui::ShortcutState::ALWAYS);

			// bias
			auto pBias = GetWidgetByName<gui::SliderBar>(L"Bias");
			auto& bias = pBias->GetSlider();
			bias.SigValueChanged.Connect(this, &ParameterController::OnChangeBias);
			bias.SetValue(m_bias);

			// intensity
			auto pIntensity = GetWidgetByName<gui::SliderBar>(L"Intensity");
			auto& intensity = pIntensity->GetSlider();
			intensity.SigValueChanged.Connect(this, &ParameterController::OnChangeIntensity);
			intensity.SetValue(m_intensity);

			// radius
			auto pSlider = GetWidgetByName<gui::SliderBar>(L"Radius");
			auto& slider = pSlider->GetSlider();
			slider.SigValueChanged.Connect(this, &ParameterController::OnChangeRadius);
			slider.SetValue(m_radius);

			UpdateSettings();
		}

		void ParameterController::OnChangeRadius(float rad)
		{
			m_radius = rad;
			UpdateSettings();
		}

		void ParameterController::OnChangeIntensity(float intensity)
		{
			m_intensity = intensity;
			UpdateSettings();
		}

		void ParameterController::OnChangeBias(float bias)
		{
			m_bias = bias;
			UpdateSettings();
		}

		void ParameterController::OnChangeScale(float scale)
		{
			m_scale = scale;
			UpdateSettings();
		}

		void ParameterController::UpdateSettings(void)
		{
			m_pSettings->SigUpdateSettings(*m_pSettings);
		}

	}
}