#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace ssao
	{

		class ParameterController :
			public gui::BaseController
		{
		public:
			ParameterController(gui::Module& module, core::SettingModule& settings);

		private:

			void OnChangeRadius(float rad);
			void OnChangeIntensity(float intensity);
			void OnChangeBias(float bias);
			void OnChangeScale(float bias);

			void UpdateSettings(void);

			float m_scale, m_bias, m_radius, m_intensity;

			core::SettingModule* m_pSettings;

		};

	}
}

