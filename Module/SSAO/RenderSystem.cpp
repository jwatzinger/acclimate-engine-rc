#include "RenderSystem.h"
#include "Core\SettingModule.h"
#include "Entity\Message.h"
#include "Entity\EntityManager.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Gfx\Camera.h"
#include "Gfx\Models.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

namespace acl
{
	namespace ssao
	{

		RenderSystem::RenderSystem(const gfx::Materials& materials, const gfx::Textures& textures, const gfx::Effects& effects, const gfx::FullscreenEffect& effect, core::SettingModule& settings) :
			m_bEnabled(true), m_pTextures(&textures), m_pMaterials(&materials), m_pEffects(&effects), m_pSettings(&settings)
		{
			if(auto pMaterial = materials[L"LightAmbient"])
			{
				if(auto pTexture = textures[L"ssao"])
					pMaterial->SetTexture(2, pTexture);
			}

			m_pInstance = &effect.CreateInstance(L"SSAO", L"Prelight", L"ssao", 0);

			const float params[2][4] = {
				{ (float)m_vScreenSize.x, (float)m_vScreenSize.y, 1.0f / 64.0f, 1.0f / 64.0f },
				{ 1.0f, 0.045f, 2.0f, 0.5f } // scale, bias, intensity, sample rad
			};

			m_pInstance->SetShaderConstant(0, (float*)params, 2);

			m_pBlur = &effect.CreateInstance(L"SSAOBlur", L"Prelight", L"ssaoblured", 0);
			float screenSizeArr[4] = {1.0f / m_vScreenSize.x, 1.0f / m_vScreenSize.y, 0.0f, 0.0f};
			m_pBlur->SetShaderConstant(0, screenSizeArr, 1);

			settings.SigUpdateSettings.Connect(this, &RenderSystem::OnSettingChange);
			OnSettingChange(settings);
		}

		RenderSystem::~RenderSystem(void)
		{
			m_pSettings->SigUpdateSettings.Disconnect(this, &RenderSystem::OnSettingChange);

			delete m_pInstance;
			delete m_pBlur;
		}

		void RenderSystem::Init(ecs::MessageManager& messages)
		{
			messages.Subscribe(ecs::MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void RenderSystem::Update(double dt)
		{
			if(!m_bEnabled)
				return;

			static const unsigned int lightId = ecs::ComponentRegistry::GetComponentId(L"Light");
			static const unsigned int ambientId = ecs::ComponentRegistry::GetComponentId(L"Ambient");

			if(m_pEntities->EntitiesWithComponents(lightId, ambientId).empty())
				return;

			m_pInstance->Draw();
			//m_pBlur->Draw();
		}

		void RenderSystem::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(const auto pUpdate = message.Check(ecs::MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pInstance->SetShaderConstant(2, (float*)&pUpdate->GetParam<gfx::Camera>(L"Camera")->GetViewMatrix(), 4);
		}

		void RenderSystem::OnSettingChange(const core::SettingModule& settings)
		{
			const float screen[2] = { (float)settings.GetSetting(L"screenX")->GetData<unsigned int>(), (float)settings.GetSetting(L"screenY")->GetData<unsigned int>() };
			m_pInstance->SetShaderConstant(0, (float*)screen, 1);

			m_bEnabled = settings.GetSetting(L"ssao")->GetData<bool>();

			auto pEffect = m_pEffects->Get(L"LightAmbient");
			if(m_bEnabled)
				pEffect->SelectExtention(L"SSAO");
			else
				pEffect->SelectExtention(L"");
				
		}

	}
}
