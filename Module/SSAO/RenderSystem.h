#pragma once
#include "Entity\System.h"
#include "Gfx\Materials.h"
#include "Gfx\Textures.h"
#include "Gfx\Effects.h"
#include "Math\Vector.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace gfx
	{
		class FullscreenEffect;
		class FxInstance;
	}

	namespace ssao
	{

		class RenderSystem final : 
			public ecs::System<RenderSystem>
		{
		public:
			RenderSystem(const gfx::Materials& materials, const gfx::Textures& textures, const gfx::Effects& effects, const gfx::FullscreenEffect& effect, core::SettingModule& settings);
			~RenderSystem(void);

			void Init(ecs::MessageManager& messages) override;
			void Update(double dt) override;
			void ReceiveMessage(const ecs::BaseMessage& message) override;

			void OnSettingChange(const core::SettingModule& settings);

		private:

			bool m_bEnabled;

			core::SettingModule* m_pSettings;
			gfx::FxInstance* m_pInstance, *m_pBlur;
			const gfx::Textures* m_pTextures;
			const gfx::Materials* m_pMaterials;
			const gfx::Effects* m_pEffects;

			math::Vector2 m_vScreenSize;
		};

	}
}
