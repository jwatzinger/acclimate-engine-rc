#include "AttachController.h"
#include "Component.h"
#include "IEditor.h"
#include "Entity\Entity.h"
#include "System\Convert.h"

namespace acl
{
	namespace script
	{
		
		/***************************************
		* POSITION
		***************************************/

		ScriptAttachController::ScriptAttachController(gui::Module& module, const editor::IEditor& editor) :
			BaseController(module, L"Editor/AttachScript.axm"), m_pEditor(&editor)
		{
			m_pClass = GetWidgetByName<gui::Textbox<>>(L"Class");
			m_pClass->SigReleased.Connect(this, &ScriptAttachController::OnPickClass);

			m_pInput = GetWidgetByName<gui::Textbox<>>(L"Input");
		}

		editor::IComponentController& ScriptAttachController::Clone(void) const
		{
			return *new ScriptAttachController(*m_pModule, *m_pEditor);
		}

		void ScriptAttachController::Attach(ecs::Entity& entity) const
		{
			entity.AttachComponent<Script>(conv::ToA(m_pClass->GetContent()), m_pInput->GetContent());
		}

		gui::BaseController& ScriptAttachController::GetController(void)
		{
			return *this;
		}

		size_t ScriptAttachController::GetComponentId(void) const
		{
			return Script::family();
		}

		void ScriptAttachController::OnPickClass(void)
		{
			std::string stClass;
			if(m_pEditor->PickScriptClass(conv::ToA(m_pClass->GetContent()), "IGameScript", &stClass))
				m_pClass->SetText(conv::ToW(stClass));
		}

	}
}

