#pragma once
#include "IComponentController.h"
#include "Gui\Textbox.h"
#include "Gui\BaseController.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace script
	{

		/***************************************
		* Position
		***************************************/

		class ScriptAttachController:
			public editor::IComponentController, gui::BaseController
		{
		public:
			ScriptAttachController(gui::Module& module, const editor::IEditor& editor);

			IComponentController& Clone(void) const override;

			void Attach(ecs::Entity& entity) const override;

			gui::BaseController& GetController(void) override;
			size_t GetComponentId(void) const override;

		private:

			void OnPickClass(void);

			gui::Textbox<>* m_pClass, *m_pInput;

			const editor::IEditor* m_pEditor;
		};

	}
}


