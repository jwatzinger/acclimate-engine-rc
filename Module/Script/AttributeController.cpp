#include "AttributeController.h"
#include "Component.h"
#include "IEditor.h"
#include "Entity\Entity.h"

namespace acl
{
	namespace script
	{

		AttributeController::AttributeController(const editor::IEditor& editor) : m_pEntity(nullptr),
			m_pEditor(&editor)
		{
			m_pClass = new gui::Textbox<>(0.0f, 0.0f, 1.0f, 1.0f, L"");
			m_pClass->SetEditable(false);
			m_pClass->SigReleased.Connect(this, &AttributeController::OnPick);
		}

		AttributeController::~AttributeController(void)
		{
			delete m_pClass;
		}

		bool AttributeController::OnHasComponent(const ecs::Entity& entity) const
		{
			return entity.GetComponent<Script>() != nullptr;
		}

		void AttributeController::OnSelectEntity(const ecs::Entity& entity)
		{
			if(auto pScript = entity.GetComponent<Script>())
			{
				m_pClass->SetText(conv::ToW(pScript->stClass));
				m_pEntity = &entity;
			}
			else
				m_pEntity = nullptr;
		}

		void AttributeController::OnPick(void)
		{
			if(m_pEntity)
			{
				std::string stClass;
				if(m_pEditor->PickScriptClass(conv::ToA(m_pClass->GetContent()), "IGameScript", &stClass))
				{
					auto pScript = m_pEntity->GetComponent<Script>();

					if(stClass != pScript->stClass)
					{
						m_pClass->SetText(conv::ToW(stClass));
						pScript->stClass = stClass;
						pScript->bDirty = true;
					}
				}
			}
		}

		void AttributeController::OnFillTable(TableMap& mTable)
		{
			mTable[L"Class"] = m_pClass;
		}

		void AttributeController::OnUpdate(void)
		{
			if(m_pEntity)
			{
				if(auto pScript = m_pEntity->GetComponent<Script>())
					m_pClass->SetText(conv::ToW(pScript->stClass));
			}
		}

	}
}
