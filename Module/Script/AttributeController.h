#pragma once
#include "IComponentAttribute.h"
#include "Gui\Textbox.h"

namespace acl
{
	namespace editor
	{
		class IEditor;
	}

	namespace script
	{

		class AttributeController :
			public editor::IComponentAttribute
		{
		public:

			AttributeController(const editor::IEditor& editor);
			~AttributeController(void);

			bool OnHasComponent(const ecs::Entity& entity) const override;
			void OnSelectEntity(const ecs::Entity& entity) override;
			void OnFillTable(TableMap& mTable) override;
			void OnUpdate(void) override;

		private:

			void OnPick(void);

			const ecs::Entity* m_pEntity;
			const editor::IEditor* m_pEditor;
			gui::Textbox<>* m_pClass;
		};

	}
}

