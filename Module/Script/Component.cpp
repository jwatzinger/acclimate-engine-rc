#include "Component.h"
#include "Input\Module.h"
#include "Script\Instance.h"

namespace acl
{
	namespace script
	{

		Script::Script(const std::string& stClass, const std::wstring& stInput) : stClass(stClass), pInstance(nullptr), bDirty(true),
			pHandler(nullptr), pModule(nullptr), stInput(stInput), needsInit(true)
		{
		}

		Script::~Script(void)
		{
			delete pInstance;
			if(pModule && pHandler)
				pModule->RemoveHandler(*pHandler);
		}

		void Script::OnProcessInput(const input::MappedInput& input) const
		{
			if(pInstance)
				pInstance->CallMethod<void>("void OnProcessInput(const MappedInput@)", input);
		}

	}
}

