#pragma once
#include <unordered_map>
#include "Entity\Component.h"

namespace acl
{
	namespace input
	{
		class Module;
		class Handler;
		struct MappedInput;
	}

	namespace script
	{

		class Instance;

		class Script :
			public ecs::Component<Script>
		{
		public:

			typedef std::unordered_map<std::wstring, std::wstring> AttributeMap;

			Script(const std::string& stClass, const std::wstring& stInput);
			~Script(void);

			void OnProcessInput(const input::MappedInput& input) const;

			std::string stClass;
			std::wstring stInput;
			script::Instance* pInstance;
			bool bDirty, needsInit;
			AttributeMap mAttributes;
			input::Module* pModule;
			input::Handler* pHandler;
		};

	}
}


