#include "EditorPlugin.h"
#include "IComponentRegistry.h"
#include "IEditor.h"
#include "AttachController.h"
#include "AttributeController.h"
//#include "AttributeController.h"

namespace acl
{
	namespace script
	{

		EditorPlugin::EditorPlugin(void)
		{
		}

		void EditorPlugin::OnInit(editor::IEditor& editor)
		{
			m_pEditor = &editor;

			auto& module = editor.GetModule();
			auto& registry = editor.GetComponentRegistry();
			registry.AddComponentAttachController(L"Script", *new ScriptAttachController(module, editor));

			// attriutes
			registry.AddComponentAtttributeController(L"Script", *new AttributeController(editor));
		}

		void EditorPlugin::OnUninit(void)
		{
			auto& registry = m_pEditor->GetComponentRegistry();

			registry.RemoveComponentAttachController(L"Script");
			registry.RemoveComponentAtttributeController(L"Script");
		}

		void EditorPlugin::OnBeginTest(void)
		{
		}

		void EditorPlugin::OnEndTest(void)
		{
		}

	}
}

