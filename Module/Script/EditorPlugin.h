#pragma once
#include "IPlugin.h"

namespace acl
{
	namespace script
	{

		class EditorPlugin :
			public editor::IPlugin
		{
		public:

			EditorPlugin(void);

			void OnInit(editor::IEditor& editor) override;
			void OnLoadResources(const gfx::LoadContext& load) override {}
			void OnUpdate(void) override {}
			void OnRender(void) const override {}
			void OnUninit(void) override;
			void OnBeginTest(void) override;
			void OnEndTest(void) override;

		private:

			editor::IEditor* m_pEditor;
		};

	}
}


