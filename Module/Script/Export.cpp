#include "Export.h"
#include "Module.h"
#include "EditorPlugin.h"

core::IModule& CreateModule(void)
{
	return *new script::Module();
}

editor::IPlugin& CreatePlugin(core::IModule& module)
{
	return *new script::EditorPlugin();
}