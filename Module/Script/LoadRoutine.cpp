#include "LoadRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"


namespace acl
{
	namespace script
	{

		void LoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pScript = entityNode.FirstNode(L"Script"))
			{
				auto pScriptComp = entity.AttachComponent<Script>(conv::ToA(pScript->Attribute(L"class")->GetValue()), pScript->Attribute(L"input")->GetValue());
				if(auto pAttributes = pScript->Nodes(L"Attribute"))
				{
					for(auto pAttribute : *pAttributes)
					{
						pScriptComp->mAttributes.emplace(pAttribute->GetValue(), pAttribute->Attribute(L"value")->GetValue());
					}
				}
			}
		}

	}
}
