#pragma once
#include "Entity\ISubLoadRoutine.h"

namespace acl
{
	namespace script
	{

		class LoadRoutine : 
			public ecs::ISubLoadRoutine
		{
		public:
			
			void Execute(ecs::Entity& entity, const xml::Node& entityNode) const override;
		};

	}
}

