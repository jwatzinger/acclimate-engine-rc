#include "Module.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"

#include "Component.h"
#include "System.h"
#include "RegisterScript.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"

namespace acl
{
	namespace script
	{

		Module::Module(void) : m_pSystems(nullptr)
		{
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			m_pSystems->AddSystem<System>(context.script.core, context.input);

			// components
			ecs::ComponentRegistry::RegisterComponent<Script>(L"Script");
			context.ecs.componentFactory.Register<Script, const char*, const wchar_t*>();

			// io
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Script");
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Script");

			RegisterScript(context.script.core);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<System>();

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Script");
			context.ecs.componentFactory.Unregister<Script>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Script");
			ecs::Loader::UnregisterSubRoutine(L"Script");

			m_pSystems = nullptr;
		}

		void Module::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<System>(dt);
		}

	}
}
