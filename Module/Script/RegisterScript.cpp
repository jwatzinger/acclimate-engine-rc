#include "RegisterScript.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace script
	{
		/*******************************************
		* Script
		*******************************************/

		Script* AttachScript(ecs::EntityHandle* entity, const std::wstring& stName, const std::wstring& stInput)
		{
			return (*entity)->AttachComponent<Script>(conv::ToA(stName), stInput);
		}

		void RemoveScript(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<Script>();
		}

		Script* GetScript(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<Script>();
		}

		void RegisterScript(Core& script)
		{
			// register scripts
			auto gameScript = script.RegisterInterface("IGameScript");
			gameScript.RegisterMethod("void OnUpdate(double)");
			gameScript.RegisterMethod("void OnProcessInput(const MappedInput@)");
			gameScript.RegisterMethod("void OnInit()");

			auto scriptComp = script.RegisterReferenceType("Script", asOBJ_NOCOUNT);

			// position
			script.RegisterGlobalFunction("Script@ AttachScript(Entity& in, const string& in, const string& in)", asFUNCTION(AttachScript));
			script.RegisterGlobalFunction("void RemoveScript(Entity& in)", asFUNCTION(RemoveScript));
			script.RegisterGlobalFunction("Script@ GetScript(const Entity& in)", asFUNCTION(GetScript));
		}

	}
}
