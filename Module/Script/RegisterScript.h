#pragma once

namespace acl
{
	namespace script
	{
		class Core;

		void RegisterScript(Core& script);
	}
}
