#include "SaveRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace script
	{

		void SaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Script* pScript = entity.GetComponent<Script>())
			{
				auto& component = entityNode.InsertNode(L"Script");
				component.ModifyAttribute(L"class", conv::ToW(pScript->stClass));
				component.ModifyAttribute(L"input", pScript->stInput);

				for(auto& attributes : pScript->mAttributes)
				{
					auto& attributeNode = component.InsertNode(L"Attribute");
					attributeNode.SetValue(attributes.first);
					attributeNode.ModifyAttribute(L"value", attributes.second);
				}
			}
		}

	}
}
