#include "System.h"
#include "Component.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Input\Context.h"
#include "Input\Module.h"
#include "Input\ILoader.h"
#include "Input\Handler.h"
#include "Script\Core.h"
#include "System\Convert.h"

namespace acl
{
	namespace script
	{

		System::System(Core& script, const input::Context& input) : 
			m_pScript(&script), m_pInput(&input)
		{
		}

		void System::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<Script>();

			for(auto& entity : vEntities)
			{
				auto& script = *entity->GetComponent<Script>();

				if(!script.pInstance || script.bDirty)
				{
					auto vTypes = m_pScript->GetTypesWithInterface("IGameScript");
					for(auto& type : vTypes)
					{
						if(type.GetName() == script.stClass)
						{
							if(script.pInstance)
								delete script.pInstance;

							script.pInstance = type.CreateInstance("const Entity& in entity", entity);
							for(auto& attribute : script.mAttributes)
							{
								if(auto pAttribute = script.pInstance->GetAttribute(attribute.first))
								{
									switch(pAttribute->GetType())
									{
									case Attribute::Type::INT:
										pAttribute->ModifyData<int>(conv::FromString<int>(attribute.second));
										break;
									case Attribute::Type::FLOAT:
										pAttribute->ModifyData<float>(conv::FromString<float>(attribute.second));
										break;
									case Attribute::Type::BOOL:
										pAttribute->ModifyData<bool>(conv::FromString<bool>(attribute.second));
										break;
									case Attribute::Type::UINT:
										pAttribute->ModifyData<unsigned int>(conv::FromString<unsigned int>(attribute.second));
										break;
									}
								}
							}

							auto pHandler = m_pInput->module.GetHandler(script.stInput);
							if(pHandler)
							{
								script.pModule = &m_pInput->module;
								script.pHandler = pHandler;
								pHandler->SigProcessInput.Connect(&script, &Script::OnProcessInput);
							}
						}
					}
					script.bDirty = false;
					script.needsInit = true;
				}
				else
				{
					if(script.needsInit)
					{
						script.pInstance->CallMethod<void>("void OnInit()");
						script.needsInit = false;
					}
					script.pInstance->CallMethod<void>("void OnUpdate(double)", dt);
				}
			}
		}

	}
}

