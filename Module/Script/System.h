#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace input
	{
		struct Context;
	}

	namespace script
	{

		class Core;

		class System : 
			public ecs::System<System>
		{
		public:
			System(Core& script, const input::Context& input);

			void Update(double dt) override;

		private:

			Core* m_pScript;
			const input::Context* m_pInput;
		};

	}
}


