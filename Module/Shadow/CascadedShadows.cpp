#include "CascadedShadows.h"
#include "Entity\MessageFactory.h"
#include "Gfx\Context.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IZBufferLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\Screen.h"
#include "Math\Vector4.h"
#include "System\Convert.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

namespace acl
{
	namespace shadow
	{

		CascadedShadows::CascadedShadows(unsigned int id, const ecs::MessageFactory& messages, const gfx::Context& gfx, render::IRenderer& renderer, Quality quality) :
			m_camera(math::Vector2(m_bufferSize, m_bufferSize), math::Vector3(-3.0f, 3.0f, -2.3f)), m_nCascadeLevels(7),
			m_bufferSize(2048), m_PCFBlurSize(3), m_bMoveLightTexelSize(true), m_selectedCascadesFit(FitProjection::TO_SCENE),
			m_selectedNearFarFit(FitNearFar::SCENE_AABB), m_pMessages(&messages), m_vSceneAABBMin(0.0, 0.0, 0.0),
			m_vSceneAABBMax(2048.0, 32.0f, 2048.0), m_quality(Quality::ULTRA)
		{
			SetQuality(quality);

			/************************************************
			* Depth map generation
			************************************************/

			const std::wstring stId = conv::ToString(id), stCascaded = L"CascadedShadow" + stId;
			const size_t queueId = renderer.AddQueue(L"Scene");

			m_pZbuffer = gfx.load.zbuffers.Create(stCascaded, math::Vector2(m_bufferSize*m_nCascadeLevels, m_bufferSize));

			const std::wstring stMap = stCascaded + L"Map";
			auto* pShadowMap = gfx.load.textures.Create(stMap, math::Vector2(m_bufferSize*m_nCascadeLevels, m_bufferSize), gfx::TextureFormats::R32, gfx::LoadFlags::RENDER_TARGET);
			for(unsigned int i = 0; i < m_nCascadeLevels; i++)
			{
				const std::wstring stCascade = conv::ToString(i);

				auto pStage = renderer.AddStage(stCascaded + stCascade, L"Scene", queueId, render::CLEAR_Z);
				pStage->SetRenderTarget(0, pShadowMap);
				pStage->SetDepthBuffer(m_pZbuffer);
				pStage->SetViewport(math::Rect(i*m_bufferSize, 0, m_bufferSize, m_bufferSize));

				m_pStages[i] = pStage;
			}

			/*********************************************
			* Shadow map generation
			**********************************************/

			const std::wstring stTargetName = stCascaded + L"Out";
			m_pMap = gfx.load.textures.Create(stTargetName, gfx.screen.GetSize(), gfx::TextureFormats::L8, gfx::LoadFlags::RENDER_TARGET);

			const gfx::TextureVector vTextures = { L"deferred pos", stMap };
			gfx.load.materials.Load(stCascaded, L"CascadedShadowMap", m_nCascadeLevels, vTextures);
			m_pMapEffect = &gfx.effect.CreateInstance(stCascaded, L"Prelight", stTargetName, 0);

			const unsigned int constantOffset = 4 + m_nCascadeLevels*2;
			// The border padding values keep the pixel shader from reading the borders during PCF filtering.
			const float maxBorderPadding = (m_bufferSize - 1.0f) / (float)m_bufferSize;
			const float minBorderPadding = 1.0f / (float)m_bufferSize;
			const float borderPadding[4] = { minBorderPadding, maxBorderPadding, 1.0f / m_nCascadeLevels, minBorderPadding / m_nCascadeLevels };
			m_pMapEffect->SetShaderConstant(constantOffset, borderPadding, 1);

			// xy => pcf, w => bias
			const float mapBlur[4] = { (float)(int)(m_PCFBlurSize / -2.0f), (float)(int)(m_PCFBlurSize / 2.0f + 1), 0.002f, 0.0f };
			m_pMapEffect->SetShaderConstant(constantOffset + 1, mapBlur, 1);

			/*********************************************
			* Shadow blur generation
			**********************************************/

			const std::wstring stBlurName = stCascaded + L"Blur";
			const auto& vScreenSize = gfx.screen.GetSize();
			gfx.load.textures.Create(stBlurName, vScreenSize, gfx::TextureFormats::L8, gfx::LoadFlags::RENDER_TARGET);

			gfx.load.materials.Load(stBlurName, L"Blur", 0, stTargetName);
			m_pBlur = &gfx.effect.CreateInstance(stBlurName, L"Prelight", stBlurName, 0);

			const float blur[4] = { 1.0f / vScreenSize.x, 1.0f /vScreenSize.y, 0.0f, 0.0f };
			m_pBlur->SetShaderConstant(0, blur, 1);
		}

		CascadedShadows::~CascadedShadows(void)
		{
		}

		const gfx::ITexture& CascadedShadows::GetMap(void) const
		{
			return *m_pMap;
		}

		void CascadedShadows::Update(const gfx::Camera& camera, const math::Vector3& vDirection, const math::Vector3& vPosition)
		{
			m_camera.SetPosition(camera.GetPosition()-vDirection);
			m_camera.SetLookAt(camera.GetPosition());

			// adjust view camera
			m_pViewCamera = &camera;

			const float n = camera.GetNear();
			const float f = camera.GetFar();
			const float blendFactor = 0.5f;
			for(unsigned int i = 0; i < m_nCascadeLevels; i++)
			{
				const float partition = (i+1) / (float)(m_nCascadeLevels);
				const float partitionLog = n * pow(f/n, partition);
				const float partitionUni = n + (f - n) * partition;

				float c = blendFactor * partitionLog + (1 - blendFactor) * partitionUni;
				c /= f;
				m_cascadePartitionsZeroToOne[i] = (unsigned int)(c * 100);
			}	

			CalculateSplits();
		}

		void CascadedShadows::Render(void)
		{
			/************************************************
			* Depth map generation
			************************************************/

			for(unsigned int i = 0; i < m_nCascadeLevels; i++)
			{
				const auto mViewProjection = m_matShadowView * m_matShadowProj[i];

				auto pStage = m_pStages[i];
				pStage->SetVertexConstant(0, (float*)&mViewProjection, 4);
				pStage->SetGeometryConstant(0, (float*)&mViewProjection, 4);

				gfx::Camera tempCamp(mViewProjection);
				static const size_t messageId = ecs::MessageRegistry::GetMessageId(L"RenderScene");
				m_pMessages->DeliverMessage(messageId, tempCamp, *pStage, 1);
			}

			/*********************************************
			* Shadow map generation
			**********************************************/

			m_pMapEffect->SetShaderConstant(0, (float*)&m_matShadowView, 4);

			const auto& matTextureScale = math::MatScale(0.5f, -0.5f, 1.0f);
			const auto& matTextureTranslation = math::MatTranslation(0.5f, 0.5f, 0.0f);

			for(unsigned int index = 0; index < m_nCascadeLevels; ++index)
			{
				auto mShadowTexture = m_matShadowProj[index] * matTextureScale * matTextureTranslation;
				m_pMapEffect->SetShaderConstant(4 + index, (float*)&math::Vector4(mShadowTexture.m11, mShadowTexture.m22, mShadowTexture.m33, 1.0f), 1);
				m_pMapEffect->SetShaderConstant(4 + m_nCascadeLevels + index, (float*)&math::Vector4(mShadowTexture.m41, mShadowTexture.m42, mShadowTexture.m43, 0.0f), 1);
			}

			m_pMapEffect->Draw();

			/*********************************************
			* Shadow blur generation
			**********************************************/

			// render blur
			m_pBlur->Draw();
		}

		void CascadedShadows::SetQuality(Quality quality)
		{
			switch(quality)
			{
			case Quality::VERY_LOW:
				m_nCascadeLevels = 3;
				m_bufferSize = 1024;
				m_PCFBlurSize = 3;
				break;
			case Quality::LOW:
				m_nCascadeLevels = 4;
				m_bufferSize = 1024;
				m_PCFBlurSize = 3;
				break;
			case Quality::MEDIUM:
				m_nCascadeLevels = 5;
				m_bufferSize = 1024;
				m_PCFBlurSize = 3;
				break;
			case Quality::HIGH:
				m_nCascadeLevels = 6;
				m_bufferSize = 2048;
				m_PCFBlurSize = 5;
				break;
			case Quality::ULTRA:
				m_nCascadeLevels = 7;
				m_bufferSize = 2048;
				m_PCFBlurSize = 5;
				break;
			}

			m_quality = quality;
		}

		void CascadedShadows::CalculateSplits(void)
		{
			// Copy D3DX matricies into XNA Math matricies.
			auto matViewCameraProjection = m_pViewCamera->GetProjectionMatrix();
			auto matViewCameraView = m_pViewCamera->GetViewMatrix();
			auto matLightCameraView = m_camera.GetViewMatrix();

			auto matInverseViewCamera = matViewCameraView.inverse();

			// Convert from min max representation to center extents represnetation.
			// This will make it easier to pull the points out of the transformation.
			auto vSceneCenter = m_vSceneAABBMin + m_vSceneAABBMax;
			vSceneCenter *= 0.5f;
			auto vSceneExtents = m_vSceneAABBMax - m_vSceneAABBMin;
			vSceneExtents *= 0.5f;

			math::Vector3 vSceneAABBPointsLightSpace[8];
			// This function simply converts the center and extents of an AABB into 8 points
			CreateAABBPoints(vSceneAABBPointsLightSpace, vSceneCenter, vSceneExtents);
			// Transform the scene AABB to Light space.
			for(int index = 0; index < 8; ++index)
			{
				vSceneAABBPointsLightSpace[index] = matLightCameraView.TransformCoord(vSceneAABBPointsLightSpace[index]);
			}


			float frustumIntervalBegin, frustumIntervalEnd;
			float cameraNearFarRange = m_pViewCamera->GetFar() - m_pViewCamera->GetNear();

			math::Vector3 vWorldUnitsPerTexel;

			// We loop over the cascades to calculate the orthographic projection for each cascade.
			for(unsigned int cascadeIndex = 0; cascadeIndex < m_nCascadeLevels; ++cascadeIndex)
			{
				// Calculate the interval of the View Frustum that this cascade covers. We measure the interval 
				// the cascade covers as a Min and Max distance along the Z Axis.
				if(m_selectedCascadesFit == FitProjection::TO_CASCADES)
				{
					// Because we want to fit the orthogrpahic projection tightly around the Cascade, we set the Mimiumum cascade 
					// value to the previous Frustum end Interval
					if(cascadeIndex == 0) 
						frustumIntervalBegin = 0.0f;
					else 
						frustumIntervalBegin = (float)m_cascadePartitionsZeroToOne[cascadeIndex - 1];
				}
				else
				{
					// In the FIT_TO_SCENE technique the Cascades overlap eachother.  In other words, interval 1 is coverd by
					// cascades 1 to 8, interval 2 is covered by cascades 2 to 8 and so forth.
					frustumIntervalBegin = 0.0f;
				}

				// Scale the intervals between 0 and 1. They are now percentages that we can scale with.
				frustumIntervalEnd = (float)m_cascadePartitionsZeroToOne[cascadeIndex];
				frustumIntervalBegin /= (float)MAX_PARTITION;
				frustumIntervalEnd /= (float)MAX_PARTITION;
				frustumIntervalBegin = frustumIntervalBegin * cameraNearFarRange;
				frustumIntervalEnd = frustumIntervalEnd * cameraNearFarRange;
				math::Vector3 vFrustumPoints[8];

				// This function takes the began and end intervals along with the projection matrix and returns the 8
				// points that repreresent the cascade Interval
				CreateFrustumPointsFromCascadeInterval(frustumIntervalBegin, frustumIntervalEnd,
					matViewCameraProjection, vFrustumPoints);

				math::Vector3 vLightCameraOrthographicMin(FLT_MAX, FLT_MAX, FLT_MAX),
							  vLightCameraOrthographicMax(FLT_MIN, FLT_MIN, FLT_MIN);

				// This next section of code calculates the min and max values for the orthographic projection.
				for(int icpIndex = 0; icpIndex < 8; ++icpIndex)
				{
					// Transform the frustum from camera view space to world space.
					matInverseViewCamera.TransformCoord(vFrustumPoints[icpIndex]);
					// Transform the point from world space to Light Camera Space.
					const math::Vector3 vTempTranslatedCornerPoint = matLightCameraView.TransformCoord(math::Vector3(vFrustumPoints[icpIndex]));
					// Find the closest point.
					vLightCameraOrthographicMin = vTempTranslatedCornerPoint.Min(vLightCameraOrthographicMin);
					vLightCameraOrthographicMax = vTempTranslatedCornerPoint.Max(vLightCameraOrthographicMax);
				}

				// This code removes the shimmering effect along the edges of shadows due to
				// the light changing to fit the camera.
				if(m_selectedCascadesFit == FitProjection::TO_SCENE)
				{
					// Fit the ortho projection to the cascades far plane and a near plane of zero. 
					// Pad the projection to be the size of the diagonal of the Frustum partition. 
					// 
					// To do this, we pad the ortho transform so that it is always big enough to cover 
					// the entire camera view frustum.
					auto vDiagonal = vFrustumPoints[0] - vFrustumPoints[6];

					// The bound is the length of the diagonal of the frustum interval.
					float cascadeBound = vDiagonal.length();
					vDiagonal = math::Vector3(cascadeBound, cascadeBound, cascadeBound);

					// The offset calculated will pad the ortho projection so that it is always the same size 
					// and big enough to cover the entire cascade interval.
					auto vBoarderOffset = (vDiagonal -
						(vLightCameraOrthographicMax - vLightCameraOrthographicMin))
						* 0.5f;
					// Set the Z and W components to zero.
					vBoarderOffset.z = 0.0f;

					// Add the offsets to the projection.
					vLightCameraOrthographicMax += vBoarderOffset;
					vLightCameraOrthographicMin -= vBoarderOffset;

					// The world units per texel are used to snap the shadow the orthographic projection
					// to texel sized increments.  This keeps the edges of the shadows from shimmering.
					float fWorldUnitsPerTexel = cascadeBound / (float)m_bufferSize;
					vWorldUnitsPerTexel = math::Vector3(fWorldUnitsPerTexel, fWorldUnitsPerTexel, 0.0f);


				}
				else if(m_selectedCascadesFit == FitProjection::TO_CASCADES)
				{

					// We calculate a looser bound based on the size of the PCF blur.  This ensures us that we're 
					// sampling within the correct map.
					float scaleDuetoBlureAMT = ((float)(m_PCFBlurSize * 2 + 1)
						/ (float)m_bufferSize);
					math::Vector3 vScaleDuetoBlureAMT(scaleDuetoBlureAMT, scaleDuetoBlureAMT, 0.0f);

					float normalizeByBufferSize = (1.0f / (float)m_bufferSize);
					math::Vector3 vNormalizeByBufferSize(normalizeByBufferSize, normalizeByBufferSize, 0.0f);

					// We calculate the offsets as a percentage of the bound.
					math::Vector3 vBoarderOffset = vLightCameraOrthographicMax - vLightCameraOrthographicMin;
					vBoarderOffset *= 0.5f;
					vBoarderOffset *= vScaleDuetoBlureAMT;
					vLightCameraOrthographicMax += vBoarderOffset;
					vLightCameraOrthographicMin -= vBoarderOffset;

					// The world units per texel are used to snap  the orthographic projection
					// to texel sized increments.  
					// Because we're fitting tighly to the cascades, the shimmering shadow edges will still be present when the 
					// camera rotates.  However, when zooming in or strafing the shadow edge will not shimmer.
					vWorldUnitsPerTexel = vLightCameraOrthographicMax - vLightCameraOrthographicMin;
					vWorldUnitsPerTexel *= vNormalizeByBufferSize;

				}
				float lightCameraOrthographicMinZ = vLightCameraOrthographicMin.z;

				if(m_bMoveLightTexelSize)
				{
					// We snape the camera to 1 pixel increments so that moving the camera does not cause the shadows to jitter.
					// This is a matter of integer dividing by the world space size of a texel
					vLightCameraOrthographicMin /= vWorldUnitsPerTexel;
					vLightCameraOrthographicMin.Floor();
					vLightCameraOrthographicMin *= vWorldUnitsPerTexel;

					vLightCameraOrthographicMax /= vWorldUnitsPerTexel;
					vLightCameraOrthographicMax.Floor();
					vLightCameraOrthographicMax *= vWorldUnitsPerTexel;

				}

				//These are the unconfigured near and far plane values.  They are purposly awful to show 
				// how important calculating accurate near and far planes is.
				float nearPlane = 0.0f;
				float farPlane = 10000.0f;

				if(m_selectedNearFarFit == FitNearFar::AABB)
				{
					math::Vector3 vLightSpaceSceneAABBminValue(FLT_MAX, FLT_MAX, FLT_MAX);  // world space scene aabb 
					math::Vector3 vLightSpaceSceneAABBmaxValue(FLT_MIN, FLT_MIN, FLT_MIN);
					// We calculate the min and max vectors of the scene in light space. The min and max "Z" values of the  
					// light space AABB can be used for the near and far plane. This is easier than intersecting the scene with the AABB
					// and in some cases provides similar results.
					for(unsigned int index = 0; index< 8; ++index)
					{
						vLightSpaceSceneAABBminValue = vSceneAABBPointsLightSpace[index].Min(vLightSpaceSceneAABBminValue);
						vLightSpaceSceneAABBmaxValue = vSceneAABBPointsLightSpace[index].Max(vLightSpaceSceneAABBmaxValue);
					}

					// The min and max z values are the near and far planes.
					nearPlane = vLightSpaceSceneAABBminValue.z;
					farPlane = vLightSpaceSceneAABBmaxValue.z;
				}
				else if(m_selectedNearFarFit == FitNearFar::SCENE_AABB
					|| m_selectedNearFarFit == FitNearFar::PANCAKING)
				{
					// By intersecting the light frustum with the scene AABB we can get a tighter bound on the near and far plane.
					ComputeNearAndFar(nearPlane, farPlane, vLightCameraOrthographicMin,
						vLightCameraOrthographicMax, vSceneAABBPointsLightSpace);
					if(m_selectedNearFarFit == FitNearFar::PANCAKING)
					{
						if(lightCameraOrthographicMinZ > nearPlane)
							nearPlane = lightCameraOrthographicMinZ;
					}
				}
				else
				{

				}
				// Craete the orthographic projection for this cascade.
				m_matShadowProj[cascadeIndex] = math::MatOrthoOffCenterLH(vLightCameraOrthographicMin.x, vLightCameraOrthographicMax.x, vLightCameraOrthographicMin.y, vLightCameraOrthographicMax.y, nearPlane, farPlane);

				m_cascadePartitionsFrustum[cascadeIndex] = frustumIntervalEnd;
			}

			m_matShadowView = m_camera.GetViewMatrix();
		}

		void CascadedShadows::CreateAABBPoints(math::Vector3* vAABBPoints, const math::Vector3& vCenter, const math::Vector3& vExtents) const
		{
			//This map enables us to use a for loop and do vector math.
			static const math::Vector3 vExtentsMap[] =
			{
				{ 1.0f, 1.0f, -1.0f },
				{ -1.0f, 1.0f, -1.0f },
				{ 1.0f, -1.0f, -1.0f },
				{ -1.0f, -1.0f, -1.0f, },
				{ 1.0f, 1.0f, 1.0f },
				{ -1.0f, 1.0f, 1.0f },
				{ 1.0f, -1.0f, 1.0f },
				{ -1.0f, -1.0f, 1.0f}
			};

			for(unsigned int index = 0; index < 8; ++index)
			{
				vAABBPoints[index] = vExtentsMap[index] * vExtents + vCenter;
			}
		}

		struct Frustum
		{
			math::Vector3 vOrigin;            // Origin of the frustum (and projection).
			math::Vector4 orientation;       // Unit quaternion representing rotation.

			float RightSlope;           // Positive X slope (X/Z).
			float LeftSlope;            // Negative X slope.
			float TopSlope;             // Positive Y slope (Y/Z).
			float BottomSlope;          // Negative Y slope.
			float Near, Far;            // Z of the near plane and far plane.
		};

		void ComputeFrustumFromProjection(Frustum& out, const math::Matrix& projection)
		{
			// Corners of the projection frustum in homogenous space.
			static const math::Vector4 HomogenousPoints[6] =
			{
				{ 1.0f, 0.0f, 1.0f, 1.0f },   // right (at far plane)
				{ -1.0f, 0.0f, 1.0f, 1.0f },   // left
				{ 0.0f, 1.0f, 1.0f, 1.0f },   // top
				{ 0.0f, -1.0f, 1.0f, 1.0f },   // bottom

				{ 0.0f, 0.0f, 0.0f, 1.0f },     // near
				{ 0.0f, 0.0f, 1.0f, 1.0f }      // far
			};


			auto matInverse = projection.inverse();

			// Compute the frustum corners in world space.
			math::Vector4 vPoints[6];

			for(unsigned int i = 0; i < 6; i++)
			{
				// Transform point.
				vPoints[i] = matInverse.TransformVector4(HomogenousPoints[i]);
			}

			out.orientation = math::Vector4(0.0f, 0.0f, 0.0f, 1.0f);

			// Compute the slopes.
			vPoints[0].x *= 1.0f / vPoints[0].z;
			vPoints[1].x *= 1.0f / vPoints[1].z;
			vPoints[2].x *= 1.0f / vPoints[2].z;
			vPoints[3].x *= 1.0f / vPoints[3].z;

			out.RightSlope = vPoints[0].x;
			out.LeftSlope = vPoints[1].x;
			out.TopSlope = vPoints[2].y;
			out.BottomSlope = vPoints[3].y;

			// Compute near and far.
			vPoints[4].z *= 1.0f / vPoints[4].w;
			vPoints[5].z *= 1.0f / vPoints[5].w;

			out.Near = vPoints[4].z;
			out.Far = vPoints[5].z;
		}

		void CascadedShadows::CreateFrustumPointsFromCascadeInterval(float cascadeIntervalBegin, float cascadeIntervalEnd, const math::Matrix &mViewProjection, math::Vector3* pvCornerPointsWorld) const
		{
			Frustum viewFrustum;
			ComputeFrustumFromProjection(viewFrustum, mViewProjection);
			viewFrustum.Near = cascadeIntervalBegin;
			viewFrustum.Far = cascadeIntervalEnd;

			math::Vector3 vRightTop(viewFrustum.RightSlope, viewFrustum.TopSlope, 1.0f);
			math::Vector3 vLeftBottom(viewFrustum.LeftSlope, viewFrustum.BottomSlope, 1.0f);
			math::Vector3 vNear(viewFrustum.Near, viewFrustum.Near, viewFrustum.Near);
			math::Vector3 vFar(viewFrustum.Far, viewFrustum.Far, viewFrustum.Far);
			math::Vector3 vRightTopNear = vRightTop * vNear;
			math::Vector3 vRightTopFar = vRightTop * vFar;
			math::Vector3 vLeftBottomNear = vLeftBottom * vNear;
			math::Vector3 vLeftBottomFar = vLeftBottom * vFar;

			pvCornerPointsWorld[0] = vRightTopNear;
			pvCornerPointsWorld[1] = math::Vector3(vLeftBottomNear.x, vRightTopNear.y, vRightTopNear.z);
			pvCornerPointsWorld[2] = vLeftBottomNear;
			pvCornerPointsWorld[3] = math::Vector3(vRightTopNear.x, vLeftBottomNear.y, vRightTopNear.z);

			pvCornerPointsWorld[4] = vRightTopFar;
			pvCornerPointsWorld[5] = math::Vector3(vLeftBottomFar.x, vRightTopFar.y, vRightTopFar.z);
			pvCornerPointsWorld[6] = vLeftBottomFar;
			pvCornerPointsWorld[7] = math::Vector3(vRightTopFar.x, vLeftBottomFar.y, vRightTopFar.z);
		}

		struct Triangle
		{
			math::Vector3 pt[3];
			bool culled;
		};

		void CascadedShadows::ComputeNearAndFar(float& nearPlane, float& farPlane, const math::Vector3& vLightCameraOrthographicMin, const math::Vector3& vLightCameraOrthographicMax, math::Vector3* pvPointsInCameraView)
		{
			// Initialize the near and far planes
			nearPlane = FLT_MAX;
			farPlane = -FLT_MAX;

			Triangle triangleList[16];
			unsigned int triangleCnt = 1;

			triangleList[0].pt[0] = pvPointsInCameraView[0];
			triangleList[0].pt[1] = pvPointsInCameraView[1];
			triangleList[0].pt[2] = pvPointsInCameraView[2];
			triangleList[0].culled = false;

			// These are the indices used to tesselate an AABB into a list of triangles.
			static const unsigned int iAABBTriIndexes[] =
			{
				0, 1, 2, 1, 2, 3,
				4, 5, 6, 5, 6, 7,
				0, 2, 4, 2, 4, 6,
				1, 3, 5, 3, 5, 7,
				0, 1, 4, 1, 4, 5,
				2, 3, 6, 3, 6, 7
			};

			unsigned int pointPassesCollision[3];

			// At a high level: 
			// 1. Iterate over all 12 triangles of the AABB.  
			// 2. Clip the triangles against each plane. Create new triangles as needed.
			// 3. Find the min and max z values as the near and far plane.

			//This is easier because the triangles are in camera spacing making the collisions tests simple comparisions.

			float fLightCameraOrthographicMinX = vLightCameraOrthographicMin.x;
			float fLightCameraOrthographicMaxX = vLightCameraOrthographicMax.x;
			float fLightCameraOrthographicMinY = vLightCameraOrthographicMin.y;
			float fLightCameraOrthographicMaxY = vLightCameraOrthographicMax.y;

			for(unsigned int AABBTriIter = 0; AABBTriIter < 12; ++AABBTriIter)
			{

				triangleList[0].pt[0] = pvPointsInCameraView[iAABBTriIndexes[AABBTriIter * 3 + 0]];
				triangleList[0].pt[1] = pvPointsInCameraView[iAABBTriIndexes[AABBTriIter * 3 + 1]];
				triangleList[0].pt[2] = pvPointsInCameraView[iAABBTriIndexes[AABBTriIter * 3 + 2]];
				triangleCnt = 1;
				triangleList[0].culled = false;

				// Clip each invidual triangle against the 4 frustums.  When ever a triangle is clipped into new triangles, 
				//add them to the list.
				for(unsigned int frustumPlaneIter = 0; frustumPlaneIter < 4; ++frustumPlaneIter)
				{

					float edge;
					unsigned int component;

					if(frustumPlaneIter == 0)
					{
						edge = fLightCameraOrthographicMinX;
						component = 0;
					}
					else if(frustumPlaneIter == 1)
					{
						edge = fLightCameraOrthographicMaxX;
						component = 0;
					}
					else if(frustumPlaneIter == 2)
					{
						edge = fLightCameraOrthographicMinY;
						component = 1;
					}
					else
					{
						edge = fLightCameraOrthographicMaxY;
						component = 1;
					}

					for(unsigned int triIter = 0; triIter < triangleCnt; ++triIter)
					{
						// We don't delete triangles, so we skip those that have been culled.
						if(!triangleList[triIter].culled)
						{
							unsigned int insideVertCount = 0;
							math::Vector3 tempOrder;
							// Test against the correct frustum plane.
							// This could be written more compactly, but it would be harder to understand.

							if(frustumPlaneIter == 0)
							{
								for(unsigned int triPtIter = 0; triPtIter < 3; ++triPtIter)
								{
									if(triangleList[triIter].pt[triPtIter].x >
										vLightCameraOrthographicMin.x)
										pointPassesCollision[triPtIter] = 1;
									else
										pointPassesCollision[triPtIter] = 0;
									insideVertCount += pointPassesCollision[triPtIter];
								}
							}
							else if(frustumPlaneIter == 1)
							{
								for(unsigned int triPtIter = 0; triPtIter < 3; ++triPtIter)
								{
									if(triangleList[triIter].pt[triPtIter].x <
										vLightCameraOrthographicMax.x)
										pointPassesCollision[triPtIter] = 1;
									else
										pointPassesCollision[triPtIter] = 0;

									insideVertCount += pointPassesCollision[triPtIter];
								}
							}
							else if(frustumPlaneIter == 2)
							{
								for(unsigned int triPtIter = 0; triPtIter < 3; ++triPtIter)
								{
									if(triangleList[triIter].pt[triPtIter].y >
										vLightCameraOrthographicMin.y)
										pointPassesCollision[triPtIter] = 1;
									else
										pointPassesCollision[triPtIter] = 0;

									insideVertCount += pointPassesCollision[triPtIter];
								}
							}
							else
							{
								for(unsigned int triPtIter = 0; triPtIter < 3; ++triPtIter)
								{
									if(triangleList[triIter].pt[triPtIter].y <
										vLightCameraOrthographicMax.y)
										pointPassesCollision[triPtIter] = 1;
									else
										pointPassesCollision[triPtIter] = 0;

									insideVertCount += pointPassesCollision[triPtIter];
								}
							}

							// Move the points that pass the frustum test to the begining of the array.
							if(pointPassesCollision[1] && !pointPassesCollision[0])
							{
								tempOrder = triangleList[triIter].pt[0];
								triangleList[triIter].pt[0] = triangleList[triIter].pt[1];
								triangleList[triIter].pt[1] = tempOrder;
								pointPassesCollision[0] = true;
								pointPassesCollision[1] = false;
							}
							if(pointPassesCollision[2] && !pointPassesCollision[1])
							{
								tempOrder = triangleList[triIter].pt[1];
								triangleList[triIter].pt[1] = triangleList[triIter].pt[2];
								triangleList[triIter].pt[2] = tempOrder;
								pointPassesCollision[1] = true;
								pointPassesCollision[2] = false;
							}
							if(pointPassesCollision[1] && !pointPassesCollision[0])
							{
								tempOrder = triangleList[triIter].pt[0];
								triangleList[triIter].pt[0] = triangleList[triIter].pt[1];
								triangleList[triIter].pt[1] = tempOrder;
								pointPassesCollision[0] = true;
								pointPassesCollision[1] = false;
							}

							if(insideVertCount == 0)
							{ // All points failed. We're done,  
								triangleList[triIter].culled = true;
							}
							else if(insideVertCount == 1)
							{// One point passed. Clip the triangle against the Frustum plane
								triangleList[triIter].culled = false;

								// 
								math::Vector3 vVert0ToVert1 = triangleList[triIter].pt[1] - triangleList[triIter].pt[0];
								math::Vector3 vVert0ToVert2 = triangleList[triIter].pt[2] - triangleList[triIter].pt[0];

								// Find the collision ratio.
								float hitPointTimeRatio = edge - triangleList[triIter].pt[0].GetByIndex(component);
								// Calculate the distance along the vector as ratio of the hit ratio to the component.
								float distanceAlongVector01 = hitPointTimeRatio / vVert0ToVert1.GetByIndex(component);
								float distanceAlongVector02 = hitPointTimeRatio / vVert0ToVert2.GetByIndex(component);
								// Add the point plus a percentage of the vector.
								vVert0ToVert1 *= distanceAlongVector01;
								vVert0ToVert1 += triangleList[triIter].pt[0];
								vVert0ToVert2 *= distanceAlongVector02;
								vVert0ToVert2 += triangleList[triIter].pt[0];

								triangleList[triIter].pt[1] = vVert0ToVert2;
								triangleList[triIter].pt[2] = vVert0ToVert1;

							}
							else if(insideVertCount == 2)
							{ // 2 in  // tesselate into 2 triangles

								// Copy the triangle\(if it exists) after the current triangle out of
								// the way so we can override it with the new triangle we're inserting.
								triangleList[triangleCnt] = triangleList[triIter + 1];

								triangleList[triIter].culled = false;
								triangleList[triIter + 1].culled = false;

								// Get the vector from the outside point into the 2 inside points.
								auto vVert2ToVert0 = triangleList[triIter].pt[0] - triangleList[triIter].pt[2];
								auto vVert2ToVert1 = triangleList[triIter].pt[1] - triangleList[triIter].pt[2];

								// Get the hit point ratio.
								float hitPointTime_2_0 = edge - triangleList[triIter].pt[2].GetByIndex(component);
								float distanceAlongVector_2_0 = hitPointTime_2_0 / vVert2ToVert0.GetByIndex(component);
								// Calcaulte the new vert by adding the percentage of the vector plus point 2.
								vVert2ToVert0 *= distanceAlongVector_2_0;
								vVert2ToVert0 += triangleList[triIter].pt[2];

								// Add a new triangle.
								triangleList[triIter + 1].pt[0] = triangleList[triIter].pt[0];
								triangleList[triIter + 1].pt[1] = triangleList[triIter].pt[1];
								triangleList[triIter + 1].pt[2] = vVert2ToVert0;

								//Get the hit point ratio.
								float hitPointTime_2_1 = edge - triangleList[triIter].pt[2].GetByIndex(component);
								float distanceAlongVector_2_1 = hitPointTime_2_1 / vVert2ToVert1.GetByIndex(component);
								vVert2ToVert1 *= distanceAlongVector_2_1;
								vVert2ToVert1 += triangleList[triIter].pt[2];
								triangleList[triIter].pt[0] = triangleList[triIter + 1].pt[1];
								triangleList[triIter].pt[1] = triangleList[triIter + 1].pt[2];
								triangleList[triIter].pt[2] = vVert2ToVert1;
								// Cncrement triangle count and skip the triangle we just inserted.
								++triangleCnt;
								++triIter;
							}
							else
								triangleList[triIter].culled = false;

						}// end if !culled loop            
					}
				}
				for(unsigned int index = 0; index < triangleCnt; ++index)
				{
					if(!triangleList[index].culled)
					{
						// Set the near and far plan and the min and max z values respectivly.
						for(int vertind = 0; vertind < 3; ++vertind)
						{
							float triangleCoordZ = triangleList[index].pt[vertind].z;
							if(nearPlane > triangleCoordZ)
								nearPlane = triangleCoordZ;

							if(farPlane  <triangleCoordZ)
								farPlane = triangleCoordZ;
						}
					}
				}
			}

		}

		void CascadedShadows::Reset(void)
		{
			// TODO: implement
		}

	}
}

