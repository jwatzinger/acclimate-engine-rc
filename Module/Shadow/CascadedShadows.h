#pragma once
#include "IShadowImplementation.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace ecs
	{
		class MessageFactory;
	}

	namespace gfx
	{
		class FxInstance;
		struct Context;
		class IZBuffer;
	}

	namespace render
	{
		class IStage;
		class IRenderer;
	}

	namespace shadow
	{

		enum class FitProjection
		{
			TO_CASCADES,
			TO_SCENE
		};

		enum FitNearFar
		{
			PANCAKING,
			ZERO_ONE,
			AABB,
			SCENE_AABB
		};

		class CascadedShadows :
			public IShadowImplementation
		{
			static const unsigned int MAX_CASCADES = 8;
			static const unsigned int MAX_PARTITION = 100;
		public:
			CascadedShadows(unsigned int id, const ecs::MessageFactory& messages, const gfx::Context& gfx, render::IRenderer& renderer, Quality quality);
			~CascadedShadows(void);

			const gfx::ITexture& GetMap(void) const override;

			void Update(const gfx::Camera& camera, const math::Vector3& vDirection, const math::Vector3& vPosition) override;
			void Render(void) override;

			void SetQuality(Quality quality) override;

		private:

			void CalculateSplits(void);
			void CreateAABBPoints(math::Vector3* vAABBPoints, const math::Vector3& vCenter, const math::Vector3& vExtents) const;
			void CreateFrustumPointsFromCascadeInterval(float cascadeIntervalBegin, float cascadeIntervalEnd, const math::Matrix &mViewProjection, math::Vector3* pvCornerPointsWorld) const;
			void ComputeNearAndFar(float& nearPlane, float& farPlane, const math::Vector3& vLightCameraOrthographicMin, const math::Vector3& vLightCameraOrthographicMax, math::Vector3* pvPointsInCameraView);

			void Reset(void);

			Quality m_quality;
			unsigned int m_nCascadeLevels, m_bufferSize, m_PCFBlurSize;
			unsigned int m_cascadePartitionsZeroToOne[MAX_CASCADES];
			bool m_bMoveLightTexelSize;
			float m_cascadePartitionsFrustum[MAX_CASCADES];
			FitProjection m_selectedCascadesFit;
			FitNearFar m_selectedNearFarFit;
			math::Vector3 m_vSceneAABBMin, m_vSceneAABBMax;
			math::Matrix m_matShadowView;
			math::Matrix m_matShadowProj[MAX_CASCADES];

			const ecs::MessageFactory* m_pMessages;

			gfx::Camera m_camera;
			gfx::ITexture* m_pMap;
			gfx::IZBuffer* m_pZbuffer;
			const gfx::Camera* m_pViewCamera;

			gfx::FxInstance* m_pMapEffect, *m_pBlur;
			render::IStage* m_pStages[MAX_CASCADES];

		};

	}
}


