#include "Components.h"
#include "IShadowImplementation.h"

namespace acl
{
	namespace ecs
	{

		ShadowCaster::ShadowCaster(void): id(-1),
			pShadow(nullptr)
		{
		}

		ShadowCaster::~ShadowCaster(void)
		{
			delete pShadow;
		}

	}
}