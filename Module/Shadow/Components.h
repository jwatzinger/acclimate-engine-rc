#pragma once
#include "Entity\Component.h"

namespace acl
{
	namespace shadow
	{
		class IShadowImplementation;
	}

	namespace ecs
	{

		struct ShadowCaster : Component<ShadowCaster>
		{
			ShadowCaster(void);
			~ShadowCaster(void);

			int id;
			shadow::IShadowImplementation* pShadow;
		};

	}
}