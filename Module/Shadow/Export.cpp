#include "Export.h"
#include "ShadowModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::ShadowModule();
}