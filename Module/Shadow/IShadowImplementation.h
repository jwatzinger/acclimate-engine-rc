#pragma once

namespace acl
{
	namespace gfx
	{
		class ITexture;
		class Camera;
	}

	namespace math
	{
		struct Vector3;
	}

	namespace shadow
	{
		enum class Quality
		{
			VERY_LOW,
			LOW,
			MEDIUM,
			HIGH,
			ULTRA
		};

		class IShadowImplementation
		{
		public:

			virtual ~IShadowImplementation(void) = 0 {};

			virtual const gfx::ITexture& GetMap(void) const = 0;

			virtual void Update(const gfx::Camera& camera, const math::Vector3& vDirection, const math::Vector3& vPosition) = 0;
			virtual void Render(void) = 0;

			virtual void SetQuality(Quality quality) = 0;
		};
	}
}