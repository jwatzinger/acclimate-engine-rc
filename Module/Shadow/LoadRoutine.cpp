#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void ShadowLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pShadow = entityNode.FirstNode(L"ShadowCaster"))
			{
				entity.AttachComponent<ShadowCaster>();
			}
		}

	}
}
