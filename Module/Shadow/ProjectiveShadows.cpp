#include "ProjectiveShadows.h"
#include "ApiInclude.h"
#include "Entity\MessageFactory.h"
#include "Gfx\Context.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IZBufferLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\Screen.h"
#include "Math\Utility.h"
#include "Render\IStage.h"
#include "Render\IRenderer.h"
#include "System\Convert.h"

namespace acl
{
	namespace shadow
	{

		ProjectiveShadows::ProjectiveShadows(unsigned int id, const ecs::MessageFactory& messages, const gfx::Context& gfx, render::IRenderer& renderer) :
			m_bufferSize(1024), m_camera(math::Vector2(m_bufferSize, m_bufferSize), math::Vector3(), math::PI / 2.0f), m_pMessages(&messages)
		{			
			m_camera.SetClipPlanes(2.0f, 100.0f);

			/************************************************
			* Depth map generation
			************************************************/

			const std::wstring stId = conv::ToString(id), stProjective = L"ProjectiveShadow" + stId;
			const size_t queueId = renderer.AddQueue(L"Scene");

			m_pZbuffer = gfx.load.zbuffers.Create(stProjective, math::Vector2(m_bufferSize, m_bufferSize));

			const std::wstring stMap = stProjective + L"Map";
			auto* pShadowMap = gfx.load.textures.Create(stMap, math::Vector2(m_bufferSize, m_bufferSize), gfx::TextureFormats::R32, gfx::LoadFlags::RENDER_TARGET);

			m_pStage = renderer.AddStage(stProjective + L"Depth", L"Scene", queueId, render::CLEAR_Z);
			m_pStage->SetRenderTarget(0, pShadowMap);
			m_pStage->SetDepthBuffer(m_pZbuffer);
			m_pStage->SetViewportFromTarget();

			/*********************************************
			* Shadow map generation
			**********************************************/

			const std::wstring stTargetName = stProjective + L"Out";
			m_pMap = gfx.load.textures.Create(stTargetName, gfx.screen.GetSize(), gfx::TextureFormats::L8, gfx::LoadFlags::RENDER_TARGET);

			const gfx::TextureVector vTextures = { L"deferred pos", stMap };
			gfx.load.materials.Load(stProjective, L"ShadowProjMap", 0, vTextures);
			m_pMapEffect = &gfx.effect.CreateInstance(stProjective, L"Prelight", stTargetName, 0);

			/*********************************************
			* Shadow blur generation
			**********************************************/

			const std::wstring stBlurName = stProjective + L"Blur";
			const auto& vScreenSize = gfx.screen.GetSize();
			gfx.load.textures.Create(stBlurName, vScreenSize, gfx::TextureFormats::L8, gfx::LoadFlags::RENDER_TARGET);

			gfx.load.materials.Load(stBlurName, L"Blur", 0, stTargetName);
			m_pBlur = &gfx.effect.CreateInstance(stBlurName, L"Prelight", stBlurName, 0);

			const float blur[4] = { 1.0f / vScreenSize.x, 1.0f / vScreenSize.y, 0.0f, 0.0f };
			m_pBlur->SetShaderConstant(0, blur, 1);
		}

		ProjectiveShadows::~ProjectiveShadows()
		{
		}

		const gfx::ITexture& ProjectiveShadows::GetMap(void) const
		{
			return *m_pMap;
		}

		void ProjectiveShadows::Update(const gfx::Camera& camera, const math::Vector3& vDirection, const math::Vector3& vPosition)
		{
			m_camera.SetPosition(vPosition);
			m_camera.SetLookAt(vPosition + vDirection);

			const math::Vector3 vUp(0.0f, 1.0f, 0.0f);
			const auto vRight = vUp.Cross(vDirection.normal()).Normalize();
			const auto vNewUp = vRight.Cross(vDirection).Normalize();
			m_camera.SetUp(vNewUp);
		}

		void ProjectiveShadows::Render(void)
		{
			/*********************************************
			* Shadow map generation
			**********************************************/

			const math::Matrix& mViewProj = m_camera.GetViewProjectionMatrix();
			m_pStage->SetVertexConstant(0, (float*)&mViewProj, 4);

			static const size_t messageId = ecs::MessageRegistry::GetMessageId(L"RenderScene");
			m_pMessages->DeliverMessage(messageId, m_camera, *m_pStage, 1);

			/*********************************************
			* Shadow map generation
			**********************************************/

			float fTexOffs = 0.5f;

			const auto api = getUsedAPI();
			if(api == GraphicAPI::DX9)
				fTexOffs += (0.5f / m_bufferSize);

			math::Matrix matTexAdj(0.5f, 0.0f, 0.0f, 0.0f,
				0.0f, -0.5f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				fTexOffs, fTexOffs, 0.0f, 1.0f);

			if(api == GraphicAPI::GL4)
				matTexAdj.m22 = -matTexAdj.m22;

			const auto mTexture = mViewProj * matTexAdj;

			const float arr[4] = { 1.0f / m_bufferSize, 1.0f / m_bufferSize, 0, 0 };

			m_pMapEffect->SetShaderConstant(0, (float*)&mTexture, 4);
			m_pMapEffect->SetShaderConstant(4, (float*)&mViewProj, 4);
			m_pMapEffect->SetShaderConstant(8, arr, 1);

			m_pMapEffect->Draw();

			/************************************************
			* Shadow blurring
			************************************************/

			m_pBlur->Draw();
		}

		void ProjectiveShadows::SetQuality(Quality quality)
		{
		}

	}
}

