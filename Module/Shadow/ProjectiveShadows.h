#pragma once
#include "IShadowImplementation.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace ecs
	{
		class MessageFactory;
	}

	namespace gfx
	{
		class FxInstance;
		struct Context;
		class IZBuffer;
	}

	namespace render
	{
		class IStage;
		class IRenderer;
	}

	namespace shadow
	{

		class ProjectiveShadows :
			public IShadowImplementation
		{
		public:
			ProjectiveShadows(unsigned int id, const ecs::MessageFactory& messages, const gfx::Context& gfx, render::IRenderer& renderer);
			~ProjectiveShadows(void);

			const gfx::ITexture& GetMap(void) const override;

			void Update(const gfx::Camera& camera, const math::Vector3& vDirection, const math::Vector3& vPosition) override;
			void Render(void) override;

			void SetQuality(Quality quality) override;

		private:

			unsigned int m_bufferSize;

			const ecs::MessageFactory* m_pMessages;

			gfx::Camera m_camera;
			gfx::ITexture* m_pMap;
			gfx::IZBuffer* m_pZbuffer;
			gfx::FxInstance* m_pMapEffect, *m_pBlur;

			render::IStage* m_pStage;
		};

	}
}


