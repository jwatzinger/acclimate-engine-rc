#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void ShadowSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(ShadowCaster* pShadow = entity.GetComponent<ShadowCaster>())
			{
				entityNode.InsertNode(L"ShadowCaster");
			}
		}

	}
}
