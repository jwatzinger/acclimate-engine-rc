#include "ShadowModule.h"
#include "ShadowSystem.h"
#include "ShadowRenderSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "Core\BaseContext.h"
#include "Core\SettingLoader.h"
#include "Core\SettingModule.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Gfx\IResourceLoader.h"

namespace acl
{
	namespace modules
	{

		ShadowModule::ShadowModule(void): m_pSystems(nullptr)
		{
		}

		void ShadowModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void ShadowModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// load settings
			context.core.settingLoader.Load(L"Settings.axm");

			// system
			m_pSystems->AddSystem<ecs::ShadowSystem>(context.gfx, context.render.renderer, context.core.settings);
			m_pSystems->AddSystem<ecs::ShadowRenderSystem>(context.core.settings);

			// io
			ecs::Saver::RegisterSubRoutine<ecs::ShadowSaveRoutine>(L"Shadow");
			ecs::Loader::RegisterSubRoutine<ecs::ShadowLoadRoutine>(L"Shadow");

			// component
			ecs::ComponentRegistry::RegisterComponent<ecs::ShadowCaster>(L"ShadowCaster");
			context.ecs.componentFactory.Register<ecs::ShadowCaster>();
		}

		void ShadowModule::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<ecs::ShadowSystem>();
			m_pSystems->RemoveSystem<ecs::ShadowRenderSystem>();

			// component
			ecs::ComponentRegistry::UnregisterComponent(L"ShadowCaster");
			context.ecs.componentFactory.Unregister<ecs::ShadowCaster>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Shadow");
			ecs::Loader::UnregisterSubRoutine(L"Shadow");

			m_pSystems = nullptr;
		}

		void ShadowModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::ShadowSystem>(dt);
		}

		void ShadowModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::ShadowRenderSystem>(0.0f);
		}

	}
}
