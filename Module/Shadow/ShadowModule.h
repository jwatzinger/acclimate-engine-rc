#pragma once
#include "Core\IModule.h"

namespace acl
{
	namespace ecs
	{
		class SystemManager;
	}

	namespace modules
	{

		class ShadowModule final :
			public core::IModule
		{
		public:

			ShadowModule(void);

			void OnLoadResources(const gfx::LoadContext& context) override;
			void OnLoadRender(const render::Context& context) override {}
			void OnInit(const core::GameStateContext& context) override;
			void OnUninit(const core::GameStateContext& context) override;
			void OnUpdate(double dt) override;
			void OnRender(void) const override;

		private:

			ecs::SystemManager* m_pSystems;

		};

	}
}

