#include "ShadowRenderSystem.h"
#include "Components.h"
#include "IShadowImplementation.h"
#include "Core\SettingModule.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
	
namespace acl
{
	namespace ecs
	{

		ShadowRenderSystem::ShadowRenderSystem(core::SettingModule& settings) :
			m_bRenderShadows(true), m_pSettings(&settings)
		{
			settings.SigUpdateSettings.Connect(this, &ShadowRenderSystem::OnSettingChange);
			OnSettingChange(settings);
		}

		ShadowRenderSystem::~ShadowRenderSystem(void)
		{
			m_pSettings->SigUpdateSettings.Connect(this, &ShadowRenderSystem::OnSettingChange);
		}

		void ShadowRenderSystem::Update(double dt)
		{
			if(!m_bRenderShadows)
				return;

	        auto vEntities = m_pEntities->EntitiesWithComponents<ShadowCaster>();

			size_t count = 0;
	        for(auto& entity : vEntities)
	        {
				ShadowCaster& shadowCaster = *entity->GetComponent<ShadowCaster>();

				if(shadowCaster.pShadow)
					shadowCaster.pShadow->Render();
			}
		}

		void ShadowRenderSystem::OnSettingChange(const core::SettingModule& settings)
		{
			auto pShadows = settings.GetSetting(L"shadows");
			m_bRenderShadows = pShadows->GetData<bool>();
		}

	}
}
