#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace ecs
	{

		class ShadowRenderSystem final :
			public System<ShadowRenderSystem>
		{
		public:

			ShadowRenderSystem(core::SettingModule& settings);
			~ShadowRenderSystem(void);

			void Update(double dt) override;

			void OnSettingChange(const core::SettingModule& settings);

		private:

			bool m_bRenderShadows;
			
			core::SettingModule* m_pSettings;
		};

	}
}

