#include "ShadowSystem.h"
#include "Components.h"
#include "Core\SettingModule.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\Message.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\IMaterial.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\IModel.h"
#include "Math\Utility.h"
#include "System\Convert.h"

#include "CascadedShadows.h"
#include "ProjectiveShadows.h"
#include "Gfx\Context.h"

namespace acl
{
	namespace ecs
	{

		ShadowSystem::ShadowSystem(const gfx::Context& gfx, render::IRenderer& renderer, core::SettingModule& settings) : m_pTextures(&gfx.resources.textures), m_pCamera(nullptr),
			m_bRenderShadows(true), m_pSettings(&settings), m_pEffects(&gfx.resources.effects), m_pGfx(&gfx), m_pRenderer(&renderer)
		{
			settings.SigUpdateSettings.Connect(this, &ShadowSystem::OnSettingChange);
			OnSettingChange(settings);
		}

		ShadowSystem::~ShadowSystem(void)
		{
			m_pSettings->SigUpdateSettings.Disconnect(this, &ShadowSystem::OnSettingChange);
		}

		void ShadowSystem::Init(MessageManager& messages)
		{
			messages.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void ShadowSystem::Update(double dt)
		{			
			if(!m_bRenderShadows)
				return;

	        auto vEntities = m_pEntities->EntitiesWithComponents<ShadowCaster>();

			size_t count = 0;
	        for(auto& entity : vEntities)
	        {
				ShadowCaster& shadowCaster = *entity->GetComponent<ShadowCaster>();

				shadowCaster.id = count;

				math::Vector3 vPosition, vDirection;
				if(auto pPosition = entity->GetComponent(ComponentRegistry::GetComponentId(L"Position")))
				{
					vPosition = *pPosition->GetAttribute<math::Vector3>(L"Vector");

					if(auto pDirection = entity->GetComponent(ComponentRegistry::GetComponentId(L"Direction")))
					{
						vDirection = *pDirection->GetAttribute<math::Vector3>(L"Vector");

						if(!shadowCaster.pShadow)
							shadowCaster.pShadow = new shadow::ProjectiveShadows(0, *m_pMessageFactory, *m_pGfx, *m_pRenderer);
					}
				}
				else if(auto pDirection = entity->GetComponent(ComponentRegistry::GetComponentId(L"Direction")))
				{
					vDirection = *pDirection->GetAttribute<math::Vector3>(L"Vector");

					if(!shadowCaster.pShadow)
						shadowCaster.pShadow = new shadow::CascadedShadows(0, *m_pMessageFactory, *m_pGfx, *m_pRenderer, m_quality);
				}

				if(m_pCamera && shadowCaster.pShadow)
					shadowCaster.pShadow->Update(*m_pCamera, vDirection, vPosition);

				if(auto pLight = entity->GetComponent(ComponentRegistry::GetComponentId(L"Light")))
				{
					if(auto pModel = pLight->GetAttribute<gfx::ModelInstance>(L"Model"))
					{
						if(auto pMaterial = pModel->GetParent()->GetMaterial(2))
						{
							if(shadowCaster.pShadow)
								pMaterial->SetTexture(3, &shadowCaster.pShadow->GetMap());
						}
					}
				}

				count++;
			}
		}

		void ShadowSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pCamera = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pCamera = pCamera->GetParam<gfx::Camera>(L"Camera");
		}

		void ShadowSystem::OnSettingChange(const core::SettingModule& settings)
		{
			auto pShadows = settings.GetSetting(L"shadows");
			m_bRenderShadows = pShadows->GetData<bool>();

			const std::vector<std::wstring> vEffects = { L"LightDirectional", L"LightSpot" };
			for(auto& stEffect : vEffects)
			{
				auto pEffect = m_pEffects->Get(stEffect);
				if(m_bRenderShadows)
					pEffect->SelectExtention(L"Shadow");
				else
					pEffect->SelectExtention(L"");
			}

			auto pQuality = settings.GetSetting(L"shadowQuality");
			const auto quality = pQuality->GetData<int>();
			ACL_ASSERT(quality >= 0 && quality <= 4);

			m_quality = (shadow::Quality)quality;
		}

	}
}