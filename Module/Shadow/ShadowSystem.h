#pragma once
#include "Entity\System.h"
#include "Gfx\Textures.h"
#include "Gfx\Effects.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace gfx
	{
		class Camera;
		struct Context;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace shadow
	{
		enum class Quality;
	}

	namespace ecs
	{

		class ShadowSystem final :
			public ecs::System<ShadowSystem>
		{
		public:
			ShadowSystem(const gfx::Context& gfx, render::IRenderer& renderer, core::SettingModule& settings);
			~ShadowSystem(void);

			void Init(MessageManager& messages) override;
			void Update(double dt) override;
			void ReceiveMessage(const BaseMessage& message) override;

			void OnSettingChange(const core::SettingModule& settings);

		private:
			
			bool m_bRenderShadows;
			shadow::Quality m_quality;

			core::SettingModule* m_pSettings;
			const gfx::Camera* m_pCamera;
			const gfx::Textures* m_pTextures;
			const gfx::Effects* m_pEffects;

			const gfx::Context* m_pGfx;
			render::IRenderer* m_pRenderer;
		};

	}
}
