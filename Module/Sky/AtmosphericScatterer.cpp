#include "AtmosphericScatterer.h"
#include "Math\Utility.h"

namespace acl
{
	namespace sky
	{

		AtmosphericScatterer::AtmosphericScatterer(void) : m_hGgFunction(0.99f), m_inscatteringMultiplier(0.9f),
			m_betaRayMultiplier(5.0f), m_betaMieMultiplier(0.0006f), m_sunIntensity(5.0f), m_turbitity(0.8f),
			m_vSunDirection(0.0f, 1.0f, 5.0f), m_vZenith(0.0f, 0.0f, 1.0f), m_isDirty(true)
		{
			m_vSunDirection.Normalize();

			const float n = 1.0003f;
			const float N = 2.545e25f;
			const float pn = 0.035f;

			float fLambda[3];
			float fLambda2[3];
			float fLambda4[3];

			fLambda[0] = 1.0f / 650e-9f;   // red
			fLambda[1] = 1.0f / 570e-9f;   // green
			fLambda[2] = 1.0f / 475e-9f;   // blue

			for(int i = 0; i < 3; ++i)
			{
				fLambda2[i] = fLambda[i] * fLambda[i];
				fLambda4[i] = fLambda2[i] * fLambda2[i];
			}

			const math::Vector3 vLambda2(fLambda2[0], fLambda2[1], fLambda2[2]);
			const math::Vector3 vLambda4(fLambda4[0], fLambda4[1], fLambda4[2]);

			const float temp = math::PI * math::PI * (n * n - 1.0f) * (n * n - 1.0f) * (6.0f + 3.0f * pn) / (6.0f - 7.0f * pn) / N;
			const float beta = 8.0f * temp * math::PI / 3.0f;

			m_vBetaRay = beta * vLambda4;

			const float fBetaDash = temp / 2.0f;

			m_vBetaDashRay = fBetaDash * vLambda4;

			const float T = 2.0f;
			const float c = (6.544f * T - 6.51f) * 1e-17f;
			const float temp2 = 0.434f * c * (2.0f * math::PI) * (2.0f* math::PI) * 0.5f;

			m_vBetaDashMie = temp2 * vLambda2;

			const float K[3] = { 0.685f, 0.679f, 0.670f };
			const float temp3 = 0.434f * c * math::PI * (2.0f * math::PI) * (2.0f * math::PI);

			const math::Vector3 vBetaMieTemp(K[0] * fLambda2[0], K[1] * fLambda2[1], K[2] * fLambda2[2]);

			m_vBetaMie = temp3 * vBetaMieTemp;
		}

		bool AtmosphericScatterer::IsDirty(void) const
		{
			return m_isDirty;
		}

		const AtmosphericScatterer::Output& AtmosphericScatterer::GenerateOutput(void)
		{
			if(m_isDirty)
			{
				m_output.vSunDir = m_vSunDirection;

				float thetaS = m_vSunDirection.Dot(m_vZenith);
				thetaS = acos(thetaS);

				m_output.vSunColorIntensity = ComputeAttenuation(thetaS);

				const float fReflectance = 0.1f;

				const auto vBetaR = m_vBetaRay * m_betaRayMultiplier;
				const auto vBetaM = m_vBetaMie * m_betaMieMultiplier;
				const auto vBetaRM = vBetaR + vBetaM;

				m_output.vBetaDashR = m_vBetaDashRay * m_betaRayMultiplier;
				m_output.vBetaDashM = m_vBetaDashMie * m_betaMieMultiplier;
				m_output.vBetaRPlusBetaM = vBetaRM;
				m_output.vOneOverBetaRPlusBetaM = math::Vector3(1.0f / vBetaRM.x, 1.0f / vBetaRM.y, 1.0f / vBetaRM.z);
				m_output.vHGg = math::Vector3(1.0f - m_hGgFunction * m_hGgFunction, 1.0f + m_hGgFunction * m_hGgFunction, 2.0f * m_hGgFunction);
				m_output.vMultipliers = math::Vector4(m_inscatteringMultiplier, 0.113f * fReflectance, 0.113f * fReflectance, 0.08f * fReflectance);

				m_isDirty = false;
			}

			return m_output;
		}

		math::Vector4 AtmosphericScatterer::ComputeAttenuation(float Theta) const
		{
			const float fBeta = 0.04608365822050f * m_turbitity - 0.04586025928522f;
			float fTau[3];
			const float m = 1.0f / (cos(Theta) + 0.15f * pow(93.885f - Theta / math::PI * 180.0f, -1.253f));
			const float fLambda[3] = { 0.65f, 0.57f, 0.475f };

			for(int i = 0; i < 3; ++i)
			{

				const float fTauR = exp(-m * 0.008735f * pow(fLambda[i], (-4.08f)));

				const float fAlpha = 1.3f;
				const float fTauA = exp(-m * fBeta * pow(fLambda[i], -fAlpha));  // lambda should be in um


				fTau[i] = fTauR * fTauA;

			}

			return math::Vector4(fTau[0], fTau[1], fTau[2], m_sunIntensity * 100.0f);
		}

	}
}

