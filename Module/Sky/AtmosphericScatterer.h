#pragma once
#include "Math\Vector3.h"
#include "Math\Vector4.h"

namespace acl
{
	namespace sky
	{

		class AtmosphericScatterer
		{
		public:
			AtmosphericScatterer(void);

			struct Output
			{
				math::Vector3 vSunDir, vHGg, vBetaRPlusBetaM;
				math::Vector4 vMultipliers;
				math::Vector3 vBetaDashR, vBetaDashM, vOneOverBetaRPlusBetaM;
				math::Vector4 vSunColorIntensity;
			};

			bool IsDirty(void) const;
			const Output& GenerateOutput(void);

		private:

			math::Vector4 ComputeAttenuation(float theta) const;

			bool m_isDirty;
			float m_sunIntensity, m_turbitity, m_hGgFunction, m_inscatteringMultiplier, m_betaRayMultiplier, m_betaMieMultiplier;
			math::Vector3 m_vBetaRay, m_vBetaDashRay, m_vBetaMie, m_vBetaDashMie, m_vSunDirection, m_vZenith;

			Output m_output;
		};

	}
}


