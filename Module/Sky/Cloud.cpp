#include "Cloud.h"
#include <algorithm>
#include "CloudData.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\IModel.h"
#include "Gfx\InstanceBufferAccessor.h"
#include "Gfx\Camera.h"
#include "Gfx\ITexture.h"
#include "Gfx\TextureAccessors.h"
#include "Math\Vector3.h"
#include "System\Random.h"

namespace acl
{
	namespace sky
	{

		const float horizon = 20000.0f;

		const math::Vector3 vWorldCenter(512.0f, 0.0f, 512.0f);
		const float maxRadius = 8192.0f*1.5f;
		const float minCellRadius = 200.0f;
		const float maxCellRadius = 250.0f;

		Cloud::Particle::Particle(float x, float y, float z, float scale, float rotation, unsigned int type, float realY, float alpha) : vTranslation(x, y, z),
			scale(scale), rotation(rotation), type((float)type), realY(realY), alpha(alpha)
		{
		}

		Cloud::Particle::Particle(const math::Vector3& vPos, float scale, float rotation, unsigned int type, float realY, float alpha) : vTranslation(vPos),
			scale(scale), rotation(rotation), type((float)type), realY(realY), alpha(alpha)
		{
		}

		Cloud::Cell::Cell(const math::Vector3& vPos, float alpha) : vPos(vPos), alpha(alpha)
		{
		}

		void Cloud::Cell::InsertParticles(DistanceVector& vDistances, const math::Vector3& vCameraPos)
		{
			const float length = (vCameraPos - vPos).squaredLength();
			const float density = max(0.0f, 1.0f - (length / (maxRadius * maxRadius)));

			const unsigned int numParticles = (unsigned int)(vInstances.size() * density);

			for(unsigned int i = 0; i < numParticles; i++)
			{
				vDistances.push_back(&vInstances[i]);
			}
		}

		Cloud::Cloud(gfx::IInstancedMesh& mesh, gfx::ModelInstance& model, unsigned int numInstances, gfx::ITexture& texture, const CloudData& data) : m_pMesh(&mesh), m_pModel(&model),
			m_numInstances(numInstances), m_pData(&data), m_vLastPosition(-10000, -10000, -10000)
		{
			// setup model
			model.GetParent()->SetMesh(mesh);

			float halfMinScale = 0.0f;
			for(auto& particle : data.vParticles)
			{
				const float halfScale = particle.minScale / 2.0f;
				halfMinScale = max(halfScale, halfMinScale);
			}
			const float minValue = -halfMinScale + data.minHeight;
			const float maxValue = data.maxHeight + halfMinScale;
			const float constants[4] = { minValue, maxValue - minValue, 0, 0 };
			model.SetPixelConstant(0, constants, 1);

			InitCells();

			// setup colors

			auto& vTextureSize = texture.GetSize();
			const unsigned int factor = vTextureSize.y / (data.vColors.size() - 1);

			gfx::TextureAccessorR32 accessor(texture, false);
			for(unsigned int y = 0; y < (unsigned int)vTextureSize.y; y++)
			{
				const auto id = y / factor;
				const auto firstColor = data.vColors[id];
				const auto secondColor = data.vColors[id+1];

				const auto range = 1.0f - ((y % factor) / (float)factor);
				const auto value = (firstColor - secondColor) * range + secondColor;

				accessor.SetPixel(0, y, value);
			}
		}

		Cloud::~Cloud(void)
		{
			delete m_pMesh;
			delete m_pModel;
		}

		void Cloud::UpdateCamera(const gfx::Camera& camera)
		{
			const auto& vPos = camera.GetPosition();

			if((vPos - m_vLastPosition).length() >= 32.0f)
			{
				PickParticles(vPos);

				gfx::InstanceBufferAccessor<Particle> accessor(*m_pMesh);
				auto pData = accessor.GetData();
				for(auto pInstance : m_vDistances)
				{
					*pData = *pInstance;
					pData++;
				}

				m_vLastPosition = vPos;
			}
		}

		void Cloud::Draw(render::IStage& stage) const
		{
			m_pModel->Draw(stage, 0);
		}

		float getHeightModifier(float x, float z)
		{
			const float xDist = abs(x / horizon);
			const float xHeight = cos(xDist * 3.14151f / 2.0f);

			const float zDist = abs(z / horizon);
			const float zHeight = cos(zDist * 3.14151f / 2.0f);

			return xHeight * (1.0f - zDist) + zHeight * (1.0f - xDist);
		}

		math::Vector3 getDirection(void)
		{
			const float angle = rnd::fRange(0, 3.14151f*2.0f);

			math::Vector3 vDir(sin(angle), 0.0f, cos(angle));
			return vDir.normal();
		}

		float getRadiusEven(void)
		{
			const float WEIGTH = 0.25f;

			while(true)
			{
				const float radius = rnd::fMax(1.0f);

				if(radius >= rnd::fMax(WEIGTH))
					return radius;
			}
		}

		void Cloud::InitCells(void)
		{
			const float maxArea = maxRadius * maxRadius * 3.1415f;
			const float avgCellArea = pow((maxCellRadius + minCellRadius) / 2, 2) * 3.1415f;

			const unsigned int numCells = (unsigned int)((maxArea / avgCellArea) * m_pData->density);

			m_vCells.reserve(numCells);
			for(unsigned int i = 0; i < numCells; i++)
			{
				const float radius = getRadiusEven() * maxRadius;

				auto vDir = getDirection();
				vDir *= radius;

				const float realY = rnd::fRange(m_pData->minHeight, m_pData->maxHeight);
				vDir.y = realY * getHeightModifier(vDir.x, vDir.y);
				const float heightDiff = vDir.y - realY;

				const float cellAlpha = rnd::fRange(m_pData->minAlpha, m_pData->maxAlpha);

				m_vCells.emplace_back(vDir + vWorldCenter, cellAlpha);
				
				auto& cell = m_vCells.back();
				const unsigned int numParticles = rnd::iRange(m_pData->minParticles, m_pData->maxParticles);

				cell.vInstances.reserve(numParticles);
				for(unsigned int i = 0; i < numParticles; i++)
				{
					math::Vector3 vDir = getDirection();
					const float radius = getRadiusEven();
					vDir *= radius * (maxCellRadius - minCellRadius) + minCellRadius;

					const ParticleData* pParticle = nullptr;
					const float random = rnd::fMax(1.0f);
					for(auto& particle : m_pData->vParticles)
					{
						if(random <= particle.probability)
						{
							pParticle = &particle;
							break;
						}
					}

					// particle height
					vDir.y = rnd::fRange(pParticle->minHeight, pParticle->maxHeight);

					ACL_ASSERT(pParticle);

					cell.vInstances.emplace_back(cell.vPos + vDir, rnd::fRange(pParticle->minScale, pParticle->maxScale), rnd::fRange(-pParticle->maxRotation, pParticle->maxRotation), rnd::iRange(pParticle->minImage, pParticle->maxImage), heightDiff, (1.0f - radius) * cellAlpha);
				}
			}
		}

		void Cloud::PickParticles(const math::Vector3& vPosition)
		{
			// setup particles
			m_vDistances.clear();
			m_vDistances.reserve((size_t)(m_vCells.size() * (m_pData->minParticles / 2.0f)));
			
			for(auto& cell : m_vCells)
			{
				if(m_vDistances.size() >= m_numInstances)
					break;

				cell.InsertParticles(m_vDistances, vPosition);
			}
		}

	}
}

