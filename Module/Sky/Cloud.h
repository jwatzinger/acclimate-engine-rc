#pragma once
#include <vector>
#include "Math\Vector3.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
		class IInstancedMesh;
		class Camera;
		class ITexture;
	}

	namespace render
	{
		class IStage;
	}

	namespace sky
	{

		struct CloudData;

		class Cloud
		{
			struct Particle
			{
				Particle(float x, float y, float z, float scale, float rotation, unsigned int type, float realY, float alpha);
				Particle(const math::Vector3& vPos, float scale, float rotation, unsigned int type, float realY, float alpha);

				math::Vector3 vTranslation;
				float scale;
				float rotation;
				float type;
				float realY;
				float alpha;
			};

			typedef std::vector<Particle*> DistanceVector;


			struct Cell
			{
				typedef std::vector<Particle> InstanceVector;

				Cell(const math::Vector3& vPos, float alpha);

				void InsertParticles(DistanceVector& vDistances, const math::Vector3& vCameraPos);

				float alpha;
				math::Vector3 vPos;
				InstanceVector vInstances;
			};

			typedef std::vector<Cell> CellVector;
		public:
			Cloud(gfx::IInstancedMesh& mesh, gfx::ModelInstance& model, unsigned int numInstances, gfx::ITexture& texture, const CloudData& data);
			~Cloud(void);

			void UpdateCamera(const gfx::Camera& camera);

			void Draw(render::IStage& stage) const;

		private:

			void InitCells(void);
			void PickParticles(const math::Vector3& vPosition);

			unsigned int m_numInstances;

			gfx::IInstancedMesh* m_pMesh;
			gfx::ModelInstance* m_pModel;

			CellVector m_vCells;
			DistanceVector m_vDistances;
			const CloudData* m_pData;
			math::Vector3 m_vLastPosition;
		};

	}
}

