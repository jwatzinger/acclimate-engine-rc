#pragma once
#include "Core\Resources.h"

namespace acl
{
	namespace sky
	{

		struct ParticleData
		{
			ParticleData(unsigned int minImage, unsigned int maxImage, float probability, float maxRotation, float minScale, float maxScale, float minHeight, float maxHeight) : minImage(minImage), maxImage(maxImage),
			probability(probability), maxRotation(maxRotation), minScale(minScale), maxScale(maxScale), minHeight(minHeight), maxHeight(maxHeight)
			{
			}

			unsigned int minImage, maxImage;
			float probability;
			float maxRotation;
			float minScale, maxScale;
			float minHeight, maxHeight;
		};

		struct CloudData
		{
			typedef std::vector<ParticleData> ParticleVector;
			typedef std::vector<float> ColorVector;

			CloudData(float minHeight, float maxHeight, float density, unsigned int minParticles, unsigned int maxParticles, float minAlpha, float maxAlpha) : minHeight(minHeight), maxHeight(maxHeight), density(density),
				minParticles(minParticles), maxParticles(maxParticles), minAlpha(minAlpha), maxAlpha(maxAlpha)
			{
			}

			float minHeight, maxHeight, density, minAlpha, maxAlpha;
			unsigned int minParticles, maxParticles;
			ParticleVector vParticles;
			ColorVector vColors;
		};

		typedef core::Resources<std::wstring, CloudData> Clouds;

	}
}