#include "CloudLoader.h"
#include "Math\Utility.h"
#include "System\Convert.h"
#include "XML\Doc.h"

namespace acl
{
	namespace sky
	{

		void CloudLoader::Load(const std::wstring& stFilename) const
		{
			xml::Doc doc;
			doc.LoadFile(stFilename);

			if(auto pRoot = doc.Root(L"Clouds"))
			{
				if(auto pClouds = pRoot->Nodes(L"Cloud"))
				{
					for(auto pCloud : *pClouds)
					{
						const float minHeight = pCloud->Attribute(L"minHeight")->AsFloat();
						const float maxHeight = pCloud->Attribute(L"maxHeight")->AsFloat();
						const float density = pCloud->Attribute(L"density")->AsFloat();
						const unsigned int minParticles = pCloud->Attribute(L"minParticles")->AsInt();
						const unsigned int maxParticles = pCloud->Attribute(L"maxParticles")->AsInt();
						const float minAlpha = pCloud->Attribute(L"minAlpha")->AsFloat();
						const float maxAlpha = pCloud->Attribute(L"maxAlpha")->AsFloat();

						auto& cloud = *new CloudData(minHeight, maxHeight, density, minParticles, maxParticles, minAlpha, maxAlpha);

						// particles
						if(auto pParticles = pCloud->Nodes(L"Particle"))
						{
							float summedProbability = 0.0f;
							for(auto pParticle : *pParticles)
							{
								const unsigned int minImage = pParticle->Attribute(L"minImage")->AsInt();
								const unsigned int maxImage = pParticle->Attribute(L"maxImage")->AsInt();
								const float probability = pParticle->Attribute(L"probability")->AsFloat();
								const float maxRotation = math::degToRad(pParticle->Attribute(L"maxRotation")->AsFloat());
								const float minScale = pParticle->Attribute(L"minScale")->AsFloat();
								const float maxScale = pParticle->Attribute(L"maxScale")->AsFloat();
								const float minHeight = pParticle->Attribute(L"minHeight")->AsFloat();
								const float maxHeight = pParticle->Attribute(L"maxHeight")->AsFloat();

								cloud.vParticles.emplace_back(minImage, maxImage, probability, maxRotation, minScale, maxScale, minHeight, maxHeight);

								summedProbability += probability;
							}

							// process probablities;
							float lastProbability = 0.0f;
							for(auto& particle : cloud.vParticles)
							{
								particle.probability = lastProbability + particle.probability / summedProbability;
								lastProbability = particle.probability;
							}
						}

						// colors
						if(auto pColors = pCloud->FirstNode(L"Colors"))
						{
							const float intensity = pColors->Attribute(L"intensity")->AsFloat();
							if(auto pColorNodes = pColors->Nodes(L"Color"))
							{ 
								for(auto pColorNode : *pColorNodes)
								{
									cloud.vColors.emplace_back(conv::FromString<float>(pColorNode->GetValue()) * intensity);
								}
							}
						}

						AddResource(pCloud->Attribute(L"name")->GetValue(), cloud);
					}
				}
			}
		}

	}
}