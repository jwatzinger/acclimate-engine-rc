#pragma once
#include "CloudData.h"
#include "Core\ResourceSet.h"

namespace acl
{
	namespace sky
	{

		class CloudLoader :
			public core::ResourceLoader<std::wstring, CloudData>
		{
		public:

			void Load(const std::wstring& stFilename) const override;
		};

	}
}

