#include "CloudRenderSystem.h"
#include "Component.h"
#include "Cloud.h"
#include "CloudData.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\Context.h"
#include "System\Convert.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

namespace acl
{
	namespace sky
	{

		const unsigned int MAX_CLOUDS = 3;

		CloudRenderSystem::CloudRenderSystem(const gfx::Context& gfx, const render::IRenderer& renderer, const core::ResourceSet<std::wstring, CloudData>& clouds) :
			m_camera(math::Vector2(0, 0), math::Vector3(0.0f, 0.0f, 0.0f)), m_pGfx(&gfx), m_pClouds(&clouds), m_numClouds(0)
		{
			m_pStage = renderer.GetStage(L"clouds");
		}

		CloudRenderSystem::~CloudRenderSystem(void)
		{
		}

		void CloudRenderSystem::Init(ecs::MessageManager& messageManager)
		{
			messageManager.Subscribe(ecs::MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void CloudRenderSystem::Update(double dt)
		{
			const auto vEntities = m_pEntities->EntitiesWithComponents<CloudComponent>();

			for(auto& entity : vEntities)
			{
				auto pCloud = entity->GetComponent<CloudComponent>();

				if(!pCloud->pCloud)
				{
					// instanced
					const gfx::IGeometry::AttributeVector vInstancedAttributes =
					{
						{ gfx::AttributeSemantic::TEXCOORD, 0, gfx::AttributeType::FLOAT, 3, 1, 1 },
						{ gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 1, 1, 1 },
						{ gfx::AttributeSemantic::TEXCOORD, 2, gfx::AttributeType::FLOAT, 1, 1, 1 },
						{ gfx::AttributeSemantic::TEXCOORD, 3, gfx::AttributeType::FLOAT, 1, 1, 1 },
						{ gfx::AttributeSemantic::TEXCOORD, 4, gfx::AttributeType::FLOAT, 1, 1, 1 },
						{ gfx::AttributeSemantic::TEXCOORD, 5, gfx::AttributeType::FLOAT, 1, 1, 1 }
					};
					const unsigned int numInstances = 50000;
					auto pInstance = m_pGfx->load.meshes.Instanced(L"Cloud", numInstances, vInstancedAttributes);

					const auto& stId = conv::ToString(m_numClouds);
					// pass
					auto& model = m_pGfx->load.models.LoadInstance(L"", L"Clouds" + stId);
					pCloud->pCloud = new Cloud(*pInstance, model, numInstances, *m_pGfx->resources.textures[L"Colors" + stId], *m_pClouds->GetResources()[pCloud->stCloud]);
					pCloud->pCloud->UpdateCamera(m_camera);

					m_numClouds++;
					m_numClouds %= MAX_CLOUDS;
				}
				else
					pCloud->pCloud->Draw(*m_pStage);
			}
		}

		void CloudRenderSystem::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(auto pUpdate = message.Check(ecs::MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				auto pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
				m_camera = *pCamera;
				m_camera.SetClipPlanes(10.0f, 25000.0f);

				const auto vEntities = m_pEntities->EntitiesWithComponents<CloudComponent>();
				for(auto& entity : vEntities)
				{
					auto pCloud = entity->GetComponent<CloudComponent>();
					if(pCloud->pCloud)
						pCloud->pCloud->UpdateCamera(m_camera);
				}

				// fill stage parameters
				m_pStage->SetGeometryConstant(0, (float*)&m_camera.GetViewProjectionMatrix(), 4);
				m_pStage->SetGeometryConstant(4, (float*)&m_camera.GetPosition(), 1);
				m_pStage->SetGeometryConstant(5, (float*)&m_camera.GetUp(), 1);
			}
		}

	}
}

