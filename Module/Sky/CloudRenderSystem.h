#pragma once
#include "Core\ResourceSet.h"
#include "Entity\System.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace gfx
	{
		struct Context;
	}

	namespace render
	{
		class IRenderer;
		class IStage;
	}

	namespace sky
	{

		struct CloudData;

		class CloudRenderSystem :
			public ecs::System<CloudRenderSystem>
		{
		public:
			CloudRenderSystem(const gfx::Context& gfx, const render::IRenderer& renderer, const core::ResourceSet<std::wstring, CloudData>& clouds);
			~CloudRenderSystem(void);

			void Init(ecs::MessageManager& messageManager) override;
			void Update(double dt) override;
			void ReceiveMessage(const ecs::BaseMessage& message) override;

		private:

			gfx::Camera m_camera;

			unsigned int m_numClouds;
			const gfx::Context* m_pGfx;
			render::IStage* m_pStage;
			const core::ResourceSet<std::wstring, CloudData>* m_pClouds;
		};

	}
}


