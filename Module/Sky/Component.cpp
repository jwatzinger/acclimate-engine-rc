#include "Component.h"
#include "Cloud.h"

namespace acl
{
	namespace sky
	{

		CloudComponent::CloudComponent(const std::wstring& stCloud) : pCloud(nullptr),
			stCloud(stCloud)
		{
		}

		CloudComponent::~CloudComponent(void)
		{
			delete pCloud;
		}

	}
}