#pragma once
#include "Entity\Component.h"
#include "AtmosphericScatterer.h"

namespace acl
{
	namespace sky
	{

		class Cloud;

		struct Sky :
			public ecs::Component<Sky>
		{
			AtmosphericScatterer scatterer;
		};

		struct CloudComponent :
			public ecs::Component<CloudComponent>
		{
			CloudComponent(const std::wstring& stCloud);
			~CloudComponent(void);

			Cloud* pCloud;
			std::wstring stCloud;
		};

	}
}