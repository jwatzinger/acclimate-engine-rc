#include "LoadRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace sky
	{

		void LoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pShadow = entityNode.FirstNode(L"Sky"))
			{
				entity.AttachComponent<Sky>();
			}

			if(const xml::Node* pCloud = entityNode.FirstNode(L"Cloud"))
				entity.AttachComponent<CloudComponent>(pCloud->GetValue());
		}

	}
}
