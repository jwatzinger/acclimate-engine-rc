#include "Module.h"
#include "System.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "CloudRenderSystem.h"
#include "CloudLoader.h"
#include "Core\BaseContext.h"
#include "Core\ResourceManager.h"
#include "Entity\SystemManager.h"
#include "Entity\Loader.h"
#include "Entity\Saver.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\IMeshLoader.h"
#include "Render\ILoader.h"


namespace acl
{
	namespace sky
	{

		void Module::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");

			// cloud base mesh
			const gfx::IGeometry::AttributeVector vAttributes = { { gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 1 } };
			const gfx::IMeshLoader::VertexVector vVertices = { 0 };
			const gfx::IMeshLoader::IndexVector vIndices = { 0 };
			context.meshes.Custom(L"Cloud", vAttributes, vVertices, vIndices, gfx::PrimitiveType::POINT);
		}

		void Module::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// loader & set
			auto& loader = *new CloudLoader();
			auto& set = context.core.resources.AddSet<std::wstring, CloudData>(L"Clouds", L"Clouds.axm", loader);

			// systems
			m_pSystems->AddSystem<System>(context.gfx.effect, context.render.postprocess);
			m_pSystems->AddSystem<CloudRenderSystem>(context.gfx, context.render.renderer, set);

			// io
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Sky");
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Sky");
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			// io
			ecs::Saver::RegisterSubRoutine<SaveRoutine>(L"Sky");
			ecs::Loader::RegisterSubRoutine<LoadRoutine>(L"Sky");

			m_pSystems->RemoveSystem<System>();

			m_pSystems = nullptr;
		}

		void Module::OnRender(void) const
		{
			m_pSystems->UpdateSystem<System>(0.0f);
			m_pSystems->UpdateSystem<CloudRenderSystem>(0.0f);
		}

	}
}
