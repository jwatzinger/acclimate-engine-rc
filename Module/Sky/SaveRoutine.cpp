#include "SaveRoutine.h"
#include "Component.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace sky
	{

		void SaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Sky* pSky = entity.GetComponent<Sky>())
			{
				entityNode.InsertNode(L"Sky");
			}

			if(auto pCloud = entity.GetComponent<CloudComponent>())
			{
				auto& cloud = entityNode.InsertNode(L"Cloud");
				cloud.SetValue(pCloud->stCloud);
			}
		}

	}
}
