#pragma once
#include "Entity\ISubSaveRoutine.h"

namespace acl
{
	namespace sky
	{

		class SaveRoutine : 
			public ecs::ISubSaveRoutine
		{
		public:
			
			void Execute(const ecs::Entity& entity, xml::Node& entityNode) const override;
		};

	}
}

