#include "System.h"
#include "Component.h"
#include "Entity\EntityManager.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Gfx\ModelInstance.h"
#include "Gfx\Camera.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Gfx\Utility.h"
#include "Math\Rect.h"
#include "Render\Postprocessor.h"
#include "Render\PostprocessEffect.h"

namespace acl
{
	namespace sky
	{

		System::System(gfx::FullscreenEffect& fullscreen, render::Postprocessor& postprocess) : m_doGodrays(false)
		{
			m_pRays = &fullscreen.CreateInstance(L"Godrays", L"Nolight", L"Rays", 0);
			m_pRayBlur = &fullscreen.CreateInstance(L"GodRayBlur", L"Nolight", L"RayBlur", 0);
			m_pRayBlend = &fullscreen.CreateInstance(L"GodRayBlend", L"Nolight", L"final scene", 0);

			m_pAtmo = postprocess.AppendEffect(L"AtmosphericPost", render::InputTextures::SCENE_POS, true);
		}

		void System::Init(ecs::MessageManager& messageManager)
		{
			messageManager.Subscribe(ecs::MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void System::Update(double dt)
		{
			static const size_t actorId = ecs::ComponentRegistry::GetComponentId(L"Actor");
			static const size_t skyId = Sky::family();
			auto vEntities = m_pEntities->EntitiesWithComponents(actorId, skyId);

			if(m_doGodrays)
			{
				m_pRays->Draw();
				m_pRayBlur->Draw();
				m_pRayBlend->Draw();
			}

			for(auto& entity : vEntities)
			{
				auto pActor = entity->GetComponent(actorId);

				if(auto pModel = pActor->GetAttribute<gfx::ModelInstance>(L"Model"))
				{
					auto& sky = *entity->GetComponent<Sky>();

					if(sky.scatterer.IsDirty())
					{
						auto& out = sky.scatterer.GenerateOutput();
						m_vSunColorIntensity = out.vSunColorIntensity;
						m_vSunColorIntensity.w /= 50.0f;

						pModel->SetPixelConstant(0, (float*)&out.vHGg, 1);
						pModel->SetPixelConstant(1, (float*)&out.vBetaRPlusBetaM, 1);
						pModel->SetPixelConstant(2, (float*)&out.vMultipliers, 1);
						pModel->SetPixelConstant(3, (float*)&out.vBetaDashR, 1);
						pModel->SetPixelConstant(4, (float*)&out.vBetaDashM, 1);
						pModel->SetPixelConstant(5, (float*)&out.vOneOverBetaRPlusBetaM, 1);
						pModel->SetPixelConstant(6, (float*)&out.vSunColorIntensity, 1);
						pModel->SetPixelConstant(7, (float*)&out.vSunDir, 1);

						m_pAtmo->SetShaderConstant(4, (float*)&out.vSunDir, 1);
						m_pAtmo->SetShaderConstant(5, (float*)&out.vHGg, 1);
						m_pAtmo->SetShaderConstant(6, (float*)&out.vBetaRPlusBetaM, 1);
						m_pAtmo->SetShaderConstant(7, (float*)&out.vMultipliers, 1);
						m_pAtmo->SetShaderConstant(8, (float*)&out.vBetaDashR, 1);
						m_pAtmo->SetShaderConstant(9, (float*)&out.vBetaDashM, 1);
						m_pAtmo->SetShaderConstant(10, (float*)&out.vOneOverBetaRPlusBetaM, 1);
						m_pAtmo->SetShaderConstant(11, (float*)&out.vSunColorIntensity, 1);

						m_SunDir = out.vSunDir;
						m_SunDir.Normalize();
					}
				}
			}
		}

		void System::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if(auto pUpdate = message.Check(ecs::MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				auto pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");

				/**********************************
				* GODRAYS
				***********************************/
				auto& vPos = pCamera->GetPosition();
				const math::Vector3 sunpos = m_SunDir * pCamera->GetFar() + vPos;
				
				const float clip[2] = { pCamera->GetFar(), pCamera->GetNear() };
				const float sunColor[3] = { m_vSunColorIntensity.x * m_vSunColorIntensity.w, m_vSunColorIntensity.y * m_vSunColorIntensity.w, m_vSunColorIntensity.z * m_vSunColorIntensity.w };
				const math::Vector2 screen = pCamera->GetScreenSize();
				const math::Rect rViewport = math::Rect(0, 0, screen.x, screen.y);

				const math::Vector3 vSunScreenSpace = math::Vec3Project(sunpos, rViewport, pCamera->GetProjectionMatrix(), pCamera->GetViewMatrix(), math::MatIdentity());

				const math::Vector3 ssSunPos = math::Vector3(vSunScreenSpace.x / screen.x, vSunScreenSpace.y / screen.y, 0);

				// check if we even want to render rays
				const float angle = pCamera->GetDirection().Dot(m_SunDir); //determine whether sun is behind camera or not
				m_doGodrays = ssSunPos.x <= 1.7f && ssSunPos.x >= -0.7f && ssSunPos.y <= 1.7f && ssSunPos.y >= -0.7f &&  angle > 0.0f;
				if(m_doGodrays)
				{
					m_pRays->SetShaderConstant(0, (float*)&clip, 1);
					m_pRays->SetShaderConstant(1, (float*)&pCamera->GetViewMatrix(), 4);

					m_pRayBlend->SetShaderConstant(0, (float*)&ssSunPos, 1);
					m_pRayBlend->SetShaderConstant(1, (float*)&sunColor, 1);

					m_pRayBlur->SetShaderConstant(0, (float*)&ssSunPos, 1);
				}

				m_pAtmo->SetShaderConstant(0, (float*)&pCamera->GetViewMatrix(), 4);
			}
		}

	}
}

