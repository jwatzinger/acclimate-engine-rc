#pragma once
#include "Entity\System.h"
#include "Math\Vector3.h"
#include "Math\Vector4.h"

namespace acl
{
	namespace render
	{
		class Postprocessor;
		class PostprocessEffect;
	}

	namespace gfx
	{
		class FullscreenEffect;
		class FxInstance;
	}

	namespace sky
	{

		class System :
			public ecs::System<System>
		{
		public:
			System(gfx::FullscreenEffect& fullscreen, render::Postprocessor& postprocess);

			void Init(ecs::MessageManager& messageManager) override;
			void Update(double dt) override;
			void ReceiveMessage(const ecs::BaseMessage& message) override;

		private:
			gfx::FxInstance* m_pRays;
			gfx::FxInstance* m_pRayBlur;
			gfx::FxInstance* m_pRayBlend;
			bool m_doGodrays;
			math::Vector3 m_SunDir;
			math::Vector4 m_vSunColorIntensity;
			render::PostprocessEffect* m_pAtmo;
		};

	}
}


