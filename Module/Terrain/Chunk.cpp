#include "Chunk.h"
#include "Gfx\Context.h"
#include "Gfx\VertexBufferAccessor.h"
#include "Gfx\Camera.h"
#include "System\Assert.h"
#include "Math\AABB.h"
#include "algorithm"


namespace acl
{
	namespace terrain
	{

		Chunk::Chunk(const gfx::Context& gfx, const std::vector<NormalMapVector>& vNormalMaps, IndexBufferVector& indices, int dimension, int vertices, int levels, const int id)
			:m_Gfx(gfx), m_vNormalMaps(vNormalMaps), m_vIndexBuffers(indices), m_Dimension(dimension), m_Vertices(vertices), m_NumLevels(levels)
		{
			m_Drawable = false;
			m_Name =  L"TerrainChunk" + std::to_wstring(id);
			m_vLevels.reserve(4);

			for (int i = 0; i < m_NumLevels; i++)
			{
				m_vLevels.push_back(Level());
			}
			 
			gfx::IModelLoader::PassVector vPasses = { { 0, L"MipmapRender" }, { 1, L"TerrainDepth" }, { 4, L"MipmapReflect" }, { 5, L"MipmapReflect" } };
			m_pModel = &m_Gfx.load.models.LoadInstance(L"", vPasses);
			m_actualLevel = 4;
		}
		
		Chunk::~Chunk()
		{
			
		}

		bool Chunk::SetLevel(int level)
		{
			if (!m_vLevels[level].isAvailable)// reduce LOD if not available
			{
				if (++level < m_NumLevels) SetLevel(level);
			}
			else
			{
				m_actualLevel = level;
				m_pModel->GetParent()->SetMesh(*m_vLevels[level].pMesh);
				return true;
			}

			return false;				
		}
		
		bool Chunk::Load(math::Vector2 offset, const gfx::Camera& cam, const gfx::TextureAccessorL8& texAccessor, int texSize)
		{
			m_textureSize = texSize;
			m_texOffset = offset;
			for (int i = m_NumLevels - 1; i >= 0;  i--)
			{
				m_vLevels[i].isAvailable = false;
				LoadLevel(i, texAccessor);
				m_Drawable = true;
			}
			return true;
		}

		void Chunk::Relocate(math::Vector3 position, math::Vector2 offset, const gfx::Camera& cam, const gfx::TextureAccessorL8& texAccessor)
		{
			m_Position = position;
			m_Drawable = false;

			
		}

		void Chunk::Update(const gfx::Camera& camera)
		{
			math::Vector3 cameraPosition = camera.GetPosition();
			float l = 0.f;
			int maxLevel = 0;
			if (m_Drawable)
			{
				const float distThreshold[4] = { m_Dimension, 2 * m_Dimension, 3 * m_Dimension, 8 * m_Dimension };
				l = (GetCenterPosition() - cameraPosition).length();
				for (int i = 0; i < m_NumLevels; i++)
				{
					if (l < distThreshold[i])
					{
						maxLevel = i;
						break;
					}
				}

				if (maxLevel != m_actualLevel)
					SetLevel(maxLevel);

				if (m_vLevels[m_actualLevel].isAvailable)
				{
					int idx = (m_actualLevel > 2)? 0 : CheckNeighbours(camera);
					if (&m_vLevels[m_actualLevel].pMesh->GetIndexBuffer() != m_vIndexBuffers[m_actualLevel][idx])
					{
						m_vLevels[m_actualLevel].pMesh->SetVertexCount(m_vIndexBuffers[m_actualLevel][idx]->GetSize(), 0);
						m_vLevels[m_actualLevel].pMesh->SetIndexBuffer(*m_vIndexBuffers[m_actualLevel][idx]);
					}					
				}
			}
		}

		void Chunk::Render(const render::IStage& stage, size_t pass)
		{
			if (m_Drawable)
			{
				m_pModel->Draw(stage, pass);
			}				
		}

		gfx::ModelInstance* Chunk::GetModel()
		{
			return m_pModel;
		}

		void Chunk::SetPosition(math::Vector3 position)
		{
			m_Position = position;
		}

		math::Vector3 Chunk::GetPosition()
		{
			return m_Position;
		}
		
		gfx::IMeshLoader::VertexVector Chunk::CreateVertices(int level, int size,  int factor, const gfx::TextureAccessorL8& texAccessor)
		{
			const unsigned int numVertices = size * size;
			
			gfx::IMeshLoader::VertexVector vVertices;

			vVertices.reserve(numVertices);

			const unsigned int sizeMinusOne = size - 1;
			float bias = sizeMinusOne / float(m_Dimension-1);

			int texelX = 0, texelY = 0;

			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					// position
					vVertices.push_back(m_Position.x + (static_cast<float>(j)) / bias);

					texelX = min(m_texOffset.y + j * factor, m_textureSize - 1);
					texelY = min(m_texOffset.x + i * factor, m_textureSize - 1);
					vVertices.push_back(texAccessor.GetPixel(texelX, texelY) * 0.2f);

					vVertices.push_back(m_Position.z + (static_cast<float>(i)) / bias);

					// uv
					vVertices.push_back(static_cast<float>(i) / bias);
					vVertices.push_back(static_cast<float>(j) / bias);

					//uv for weights
					const float v = texelY / ((float)m_textureSize);
					const float u = texelX / ((float)m_textureSize);
					
					ACL_ASSERT(u <= 1 && v <= 1);
					vVertices.push_back(u);
					vVertices.push_back(v);

					// normal
					math::Vector2 normalOffset;
					normalOffset.x = m_texOffset.x / factor;
					normalOffset.y = m_texOffset.y / factor;

					texelX = min(normalOffset.y + j, m_vNormalMaps[level].size() - 1);
					texelY = min(normalOffset.x + i, m_vNormalMaps[level].size() - 1);
					auto& vNormal = m_vNormalMaps[level][texelX][texelY];
					
					vVertices.push_back(vNormal.x);
					vVertices.push_back(vNormal.y);
					vVertices.push_back(vNormal.z);
				}
			}
	
			return vVertices;
		}

		void Chunk::LoadLevel(int level, const gfx::TextureAccessorL8& texAccessor)
		{
			const gfx::IMeshLoader& meshLoader = m_Gfx.load.meshes;
			const gfx::IModelLoader& modelLoader = m_Gfx.load.models;

			gfx::IGeometry::AttributeVector vAttributes;
			vAttributes.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 3); // position
			vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 0, gfx::AttributeType::FLOAT, 2); // uv
			vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 2); // uv for weights
			vAttributes.emplace_back(gfx::AttributeSemantic::TEXCOORD, 2, gfx::AttributeType::FLOAT, 3); // normal

			gfx::IMeshLoader::VertexVector vLevelVertices;
			gfx::IMeshLoader::IndexVector vLevelIndices;
			gfx::IMesh* pMesh;

			int offset = 2;
			std::wstring lvlName = m_Name + L"Level";
			
			int lvlSize = int(m_Vertices / (pow(offset, level)));
			if (lvlSize % 2 == 0) lvlSize++;

			m_vLevels[level].factor = (m_Vertices-1) / (lvlSize-1);
			vLevelVertices = CreateVertices(level, lvlSize, m_vLevels[level].factor, texAccessor);

			pMesh = &meshLoader.Custom(lvlName + std::to_wstring(level), vAttributes, vLevelVertices, *m_vIndexBuffers[level][0], gfx::PrimitiveType::TRIANGLE);
			m_vLevels[level].pMesh = pMesh;
			m_vLevels[level].size = lvlSize;
			m_vLevels[level].isAvailable = true;		
		}

		gfx::IMeshLoader::IndexVector Chunk::CreateIndices(int size)
		{
			const unsigned int numIndices = (size - 1) * (size - 1) * 6;
			gfx::IMeshLoader::IndexVector vIndices;
			vIndices.reserve(numIndices);

			unsigned int index = 0;
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					vIndices.push_back(size * (j + 1) + i + 1);
					vIndices.push_back(size *  j + i);
					vIndices.push_back(size * (j + 1) + i);
					vIndices.push_back(size * (j + 1) + i + 1);
					vIndices.push_back(size *  j + i + 1);
					vIndices.push_back(size *  j + i);
				}
			}

			return vIndices;
		}

		math::Vector3 Chunk::GetCenterPosition()
		{
			return m_Position + math::Vector3((m_Dimension -1) / 2.f, 0, (m_Dimension - 1) / 2.f);
		}

		int Chunk::GetLevel()
		{
			return m_actualLevel;
		}

		void Chunk::SetNeighbours(const std::vector<Chunk*> neighbours)
		{
			m_vNeighbours = neighbours;
		}

		int Chunk::CheckNeighbours(const gfx::Camera& camera)
		{
			enum lodtype{ normal, left, right, lower, lowerleft, lowerright, top, topleft, topright };
			const float realChunkDimension = (m_Dimension - 1);
			const math::Vector3 size(realChunkDimension, realChunkDimension, realChunkDimension);
			const math::Frustum frustum = camera.GetFrustum();
			int idx = 0;

			std::vector<Chunk*> culledNeighbours;
			for (auto chunk : m_vNeighbours)
			{
				math::AABB boundingBox(chunk->GetCenterPosition(), size);
				if (chunk->m_actualLevel > m_actualLevel && frustum.TestVolume(boundingBox))
				{
					culledNeighbours.push_back(chunk);
				}
					
			}

			math::Vector3 original = GetCenterPosition();
			auto compChunkDistance = [original](const void* ch1, const void* ch2) -> int 
			{
				Chunk* chunk1 = (Chunk*)ch1;
				Chunk* chunk2 = (Chunk*)ch2;
				
				return (chunk1->GetCenterPosition() - original).length() > (chunk2->GetCenterPosition() - original).length();
			};

			std::sort(culledNeighbours.begin(), culledNeighbours.end(), compChunkDistance);
			for (int i = 0; i < min(culledNeighbours.size(), 2); i++)
			{
				if (culledNeighbours[i]->GetCenterPosition().x < GetCenterPosition().x && idx != lodtype::right) idx += lodtype::left;
				if (culledNeighbours[i]->GetCenterPosition().x > GetCenterPosition().x && idx != lodtype::left) idx += lodtype::right;
				if (culledNeighbours[i]->GetCenterPosition().z < GetCenterPosition().z && idx != lodtype::top) idx += lodtype::lower;
				if (culledNeighbours[i]->GetCenterPosition().z > GetCenterPosition().z && idx != lodtype::lower) idx += lodtype::top;
			}

			return idx;
		}


	} // terrain
} // acl
