#pragma once
#include "Gfx.h"
#include "Gfx\TextureAccessors.h"
#include "Math\Vector.h"
#include "Math\Vector2f.h"
#include "Math\Vector3.h"
#include "Math\Vector4.h"

namespace acl
{

	namespace gfx
	{
		class Camera;
		struct Context;
	}

	namespace physics
	{
		class TerrainShape;
		struct Material;
		class World;
	}

	namespace terrain
	{

		class Chunk
		{
		public:
			struct Level
			{
				int size;
				int factor;
				gfx::IMesh* pMesh;
				bool isAvailable = false;
			};
			
			typedef std::vector<Level> MeshLevelVector;
			typedef std::vector<std::vector<math::Vector3>> NormalMapVector;
			typedef std::vector<std::vector<gfx::IIndexBuffer*>> IndexBufferVector;

			Chunk(const gfx::Context& gfx, const std::vector<NormalMapVector>& vNormalMap, IndexBufferVector& indices, int dimension, int numVertices, int levels, const int id);
			~Chunk();
			bool SetLevel(int level);
			int GetLevel();
			bool Load(math::Vector2 offset, const gfx::Camera& cam, const gfx::TextureAccessorL8& texAccessor, int texSize);
			void Relocate(math::Vector3 position, math::Vector2 offset, const gfx::Camera& cam, const gfx::TextureAccessorL8& texAccessor);
			void Update(const gfx::Camera& camera);
			void Render(const render::IStage& stage, size_t pass);
			gfx::ModelInstance* GetModel();
			void SetPosition(math::Vector3 position);
			void SetNeighbours(const std::vector<Chunk*> neighbours);
			int CheckNeighbours(const gfx::Camera& camera);
			math::Vector3 GetPosition();
			math::Vector3 GetCenterPosition();

		private:
			struct Vertex{
				float x, y, z, u, v, nx, ny, nz;
			};
			const int m_NumLevels = 4;
			const int m_Vertices;
			const gfx::Context& m_Gfx;
			const int m_Dimension;
	
			bool m_Drawable;
			int m_actualLevel;
			int m_textureSize;

			MeshLevelVector m_vLevels;
			math::Vector3 m_Position;
			math::Vector2 m_texOffset;
			gfx::ModelInstance* m_pModel;
			std::wstring m_Name;
			physics::TerrainShape* m_pTerrainShape;
			std::vector<Chunk*> m_vNeighbours;
			const std::vector<NormalMapVector>& m_vNormalMaps;
			IndexBufferVector& m_vIndexBuffers;
			gfx::IMeshLoader::IndexVector Chunk::CreateIndices(int size);
			gfx::IMeshLoader::VertexVector CreateVertices(int level, int size, int factor, const gfx::TextureAccessorL8& texAccessor);
			void LoadLevel(int level, const gfx::TextureAccessorL8& texAccessor);
		};

	}
}