#include "Components.h"

namespace acl
{
	namespace ecs
	{

		Terrain::Terrain(const std::wstring& stFilename): stFilename(stFilename), bDirty(true), pTerrain(nullptr) 
		{
		}

		Mipmap::Mipmap(void) : pTerrain(nullptr)
		{
		}

	}
}