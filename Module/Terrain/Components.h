#pragma once
#include <string>
#include "Entity\Component.h"

namespace acl
{
	namespace terrain
	{
		class Terrain;
		class MipmapTerrain;
	}

	namespace ecs
	{
		
		struct Terrain : ecs::Component<Terrain>
		{
			Terrain(const std::wstring& stFilename);

			bool bDirty;
			std::wstring stFilename;
			terrain::Terrain* pTerrain;
		};
		
		struct Mipmap : ecs::Component<Mipmap>
		{
			Mipmap(void);

			terrain::MipmapTerrain* pTerrain;
		};
	}
}