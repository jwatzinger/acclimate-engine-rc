#include "Export.h"
#include "TerrainModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::TerrainModule();
}