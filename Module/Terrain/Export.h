#pragma once
#include "Core\Dll.h"

namespace acl
{
	namespace core
	{
		class IModule;
	}
}

using namespace acl;

extern "C" __declspec(dllexport) core::IModule& CreateModule(void);