#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void TerrainLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pTerrain = entityNode.FirstNode(L"Terrain"))
				entity.AttachComponent<Terrain>(pTerrain->Attribute(L"name")->GetValue());

			if(const xml::Node* pClipmap = entityNode.FirstNode(L"Mipmap"))
				entity.AttachComponent<Mipmap>();
		}

	}
}
