#include "Loader.h"
#include "MipmapTerrain.h"
#include "Core\Resources.h"
#include "Gfx\IModel.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\TextureAccessors.h"
#include "Math\HeightField.h"
#include "Math\Vector.h"
#include "Math\Utility.h"
#include "System\Random.h"
#include "System\Exception.h"
#include "System\Log.h"
#include "XML\Doc.h"

namespace acl
{
    namespace terrain
    {

        Loader::Loader(const gfx::Textures& textures, const gfx::IMeshLoader& meshLoader, const gfx::Models& models):
			m_textures(textures), m_meshLoader(meshLoader), m_models(models)
        {
        }

        void Loader::Load(const std::wstring& stFilename) const
        {
            xml::Doc doc;
            doc.LoadFile(stFilename.c_str());

			
            if(const xml::Node* pTerrains = doc.Root(L"Terrains"))
            {
                if(const xml::Node::NodeVector* pTerrain = pTerrains->Nodes(L"Terrain"))
                {
                    for(const xml::Node* terrain : *pTerrain) 
                    {
                        if(const xml::Attribute* pName = terrain->Attribute(L"name"))
                        {
                            const std::wstring& stName = pName->GetValue();
                            std::wstring stModel = L"";

							const std::wstring& stHeightmap = terrain->Attribute(L"texture")->GetValue();
							gfx::ITexture* pTexture = m_textures[stHeightmap];
							if(!pTexture)
							{
								sys::log->Out(sys::LogModule::PLUGIN, sys::LogType::ERR, "missing heightmap texture", stHeightmap, "for terrain", stName);
								throw fileException();
							}

                            const unsigned int size = pTexture->GetSize().x;

							gfx::TextureAccessorL8 accessor(*pTexture, true);

                            if(const xml::Attribute* pModel = terrain->Attribute(L"model"))
                                stModel = pModel->GetValue();


							gfx::IModel* pModel = m_models[stModel];
							if(!pModel)
							{
								sys::log->Out(sys::LogModule::PLUGIN, sys::LogType::ERR, "missing model", stModel, "for terrain", stName);
								throw fileException();
							}

							/*Terrain* pTerrain = new Terrain(field, *pModel);

                            AddResource(stName, *pTerrain);*/
                        }
                    }
                }
            }

        }

    }
}