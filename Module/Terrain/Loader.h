#pragma once
#include "Core\ResourceSet.h"
#include "Gfx\Textures.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace gfx
	{
		class IMeshLoader;
	}

    namespace terrain
    {
		class MipmapTerrain;
        /// Loads terrain from a serialized XML-file
		/** This class is used to read in terrains and store them to
		*   a terrain resource cache. */
        class Loader :
			public core::ResourceLoader<std::wstring, MipmapTerrain>
        {
        public:
            Loader(const gfx::Textures& textures, const gfx::IMeshLoader& loader, const gfx::Models& models);

            /** Loads the terrain from a file.
             *  This method loads a bunch of single terrain patches from a XML-file.
             *  These terrains are added with their specific names to the previously
             *  stored terrain cache. Any graphical resources required by the terrains
             *  need to be loaded before calling this.
             *  
             *  @param[in] stFilename The path and name of the file that should be read from.
             */
            void Load(const std::wstring& stFilename) const;

        private:

            const gfx::Textures& m_textures; ///< const reference to the texture cache the heightmaps will be read from
			const gfx::IMeshLoader& m_meshLoader; ///< const reference to the mesh loader the instanced grass meshes will be generated from.
			const gfx::Models& m_models; ///< const reference to the model cache from which the terrains models will be cloned
        };

    }
}

