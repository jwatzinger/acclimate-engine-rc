#pragma once
#include <string>
#include "Entity\Message.h"

namespace acl
{
	namespace ecs
	{
		
		struct LoadTerrains :
			public Message<LoadTerrains>
		{
			LoadTerrains(const std::wstring& stFilename): stFilename(stFilename) {}

			const std::wstring stFilename;
		};
		
	}
}