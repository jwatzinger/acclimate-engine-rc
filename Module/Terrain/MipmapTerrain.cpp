#include "MipmapTerrain.h"
#include "Chunk.h"

#include "Gfx\Context.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\Camera.h"
#include "Gfx\TextureAccessors.h"
#include "Gfx\VertexBufferAccessor.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"
#include "Physics\TerrainShape.h"
#include "Physics\World.h"
#include "Physics\Material.h"


namespace acl {
	namespace terrain {

		MipmapTerrain::MipmapTerrain(unsigned int size, unsigned chunks, const gfx::Context& gfx, render::IRenderer& renderer, const gfx::Camera& camera, physics::World& world) :
			m_Gfx(gfx), m_NumChunks(chunks*chunks), m_quadTree(math::Vector2f(0, 0), 4096, 64), m_pWorld(&world), m_Material(0.5f, 0.05f, 0.2f, 0.2f)
		{
			m_pStage = renderer.GetStage(L"world");

			if (size % 2 == 0) size += 1;
			m_Level0Vertices = size;
			Load(size, camera);
		}

		MipmapTerrain::MipmapTerrain(MipmapTerrain& copy) :m_NumChunks(copy.m_NumChunks), m_Gfx(copy.m_Gfx), m_Material(m_Material), m_Level0Vertices(copy.m_Level0Vertices), m_pStage(copy.m_pStage)
			, m_quadTree(math::Vector2f(0, 0), 4096, 64)
		{
			
		}

		MipmapTerrain::~MipmapTerrain(void)
		{
			if (m_pTerrainShape) delete m_pTerrainShape;
			for (unsigned int i = 0; i < m_NumChunks; i++)
			{
				delete m_vAllChunks[i];
			}
		}

		// TODO: Decide which Chunk gets relocated
		void MipmapTerrain::Update(const gfx::Camera& camera) 
		{
			m_Mutex.lock();
			m_vChunks = m_vAllChunks;
			Culling(m_vChunks, camera);
			for (auto& chunk : m_vChunks)
			{
				chunk->Update(camera);
			}
			m_Mutex.unlock();
		}

		void MipmapTerrain::Render(void)
		{
			m_Mutex.lock();
			for (auto& chunk : m_vChunks)
			{
				chunk->Render(*m_pStage, 0);				
			}
			m_Mutex.unlock();
		}		

		void MipmapTerrain::Render(const render::IStage& stage, size_t pass)
		{
			m_Mutex.lock();
			for (auto& chunk : m_vChunks)
			{
				chunk->Render(stage, pass);
			}
			m_Mutex.unlock();
		}

		bool MipmapTerrain::Load(const int size, const gfx::Camera& cam)
		{
			CreateRigidBody(size);
			
			loadThread = std::thread ([=](){
				int tsize = size;
				int id = 0;
				const int chunks = (int)sqrt(m_NumChunks);

				gfx::ITexture* tex = m_Gfx.resources.textures.Get(L"TerrainHeightmap");
				gfx::TextureAccessorL8* texAccessor = new gfx::TextureAccessorL8(*tex, true);

				for (int i = 0; i < m_numLevels; i++)
				{
					const int offset = 2;

					int lvlSize = int(m_Level0Vertices / (pow(offset, i)));
					if (lvlSize % 2 == 0)
						lvlSize++;

					m_vNormalMaps.push_back(CreateNormalMap(lvlSize, tex->GetSize().x, *texAccessor));
					m_vIndexBuffers.push_back(CreateIndices(lvlSize, i));
				}				

				for (int i = 0; i < chunks; i++)
				{
					for (int j = 0; j < chunks; j++)
					{
						Chunk* newChunk = new Chunk(m_Gfx, m_vNormalMaps, m_vIndexBuffers, m_chunkDimension, tsize, m_numLevels, id++);
						
						const float realChunkDimension = (m_chunkDimension - 1);
						float z = realChunkDimension * i;
						float x = realChunkDimension * j;

						math::Vector3 pos(x, 0, z);
						newChunk->SetPosition(pos);

						const math::Rectf rect(x, z, realChunkDimension, realChunkDimension);

						m_Mutex.lock();
						m_vAllChunks.push_back(newChunk);
						m_quadTree.Insert(newChunk, rect);
						m_Mutex.unlock();

						newChunk->Load(math::Vector2((tsize-1) * i, (tsize-1) * j), cam, *texAccessor, tex->GetSize().x);
					}

					
				}
				math::Vector2 dist(m_chunkDimension, m_chunkDimension);
				for (auto chunk : m_vAllChunks)
				{
					std::vector<Chunk*> neighbours;
					for (auto chunk2 : m_vAllChunks)
					{
						if (chunk == chunk2) continue;
						if (((chunk->GetCenterPosition() - chunk2->GetCenterPosition()).length() <= dist.length())
							&& (chunk->GetCenterPosition().x == chunk2->GetCenterPosition().x
							|| chunk->GetCenterPosition().z == chunk2->GetCenterPosition().z))
						{
							neighbours.push_back(chunk2);
						}
					}
					chunk->SetNeighbours(neighbours);
				}
				sys::log->Out(sys::LogModule::CUSTOM, sys::LogType::INFO, "FINISHED LOADING CHUNKS");
				delete texAccessor;
			});
			//loadThread.join();

			return true;
		}

		void MipmapTerrain::CreateRigidBody(int size)
		{
			gfx::ITexture* tex = m_Gfx.resources.textures.Get(L"TerrainHeightmap");
			gfx::TextureAccessorL8* texAccessor = new gfx::TextureAccessorL8(*tex, true);

			int texSize = tex->GetSize().x;

			physics::TerrainShape::HeightMapVector vHeights;
			std::vector<float>  vColumnHeight;
			vHeights.reserve(texSize);

			int texelX = 0, texelY = 0;
			for (int i = 0; i < texSize; i++)
			{
				vColumnHeight.clear();
				vColumnHeight.reserve(texSize);
				for (int j = 0; j < texSize; j++)
				{
					texelX = min(i, texSize - 1);
					texelY = min(j, texSize - 1);
					vColumnHeight.push_back(texAccessor->GetPixel(texelX, texelY) * 0.2f);
				}
				vHeights.push_back(vColumnHeight);
			}
			
			float scalingfactor = m_chunkDimension / size;
			m_pTerrainShape = new physics::TerrainShape(vHeights, scalingfactor, math::Vector3(0, 0, 0));
			m_pRigidBody = &m_pWorld->AddRigidBody(m_Material, *m_pTerrainShape, physics::Transform(math::Vector3(0, 0, 0), math::Quaternion()), physics::MotionType::STATIC, physics::CollisionType::STANDARD, 0.0f);
			delete texAccessor;
		}

		std::vector<gfx::IIndexBuffer*> MipmapTerrain::CreateIndices(int size, int level)
		{
			std::vector<gfx::IIndexBuffer*> vIndexBuffers;

			const unsigned int numIndices = (size) * (size) * 6;
			gfx::IMeshLoader::IndexVector vIndices;
			
			vIndices.reserve(numIndices);

			// all indices normal
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					vIndices.push_back(size * (j + 1) + i + 1);
					vIndices.push_back(size *  j + i);
					vIndices.push_back(size * (j + 1) + i);
					vIndices.push_back(size * (j + 1) + i + 1);
					vIndices.push_back(size *  j + i + 1);
					vIndices.push_back(size *  j + i);
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));

			vIndices.clear();

			// return already if it is the lowest lod level
			if (level == m_numLevels - 1) return vIndexBuffers;

			// on left side
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (j % 2 == 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 2) + i);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// on right side
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != size - 2)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (j % 2 == 0)
					{
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * j + i + 1);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// indices on lower side decreased in lod
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (j != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i%2 == 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i + 1);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// left & down

			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != 0 && j != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i == 0 && j % 2 == 0 && j != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 2) + i);
					}
					else if (j == 0 && i % 2 == 0 && i != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i + 1);
					}
					else if (j == 0 && i == 0)
					{
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i + 1);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 2) + i);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// right & lower
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != size - 2 && j != 0)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i == size - 2 && j % 2 == 0 && j != 0)
					{
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * j + i + 1);
					}
					else if (j == 0 && i % 2 == 0 && i < size - 3)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i + 1);
					}

					if (j == 0 && i == size - 3)
					{
						vIndices.push_back(size * (j + 2) + i + 2);
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();


			// on top side
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (j != size - 2)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i%2 == 0)
					{
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);
						
						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * (j + 1) + i + 2);		

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 1);
					}
				}
			}
			
			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// left & top
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != 0 && j != size - 2)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i == 0 && j % 2 == 0 && j < size - 3)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);

						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 2) + i);
					}
					else if (j == size - 2 && i % 2 == 0 && i > 0)
					{
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * (j + 1) + i + 2);

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 1);
					}
					else if (j == size - 3 && i == 0)
					{
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						
						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);
						
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			// right & top
			for (unsigned int j = 0; j < (unsigned int)size - 1; j++)
			{
				for (unsigned int i = 0; i < (unsigned int)size - 1; i++)
				{
					if (i != size - 2 && j != size - 2)
					{
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i);
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size *  j + i + 1);
						vIndices.push_back(size *  j + i);
					}
					else if (i == size - 2 && j % 2 == 0 && j < size - 3)
					{
						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);

						vIndices.push_back(size * (j + 2) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 2) + i + 1);
						vIndices.push_back(size * j + i + 1);
					}
					else if (j == size - 2 && i % 2 == 0 && i < size - 3)
					{
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * j + i);
						vIndices.push_back(size * (j + 1) + i);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * j + i + 1);
						vIndices.push_back(size * (j + 1) + i + 2);

						vIndices.push_back(size * (j + 1) + i);
						vIndices.push_back(size * (j + 1) + i + 2);
						vIndices.push_back(size * j + i + 1);
					}

					if (j == size - 3 && i == size - 3)
					{
						vIndices.push_back(size * (j + 2) + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * (j + 2) + i);

						vIndices.push_back(size * j + i + 2);
						vIndices.push_back(size * (j + 1) + i + 1);
						vIndices.push_back(size * (j + 2) + i + 2);
					}
				}
			}

			vIndexBuffers.push_back(&m_Gfx.load.meshes.Indexbuffer(vIndices));
			vIndices.clear();

			return vIndexBuffers;
		}

		MipmapTerrain::NormalMapVector MipmapTerrain::CreateNormalMap(int size, int texSize, const gfx::TextureAccessorL8& texAccessor)
		{
 			NormalMapVector vNormalMap;
			const int chunks = (int)sqrt(m_NumChunks);
			const int realSize = (size - 1) * chunks;
			const int sizeMinusOne = realSize - 1;

			std::vector<math::Vector3> vVertices;
			vVertices.reserve(sizeMinusOne * sizeMinusOne);

			float bias = size / float(m_chunkDimension - 1);

			const int level0MinusOne = m_Level0Vertices - 1;
			const int realChunkDimension = m_chunkDimension - 1;
			const int factor = level0MinusOne / (size - 1);

			for (int i = 0; i < realSize; i++)
			{
				for (int j = 0; j < realSize; j++)
				{
					float x = i / bias;
					float y = j / bias;
					// position
					vVertices.emplace_back(y, texAccessor.GetPixel(i * factor, j * factor) * 0.2f, x);
				}
			}
			
			// normals
			std::vector<math::Vector3> vNormals;
			vNormals.reserve(sizeMinusOne * sizeMinusOne);
			vNormalMap.reserve(sizeMinusOne);

			for (int j = 0; j < sizeMinusOne; j++)
			{
				for (int i = 0; i < sizeMinusOne; i++)
				{
					const size_t index1 = (j * realSize) + i;
					const size_t index2 = (j * realSize) + (i + 1);
					const size_t index3 = ((j + 1) * realSize) + i;
					
					// Get three vertices from the face.
					const math::Vector3& vVertex1 = vVertices[index1];
					const math::Vector3& vVertex2 = vVertices[index2];
					const math::Vector3& vVertex3 = vVertices[index3];

					// Calculate the two vectors for this face.
					const math::Vector3 vDist1 = vVertex1 - vVertex3;
					const math::Vector3 vDist2 = vVertex3 - vVertex2;

					// Calculate the cross product of those two vectors to get the un-normalized value for this face normal.
					vNormals.push_back(vDist1.Cross(vDist2).normal());
				}
			}

			// Now go through all the vertices and take an average of each face normal 	
			// that the vertex touches to get the averaged normal for that vertex.
			for (int j = 0; j < realSize; j++)
			{
				std::vector<math::Vector3> sumNormals;
				for (int i = 0; i < realSize; i++)
				{
					// Initialize the sum.
					math::Vector3 vSum;

					// Initialize the count.
					size_t count = 0;

					// Bottom left face.
					if (((i - 1) >= 0) && ((j - 1) >= 0))
					{
						const size_t index = ((j - 1) * sizeMinusOne) + (i - 1);

						vSum += vNormals[index];
						count++;
					}

					// Bottom right face.
					if ((i < sizeMinusOne) && ((j - 1) >= 0))
					{
						const size_t index = ((j - 1) * sizeMinusOne) + i;

						vSum += vNormals[index];
						count++;
					}

					// Upper left face.
					if (((i - 1) >= 0) && (j < sizeMinusOne))
					{
						const size_t index = (j * sizeMinusOne) + (i - 1);

						vSum += vNormals[index];
						count++;
					}

					// Upper right face.
					if ((i < sizeMinusOne) && (j < sizeMinusOne))
					{
						const size_t index = (j * sizeMinusOne) + i;

						vSum += vNormals[index];
						count++;
					}

					// Take the average of the faces touching this vertex.
					vSum /= (float)count;
					vSum.Normalize();

					sumNormals.push_back(vSum);
				}

				vNormalMap.push_back(sumNormals);

		}
			return vNormalMap;
		}

		void MipmapTerrain::Culling(ChunkVector& data, const gfx::Camera& camera)
		{
			ChunkVector out;
			math::Vector3 size(m_chunkDimension / 2.0, 1000.0, m_chunkDimension / 2.0);
			
			for (auto& chunk : data){
				math::AABB volume = math::AABB(chunk->GetCenterPosition(), size);
				if (camera.GetFrustum().TestVolume(volume)){
					out.push_back(chunk);
				}
			}

			data = out;
		}


	} // terrain
} // acl




