#pragma once
#include <vector>
#include "Gfx\Textures.h"
#include "Gfx\TextureAccessors.h"
#include "Math\Vector.h"
#include "Math\Vector2f.h"
#include "Math\Vector3.h"
#include "Math\Vector4.h"
#include "Math\AABB.h"
#include "Math\Rect.h"
#include "Physics\Materials.h"
#include "gfx/IMeshLoader.h"
#include "Utility\Quadtree.h"
#include <thread>
#include <mutex>

namespace acl
{
	namespace gfx
	{
		class IMesh;
		class IInstancedMesh;
		class IModel;
		class Camera;
		class IModelLoader;
		struct Context;
		struct LoadContext;
		class IVertexBuffer;
	}

	namespace math
	{
		class Plane;
	}

	namespace physics
	{
		class World;
		class RigidBody;
		class TerrainShape;
	}

	namespace render
	{
		class IRenderer;
		class IStage;
	}

	namespace terrain
	{	
		class Chunk;
		
		class MipmapTerrain
		{
			
			
			typedef std::vector<Chunk*> ChunkVector;
			typedef std::vector<std::vector<gfx::IIndexBuffer*>> IndexBufferVector;
		public:
			MipmapTerrain(unsigned int size, unsigned chunks, const gfx::Context& gfx, render::IRenderer& renderer, const gfx::Camera& cam, physics::World& world);
			MipmapTerrain(MipmapTerrain& copy);
			~MipmapTerrain(void);

			void Update(const gfx::Camera& camera);

			void Render(void);
			void Render(const render::IStage& stage, size_t pass);
			typedef std::vector<std::vector<math::Vector3>> NormalMapVector;
		private:			
			bool Load(int size, const gfx::Camera& cam);
			void CreateRigidBody(int size);
			void Culling(ChunkVector& data, const gfx::Camera& camera);
			std::vector<gfx::IIndexBuffer*> CreateIndices(int size, int level);
			NormalMapVector CreateNormalMap(int size, int texSize, const gfx::TextureAccessorL8& texAccessor);
			std::thread loadThread;
			render::IStage* m_pStage;
			ChunkVector m_vChunks;
			ChunkVector m_vAllChunks;
			util::Quadtree<Chunk*> m_quadTree;
			std::vector<NormalMapVector> m_vNormalMaps;
			IndexBufferVector m_vIndexBuffers;
			const unsigned m_NumChunks;
			const float m_chunkDimension = 129;
			const int m_numLevels = 4;
			int m_Level0Vertices;
			std::mutex m_Mutex;
			physics::World* m_pWorld;
			physics::RigidBody* m_pRigidBody;
			physics::TerrainShape* m_pTerrainShape;
			physics::Material m_Material;
			const gfx::Context& m_Gfx;
		};

	}
}