#include "Queries.h"

namespace acl
{
	namespace ecs
	{

		PlacePositionQuery::PlacePositionQuery(const math::Ray& ray): ray(ray)
		{
		}

		const void* PlacePositionQuery::GetResult(const std::wstring& stName, const std::type_info** type) const
		{
			if(stName == L"Position")
			{
				*type = &typeid(math::Vector3);
				return (void*)&vPosition;
			}

			return nullptr;
		}

	}
}