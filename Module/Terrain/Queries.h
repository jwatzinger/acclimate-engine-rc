#pragma once
#include "Entity\Query.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace math
	{
		struct Ray;
	}

	namespace ecs
	{
        struct PlacePositionQuery : 
			public Query<PlacePositionQuery>
        {
            PlacePositionQuery(const math::Ray& ray);

			const void* GetResult(const std::wstring& stName, const std::type_info** type) const;

            const math::Ray& ray;
            math::Vector3 vPosition;
        };
	}
}