#include "RegisterScript.h"
#include "Components.h"
#include "Queries.h"
#include "Entity\Entity.h"
#include "Entity\MessageManager.h"
#include "Script\Core.h"

namespace acl
{
	namespace terrain
	{
		namespace detail
		{
			ecs::MessageManager* _pMessages = nullptr;
		}

		/*******************************************
		* Terrain component
		*******************************************/

		ecs::Terrain* AttachTerrain(ecs::EntityHandle* entity, const std::wstring& stTerrain)
		{
			return (*entity)->AttachComponent<ecs::Terrain>(stTerrain);
		}

		void RemoveTerrain(ecs::EntityHandle* entity)
		{
			(*entity)->DetachComponent<ecs::Terrain>();
		}

		ecs::Terrain* GetTerrain(const ecs::EntityHandle* entity)
		{
			return (*entity)->GetComponent<ecs::Terrain>();
		}

		void SetTerrainName(const std::wstring& stName, ecs::Terrain* pTerrain)
		{
			pTerrain->bDirty |= stName != pTerrain->stFilename;
			pTerrain->stFilename = stName;
		}

		const std::wstring& GetTerrainName(const ecs::Terrain* pTerrain)
		{
			return pTerrain->stFilename;
		}

		/*******************************************
		* Place position query
		*******************************************/

		ecs::PlacePositionQuery* PlacePositionQueryFactory(const math::Ray& ray)
		{
			return new ecs::PlacePositionQuery(ray);
		}

		bool PlacePositionQuery(ecs::PlacePositionQuery& query)
		{
			return detail::_pMessages->Query<ecs::PlacePositionQuery>(query);
		}

		void RegisterScript(ecs::MessageManager& messages, script::Core& script)
		{
			detail::_pMessages = &messages;

			// terrain component
			auto position = script.RegisterReferenceType("Terrain", asOBJ_NOCOUNT);
			position.RegisterMethod("void SetName(const string& in)", asFUNCTION(SetTerrainName), asCALL_CDECL_OBJLAST);
			position.RegisterMethod("const string& SetVec(void)", asFUNCTION(GetTerrainName), asCALL_CDECL_OBJLAST);

			script.RegisterGlobalFunction("Terrain@ AttachTerrain(Entity& in, const string& in)", asFUNCTION(AttachTerrain));
			script.RegisterGlobalFunction("void RemoveTerrain(Entity& in)", asFUNCTION(RemoveTerrain));
			script.RegisterGlobalFunction("Terrain@ GetTerrain(const Entity& in)", asFUNCTION(GetTerrain));

			// terrain query
			auto placePosition = script.RegisterReferenceType("PlacePositionQuery", script::REF_NOCOUNT);
			placePosition.RegisterBehaviour(asBEHAVE_FACTORY, "PlacePositionQuery@ f(const Ray& in)", asFUNCTION(PlacePositionQueryFactory), asCALL_CDECL);
			placePosition.RegisterProperty("Vector3 position", asOFFSET(ecs::PlacePositionQuery, vPosition));

			script.RegisterGlobalFunction("bool queryPlacePosition(PlacePositionQuery& inout)", asFUNCTION(PlacePositionQuery));
		}

	}
}
