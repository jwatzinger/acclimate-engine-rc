#pragma once

namespace acl
{
	namespace ecs
	{
		class MessageManager;
	}

	namespace script
	{
		class Core;
	}

	namespace terrain
	{

		void RegisterScript(ecs::MessageManager& messages, script::Core& script);

	}
}
