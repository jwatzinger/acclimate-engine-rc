#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void TerrainSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Terrain* pTerrain = entity.GetComponent<Terrain>())
			{
				auto& component = entityNode.InsertNode(L"Terrain");
				component.ModifyAttribute(L"name", conv::ToString(pTerrain->stFilename));
			}
		}

	}
}
