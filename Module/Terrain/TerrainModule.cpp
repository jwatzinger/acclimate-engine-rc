#include "TerrainModule.h"
#include "TerrainSystem.h"
#include "TerrainRenderSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "Queries.h"
#include "RegisterScript.h"
#include "MipmapTerrain.h"
#include "Core\BaseContext.h"
#include "Core\ResourceManager.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Entity\QueryRegistry.h"
#include "Entity\QueryFactory.h"
#include "Gfx\IResourceLoader.h"
#include "Script\Core.h"
#include "Loader.h"

namespace acl
{
	namespace modules
	{

		TerrainModule::TerrainModule(void): m_pSystems(nullptr)
		{
		}

		void TerrainModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void TerrainModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// resources
			auto& loader = *new terrain::Loader(context.gfx.resources.textures, context.gfx.load.meshes, context.gfx.resources.models);
			auto& set = context.core.resources.AddSet<std::wstring, terrain::MipmapTerrain>(L"Terrain", L"Terrain.axm", loader);

			// system
			m_pSystems->AddSystem<ecs::TerrainSystem>(context.gfx, context.render.renderer, context.physics.world);
			m_pSystems->AddSystem<ecs::TerrainRenderSystem>(context.render.renderer);

			// io		
			ecs::Saver::RegisterSubRoutine<ecs::TerrainSaveRoutine>(L"Terrain");
			ecs::Loader::RegisterSubRoutine<ecs::TerrainLoadRoutine>(L"Terrain");

			// components
			ecs::ComponentRegistry::RegisterComponent<ecs::Terrain>(L"Terrain");
			context.ecs.componentFactory.Register<ecs::Terrain, const wchar_t*>();

			// queries
			ecs::QueryRegistry::RegisterQuery<ecs::PlacePositionQuery>(L"PlacePosition");
			context.ecs.queryFactory.Register<ecs::PlacePositionQuery, const math::Ray&>();

			// register & load script
			m_group = context.script.core.GetConfigGroup("Terrain");
			m_group.Begin();
			terrain::RegisterScript(context.ecs.messages, context.script.core);
			m_group.End();
			context.script.loader.FromFile(L"TerrainCameraController", L"Scripts/TerrainCameraController.as");
		}

		void TerrainModule::OnUninit(const core::GameStateContext& context)
		{
			m_group.Remove();
			// system
			m_pSystems->RemoveSystem<ecs::TerrainSystem>();
			m_pSystems->RemoveSystem<ecs::TerrainRenderSystem>();

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Terrain");
			context.ecs.componentFactory.Unregister<ecs::Terrain>();

			// queries
			ecs::QueryRegistry::UnregisterQuery(L"PlacePosition");
			context.ecs.queryFactory.Unregister<ecs::PlacePositionQuery>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Terrain");
			ecs::Loader::UnregisterSubRoutine(L"Terrain");

			m_pSystems = nullptr;
		}

		void TerrainModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::TerrainSystem>(dt);
		}

		void TerrainModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::TerrainRenderSystem>(0.0f);
		}

	}
}
