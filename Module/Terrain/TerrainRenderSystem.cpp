#include "TerrainRenderSystem.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Gfx\Camera.h"
#include "Render\IRenderer.h"

#include "MipmapTerrain.h"

namespace acl
{
    namespace ecs
    {

        TerrainRenderSystem::TerrainRenderSystem(const render::IRenderer& renderer): m_pCamera(nullptr), m_pStage(renderer.GetStage(L"world"))
        {
        };

        void TerrainRenderSystem::Init(MessageManager& messageManager)
        {
			messageManager.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
			messageManager.Subscribe(MessageRegistry::GetMessageId(L"RenderScene"), *this);
        }

        void TerrainRenderSystem::Update(double dt)
        {
			auto vEntities2 = m_pEntities->EntitiesWithComponents<Mipmap>();

			for(auto& entity : vEntities2)
			{
				auto& clipmap = *entity->GetComponent<Mipmap>();

				if(clipmap.pTerrain)
					clipmap.pTerrain->Render();
			}
        }

        void TerrainRenderSystem::ReceiveMessage(const BaseMessage& message)
        {
	        if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
	        {
		        m_pCamera = message.GetParam<gfx::Camera>(L"Camera");
	        }
			else if(auto pRender = message.Check(MessageRegistry::GetMessageId(L"RenderScene")))
			{
				auto vEntities = m_pEntities->EntitiesWithComponents<Mipmap>();

				//render actors
				for(auto& entity : vEntities)
				{
					auto pTerrain = entity->GetComponent<Mipmap>();
					if(pTerrain->pTerrain)
					{
						auto pStage = pRender->GetParam<render::IStage>(L"Stage");
						auto pPass = pRender->GetParam<unsigned int>(L"Pass");
						pTerrain->pTerrain->Render(*pStage, *pPass); // shadow pass
					}
				}				
			}
        }

    }
}