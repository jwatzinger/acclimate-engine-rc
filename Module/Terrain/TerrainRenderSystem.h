#pragma once
#include "Entity\System.h"

namespace acl
{
    namespace gfx
	{
		class Camera;
	}

    namespace render
    {
        class IStage;
        class IRenderer;
    }

    namespace ecs
    {

        class TerrainRenderSystem : 
	        public System<TerrainRenderSystem>
        {
        public:

	        TerrainRenderSystem(const render::IRenderer& renderer);

            void Init(MessageManager& messageManager);

	        void Update(double dt);

            void ReceiveMessage(const BaseMessage& message);

        private:

            const gfx::Camera* m_pCamera;

            render::IStage* m_pStage;
        };

    }
}