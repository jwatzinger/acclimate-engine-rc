#include "TerrainSystem.h"
#include "Components.h"
#include "Queries.h"
#include "MipmapTerrain.h"
#include "Entity\Message.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\ComponentRegistry.h"
#include "Math\HeightField.h"
#include "gfx/Camera.h"
#include "Physics/World.h"

namespace acl
{
    namespace ecs
    {

		TerrainSystem::TerrainSystem(const gfx::Context& gfx, render::IRenderer& renderer, physics::World& world) :
			m_pCamera(nullptr), m_pGfx(&gfx), m_pRenderer(&renderer), m_pWorld(&world)
        {
        }

        void TerrainSystem::Init(MessageManager& messageManager)
        {
			messageManager.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);

            messageManager.RegisterQuery<PlacePositionQuery>(*this);
        }

        void TerrainSystem::Update(double dt)
        {
			if(!m_pCamera)
				return;
			
			auto vEntities2 = m_pEntities->EntitiesWithComponents<Mipmap>();

			for(auto& entity : vEntities2)
			{
				auto& clipmap = *entity->GetComponent<Mipmap>();

				if (!clipmap.pTerrain)
					clipmap.pTerrain = new terrain::MipmapTerrain(129, 8, *m_pGfx, *m_pRenderer, *m_pCamera, *m_pWorld);
				else
					clipmap.pTerrain->Update(*m_pCamera);
			}
			
        }

        bool TerrainSystem::HandleQuery(BaseQuery& query) const
        {
           return false;
        }

		void TerrainSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
		}

    }
}