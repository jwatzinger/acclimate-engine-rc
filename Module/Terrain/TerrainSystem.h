#pragma once
#include "Entity\System.h"

namespace acl
{
	namespace gfx
	{
		struct Context;
		class Camera;
	}

	namespace render
	{
		class IRenderer;
	}

	namespace physics
	{
		class World;
	}

    namespace ecs
    {

        class TerrainSystem :
            public System<TerrainSystem>
        {
        public:

			TerrainSystem(const gfx::Context& gfx, render::IRenderer& renderer, physics::World& world);

            void Init(MessageManager& messageManager);
            void Update(double dt);
			void ReceiveMessage(const BaseMessage& message);
            bool HandleQuery(BaseQuery& query) const;

        private:

			const gfx::Camera* m_pCamera;
			const gfx::Context* m_pGfx;
			render::IRenderer* m_pRenderer;
			physics::World* m_pWorld;
        };

    }
}

