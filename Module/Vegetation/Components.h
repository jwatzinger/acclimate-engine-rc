#pragma once
#include <string>
#include "Entity\Component.h"

namespace acl
{
	namespace veg
	{
		class Grass;
		class Tree;

		struct GrassComponent : ecs::Component<GrassComponent>
		{
			GrassComponent(const std::wstring& stFilename);

			std::wstring stFilename;
			veg::Grass* pGrass;
		};

		struct TreeComponent : ecs::Component<TreeComponent>
		{
			TreeComponent();
			veg::Tree* pTree;
		};

	}
}