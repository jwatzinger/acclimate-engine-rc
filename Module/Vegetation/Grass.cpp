#include "Grass.h"
#include "Gfx\Context.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\VertexBufferAccessor.h"
#include "Gfx\InstanceBufferAccessor.h"
#include "Gfx\ResourceBlock.h"
#include "Gfx\TextureAccessors.h"
#include "Gfx\Camera.h"
#include "Render\IRenderer.h"
#include "Render\Context.h"
#include "Gfx\ModelInstance.h"
#include <algorithm>
#include <time.h>
#include <thread>
#include "System\Random.h"



/*
#include "Gfx\IInstancedMesh.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\Camera.h"
#include "Gfx\TextureAccessors.h"
#include "Gfx\BaseMeshLoader.h"
#include "Render\IStage.h"
*/

namespace acl
{
	namespace veg
	{
		struct GrassInstance
		{
			float x, y, z, w;
			float scale;
			float grassType;
		};

		Grass::Grass(gfx::Context& ctx, std::wstring texFile, float cullDistance, unsigned int maxInstance) : m_pModel(nullptr), m_meshLoader(nullptr),
			m_maxInstance(maxInstance), m_cullDistance(cullDistance), m_doneLoading(false), m_WindForceCounter(0), m_pWindMap(nullptr), m_vWindProgressor(0, 0, 0),
			m_vWindDirection(0.0f, 0.0f, 1.0f)
		{
			m_rBlock = new gfx::ResourceBlock(ctx.resources);
			m_rBlock->Begin();

			m_meshLoader = &ctx.load.meshes;
			const gfx::IModelLoader::PassVector vPasses{ { 0, L"Grass" }, { 1, L"GrassShadow" } };
			const gfx::IMaterialLoader& matLoader = ctx.load.materials;
			//const gfx::TextureVector vTextures{ L"Grass", L"Grass01"};

			auto pMat = matLoader.Load(L"Grass", L"GrassGeometry", 0, texFile);
			matLoader.Load(L"GrassShadow", L"GrassShadow", 0, texFile);

			const gfx::IModelLoader& modLoader = ctx.load.models;
			m_pModel = &modLoader.LoadInstance(L"", vPasses);
			
			pMat->SetVertexTexture(0, ctx.resources.textures[L"WindForce"]);

			gfx::IGeometry::AttributeVector vAttr;
			vAttr.emplace_back(gfx::AttributeSemantic::TEXCOORD, 0, gfx::AttributeType::FLOAT, 4, 1, 1);
			vAttr.emplace_back(gfx::AttributeSemantic::TEXCOORD, 1, gfx::AttributeType::FLOAT, 1, 1, 1);
			vAttr.emplace_back(gfx::AttributeSemantic::TEXCOORD, 2, gfx::AttributeType::FLOAT, 1, 1, 1);
			m_pInstMesh = m_meshLoader->Instanced(L"Grass", m_maxInstance, vAttr);
			m_pModel->GetParent()->SetMesh(*m_pInstMesh);
			m_rBlock->End();

			srand((unsigned int)time(NULL));

			//m_heightMap = nullptr;
			m_heightMap = ctx.resources.textures.Get(L"TerrainHeightmap");
			m_densityMap = ctx.resources.textures.Get(L"GrassDensity");
			//m_pWindMap = ctx.resources.textures.Get(L"WindForce");

			//TODO:setup query to get acutal tilesize from terrain module
			//math::Vector3 tileSize(0.5f, 0.5f, 0);
			math::Vector3 tileSize(1.0f, 1.0f, 0);

			LoadGrass(ctx, tileSize);
			//std::thread loadThread(&Grass::LoadGrass, *this, ctx, tileSize);

			//Instanciate();
		}

		Grass::~Grass()
		{
			m_rBlock->Clear();

			if (m_doneLoading)
				delete m_pInstMesh;
		}
		
		void Grass::Render(const render::IStage& stage, size_t pass)
		{
			/*gfx::InstanceBufferAccessor<GrassInstance>* iBAccessor = new gfx::InstanceBufferAccessor<GrassInstance>(*m_pInstMesh);
			
			gfx::TextureAccessorL8* texAccessorWM = new gfx::TextureAccessorL8(*m_pWindMap, true);

			auto pData = iBAccessor->GetData();
			float windPower = 0.0f;

			for (auto& pos : m_vPositions)
			{
				windPower = texAccessorWM->GetPixel(((int)pos.vPosition.x % m_pWindMap->GetSize().x),// + m_vWindProgressor.x,
													((int)pos.vPosition.y % m_pWindMap->GetSize().y));// + m_vWindProgressor.y);
				windPower /= 128.0f;
				//m_vWindProgressor += {1, 0};
				pData->windOffset = windPower;
				pData++;
			}
			delete texAccessorWM;
			delete iBAccessor;*/

			UpdateWindPos();

			m_pModel->SetGeometryConstant(0, (float*)&math::Vector3(m_cullDistance, m_cullDistance*0.5f, m_cullDistance*0.25f), 1);

			//windirection
			m_pModel->SetGeometryConstant(1, (float*)&m_vWindDirection, 1);

			//windstrength
			//float windPower = GetWindPower();
			//m_pModel->SetGeometryConstant(2, (float*)&math::Vector3(windPower, 0.0f, 0.0f), 1);

			m_pModel->SetVertexConstant(0, (float*)&m_vWindProgressor, 1);

			m_pModel->Draw(stage, pass);
		}

		void Grass::LoadGrass(gfx::Context& ctx, const math::Vector3& tileSize)
		{
			float totalDensity = TotalGrassDensity(ctx);
			SetGrassPositions(totalDensity, tileSize);
			m_doneLoading = true;
		}

		void Grass::Instanciate(const acl::gfx::Camera& camera)
		{
			if (!m_doneLoading)
				return;
			PrepVisible(camera);
			//SortBlades(camera.GetPosition());
				
			gfx::InstanceBufferAccessor<GrassInstance> iBAccessor(*m_pInstMesh);
			//gfx::TextureAccessorL8 texAccessorWM(*m_pWindMap, true);

			float windOffset = 0.0f;

			auto pData = iBAccessor.GetData();

			for (auto& pos : m_vPositions)
			{
				pData->x = pos.vPosition.x;
				pData->y = pos.vPosition.y;
				pData->z = pos.vPosition.z;
				//pData->w = 0;
				//m_vWindProgressor += {1, 0};
				pData->scale = pos.scale;
				pData->grassType = (float)((int)(pos.scale * 10.0f) % 4);
				pData++;
			}
			m_pInstMesh->SetInstanceCount(m_vPositions.size());
		}

		float Grass::GetWindPower()
		{
			float windPower = 0.0f;
			gfx::TextureAccessorL8 texAccessorWM(*m_pWindMap, true);
			m_WindForceCounter += 0.5f;
			if (m_WindForceCounter >= m_pWindMap->GetSize().x*m_pWindMap->GetSize().y)
				m_WindForceCounter = 0;
			windPower = texAccessorWM.GetPixel((int)m_WindForceCounter % m_pWindMap->GetSize().x, m_WindForceCounter / m_pWindMap->GetSize().x);
			windPower /= 256.0f;
			return windPower;
		}
		
		void Grass::SortBlades(const math::Vector3& vCameraPos)
		{
			//iterate through particles for depth sorting

			for (auto& pos : m_vPositions)
			{
				//get index of currect particle to keep temporal coherency
				pos.depth = abs(pos.vPosition.x - vCameraPos.x);
				pos.depth += abs(pos.vPosition.y - vCameraPos.y);
				pos.depth += abs(pos.vPosition.z - vCameraPos.z);
			}
			std::sort(m_vPositions.begin(), m_vPositions.end(), GrassSorter());
		}

		void Grass::PrepVisible(const acl::gfx::Camera& camera)
		{
			//TODO: think of something nifty to update m_vPositions instead of clearing
			m_vPositions.clear();
			float depth = 0.0f;
			for (auto& pos : m_vGrasses)
			{
				depth = (pos.first - camera.GetPosition()).length();
				if (depth < m_cullDistance && camera.GetFrustum().DotInside(pos.first, -2.0f))
					m_vPositions.emplace_back(GrassDepth{ pos.first, depth, pos.second });
			}

			SortBlades(camera.GetPosition());
		}

		float Grass::TotalGrassDensity(gfx::Context& ctx)
		{
			gfx::TextureAccessorL8 texAccessor(*m_densityMap, true);

			float total = 0.0f;
			int texSize = m_densityMap->GetSize().x * m_densityMap->GetSize().y;

			for (int i = 0; i < m_densityMap->GetSize().x; i++)
			{
				for (int k = 0; k < m_densityMap->GetSize().y; k++)
				{
					total += (float)texAccessor.GetPixel(i, k) / m_maxInstance;
				}
			}
			return total;
		}

		void Grass::SetGrassPositions(float spawnThreshold, const math::Vector3& tileSize)
		{
			gfx::TextureAccessorL8 texAccessorHM(*m_heightMap, true);
			gfx::TextureAccessorL8 texAccessorDM(*m_densityMap, true);

			float mapWidth = (float)m_densityMap->GetSize().x;
			float mapHeight = (float)m_densityMap->GetSize().y;
			float elevation = 0.0f;
			float density = 0.0f;
			float maxVegHeight = 100.0f;
			float overHead = 0.0f;
			int maxSpawnPerTile = 4;
			int counter = 0;
			std::list<math::Vector3> tile;
			math::Vector3 grassPos;

			//TODO: check if elevation is above water

			for (int i = 0; i < mapWidth; i++)
			{
				for (int k = 0; k < mapHeight; k++)
				{
					tile.clear();
					if (m_vGrasses.size() >= m_maxInstance)
						break;
					density = texAccessorDM.GetPixel(i, k);

					density += overHead;
					while (density > spawnThreshold)
					{
						if (counter > maxSpawnPerTile)
							break;	
						grassPos = RndGrassTilePos(tile, tileSize);

						tile.emplace_back(grassPos);

						float x = i + grassPos.x;
						float y = k + grassPos.y;

						if (x < 0 || y < 0 || x >= mapWidth-1 || y >= mapHeight-1)
							break;

						float diffx = x - floor(x);
						float diffy = y - floor(y);

						math::Vector3 f(x, y, 0);


						math::Vector3 p1(0, 0, 0);
						math::Vector3 p2(0, 0, 0);
						math::Vector3 p3(0, 0, 0);

						if (1.0f - diffx < diffy)	//upper right triangle
						{
							p1 = math::Vector3(ceil(x), floor(y), 0);
							p2 = math::Vector3(floor(x), ceil(y), 0);
							p3 = math::Vector3(ceil(x), ceil(y), 0);
						}
						else	//bottom left triangle
						{
							p1 = math::Vector3(ceil(x), floor(y), 0);
							p2 = math::Vector3(floor(x), floor(y), 0);
							p3 = math::Vector3(floor(x), ceil(y), 0);
						}


						math::Vector3 f1 = p1 - f;
						math::Vector3 f2 = p2 - f;
						math::Vector3 f3 = p3 - f;

						float a = ((p1 - p2).Cross(p1 - p3)).length();
						float a1 = (f2.Cross(f3)).length() / a;
						float a2 = (f3.Cross(f1)).length() / a;
						float a3 = (f1.Cross(f2)).length() / a;

						float h1 = texAccessorHM.GetPixel(p1.x, p1.y);
						float h2 = texAccessorHM.GetPixel(p2.x, p2.y);
						float h3 = texAccessorHM.GetPixel(p3.x, p3.y);

						float maxIncline = 2.0f;
						if (abs(h1 - h2) > maxIncline || abs(h1 - h3) > maxIncline || abs(h2 - h3) > maxIncline)
							break;

						elevation = h1 * a1 + h2 * a2 + h3 * a3;
						
						if (elevation > maxVegHeight)
							break;
						
						m_vGrasses.emplace_back(std::pair<math::Vector3, float>({ x, elevation*0.2f, y }, rnd::fRange(0.35f, 0.9f)));
						density -= spawnThreshold;
						counter++;
					}
					counter = 0;
					overHead = density;
				}
			}
			for (auto& pos : m_vGrasses)
			{
				m_vPositions.emplace_back(GrassDepth{ pos.first, 0.0f , pos.second});
			}
		}

		math::Vector3 Grass::RndGrassTilePos(const std::list<math::Vector3>& tile, const math::Vector3& tileSize)
		{
			int maxTry = 10;
			int counter = 0;
			float xPos = 0.0f;
			float yPos = 0.0f;
			float distance = 0.0f;
			float minSpacing = tileSize.x * 0.5f;
			float variance = 5.0f;
			bool isValid = true;

			while (counter < maxTry)
			{
				xPos = (float)(rand() % (int)(tileSize.x * 100 * variance));
				yPos = (float)(rand() % (int)(tileSize.y * 100 * variance));
				xPos /= 100.0f;
				yPos /= 100.0f;
				xPos -= tileSize.x*0.5f;
				yPos -= tileSize.y*0.5f;

				if (tile.size() == 0)
					return math::Vector3(xPos, yPos, 0);
				for(auto& v : tile)
				{
					distance = (xPos - v.x)*(xPos - v.x) + (yPos - v.y)*(yPos - v.y);
					if (distance <= minSpacing*minSpacing)
						isValid = false;
				}
				if (isValid)
					return math::Vector3(xPos, yPos, 0);
				counter++;
				isValid = true;
			}
			return math::Vector3(0, 0, 0);
		}

		void Grass::UpdateWindPos()
		{
			//TODO: winddirection hardcoded (also in grass.gfx shader), loopcycle hardcoded in vertexshader
			float loopCycle = 400.0f;
			m_vWindProgressor += m_vWindDirection * 0.25f;
			if (m_vWindProgressor.z >= loopCycle - 1)	m_vWindProgressor.z = 0.0f;
		}
	}
}