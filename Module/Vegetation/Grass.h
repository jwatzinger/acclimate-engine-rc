#pragma once
#include <vector>
#include "Math\Vector3.h"
#include "Math\Vector2f.h"
#include <list>

namespace acl
{
	namespace gfx
	{
		class IInstancedMesh;
		class ModelInstance;
		class Camera;
		struct Context;
		class IMesh;
		class IModelLoader;
		class IVertexBuffer;
		class IMeshLoader;
		class ResourceBlock;
		class ITexture;
	}

	namespace render
	{
		class IStage;
	}

	namespace veg
	{

		struct GrassDepth
		{
			math::Vector3 vPosition;
			float depth;
			float scale;
		};

		struct GrassSorter
		{
			/** Compares two ParticleDepth depth values.
			*
			*	@param[in] left First grassblade to compare.
			*	@param[in] right Second grassblade to compare.
			*/
			inline bool operator() (const GrassDepth& left, const GrassDepth& right)
			{
				return left.depth < right.depth;
			}
		};

		class Grass
		{
		
		typedef std::vector<GrassDepth> DepthVector;

		public:

			Grass(gfx::Context& ctx, std::wstring texFile, float cullDistance, unsigned int maxInstance = 1000);
			//Grass(const gfx::Context& gfx, render::IRenderer& renderer);
			//Grass(size_t maxCount, gfx::IInstancedMesh& mesh, gfx::IModel& model);
			~Grass(void);

			void Update(const gfx::Camera& camera);

			void Render(const render::IStage& stage, size_t pass);

			void Instanciate(const acl::gfx::Camera& camera);

			float GetWindPower();

		private:

			void SortBlades(const math::Vector3& vCameraPos);

			void PrepVisible(const acl::gfx::Camera& camera);

			float TotalGrassDensity(gfx::Context& ctx);

			void SetGrassPositions(float spawnThreshold, const math::Vector3& tileSize);

			math::Vector3 RndGrassTilePos(const std::list<math::Vector3>& tile, const math::Vector3& tileSize);

			void LoadGrass(gfx::Context& ctx, const math::Vector3& tileSize);

			void UpdateWindPos();

			gfx::ModelInstance* m_pModel;
			gfx::IInstancedMesh* m_pInstMesh;
			gfx::IVertexBuffer* m_pInstBuffer;

			gfx::ResourceBlock* m_rBlock;
			gfx::ITexture* m_heightMap;
			gfx::ITexture* m_densityMap;

			unsigned int m_maxInstance;
			const gfx::IMeshLoader* m_meshLoader;

			//TODO: change depthvector members to references to m_vGrasses
			DepthVector m_vPositions;

			std::vector<std::pair<math::Vector3, float>> m_vGrasses;
			float m_cullDistance;
			bool m_doneLoading;

			float m_WindForceCounter;
			math::Vector3 m_vWindProgressor;
			gfx::ITexture* m_pWindMap;
			math::Vector3 m_vWindDirection;
		};
	}
}
