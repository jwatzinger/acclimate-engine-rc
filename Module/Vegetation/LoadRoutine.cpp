#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace veg
	{

		void VegetationLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{
			if (const xml::Node* pGrass = entityNode.FirstNode(L"Grass"))
				entity.AttachComponent<GrassComponent>(pGrass->Attribute(L"file")->GetValue());
			if (const xml::Node* pTree = entityNode.FirstNode(L"Tree"))
				entity.AttachComponent<TreeComponent>();
		}

	}
}
