#pragma once
#include "Entity\ISubLoadRoutine.h"

namespace acl
{
	namespace veg
	{

		class VegetationLoadRoutine :
			public ecs::ISubLoadRoutine
		{
		public:

			void Execute(ecs::Entity& entity, const xml::Node& entityNode) const override;
		};

	}
}

