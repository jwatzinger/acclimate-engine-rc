#include "Module.h"
#include "Core\BaseContext.h"
#include "Core\ResourceManager.h"
#include "Entity\SystemManager.h"
#include "Entity\Loader.h"
#include "Entity\Saver.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IMeshLoader.h"
#include "Core\SettingLoader.h"
#include "RenderSystem.h"
#include "LoadRoutine.h"

namespace acl
{
	namespace veg
	{

		Module::Module(void) : m_pSystems(nullptr), m_pGrassMesh(nullptr)
		{
		}

		void Module::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");

			gfx::IGeometry::AttributeVector vAttr;
			vAttr.emplace_back(gfx::AttributeSemantic::POSITION, 0, gfx::AttributeType::FLOAT, 1);
			const gfx::IMeshLoader::VertexVector vVtx = { 0 };
			const gfx::IMeshLoader::IndexVector vIdx = { 0 };

			m_pGrassMesh = &context.meshes.Custom(L"Grass", vAttr, vVtx, vIdx, gfx::PrimitiveType::POINT);
		}

		void Module::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			//io
			ecs::Loader::RegisterSubRoutine<veg::VegetationLoadRoutine>(L"Grass");
			ecs::Loader::RegisterSubRoutine<veg::VegetationLoadRoutine>(L"Tree");

			// load settings
			context.core.settingLoader.Load(L"Settings.axm");

			// resources
			//auto& loader = *new veg::Loader(context.gfx.resources.textures, context.gfx.load.meshes, context.gfx.resources.models);
			//auto& set = context.core.resources.AddSet<std::wstring, veg::Terrain>(L"Terrain", L"Terrain.axm", loader);

			// system
			//m_pSystems->AddSystem<veg::System>(context.gfx, context.render.renderer, set.GetResources());
			m_pSystems->AddSystem<veg::RenderSystem>(context);
		}

		void Module::OnUninit(const core::GameStateContext& context)
		{
			//m_pSystems->RemoveSystem<System>();
			m_pSystems->RemoveSystem<RenderSystem>();
			m_pSystems = nullptr;
		}

		void Module::OnUpdate(double dt)
		{
			//m_pSystems->UpdateSystem<System>(0.0f);
		}

		void Module::OnRender(void) const
		{
			m_pSystems->UpdateSystem<veg::RenderSystem>(0.0f);
		}

	}
}
