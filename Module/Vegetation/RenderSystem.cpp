#include "RenderSystem.h"
#include "Core\BaseContext.h"
#include "Core\SettingModule.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"
#include "Gfx\Camera.h"
#include "Gfx\TextureAccessors.h"
#include "Gfx\IResourceLoader.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"
#include "ApiInclude.h"

#include "Components.h"
#include "Grass.h"
#include "Tree.h"

#include <iostream>

#define PI 3.14159265

namespace acl
{
	namespace veg
	{

		RenderSystem::RenderSystem(const core::GameStateContext& context) : m_pCamera(nullptr), m_pGfx(&context.gfx), m_cullDistance(75.0f),
			m_pWorldCtx(&context.physics.world), m_grassDensity(10000), m_treeDensity(1000)
		{
			m_pStage = context.render.renderer.GetStage(L"world");

			context.core.settings.SigUpdateSettings.Connect(this, &RenderSystem::OnSettingChange);
			OnSettingChange(context.core.settings);
		};

		void RenderSystem::Init(ecs::MessageManager& messageManager)
		{
			messageManager.Subscribe(ecs::MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
			messageManager.Subscribe(ecs::MessageRegistry::GetMessageId(L"RenderScene"), *this);
		}

		void RenderSystem::Update(double dt)
		{

			//get entities with grass component
			auto vGrassEntities = m_pEntities->EntitiesWithComponents<GrassComponent>();

			//loop through those entities to update grass
			for (auto& entity : vGrassEntities)
			{
				auto* pVegetation = entity->GetComponent<GrassComponent>();
				if (pVegetation->pGrass != nullptr)
				{
					pVegetation->pGrass->Render(*m_pStage, 0);
				}
				else
					pVegetation->pGrass = new veg::Grass(*m_pGfx, pVegetation->stFilename, m_cullDistance, m_grassDensity);
			}
			
			//get entities with tree component
			auto vTreeEntities = m_pEntities->EntitiesWithComponents<TreeComponent>();

			//loop through those entities to update tree
			for (auto& entity : vTreeEntities)
			{
				auto* pVegetation = entity->GetComponent<TreeComponent>();
				if (pVegetation->pTree != nullptr)
				{
					pVegetation->pTree->Render(*m_pStage, 0);
				}
				else
					pVegetation->pTree = new veg::Tree(*m_pGfx, m_cullDistance, m_treeDensity, m_pWorldCtx);
			}
		}

		void RenderSystem::ReceiveMessage(const ecs::BaseMessage& message)
		{
			if (auto pUpdate = message.Check(ecs::MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				m_pCamera = message.GetParam<gfx::Camera>(L"Camera");

				if (getUsedAPI() != GraphicAPI::DX9)
				{
					m_pStage->SetGeometryConstant(0, (float*)&m_pCamera->GetViewProjectionMatrix(), 4);
					m_pStage->SetGeometryConstant(4, (float*)&m_pCamera->GetPosition(), 1);
					m_pStage->SetGeometryConstant(5, (float*)&SortGrassFaces(m_pCamera->GetDirection()), 1);

					auto vGrassEntities = m_pEntities->EntitiesWithComponents<GrassComponent>();
					for (auto& entity : vGrassEntities)
					{
						auto* pVegetation = entity->GetComponent<GrassComponent>();
						if (pVegetation->pGrass != nullptr)
							pVegetation->pGrass->Instanciate(*m_pCamera);
					}

					auto vTreeEntities = m_pEntities->EntitiesWithComponents<TreeComponent>();
					for (auto& entity : vTreeEntities)
					{
						auto* pVegetation = entity->GetComponent<TreeComponent>();
						if (pVegetation->pTree != nullptr)
							pVegetation->pTree->Instanciate(*m_pCamera);
					}
				}
			}
			else if (auto pRender = message.Check(ecs::MessageRegistry::GetMessageId(L"RenderScene")))
			{
				auto pStage = pRender->GetParam<render::IStage>(L"Stage");
				auto pPass = pRender->GetParam<unsigned int>(L"Pass");

				//get entities with grass component
				auto vGrassEntities = m_pEntities->EntitiesWithComponents<GrassComponent>();

				//loop through those entities to update grass
				for(auto& entity : vGrassEntities)
				{
					auto* pVegetation = entity->GetComponent<GrassComponent>();
					if(pVegetation->pGrass != nullptr)
					{
						pVegetation->pGrass->Render(*pStage, *pPass);
					}
				}

				auto vTreeEntities = m_pEntities->EntitiesWithComponents<TreeComponent>();
				for(auto& entity : vTreeEntities)
				{
					auto* pVegetation = entity->GetComponent<TreeComponent>();
					if(pVegetation->pTree != nullptr)
						pVegetation->pTree->Render(*pStage, *pPass);
				}
			}
		}

		math::Vector3 RenderSystem::SortGrassFaces(const math::Vector3& CamDir)
		{
			math::Vector3 vTmp(CamDir);
			vTmp.y = 0;
			vTmp.Normalize();
			float tmp = -1;
			math::Vector3 sort = { -1, -1, -1 };
			math::Vector3 faceNrmls[6];

			faceNrmls[0] = math::Vector3(0, 0, 1) + vTmp;
			faceNrmls[1] = math::Vector3(0.83f, 0, -0.55f) + vTmp;
			faceNrmls[2] = math::Vector3(-0.83f, 0, -0.55f) + vTmp;
			faceNrmls[3] = math::Vector3(0, 0, -1) + vTmp;
			faceNrmls[4] = math::Vector3(-0.83f, 0, 0.55f) + vTmp;
			faceNrmls[5] = math::Vector3(0.83f, 0, 0.55f) + vTmp;
			
			if (faceNrmls[0].length() < faceNrmls[3].length())
				sort.x = 0;
			else
				sort.x = 3;
			if (faceNrmls[1].length() < faceNrmls[4].length())
				sort.y = 1;
			else
				sort.y = 4;
			if (faceNrmls[2].length() < faceNrmls[5].length())
				sort.z = 2;
			else
				sort.z = 5;

			if (faceNrmls[(int)sort.y].length() > faceNrmls[(int)sort.z].length())
			{
				tmp = sort.y;
				sort.y = sort.z;
				sort.z = tmp;
			}
			if (faceNrmls[(int)sort.x].length() > faceNrmls[(int)sort.y].length())
			{
				tmp = sort.x;
				sort.x = sort.y;
				sort.y = tmp;
				if (faceNrmls[(int)sort.y].length() > faceNrmls[(int)sort.z].length())
				{
					tmp = sort.y;
					sort.y = sort.z;
					sort.z = tmp;
				}
			}
			if (sort.x > 2)	sort.x -= 3;
			if (sort.y > 2)	sort.y -= 3;
			if (sort.z > 2)	sort.z -= 3;

			return sort;
		}

		void RenderSystem::OnSettingChange(const core::SettingModule& settings)
		{
			auto s = settings.GetSetting(L"VegViewDistance");
			m_cullDistance = 250.0f;// s->GetData<float>();
			s = settings.GetSetting(L"GrassDensity");
			m_grassDensity = (int)s->GetData<float>();
			s = settings.GetSetting(L"TreeDensity");
			m_treeDensity = (int)s->GetData<float>();
		}
	}
}