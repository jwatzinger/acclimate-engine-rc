#pragma once
#include "Entity\System.h"
#include "Core\IModule.h"
#include "Physics\World.h"

namespace acl
{
	namespace core
	{
		class SettingModule;
	}

	namespace gfx
	{
		class Camera;
		struct Context;
		class ITexture;
	}

	namespace render
	{
		class IStage;
		class IRenderer;
	}

	namespace math
	{
		struct Vector3;
	}

	namespace veg
	{
		class Grass;

		class RenderSystem :
			public ecs::System<RenderSystem>
		{
		public:

			RenderSystem(const core::GameStateContext& context);

			void Init(ecs::MessageManager& messageManager);

			void Update(double dt);

			void ReceiveMessage(const ecs::BaseMessage& message);

			void OnSettingChange(const core::SettingModule& settings);

		private:

			math::Vector3 SortGrassFaces(const math::Vector3& CamDir);

			const gfx::Camera* m_pCamera;
			render::IStage* m_pStage;
			gfx::Context* m_pGfx;
			physics::World* m_pWorldCtx;

			float m_cullDistance;
			int m_grassDensity;
			int m_treeDensity;
		};

	}
}