#include "Tree.h"
#include "Gfx\Context.h"
#include "Gfx\IResourceLoader.h"
#include "Gfx\IModelLoader.h"
#include "Gfx\IMaterialLoader.h"
#include "Gfx\IInstancedMesh.h"
#include "Gfx\IMeshLoader.h"
#include "Gfx\VertexBufferAccessor.h"
#include "Gfx\InstanceBufferAccessor.h"
#include "Gfx\ResourceBlock.h"
#include "Gfx\TextureAccessors.h"
#include "Gfx\Camera.h"
#include "Render\IRenderer.h"
#include "Render\Context.h"
#include "Gfx\ModelInstance.h"
#include <algorithm>
#include <time.h>
#include <thread>
#include "Physics\RigidBody.h"
#include "System\Random.h"

namespace acl
{
	namespace veg
	{
		struct TreeInstance
		{
			float x, y, z, scale;
			float treeType;
		};

		Tree::Tree(gfx::Context& ctx, float cullDistance, unsigned int maxInstance, physics::World* world) : m_maxInstance(maxInstance), m_pModel(nullptr), m_rBlock(nullptr),
			m_cullDistance(cullDistance), m_doneLoading(false), m_heightMap(nullptr), m_densityMap(nullptr), m_meshLoader(nullptr), m_pInstMesh(nullptr),
			m_pWorld(world), m_vHBPool(), m_poolSize(50), m_Material(20.0f, 0.05f, 0.2f, 0.2f), m_capsule(40.4f, 0.5f)
		{
			m_rBlock = new gfx::ResourceBlock(ctx.resources);
			m_rBlock->Begin();

			m_meshLoader = &ctx.load.meshes;
			/*const gfx::IModelLoader::PassVector vPasses{ { 0, L"WalnutTree" } };

			//const gfx::IMaterialLoader& matLoader = ctx.load.materials;

			//matLoader.Load(L"WalnutTree", L"Tree", 0, L"WalnutTree");"

			//const gfx::IModelLoader& modLoader = ctx.load.models;
			//m_pModel = &modLoader.LoadInstance(L"WalnutTree", vPasses);*/
			m_pModel = &(ctx.resources.models.Get(L"WalnutTree")->CreateInstance());

			gfx::IGeometry::AttributeVector vAttr;
			vAttr.emplace_back(gfx::AttributeSemantic::TEXCOORD, 5, gfx::AttributeType::FLOAT, 4, 1, 1);
			vAttr.emplace_back(gfx::AttributeSemantic::TEXCOORD, 6, gfx::AttributeType::FLOAT, 1, 1, 1);
			m_pInstMesh = m_meshLoader->Instanced(L"WalnutTree", m_maxInstance, vAttr);
			m_pModel->GetParent()->SetMesh(*m_pInstMesh);
			m_heightMap = ctx.resources.textures.Get(L"TerrainHeightmap");
			m_densityMap = ctx.resources.textures.Get(L"TreeDensity");

			m_rBlock->End();

			//TODO:setup query to get acutal tilesize from terrain module
			math::Vector3 tileSize(1.0f, 1.0f, 0);
			LoadTree(ctx, tileSize);
			//std::thread loadThread(&Tree::LoadTree, this, ctx, tileSize);
			//loadThread.detach();
		}


		Tree::~Tree()
		{
			m_rBlock->Clear();

			if (m_doneLoading)
				delete m_pInstMesh;
		}
		
		void Tree::Render(const render::IStage& stage, size_t pass)
		{
			m_pModel->Draw(stage, pass);
		}

		void Tree::LoadTree(gfx::Context& ctx, const math::Vector3& tileSize)
		{
			float totalDensity = TotalTreeDensity(ctx);
			SetTreePositions(totalDensity, tileSize);
			CreateHitboxPool();
			m_doneLoading = true;
		}

		void Tree::Instanciate(const acl::gfx::Camera& camera)
		{
			if (!m_doneLoading)
				return;

			PrepVisible(camera);

			gfx::InstanceBufferAccessor<TreeInstance> iBAccessor(*m_pInstMesh);
			
			auto pData = iBAccessor.GetData();

			for (auto& pos : m_vPositions)
			{
				pData->x = pos.vPosition.x;
				pData->y = pos.vPosition.y;
				pData->z = pos.vPosition.z;
				//pData->w = 0;
				pData->scale = pos.scale;
				pData->treeType = (float)((int)(pos.scale * 10.0f) % 4); // TODO: figure out why tree type two causes stupid jittering
				if(pData->treeType == 2)
					pData->treeType = 0;
				pData++;
			}
			m_pInstMesh->SetInstanceCount(m_vPositions.size());
		}

		void Tree::PrepVisible(const acl::gfx::Camera& camera)
		{
			//TODO: think of something nifty to update m_vPositions instead of clearing
			m_vPositions.clear();
			float depth = 0.0f;
			int ctr = 0;
			for (auto pos : m_vGrasses)
			{
				depth = (pos.first - camera.GetPosition()).length();
				if (depth < m_cullDistance){
					m_vPositions.emplace_back(TreeStruct{ pos.first, pos.second });
					if (ctr < m_poolSize){
						m_vHBPool.at(ctr)->m_transform.vPosition = pos.first;
						ctr++;
					}
				}
			}
		}

		void Tree::SetTreePositions(float spawnThreshold, const math::Vector3& tileSize)
		{
			gfx::TextureAccessorL8 texAccessorHM(*m_heightMap, true);
			gfx::TextureAccessorL8 texAccessorDM(*m_densityMap, true);

			float mapWidth = (float)m_densityMap->GetSize().x-1;
			float mapHeight = (float)m_densityMap->GetSize().y-1;
			float elevation = 0.0f;
			float density = 0.0f;
			float maxVegHeight = 100.0f;
			float overHead = 0.0f;
			int maxSpawnPerTile = 4;
			int counter = 0;
			std::list<math::Vector3> tile;
			math::Vector3 treePos;

			//TODO: check if elevation is above water

			for (int i = 0; i < mapWidth; i++)
			{
				for (int k = 0; k < mapHeight; k++)
				{
					tile.clear();
					if (m_vGrasses.size() >= m_maxInstance)
						break;
					density = texAccessorDM.GetPixel(i, k);

					density += overHead;
					while (density > spawnThreshold)
					{
						if (counter > maxSpawnPerTile)
							break;
						treePos = RndTreeTilePos(tile, tileSize);

						tile.emplace_back(treePos);

						float x = i + treePos.x;
						float y = k + treePos.y;

						if (x < 0 || y < 0 || x >= mapWidth || y >= mapHeight)
							break;

						float diffx = x - floor(x);
						float diffy = y - floor(y);

						math::Vector3 f(x, y, 0);


						math::Vector3 p1(0, 0, 0);
						math::Vector3 p2(0, 0, 0);
						math::Vector3 p3(0, 0, 0);

						if (1.0f - diffx < diffy)	//upper right triangle
						{
							p1 = math::Vector3(ceil(x), floor(y), 0);
							p2 = math::Vector3(floor(x), ceil(y), 0);
							p3 = math::Vector3(ceil(x), ceil(y), 0);
						}
						else	//bottom left triangle
						{
							p1 = math::Vector3(ceil(x), floor(y), 0);
							p2 = math::Vector3(floor(x), floor(y), 0);
							p3 = math::Vector3(floor(x), ceil(y), 0);
						}


						math::Vector3 f1 = p1 - f;
						math::Vector3 f2 = p2 - f;
						math::Vector3 f3 = p3 - f;

						float a = ((p1 - p2).Cross(p1 - p3)).length();
						float a1 = (f2.Cross(f3)).length() / a;
						float a2 = (f3.Cross(f1)).length() / a;
						float a3 = (f1.Cross(f2)).length() / a;

						float h1 = texAccessorHM.GetPixel(p1.x, p1.y);
						float h2 = texAccessorHM.GetPixel(p2.x, p2.y);
						float h3 = texAccessorHM.GetPixel(p3.x, p3.y);

						float maxIncline = 2.0f;
						if (abs(h1 - h2) > maxIncline || abs(h1 - h3) > maxIncline || abs(h2 - h3) > maxIncline)
							break;

						elevation = h1 * a1 + h2 * a2 + h3 * a3;

						if (elevation > maxVegHeight)
							break;

						m_vGrasses.emplace_back(std::pair<math::Vector3, float>({ x, elevation*0.2f, y }, rnd::fRange(0.5f, 1.5f)));
						density -= spawnThreshold;
						counter++;
					}
					counter = 0;
					overHead = density;
				}
			}
			for (auto& pos : m_vGrasses)
			{
				m_vPositions.emplace_back(TreeStruct{ pos.first, pos.second });
			}
		}

		math::Vector3 Tree::RndTreeTilePos(const std::list<math::Vector3>& tile, const math::Vector3& tileSize)
		{
			int maxTry = 10;
			int counter = 0;
			float xPos = 0.0f;
			float yPos = 0.0f;
			float distance = 0.0f;
			float minSpacing = tileSize.x * 0.5f;
			float variance = 5.0f;
			bool isValid = true;

			while (counter < maxTry)
			{
				xPos = (float)(rand() % (int)(tileSize.x * 100 * variance));
				yPos = (float)(rand() % (int)(tileSize.y * 100 * variance));
				xPos /= 100.0f;
				yPos /= 100.0f;
				xPos -= tileSize.x*0.5f;
				yPos -= tileSize.y*0.5f;

				if (tile.size() == 0)
					return math::Vector3(xPos, yPos, 0);
				for (auto& v : tile)
				{
					distance = (xPos - v.x)*(xPos - v.x) + (yPos - v.y)*(yPos - v.y);
					if (distance <= minSpacing*minSpacing)
						isValid = false;
				}
				if (isValid)
					return math::Vector3(xPos, yPos, 0);
				counter++;
				isValid = true;
			}
			return math::Vector3(0, 0, 0);
		}

		float Tree::TotalTreeDensity(gfx::Context& ctx)
		{
			gfx::TextureAccessorL8 texAccessor(*m_densityMap, true);

			float total = 0.0f;
			int texSize = m_densityMap->GetSize().x * m_densityMap->GetSize().y;

			for (int i = 0; i < m_densityMap->GetSize().x; i++)
			{
				for (int k = 0; k < m_densityMap->GetSize().y; k++)
				{
					total += (float)texAccessor.GetPixel(i, k) / m_maxInstance;
				}
			}
			return total;
		}

		void Tree::CreateHitboxPool()
		{
			m_vHBPool.reserve(m_poolSize);
			for (int i = 0; i < m_poolSize;i++)
			{
				m_vHBPool.emplace_back(&m_pWorld->AddRigidBody(m_Material, m_capsule, physics::Transform(math::Vector3(-10 * (i + 1), 0, 0), math::Quaternion()), physics::MotionType::STATIC, physics::CollisionType::STANDARD, 0.0f));
			}
		}
	}
}

