#pragma once
#include <vector>
#include "Math\Vector3.h"
#include "Math\Vector2f.h"
#include <list>
#include "Physics\World.h"
#include "Physics\Material.h"
#include "Physics\CapsuleShape.h"
 
namespace acl
{
	namespace gfx
	{
		class IInstancedMesh;
		class ModelInstance;
		class Camera;
		struct Context;
		class IMesh;
		class IModelLoader;
		class IVertexBuffer;
		class IMeshLoader;
		class ResourceBlock;
		class ITexture;
	}

	namespace render
	{
		class IStage;
	}

	namespace veg
	{
		struct TreeStruct
		{
			math::Vector3 vPosition;
			float scale;
		};

		typedef std::vector<TreeStruct> TreeVector;

		class Tree
		{
		public:
			Tree(gfx::Context& ctx, float cullDistance, unsigned int maxInstance, physics::World* world);

			~Tree(void);

			void Render(const render::IStage& stage, size_t pass);

			void Instanciate(const acl::gfx::Camera& camera);
		private:
			void PrepVisible(const acl::gfx::Camera& camera);

			math::Vector3 RndTreeTilePos(const std::list<math::Vector3>& tile, const math::Vector3& tileSize);

			void SetTreePositions(float spawnThreshold, const math::Vector3& tileSize);

			float TotalTreeDensity(gfx::Context& ctx);

			void LoadTree(gfx::Context& ctx, const math::Vector3& tileSize);

			void CreateHitboxPool();

			gfx::ResourceBlock* m_rBlock;
			gfx::ModelInstance* m_pModel;

			gfx::ITexture* m_heightMap;
			gfx::ITexture* m_densityMap;

			unsigned int m_maxInstance;
			float m_cullDistance;

			const gfx::IMeshLoader* m_meshLoader;
			gfx::IInstancedMesh* m_pInstMesh;

			TreeVector m_vPositions;
			std::vector<std::pair<math::Vector3, float>> m_vGrasses;

			bool m_doneLoading;

			physics::World* m_pWorld;
			std::vector<physics::RigidBody*> m_vHBPool;
			const int m_poolSize;
			physics::Material m_Material;
			physics::CapsuleShape m_capsule;
		};

	}
}

