#include "Components.h"
#include "Surface.h"
#include "Gfx\ModelInstance.h"

namespace acl
{
	namespace ecs
	{

		Water::Water(float offsetY, float strenght, const gfx::FColor& color): pSurface(nullptr), pModel(nullptr), 
			offsetY(offsetY), strength(strenght), color(color)
		{
		}

		Water::~Water(void)
		{
			delete pSurface;
			delete pModel;
		}

	}
}