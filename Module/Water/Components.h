#pragma once
#include "Entity\Component.h"
#include "Gfx\Color.h"

namespace acl
{
	namespace gfx
	{
		class ModelInstance;
	}

	namespace water
	{
		class Surface;
	}

	namespace ecs
	{

		struct Water :
			public Component<Water>
		{
			Water(float offsetY, float strength, const gfx::FColor& color);
			~Water(void);

			float offsetY, strength;
			water::Surface* pSurface;
			gfx::ModelInstance* pModel;
			gfx::FColor color;
		};

	}
}