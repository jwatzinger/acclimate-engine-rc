#include "Export.h"
#include "WaterModule.h"

core::IModule& CreateModule(void)
{
	return *new modules::WaterModule();
}