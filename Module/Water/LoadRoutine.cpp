#include "LoadRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void WaterLoadRoutine::Execute(ecs::Entity& entity, const xml::Node& entityNode) const
		{		
			if(const xml::Node* pWater = entityNode.FirstNode(L"Water"))
			{
				gfx::FColor color(0.5f, 0.75f, 1.0f, 2.0f);
				if(auto pColor = pWater->FirstNode(L"Color"))
					color = gfx::FColor(pColor->Attribute(L"r")->AsFloat(), pColor->Attribute(L"g")->AsFloat(), pColor->Attribute(L"b")->AsFloat(), pColor->Attribute(L"a")->AsFloat());

				entity.AttachComponent<Water>(pWater->Attribute(L"offset")->AsFloat(), pWater->Attribute(L"strength")->AsFloat(), color);
			}		
		}

	}
}
