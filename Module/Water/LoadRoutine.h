#pragma once
#include "Entity\ISubLoadRoutine.h"

namespace acl
{
	namespace ecs
	{

		class WaterLoadRoutine : 
			public ISubLoadRoutine
		{
		public:
			
			void Execute(ecs::Entity& entity, const xml::Node& entityNode) const override;
		};

	}
}

