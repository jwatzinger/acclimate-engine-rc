#include "NoiseMaker.h"
#include <math.h>
#include <minmax.h>
#include "Gfx\TextureAccessors.h"
#include "Gfx\ITextureLoader.h"
#include "Math\Utility.h"
#include "Math\Matrix.h"
#include "Math\Vector.h"
#include "Math\Vector2f.h"
#include "Math\Vector3.h"

namespace acl
{
	namespace water
	{
		#define packednoise true

		#define scale_decimalbits	15
		#define scale_magnitude		(1<<(scale_decimalbits-1))

#pragma warning( disable: 4351 )
		NoiseMaker::NoiseMaker(int sizeX, int sizeY, float strength, float offsetY): m_sizeX(sizeX), 
			m_sizeY(sizeY), m_octaves(8), m_falloff(0.607f), m_time(0.0), m_animSpeed(1.4f), m_timeMulti(1.27f), 
			m_scale(0.38f), m_strenght(strength), m_offsetY(offsetY), m_oNoise(), m_pNoise(), m_rNoise(),
			m_bPaused(false)
		{
			m_pVertices = new SOFTWARESURFACEVERTEX[sizeX*sizeY];	
			for(int v=0; v<m_sizeY; v++)
			{
				for(int u=0; u<m_sizeX; u++)
				{
					m_pVertices[v*sizeX + u].nx = 0.0f;
					m_pVertices[v*sizeX + u].ny = 1.0f;
					m_pVertices[v*sizeX + u].nz = 0.0f;
					m_pVertices[v*sizeX + u].tu = (float)u/(sizeX-1);
					m_pVertices[v*sizeX + u].tv = (float)v/(sizeY-1);
				}
			}

			InitNoise();
		}
#pragma warning( default: 4351 )

		NoiseMaker::~NoiseMaker(void)
		{
			delete[] m_pVertices;
		}

		void NoiseMaker::SetOffset(float y)
		{
			m_offsetY = y;
		}

		void NoiseMaker::SetStrength(float strength)
		{
			m_strenght = strength;
		}

		void NoiseMaker::SetPaused(bool bPaused)
		{
			m_bPaused = bPaused;
		}

		const SOFTWARESURFACEVERTEX* NoiseMaker::GetVertices(void) const
		{
			return m_pVertices;
		}

		const math::Vector4& NoiseMaker::GetCorner(unsigned int id) const
		{
			switch(id)
			{
			case 0:
				return vCorners0;
			case 1:
				return vCorners1;
			case 2:
				return vCorners2;
			case 3:
				return vCorners3;
			default:
				return vCorners0;
			}
		}

		void NoiseMaker::RenderGeometry(const math::Matrix& matrix, double dt)
		{
			CalculateNoise(dt);

			float magnitude = n_dec_magn * m_scale;

			vCorners0 = CalcWorldPos(math::Vector2f(0.0f, 0.0f), matrix);
			vCorners1 = CalcWorldPos(math::Vector2f(1.0f, 0.0f), matrix);
			vCorners2 = CalcWorldPos(math::Vector2f(0.0f, 1.0f), matrix);
			vCorners3 = CalcWorldPos(math::Vector2f(1.0f, 1.0f), matrix);

			float du = 1.0f/float(m_sizeX-1), dv = 1.0f/float(m_sizeY-1), v=0.0f;

			math::Vector4 result;
			int i=0;
			for(int iv=0; iv<m_sizeY; iv++)
			{
				float u = 0.0f;		
				for(int iu=0; iu<m_sizeX; iu++)
				{							
					result.x = (1.0f-v)*( (1.0f-u)*vCorners0.x + u*vCorners1.x ) + v*( (1.0f-u)*vCorners2.x + u*vCorners3.x );				
					result.z = (1.0f-v)*( (1.0f-u)*vCorners0.z + u*vCorners1.z ) + v*( (1.0f-u)*vCorners2.z + u*vCorners3.z );				
					result.w = (1.0f-v)*( (1.0f-u)*vCorners0.w + u*vCorners1.w ) + v*( (1.0f-u)*vCorners2.w + u*vCorners3.w );				

					const float divide = 1.0f/result.w;				
					result.x *= divide;
					result.z *= divide;

					m_pVertices[i].x = result.x;
					m_pVertices[i].z = result.z;
					m_pVertices[i].y = GetHeightDual((int)(magnitude*result.x), (int)(magnitude*result.z) ) + m_offsetY;

					i++;
					u += du;
				}
				v += dv;			
			}

			// smooth the heightdata
			//for(int n=0; n<3; n++)
			for(int v=1; v<(m_sizeY-1); v++)
			{
				for(int u=1; u<(m_sizeX-1); u++)
				{				
					m_pVertices[v*m_sizeX + u].y =	0.2f * (m_pVertices[v*m_sizeX + u].y +
						m_pVertices[v*m_sizeX + (u+1)].y + 
						m_pVertices[v*m_sizeX + (u-1)].y + 
						m_pVertices[(v+1)*m_sizeX + u].y + 
						m_pVertices[(v-1)*m_sizeX + u].y);															
				}
			}
		}

		void NoiseMaker::InitNoise(void)
		{
			// create noise (uniform)
			float tempnoise[n_size_sq*noise_frames];
			for(int i=0; i<(n_size_sq*noise_frames); i++)
			{
				//this->noise[i] = rand()&0x0000FFFF;		
				float temp = (float) rand()/RAND_MAX;		
				tempnoise[i] = 4*(temp - 0.5f);	
			}	

			for(int frame=0; frame<noise_frames; frame++)
			{
				for(int v=0; v<n_size; v++)
				{
					for(int u=0; u<n_size; u++)
					{	
						int v0 = ((v-1)&n_size_m1)*n_size,
							v1 = v*n_size,
							v2 = ((v+1)&n_size_m1)*n_size,
							u0 = ((u-1)&n_size_m1),
							u1 = u,
							u2 = ((u+1)&n_size_m1),					
							f  = frame*n_size_sq;
						float temp = (1.0f/14.0f) * (	tempnoise[f + v0 + u0] + tempnoise[f + v0 + u1] + tempnoise[f + v0 + u2] +
												tempnoise[f + v1 + u0] + 6.0f*tempnoise[f + v1 + u1] + tempnoise[f + v1 + u2] +
												tempnoise[f + v2 + u0] + tempnoise[f + v2 + u1] + tempnoise[f + v2 + u2]);
									  
						m_noise[frame*n_size_sq + v*n_size + u] = (int)(noise_magnitude*temp);
					}
				}
			}	
		}

		math::Vector4 NoiseMaker::CalcWorldPos(const math::Vector2f& vector, const math::Matrix& matrix)
		{
			math::Vector4 vOrigin(vector.x, vector.y, -1, 1);
			math::Vector4 vDirection(vector.x, vector.y, 1, 1);

			vOrigin = matrix.TransformVector4(vOrigin);
			vDirection = matrix.TransformVector4(vDirection);
			vDirection -= vOrigin;    

			float l = -vOrigin.y / vDirection.y;	// assumes the plane is y=0  
  
			return vOrigin + vDirection*l;
		}

		void NoiseMaker::CalculateNoise(double dt)
		{
			unsigned int octaves = min(m_octaves, max_octaves);

			float multitable[32];
			float sum = 0.0f;
			for(unsigned int i = 0; i<octaves; i++)
			{
				multitable[i] = powf(m_falloff, 1.0f*i);
				sum += multitable[i];
			}

			for(unsigned int i = 0; i<octaves; i++)
			{
				multitable[i] /= sum;
			}

			if( !m_bPaused )
				m_time += dt;
	
			double	r_timemulti = 1.0;

			for(unsigned int o=0; o<octaves; o++)
			{		
				unsigned int image[3];
				int amount[3];
				double dImage, fraction = modf(m_time*r_timemulti,&dImage);
				int iImage = (int)dImage;
				amount[0] = (int)(scale_magnitude*multitable[o]*(pow(sin((fraction+2)*math::PI/3),2)/1.5));
				amount[1] = (int)(scale_magnitude*multitable[o]*(pow(sin((fraction+1)*math::PI/3),2)/1.5));
				amount[2] = (int)(scale_magnitude*multitable[o]*(pow(sin((fraction)*math::PI/3),2)/1.5));
				image[0] = (iImage) & noise_frames_m1;
				image[1] = (iImage+1) & noise_frames_m1;
				image[2] = (iImage+2) & noise_frames_m1;
				{	
					for(int i=0; i<n_size_sq; i++)
					{
						m_oNoise[i + n_size_sq*o] =	(	((amount[0] * m_noise[i + n_size_sq * image[0]])>>scale_decimalbits) + 
														((amount[1] * m_noise[i + n_size_sq * image[1]])>>scale_decimalbits) + 
														((amount[2] * m_noise[i + n_size_sq * image[2]])>>scale_decimalbits));
					}
				}

				r_timemulti *= m_timeMulti;
			}

			if(packednoise)
			{
				int octavepack = 0;
				for(unsigned int o=0; o<octaves; o+=n_packsize)
				{
					for(int v=0; v<np_size; v++)
					for(int u=0; u<np_size; u++)
					{
						m_pNoise[v*np_size+u+octavepack*np_size_sq] = m_oNoise[(o+3)*n_size_sq + (v&n_size_m1)*n_size + (u&n_size_m1)];
						m_pNoise[v*np_size+u+octavepack*np_size_sq] += MapSample( u, v, 3, o);
						m_pNoise[v*np_size+u+octavepack*np_size_sq] += MapSample( u, v, 2, o+1);
						m_pNoise[v*np_size+u+octavepack*np_size_sq] += MapSample( u, v, 1, o+2);				
					}
					octavepack++;
			
				}
			}
		}

		int NoiseMaker::MapSample(int u, int v, int upsamplepower, int octave)
		{
			int magnitude = 1<<upsamplepower;
			int pu = u >> upsamplepower;
			int pv = v >> upsamplepower;	
			int fu = u & (magnitude-1);
			int fv = v & (magnitude-1);
			int fu_m = magnitude - fu;
			int fv_m = magnitude - fv;

			int o = fu_m*fv_m*m_oNoise[octave*n_size_sq + ((pv)&n_size_m1)*n_size + ((pu)&n_size_m1)] +
					fu*fv_m*m_oNoise[octave*n_size_sq + ((pv)&n_size_m1)*n_size + ((pu+1)&n_size_m1)] +
					fu_m*fv*m_oNoise[octave*n_size_sq + ((pv+1)&n_size_m1)*n_size + ((pu)&n_size_m1)] +
					fu*fv*m_oNoise[octave*n_size_sq + ((pv+1)&n_size_m1)*n_size + ((pu+1)&n_size_m1)];

			return o >> (upsamplepower+upsamplepower);
		}

		void NoiseMaker::UploadNoise(gfx::ITexture* pTextures[2]) const
		{
			for(int t=0; t<2; t++)
			{
				gfx::TextureAccessorL16 accessor(*pTextures[t], false);
				int offset = np_size_sq*t;

				for(int i=0; i<np_size_sq; i++)
					accessor.SetAt(i, 32768+m_pNoise[i+offset]);
			}
		}

	}
}
