#pragma once
#include "Math\Vector4.h"

namespace acl
{
	namespace gfx
	{
		class ITexture;
	}

	namespace math
	{
		struct Matrix;
		struct Vector2f;
	}

	namespace water
	{
#define n_bits				5
#define n_size				(1<<(n_bits-1))
#define n_size_m1			(n_size - 1)
#define n_size_sq			(n_size*n_size)
#define n_size_sq_m1		(n_size_sq - 1)

#define n_packsize			4

#define n_dec_bits			12
#define n_dec_magn			4096
#define n_dec_magn_m1		4095

#define np_bits				(n_bits+n_packsize-1)
#define np_size				(1<<(np_bits-1))
#define np_size_m1			(np_size-1)
#define np_size_sq			(np_size*np_size)
#define np_size_sq_m1		(np_size_sq-1)

#define noise_decimalbits	15
#define noise_magnitude		(1<<(noise_decimalbits-1))

#define max_octaves 32

#define noise_frames		256
#define noise_frames_m1		(noise_frames-1)

		struct SOFTWARESURFACEVERTEX
		{
			float x, y, z;
			float tu, tv;
			float nx, ny, nz;
		};

		class NoiseMaker
		{
		public:
			NoiseMaker(int sizeX, int sizeY, float strength, float offsetY);
			~NoiseMaker(void);

			void SetOffset(float y);
			void SetStrength(float strength);
			void SetPaused(bool bPaused);

			const SOFTWARESURFACEVERTEX* GetVertices(void) const;
			const math::Vector4& GetCorner(unsigned int id) const;

			void RenderGeometry(const math::Matrix& matrix, double dt);
			void UploadNoise(gfx::ITexture* pTextures[2]) const;

		private:

			void InitNoise(void);
			void CalculateNoise(double dt);
			math::Vector4 CalcWorldPos(const math::Vector2f& vector, const math::Matrix& matrix);
			int MapSample(int u, int v, int upsamplepower, int octave);

			float m_falloff, m_animSpeed, m_timeMulti, m_scale, m_strenght, m_offsetY;
			double m_time;
			unsigned int m_octaves;
			bool m_bPaused;

			int m_sizeX, m_sizeY;
			int m_noise[n_size_sq*noise_frames], m_oNoise[n_size_sq*max_octaves], m_pNoise[np_size_sq*(max_octaves>>(n_packsize-1))];
			int* m_rNoise;

			SOFTWARESURFACEVERTEX* m_pVertices;

			math::Vector4 vCorners0, vCorners1, vCorners2, vCorners3;

			int ReadTexelLinearDual(int u, int v, int o);
			float GetHeightDual(int u, int v);
		};

		inline int NoiseMaker::ReadTexelLinearDual(int u, int v, int o)
		{
			int iu, iup, iv, ivp, fu, fv;
			iu = (u>>n_dec_bits)&np_size_m1;
			iv = ((v>>n_dec_bits)&np_size_m1)*np_size;
	
			iup = ((u>>n_dec_bits) + 1)&np_size_m1;
			ivp = (((v>>n_dec_bits) + 1)&np_size_m1)*np_size;
	
			fu = u & n_dec_magn_m1;
			fv = v & n_dec_magn_m1;
		
			int ut01 = ((n_dec_magn-fu)*m_rNoise[iv + iu] + fu*m_rNoise[iv + iup])>>n_dec_bits;
			int ut23 = ((n_dec_magn-fu)*m_rNoise[ivp + iu] + fu*m_rNoise[ivp + iup])>>n_dec_bits;
			int ut = ((n_dec_magn-fv)*ut01 + fv*ut23) >> n_dec_bits;
			return ut;
		}

		inline float NoiseMaker::GetHeightDual(int u, int v)
		{	
			int value=0;	
			m_rNoise = m_pNoise;	// pointer to the current noise source octave
			int hoct = m_octaves / n_packsize;
			for(int i=0; i<hoct; i++)
			{		
				value += ReadTexelLinearDual(u,v,0);
				u = u << n_packsize;
				v = v << n_packsize;
				m_rNoise += np_size_sq;
			}		
			return value*m_strenght/noise_magnitude;
		}

	}
}

