#include "ParameterController.h"
#include "Components.h"
#include "Surface.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Gui\Checkbox.h"
#include "Gui\Sliderbar.h"
#include "System\Convert.h"

namespace acl
{
	namespace water
	{

		ParameterController::ParameterController(gui::Module& module, const ecs::EntityManager& entities): BaseController(module, L"Menu/Parameter.axm"),
			m_pEntities(&entities)
		{
			// pause
			auto pPause = GetWidgetByName<gui::CheckBox>(L"Pause");
			pPause->SigChecked.Connect(this, &ParameterController::OnPause);

			// offset
			auto pOffset = GetWidgetByName<gui::SliderBar>(L"Offset");
			auto& offset = pOffset->GetSlider();
			offset.SigValueChanged.Connect(this, &ParameterController::OnChangeOffset);
			offset.SetValue(0.0f);

			// strength
			auto pStrength = GetWidgetByName<gui::SliderBar>(L"Strength");
			auto& strength = pStrength->GetSlider();
			strength.SigValueChanged.Connect(this, &ParameterController::OnChangeStrength);
			strength.SetValue(0.0f);

			// red
			auto pRed = GetWidgetByName<gui::SliderBar>(L"Red");
			auto& red = pRed->GetSlider();
			red.SigValueChanged.Connect(this, &ParameterController::OnChangeColorRed);
			red.SetValue(0.5f);

			// green
			auto pGreen = GetWidgetByName<gui::SliderBar>(L"Green");
			auto& green = pGreen->GetSlider();
			green.SigValueChanged.Connect(this, &ParameterController::OnChangeColorGreen);
			green.SetValue(0.75f);

			// blue
			auto pBlue = GetWidgetByName<gui::SliderBar>(L"Blue");
			auto& blue = pBlue->GetSlider();
			blue.SigValueChanged.Connect(this, &ParameterController::OnChangeColorBlue);
			blue.SetValue(1.0f);

			// intensity
			auto pIntensity = GetWidgetByName<gui::SliderBar>(L"Intensity");
			auto& intensity = pIntensity->GetSlider();
			intensity.SigValueChanged.Connect(this, &ParameterController::OnChangeColorIntensity);
			intensity.SetValue(2.0f);
		}

		void ParameterController::OnPause(bool bPause)
		{
			if(auto pWater = GetWater())
				if(pWater->pSurface)
					pWater->pSurface->SetPaused(bPause);
		}

		void ParameterController::OnChangeOffset(float offset)
		{
			if(auto pWater = GetWater())
				if(pWater->pSurface)
					pWater->pSurface->SetOffset(offset);
		}

		void ParameterController::OnChangeStrength(float strength)
		{
			if(auto pWater = GetWater())
				if(pWater->pSurface)
					pWater->pSurface->SetStrength(strength);
		}

		void ParameterController::OnChangeColorRed(float red)
		{
			if(auto pWater = GetWater())
				pWater->color.r = red;
		}

		void ParameterController::OnChangeColorGreen(float green)
		{
			if(auto pWater = GetWater())
				pWater->color.g = green;
		}

		void ParameterController::OnChangeColorBlue(float blue)
		{
			if(auto pWater = GetWater())
				pWater->color.b = blue;
		}

		void ParameterController::OnChangeColorIntensity(float intensity)
		{
			if(auto pWater = GetWater())
				pWater->color.a = intensity;
		}

		ecs::Water* ParameterController::GetWater(void) const
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<ecs::Water>();

			if(vEntities.size())
				return vEntities[0]->GetComponent<ecs::Water>();
			else
				return nullptr;
		}

	}
}