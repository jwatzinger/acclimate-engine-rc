#pragma once
#include "Gui\BaseController.h"

namespace acl
{
	namespace ecs
	{
		class EntityManager;
		struct Water;
	}

	namespace water
	{

		class ParameterController :
			public gui::BaseController
		{
		public:
			ParameterController(gui::Module& module, const ecs::EntityManager& entities);

		private:

			void OnPause(bool bPause);
			void OnChangeOffset(float y);
			void OnChangeStrength(float strength);
			void OnChangeColorRed(float red);
			void OnChangeColorGreen(float red);
			void OnChangeColorBlue(float red);
			void OnChangeColorIntensity(float intensity);

			ecs::Water* GetWater(void) const;

			const ecs::EntityManager* m_pEntities;

		};

	}
}

