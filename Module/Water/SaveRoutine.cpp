#include "SaveRoutine.h"
#include "Components.h"
#include "Entity\Entity.h"
#include "System\Convert.h"
#include "XML\Node.h"

namespace acl
{
	namespace ecs
	{

		void WaterSaveRoutine::Execute(const ecs::Entity& entity, xml::Node& entityNode) const
		{		
			if(Water* pWater = entity.GetComponent<Water>())
			{
				auto& component = entityNode.InsertNode(L"Water");
				component.ModifyAttribute(L"offset", conv::ToString(pWater->offsetY));
				component.ModifyAttribute(L"strength", conv::ToString(pWater->strength));
				
				auto& color = component.InsertNode(L"Color");
				color.ModifyAttribute(L"r", conv::ToString(pWater->color.r));
				color.ModifyAttribute(L"g", conv::ToString(pWater->color.g));
				color.ModifyAttribute(L"b", conv::ToString(pWater->color.b));
				color.ModifyAttribute(L"a", conv::ToString(pWater->color.a));
			}
		}

	}
}
