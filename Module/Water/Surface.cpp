#include "Surface.h"
#include <minmax.h>
#include "Gfx\IMesh.h"
#include "Gfx\VertexBufferAccessor.h"
#include "System\Bit.h"

namespace acl
{
	namespace water
	{

		Surface::Surface(const math::Vector3& vPos, const math::Vector3& vNormal, float strenght, gfx::IMesh& mesh): m_vPos(vPos), 
			m_vNormal(vNormal), m_plane(vNormal, vPos), m_strength(strenght), m_elevation(7.0f), m_camera(math::Vector2(0, 0), math::Vector3(0.0f, 0.0f, 0.0f)), 
			m_pMesh(&mesh), m_noiseMaker(128, 256, m_strength, vPos.y)
		{
			SetDisplacementAmplitude(0.0f);
		}

		void Surface::SetOffset(float offset)
		{
			m_vPos.y = offset;
			m_noiseMaker.SetOffset(offset);
		}

		void Surface::SetStrength(float strength)
		{
			m_strength = strength;
			m_noiseMaker.SetStrength(m_strength);
		}

		void Surface::SetPaused(bool bPaused)
		{
			m_noiseMaker.SetPaused(bPaused);
		}

		float Surface::GetOffsetY(void) const
		{
			return m_vPos.y;
		}

		float Surface::GetStrength(void) const
		{
			return m_strength;
		}

		const math::Vector4& Surface::GetCorner(unsigned int id) const
		{
			return m_noiseMaker.GetCorner(id);
		}

		void Surface::Update(const gfx::Camera& camera, double dt)
		{
			math::Matrix mRange;
			bool bPlaneInFrustum = GetMinMax(mRange, camera);

			if(bPlaneInFrustum)
			{
				m_noiseMaker.RenderGeometry(mRange, dt);

				gfx::VertexBufferAccessor<SOFTWARESURFACEVERTEX> accessor(*m_pMesh);
				SOFTWARESURFACEVERTEX* pVertices = accessor.GetData();
				
				const SOFTWARESURFACEVERTEX* pInputVertices = m_noiseMaker.GetVertices();
				memcpy(pVertices, pInputVertices, sizeof(SOFTWARESURFACEVERTEX)* 128 * 256);
			}
		}

		bool Surface::GetMinMax(math::Matrix& vMatrix, const gfx::Camera& camera)
		{
			SetDisplacementAmplitude(m_strength);

			math::Vector3 vFrustum[8], vProjPoints[24];		// frustum to check the camera against

			int numPoints=0;
			int cube[] = {	0,1,	0,2,	2,3,	1,3,
							0,4,	2,6,	3,7,	1,5,
							4,6,	4,5,	5,7,	6,7};	// which frustum points are connected together?

			const math::Matrix mInvViewProj = camera.GetViewProjectionMatrix().inverse();

			vFrustum[0] = mInvViewProj.TransformCoord(math::Vector3(-1, -1, -1));
			vFrustum[1] = mInvViewProj.TransformCoord(math::Vector3(+1, -1, -1));
			vFrustum[2] = mInvViewProj.TransformCoord(math::Vector3(-1, +1, -1));
			vFrustum[3] = mInvViewProj.TransformCoord(math::Vector3(+1, +1, -1));
			vFrustum[4] = mInvViewProj.TransformCoord(math::Vector3(-1, -1, +1));
			vFrustum[5] = mInvViewProj.TransformCoord(math::Vector3(+1, -1, +1));
			vFrustum[6] = mInvViewProj.TransformCoord(math::Vector3(-1, +1, +1));
			vFrustum[7] = mInvViewProj.TransformCoord(math::Vector3(+1, +1, +1));

			const math::Vector3& vUpperNrm = m_upperBound.GetNormal();
			const float vUpperD = m_upperBound.GetD();
			const math::Vector3& vLowerNrm = m_lowerBound.GetNormal();
			const float vLowerD = m_lowerBound.GetD();

			// check intersections with upper_bound and lower_bound	
			for(int i=0; i<12; i++)
			{
				int src=cube[i*2], dst=cube[i*2+1];

				if ((vUpperNrm.Dot(vFrustum[src]) + vUpperD) / (vUpperNrm.Dot(vFrustum[dst]) + vUpperD) < 0.0f)	
				{
					m_upperBound.IntersectLine(vFrustum[src], vFrustum[dst], &vProjPoints[numPoints++]);
				}

				if ((vLowerNrm.Dot(vFrustum[src]) + vLowerD) / (vLowerNrm.Dot(vFrustum[dst]) + vLowerD) < 0.0f)	
				{
					m_lowerBound.IntersectLine(vFrustum[src], vFrustum[dst], &vProjPoints[numPoints++]);
				}
			}

			// check if any of the frustums vertices lie between the upper_bound and lower_bound planes
			for(int i=0; i<8; i++)
			{		
				if ((vUpperNrm.Dot(vFrustum[i]) + vUpperD) / (vLowerNrm.Dot(vFrustum[i]) + vLowerD) < 0.0f)
				{			
					vProjPoints[numPoints++] = vFrustum[i];
				}		
			}	

			//
			// create the camera the grid will be projected from
			//
			m_camera = camera;
			const math::Vector3& vPosition = m_camera.GetPosition();
			// make sure the camera isn't too close to the plane
			float heightInPlane = vLowerNrm.Dot(vPosition);

			bool keepItSimple = false;
			bool underwater = heightInPlane < 0.0f;

			if(!keepItSimple)
			{
				math::Vector3 vAimpoint, vAimpoint2;		

				if (heightInPlane < (m_strength+m_elevation))
				{					
					if(underwater)
						m_camera.SetPosition(vPosition + vLowerNrm * (m_strength + m_elevation - 2*heightInPlane));															
					else
						m_camera.SetPosition(vPosition + vLowerNrm*(m_strength + m_elevation - heightInPlane));
				} 
		
				// aim the projector at the point where the camera view-vector intersects the plane
				// if the camera is aimed away from the plane, mirror it's view-vector against the plane
				if( (m_plane.GetNormal().Dot(camera.GetDirection()) < 0.0f) log_xor (m_plane.GetNormal().Dot(camera.GetPosition()) < 0.0f)  )
				{				
					m_plane.IntersectLine(camera.GetPosition(), camera.GetLookAt(), &vAimpoint);
				}
				else
				{
					math::Vector3 vFlipped;
					vFlipped = camera.GetDirection() - 2*m_vNormal*camera.GetDirection().Dot(m_vNormal);
					m_plane.IntersectLine(camera.GetPosition(), camera.GetPosition() + vFlipped, &vAimpoint);
				}

				// force the point the camera is looking at in a plane, and have the projector look at it
				// works well against horizon, even when camera is looking upwards
				// doesn't work straight down/up
				float af = abs(m_plane.GetNormal().Dot(camera.GetDirection()));
				//af = 1 - (1-af)*(1-af)*(1-af)*(1-af)*(1-af);
				//aimpoint2 = (rendering_camera->position + rendering_camera->zfar * rendering_camera->forward);
				vAimpoint2 = (camera.GetPosition() + 10.0f * camera.GetDirection());
				vAimpoint2 = vAimpoint2 - m_vNormal*vAimpoint2.Dot(m_vNormal);
		
				// fade between aimpoint & aimpoint2 depending on view angle
			
				vAimpoint = vAimpoint*af + vAimpoint2*(1.0f-af);
				//aimpoint = aimpoint2;
				
				m_camera.SetLookAt(vAimpoint);
			}

			for(int i=0; i<numPoints; i++)
			{
				// project the point onto the surface plane
				vProjPoints[i] = vProjPoints[i] - m_vNormal*vProjPoints[i].Dot(m_vNormal);	
			}

			const math::Matrix& mViewProj = m_camera.GetViewProjectionMatrix();
			for(int i=0; i<numPoints; i++)
			{
				vProjPoints[i] = mViewProj.TransformCoord(vProjPoints[i]);
			}

			// get max/min x & y-values to determine how big the "projection window" must be
			if (numPoints > 0)
			{
				float x_min = vProjPoints[0].x;
				float x_max = vProjPoints[0].x;
				float y_min = vProjPoints[0].y;
				float y_max = vProjPoints[0].y;
				for(int i=1; i<numPoints; i++)
				{
					x_max = max(vProjPoints[i].x, x_max);
					x_min = min(vProjPoints[i].x, x_min);
					y_max = max(vProjPoints[i].y, y_max);
					y_min = min(vProjPoints[i].y, y_min);
				}		

				// build the packing matrix that spreads the grid across the "projection window"
				math::Matrix pack(	x_max-x_min,	0,				0,		x_min,
									0,				y_max-y_min,	0,		y_min,
									0,				0,				1,		0,	
									0,				0,				0,		1);
				pack = pack.transpose();

				vMatrix = pack*mViewProj.inverse();

				return true;
			}
			return false;
		}

		void Surface::SetDisplacementAmplitude(float amplitude)
		{
			const math::Vector3 vDist = amplitude * m_vNormal;
			m_upperBound.Set(m_vNormal, m_vPos + vDist);
			m_lowerBound.Set(m_vNormal, m_vPos - vDist);
		}
		
		void Surface::UploadNoise(gfx::ITexture* pTextures[2]) const
		{
			m_noiseMaker.UploadNoise(pTextures);
		}

	}
}
