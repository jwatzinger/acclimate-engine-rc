#pragma once
#include "NoiseMaker.h"
#include "Math\Plane.h"
#include "Gfx\Camera.h"

namespace acl
{
	namespace gfx
	{
		class Camera;
		class IMesh;
	}

	namespace water
	{

		class Surface
		{
		public:
			Surface(const math::Vector3& vPos, const math::Vector3& vNormal, float strength, gfx::IMesh& mesh);

			void SetOffset(float offset);
			void SetStrength(float strenght);
			void SetPaused(bool bPause);

			float GetOffsetY(void) const;
			float GetStrength(void) const;
			const math::Vector4& GetCorner(unsigned int id) const;

			void Update(const gfx::Camera& camera, double dt);
			void UploadNoise(gfx::ITexture* pTextures[2]) const;

		private:

			bool GetMinMax(math::Matrix& vMatrix, const gfx::Camera& camera);
			void SetDisplacementAmplitude(float amplitude);

			float m_strength, m_elevation;

			NoiseMaker m_noiseMaker;

			gfx::Camera m_camera;
			gfx::IMesh* m_pMesh;

			math::Vector3 m_vPos, m_vNormal;
			math::Plane m_plane, m_upperBound, m_lowerBound;
		};

	}
}