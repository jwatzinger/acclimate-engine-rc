#include "WaterModule.h"
#include "WaterSystem.h"
#include "WaterRenderSystem.h"
#include "LoadRoutine.h"
#include "SaveRoutine.h"
#include "Components.h"
#include "ParameterController.h"
#include "Core\BaseContext.h"
#include "Entity\SystemManager.h"
#include "Entity\Saver.h"
#include "Entity\Loader.h"
#include "Entity\ComponentFactory.h"
#include "Entity\ComponentRegistry.h"
#include "Gfx\IResourceLoader.h"
#include "Render\Context.h"
#include "Render\ILoader.h"

namespace acl
{
	namespace modules
	{

		WaterModule::WaterModule(void): m_pSystems(nullptr), m_pCtrl(nullptr)
		{
		}
				
		void WaterModule::OnLoadResources(const gfx::LoadContext& context)
		{
			context.pLoader->Load(L"Resources.axm");
		}

		void WaterModule::OnLoadRender(const render::Context& context)
		{
			context.loader.Load(L"Render.axm");
		}

		void WaterModule::OnInit(const core::GameStateContext& context)
		{
			m_pSystems = &context.ecs.systems;

			// system
			context.ecs.systems.AddSystem<ecs::WaterSystem>(context.gfx.resources.meshes, context.gfx.resources.textures);
			context.ecs.systems.AddSystem<ecs::WaterRenderSystem>(context.gfx.load.textures, context.gfx.load.zbuffers, context.gfx.resources.models, context.render.renderer, context.gfx.effect);

			// io
			ecs::Saver::RegisterSubRoutine<ecs::WaterSaveRoutine>(L"Water");
			ecs::Loader::RegisterSubRoutine<ecs::WaterLoadRoutine>(L"Water");

			// components
			ecs::ComponentRegistry::RegisterComponent<ecs::Water>(L"Water");
			context.ecs.componentFactory.Register<ecs::Water, float, float, const gfx::FColor&>();

			m_pCtrl = new water::ParameterController(context.gui.module, context.ecs.entities);
		}

		void WaterModule::OnUninit(const core::GameStateContext& context)
		{
			// system
			m_pSystems->RemoveSystem<ecs::WaterSystem>();
			m_pSystems->RemoveSystem<ecs::WaterRenderSystem>();

			// components
			ecs::ComponentRegistry::UnregisterComponent(L"Water");
			context.ecs.componentFactory.Unregister<ecs::Water>();

			// io
			ecs::Saver::UnregisterSubRoutine(L"Water");
			ecs::Loader::UnregisterSubRoutine(L"Water");

			m_pSystems = nullptr;

			delete m_pCtrl;
		}

		void WaterModule::OnUpdate(double dt)
		{
			m_pSystems->UpdateSystem<ecs::WaterSystem>(dt);
		}

		void WaterModule::OnRender(void) const
		{
			m_pSystems->UpdateSystem<ecs::WaterRenderSystem>(0.0f);
		}

	}
}
