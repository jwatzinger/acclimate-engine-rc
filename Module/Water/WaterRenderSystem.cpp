#include "WaterRenderSystem.h"
#include "Surface.h"
#include "Components.h"
#include "ApiInclude.h"
#include "Entity\Message.h"
#include "Entity\MessageRegistry.h"
#include "Entity\MessageFactory.h"
#include "Entity\MessageManager.h"
#include "Entity\EntityManager.h"
#include "Entity\Entity.h"
#include "Gfx\ITextureLoader.h"
#include "Gfx\IZBufferLoader.h"
#include "Gfx\Camera.h"
#include "Gfx\IMaterial.h"
#include "Gfx\Utility.h"
#include "Gfx\FullscreenEffect.h"
#include "Gfx\FxInstance.h"
#include "Gfx\ModelInstance.h"
#include "Math\Vector.h"
#include "Math\Matrix.h"
#include "Math\Plane.h"
#include "Math\Rect.h"
#include "Render\IRenderer.h"
#include "Render\IStage.h"

namespace acl
{
	namespace ecs
	{

		const unsigned int cRefSize = 1024;

		WaterRenderSystem::WaterRenderSystem(const gfx::ITextureLoader& textureLoader, const gfx::IZBufferLoader& zBufferLoader, const gfx::Models& models, render::IRenderer& renderer, gfx::FullscreenEffect& effect) :
			m_camera(math::Vector2(0, 0), math::Vector3(0.0f, 0.0f, 0.0f)), m_pModels(&models), m_pWorldStage(renderer.GetStage(L"waterworld")), m_pHeightmapStage(renderer.GetStage(L"waterhmap")),
			m_pNormalmapStage(renderer.GetStage(L"waternmap"))
		{
			const math::Vector2 vRefSize(cRefSize, cRefSize);

			/************************************************
			* Reflection
			************************************************/

			m_pReflectionMap = textureLoader.Create(L"ReflectionMap", vRefSize, gfx::TextureFormats::A32, gfx::LoadFlags::RENDER_TARGET);

			auto pDepthBuffer = zBufferLoader.Create(L"Reflection", vRefSize);

			size_t id = renderer.AddQueue(L"Scene");
			m_pReflectionStage = renderer.AddStage(L"waterReflection", L"Scene", id, render::CLEAR | render::CLEAR_Z);
			m_pReflectionStage->SetRenderTarget(0, m_pReflectionMap);
			m_pReflectionStage->SetDepthBuffer(pDepthBuffer);
			m_pReflectionStage->SetViewport(math::Rect(0, 0, cRefSize, cRefSize));

			/************************************************
			* Refraction
			************************************************/

			auto pRefractionMap = textureLoader.Create(L"RefractionMap", vRefSize, gfx::TextureFormats::X32, gfx::LoadFlags::RENDER_TARGET);
			auto pRefractionPos = textureLoader.Create(L"RefractionPos", vRefSize, gfx::TextureFormats::A16F, gfx::LoadFlags::RENDER_TARGET);

			auto pRefractionDepthBuffer = zBufferLoader.Create(L"Refraction", vRefSize);

			id = renderer.AddQueue(L"Scene");
			m_pRefractionStage = renderer.AddStage(L"waterRefraction", L"Scene", id, render::CLEAR | render::CLEAR_Z);
			m_pRefractionStage->SetRenderTarget(0, pRefractionMap);
			m_pRefractionStage->SetRenderTarget(1, pRefractionPos);
			m_pRefractionStage->SetDepthBuffer(pRefractionDepthBuffer);
			m_pRefractionStage->SetViewport(math::Rect(0, 0, cRefSize, cRefSize));

			/************************************************
			* Underwater
			************************************************/

			m_pRefractionOut = textureLoader.Create(L"RefractionOut", vRefSize, gfx::TextureFormats::A16F, gfx::LoadFlags::RENDER_TARGET);

			m_pUnderwater = &effect.CreateInstance(L"Underwater", L"Prelight", L"RefractionOut", 0);
			m_pUnderwater->SetTexture(0, pRefractionMap);
			m_pUnderwater->SetTexture(1, pRefractionPos);
		}

		WaterRenderSystem::~WaterRenderSystem(void)
		{
			delete m_pUnderwater;
		}

		void WaterRenderSystem::Init(MessageManager& messages)
		{
			messages.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void WaterRenderSystem::Update(double dt)
		{
			auto vEntities = m_pEntities->EntitiesWithComponents<Water>();

			for(auto& entity : vEntities)
			{
				auto pWater = entity->GetComponent<Water>();

				if(!pWater->pSurface)
					return;

				if(!pWater->pModel)
				{
					auto pModel = m_pModels->Get(L"Water");
					pWater->pModel = &pModel->CreateInstance();

					auto pMaterial = pModel->GetMaterial(0);
					pMaterial->SetTexture(0, m_pReflectionMap);
					pMaterial->SetTexture(1, m_pRefractionOut);
				}

				// draw reflections
				m_pMessageFactory->DeliverMessage(L"RenderScene", m_camera, *m_pReflectionStage, 4);

				// draw refractions
				m_pRefractionStage->SetClearColor(pWater->color * pWater->color.a);
				m_pMessageFactory->DeliverMessage(L"RenderScene", m_camera, *m_pRefractionStage, 5);

				// draw underwater
				m_pUnderwater->SetShaderConstant(0, (float*)&pWater->color, 1);
				const float waterPos = pWater->offsetY + pWater->strength / 2.0f;
				m_pUnderwater->SetShaderConstant(2, (const float*)&waterPos, 1);

				m_pUnderwater->Draw();

				// draw heightmap
				pWater->pModel->Draw(*m_pHeightmapStage, 1);

				// draw normalmap

				const math::Vector2 vNormalMapSize(512, 1024);

				const math::Vector4& vCorner0 = pWater->pSurface->GetCorner(0);
				const math::Vector4& vCorner1 = pWater->pSurface->GetCorner(1);
				const math::Vector4& vCorner2 = pWater->pSurface->GetCorner(2);
				const math::Vector4& vCorner3 = pWater->pSurface->GetCorner(3);
				const float arr[5][4] =
				{
					{vCorner0.x, vCorner0.y, vCorner0.z, vCorner0.w},
					{vCorner1.x, vCorner1.y, vCorner1.z, vCorner1.w},
					{vCorner2.x, vCorner2.y, vCorner2.z, vCorner2.w},
					{vCorner3.x, vCorner3.y, vCorner3.z, vCorner3.w},
					{1.0f/vNormalMapSize.x, 1.0f/vNormalMapSize.y, 0.0f, 0.0f}
				};

				m_pNormalmapStage->SetVertexConstant(0, (float*)arr, 5);
				
				const float pArr[3] = {1.0f/vNormalMapSize.x, 1.0f/vNormalMapSize.y, 2*pWater->pSurface->GetStrength()};
				m_pNormalmapStage->SetPixelConstant(0, pArr, 1);
				pWater->pModel->Draw(*m_pNormalmapStage, 2);

				// draw water
				pWater->pModel->Draw(*m_pWorldStage, 0);
			}
		}

		void WaterRenderSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
			{
				auto vEntities = m_pEntities->EntitiesWithComponents<Water>();

				if(!vEntities.size())
					return;

				auto pSurface = vEntities[0]->GetComponent<Water>()->pSurface;
				if(!pSurface)
					return;

				const gfx::Camera& camera = *pUpdate->GetParam<gfx::Camera>(L"Camera");
				const math::Matrix& mViewProj = camera.GetViewProjectionMatrix();

				/************************************
				* Worldstage
				************************************/ 

				m_pWorldStage->SetVertexConstant(0, (const float*)&mViewProj, 4);

				const float h = pSurface->GetOffsetY();
				const math::Vector3& vPos = camera.GetPosition();

				const float worldIn[4] = {vPos.x, vPos.y, vPos.z, h};
				m_pWorldStage->SetVertexConstant(4, worldIn, 1);

				/************************************
				* Reflection stage
				************************************/
				
				math::Matrix mReflection( 1.0f, 0.0f, 0.0f, 0.0f,
										  0.0f, -1.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 1.0f, 0.0f,
										  0.0f, h*2.0f, 0.0f, 1.0f );

				math::Vector4 vClipPlane( 0.0f, 1.0f, 0.0f, -h);
				mReflection *= gfx::clipPlaneMatrix(mViewProj, vClipPlane);

				m_pReflectionStage->SetVertexConstant(0, (float*)&(mReflection), 4);

				m_camera = camera;
				m_camera.SetPosition(mReflection.TransformCoord(m_camera.GetPosition()));

				m_pReflectionStage->SetVertexConstant(4, (float*)&vPos, 1);

				/************************************
				* Refraction stage
				************************************/

				math::Vector4 vRefractionClipPlane(0.0f, -1.0f, 0.0f, h+pSurface->GetStrength()/2.0f);

				m_pRefractionStage->SetVertexConstant(0, (float*)&gfx::clipPlaneMatrix(mViewProj, vRefractionClipPlane), 4);
				m_pRefractionStage->SetVertexConstant(4, (float*)&camera.GetPosition(), 1);

				/************************************
				* Underwater stage
				************************************/

				m_pUnderwater->SetShaderConstant(1, (const float*)&vPos, 1);

			}
		}

	}
}
