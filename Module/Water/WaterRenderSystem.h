#pragma once
#include "Entity\System.h"
#include "Gfx\Camera.h"
#include "Gfx\Models.h"

namespace acl
{
	namespace gfx
	{
		class ITextureLoader;
		class IZBufferLoader;
		class ITexture;
		class FullscreenEffect;
		class FxInstance;
	}

	namespace render
	{
		class IStage;
		class IRenderer;
	};

	namespace ecs
	{

		class WaterRenderSystem :
			public System<WaterRenderSystem>
		{
		public:
			WaterRenderSystem(const gfx::ITextureLoader& textureLoader, const gfx::IZBufferLoader& zBufferLoader, const gfx::Models& models, render::IRenderer& renderer, gfx::FullscreenEffect& effect);
			~WaterRenderSystem(void);

			void Init(MessageManager& messages);

			void Update(double dt) override;

			void ReceiveMessage(const BaseMessage& message);

		private:

			gfx::ITexture* m_pReflectionMap, *m_pRefractionOut;
			gfx::Camera m_camera;
			const gfx::Models* m_pModels;
			gfx::FxInstance* m_pUnderwater;

			render::IStage* m_pReflectionStage, *m_pRefractionStage;
			render::IStage* m_pWorldStage, *m_pHeightmapStage, *m_pNormalmapStage;
		};

	}
}

