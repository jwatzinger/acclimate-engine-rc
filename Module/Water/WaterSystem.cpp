#include "WaterSystem.h"
#include "Components.h"
#include "Surface.h"
#include "Entity\MessageManager.h"
#include "Entity\Entity.h"
#include "Entity\EntityManager.h"
#include "Entity\MessageRegistry.h"
#include "Entity\Message.h"

namespace acl
{
	namespace ecs
	{

		WaterSystem::WaterSystem(const gfx::Meshes& meshes, const gfx::Textures& textures): m_pMeshes(&meshes), 
			m_pCamera(nullptr), m_pTextures(&textures)
		{
		}

		void WaterSystem::Init(MessageManager& messages)
		{
			messages.Subscribe(MessageRegistry::GetMessageId(L"UpdateCamera"), *this);
		}

		void WaterSystem::Update(double dt)
		{
			if(!m_pCamera)
				return;

			auto vEntities = m_pEntities->EntitiesWithComponents<Water>();

			for(auto& entity : vEntities)
			{
				auto pWater = entity->GetComponent<Water>();

				if(!pWater->pSurface)
					pWater->pSurface = new water::Surface(math::Vector3(0.0f, pWater->offsetY, 0.0f), math::Vector3(0.0f, 1.0f, 0.0f), pWater->strength, *m_pMeshes->Get(L"water"));

				pWater->pSurface->Update(*m_pCamera, dt);

				// calculate heightmap
				gfx::ITexture* pTextures[2] = {m_pTextures->Get(L"noise0"), m_pTextures->Get(L"noise1")};
				pWater->pSurface->UploadNoise(pTextures);
			}
		}

		void WaterSystem::ReceiveMessage(const BaseMessage& message)
		{
			if(auto pUpdate = message.Check(MessageRegistry::GetMessageId(L"UpdateCamera")))
				m_pCamera = pUpdate->GetParam<gfx::Camera>(L"Camera");
		}

	}
}