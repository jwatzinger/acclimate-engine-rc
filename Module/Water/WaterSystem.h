#pragma once
#include "Entity\System.h"
#include "Gfx\Meshes.h"
#include "Gfx\Textures.h"

namespace acl
{
	namespace gfx
	{
		class Camera;
	}

	namespace ecs
	{

		class WaterSystem :
			public System<WaterSystem>
		{
		public:
			WaterSystem(const gfx::Meshes& meshes, const gfx::Textures& textures);
			
			void Init(MessageManager& messages);

			void Update(double dt);

			void ReceiveMessage(const BaseMessage& message);

		private:

			const gfx::Camera* m_pCamera;
			const gfx::Meshes* m_pMeshes;
			const gfx::Textures* m_pTextures;
		};

	}
}
