@echo off
IF "%1"=="" GOTO ALL
GOTO SPEC

:ALL
echo Copy Everything
for /f %%i in (ModulesList.txt) do (
xcopy /y "Release\%%i.dll" "Game\Modules\%%i\"
)
GOTO END

:SPEC
echo Copy %1%
pushd ..\..\
xcopy /y "Release\%1.dll" "Game\Modules\%1%\"
popd


:END
exit
